//
//  Header.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 18/02/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#ifndef buzz_Header_h
#define buzz_Header_h





#define RGBA(r, g, b, a) \
[UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]
#define BACKGROUNDIMAGE [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg"]]
#define CHAT_BACKGROUNDIMAGE [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg"]]
#define BACKGROUNDIMAGE_4 [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_4"]]


#define LOGOIMAGE [UIImage imageNamed:@"logo.png"]
#define FontString @"Verdana"
#define IS_IPHONE_5 ([UIScreen mainScreen].bounds.size.height == 568.0)
#define IS_IPHONE_4 ([UIScreen mainScreen].bounds.size.height == 480.0)

#define IS_IPhone6  ([UIScreen mainScreen].bounds.size.height == 667.0)
#define IS_IPhone6Plus ([UIScreen mainScreen].bounds.size.height == 736.0)

#import "AvatarImage.h"
#import "DotLineView.h"
#import "NSSortDescriptor+SortValues.h"
#import "UIImage+ReCreate.h"

#define USERNAME_XMPP @"username_XMPP"
#define PASSWORD_XMPP @"123"

#define USERNAME @"username"
#define AUTHENTICATION_CODE @"1234"

#define ALERTVIEW_TITLE @"WhatstheBuzz!"
#define HOSTNAME @"123.176.47.85"
#define DOMAINAME @"shaili"
//#define HOSTNAME @"128.199.136.36"
//#define DOMAINAME @"shaili"
#define AVATAR @"avatar_img"

#define RUTER_SERVICE @"http://123.176.47.85/sosruthere/server.php"
#define FILE_UPLOAD @"http://123.176.47.85/sosruthere/client2.php"
#define GROUP_ICON_SERVICE @"http://123.176.47.85/sosruthere/client4.php"

#define ImageURLString @"http://123.176.47.85/sosruthere/profile_pictures/"
#define BuzzStr @"Buzzing..."
#define KEY_UNREAD @"UNREADCOUNT%@"

#define SOS_SERVICE @"http://123.176.47.85/sosruthere/design/server.php"
#define SOS_CODE @"SOS CODE"
#define SOS_ID @"SOS ID"
#define SOS_URL @"SOS URL"
#define MOBILE_NUM @"mobile number"
#define SOS_CONTACTS @"SOS Contacts"
#define SOS_CONTACTDATA @"contactData"
#define SOS_ACTIVE @"SOSActivation"
#define SOS_EGY_INFO  @"emergencyContactsInfo"

#define SOS_EMAILS @"SOS Emails"
#define SOS_CODE @"SOS CODE"
#define SOS_STATUS @"SOS STATUS"
#define SOS_IM_STATUS @"SOS IM MSG STATUS"
#define DEVICE_TOKEN "DEVICE TOKEN"
#define privateTimer_int 30

#define K_FONT_CHESTERFIELD @"chesterfield"
#define K_FONT_EBRIMA @"ebrima_0"

#define QUICK_MENU_FOR_RECENT @"quickMenuClicked1"
#define QUICK_MENU_FOR_CONTACT @"quickMenuClicked2"

#define ENTER_PRIVATE_PWD @"Enter Private Password"
#define SET_PRIVATE_PWD @"Set Private Password"
#define CONFRM_PRIVATE_PWD @"Confirm Private Password"
#define IS_PRIVATE_FIRST @"isFirstTime"

#define SECURITY_QUESTION @"MASTER QUESTION"
#define SECURITY_ANSWER @"MASTER ANSWER"

#define MASTER_PIN @"Master_Pin"



#define DEVICE_IS_IPHONE_5 DEVICE_SCREEN_HAS_LENGTH([UIScreen mainScreen].bounds, 568.f)
#define DEVICE_IS_IPHONE_6 DEVICE_SCREEN_HAS_LENGTH([UIScreen mainScreen].bounds, 667.f)

#define DEVICE_IS_IPHONE_6_PLUS DEVICE_SCREEN_HAS_LENGTH([UIScreen mainScreen].bounds, 736.f)
#define DEVICE_SCREEN_HAS_LENGTH(_frame, length) ( fabsf( MAX(CGRectGetHeight(_frame), CGRectGetWidth(_frame)) - length) < FLT_EPSILON )




#define SMS_SENDERID @"MOBUZZ"
#define SMS_API_KEY @"A13ffa774b9bd9c9319a3029227f9f8e7"
#define SMS_SERVICE @"alerts.spearcommunication.in"

#define TURN_BUBBLE_MENU @"turnBubbleMenu"
#define ONLINE_OFFLINE_VISIBILITY @"showOnlineOfflineOnChatScreen"


#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)



#define IsEqualCall  1
#define IsEqualChat  2
#define IsEqualProfile 3

BOOL fromSOS;
BOOL fromSecurityQuest;
BOOL fromPassCodeVC;

#endif
