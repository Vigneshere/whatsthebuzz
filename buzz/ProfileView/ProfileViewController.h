//
//  ProfileViewController.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 18/02/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XMPPFramework.h"

@interface ProfileViewController : UIViewController
@property(nonatomic,retain)XMPPUserCoreDataStorageObject *userDetails;
@property(nonatomic)int tablecount;



@end
