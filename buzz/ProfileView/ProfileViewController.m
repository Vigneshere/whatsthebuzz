//
//  ProfileViewController.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 18/02/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "ProfileViewController.h"
#import "XMPPUser.h"
#import "AppDelegate.h"
#import "chatViewController.h"
#import "XMPPLastActivity.h"
#import "GroupChatViewController.h"
#import "sharedfileView.h"
#import "DetailGalleryViewController.h"
#import "UIImageEffects.h"
#define Xposition 10
#define Width self.view.frame.size.width-20

@interface ProfileViewController ()<UITableViewDataSource,UITableViewDelegate,SharedFiledelegate>
{
    UIButton *backButton;
    //--_Status View
    UIView *statusView;
    UIView *presenceView;
    UILabel *presenceLabel,*lastLoginLabel,*statusLabel;
    UIImageView  *blurView;
    //-->Shared Files View
    UILabel *sharedfileLabel;
    sharedfileView *shareFileView;
    
    
    //-->Group View
    UILabel *groupsLabel;
    UITableView *groupTableView;
    NSMutableArray *groupArray;
    
    //-->CallOrMessageView
    UILabel *numberLabel,*msgLabel;
    BOOL carouselCheck;
    AvatarImage *avatarImg;
    DotLineView *lineView;
    XMPPResourceCoreDataStorageObject *useResource;
    XMPPLastActivity *lastActivity;
}
@property(nonatomic,strong)UILabel *profileTitleLabel;
@property(nonatomic,strong)UIImageView *profileImageView;
@property(nonatomic,strong)GroupChatViewController *groupChatView;

@end

@implementation ProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Appdelegate Values
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (AppDelegate *)appDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (XMPPStream *)xmppStream
{
    return [[self appDelegate] xmppStream];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark View Life Cycle
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    if(IS_IPHONE_4)
    {
        self.view.backgroundColor = BACKGROUNDIMAGE_4;
        
    }
    else
    {
        self.view.backgroundColor = BACKGROUNDIMAGE;
        
    }
    
    self.navigationItem.hidesBackButton = YES;
    _profileTitleLabel = [UILabel new];
    self.profileImageView = [UIImageView new];
    
    //-->Status View
    statusView = [UIView new];
    presenceView = [UIView new];
    presenceLabel = [UILabel new];
    lastLoginLabel = [UILabel new];
    lastLoginLabel.adjustsFontSizeToFitWidth = YES;
    statusLabel = [UILabel new];
    
    //-->Shared Files View
    sharedfileLabel = [UILabel new];
    //->GroupView
    groupsLabel = [UILabel new];
    groupTableView = [UITableView new];
    groupArray = [NSMutableArray new];
    
    //-->CallOrMessageView
    numberLabel = [UILabel new];
    msgLabel = [UILabel new];
    avatarImg = [AvatarImage new];
    lineView = [DotLineView new];
    lastActivity = [[XMPPLastActivity alloc]initWithDispatchQueue:dispatch_get_main_queue()];
    [lastActivity activate:[self xmppStream]];
    [lastActivity addDelegate:self delegateQueue:dispatch_get_main_queue()];
    [[self xmppStream] autoAddDelegate:self
                         delegateQueue:dispatch_get_main_queue()
                      toModulesOfClass:[XMPPvCardAvatarModule class]];
    
    
    ////Edit the Presence
    //    XMPPPresence *presence = [XMPPPresence presence];
    //    NSXMLElement *show = [NSXMLElement elementWithName:@"show" stringValue:@"Available"];
    //    NSXMLElement *status = [NSXMLElement elementWithName:@"status" stringValue:@"CutomeStatus"];
    //    [presence addChild:show];
    //    [presence addChild:status];
    //    [[self xmppStream] sendElement:presence];
    
    
    //  float blurHeight = (self.view.frame.size.height/2)-64;
    blurView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width,(self.view.frame.size.height * 37)/100)];
    blurView.backgroundColor = [UIColor clearColor];
    blurView.image = [UIImageEffects imageByApplyingLightEffectToProfileImage:[avatarImg profileImage:_userDetails]];
    [self.view addSubview:blurView];
    
    self.profileImageView.frame = CGRectMake((self.view.frame.size.width/2)-50, (blurView.frame.size.height/2)-50, 100, 100);
    self.profileImageView.layer.cornerRadius = 50;
    self.profileImageView.layer.borderWidth = 2.0;
    self.profileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.profileImageView.clipsToBounds = YES;
    self.profileImageView.image = [self imageWithImage:[avatarImg profileImage:_userDetails] scaledToSize:CGSizeMake(100, 100) ];
    [blurView addSubview:self.profileImageView];
    
    
    [self createStatusView];
    [self createSharedFilesView];
    [self createGroupView];
    [self getRoomDetails];
    
    [self CreateCallOrMessageView];
    [[self xmppStream] addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    UIImage *backButtonImage = [UIImage imageNamed:@"back_btn"];
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 0, backButtonImage.size.width, backButtonImage.size.height);
    [backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.navigationController.navigationBar addSubview:backButton];
    self.navigationController.navigationBarHidden = NO;
    [[self appDelegate].statusBarView setHidden:NO];


    
    _profileTitleLabel.frame = CGRectMake((self.view.frame.size.width/2) - 100,10, 200, 30);
    _profileTitleLabel.backgroundColor = [UIColor clearColor];
    _profileTitleLabel.textColor = [UIColor blackColor];
    _profileTitleLabel.font = [UIFont fontWithName:FontString size:20];
    _profileTitleLabel.text = _userDetails.displayName;
    _profileTitleLabel.textAlignment = NSTextAlignmentCenter;
    [self.navigationController.navigationBar addSubview:_profileTitleLabel];
    

    
    
    if ([_userDetails isOnline])
    {
        for (useResource in _userDetails.resources)
        {
            if ([useResource.jid.bareJID isEqualToJID:_userDetails.jid.bareJID])
            {
                if([useResource.type isEqualToString:@"available"])
                {
                    presenceView.backgroundColor = [UIColor greenColor];
                    presenceLabel.text = @"Available";
                    lastLoginLabel.text = @"";
                    statusLabel.text = useResource.status;

                }
                else if([useResource.type isEqualToString:@"unavailable"])
                {
                    presenceView.backgroundColor = [UIColor redColor];
                    presenceLabel.text = @"Unavailable";
                    [lastActivity sendLastActivityQueryToJID:_userDetails.jid];
                }
            }
        }
    }
    else{
        presenceView.backgroundColor = [UIColor redColor];
        presenceLabel.text = @"Unavailable";
        [lastActivity sendLastActivityQueryToJID:_userDetails.jid];
        
    }
    
        [shareFileView getData:self.userDetails.jid];

    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [_profileTitleLabel removeFromSuperview];
    [backButton removeFromSuperview];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Create Status View
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)createStatusView
{
    statusView.frame = CGRectMake(0,blurView.frame.size.height - 50, self.view.frame.size.width, 50);
    statusView.backgroundColor = [UIColor clearColor];
    [blurView addSubview:statusView];
    
    
    presenceLabel.frame = CGRectMake((self.view.frame.size.width/2) - 50,10,100,20);
    presenceLabel.backgroundColor = [UIColor clearColor];
    presenceLabel.textColor = [UIColor whiteColor];
    presenceLabel.textAlignment = NSTextAlignmentCenter;
    presenceLabel.font = [UIFont fontWithName:FontString size:13];
    [statusView addSubview:presenceLabel];
    
    presenceView.frame = CGRectMake(presenceLabel.frame.origin.x - 10, 10, 10, 10);
    presenceView.layer.cornerRadius = 5;
    [statusView addSubview:presenceView];
    
    
    lastLoginLabel.frame = CGRectMake((self.view.frame.size.width/2) - 85, presenceLabel.frame.origin.y+presenceLabel.frame.size.height, 170,15);
    lastLoginLabel.backgroundColor = [UIColor clearColor];
    lastLoginLabel.textColor = [UIColor whiteColor];
    lastLoginLabel.textAlignment = NSTextAlignmentCenter;
    lastLoginLabel.font = [UIFont fontWithName:FontString size:11];
    [statusView addSubview:lastLoginLabel];
    
    statusLabel.frame = CGRectMake((self.view.frame.size.width/2) - 100, presenceLabel.frame.origin.y+presenceLabel.frame.size.height,200,20);
    statusLabel.backgroundColor = [UIColor clearColor];
    statusLabel.textColor = [UIColor whiteColor];
    statusLabel.textAlignment = NSTextAlignmentCenter;
    statusLabel.font = [UIFont fontWithName:FontString size:11];
    [statusView addSubview:statusLabel];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Create Shared Files View
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)createSharedFilesView
{
    UIView *SharedFileslineView = [UIView new];
    SharedFileslineView.frame = CGRectMake(0, blurView.frame.origin.y+blurView.frame.size.height+15, self.view.frame.size.width, 1);
    SharedFileslineView.backgroundColor = RGBA(119, 186, 240, 0.2);
    [self.view addSubview:SharedFileslineView];
    
    sharedfileLabel = [UILabel new];
    sharedfileLabel.frame = CGRectMake((self.view.frame.size.width/2)-50, blurView.frame.origin.y+blurView.frame.size.height+5,100,25);
    sharedfileLabel.backgroundColor = RGBA(119, 186, 240,0.5);
    sharedfileLabel.textColor = [UIColor whiteColor];
    sharedfileLabel.textAlignment = NSTextAlignmentCenter;
    sharedfileLabel.layer.cornerRadius = 12;
    sharedfileLabel.clipsToBounds = YES;
    sharedfileLabel.font = [UIFont fontWithName:FontString size:14];
    sharedfileLabel.text = @"Shared Files";
    [self.view addSubview:sharedfileLabel];
    
    shareFileView = [[sharedfileView alloc]initWithFrame:CGRectMake(Xposition, sharedfileLabel.frame.origin.y+sharedfileLabel.frame.size.height+10, Width, 50)];
    shareFileView.backgroundColor = [UIColor clearColor];
    shareFileView.delegate = self;
    [self.view addSubview:shareFileView];
    
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Create Group View
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)createGroupView
{
    
    UIView *groupMemberlineView = [UIView new];
    groupMemberlineView.frame = CGRectMake(0, shareFileView.frame.origin.y+shareFileView.frame.size.height+22, self.view.frame.size.width, 1);
    groupMemberlineView.backgroundColor = RGBA(119, 186, 240,0.2);
    [self.view addSubview:groupMemberlineView];
    
    
    groupsLabel.frame = CGRectMake((self.view.frame.size.width/2)-50, shareFileView.frame.origin.y+shareFileView.frame.size.height+10,100,25);
    groupsLabel.backgroundColor = RGBA(119, 186, 240, 0.5);
    groupsLabel.textColor = [UIColor whiteColor];
    groupsLabel.textAlignment = NSTextAlignmentCenter;
    groupsLabel.font = [UIFont fontWithName:FontString size:14];
    groupsLabel.layer.cornerRadius = 12;
    groupsLabel.clipsToBounds = YES;
    groupsLabel.text = @"Groups";
    [self.view addSubview:groupsLabel];
    
    
    groupTableView.delegate = self;
    groupTableView.dataSource = self;
    groupTableView.backgroundColor = [UIColor clearColor];
    groupTableView.separatorColor = [UIColor clearColor];
    groupTableView.rowHeight = 50;
    groupTableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:groupTableView];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Create Call Or MessageView
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)CreateCallOrMessageView
{
    UIImage *callBtnImage = [UIImage imageNamed:@"phone"];
    UIButton *callButton = [UIButton buttonWithType:UIButtonTypeCustom];
    callButton.frame = CGRectMake(10,groupTableView.frame.origin.y+groupTableView.frame.size.height+10,callBtnImage.size.width,callBtnImage.size.height);
    [callButton addTarget:self action:@selector(callAction) forControlEvents:UIControlEventTouchUpInside];
    [callButton setBackgroundImage:callBtnImage forState:UIControlStateNormal];
    [self.view addSubview:callButton];
    
    numberLabel.frame = CGRectMake(callButton.frame.origin.x+callButton.frame.size.width+5, groupTableView.frame.origin.y+groupTableView.frame.size.height+5,130,30);
    numberLabel.backgroundColor = [UIColor clearColor];
    numberLabel.textColor = [UIColor blackColor];
    numberLabel.textAlignment = NSTextAlignmentLeft;
    numberLabel.font = [UIFont fontWithName:FontString size:13];
    numberLabel.text =_userDetails.jid.user;
    numberLabel.userInteractionEnabled = YES;
    [self.view addSubview:numberLabel];
    
    UITapGestureRecognizer *numbertap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callAction)];
    [numbertap setNumberOfTapsRequired:1];
    [numbertap setNumberOfTouchesRequired:1];
    [numberLabel addGestureRecognizer:numbertap];
    
    UIImage *messageBtnImage = [UIImage imageNamed:@"sms"];
    UIButton *messageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    messageButton.frame = CGRectMake(self.view.frame.size.width - (messageBtnImage.size.width+10+100),callButton.frame.origin.y, messageBtnImage.size.width,messageBtnImage.size.height);
    [messageButton addTarget:self action:@selector(messageAction) forControlEvents:UIControlEventTouchUpInside];
    [messageButton setBackgroundImage:messageBtnImage forState:UIControlStateNormal];
    [self.view addSubview:messageButton];
    
    msgLabel.frame = CGRectMake(messageButton.frame.origin.x+messageButton.frame.size.width+5,groupTableView.frame.origin.y+groupTableView.frame.size.height + 5,100,30);
    msgLabel.backgroundColor = [UIColor clearColor];
    msgLabel.textColor = [UIColor blackColor];
    msgLabel.textAlignment = NSTextAlignmentLeft;
    msgLabel.font = [UIFont fontWithName:FontString size:13];
    msgLabel.text = @"Send Message";
    msgLabel.userInteractionEnabled = YES;
    [self.view addSubview:msgLabel];
    
    UITapGestureRecognizer *messagtap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(messageAction)];
    [messagtap setNumberOfTapsRequired:1];
    [messagtap setNumberOfTouchesRequired:1];
    [msgLabel addGestureRecognizer:messagtap];
    
    
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Action For Back Button
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)backbuttonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)didSelectShareFile:(NSIndexPath *)indexpath Array:(NSMutableArray *)ary
{
    DetailGalleryViewController *detailsView = [DetailGalleryViewController new];
    detailsView.detailArray = [ary mutableCopy];
    detailsView.indexpath = indexpath;
    [self.navigationController pushViewController:detailsView animated:NO];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Changing the Image Size
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - TableView delegate and DataSource Methods
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    return groupArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        // Initialization code
        UIImageView *groupImageView = [UIImageView new];
        groupImageView.frame = CGRectMake(0, cell.frame.origin.y+5, 40, 40);
        groupImageView.layer.cornerRadius = 20;
        groupImageView.clipsToBounds = YES;
        groupImageView.tag = 1;
        [cell.contentView addSubview:groupImageView];
        
        UILabel *groupNameLbl = [UILabel new];
        groupNameLbl.frame = CGRectMake(groupImageView.frame.origin.x+groupImageView.frame.size.width+20, cell.frame.origin.y+5, 220, 30);
        groupNameLbl.backgroundColor = [UIColor clearColor];
        groupNameLbl.textColor =[UIColor blackColor];
        groupNameLbl.textAlignment = NSTextAlignmentLeft;
        groupNameLbl.font = [UIFont fontWithName:FontString size:17];
        groupNameLbl.tag = 2;
        [cell.contentView addSubview:groupNameLbl];
    }
    UIImageView *gImageView = (UIImageView *)[cell viewWithTag:1];
    UILabel *groupNameLabel = (UILabel *)[cell viewWithTag:2];
    
    XMPPJID *jid = [XMPPJID jidWithString:groupArray[indexPath.row][@"roomjid"]];
    gImageView.image =  [avatarImg groupProfileimage:jid];

    groupNameLabel.text = groupArray[indexPath.row][@"subjectName"];
    return cell;
}

-(void)getRoomDetails
{
    [groupArray removeAllObjects];
    XMPPRoomCoreDataStorage *storage = [XMPPRoomCoreDataStorage sharedInstance];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"realJIDStr == %@", self.userDetails.jidStr];
    NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:storage.occupantEntityName
                                                         inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    request.predicate = predicate;
    [request setEntity:entityDescription];
    [request setResultType:NSDictionaryResultType];
    request.propertiesToFetch = [NSArray arrayWithObject:[[entityDescription propertiesByName] objectForKey:@"jidStr"]];
    request.returnsDistinctResults = YES;
    
    NSError *error;
    NSArray *messages_arc = [moc executeFetchRequest:request error:&error];
    for (NSDictionary *details in messages_arc)
    {
        
        XMPPJID *jid = [XMPPJID jidWithString:details[@"jidStr"]];
        XMPPRoomCoreDataStorage *storage = [XMPPRoomCoreDataStorage sharedInstance];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"roomJIDStr == %@", jid.bare];
        NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
        NSEntityDescription *entityDescription = [NSEntityDescription entityForName:storage.messageDetailsEntityName
                                                             inManagedObjectContext:moc];
        NSFetchRequest *request = [[NSFetchRequest alloc]init];
        request.predicate = predicate;
        [request setEntity:entityDescription];
        [request setFetchLimit:1];
        NSArray *roomSubjectArray = [moc executeFetchRequest:request error:&error];
        if (roomSubjectArray.count == 1) {
            XMPPRoomMessageDetails *SubDetails  = (XMPPRoomMessageDetails *)roomSubjectArray[0];
            
            NSMutableDictionary *roomDetailsDic = [NSMutableDictionary new];
            
            [roomDetailsDic setObject:jid.bareJID forKey:@"roomjidStr"];
            [roomDetailsDic setObject:[NSString stringWithFormat:@"%@",jid]forKey:@"roomjid"];
            [roomDetailsDic setObject:SubDetails.subject forKey:@"subjectName"];
            [groupArray addObject:roomDetailsDic];
        }
        
    }
    [groupTableView reloadData];
    int i = 3;
    if (IS_IPHONE_5) {
        i = 4;
        groupTableView.frame = CGRectMake(Xposition, groupsLabel.frame.origin.y+groupsLabel.frame.size.height, Width, 150);
    }
    else
        groupTableView.frame = CGRectMake(Xposition, groupsLabel.frame.origin.y+groupsLabel.frame.size.height, Width, 100);
    
    if (groupArray.count < i) {
        int tableViewheight = groupTableView.rowHeight*groupArray.count;
        if(tableViewheight != 0)
            groupTableView.frame = CGRectMake(Xposition, groupsLabel.frame.origin.y+groupsLabel.frame.size.height, Width, tableViewheight);
        else
            groupTableView.frame = CGRectMake(Xposition, groupsLabel.frame.origin.y+groupsLabel.frame.size.height, Width, 50);
    }
    
    UIView *groupMemberlineView = [UIView new];
    groupMemberlineView.frame = CGRectMake(0, groupTableView.frame.origin.y+groupTableView.frame.size.height, self.view.frame.size.width, 1);
    groupMemberlineView.backgroundColor = RGBA(119, 186, 240,0.2);
    [self.view addSubview:groupMemberlineView];
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.groupChatView)
        self.groupChatView = [GroupChatViewController new];
    self.groupChatView.user = (XMPPJID *)groupArray[indexPath.row][@"roomjidStr"];
    [self.navigationController pushViewController: self.groupChatView animated:YES];
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Call Button Clicked
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)callAction
{
    
    NSString *JidStr =  _userDetails.jid.user;
    
    //  NSLog(@"Calling %@",JidStr);
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",JidStr]]];
    
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Message Button Clicked
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)messageAction
{
    [self.navigationController popViewControllerAnimated:YES];
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Converting Date to String
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(NSString *)dateToString:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd, yyyy HH:mma"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    return stringFromDate;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - xmppStream delegate
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence
{
    if([[[presence from] bareJID] isEqualToJID:_userDetails.jid])
    {
        NSString *presenceType = [presence type]; // online/offline
        if ([presenceType isEqualToString:@"available"]) {
            presenceView.backgroundColor = [UIColor greenColor];
            presenceLabel.text = @"Available";
            lastLoginLabel.text = @"";
            statusLabel.text = useResource.status;
        }
        else if ([presenceType isEqualToString:@"unavailable"]) {
            presenceView.backgroundColor = [UIColor redColor];
            presenceLabel.text = @"Unavailable";
           // if (presence.status != nil)
                //presenceLabel.text = [presence status];
        }
        
        [lastActivity sendLastActivityQueryToJID:_userDetails.jid];
        
    }
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Last Activity Delegate
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)xmppLastActivity:(XMPPLastActivity *)sender didReceiveResponse:(XMPPIQ *)response
{
    NSXMLElement *query = [response elementForName:@"query" xmlns:@"jabber:iq:last"];
    if ([[query attributeForName:@"seconds"] stringValue] != nil)
    {
        NSTimeInterval timeinterval = [[[query attributeForName:@"seconds"] stringValue] doubleValue];
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
        [offsetComponents setSecond:-timeinterval]; // note that I'm setting it to -1
        NSDate *lastDate = [gregorian dateByAddingComponents:offsetComponents toDate:[NSDate date] options:0];
        lastLoginLabel.text = [NSString stringWithFormat:@"Last Login: %@",[self dateToString:lastDate]];
        statusLabel.text =@"";
        
        gregorian = nil;
        offsetComponents = nil;
        lastDate = nil;
    }
}
- (void)xmppLastActivity:(XMPPLastActivity *)sender didNotReceiveResponse:(NSString *)queryID dueToTimeout:(NSTimeInterval)timeout
{
    //      lastLoginLabel.text = [NSString stringWithFormat:@"Last seen:%@",[self dateToString:[NSDate dateWithTimeIntervalSinceNow:0]]];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark XMPPvCardAvatarDelegate
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)xmppvCardAvatarModule:(XMPPvCardAvatarModule *)vCardTempModule didReceivePhoto:(UIImage *)photo forJID:(XMPPJID *)jid
{
    if ([_userDetails.jid.bare isEqualToString:jid.bare])
        self.profileImageView.image = photo;
}
-(void)dealloc
{
    backButton = nil;
    statusView =nil;
    presenceView = nil;
    presenceLabel = nil;
    lastLoginLabel = nil;
    statusLabel = nil;
    sharedfileLabel = nil;
    groupsLabel  = nil;
    groupTableView = nil;
    groupArray = nil;
    numberLabel = nil;
    msgLabel = nil;
    avatarImg = nil;
    lineView = nil;
    useResource = nil;
    lastActivity = nil;
    self.profileImageView = nil;
    self.profileTitleLabel  = nil;
    self.userDetails  = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    // NSLog(@"RECEIVED MEMORY WARNINGS IN ProfileViewController");
}
@end
