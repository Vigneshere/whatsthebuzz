//
//  tvCellForReview.m
//  SSLC
//
//  Created by Mac 009 on 14/11/14.
//
//

#import "tvCellForReview.h"

@implementation tvCellForReview



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        CGRect contentViewFrame = self.contentView.frame;
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        
        contentViewFrame.size.width = screenRect.size.width;
        self.contentView.frame = contentViewFrame;
        self.contentView.backgroundColor = [UIColor clearColor];
        
        mainView=[[UIView alloc]init];
        mainView.frame=CGRectMake(0, 0, self.contentView.frame.size.width, 50);
      //  mainView.backgroundColor=[UIColor clearColor];
        mainView.hidden=NO;
       // [mainView.layer setCornerRadius:5.0f];
        [mainView.layer setMasksToBounds:YES];
        mainView.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.5];
        [self.contentView addSubview:mainView];
        
        
        _categoryLabel =[[UITextField alloc]initWithFrame:CGRectMake(10,3, (self.contentView.frame.size.width/2)-20, 44)];
        _categoryLabel.userInteractionEnabled = NO;
        _categoryLabel.keyboardType = UIKeyboardTypeAlphabet;
        //titleLabel.lineBreakMode=NSLineBreakByWordWrapping;
       // _categoryLabel.numberOfLines=0;
        _categoryLabel.layer.cornerRadius = 5.0;
        _categoryLabel.adjustsFontSizeToFitWidth=YES;
        _categoryLabel.backgroundColor=[UIColor clearColor];
        _categoryLabel.textColor = [UIColor grayColor];
        _categoryLabel.font=[UIFont fontWithName:FontString size:15];
        [mainView addSubview:_categoryLabel];
        
        _valueLabel =[[UITextField alloc]initWithFrame:CGRectMake(_categoryLabel.frame.size.width + _categoryLabel.frame.origin.x +1, 0, (self.contentView.frame.size.width/2)-2, 50)];
        //titleLabel.lineBreakMode=NSLineBreakByWordWrapping;
      //  _valueLabel.numberOfLines=0;
        _valueLabel.layer.cornerRadius = 5.0;
        _valueLabel.textColor = [UIColor darkGrayColor];
        _valueLabel.adjustsFontSizeToFitWidth=YES;
        _valueLabel.backgroundColor=[UIColor clearColor];
        _valueLabel.font=[UIFont fontWithName:FontString size:15];
        [mainView addSubview:_valueLabel];
        
    }
    return self;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}


- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
