//
//  tvCellForReview.h
//  SSLC
//
//  Created by Mac 009 on 14/11/14.
//
//

#import <UIKit/UIKit.h>

@interface tvCellForReview : UITableViewCell<UITextFieldDelegate>
{
    UIView *mainView;
    NSString *userType;

}

@property(nonatomic,retain) UITextField *categoryLabel;
@property(nonatomic,retain) UITextField *valueLabel;


@end
