//
//  profileFormViewController.m
//  Buzz
//
//  Created by shaili_macMini02 on 13/02/14.
//  Copyright (c) 2014 shaili_macMini02. All rights reserved.
//

#import "profileFormViewController.h"
#import "PECropViewController.h"
#import "AFJSONRequestOperation.h"
#import "RXMLElement.h"
#import "ViewController.h"
#import "AvatarImage.h"
#import "Singleton.h"
#import "MSSlidingPanelController.h"
#import "SOSHomeController.h"
#import "UIImage+Resize.h"
#import "AFImageRequestOperation.h"
#import <FacebookSDK/FacebookSDK.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import "AFHTTPClient.h"
#import "AFJSONRequestOperation.h"

#import "FavoriteTableViewController.h"
#import "UIImageEffects.h"
#import "tvCellForReview.h"

#define PROFILE_PIC_RADIUS  50.0f
#define BOX_WIDTH  248
#define NAVIGATION_BAR 100


@interface profileFormViewController()<UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate,PECropViewControllerDelegate,MSSlidingPanelControllerDelegate,FBLoginViewDelegate>
{
    UIScrollView* base_ScrollVw;
    UIButton* profile_Pic;
    UIView* Contact_form_vw;
    
    UITextField* firstName_TxtFld;
    UITextField* lastName_TxtFld;
    UITextField* email_TxtFld;
    UITextField* activeTxtFld;
    
    UIEdgeInsets     _currentEdgeInsets;
    
    NSString *deviceName;
    NSString *countryName;
    
    UIButton* goBtn;
    UIButton* skipBtn;
    Singleton* singleton;
    UILabel *_profileTitleLabel;
    FBLoginView *FB_Btn;
    
    UIButton* backButton;
    BOOL shownCam;
    UIImageView *blurView;
    
    UITableView *profileContentTableView;
    NSArray *profileContentsArray;
    tvCellForReview *cell;
    NSString *firstNameStr,*lastNameStr;
    
}

@end

@implementation profileFormViewController


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Appdelegate Values
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (AppDelegate *)appDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (XMPPStream *)xmppStream
{
    return [[self appDelegate] xmppStream];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.singleton = [Singleton sharedMySingleton];
    
    
    if(IS_IPHONE_4)
    {
        self.view.backgroundColor = BACKGROUNDIMAGE_4;
        
    }
    else
    {
        self.view.backgroundColor = BACKGROUNDIMAGE;
        
    }
    
    //  self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBarHidden = YES;
    
    
    base_ScrollVw = [[UIScrollView alloc]init];
    _profileTitleLabel = [UILabel new];
    profile_Pic = [[UIButton alloc]init];
    FB_Btn = [[FBLoginView alloc] init];
    goBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    skipBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    Contact_form_vw = [[UIView alloc]init];
    
    firstName_TxtFld = [[UITextField alloc]init];
    lastName_TxtFld = [[UITextField alloc]init];
    email_TxtFld = [[UITextField alloc]init];
    activeTxtFld = [UITextField new];
    singleton = [Singleton sharedMySingleton];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    
    profileContentsArray = [[NSArray alloc]initWithObjects:@"First Name:",@"Last Name:",@"Country:", nil];
    
    
    NSLocale *locale = [NSLocale currentLocale];
    NSString *countryCode = [locale objectForKey: NSLocaleCountryCode];
    
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    
    countryName = [usLocale displayNameForKey: NSLocaleCountryCode value: countryCode];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[self appDelegate].statusBarView setHidden:NO];
    
    
    
    
    base_ScrollVw.frame = CGRectMake(0, 0, self.view.frame.size.width,self.view.frame.size.height);
    base_ScrollVw.backgroundColor = [UIColor clearColor];
    base_ScrollVw.contentSize = CGSizeMake(self.view.frame.size.width,self.view.frame.size.height);
    base_ScrollVw.scrollEnabled = YES;
    self.automaticallyAdjustsScrollViewInsets=NO;
    [self.view addSubview:base_ScrollVw];
    
    
    if (!_fromPasswordScreen)
    {
        self.navigationItem.hidesBackButton = NO;
        self.navigationController.navigationBarHidden = NO;
        
    }
    
    if (_fromPasswordScreen)
    {
        deviceName =  [[UIDevice currentDevice] name];
        firstNameStr = deviceName;
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@", firstNameStr] forKey:@"UserFirtName"];
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",@"--"] forKey:@"UserLastName"];

    }
    
    _profileTitleLabel.frame = CGRectMake(0, 10, self.view.frame.size.width, 30);
    _profileTitleLabel.backgroundColor = [UIColor clearColor];
    _profileTitleLabel.textColor = [UIColor grayColor];
    _profileTitleLabel.font = [UIFont fontWithName:FontString size:20];
    _profileTitleLabel.textAlignment = NSTextAlignmentCenter;
    [self.navigationController.navigationBar addSubview:_profileTitleLabel];
    
    if (_fromPasswordScreen)
    {
        blurView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 20 , self.view.frame.size.width,(self.view.frame.size.height * 29.5)/100)];
        
        UIImage *blurImg = [UIImageEffects imageByApplyingLightEffectToProfileImage:[UIImage imageNamed:@"defaultPerson"]];
        blurView.image = blurImg;

    }
    else
    {
    blurView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width,(self.view.frame.size.height * 35)/100)];
    
    }
    
    blurView.backgroundColor = [UIColor clearColor];
    blurView.userInteractionEnabled = YES;
    base_ScrollVw.userInteractionEnabled = YES;
    [base_ScrollVw addSubview:blurView];
    
    profile_Pic.frame = CGRectMake((self.view.frame.size.width/2) - (PROFILE_PIC_RADIUS), (blurView.frame.size.height/2)-(PROFILE_PIC_RADIUS), 2*PROFILE_PIC_RADIUS, 2*PROFILE_PIC_RADIUS);
    profile_Pic.layer.borderWidth = 2.0;
    profile_Pic.layer.borderColor = [UIColor whiteColor].CGColor;
    [profile_Pic setBackgroundImage:[UIImage imageNamed:@"defaultPerson"] forState:UIControlStateNormal];
    profile_Pic.layer.cornerRadius = PROFILE_PIC_RADIUS;
    profile_Pic.clipsToBounds = YES;
    profile_Pic.userInteractionEnabled = YES;
    [profile_Pic addTarget:self action:@selector(PROFILE_BTN_CLICKED) forControlEvents:UIControlEventTouchUpInside];
    [blurView addSubview:profile_Pic];
    
    
    UILabel *statusLbl;
    
    if (IS_IPHONE_4)
    {
        statusLbl = [[UILabel alloc]initWithFrame:CGRectMake(0,blurView.frame.size.height - 30, self.view.frame.size.width, 40)];
        
    }
    else
    {
        statusLbl = [[UILabel alloc]initWithFrame:CGRectMake(0,blurView.frame.size.height - 40, self.view.frame.size.width, 40)];
    }

    
    statusLbl.font = [UIFont fontWithName:FontString size:13];
    statusLbl.textAlignment = NSTextAlignmentCenter;
    statusLbl.adjustsFontSizeToFitWidth = YES;
    statusLbl.textColor = [UIColor whiteColor];
    NSString *statusStr = [[NSUserDefaults standardUserDefaults]valueForKey:@"status"];
    
    if (statusStr.length == 0)
    {
        statusLbl.text = @"Hi! I'm using WhatsTheBuzz";
        
    }
    else
    {
        statusLbl.text = statusStr;
        
    }
    
    [blurView addSubview:statusLbl];
    
    
    profileContentTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, blurView.frame.origin.y + blurView.frame.size.height, self.view.frame.size.width, 150)];
    profileContentTableView.backgroundColor = [UIColor clearColor];
    profileContentTableView.bounces = NO;
    profileContentTableView.delegate = self;
    profileContentTableView.dataSource = self;
    [base_ScrollVw addSubview:profileContentTableView];
    
    
    
    Contact_form_vw.frame = CGRectMake(0,profile_Pic.frame.origin.y + profile_Pic.frame.size.height + 10 , self.view.frame.size.width, (140/4)*2);
    Contact_form_vw.backgroundColor = [UIColor clearColor];
    [base_ScrollVw addSubview:Contact_form_vw];
    
    
    firstName_TxtFld.frame = CGRectMake(0,0, self.view.frame.size.width, Contact_form_vw.frame.size.height/2);
    firstName_TxtFld.backgroundColor = [UIColor clearColor];
    firstName_TxtFld.placeholder =@"First Name";
    firstName_TxtFld.delegate = self;
    firstName_TxtFld.font = [UIFont fontWithName:FontString size:14];
    firstName_TxtFld.textColor = [UIColor whiteColor];
    [firstName_TxtFld setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    firstName_TxtFld.textAlignment = NSTextAlignmentCenter;
    //  [Contact_form_vw addSubview:firstName_TxtFld];
    
    
    //  [self drawLine:CGRectMake(0, 1*(Contact_form_vw.frame.size.height/2), self.view.frame.size.width, 1) :Contact_form_vw :[UIColor whiteColor]];
    
    lastName_TxtFld.frame = CGRectMake(15, 1*(Contact_form_vw.frame.size.height/2), self.view.frame.size.width, Contact_form_vw.frame.size.height/2);
    lastName_TxtFld.delegate = self;
    lastName_TxtFld.backgroundColor = [UIColor clearColor];
    lastName_TxtFld.placeholder =@"Last Name";
    lastName_TxtFld.font = [UIFont fontWithName:FontString size:14];
    lastName_TxtFld.textColor = [UIColor whiteColor];
    lastName_TxtFld.textAlignment = NSTextAlignmentCenter;
    [lastName_TxtFld setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    //  [Contact_form_vw addSubview:lastName_TxtFld];
    
    //    [self drawLine:CGRectMake(0, 1*(Contact_form_vw.frame.size.height/2) + Contact_form_vw.frame.size.height/2, self.view.frame.size.width, 1) :Contact_form_vw :[UIColor whiteColor]];
    
    
    
    
    
    
    
    //[self drawLine:CGRectMake(15, 2*(Contact_form_vw.frame.size.height/3), BOX_WIDTH - 30, 1) :Contact_form_vw :[UIColor grayColor]];
    
    
    email_TxtFld.frame = CGRectMake(15, 2*(Contact_form_vw.frame.size.height/3), BOX_WIDTH - 30, Contact_form_vw.frame.size.height/3);
    email_TxtFld.delegate = self;
    email_TxtFld.backgroundColor = [UIColor clearColor];
    email_TxtFld.placeholder =@"Email";
    email_TxtFld.font = [UIFont fontWithName:FontString size:13];
    email_TxtFld.textColor = [UIColor lightGrayColor];
    [email_TxtFld setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    //[Contact_form_vw addSubview:email_TxtFld];
    
    // goBtn.backgroundColor = RGBA(154, 60, 192, 1.0f) ;
  //  goBtn.backgroundColor = [UIColor orangeColor];
    UIImage *tickImage = [UIImage imageNamed:@"check_C"];

    goBtn.alpha = 0.7;
    [goBtn setBackgroundImage:tickImage forState:UIControlStateNormal];
    goBtn.frame = CGRectMake(((self.view.frame.size.width/2)/2) - 25, profileContentTableView.frame.origin.y + profileContentTableView.frame.size.height + 10, 50, 50);
    goBtn.layer.cornerRadius = 5.0;
    [goBtn addTarget:self action:@selector(GO_BTN_CLICKED) forControlEvents:UIControlEventTouchUpInside];
    goBtn.titleLabel.font = [UIFont fontWithName:FontString size:15];
    goBtn.layer.cornerRadius = 50/2;
    goBtn.clipsToBounds = YES;
    [base_ScrollVw addSubview:goBtn];
    
    UILabel * updateLabel =  [UILabel new];
    updateLabel.text = @"Update";
    updateLabel.textColor = [UIColor grayColor];
    updateLabel.textAlignment = NSTextAlignmentCenter;
    updateLabel.frame = CGRectMake(((self.view.frame.size.width/2)/2) -40, goBtn.frame.origin.y + goBtn.frame.size.height - 10, 80, 40);
    updateLabel.font = [UIFont fontWithName:FontString size:10];
    [base_ScrollVw addSubview:updateLabel];

    
    
    FB_Btn.frame = CGRectMake((self.view.frame.size.width/2)+((self.view.frame.size.width/2)/2) - 25, goBtn.frame.origin.y, 50, 50);
    for (id obj in FB_Btn.subviews)
    {
        if ([obj isKindOfClass:[UIButton class]])
        {
            UIButton * loginButton =  obj;
           UIImage *loginImage = [UIImage imageNamed:@"fb_c"];
  //       [loginButton setImage:loginImage forState:UIControlStateNormal];
             [loginButton setBackgroundImage:loginImage forState:UIControlStateNormal];
            [loginButton setBackgroundImage:nil forState:UIControlStateSelected];
            [loginButton setBackgroundImage:nil forState:UIControlStateHighlighted];
            loginButton.frame = CGRectMake(0, 0, 50,50);

           // [loginButton sizeToFit];
        }
        if ([obj isKindOfClass:[UILabel class]])
        {
            UILabel * loginLabel =  obj;
            loginLabel.text = @"Facebook";
            loginLabel.textColor = [UIColor whiteColor];
            loginLabel.textAlignment = NSTextAlignmentCenter;
            loginLabel.frame = CGRectMake(0, 45, 60, 40);
        }
    }
    
    FB_Btn.delegate = self;
    FB_Btn.layer.cornerRadius = 50/2;
    FB_Btn.clipsToBounds = YES;
    [base_ScrollVw addSubview:FB_Btn];
    
    UILabel * fbLabel =  [UILabel new];
    fbLabel.text = @"Facebook";
    fbLabel.textColor = [UIColor grayColor];
    fbLabel.textAlignment = NSTextAlignmentCenter;
    fbLabel.frame = CGRectMake((self.view.frame.size.width/2)+((self.view.frame.size.width/2)/2) - 40, FB_Btn.frame.origin.y + FB_Btn.frame.size.height - 10, 80, 40);
    fbLabel.font = [UIFont fontWithName:FontString size:10];
    [base_ScrollVw addSubview:fbLabel];

    
    
    if (_fromPasswordScreen)
    {
        
        
        UILabel * orLbl =  [UILabel new];
        orLbl.text = @"OR";
        orLbl.textColor = [UIColor lightGrayColor];
        orLbl.textAlignment = NSTextAlignmentCenter;
        orLbl.font = [UIFont fontWithName:FontString size:10];
        orLbl.frame = CGRectMake( (self.view.frame.size.width/2 - 25),FB_Btn.frame.origin.y + FB_Btn.frame.size.height  + 12,50, 20);
        [base_ScrollVw addSubview:orLbl];
        
        UIView *SharedFileslineView = [UIView new];
        SharedFileslineView.frame = CGRectMake(self.view.frame.size.width/2 - (110 + 30),orLbl.frame.origin.y + 15 ,110, 1);
        SharedFileslineView.backgroundColor = [UIColor lightGrayColor];
        [base_ScrollVw addSubview:SharedFileslineView];

        
        UIView *SharedFileslineView2 = [UIView new];
        SharedFileslineView2.frame = CGRectMake((self.view.frame.size.width/2 + 30),orLbl.frame.origin.y  + 15, 110, 1);
        SharedFileslineView2.backgroundColor = [UIColor lightGrayColor];
        [base_ScrollVw addSubview:SharedFileslineView2];

        
        UIImage *skipImage = [UIImage imageNamed:@"home_c"];
        [skipBtn setBackgroundImage:skipImage forState:UIControlStateNormal];
        skipBtn.backgroundColor = [UIColor  clearColor] ;
     //   [skipBtn setTitle:@"Skip" forState:UIControlStateNormal];
        skipBtn.frame = CGRectMake((self.view.frame.size.width/2) - 25, orLbl.frame.origin.y + orLbl.frame.size.height + 9, 50, 50);
        [skipBtn addTarget:self action:@selector(skipBtnClicked) forControlEvents:UIControlEventTouchUpInside];
        [skipBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        skipBtn.titleLabel.font = [UIFont fontWithName:FontString size:15];
        skipBtn.layer.cornerRadius = 50/2;
        skipBtn.clipsToBounds = YES;
        [base_ScrollVw addSubview:skipBtn];
        
        UILabel *homeLabel =  [UILabel new];
        homeLabel.text = @"Home";
        homeLabel.textColor = [UIColor grayColor];
        homeLabel.textAlignment = NSTextAlignmentCenter;
        homeLabel.frame = CGRectMake((self.view.frame.size.width/2) - 40, skipBtn.frame.origin.y + skipBtn.frame.size.height -10, 80, 40);
        homeLabel.font = [UIFont fontWithName:FontString size:10];
        [base_ScrollVw addSubview:homeLabel];

    }
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    tapGesture.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tapGesture];
    
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGesture:)];
    swipeGesture.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:swipeGesture];
    
    if (_fromPasswordScreen)
    {
     
        if ([singleton Avatar_Img] != nil)
        {
            [profile_Pic setBackgroundImage:[singleton Avatar_Img] forState:UIControlStateNormal];
            UIImage *blurImg = [UIImageEffects imageByApplyingLightEffectToProfileImage:[singleton Avatar_Img]];
            blurView.image = blurImg;
        }

    }
    
    if (shownCam)
    {
        shownCam = !shownCam;
    }
    else
    {
        if ([self.viewControllerString isEqualToString:@"userSetting"]) {
            
            
            firstNameStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserFirtName"];
            lastNameStr= [[NSUserDefaults standardUserDefaults] objectForKey:@"UserLastName"];
            email_TxtFld.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"];
            _profileTitleLabel.text = @"Profile";
            AvatarImage *avatarImage = [AvatarImage new];
            if ([avatarImage myProfileImage] != nil)
            {
                [profile_Pic setBackgroundImage:[avatarImage myProfileImage] forState:UIControlStateNormal];
                
                UIImage *profileImg = [avatarImage myProfileImage];
                UIImage *blurImg = [UIImageEffects imageByApplyingLightEffectToProfileImage:profileImg];
                blurView.image = blurImg;
                
            }
            else
            {
                [profile_Pic setBackgroundImage:[singleton Avatar_Img] forState:UIControlStateNormal];
            }
        }
        else
            _profileTitleLabel.text = @"Create Profile";
    }
    
}

#pragma mark - TableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    return [profileContentsArray count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *Cell=@"CellIdentifier";
    cell =[tableView dequeueReusableCellWithIdentifier:Cell];
    cell = [[tvCellForReview alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Cell];
    cell.editing = YES;
    
    
    if (cell == nil)
    {
        
    }
    
    if (indexPath.row == 0)
    {
        cell.categoryLabel.text = [profileContentsArray objectAtIndex:indexPath.row];
        cell.valueLabel.userInteractionEnabled = YES;
        
        cell.valueLabel.text = firstNameStr;
        
        
    }
    else if (indexPath.row == 1)
    {
        cell.categoryLabel.text = [profileContentsArray objectAtIndex:indexPath.row];
        cell.valueLabel.userInteractionEnabled = YES;
        
        NSString *lastName = lastNameStr;
        if (lastName.length == 0)
        {
            cell.valueLabel.text = @"--";
            
        }
        else
        {
            cell.valueLabel.text = lastName;
            
        }
        
    }
    else
    {
        cell.categoryLabel.text = [profileContentsArray objectAtIndex:indexPath.row];
        cell.valueLabel.userInteractionEnabled = NO;
        cell.valueLabel.text = countryName;
        
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    cell.categoryLabel.userInteractionEnabled = NO;
    // cell.valueLabel.userInteractionEnabled = YES;
    cell.valueLabel.delegate = self;
    
    return cell;
    
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [UIView new];
    return view;
}

-(void)viewDidAppear:(BOOL)animated
{
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [_profileTitleLabel removeFromSuperview];
    [backButton removeFromSuperview];
    
    [firstName_TxtFld resignFirstResponder];
    [lastName_TxtFld resignFirstResponder];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Action For Back Button
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)backbuttonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - FBLoginViewDelegate

- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView
{
    


}

- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user {
    NSLog(@"%@",user);
    
 
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@", user.first_name] forKey:@"UserFirtName"];
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@", user.last_name] forKey:@"UserLastName"];
        
        [profileContentTableView reloadData];
        
        firstNameStr = [NSString stringWithFormat:@"%@", user.first_name];
        lastNameStr = [NSString stringWithFormat:@"%@", user.last_name];
        [self FetchFBImage_FBID:[NSString stringWithFormat:@"%@", user.id]];
        
        
        [self profileUpdate_userName:[NSString stringWithFormat:@"%@", user.first_name] nameStr:[NSString stringWithFormat:@"%@", user.last_name] emailStr:@"xyz@gmail.com"];

        
    


}


- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView
{

}

- (void)loginView:(FBLoginView *)loginView handleError:(NSError *)error {
    // see https://developers.facebook.com/docs/reference/api/errors/ for general guidance on error handling for Facebook API
    // our policy here is to let the login view handle errors, but to log the results
    NSLog(@"FBLoginView encountered an error=%@", error);
}


-(void)FetchFBImage_FBID:(NSString*)IdStr
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=large",IdStr]]];
    
    AFImageRequestOperation *operation;
    operation = [AFImageRequestOperation imageRequestOperationWithRequest:request
                                                     imageProcessingBlock:nil
                                                                  success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image){
                                                                      [profile_Pic setBackgroundImage:image forState:UIControlStateNormal];
                                                                      
                                                                      UIImage *blurImg = [UIImageEffects imageByApplyingLightEffectToProfileImage:image];
                                                                      blurView.image = blurImg;
                                                                      
                                                                      image = [image resizedImage:CGSizeMake(100, 100) interpolationQuality:kCGInterpolationHigh ];
                                                                      singleton.Avatar_Img = image;
                                                                      AvatarImage* avaImg_Obj = [AvatarImage new];
                                                                      [avaImg_Obj updateAvatar:singleton.Avatar_Img];
                                                                      

                                                                  }
                                                                  failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                                                      NSLog(@"%@", [error localizedDescription]);
                                                                  }];
    [operation start];
}

-(void)skipBtnClicked
{
    [self pushToViewController];
}


-(void)GO_BTN_CLICKED
{
    if (firstNameStr.length == 0) {
        [self showAlert:@"Fill first name field"];
    }
    else if (lastNameStr.length == 0)
    {
        [self showAlert:@"Fill last name field"];
    }
    //    else if (lastName_TxtFld.text.length == 0) {
    //        [self showAlert:@"Fill last name field"];
    //    }
    //    else if (email_TxtFld.text.length == 0) {
    //        [self showAlert:@"Fill email field"];
    //    }
    //    else if (![self validateEmailWithString:email_TxtFld.text]) {
    //        [self showAlert:@"Invalid Email"];
    //    }
    else{
        
        [activeTxtFld resignFirstResponder];
        [self profileUpdate_userName:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME] nameStr:[NSString stringWithFormat:@"%@ %@",firstNameStr,lastNameStr] emailStr:@"xyz@gmail.com"];
    }
}

#pragma mark - Registration Service

-(void)profileUpdate_userName :(NSString*)userName nameStr:(NSString*)nameStr emailStr:(NSString*)emailStr
{
    //http://192.168.1.12:9090/plugins/userService/userservice?type=update&secret=deepak&username=9092614899&password=123&name=test1&email=test1@gmail.com
    if (self.singleton.internetConnection) {
        
        [self LoadingWithText:@"Updating profile"];
        NSString *urlStr = [NSString stringWithFormat:@"http://%@:9090/plugins/userService/userservice?type=update&secret=shaili&username=%@&password=123&name=%@&email=%@",HOSTNAME,userName,nameStr,emailStr];
        NSString *properlyEscapedURL = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        properlyEscapedURL = [properlyEscapedURL stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
        
        NSURL *url = [NSURL URLWithString:properlyEscapedURL];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]
                                             initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation
                                                   , id responseObject) {
            
            
            NSString *str = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            
            
            RXMLElement *rxml = [RXMLElement elementFromXMLString:str encoding:NSUTF8StringEncoding];
            
            //   NSLog(@"xml  ::: %@",rxml);
            
            if ([[NSString stringWithFormat:@"%@",rxml] isEqualToString:@"ok"])
            {
                [[NSUserDefaults standardUserDefaults] setObject:firstNameStr forKey:@"UserFirtName"];
                [[NSUserDefaults standardUserDefaults] setObject:lastNameStr forKey:@"UserLastName"];
                [[NSUserDefaults standardUserDefaults] setObject:email_TxtFld.text forKey:@"userEmail"];
                
                
                [profileContentTableView reloadData];

                
                if ([self.viewControllerString isEqualToString:@"userSetting"])
                {
                    if (singleton.Avatar_Img) {
                        
                        AvatarImage* avaImg_Obj = [AvatarImage new];
                        [avaImg_Obj updateAvatar:singleton.Avatar_Img];
                        
                    }
                    [self unLoadView];
                    [self.navigationController popViewControllerAnimated:YES];
                    
                }
                else
                    [self pushToViewController];
                
            }
            [self unLoadView];
            // code
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            // code
            [self unLoadView];
            NSLog(@"Error: %@", error.localizedDescription);
        }
         ];
        [operation start];
        
    }
    
    else
    {
        [self showAlertWithMessage:@"It appears your internet connection is offline."  cancelButtonTile:@"Ok"];
    }
    
    
}



-(void)pushToViewController
{
    //    ViewController *contactView = [[ViewController alloc]init];
    //   [self.navigationController pushViewController:contactView animated:YES];
    
    [firstName_TxtFld resignFirstResponder];
    [lastName_TxtFld resignFirstResponder];
    [self resignFirstResponder];
    UINavigationController *contoller = [[UINavigationController alloc]initWithRootViewController:[ViewController new]];
    [contoller.navigationBar setTranslucent:YES];
    
    
    UINavigationController *contoller2 = [[UINavigationController alloc]initWithRootViewController:[SOSHomeController new]];
    
    [contoller2.navigationBar setTranslucent:YES];
    
    UINavigationController *contoller3 = [[UINavigationController alloc]initWithRootViewController:[FavoriteTableViewController new]];
    
    
    [contoller3.navigationBar setTranslucent:YES];
    
    NSString *ver = [[UIDevice currentDevice] systemVersion];
    int ver_int = [ver intValue];
    
    if (ver_int < 7) {
        [contoller.navigationBar setTintColor:[UIColor clearColor]];
        [contoller2.navigationBar setTintColor:[UIColor clearColor]];
        [contoller3.navigationBar setTintColor:[UIColor clearColor]];
        
    }
    
    else {
        [contoller.navigationBar setBarTintColor:[UIColor clearColor]];
        [contoller2.navigationBar setBarTintColor:[UIColor clearColor]];
        [contoller3.navigationBar setBarTintColor:[UIColor clearColor]];
        
    }
    
    ViewController        *centerViewController;
    SOSHomeController     *leftPanelViewController;
    MSSlidingPanelController    *slidingPanelController;
    
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    centerViewController = [[ViewController alloc] initWithNibName:nil bundle:nil];
    leftPanelViewController = [[SOSHomeController alloc] initWithNibName:nil bundle:nil];
    
    slidingPanelController = [[MSSlidingPanelController alloc] initWithCenterViewController:contoller leftPanelController:contoller2 andRightPanelController:contoller3];
    [slidingPanelController setDelegate:centerViewController];
    
    [[self appDelegate] setWindow:[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]]];
    [[[self appDelegate] window] setRootViewController:slidingPanelController];
    [[[self appDelegate] window] setBackgroundColor:[UIColor clearColor]];
    [[[self appDelegate] window] makeKeyAndVisible];
    
    UIView *statusBarView = [UIView new];
    statusBarView.frame = CGRectMake(0, 0,[[self appDelegate] window].frame.size.width, 20);
    statusBarView.backgroundColor = [UIColor colorWithRed:53/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];  //RGBA(53, 152, 219, 1)
    [[[self appDelegate] window] addSubview:statusBarView];
    
    // Uncomment to assign a custom backgroung image
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"nav-bar"] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    
    UIImage *backButtonImg = [UIImage imageNamed:@"back_btn.png"];
    
    UIImage *backButtonHomeImage = [[UIImage imageNamed:@"back_btn.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, backButtonImg.size.width - 1, 0, 0)];
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:backButtonHomeImage  forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, - backButtonHomeImage.size.height*2) forBarMetrics:UIBarMetricsDefault];
    
    
    
    
    
    
}
-(void)PROFILE_BTN_CLICKED
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:NSLocalizedString(@"Photo Album", nil), nil];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Camera", nil)];
    }
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    actionSheet.cancelButtonIndex = actionSheet.numberOfButtons - 1;
    
    
    [actionSheet showFromToolbar:self.navigationController.toolbar];
    
    
    
}

- (void)showCamera
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        [[self appDelegate].statusBarView setHidden:YES];
        controller.delegate = self;
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        controller.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        [self presentViewController:controller animated:YES completion:NULL];
    }
}

- (void)openPhotoAlbum
{
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.delegate = self;
    controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:controller animated:YES completion:NULL];
    
}
#pragma mark -

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    if ([buttonTitle isEqualToString:NSLocalizedString(@"Photo Album", nil)]) {
        shownCam = YES;
        [self openPhotoAlbum];
        
    } else if ([buttonTitle isEqualToString:NSLocalizedString(@"Camera", nil)]) {
        shownCam = YES;
        [self showCamera];
    }
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    
    
    
    [picker dismissViewControllerAnimated:YES completion:^{
        [self openEditor:image];
    }];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    shownCam = NO;

    [picker dismissViewControllerAnimated:YES completion:NULL];

}


- (void)openEditor:(UIImage*)img
{
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.image = img;
    controller.cropAspectRatio = 1.0;
    
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    
    [self presentViewController:navigationController animated:YES completion:NULL];
}


#pragma mark -

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    UIImage* imag = [UIImage imageWithImage:croppedImage scaledToWidth:100];
    [profile_Pic setBackgroundImage:imag  forState:UIControlStateNormal];
    
    NSData *imageData = UIImageJPEGRepresentation(imag, 0.7);
    //  NSLog(@"%lu",(unsigned long)imageData.length);
    singleton.Avatar_Img = [UIImage imageWithData:imageData];
    
    if ([[self xmppStream] isConnected] )
    {
        AvatarImage* avaImg_Obj = [AvatarImage new];
        [avaImg_Obj updateAvatar:singleton.Avatar_Img];
        
        
    }
    
    
        AvatarImage* avaImg_Obj = [AvatarImage new];
        [avaImg_Obj updateAvatar:singleton.Avatar_Img];

        

    
    // UIImage *profileImg = [UIImage imageWithImage:croppedImage scaledToWidth:self.view.frame.size.width];
    UIImage *blurImg = [UIImageEffects imageByApplyingLightEffectToProfileImage:croppedImage];
    blurView.image = blurImg;
    
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}
-(void)drawLine :(CGRect)rect :(UIView*)onView :(UIColor*)color
{
    //  NSLog(@"%@",NSStringFromCGRect(rect));
    //    UIView* lineVw = [[UIView alloc]init];
    //    lineVw.frame = rect;
    //    lineVw.backgroundColor = color;
    //    [onView addSubview:lineVw];
    //    lineVw = nil;
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    [shapeLayer setBounds:CGRectMake(0, 0, BOX_WIDTH, 140)];
    [shapeLayer setPosition:CGPointMake(BOX_WIDTH/2, 70)];
    [shapeLayer setFillColor:[[UIColor clearColor] CGColor]];
    [shapeLayer setStrokeColor:[[UIColor lightGrayColor] CGColor]];
    [shapeLayer setLineWidth:1.0f];
    [shapeLayer setLineJoin:kCALineJoinRound];
    [shapeLayer setLineDashPattern:
     [NSArray arrayWithObjects:[NSNumber numberWithInt:2],
      [NSNumber numberWithInt:2],nil]];
    
    // Setup the path
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL,rect.origin.x, rect.origin.y);
    CGPathAddLineToPoint(path, NULL, rect.origin.x + rect.size.width,rect.origin.y);
    [shapeLayer setPath:path];
    CGPathRelease(path);
    
    [[onView layer] addSublayer:shapeLayer];
}

-(void)handleSwipeGesture:(UISwipeGestureRecognizer *) sender
{
    
    [firstName_TxtFld resignFirstResponder];
    [lastName_TxtFld resignFirstResponder];
    [activeTxtFld resignFirstResponder];
    [base_ScrollVw setContentOffset:CGPointMake(0, 0) animated:YES];
    
}

-(void)handleTapGesture:(UITapGestureRecognizer *) sender
{
    [activeTxtFld resignFirstResponder];
    [base_ScrollVw setContentOffset:CGPointMake(0, 0) animated:YES];
}

#pragma mark - textField
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    activeTxtFld = textField;
    
    return YES;
}


- (void) textFieldDidBeginEditing:(UITextField *)textField {
    
    if (textField == email_TxtFld) {
        [textField setKeyboardType:UIKeyboardTypeEmailAddress];
        [textField reloadInputViews];
        
    }
    
    else
    {
        
        CGPoint buttonPosition = [textField convertPoint:CGPointZero toView:profileContentTableView];
        NSIndexPath *indexPath = [profileContentTableView indexPathForRowAtPoint:buttonPosition];
        NSLog(@"%ld",(long)indexPath.row);
        
        cell = (tvCellForReview *)[profileContentTableView cellForRowAtIndexPath:indexPath];
        // cell.valueLabel.backgroundColor =  RGBA(153, 204, 255, 0.8);
        cell.valueLabel.backgroundColor =  [UIColor whiteColor];
        
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    CGPoint buttonPosition = [textField convertPoint:CGPointZero toView:profileContentTableView];
    NSIndexPath *indexPath = [profileContentTableView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"%ld",(long)indexPath.row);
    [textField resignFirstResponder];
    if (indexPath.row == 0)
    {
        cell = (tvCellForReview *)[profileContentTableView cellForRowAtIndexPath:indexPath];
        firstNameStr = cell.valueLabel.text;
        

        cell.valueLabel.backgroundColor =  [UIColor clearColor];
        
    }
    else
    {
        cell = (tvCellForReview *)[profileContentTableView cellForRowAtIndexPath:indexPath];
        lastNameStr = cell.valueLabel.text;

        cell.valueLabel.backgroundColor =  [UIColor clearColor];
        
    }
    
    
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    
    
    return YES;
}
//
//- (void)textFieldDidEndEditing:(UITextField *)textField{
//    if (textField == firstName_TxtFld) {
//
//        [lastName_TxtFld becomeFirstResponder];
//    }
//    else if (textField == lastName_TxtFld)
//    {
//        [textField resignFirstResponder];
//        //[email_TxtFld becomeFirstResponder];
//
//    }
//    else{
//
//    }
//
//    [textField resignFirstResponder];
//}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField == email_TxtFld) {
        
        NSString *resultingString = [textField.text stringByReplacingCharactersInRange: range withString: string];
        NSCharacterSet *whitespaceSet = [NSCharacterSet whitespaceCharacterSet];
        if  ([resultingString rangeOfCharacterFromSet:whitespaceSet].location == NSNotFound)      {
            return YES;
        }  else  {
            return NO;
        }
    }
    
    return YES;
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    // UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    //base_ScrollVw.contentInset = contentInsets;
    //[base_ScrollVw setContentOffset:CGPointMake(0, 0) animated:YES];
    [textField resignFirstResponder];
    return YES;
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    _currentEdgeInsets = base_ScrollVw.contentInset; // store current insets to restore them later
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    base_ScrollVw.contentInset = contentInsets;
    base_ScrollVw.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    
    CGPoint buttonPosition = [activeTxtFld convertPoint:CGPointZero toView:profileContentTableView];
    NSIndexPath *indexPath = [profileContentTableView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"%ld",(long)indexPath.row);
    if (indexPath.row == 0)
    {
        cell = (tvCellForReview *)[profileContentTableView cellForRowAtIndexPath:indexPath];
        
        
        
    }
    else
    {
        cell = (tvCellForReview *)[profileContentTableView cellForRowAtIndexPath:indexPath];
        
    }

    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    CGRect tbVwRect = cell.frame;
    tbVwRect.origin.y = cell.frame.origin.y + (blurView.frame.origin.y+blurView.frame.size.height );
    if (!CGRectContainsPoint(aRect, tbVwRect.origin) )
    {
        tbVwRect.origin.y = cell.frame.origin.y - (blurView.frame.origin.y+blurView.frame.size.height );

        CGPoint scrollPoint = CGPointMake(0.0, base_ScrollVw.frame.size.height - (kbSize.height - (tbVwRect.size.height + tbVwRect.origin.y )));
        [base_ScrollVw setContentOffset:scrollPoint animated:YES];
    }
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    base_ScrollVw.contentInset = contentInsets;
    base_ScrollVw.scrollIndicatorInsets = contentInsets;
    [base_ScrollVw setContentOffset:CGPointMake(0, 0) animated:YES];
}





-(void)showAlert :(NSString*)str
{
    UIAlertView*  alert = [[UIAlertView alloc] initWithTitle:ALERTVIEW_TITLE
                                                     message:str
                                                    delegate:self
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
    
    [alert show];
    
}
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

#pragma mark ALERT MESSAGE
-(void)showAlertWithMessage:(NSString *)message cancelButtonTile:(NSString *)cancelTitle {
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ALERTVIEW_TITLE message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    alert = nil;
    
}
-(void)LoadingWithText:(NSString*)String
{
    [DejalBezelActivityView activityViewForView:self.view withLabel:String width:120];
}

-(void)unLoadView
{
    [DejalBezelActivityView removeViewAnimated:YES];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
