//
//  profileFormViewController.h
//  Buzz
//
//  Created by shaili_macMini02 on 13/02/14.
//  Copyright (c) 2014 shaili_macMini02. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DejalActivityView.h"

@interface profileFormViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
@property(nonatomic,retain)NSString *viewControllerString;
@property (strong, nonatomic)Singleton *singleton;
@property(nonatomic)BOOL fromPasswordScreen;
@property(nonatomic)BOOL isFbLoginDone;


@end
