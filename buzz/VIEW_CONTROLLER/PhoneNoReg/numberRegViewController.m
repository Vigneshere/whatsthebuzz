//
//  numberRegViewController.m
//  Buzz
//
//  Created by shaili_macMini02 on 13/02/14.
//  Copyright (c) 2014 shaili_macMini02. All rights reserved.
//

#import "numberRegViewController.h"
#import "passcodeViewController.h"
#import "RUTerViewController.h"
#import "AFJSONRequestOperation.h"
#import "RXMLElement.h"
#import "HMDiallingCode.h"
#import "CountryPicker.h"


#define LOGO_IMG_RADIUS  54
#define BOX_WIDTH  218
#define BOX_HEIGHT 40


@interface numberRegViewController ()<UITextFieldDelegate,HMDiallingCodeDelegate,CountryPickerDelegate>
{
    UIScrollView* base_ScrollVw;
    UIImageView* logoImgVw;
    UIView* numberfldView;
    UIButton* countryCode_Lbl;
    UIButton* goBtn;
    UITextField* numberFld;
    CountryPicker *countryPick;
    
    UIPageControl *pageControl;
    UIImageView *image0;
    UIImageView *image1;
    UIImageView *image2;
    UIImageView *image3;
    NSMutableArray *annimateImages;
    UILabel *appLabel;
    UILabel *appDes;
    
    UILabel *shareLabel;
    UILabel *shareLabel1;
    UILabel *shareDes;
    
    UIImageView *appIcon;
    
    UILabel *discoverLabel;
    UILabel *discoverLabel1;
    UILabel *discoverDes;
    
    UILabel *planLabel;
    UILabel *planLabel1;
    UILabel *planDes;
    UIButton *proceedBtn;
    UIView *bgView;
    
}
@property (strong, nonatomic) HMDiallingCode *diallingCode;

@end

@implementation numberRegViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Life Cycle
//-(void)loadView
//{
//   // self.view = [[UIView alloc]initWithFrame:[[UIScreen mainScreen]applicationFrame]];
//}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    fromPassCodeVC = NO;
    
    
    if(IS_IPHONE_4)
    {
        self.view.backgroundColor = BACKGROUNDIMAGE_4;
        
    }
    else
    {
        self.view.backgroundColor = BACKGROUNDIMAGE;
        
    }
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBarHidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushNotificationReceived:) name:@"pushNotification" object:nil];
    
    self.singleton = [Singleton sharedMySingleton];
    base_ScrollVw = [[UIScrollView alloc]init];
    logoImgVw = [[UIImageView alloc]init];
    numberfldView = [[UIView alloc]init];
    countryCode_Lbl =  [UIButton buttonWithType:UIButtonTypeCustom];
    numberFld = [[UITextField alloc]init];
    
    goBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.diallingCode = [[HMDiallingCode alloc] initWithDelegate:self];
    countryPick = [CountryPicker new];
}
-(void)pushNotificationReceived:(NSNotification *)notification
{
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    base_ScrollVw.frame = CGRectMake(0, 0, self.view.frame.size.width,self.view.frame.size.height);
    base_ScrollVw.backgroundColor = [UIColor clearColor];
    base_ScrollVw.contentSize = CGSizeMake(self.view.frame.size.width,self.view.frame.size.height);
    base_ScrollVw.scrollEnabled = NO;
    [self.view addSubview:base_ScrollVw];
    
    logoImgVw.frame = CGRectMake((self.view.frame.size.width/2) - (LOGOIMAGE.size.width/2), 0, LOGOIMAGE.size.width, LOGOIMAGE.size.height);
    logoImgVw.image = LOGOIMAGE;
    [base_ScrollVw addSubview:logoImgVw];
    
    numberfldView.frame = CGRectMake((self.view.frame.size.width - BOX_WIDTH)/2, logoImgVw.frame.origin.y + logoImgVw.frame.size.height + 25, BOX_WIDTH, BOX_HEIGHT);
    numberfldView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    numberfldView.layer.cornerRadius = BOX_HEIGHT/2;
    [base_ScrollVw addSubview:numberfldView];
    
    countryCode_Lbl.frame = CGRectMake(0, 0, 60, numberfldView.frame.size.height);
    countryCode_Lbl.backgroundColor = [UIColor clearColor];
    countryCode_Lbl.titleLabel.font =  [UIFont fontWithName:FontString size:18];
    [countryCode_Lbl addTarget:self action:@selector(countryButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [countryCode_Lbl setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [numberfldView addSubview:countryCode_Lbl];
    
    CALayer *borderlayer = [CALayer layer];
    borderlayer.frame = CGRectMake(countryCode_Lbl.frame.size.width-1,countryCode_Lbl.frame.origin.y+5 ,1, countryCode_Lbl.frame.size.height-10);
    borderlayer.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.7].CGColor;
    [countryCode_Lbl.layer addSublayer:borderlayer];
    
    numberFld .frame = CGRectMake(countryCode_Lbl.frame.size.width, 0, BOX_WIDTH - (countryCode_Lbl.frame.size.width ) , BOX_HEIGHT);
    numberFld.delegate = self;
    numberFld.keyboardType = UIKeyboardTypeNumberPad;
    numberFld.font =  [UIFont fontWithName:FontString size:18];
    numberFld.textColor = [UIColor grayColor];
    numberFld.textAlignment = NSTextAlignmentLeft;
    numberFld.backgroundColor = [UIColor clearColor];
    [numberfldView addSubview:numberFld];
    
    [goBtn setTitle:@"Submit" forState:UIControlStateNormal];
    goBtn.frame = CGRectMake((self.view.frame.size.width - 80)/2 , numberfldView.frame.origin.y + numberfldView.frame.size.height + 30, 90, 30);
    [goBtn addTarget:self action:@selector(GO_BTN_CLICKED) forControlEvents:UIControlEventTouchUpInside];
    [goBtn setTitleColor:RGBA(53, 152, 219, 1) forState:UIControlStateNormal];
    goBtn.backgroundColor = [UIColor clearColor];
    goBtn.titleLabel.font = [UIFont fontWithName:FontString size:17];//[UIFont fontWithName:FontString size:14];
    goBtn.layer.cornerRadius = 30/2;
    goBtn.layer.borderColor =  RGBA(53, 152, 219, 1).CGColor;
    goBtn.layer.borderWidth = 1.2f;
    [base_ScrollVw addSubview:goBtn];
    
    
    NSString *labelText = @"WhatstheBuzz is a private mobile network for friends that lets you assign remainders, track location, chat with your loved ones and much more...";
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelText length])];
    
    UILabel *textdetailsLabel = [UILabel new];
    textdetailsLabel.backgroundColor = [UIColor clearColor];
    textdetailsLabel.frame = CGRectMake(10, goBtn.frame.origin.y + goBtn.frame.size.height + 10, self.view.frame.size.width-20, BOX_HEIGHT*3);
    textdetailsLabel.attributedText = attributedString;
    textdetailsLabel.font =  [UIFont fontWithName:FontString size:15];
    textdetailsLabel.textColor = [[UIColor blackColor] colorWithAlphaComponent:0.75];
    textdetailsLabel.numberOfLines = 4;
    [base_ScrollVw addSubview:textdetailsLabel];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    tapGesture.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tapGesture];
    
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGesture:)];
    swipeGesture.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:swipeGesture];
    
    NSLocale *locale = [NSLocale currentLocale];
    NSString *countryCode = [locale objectForKey: NSLocaleCountryCode];
    [self.diallingCode getDiallingCodeForCountry:countryCode];

    
    if (fromPassCodeVC == NO)
    {
     
    bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    bgView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bgView];
        
    self.navigationController.navigationBarHidden = YES;
    
    self.scrollView = [UIScrollView new];
    self.scrollView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    self.scrollView.backgroundColor = [UIColor clearColor];
    [self.scrollView setShowsVerticalScrollIndicator:NO];
    [self.scrollView setShowsHorizontalScrollIndicator:NO];
    self.scrollView.alwaysBounceHorizontal = NO;
    self.scrollView.alwaysBounceVertical = NO;
    self.scrollView.directionalLockEnabled = YES;
    self.scrollView.clipsToBounds = YES;
    self.scrollView.pagingEnabled = YES;
    self.scrollView.delegate = self;
    [bgView addSubview:self.scrollView];
    
    
    image0 = [UIImageView new];
    image0.frame = CGRectMake(0, 0, self.scrollView.frame.size.width, self.view.frame.size.height);
    image0.image = [UIImage imageNamed:@"splash1"];
    image0.backgroundColor = [UIColor clearColor];
    image0.clipsToBounds = YES;
    [self.scrollView addSubview:image0];
    
    image1 = [UIImageView new];
    image1.frame = CGRectMake(image0.frame.size.width+image0.frame.origin.x, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height);
    image1.image = [UIImage imageNamed:@"splash2"];
    image1.backgroundColor = [UIColor clearColor];
    image1.clipsToBounds = YES;
    [self.scrollView addSubview:image1];
    
    image2 = [UIImageView new];
    image2.frame = CGRectMake(image1.frame.size.width+image1.frame.origin.x, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height);
    image2.image = [UIImage imageNamed:@"splash3"];
    image2.backgroundColor = [UIColor clearColor];
    image2.clipsToBounds = YES;
    [self.scrollView addSubview:image2];
    
    image3 = [UIImageView new];
    image3.frame = CGRectMake(image2.frame.size.width+image2.frame.origin.x, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height);
    image3.image = [UIImage imageNamed:@"splash"];
    image3.backgroundColor = [UIColor clearColor];
    image3.clipsToBounds = YES;
    image3.userInteractionEnabled = YES;
    [self.scrollView addSubview:image3];
    
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width*4, self.scrollView.frame.size.height-40);
    
    
    appDes = [UILabel new];
    appDes.frame = CGRectMake(15, self.view.frame.size.height - 55, self.view.frame.size.width-30, 40);
    appDes.text = @"App Description";
    appDes.font = [UIFont boldSystemFontOfSize:14];
    appDes.numberOfLines = 2;
    appDes.lineBreakMode = NSLineBreakByWordWrapping;
    appDes.textColor = RGBA(231, 76, 60, 1);
    appDes.textAlignment = NSTextAlignmentCenter;
    appDes.alpha = 1;
    //  [image0 addSubview:appDes];
    
    shareDes = [UILabel new];
    shareDes.frame = CGRectMake(15, self.view.frame.size.height - 85, self.view.frame.size.width-30, 40);
    shareDes.text = @"App Description";
    shareDes.font = [UIFont boldSystemFontOfSize:15];
    shareDes.numberOfLines = 2;
    shareDes.lineBreakMode = NSLineBreakByWordWrapping;
    shareDes.textColor = RGBA(231, 76, 60, 1);
    shareDes.textAlignment = NSTextAlignmentCenter;
    //  [image1 addSubview:shareDes];
    
    discoverDes = [UILabel new];
    discoverDes.frame = CGRectMake(15, self.view.frame.size.height - 85, self.view.frame.size.width-30, 40);
    discoverDes.text = @"App Description";
    discoverDes.font = [UIFont boldSystemFontOfSize:15];
    discoverDes.numberOfLines = 2;
    discoverDes.lineBreakMode = NSLineBreakByWordWrapping;
    discoverDes.textColor = RGBA(231, 76, 60, 1);
    discoverDes.textAlignment = NSTextAlignmentCenter;
    //  [image2 addSubview:discoverDes];
    
    pageControl = [UIPageControl new];
    pageControl.frame = CGRectMake((self.view.frame.size.width-60)/2, self.view.frame.size.height-60, 60,20);
    pageControl.numberOfPages = 4;
    pageControl.currentPage = 0;
    pageControl.userInteractionEnabled = NO;
    pageControl.backgroundColor = [UIColor clearColor];
    pageControl.pageIndicatorTintColor = [UIColor whiteColor];
    pageControl.currentPageIndicatorTintColor = RGBA(231, 76, 60, 1);
    pageControl.alpha = 1;
    [self.view addSubview:pageControl];
    
    planDes = [UILabel new];
    planDes.frame = CGRectMake(15, self.view.frame.size.height - 100, self.view.frame.size.width-30, 40);
    planDes.text = @"AppDescription";
    planDes.font = [UIFont boldSystemFontOfSize:15];
    planDes.numberOfLines = 2;
    planDes.lineBreakMode = NSLineBreakByWordWrapping;
    planDes.textColor = RGBA(231, 76, 60, 1);
    planDes.textAlignment = NSTextAlignmentCenter;
    // [image3 addSubview:planDes];
    
    proceedBtn  = [UIButton buttonWithType:UIButtonTypeCustom];
    proceedBtn.frame = CGRectMake((self.view.frame.size.width/2)-75, pageControl.frame.origin.y+pageControl.frame.size.height, 150, 35);
    [proceedBtn setTitle:@"GOT IT!" forState:UIControlStateNormal];
    [proceedBtn setBackgroundColor:[UIColor orangeColor]];
    proceedBtn.alpha = 0.9;
    proceedBtn.layer.cornerRadius = 3.0;
    proceedBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    proceedBtn.titleLabel.font = [UIFont fontWithName:FontString size:13];
    [proceedBtn addTarget:self action:@selector(proceedBtnTapped) forControlEvents:UIControlEventTouchUpInside];
    [image3 addSubview:proceedBtn];
        
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    CGFloat xPos = scrollView.contentOffset.x;
    
    if(xPos == 0){
        pageControl.currentPage = 0;
        pageControl.hidden = NO;
        
    }else if (xPos == self.view.frame.size.width){
        pageControl.currentPage = 1;
        pageControl.hidden = NO;
        
    }else if (xPos == self.view.frame.size.width*2){
        pageControl.currentPage = 2;
        pageControl.hidden = NO;
        
    }else{
        
        pageControl.currentPage = 3;
        pageControl.hidden = NO;
    }
}

- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag {
    if (flag) {
        //    appIcon.image = lastImageInAnimation;
        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            appLabel.alpha = 1;
            appDes.alpha = 1;
            pageControl.alpha = 1;
        } completion:^(BOOL finished) {
            self.view.userInteractionEnabled = YES;
        }];
        
    }
}
-(void)handleSwipeGesture:(UISwipeGestureRecognizer *) sender
{
    [numberFld resignFirstResponder];
}
-(void)handleTapGesture:(UITapGestureRecognizer *) sender
{
    [numberFld resignFirstResponder];
}
-(void)countryButtonAction
{
    [numberFld resignFirstResponder];
    [countryPick removeFromSuperview];
    countryPick.frame = CGRectMake(0, numberfldView.frame.origin.y+numberfldView.frame.size.height+5, 0, 0);
    countryPick.backgroundColor = [UIColor whiteColor];
    countryPick.delegate = self;
    [base_ScrollVw addSubview:countryPick];
}
- (void)countryPicker:(__unused CountryPicker *)picker didSelectCountryWithName:(NSString *)name code:(NSString *)code
{
    [self.diallingCode getDiallingCodeForCountry:code];
}
#pragma mark - HMDiallingCodeDelegate

- (void)didGetDiallingCode:(NSString *)diallingCode forCountry:(NSString *)countryCode {
    
    [countryCode_Lbl setTitle:[NSString stringWithFormat:@"+%@",diallingCode] forState:UIControlStateNormal];
    
    [[NSUserDefaults standardUserDefaults] setObject:countryCode_Lbl.titleLabel.text forKey:@"countryCode"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)didGetCountries:(NSArray *)countries forDiallingCode:(NSString *)diallingCode {
    
}
- (void)failedToGetDiallingCode {
    
    [self.diallingCode getDiallingCodeForCurrentLocation];
    
}

-(void)GO_BTN_CLICKED
{
    if ( numberFld.text.length >=  10 && countryCode_Lbl.titleLabel.text.length != 0)
    {
        
        passcodeViewController* passCodeVwCtrl = [[passcodeViewController alloc]init];
        passCodeVwCtrl.phoneNumberStr = numberFld.text;
        [self.navigationController pushViewController:passCodeVwCtrl animated:YES];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ALERTVIEW_TITLE message:@"Please provide a valid Phone number" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        alert = nil;
    }
}

#pragma mark - Proceed Btn Action

-(void)proceedBtnTapped
{
    
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         
                         bgView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 0);
                     }
                     completion:^(BOOL finished){
                         
                         pageControl.hidden = YES;
                         image0.hidden = YES;
                         image1.hidden = YES;
                         image2.hidden = YES;
                         image3.hidden = YES;
                         bgView.hidden = YES;
                         [self.scrollView removeFromSuperview];
                     }];
    
}

#pragma mark - textField
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (numberfldView.frame.origin.y + textField.frame.size.height > self.view.frame.size.height - 250) {
        
        
        [base_ScrollVw setContentOffset:CGPointMake(0, -(self.view.frame.size.height - 300 -(numberfldView.frame.origin.y + textField.frame.size.height))) animated:YES];
        
    }
    return YES;
}


- (void) textFieldDidBeginEditing:(UITextField *)textField {
    
    [countryPick removeFromSuperview];
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    [base_ScrollVw setContentOffset:CGPointMake(0,0) animated:YES];
    [textField resignFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    //    if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
    //    {
    //        // BasicAlert(@"", @"This field accepts only numeric entries.");
    //
    //        return NO;
    //    }
    //    // verify max length has not been exceeded
    //    NSUInteger newLength = textField.text.length + string.length - range.length;
    //
    //    BOOL withinMaxLengthLimit = (newLength <= 12);
    //
    //    if (!withinMaxLengthLimit)
    //    {
    //        // suppress the max length message only when the user is typing
    //        // easy: pasted data has a length greater than 1; who copy/pastes one character?
    //        if (string.length > 1)
    //        {
    //            // BasicAlert(@"", @"This field accepts a maximum of 4 characters.");
    //        }
    //
    //        return NO;
    //    }
    //
    //
    //
    
    
    NSString *validRegEx =@"^[0-9]*$"; //change this regular expression as your requirement
    NSPredicate *regExPredicate =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", validRegEx];
    BOOL myStringMatchesRegEx = [regExPredicate evaluateWithObject:string];
    if (myStringMatchesRegEx)
    {
        int length = [self getLength:numberFld.text];
        
        if(length == 10)
        {
            if(range.length == 0)
                return NO;
        }
        
        //        if(length == 3)
        //        {
        //            NSString *num = [self formatNumber:numberFld.text];
        //            textField.text = [NSString stringWithFormat:@"(%@) ",num];
        //            if(range.length > 0)
        //                textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
        //        }
        //        else if(length == 6)
        //        {
        //            NSString *num = [self formatNumber:numberFld.text];
        //            textField.text = [NSString stringWithFormat:@"(%@)-%@-",[num  substringToIndex:3],[num substringFromIndex:3]];
        //            if(range.length > 0)
        //                textField.text = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];
        //        }
        
        
        return YES;
    }
    
    return NO;
    
}


-(NSString*)formatNumber:(NSString*)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = [mobileNumber length];
    if(length > 10)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
    }
    return mobileNumber;
}

-(int)getLength:(NSString*)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = [mobileNumber length];
    
    return length;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    [base_ScrollVw setContentOffset:CGPointMake(0, 0) animated:YES];
    [textField resignFirstResponder];
    return YES;
}


#pragma mark ALERT MESSAGE
-(void)showAlertWithMessage:(NSString *)message cancelButtonTile:(NSString *)cancelTitle
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ALERTVIEW_TITLE message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    alert = nil;
}
-(void)LoadingWithText:(NSString*)String
{
    [DejalBezelActivityView activityViewForView:self.view withLabel:String width:120];
}

-(void)unLoadView
{
    [DejalBezelActivityView removeViewAnimated:YES];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
