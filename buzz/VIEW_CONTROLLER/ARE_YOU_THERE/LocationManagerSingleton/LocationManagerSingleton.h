//
//  LocationManagerSingleton.h
//  buzz
//
//  Created by shaili_macMini02 on 28/03/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MessageMethods.h"

#import <MapKit/MapKit.h>
@protocol Delegate_RUTer;
@interface LocationManagerSingleton : NSObject <CLLocationManagerDelegate>
{
    BOOL isUpdated;
    CLLocation* LastUpdateLocation;
    MessageMethods* messageMethods;
}

@property (nonatomic, strong) CLLocationManager* locationManager;
@property (nonatomic, strong) CLLocation* LastUpdateLocation;

@property(nonatomic,weak) id <Delegate_RUTer> delegate_ruter;

+ (LocationManagerSingleton*)sharedSingleton;
-(void)startUpdatingCurrentLocation;

@end

@protocol Delegate_RUTer <NSObject>

-(void)updateChatView;

@end