//
//  LocationManagerSingleton.m
//  buzz
//
//  Created by shaili_macMini02 on 28/03/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "LocationManagerSingleton.h"
#import "AFJSONRequestOperation.h"
#import "RXMLElement.h"
#import "DBEditer.h"
#import "sosContactObject.h"
#import "Header.h"

@implementation LocationManagerSingleton
@synthesize delegate_ruter;
- (id)init {
    self = [super init];
    
    if(self) {
        self.locationManager = [CLLocationManager new];
        
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        {
            [self.locationManager requestWhenInUseAuthorization];
        }
        
        
        //do any more customization to your location manager
    }
    
    return self;
}

-(void)startUpdatingCurrentLocation
{
    [self stopCurrentLocation];
    
    [self.locationManager setDelegate:self];

    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager setDistanceFilter:kCLDistanceFilterNone];
    [self.locationManager setHeadingFilter:kCLHeadingFilterNone];
    [self.locationManager startUpdatingLocation];
}
-(void)stopCurrentLocation
{
    [self.locationManager stopUpdatingLocation];
}

+ (LocationManagerSingleton*)sharedSingleton {
    static LocationManagerSingleton* sharedSingleton;
    if(!sharedSingleton) {
        @synchronized(sharedSingleton) {
            sharedSingleton = [LocationManagerSingleton new];
        }
    }
    
    return sharedSingleton;
}

- (void)setDelegateRUTer:(id<Delegate_RUTer>)val {
    
    self.delegate_ruter = val;
    
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Appdelegate Values
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (AppDelegate *)appDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (XMPPStream *)xmppStream
{
    return [[self appDelegate] xmppStream];
}

-(XMPPMessageArchivingCoreDataStorage *)xmppMessageArchivingCoreDataStorage
{
    
    return [[self appDelegate] xmppMessageArchivingStorage];
}


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    //    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Whats the Buzz" message:[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    //    [alert show];
    
    NSDate *LastDate;
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"LastDate"]) {
        LastDate = [NSDate date];
        NSDate *currentDate = [NSDate date];
        [[NSUserDefaults standardUserDefaults] setObject:[currentDate dateByAddingTimeInterval:-120] forKey:@"LastDate"];
    }
    LastDate = (NSDate *)[[NSUserDefaults standardUserDefaults] objectForKey:@"LastDate"];
    NSDate *nowDate = [NSDate date];
    NSTimeInterval interval = [nowDate timeIntervalSinceDate:LastDate];
    int hours = (int)interval / 3600;             // integer division to get the hours part
    int minutes = (interval - (hours*3600)) / 60; // interval minus hours part (in seconds) divided by 60 yields minutes
    
    
    if ( minutes >= 1)
    {
        [[NSUserDefaults standardUserDefaults] setObject:nowDate forKey:@"LastDate"];
        
        // Get
        
        DBEditer* dbEditer_Obj = [DBEditer new];
        NSMutableArray* ActiveAry = [dbEditer_Obj FetchActive_RUTER_ID];
        
        if ([ActiveAry count] || [[[NSUserDefaults standardUserDefaults] objectForKey:SOS_STATUS] isEqualToString:@"activate"] )
        {
            
            
            if ([ActiveAry count])
            {
                
              //   NSLog(@"[ActiveAry count]  %lu",(unsigned long)[ActiveAry count]);
                
                [self ruthereGPSLocation:ActiveAry Latitude:[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude] Longitude:[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude] userId:[self appDelegate].xmppStream.myJID.bare];
            }
            
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:SOS_STATUS] isEqualToString:@"activate"])
            {
                [self getAddressWebService:newLocation];
            }
            
            
        }
        else
        {
            [self stopCurrentLocation];
        }
        
        
    }
    
    
    
    
    if (UIApplication.sharedApplication.applicationState == UIApplicationStateActive)
    {
        // NSLog(@"App is foreground. New location is %@", newLocation);
        
    }
    else
    {
      //  NSLog(@"App is backgrounded. New location is %@", newLocation);
        
        
    }
    
    
    
    
}


#pragma mark - Web Services ruthereGPSLocation
-(void)ruthereGPSLocation:(NSMutableArray*)ids_Ary Latitude:(NSString*)Latitude_String Longitude:(NSString*)Longitude_String userId:(NSString*)userId_String
{
    
    NSString *soapMessage = [NSString stringWithFormat:
                             @"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                             "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:server\">\n"
                             "<soapenv:Header/>\n"
                             "<soapenv:Body>\n"
                             "<urn:ruthereGPSLocation soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\n"
                             "<GPSLocationInput xsi:type=\"urn:GPSLocationInput\">\n"
                             "<!--You may enter the following 4 items in any order-->\n"
                             "%@"
                             "<Latitude>%@</Latitude>\n"
                             "<Longitude>%@</Longitude>\n"
                             "<userId>%@</userId>\n"
                             "</GPSLocationInput>\n"
                             "</urn:ruthereGPSLocation>\n"
                             "</soapenv:Body>\n"
                             "</soapenv:Envelope>\n",[self CreateXmlStringWithArray:ids_Ary KeyWord:@"Ruthere_id"],Latitude_String,Longitude_String,[userId_String stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"@%@",DOMAINAME] withString:@""]
                             ];
    
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",RUTER_SERVICE]];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    [theRequest addValue:[NSString stringWithFormat:@"%@/ruthereGPSLocation",RUTER_SERVICE] forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]
                                         initWithRequest:theRequest];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation
                                               , id responseObject) {
        
        
        NSString *str = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        
        
        
        RXMLElement *rxml = [RXMLElement elementFromXMLString:str encoding:NSUTF8StringEncoding];
        
        
        NSLog(@"responseString  ::: %@",rxml);

        
        DBEditer* dbEditer_Obj = [DBEditer new];
        
        isUpdated = NO;
        
        
        //XML RESPONSE OF CURRENT R U THERE WHEN IM SENDER
        
        [rxml iterateWithRootXPath:@"//return" usingBlock: ^(RXMLElement *player) {
            RXMLElement *responseString = [player children:@"ReturnCode"];
         
            NSLog(@"responseString  ::: %@",responseString);
            
            
            [rxml iterateElements:[player children:@"RuthereDetails"] usingBlock:^(RXMLElement *RuthereDetailsElement)
             {
                 
                 [rxml iterateElements:[RuthereDetailsElement children:@"as_sender"] usingBlock:^(RXMLElement *as_senderElement)
                  {
                      // NSLog(@"as_senderElement");
                      
                      [rxml iterateElements:[as_senderElement children:@"item"] usingBlock:^(RXMLElement *itemElement)
                       {
                           [rxml iterateElements:[itemElement children:@"item"] usingBlock:^(RXMLElement *itemsElement)
                            {
                                NSString* string = [NSString stringWithFormat:@"Ruthere_id -- >%@ receiver_id -- >%@ Is_Completed -- >%@ Status -- >%@ Sender_Notif -- >%@ Receiver_Notif -- >%@",[itemsElement child:@"Ruthere_id"],[itemsElement child:@"receiver_id"],[itemsElement child:@"Is_Completed"],[itemsElement child:@"Status"],[itemsElement child:@"Sender_Notif"],[itemsElement child:@"Receiver_Notif"]];
                                
                                NSLog(@"as_senderElement %@",string);
                                NSMutableDictionary* RUTer_Obj = [NSMutableDictionary new];
                                [RUTer_Obj setObject:[NSString stringWithFormat:@"%@",[itemsElement child:@"Ruthere_id"]] forKey:@"RUTer_Id"];
                                [RUTer_Obj setObject:[NSString stringWithFormat:@"%@",[itemsElement child:@"receiver_id"]] forKey:@"from_Id"];
                                [RUTer_Obj setObject:[NSString stringWithFormat:@"%@",[itemsElement child:@"Is_Completed"]] forKey:@"is_Completed"];
                                
                                if(([[NSString stringWithFormat:@"%@",[itemsElement child:@"Status"]] integerValue] > 1) )
                                {
                                    if(([[NSString stringWithFormat:@"%@",[itemsElement child:@"Status"]] integerValue] == 2) )
                                    {
                                        [RUTer_Obj setObject:@"3" forKey:@"ruter_status"];
                                        
                                    }
                                    else if(([[NSString stringWithFormat:@"%@",[itemsElement child:@"Status"]] integerValue] == 3) )
                                    {
                                        [RUTer_Obj setObject:@"4" forKey:@"ruter_status"];
                                        
                                    }
                                    else
                                    {
                                        [RUTer_Obj setObject:@"2" forKey:@"ruter_status"];

                                        
                                    }
                                    
                                }
                                else
                                {
                                    [RUTer_Obj setObject:[NSString stringWithFormat:@"%@",[itemsElement child:@"Status"]] forKey:@"ruter_status"];
                                }
                                
                                
                                BOOL updateStatus = [dbEditer_Obj UpdateRuter_Req_RUTerId_response:RUTer_Obj];
                                
                                if (updateStatus) {
                                    isUpdated = YES;
                                    [dbEditer_Obj AnyReflectionOnRUTerStatus:[RUTer_Obj objectForKey:@"RUTer_Id"]];
                                }
                                
                                
                            }];
                           
                           
                       }];
                  }];
                 
                 //XML RESPONSE OF CURRENT R U THERE WHEN IM RECEIVER

                 [rxml iterateElements:[RuthereDetailsElement children:@"as_receiver"] usingBlock:^(RXMLElement *as_receiverElement)
                  {
                      // NSLog(@"as_receiverElement");
                      
                      [rxml iterateElements:[as_receiverElement children:@"item"] usingBlock:^(RXMLElement *itemElement)
                       {
                           [rxml iterateElements:[itemElement children:@"item"] usingBlock:^(RXMLElement *itemsElement)
                            {
                                NSString* string = [NSString stringWithFormat:@"Ruthere_id -- >%@ receiver_id -- >%@ Is_Completed -- >%@ Status -- >%@ Sender_Notif -- >%@ Receiver_Notif -- >%@",[itemsElement child:@"Ruthere_id"],[itemsElement child:@"sender_id"],[itemsElement child:@"Is_Completed"],[itemsElement child:@"Status"],[itemsElement child:@"Sender_Notif"],[itemsElement child:@"Receiver_Notif"]];
                                NSLog(@"as_receiverElement %@",string);
                                
                                NSMutableDictionary* RUTer_Obj = [NSMutableDictionary new];
                                [RUTer_Obj setObject:[NSString stringWithFormat:@"%@",[itemsElement child:@"Ruthere_id"]] forKey:@"RUTer_Id"];
                                [RUTer_Obj setObject:[NSString stringWithFormat:@"%@",[itemsElement child:@"sender_id"]] forKey:@"from_Id"];
                                [RUTer_Obj setObject:[NSString stringWithFormat:@"%@",[itemsElement child:@"Is_Completed"]] forKey:@"is_Completed"];
                                if(([[NSString stringWithFormat:@"%@",[itemsElement child:@"Status"]] integerValue] > 1) )
                                {
                                    if(([[NSString stringWithFormat:@"%@",[itemsElement child:@"Status"]] integerValue] == 2) )
                                    {
                                        [RUTer_Obj setObject:@"3" forKey:@"ruter_status"];
                                        
                                    }
                                    else if(([[NSString stringWithFormat:@"%@",[itemsElement child:@"Status"]] integerValue] == 3) )
                                    {
                                        [RUTer_Obj setObject:@"4" forKey:@"ruter_status"];
                                        
                                    }
                                    else
                                    {
                                        [RUTer_Obj setObject:@"2" forKey:@"ruter_status"];
                                    }
                                    
                                }
                                else
                                {
                                    [RUTer_Obj setObject:[NSString stringWithFormat:@"%@",[itemsElement child:@"Status"]] forKey:@"ruter_status"];
                                }
                                
                                BOOL updateStatus = [dbEditer_Obj UpdateRuter_Req_RUTerId_response:RUTer_Obj];
                                if (updateStatus) {
                                    isUpdated = YES;
                                    [dbEditer_Obj AnyReflectionOnRUTerStatus:[RUTer_Obj objectForKey:@"RUTer_Id"]];
                                }
                                
                            }];
                           
                           
                       }];
                  }];
             }];
            
        }];
        
        
        if (isUpdated)
        {
            if ( [self.delegate_ruter respondsToSelector:@selector(updateChatView)]) {
                [self.delegate_ruter updateChatView];
            }
            
        }
        
        
        
        
        // code
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // code
      //  NSLog(@"Error: %@", error.localizedDescription);
    }
     ];
    [operation start];
    
}

-(NSString*)CreateXmlStringWithArray:(NSMutableArray*)objects KeyWord:(NSString*)KeyWord
{
    NSMutableString* FinalStr = [NSMutableString new];
    
    for (NSString* obj in objects)
    {
        [FinalStr appendString:[NSString stringWithFormat:@"<%@>%@</%@>\n",KeyWord,obj,KeyWord]];
    }
    return FinalStr;
}


#pragma mark - Web Services LAT LONG

-(void)UpdateLatLong :(CLLocation*)location :(NSString*)addressStr :(NSString*)mobileNum :(NSString*)sosId
{
    
    NSString *soapMessage = [NSString stringWithFormat:
                             @"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                             "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:server\">\n"
                             "<soapenv:Header/>\n"
                             "<soapenv:Body>\n"
                             "<urn:userLocation soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\n"
                             "<userLocationInput xsi:type=\"urn:userLocationInput\">\n"
                             "<!--You may enter the following 5 items in any order-->\n"
                             "<sosId xsi:type=\"xsd:int\">%@</sosId>\n"
                             "<userMobileNo xsi:type=\"xsd:string\">%@</userMobileNo>\n"
                             "<latitude xsi:type=\"xsd:float\">%f</latitude>\n"
                             "<longitude xsi:type=\"xsd:float\">%f</longitude>\n"
                             "<address xsi:type=\"xsd:string\">%@</address>\n"
                             "</userLocationInput>\n"
                             "</urn:userLocation>\n"
                             "</soapenv:Body>\n"
                             "</soapenv:Envelope>\n",sosId,mobileNum,location.coordinate.latitude,location.coordinate.longitude,addressStr
                             ];
    
    
    NSURL *url = [NSURL URLWithString:RUTER_SERVICE];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%d", [soapMessage length]];
    
    NSString *userLocation = [NSString stringWithFormat:@"%@/%@",SOS_SERVICE,@"userLocation"];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: userLocation forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]
                                         initWithRequest:theRequest];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation
                                               , id responseObject) {
        
        
        NSString *str = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        
        
        
        
        
        RXMLElement *rxml = [RXMLElement elementFromXMLString:str encoding:NSUTF8StringEncoding];
        
        
        [rxml iterateWithRootXPath:@"//return" usingBlock: ^(RXMLElement *player) {
       //     NSLog(@"Player: %@ %@",[player child:@"ReturnCode"], [player child:@"ResponseMsg"]);
        }];
        
        //NSLog(@"%@",[rxml attribute:@"Response"]);
        
        
        
        // code
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // code
      //  NSLog(@"Error: %@", error.localizedDescription);
    }
     ];
    [operation start];
    
}

-(void)getAddressWebService :(CLLocation*)currentLocation
{
    //http://maps.googleapis.com/maps/api/geocode/json?address=13.07279,80.254898&sensor=true
    
    NSString *urlStr = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?address=%f,%f&sensor=true",currentLocation.coordinate.latitude,currentLocation.coordinate.longitude];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    
    
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: request
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        
                                                        NSDictionary *dict = (NSDictionary *)JSON;
                                                        
                                                        
                                                        
                                                        if ([[dict objectForKey:@"status"] isEqualToString:@"OK"]) {
                                                            
                                                            
                                                            NSString* AddressStr = [NSString stringWithFormat:@"%@ [Accurate to %.0fm]",[[[dict objectForKey:@"results"] objectAtIndex:0] objectForKey:@"formatted_address"],currentLocation.horizontalAccuracy];
                                                            
                                                            
                                                            if (AddressStr.length > 0) {
                                                                [self UpdateLatLong:currentLocation :AddressStr :[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME] :[[NSUserDefaults standardUserDefaults] objectForKey:SOS_ID]];
                                                            }
                                                            else
                                                            {
                                                                [self UpdateLatLong:currentLocation :@"ADDRESS UNKNOWN" :[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME] :[[NSUserDefaults standardUserDefaults] objectForKey:SOS_ID]];
                                                            }
                                                            
                                                            if ( [[[NSUserDefaults standardUserDefaults] objectForKey:SOS_IM_STATUS] isEqualToString:@"false"])
                                                            {
                                                                [[NSUserDefaults standardUserDefaults] setObject:@"true" forKey:SOS_IM_STATUS];
                                                                [self sendImMsg_address:AddressStr latLong:[NSString stringWithFormat:@"%f,%f",currentLocation.coordinate.latitude,currentLocation.coordinate.longitude]];
                                                            }
                                                            
                                                        }
                                                        
                                                        
                                                        
                                                        
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        
                                                        
                                                       // NSLog(@"Error: %@", error.localizedDescription);
                                                        
                                                    }];
    
    
    [operation start];
    
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark DataBase Methods For Recent Chats
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (XMPPUserCoreDataStorageObject *)RecentcontactsFetch:(NSString *)Str
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:[self managedObjectContext_roster]];
    [request setEntity:entity];
    NSString *predicateFrmt = @"jidStr = %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, Str];
    [request setPredicate:predicate];
    NSArray *empArray=[[self managedObjectContext_roster] executeFetchRequest:request error:nil];
    if ([empArray count] == 1)
    {
        for (XMPPUserCoreDataStorageObject *user in  empArray)
        {
            return user;
        }
    }
    return nil;
}

-(NSMutableArray*)fetchUserContactsArrayFromJidAry
{
    NSMutableArray* UserContactAry = [NSMutableArray new];
    
    NSMutableArray * EncodedSelContactsAry = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:SOS_CONTACTDATA]];
    NSMutableArray* selContactsAry = [NSMutableArray new];
    
    for (NSData *personEncodedObject in EncodedSelContactsAry) {
        sosContactObject *personObject = [NSKeyedUnarchiver unarchiveObjectWithData:personEncodedObject];
        [selContactsAry addObject:personObject];
    }
    
    for (sosContactObject * person in selContactsAry)
    {
        NSString*  number = [[person.selectedNumber componentsSeparatedByCharactersInSet:
                              [[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"]
                               invertedSet]]
                             componentsJoinedByString:@""];
        
        
        number = [self checkPhoneNum:number];
        
        if (number)
        {
            if ([[self appDelegate] RecentcontactsFetch:[NSString stringWithFormat:@"%@@%@",number,DOMAINAME]] != nil)
            {
                [UserContactAry addObject:person];
                
            }
        }
        
    }
    return UserContactAry;
}
-(NSString*)checkPhoneNum:(NSString*)number
{
    if (number.length == 10)
    {
        number = [NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"],number];
        return number;
    }
    else if (number.length == 11)
    {
        NSString* subNumber=[number substringToIndex:1];
        if ([subNumber isEqualToString:@"0"])
        {
            NSString *newStr = [number substringWithRange:NSMakeRange(1, [number length]-1)];
            number =  [NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"],newStr];
            return number;
        }
        else
        {
            return nil;
        }
    }
    else
    {
        return number;
    }
}

- (NSManagedObjectContext *)managedObjectContext_roster
{
    return [[[self appDelegate] xmppRosterStorage] mainThreadManagedObjectContext];
}


-(void)sendVirtualMessageForRuthereReached:(NSMutableDictionary *)rutherDic
{
    
}



-(void)sendImMsg_address:(NSString*)address latLong:(NSString*)latLong
{
    NSMutableArray* contactsAry = [NSMutableArray new];
    contactsAry = [self fetchUserContactsArrayFromJidAry];
    
    for (sosContactObject* person in contactsAry) {
        NSString*  number = [[person.selectedNumber componentsSeparatedByCharactersInSet:
                              [[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"]
                               invertedSet]]
                             componentsJoinedByString:@""];
        
        
        number = [self checkPhoneNum:number];
        
        [self SendButtonAction:[NSString stringWithFormat:@"%@@%@",number,DOMAINAME] address:address latLong:latLong username:[NSString stringWithFormat:@"%@ %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserFirtName"],[[NSUserDefaults standardUserDefaults] objectForKey:@"UserLastName"]]];
    }
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Action for sending Message
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)SendButtonAction:(NSString*)userString address:(NSString*)address latLong:(NSString*)latLong username:(NSString*)username
{
    messageMethods = [MessageMethods new];
    [messageMethods textmessage:[NSString stringWithFormat:@"This is Emergency for %@ ......\nAddress : %@ \nMAP Point: https://maps.google.com/maps?daddr=%@ ",username,address,latLong] jid:userString];
}
- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    //handle your heading updates here- I would suggest only handling the nth update, because they
    //come in fast and furious and it takes a lot of processing power to handle all of them
}
@end