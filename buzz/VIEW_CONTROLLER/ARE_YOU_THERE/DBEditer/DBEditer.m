//
//  DBEditer.m
//  buzz
//
//  Created by shaili_macMini02 on 26/03/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "DBEditer.h"
#import "RuterData_ud.h"
#import "RuterStatus_ud.h"
#import "LocationManagerSingleton.h"

#define DEFAULT_RANGE @"1.5"

@implementation DBEditer
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize Delegate_DBE;

#pragma mark - remove DomainName from String

//RUTER STATUS Table
//ruter_Id   UNIQUE ID Shall be Sender id/Receiver id
//ruter_status   (0 —> Request Pending)  (1 —> Loc will Update - Active) (2 —> Reached)  (3 —> Rejected) (4 —> Cancelled)
//iS_Sender     (NO || 0  —> Not Sender)  (YES || 1  —> Sender)
//is_Completed  (1  —> Active)  (2 —> inActive)

//RuTer INFO Table
//latitude     (Latitude String of Opponent)
//longitude    (Longitude String of Opponent)
//updated_time
//range        (Radius Set by me for this Ruter Id)


-(NSString*)removeDomainName_String:(NSString*)string
{
    return [string stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"@%@",DOMAINAME] withString:@""];
}

//- (void)setDelegate_DBE:(id<Delegate_DBEditer>)val {
//    
//    self.Delegate_DBE = val;
//     self.singleton = [Singleton sharedMySingleton];
//    self.singleton.ChatViewUpdate = val;
//}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Appdelegate Values
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (AppDelegate *)appDelegate
{
	return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark ARE YOU THERE CORE DATA
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (XMPPStream *)xmppStream
{
	return [[self appDelegate] xmppStream];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark ARE YOU THERE CORE DATA Update With IM Msg
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(void)UpdateRUTer_MsgDb :(NSXMLElement*)ruder
{
    
    if ([[[ruder attributeForName:@"type"] stringValue] isEqualToString:@"request"]) {
        NSMutableArray* toAry = [[NSMutableArray alloc]init];
        [toAry addObject:[self removeDomainName_String:[[self xmppStream] myJID].bare]];
        
        NSMutableDictionary* Ruter_Dict = [NSMutableDictionary new];
        [Ruter_Dict setObject:[self removeDomainName_String:[[ruder attributeForName:@"from"] stringValue]] forKey:@"from_Id"];
        [Ruter_Dict setObject:toAry forKey:@"to_Id"];
        [Ruter_Dict setObject:[[ruder attributeForName:@"latitude"] stringValue] forKey:@"latitude"];
        [Ruter_Dict setObject:[[ruder attributeForName:@"longitude"] stringValue] forKey:@"longitude"];
        [Ruter_Dict setObject:[[ruder attributeForName:@"ruthere_id"] stringValue] forKey:@"RUTer_Id"];
        [Ruter_Dict setObject:@"0" forKey:@"ruter_status"];
        [Ruter_Dict setObject:DEFAULT_RANGE forKey:@"range"];
        
        BOOL insertStatus = [self InsertRuter_Req_RUTerId:Ruter_Dict];
        if (insertStatus) {
            //Successfully inserted
        }
        
    }
    else  if ([[[ruder attributeForName:@"type"] stringValue] isEqualToString:@"response"])
    {
        NSMutableDictionary* Ruter_Dict = [NSMutableDictionary new];
        [Ruter_Dict setObject:[self removeDomainName_String:[[ruder attributeForName:@"from"] stringValue]] forKey:@"from_Id"];
        [Ruter_Dict setObject:[[ruder attributeForName:@"latitude"] stringValue] forKey:@"latitude"];
        [Ruter_Dict setObject:[[ruder attributeForName:@"longitude"] stringValue] forKey:@"longitude"];
        [Ruter_Dict setObject:[[ruder attributeForName:@"ruthere_id"] stringValue] forKey:@"RUTer_Id"];
        
        if ([[[ruder attributeForName:@"accepted"] stringValue] isEqualToString:@"true"])
        {
            [Ruter_Dict setObject:@"1" forKey:@"ruter_status"];
            [Ruter_Dict setObject:@"1" forKey:@"is_Completed"];
        }
        else
        {
            [Ruter_Dict setObject:@"3" forKey:@"ruter_status"];
            [Ruter_Dict setObject:@"2" forKey:@"is_Completed"];
        }
        
        BOOL updateStatus = [self UpdateRuter_Req_RUTerId_response:Ruter_Dict];
        if (updateStatus) {
            [self AnyReflectionOnRUTerStatus:[[ruder attributeForName:@"ruthere_id"] stringValue]];
            if ([[[ruder attributeForName:@"accepted"] stringValue] isEqualToString:@"true"])
            {
            [self locUpdate];
            }

        }
        
    }
    
    
    else if ([[[ruder attributeForName:@"type"] stringValue] isEqualToString:@"cancelled"])
    {
        NSMutableDictionary* Ruter_Dict = [NSMutableDictionary new];
        [Ruter_Dict setObject:[[ruder attributeForName:@"ruthere_id"] stringValue] forKey:@"RUTer_Id"];
        [Ruter_Dict setObject:@"4" forKey:@"ruter_status"];
        [Ruter_Dict setObject:@"2" forKey:@"is_Completed"];
        [Ruter_Dict setObject:[self removeDomainName_String:[[ruder attributeForName:@"from"] stringValue]] forKey:@"from_Id"];
        
        BOOL updateStatus = [self UpdateRuter_Req_RUTerId_response:Ruter_Dict];
        if (updateStatus) {
            [self AnyReflectionOnRUTerStatus:[[ruder attributeForName:@"ruthere_id"] stringValue]];
        }
    }
    
}

-(void)locUpdate
{
     LocationManagerSingleton* locSingleton;
    locSingleton = [LocationManagerSingleton sharedSingleton];
    [locSingleton startUpdatingCurrentLocation];
    NSDate *currentDate = [NSDate date];
    [[NSUserDefaults standardUserDefaults] setObject:[currentDate dateByAddingTimeInterval:-120] forKey:@"LastDate"];
    
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Insert multiple Rows In CoreData including Sender/Receiver
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(BOOL)InsertRuter_Req_RUTerId:(NSMutableDictionary*)RUTer_Obj
{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // to insert
    
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"RuterStatus_ud"
                                               inManagedObjectContext:context];
    [request setEntity:entity1];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"ruter_Id = \"%@\"",[RUTer_Obj objectForKey:@"RUTer_Id"]]];
    [request setPredicate:pred];
    
    NSArray *empArray=[self.managedObjectContext executeFetchRequest:request error:nil];
    
    BOOL EXIST = NO;
    
    if ([empArray count])
    {
        EXIST = YES;
        
        return NO;
    }
    
    if (!EXIST) {
        
        BOOL isSenderInserted = [self insertRowInCoreData_id:[RUTer_Obj objectForKey:@"from_Id"] is_Completed:@"1" iS_Sender:@"YES" ruter_Id:[RUTer_Obj objectForKey:@"RUTer_Id"] ruter_status:[RUTer_Obj objectForKey:@"ruter_status"] latitude:[RUTer_Obj objectForKey:@"latitude"] longitude:[RUTer_Obj objectForKey:@"longitude"] updated_time:[RUTer_Obj objectForKey:@"updated_time"] range:[RUTer_Obj objectForKey:@"range"]];
        
        if (isSenderInserted) {
            
            
            NSMutableArray* receiversAry = [RUTer_Obj objectForKey:@"to_Id"];
            
            for (NSString* recev_id in receiversAry)
            {
                BOOL  isReceiverInserted = [self insertRowInCoreData_id:recev_id is_Completed:@"1" iS_Sender:@"NO" ruter_Id:[RUTer_Obj objectForKey:@"RUTer_Id"] ruter_status:[RUTer_Obj objectForKey:@"ruter_status"] latitude:@"UNKNOWN" longitude:@"UNKNOWN"  updated_time:[RUTer_Obj objectForKey:@"updated_time"] range:[RUTer_Obj objectForKey:@"range"]];
                if (!isReceiverInserted) {
                    return NO;
                }
            }
        }
        else{
            return NO;
        }
        [self updateChatView];
        return YES;
    }
    else
    {
        return NO;
    }
    
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Insert Row In CoreData
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////


-(BOOL)insertRowInCoreData_id:(NSString*)id_Str is_Completed:(NSString*)is_Completed iS_Sender:(NSString*)iS_Sender ruter_Id:(NSString*)ruter_Id ruter_status:(NSString*)ruter_status latitude:(NSString*)latitude longitude:(NSString*)longitude updated_time:(NSString*)updated_time range:(NSString*)range
{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    RuterData_ud *Ruter_Data_Info = [NSEntityDescription
                                     insertNewObjectForEntityForName:@"RuterData_ud"
                                     inManagedObjectContext:context];
    Ruter_Data_Info.updated_time = updated_time;
     Ruter_Data_Info.range = range;
    Ruter_Data_Info.latitude = latitude;
    Ruter_Data_Info.longitude = longitude;
    
    RuterStatus_ud *Ruter_Data_Status = [NSEntityDescription
                                         insertNewObjectForEntityForName:@"RuterStatus_ud"
                                         inManagedObjectContext:context];
    Ruter_Data_Status.ruter_Id = ruter_Id;
    Ruter_Data_Status.ruter_status = ruter_status;
    Ruter_Data_Status.iD = id_Str;
    Ruter_Data_Status.is_Completed = is_Completed;
    Ruter_Data_Status.iS_Sender = iS_Sender;
    
    Ruter_Data_Status.info = Ruter_Data_Info;
    Ruter_Data_Info.status = Ruter_Data_Status;
    
    NSError *error;
    if (![context save:&error]) {
      //  NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        return NO;
    }
    return YES;
    
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Update Ruter_Req Coordinates
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(BOOL)UpdateRuter_Req_RUTerId_Coor:(NSMutableDictionary*)RUTer_Obj
{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // to insert
    
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"RuterStatus_ud"
                                               inManagedObjectContext:context];
    [request setEntity:entity1];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(ruter_Id = \"%@\") AND (iD = \"%@\")",[RUTer_Obj objectForKey:@"RUTer_Id"],[RUTer_Obj objectForKey:@"from_Id"]]];
    [request setPredicate:pred];
    
    NSArray *empArray=[self.managedObjectContext executeFetchRequest:request error:nil];
    
    
    
    for (RuterStatus_ud *ruter_status in empArray)
    {
        
        if ([RUTer_Obj objectForKey:@"ruter_status"]) {
            ruter_status.ruter_status = [RUTer_Obj objectForKey:@"ruter_status"];
        }
        
        
        
        
        RuterData_ud *Ruter_Data_Info = ruter_status.info;
        
        
        if ([RUTer_Obj objectForKey:@"latitude"]) {
            Ruter_Data_Info.latitude = [RUTer_Obj objectForKey:@"latitude"];
        }
        
        if ([RUTer_Obj objectForKey:@"longitude"]) {
            Ruter_Data_Info.longitude = [RUTer_Obj objectForKey:@"longitude"];
        }
        
        
        NSError *error;
        if (![context save:&error]) {
       //     NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            return NO;
        }
        return YES;
    }
    
    return NO;
    
    
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Update RUTerId response/cancel
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////


-(BOOL)UpdateRuter_Req_RUTerId_response:(NSMutableDictionary*)RUTer_Obj
{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // to insert
    
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"RuterStatus_ud"
                                               inManagedObjectContext:context];
    [request setEntity:entity1];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(ruter_Id = \"%@\") AND (iD = \"%@\")",[RUTer_Obj objectForKey:@"RUTer_Id"],[RUTer_Obj objectForKey:@"from_Id"]]];
    [request setPredicate:pred];
    
    NSArray *empArray=[context executeFetchRequest:request error:nil];
    
    
    
    for (RuterStatus_ud *ruter_status in empArray)
    {
        
        if ([RUTer_Obj objectForKey:@"ruter_status"]) {
            ruter_status.ruter_status = [RUTer_Obj objectForKey:@"ruter_status"];
        }
        if ([RUTer_Obj objectForKey:@"is_Completed"]) {
            ruter_status.is_Completed = [RUTer_Obj objectForKey:@"is_Completed"];
        }
        
        
        
        
        RuterData_ud *Ruter_Data_Info = ruter_status.info;
        
        
        if ([RUTer_Obj objectForKey:@"latitude"]) {
            Ruter_Data_Info.latitude = [RUTer_Obj objectForKey:@"latitude"];
        }
        
        if ([RUTer_Obj objectForKey:@"longitude"]) {
            Ruter_Data_Info.longitude = [RUTer_Obj objectForKey:@"longitude"];
        }
        if ([RUTer_Obj objectForKey:@"range"]) {
            Ruter_Data_Info.range = [RUTer_Obj objectForKey:@"range"];
        }
        
        NSError *error;
        if (![context save:&error]) {
          //  NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            return NO;
        }
        
        
        
        return YES;
    }
    
    return NO;
    
    
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Get Status of _RUTerId  for particular user
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////


-(NSString*)GetStatus_RUTerId:(NSString*)RuTer_Id Sender:(NSString*)sender_Status user_Id:(NSString*)user_Id
{
    NSString* status_Str;
    
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSFetchRequest *SenderStatus_request = [[NSFetchRequest alloc] init];
    NSEntityDescription *Update_entity = [NSEntityDescription entityForName:@"RuterStatus_ud"
                                                     inManagedObjectContext:context];
    [SenderStatus_request setEntity:Update_entity];
    
    
    NSPredicate *Update_pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(ruter_Id = \"%@\") AND (iS_Sender = \"%@\") AND (iD = \"%@\")",RuTer_Id,sender_Status,user_Id]];
    [SenderStatus_request setPredicate:Update_pred];
    
    
    NSArray *Update_Array=[self.managedObjectContext executeFetchRequest:SenderStatus_request error:nil];
    
    for (RuterStatus_ud *ruter_status in Update_Array)
    {
        
        
        status_Str = ruter_status.ruter_status;
        
    }
    
   // NSLog(@"fetch Data RuTerid %@   =  %@",RuTer_Id,[self fetchData_RUTER_ID:RuTer_Id]);
    
    return status_Str;
    
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Get Status of _RUTerId common for group
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(NSMutableDictionary*)GetStatus_RUTerId:(NSString*)RuTer_Id Sender:(NSString*)sender_Status
{
    NSMutableDictionary* returnDict = [NSMutableDictionary new];
    
    NSString* status_Str = @"4";
    NSString* isCompleted = @"3";
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSFetchRequest *SenderStatus_request = [[NSFetchRequest alloc] init];
    NSEntityDescription *Update_entity = [NSEntityDescription entityForName:@"RuterStatus_ud"
                                                     inManagedObjectContext:context];
    [SenderStatus_request setEntity:Update_entity];
    
    
    NSPredicate *Update_pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(ruter_Id = \"%@\") AND (iS_Sender = \"%@\")",RuTer_Id,sender_Status]];
    [SenderStatus_request setPredicate:Update_pred];
    
    
    NSArray *Update_Array=[self.managedObjectContext executeFetchRequest:SenderStatus_request error:nil];
    
    for (RuterStatus_ud *ruter_status in Update_Array)
    {
        
        if ([ruter_status.ruter_status integerValue]  < [status_Str integerValue])
        {
            status_Str = ruter_status.ruter_status;
        }
        
        if ([ruter_status.is_Completed integerValue]  < [isCompleted integerValue])
        {
        
            isCompleted = ruter_status.is_Completed;
        }
        
    }
    
    [returnDict setObject:status_Str forKey:@"status_Str"];
    [returnDict setObject:isCompleted forKey:@"isCompleted"];
    
    return returnDict;
    
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Check for Any Reflection On RUTer Status
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////


-(void)AnyReflectionOnRUTerStatus1:(NSString*)RuTer_Id
{
    
    //senderPart
    
    NSMutableDictionary* statusSender = [self GetStatus_RUTerId:RuTer_Id Sender:@"YES"];
    NSMutableDictionary* statusReceiver = [self GetStatus_RUTerId:RuTer_Id Sender:@"NO"];
    int i = [[statusSender objectForKey:@"status_Str"] intValue];
    NSLog(@"%d",i);
    
    if ( ([[statusSender objectForKey:@"isCompleted"] isEqualToString:@"1"] && ([[statusSender objectForKey:@"status_Str"] isEqualToString:@"2"] || [[statusSender objectForKey:@"status_Str"] isEqualToString:@"3"])) && ([[statusReceiver objectForKey:@"isCompleted"] isEqualToString:@"1"] && ([[statusReceiver objectForKey:@"status_Str"] isEqualToString:@"2"] || [[statusReceiver objectForKey:@"status_Str"] isEqualToString:@"3"])))
    {
        
    }
    else if ([[statusSender objectForKey:@"isCompleted"] isEqualToString:@"1"] && ([[statusSender objectForKey:@"status_Str"] isEqualToString:@"2"] || [[statusSender objectForKey:@"status_Str"] isEqualToString:@"3"]))
    {
        [self updateAllReceverOrSenderStatus:RuTer_Id :statusSender :@"NO"];
    }
    else if ([[statusReceiver objectForKey:@"isCompleted"] isEqualToString:@"1"] && ([[statusReceiver objectForKey:@"status_Str"] isEqualToString:@"2"] || [[statusReceiver objectForKey:@"status_Str"] isEqualToString:@"3"]))
    {
        [self updateAllReceverOrSenderStatus:RuTer_Id :statusReceiver :@"YES"];
    }
    else if ([[statusReceiver objectForKey:@"status_Str"] integerValue] != [[statusSender objectForKey:@"status_Str"] integerValue])
    {
        if ([[statusReceiver objectForKey:@"status_Str"] integerValue] > [[statusSender objectForKey:@"status_Str"] integerValue])
        {
            [self updateAllReceverOrSenderStatus:RuTer_Id :statusReceiver :@"YES"];
        }
        else
        {
            [self updateAllReceverOrSenderStatus:RuTer_Id :statusSender :@"NO"];
        }
    }
    
}

-(void)AnyReflectionOnRUTerStatus:(NSString*)RuTer_Id
{
    
    //senderPart
    
    NSMutableDictionary* statusSender = [self GetStatus_RUTerId:RuTer_Id Sender:@"YES"];
    NSMutableDictionary* statusReceiver = [self GetStatus_RUTerId:RuTer_Id Sender:@"NO"];
    int i = [[statusSender objectForKey:@"status_Str"] intValue];
    NSLog(@" %d",i);
    
    if (( ([[statusSender objectForKey:@"isCompleted"] integerValue] > 1) && ([[statusSender objectForKey:@"status_Str"] integerValue] > 1))  && (([[statusReceiver objectForKey:@"isCompleted"] integerValue] > 1) && ([[statusReceiver objectForKey:@"status_Str"] integerValue] > 1)))
    {
        
    }
    else if ( ([[statusSender objectForKey:@"isCompleted"] integerValue] > 1) && ([[statusSender objectForKey:@"status_Str"] integerValue] > 1))
    {
        [self updateAllReceverOrSenderStatus:RuTer_Id :statusSender :@"NO"];
    }
    else if (([[statusReceiver objectForKey:@"isCompleted"] integerValue] > 1) && ([[statusReceiver objectForKey:@"status_Str"] integerValue] > 1))
    {
        [self updateAllReceverOrSenderStatus:RuTer_Id :statusReceiver :@"YES"];
    }
    else if ([[statusReceiver objectForKey:@"status_Str"] integerValue] != [[statusSender objectForKey:@"status_Str"] integerValue])
    {
        if ([[statusReceiver objectForKey:@"status_Str"] integerValue] > [[statusSender objectForKey:@"status_Str"] integerValue])
        {
            [self updateAllReceverOrSenderStatus:RuTer_Id :statusReceiver :@"YES"];
        }
        else
        {
            [self updateAllReceverOrSenderStatus:RuTer_Id :statusSender :@"NO"];
        }
    }
    [self updateChatView];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark update All Recever Or Sender Status
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////


-(void)updateAllReceverOrSenderStatus:(NSString*)RuTer_Id :(NSMutableDictionary*)RuTer_Status :(NSString*)isSender
{
    
    
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSFetchRequest *SenderStatus_request = [[NSFetchRequest alloc] init];
    NSEntityDescription *Update_entity = [NSEntityDescription entityForName:@"RuterStatus_ud"
                                                     inManagedObjectContext:context];
    [SenderStatus_request setEntity:Update_entity];
    
    
    NSPredicate *Update_pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(ruter_Id = \"%@\") AND (iS_Sender = \"%@\")",RuTer_Id,isSender]];
    [SenderStatus_request setPredicate:Update_pred];
    
    
    NSArray *Update_Array=[self.managedObjectContext executeFetchRequest:SenderStatus_request error:nil];
    
    for (RuterStatus_ud *ruter_status in Update_Array)
    {
        if (![ruter_status.ruter_status isEqualToString:@"2"]) {
            ruter_status.ruter_status= [RuTer_Status objectForKey:@"status_Str"];
        }
        ruter_status.is_Completed= [RuTer_Status objectForKey:@"isCompleted"];
        
        NSError *error;
        if (![context save:&error]) {
        //    NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            
        }
        
    }
    
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark delete  ARE YOU THERE in CORE DATA using _RUTER_ID
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////


-(BOOL)delete_RUTER_ID :(NSString*)ID
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSFetchRequest *deleterequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *delentity = [NSEntityDescription entityForName:@"RuterStatus_ud"
                                                 inManagedObjectContext:context];
    [deleterequest setEntity:delentity];
    
    
    NSPredicate *delpred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"ruter_Id = \"%@\"",ID]];
    [deleterequest setPredicate:delpred];
    
    
    NSArray *delArray=[self.managedObjectContext executeFetchRequest:deleterequest error:nil];
    
    
    for (RuterStatus_ud *ruter_status in delArray)
    {
        
        
        [context deleteObject:ruter_status];
        [context save:nil];
        
        NSError *error;
        
        if (![context save:&error]) {
        //    NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            return NO;
        }
        
    }
    return YES;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark FetchData using RUTER_ID
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////


-(NSMutableDictionary*)fetchData_RUTER_ID :(NSString*)ID
{
    // to fetch and update
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"RuterStatus_ud"
                                               inManagedObjectContext:context];
    [request setEntity:entity1];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"ruter_Id = \"%@\"",ID]];
    [request setPredicate:pred];
    
    NSArray *empArray=[self.managedObjectContext executeFetchRequest:request error:nil];
    
    NSMutableDictionary* RUTer_Dict = [[NSMutableDictionary alloc]init];
    NSMutableArray* RUTerReceiver_Dict = [[NSMutableArray alloc]init];
    [RUTer_Dict setObject:ID forKey:@"RUTer_Id"];
    
    NSString* rangeStr;
    
    for (RuterStatus_ud *Rut_Status in empArray)
    {
        if (([Rut_Status.iD isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME]] && [Rut_Status.iS_Sender isEqualToString:@"YES"]))
        {
            [RUTer_Dict setObject:@"YES" forKey:@"iAmSender"];
            [RUTer_Dict setObject:@"YES" forKey:@"isActivated"];
        }
        if ([Rut_Status.iD isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME]] && [Rut_Status.iS_Sender isEqualToString:@"NO"]  )
        {
            [RUTer_Dict setObject:@"NO" forKey:@"iAmSender"];
            if (![Rut_Status.ruter_status isEqualToString:@"0"])
            {
                [RUTer_Dict setObject:@"YES" forKey:@"isActivated"];

            }
            else
            {
            [RUTer_Dict setObject:@"NO" forKey:@"isActivated"];
            }
        }
        NSMutableDictionary* tempDict = [[NSMutableDictionary alloc]init];
        
        [tempDict setObject:[NSString stringWithFormat:@"%@",Rut_Status.ruter_Id] forKey:@"ruter_Id"];
        [tempDict setObject:[NSString stringWithFormat:@"%@",Rut_Status.ruter_status] forKey:@"ruter_status"];
        [tempDict setObject:[NSString stringWithFormat:@"%@",Rut_Status.iD] forKey:@"id"];
        
        [tempDict setObject:[NSString stringWithFormat:@"%@",Rut_Status.is_Completed] forKey:@"is_Completed"];
        
        
        RuterData_ud *details = Rut_Status.info;
        [tempDict setObject:[NSString stringWithFormat:@"%@",details.longitude] forKey:@"longitude"];
        [tempDict setObject:[NSString stringWithFormat:@"%@",details.latitude] forKey:@"latitude"];
        
        [tempDict setObject:[NSString stringWithFormat:@"%@",details.updated_time] forKey:@"updated_time"];
        
        rangeStr = [NSString stringWithFormat:@"%@",details.range];
        
        if ([Rut_Status.iS_Sender isEqualToString:@"YES"]) {
            [tempDict setObject:[NSString stringWithFormat:@"YES"] forKey:@"iS_Sender"];
            [RUTer_Dict setObject:tempDict forKey:@"Sender"];
        }
        else
        {
            [tempDict setObject:[NSString stringWithFormat:@"NO"] forKey:@"iS_Sender"];
            [RUTerReceiver_Dict addObject:tempDict];
        }
        
    }
    [RUTer_Dict setObject:RUTerReceiver_Dict forKey:@"Receiver"];
    [RUTer_Dict setObject:rangeStr forKey:@"range"];
    return RUTer_Dict;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Fetch Active RUTER_ID
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////


-(NSMutableArray*)FetchActive_RUTER_ID
{
    NSMutableArray* Ids = [NSMutableArray new];
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"RuterStatus_ud"
                                               inManagedObjectContext:context];
    [request setEntity:entity1];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"is_Completed = \"%@\" AND iD = \"%@\"",@"1", [[NSUserDefaults standardUserDefaults] objectForKey:USERNAME]]];
    [request setPredicate:pred];
    
    NSArray *empArray=[self.managedObjectContext executeFetchRequest:request error:nil];
    
   
    
    for (RuterStatus_ud *Rut_Status in empArray)
    {
        if (([Rut_Status.iD isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME]] && [Rut_Status.iS_Sender isEqualToString:@"YES"]) || ([Rut_Status.iD isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME]] && [Rut_Status.iS_Sender isEqualToString:@"NO"]  && ![Rut_Status.ruter_status isEqualToString:@"0"]))
        {
            if (![Ids containsObject:Rut_Status.ruter_Id])
            {
                [Ids addObject:Rut_Status.ruter_Id];
            }
        }
        
    }
  
    
    return Ids;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark DEActive All Active RUTER_ID
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////


-(void)DEActive_RUTER_ID
{
   
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"RuterStatus_ud"
                                               inManagedObjectContext:context];
    [request setEntity:entity1];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"is_Completed = \"%@\" AND iD = \"%@\"",@"1", [[NSUserDefaults standardUserDefaults] objectForKey:USERNAME]]];
    [request setPredicate:pred];
    
    NSArray *empArray=[self.managedObjectContext executeFetchRequest:request error:nil];
    
    
    
    for (RuterStatus_ud *ruter_status in empArray)
    {
        
            ruter_status.ruter_status = @"3";
      
        
            ruter_status.is_Completed = @"2";
        
        NSError *error;
        if (![context save:&error]) {
          //  NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
          
        }
        
        
        
       
    }
}


-(void)updateChatView
{
//    if (self.Delegate_DBE == nil) {
//        self.singleton = [Singleton sharedMySingleton];
//       self.Delegate_DBE = self.singleton.ChatViewUpdate ;
//    }
    if ( [self.Delegate_DBE respondsToSelector:@selector(updateChatView)]) {
        [self.Delegate_DBE updateChatView];
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Core Data saveContext
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        //    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
	// This is a private method.
	//
	// NSManagedObjectContext is NOT thread-safe.
	// Therefore it is VERY VERY BAD to use our private managedObjectContext outside our private storageQueue.
	//
	// You should NOT remove the assert statement below!
	// You should NOT give external classes access to the storageQueue! (Excluding subclasses obviously.)
	//
	// When you want a managedObjectContext of your own (again, excluding subclasses),
	// you can use the mainThreadManagedObjectContext (below),
	// or you should create your own using the public persistentStoreCoordinator.
	//
	// If you even comtemplate ignoring this warning,
	// then you need to go read the documentation for core data,
	// specifically the section entitled "Concurrency with Core Data".
	//
	//
	// Do NOT remove the assert statment above!
	// Read the comments above!
	//
    
   // NSAssert([NSThread isMainThread], @"Context reserved for main thread only");

	
	if (_managedObjectContext)
	{
		return _managedObjectContext;
	}
	
	NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
	if (coordinator)
	{
		
		
		if ([NSManagedObjectContext instancesRespondToSelector:@selector(initWithConcurrencyType:)])
			_managedObjectContext =
            [[NSManagedObjectContext alloc] initWithConcurrencyType:NSConfinementConcurrencyType];
		else
			_managedObjectContext = [[NSManagedObjectContext alloc] init];
		
		_managedObjectContext.persistentStoreCoordinator = coordinator;
		_managedObjectContext.undoManager = nil;
		
		
	}
	
	return _managedObjectContext;
}


// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"RUTER_MODEL" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"RUTER_MODEL.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
    //    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
