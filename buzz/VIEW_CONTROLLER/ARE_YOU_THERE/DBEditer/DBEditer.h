//
//  DBEditer.h
//  buzz
//
//  Created by shaili_macMini02 on 26/03/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Singleton.h"
@protocol Delegate_DBEditer <NSObject>

-(void)updateChatView;

@end

@interface DBEditer : NSObject
{
    
    
}
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property(nonatomic,weak) id <Delegate_DBEditer> Delegate_DBE;

@property (strong, nonatomic) Singleton* singleton;

//pragma mark ARE YOU THERE CORE DATA Update With IM Msg
-(void)UpdateRUTer_MsgDb :(NSXMLElement*)ruder;

//Insert multiple Rows In CoreData including Sender/Receiver
-(BOOL)InsertRuter_Req_RUTerId:(NSMutableDictionary*)RUTer_Obj;

//pragma mark Insert Row In CoreData
-(BOOL)insertRowInCoreData_id:(NSString*)id_Str is_Completed:(NSString*)is_Completed iS_Sender:(NSString*)iS_Sender ruter_Id:(NSString*)ruter_Id ruter_status:(NSString*)ruter_status latitude:(NSString*)latitude longitude:(NSString*)longitude updated_time:(NSString*)updated_time  range:(NSString*)range;

//pragma mark Update Ruter_Req Coordinates
-(BOOL)UpdateRuter_Req_RUTerId_Coor:(NSMutableDictionary*)RUTer_Obj;

//pragma mark Update RUTerId response/cancel
-(BOOL)UpdateRuter_Req_RUTerId_response:(NSMutableDictionary*)RUTer_Obj;

//Get Status of _RUTerId  for particular user
-(NSString*)GetStatus_RUTerId:(NSString*)RuTer_Id Sender:(NSString*)sender_Status user_Id:(NSString*)user_Id;

//Get Status of _RUTerId common for group
-(NSMutableDictionary*)GetStatus_RUTerId:(NSString*)RuTer_Id Sender:(NSString*)sender_Status;

//Check for Any Reflection On RUTer Status
-(void)AnyReflectionOnRUTerStatus:(NSString*)RuTer_Id;

//update All Recever Or Sender Status
-(void)updateAllReceverOrSenderStatus:(NSString*)RuTer_Id :(NSMutableDictionary*)RuTer_Status :(NSString*)isSender;

//delete  ARE YOU THERE in CORE DATA using _RUTER_ID
-(BOOL)delete_RUTER_ID :(NSString*)ID;

//FetchData using RUTER_ID
-(NSMutableDictionary*)fetchData_RUTER_ID :(NSString*)ID;

//Fetch Active RUTER_ID's
-(NSMutableArray*)FetchActive_RUTER_ID;

//#pragma mark DEActive All Active RUTER_ID
-(void)DEActive_RUTER_ID;

@end

