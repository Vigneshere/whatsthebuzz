//
//  RUTerViewController.h
//  Buzz
//
//  Created by shaili_macMini02 on 25/02/14.
//  Copyright (c) 2014 shaili_macMini02. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "DejalActivityView.h"

@interface RUTerViewController : UIViewController
{


}

@property (strong, nonatomic)Singleton *singleton;

@property ( nonatomic, retain) NSString *title_Name;

@property ( nonatomic, retain) NSString *Room_Id;


@property (nonatomic,assign) BOOL iAmSender;

@property(nonatomic,retain)NSMutableDictionary *RUTer_Dict;


@end
