//
//  RUTerViewController.m
//  Buzz
//
//  Created by shaili_macMini02 on 25/02/14.
//  Copyright (c) 2014 shaili_macMini02. All rights reserved.
//

#import "RUTerViewController.h"
//#import "JGDetailScrubber.h"
#import "JFMapAnnotation.h"
#import "UIImage+RoundedCorner.h"
#import "UIImage+Resize.h"
#import "AFHTTPClient.h"
#import "AFJSONRequestOperation.h"
#import "RXMLElement.h"
#import "AvatarImage.h"
#import "DBEditer.h"
#import "LocationManagerSingleton.h"
#import "DejalActivityView.h"
#import "ASValueTrackingSlider.h"
#import "UIColor+HexColors.h"
#import "Header.h"


@interface RUTerViewController ()<MKMapViewDelegate,ASValueTrackingSliderDataSource,ASValueTrackingSliderDelegate,UIAlertViewDelegate,CLLocationManagerDelegate>
{
     MKMapView *mapView;
    MKCircle *circle;
    ASValueTrackingSlider *_scrubber;
    UILabel* notifyLbl;
    UILabel* sliderMaxLbl;
    UILabel* sliderMinLbl;
    UIButton* sendBtn,*backButton;
     UIButton* ActivateBtn;
     UIButton* DeActivateBtn;
    MKUserLocation* userLoc;
    
    float SliderValue;
    CLLocation* friendLocation;
    JFMapAnnotation *FrndLoc;
    JFMapAnnotation *MyLoc;
    
    float min_Value;
    float max_Value;
    
    UIButton* cancelBtn;

    LocationManagerSingleton* locSingleton;
    
    UIImageView* testimg;
    BOOL firstInstance;
    
    UIButton* me_Btn;
    UIButton* all_btn;
    DejalActivityView* djLoder;
    NSTimer* myTimer;
    
    CLLocationManager* locationManager;
    
    UIView* DataVw;
}

@end

@implementation RUTerViewController
@synthesize title_Name;
@synthesize RUTer_Dict,Room_Id;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - remove DomainName from String

-(NSString*)removeDomainName_String:(NSString*)string
{
    return [string stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"@%@",DOMAINAME] withString:@""];
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Appdelegate Values
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (AppDelegate *)appDelegate
{
	return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (XMPPStream *)xmppStream
{
	return [[self appDelegate] xmppStream];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    // self.navigationItem.hidesBackButton = YES;
    
        self.singleton = [Singleton sharedMySingleton];
    
    mapView = [[MKMapView alloc]init];
    
    _scrubber = [[ASValueTrackingSlider alloc] init];
    
    notifyLbl = [[UILabel alloc]init];
    sliderMinLbl = [[UILabel alloc]init];
    sliderMaxLbl = [[UILabel alloc]init];
    
    UIImage *backButtonImage = [UIImage imageNamed:@"back_btn.png"];
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 0, backButtonImage.size.width, backButtonImage.size.height);
    [backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonAction:) forControlEvents:UIControlEventTouchUpInside];

    
    
    
    sendBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    ActivateBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    DeActivateBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    
    cancelBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    
    friendLocation = [[CLLocation alloc]init];
     FrndLoc = [[JFMapAnnotation alloc]init];
    MyLoc = [[JFMapAnnotation alloc]init];
    
   
    testimg = [UIImageView new];
    
    me_Btn = [UIButton buttonWithType:UIButtonTypeSystem];
    all_btn = [UIButton buttonWithType:UIButtonTypeSystem];
    locationManager = [CLLocationManager new];
    
    DataVw = [UIView new];
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self getCurrentLocation];
    
    self.navigationItem.hidesBackButton = YES;

    [self.navigationController.navigationBar addSubview:backButton];

    firstInstance = YES;
    
    mapView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 120);
    mapView.showsUserLocation = YES;
    mapView.delegate = self;
    [mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];

    [self.view addSubview:mapView];
    
    DataVw.frame = CGRectMake(0, self.view.frame.size.height - 120, self.view.frame.size.width, 120);
    [self.view addSubview:DataVw];
    
    
    cancelBtn.frame = CGRectMake(self.navigationController.navigationBar.frame.size.width - 54, self.navigationController.navigationBar.frame.size.height - 30, 54, 20);
    [cancelBtn setTitle:@"CANCEL" forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelBtn_CLICKED) forControlEvents:UIControlEventTouchUpInside];
    [cancelBtn setTintColor:RGBA(53, 152, 219, 1)];
    cancelBtn.titleLabel.font = [UIFont fontWithName:FontString size:12];
  
    me_Btn.frame = CGRectMake(10,self.navigationController.navigationBar.frame.size.height + 24, 60, 30);
    [me_Btn setTitle:@"Locate Me" forState:UIControlStateNormal];
    [me_Btn addTarget:self action:@selector(me_Btn_CLICKED) forControlEvents:UIControlEventTouchUpInside];
    [me_Btn setTintColor:[UIColor whiteColor]];
    me_Btn.backgroundColor = [UIColor colorWithHexString:@"FF2D55"];
    me_Btn.alpha = 0.8;
    me_Btn.titleLabel.font = [UIFont fontWithName:FontString size:10];
    me_Btn.layer.cornerRadius = 5;
    [mapView addSubview:me_Btn];
    
    all_btn.frame = CGRectMake(self.view.frame.size.width - 64, me_Btn.frame.origin.y, 60, 30);
    [all_btn setTitle:@"Locate All" forState:UIControlStateNormal];
    all_btn.alpha = 0.8;
    [all_btn addTarget:self action:@selector(all_btn_CLICKED) forControlEvents:UIControlEventTouchUpInside];
    [all_btn setTintColor:[UIColor whiteColor]];
    all_btn.backgroundColor = [UIColor colorWithHexString:@"FF2D55"];
    all_btn.titleLabel.font = [UIFont fontWithName:FontString size:10];
    all_btn.layer.cornerRadius = 5;
    all_btn.hidden = YES;
    [mapView addSubview:all_btn];

    if (![[RUTer_Dict objectForKey:@"range"] floatValue]) {
         SliderValue = [[RUTer_Dict objectForKey:@"range"] floatValue] ;
    }
    else
    {
    SliderValue = [[RUTer_Dict objectForKey:@"range"] floatValue] ;
    }
    
    if (![[RUTer_Dict objectForKey:@"isActivated"]  isEqualToString:@"YES"] )
    {
        [self createDataView];
        
    }
    else
    {
        mapView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height );
                [self.navigationController.navigationBar addSubview:cancelBtn];
    }
    
    NSArray *status = [[RUTer_Dict valueForKey:@"Receiver"]valueForKey:@"ruter_status"];
    
    NSString *ruthereStatus;
    
    if (status.count != 0)
    {
        ruthereStatus = [status objectAtIndex:0];
        
       if (  ruthereStatus != (id)[NSNull null] )
        {
            if([ruthereStatus isEqualToString:@"2"])
            {
                cancelBtn.hidden = YES;
            }
        }
        

    }
    
    
    
    [self createAnotations];
   

}

-(void)createDataView
{
    
    NSMutableDictionary* senderDict = [RUTer_Dict objectForKey:@"Sender"];
    NSMutableArray* receiverAry = [RUTer_Dict objectForKey:@"Receiver"];

    if ([[RUTer_Dict objectForKey:@"iAmSender"] isEqualToString:@"NO"])
    {
        notifyLbl.text = [NSString stringWithFormat:@"Notify me when %@ is less than %.2f km away from me",[self friendName:[senderDict objectForKey:@"id"]] ,SliderValue];
        
    }
    else if ([[RUTer_Dict objectForKey:@"iAmSender"] isEqualToString:@"YES"] || ([receiverAry count] > 1))
    {
        notifyLbl.text = [NSString stringWithFormat:@"Notify me when group members are less than %.2f km away from me",SliderValue];
    }
    else
    {
        notifyLbl.text = [NSString stringWithFormat:@"Notify me when %@ is less than %@ km away from me",[self friendName:[[receiverAry objectAtIndex:0] objectForKey:@"id"]] ,[NSString stringWithFormat:@"%.2f",SliderValue]];
    }
    notifyLbl.frame = CGRectMake(0, mapView.frame.size.height, mapView.frame.size.width, 40);
    
    notifyLbl.textAlignment = NSTextAlignmentCenter;
    notifyLbl.adjustsFontSizeToFitWidth = YES;
    notifyLbl.font = [UIFont fontWithName:FontString size:10];
    [self.view addSubview:notifyLbl];
    
    
    
    min_Value = 0.0;
    max_Value = 4.5;
   
    
    
    _scrubber.frame = CGRectMake(50,notifyLbl.frame.origin.y + notifyLbl.frame.size.height, 220,30);
   _scrubber.maximumValue = max_Value;
    _scrubber.minimumValue = min_Value;
    _scrubber.value = SliderValue;
    _scrubber.popUpViewCornerRadius = 10.0;
    [_scrubber setMaxFractionDigitsDisplayed:0];
    _scrubber.delegate = self;
    _scrubber.dataSource = self;
    _scrubber.popUpViewColor = RGBA(46, 141, 214, 1);
    _scrubber.font = [UIFont fontWithName:FontString size:18];
    _scrubber.textColor = [UIColor whiteColor];
    [self.view addSubview:_scrubber];

    sliderMinLbl.frame = CGRectMake(0, notifyLbl.frame.origin.y + notifyLbl.frame.size.height, 40, 30);
    sliderMinLbl.text = [NSString stringWithFormat:@"%.02f",min_Value];
    sliderMinLbl.textAlignment = NSTextAlignmentRight;
    sliderMinLbl.textColor = [UIColor grayColor];
    sliderMinLbl.font = [UIFont fontWithName:FontString size:11];
    [self.view addSubview:sliderMinLbl];
    
    sliderMaxLbl.frame = CGRectMake(280, notifyLbl.frame.origin.y + notifyLbl.frame.size.height, 40, 30);
    sliderMaxLbl.text = [NSString stringWithFormat:@"%.02f",max_Value];
    sliderMaxLbl.textColor = [UIColor grayColor];
    sliderMaxLbl.textAlignment = NSTextAlignmentLeft;
    sliderMaxLbl.font = [UIFont fontWithName:FontString size:11];
    [self.view addSubview:sliderMaxLbl];
    
    if (([[RUTer_Dict objectForKey:@"iAmSender"] isEqualToString:@"YES"]) ||   ([RUTer_Dict objectForKey:@"RUTer_Id"] == nil ))
    
     {
         sendBtn.frame = CGRectMake((self.view.frame.size.width - 80)/2, self.view.frame.size.height - 40, 80, 30);
         [sendBtn setTitle:@"Send" forState:UIControlStateNormal];
         [sendBtn setTintColor:[UIColor whiteColor]];
         [sendBtn addTarget:self action:@selector(SEND_BTN_CLICKED) forControlEvents:UIControlEventTouchUpInside];
         sendBtn.backgroundColor = RGBA(46, 141, 214, 1);
         sendBtn.titleLabel.font = [UIFont fontWithName:FontString size:13];
         [self.view addSubview:sendBtn];
     }
    else
    {
    
    
    
    ActivateBtn.frame = CGRectMake(((self.view.frame.size.width/2) - 50)/2, self.view.frame.size.height - 40, 50, 20);
    [ActivateBtn setTitle:@"Activate" forState:UIControlStateNormal];
    [ActivateBtn setTintColor:[UIColor whiteColor]];
    [ActivateBtn addTarget:self action:@selector(ActivateBtn_CLICKED) forControlEvents:UIControlEventTouchUpInside];
    ActivateBtn.backgroundColor = RGBA(46, 141, 214, 1);
    ActivateBtn.titleLabel.font = [UIFont fontWithName:FontString size:11];
    [self.view addSubview:ActivateBtn];
    
    DeActivateBtn.frame = CGRectMake((self.view.frame.size.width/2)+(((self.view.frame.size.width/2) - 50)/2), self.view.frame.size.height - 40, 50, 20);
    [DeActivateBtn setTitle:@"Decline" forState:UIControlStateNormal];
    [DeActivateBtn setTintColor:[UIColor whiteColor]];
    [DeActivateBtn addTarget:self action:@selector(DeActivateBtn_CLICKED) forControlEvents:UIControlEventTouchUpInside];
    DeActivateBtn.backgroundColor = RGBA(46, 141, 214, 1);
    DeActivateBtn.titleLabel.font = [UIFont fontWithName:FontString size:11];
    [self.view addSubview:DeActivateBtn];
    }
 
}



-(void)createAnotations
{
    NSMutableDictionary* senderDict = [RUTer_Dict objectForKey:@"Sender"];
    NSMutableArray* receiverAry = [RUTer_Dict objectForKey:@"Receiver"];
   
    
 
     //annotation for sender
    
    
    if ((([[NSString stringWithFormat:@"%@",[senderDict objectForKey:@"latitude"]] doubleValue] > 0 ) && ([[NSString stringWithFormat:@"%@",[senderDict objectForKey:@"longitude"]] doubleValue] > 0 )) || ([[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME] isEqualToString:[senderDict objectForKey:@"id"]]))
    {
        CLLocationCoordinate2D location;
        location.latitude = [[NSString stringWithFormat:@"%@",[senderDict objectForKey:@"latitude"]] doubleValue];
        location.longitude = [[NSString stringWithFormat:@"%@",[senderDict objectForKey:@"longitude"]] doubleValue];
        
        JFMapAnnotation* sen_Loc = [JFMapAnnotation new];

        if ([[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME] isEqualToString:[senderDict objectForKey:@"id"]])
        {
            
            sen_Loc.coordinate = userLoc.coordinate;
            sen_Loc.title = [NSString stringWithFormat:@"My Location"];
        }
        else
        {
            
            sen_Loc.coordinate = location;
            sen_Loc.title = [NSString stringWithFormat:@"%@'s Location",[self friendName:[senderDict objectForKey:@"id"]]];
        }
        sen_Loc.subtitle = @"";
        
        sen_Loc.IsMe = [senderDict objectForKey:@"id"];
        
        
        
        
        [mapView addAnnotation:sen_Loc];
    }
    
    //annotation for receiver
    BOOL isReceiverExist;
    isReceiverExist = NO;
    
    for (NSMutableDictionary* rec_dict in receiverAry)
    {
        if ((([[NSString stringWithFormat:@"%@",[rec_dict objectForKey:@"latitude"]] doubleValue]) && ([[NSString stringWithFormat:@"%@",[rec_dict objectForKey:@"longitude"]] doubleValue])) || ([[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME] isEqualToString:[rec_dict objectForKey:@"id"]]))
        {
            CLLocationCoordinate2D location;
            location.latitude = [[NSString stringWithFormat:@"%@",[rec_dict objectForKey:@"latitude"]] doubleValue];
            location.longitude = [[NSString stringWithFormat:@"%@",[rec_dict objectForKey:@"longitude"]] doubleValue];
            
            isReceiverExist = YES;

            JFMapAnnotation* rec_Loc = [JFMapAnnotation new];
            
            if (![[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME] isEqualToString:[rec_dict objectForKey:@"id"]])
            {
               

                rec_Loc.coordinate = location;
                rec_Loc.title = [NSString stringWithFormat:@"%@'s Location",[self friendName:[rec_dict objectForKey:@"id"]]];
            }
           else
           {
              
             
           }
            rec_Loc.subtitle = @"";
            
            rec_Loc.IsMe = [rec_dict objectForKey:@"id"];
           
           
            
            
            [mapView addAnnotation:rec_Loc];
        }
       
    }
    if ([receiverAry count] > 1)
    {
        all_btn.hidden = NO;
    }
    
    
    
}
-(void)viewDidAppear:(BOOL)animated
{
   
     self.navigationController.navigationBar.topItem.title = title_Name;
    
       if ([mapView.annotations count] > 1)
    {
//        [self ruthereRequestDetailMethod];
//        myTimer = [NSTimer scheduledTimerWithTimeInterval: 6.0 target: self
//                                                       selector: @selector(ruthereRequestDetailMethod) userInfo: nil repeats: YES];


        
    }
}

-(void)ruthereRequestDetailMethod
{
     [self ruthereRequestDetail:[RUTer_Dict objectForKey:@"RUTer_Id"] User_id:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME]];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [myTimer  invalidate];
    myTimer = nil;
    mapView = nil;
    [locationManager stopUpdatingLocation];
    [cancelBtn removeFromSuperview];
    [backButton removeFromSuperview];

    
}
-(void)viewDidDisappear:(BOOL)animated
{
    
}

-(void)fitAnnotations
{

    [mapView showAnnotations:mapView.annotations animated:YES];
    
    }

-(void)SEND_BTN_CLICKED
{
    if ([self CheckLocPermission])
    {
        if (!(userLoc.coordinate.latitude == 0 && userLoc.coordinate.longitude == 0)) {
            
            
            NSMutableArray* receiverIds = [self getReceiverIds:[RUTer_Dict objectForKey:@"Receiver"]];
            
            if ([receiverIds count] > 0) {
                [self ruthereSender_Range:[NSString stringWithFormat:@"%f",SliderValue*1000] receiver_id:receiverIds sender_id:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME] Latitude:[NSString stringWithFormat:@"%f",userLoc.coordinate.latitude] Longitude:[NSString stringWithFormat:@"%f",userLoc.coordinate.longitude]];
            }
            else
            {
                UIAlertView    *alert = [[UIAlertView alloc] initWithTitle:ALERTVIEW_TITLE
                                                                   message:@"No Receivers Found"
                                                                  delegate:nil
                                                         cancelButtonTitle:@"OK"
                                                         otherButtonTitles:nil];
                [alert show];
            }
            
            
        }
        else
        {
            UIAlertView    *alert = [[UIAlertView alloc] initWithTitle:@"Failed to fetch Location"
                                                               message:@"Please try again"
                                                              delegate:nil
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil];
            [alert show];
        }
        
    }
    
}

-(void)ActivateBtn_CLICKED
{
      if ([self CheckLocPermission])
      {
          
          if (!(userLoc.coordinate.latitude == 0 && userLoc.coordinate.longitude == 0))
          {
             
              [self ReceiveRUTer:[RUTer_Dict objectForKey:@"RUTer_Id"] Range:[NSString stringWithFormat:@"%f",SliderValue*1000] User_id:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME] Is_accept:@"1" Latitude:[NSString stringWithFormat:@"%f",userLoc.coordinate.latitude] Longitude:[NSString stringWithFormat:@"%f",userLoc.coordinate.longitude]];
          }
          else
          {
              UIAlertView    *alert = [[UIAlertView alloc] initWithTitle:@"Failed to fetch Location"
                                                                 message:@"Please try again"
                                                                delegate:nil
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
              [alert show];
          }

    
      }
}

-(void)DeActivateBtn_CLICKED
{
    
    
    [self ReceiveRUTer:[RUTer_Dict objectForKey:@"RUTer_Id"] Range:[NSString stringWithFormat:@"%f",SliderValue] User_id:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME] Is_accept:@"2" Latitude:[NSString stringWithFormat:@"%f",0.000] Longitude:[NSString stringWithFormat:@"%f",0.000]];
}

-(void)all_btn_CLICKED
{
     [self zoomLevel:NO];
}

-(void)me_Btn_CLICKED
{
    [self zoomLevel:YES];
}


-(BOOL)CheckLocPermission
{
      if([CLLocationManager locationServicesEnabled])
      {
          if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied)
          {
              if (IS_OS_8_OR_LATER) {
                  
                  
                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location Service Denied"
                                                                  message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                                                 delegate:self
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles:@"Settings",nil];
                  
                  alert.tag = 100;
                  [alert show];
                  
                  
              }
              else
              {
                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location Service Denied"
                                                                  message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                                                 delegate:self
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles:nil];
                  
                  alert.tag = 101;
                  [alert show];
                  
              }
              return NO;
          }
          else
          {
             
              return YES;
          }
      }

    return NO;
    
}
#pragma mark - mapViewDelegate


#pragma mark -MapView Delegate Methods
- (MKAnnotationView *)mapView:(MKMapView *)mapVieww viewForAnnotation:(id<MKAnnotation>)annotation {
    //7
     AvatarImage* avatarImg = [AvatarImage new];
    UIImage *mask;
    JFMapAnnotation* ano = annotation;
   // NSLog(@"annotation  %@ ", annotation);
    if ([ano isKindOfClass:[JFMapAnnotation class]]) {
                   mask = [avatarImg profileImage:[self friendDetailFetch:[NSString stringWithFormat:@"%@@%@",ano.IsMe,DOMAINAME]]];
        

    }
    else if ([ano isKindOfClass:[MKUserLocation class]])
    {
         mask = [avatarImg myProfileImage];
    }
    static NSString *identifier = @"myAnnotation";
    MKAnnotationView * annotationView = (MKAnnotationView*)[mapVieww dequeueReusableAnnotationViewWithIdentifier:identifier];
       
    mask = [mask resizedImage:CGSizeMake(27, 27) interpolationQuality:kCGInterpolationHigh ];
    mask = [mask roundedCornerImage:13.5 borderSize:0];

    if (!annotationView)
    {
        //9
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        annotationView.canShowCallout = YES;
        annotationView.image = mask;
       
    }else {
        annotationView.image = mask;
    }
    return annotationView;
}

- (void)setCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                  zoomLevel:(NSUInteger)zoomLevel animated:(BOOL)animated {
    MKCoordinateSpan span = MKCoordinateSpanMake(0, 360/pow(2, zoomLevel)*mapView.frame.size.width/256);
    [mapView setRegion:MKCoordinateRegionMake(centerCoordinate, span) animated:animated];
}
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    


    // Return if we already have a location or if the returned location is invaled
 
}



- (void)mapView:(MKMapView *)mapVieww didUpdateUserLocation:(MKUserLocation *)userLocation
{
    
    userLoc = userLocation;
    
       NSArray* annoAry = [mapView annotations];
    
    for (JFMapAnnotation* anno in annoAry)
    {
        if ([anno isKindOfClass:[JFMapAnnotation class]])
    {
        if ([anno.IsMe isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME]] )
        {
            anno.coordinate = userLocation.coordinate;
        }
    }
    else if ([anno isKindOfClass:[MKUserLocation class]])
             anno.coordinate = userLocation.coordinate;
        
    }
   
    
    
    [mapView removeOverlay:circle];
    circle = [MKCircle circleWithCenterCoordinate:userLoc.coordinate radius:SliderValue*1000];
    [mapView addOverlay:circle];
    
    
    if (firstInstance) {
        
        firstInstance = NO;
        if (([[RUTer_Dict objectForKey:@"isActivated"]  isEqualToString:@"NO"] ) && ([[RUTer_Dict objectForKey:@"iAmSender"]  isEqualToString:@"YES"] ))
        {
            [self zoomLevel:YES];
        }
        else
        {
            [self zoomLevel:NO];
        }
    }
    else
    {
        [self setCenterCoordinate:userLocation.coordinate zoomLevel:13 animated:YES];

    }
    
    
 //   17.4316492,78.4210113
    
}


#pragma mark - getCurrentLocation method


-(void)getCurrentLocation
{
    if([CLLocationManager locationServicesEnabled]){
        
        

        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        {
            [locationManager requestWhenInUseAuthorization];
        }
        [locationManager startUpdatingLocation];

        
    }
    
    
}
#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
  //  NSLog(@"%@",error);
    
    
}


#pragma mark - CLLocationManagerDelegate

/*
 *  locationManager:didUpdateToLocation:fromLocation:
 *
 *  Discussion:
 *    Invoked when a new location is available. oldLocation may be nil if there is no previous location
 *    available.
 *
 *    This method is deprecated. If locationManager:didUpdateLocations: is
 *    implemented, this method will not be called.
 */

-(void)zoomLevel:(BOOL)ISMine
{
    if (!ISMine) {
        [self fitAnnotations];
    }
    else
    {
        [self setCenterCoordinate:userLoc.coordinate zoomLevel:13 animated:YES];
    }
}


- (MKOverlayView *)mapView:(MKMapView *)map viewForOverlay:(id <MKOverlay>)overlay
{
    MKCircleView *circleView = [[MKCircleView alloc] initWithOverlay:overlay];
    circleView.strokeColor = [UIColor clearColor];
    circleView.fillColor = RGBA(162,202,228,0.6);
    return circleView;
}



#pragma mark - JGDetailScrubberDelegate

- (void)DidStopSliding
{
    [mapView removeOverlay:circle];
    circle = [MKCircle circleWithCenterCoordinate:userLoc.coordinate radius:SliderValue*1000];
    
    [mapView addOverlay:circle];
    
   
    
}

-(void)valueChanged:(float)value
{
    NSMutableArray* receiverAry = [RUTer_Dict objectForKey:@"Receiver"];
    NSMutableDictionary* senderDict = [RUTer_Dict objectForKey:@"Sender"];
    
    if ([[RUTer_Dict objectForKey:@"iAmSender"] isEqualToString:@"NO"])
    {
        notifyLbl.text = [NSString stringWithFormat:@"Notify me when %@ is less than %.2f km away from me",[self friendName:[senderDict objectForKey:@"id"]] ,value];
        
    }
    else if ([[RUTer_Dict objectForKey:@"iAmSender"] isEqualToString:@"YES"] && ([receiverAry count] > 1))
    {
        notifyLbl.text = [NSString stringWithFormat:@"Notify me when group members are less than %.2f km away from me",value];
    }
    else
    {
        notifyLbl.text = [NSString stringWithFormat:@"Notify me when he/she is less than %.2f km away from me" ,value];
    }
    
    receiverAry = nil;
    senderDict = nil;
    
    SliderValue = value;
}


#pragma mark - ASValueTrackingSliderDataSource

- (NSString *)slider:(ASValueTrackingSlider *)slider stringForValue:(float)value;
{
    value = roundf(value);
    NSString *s;
    if (value < -10.0) {
        s = @"❄️Brrr!⛄️";
    } else if (value > 29.0 && value < 50.0) {
        s = [NSString stringWithFormat:@"😎 %@ 😎", [slider.numberFormatter stringFromNumber:@(value)]];
    } else if (value >= 50.0) {
        s = @"I’m Melting!";
    }
    return s;
}

- (void)sliderWillDisplayPopUpView:(ASValueTrackingSlider *)slider;
{
    
}

-(void)backbuttonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES ];
    
}



/*
- (void)scrubber:(JGDetailScrubber *)slider StoppedScrubbing:(BOOL *)status
{
    if (status) {
        [mapView removeOverlay:circle];
        circle = [MKCircle circleWithCenterCoordinate:userLoc.coordinate radius:SliderValue*1000];
       
        [mapView addOverlay:circle];
    }
    
    
}
- (void)scrubber:(JGDetailScrubber *)slider didChangeToValue:(CGFloat)Value {
  
    NSMutableArray* receiverAry = [RUTer_Dict objectForKey:@"Receiver"];
    NSMutableDictionary* senderDict = [RUTer_Dict objectForKey:@"Sender"];
    
    if ([[RUTer_Dict objectForKey:@"iAmSender"] isEqualToString:@"NO"])
    {
         notifyLbl.text = [NSString stringWithFormat:@"Notify me when %@ is less than %f km away from me",[self friendName:[senderDict objectForKey:@"id"]] ,Value];

    }
   else if ([[RUTer_Dict objectForKey:@"iAmSender"] isEqualToString:@"YES"] && ([receiverAry count] > 1))
    {
        notifyLbl.text = [NSString stringWithFormat:@"Notify me when group members are less than %f km away from me",Value];
    }
    else
    {
        notifyLbl.text = [NSString stringWithFormat:@"Notify me when Online Group members are less than %f km away from me" ,Value];
    }
    
    receiverAry = nil;
    senderDict = nil;
    
    SliderValue = Value;
    
}
*/


#pragma mark - Web Services ruthereSender
-(void)ruthereSender_Range:(NSString*)Range_String receiver_id:(NSMutableArray*)receiver_id_String sender_id:(NSString*)sender_id_String Latitude:(NSString*)Latitude_String Longitude:(NSString*)Longitude_String
{
    
    if (self.singleton.internetConnection)
    {
    [self LoadingWithText:@"Sending Request"];
        
    //9bb6b15bc3637d37f4e8945e47a4d9b3926741c676e37bcf48908aab8bc14aa3
    NSString *soapMessage = [NSString stringWithFormat:
                             @"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                             "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:server\">\n"
                             "<soapenv:Header/>\n"
                             "<soapenv:Body>\n"
                             "<urn:ruthereSender soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\n"
                             "<ruthereSenderInput xsi:type=\"urn:ruthereSenderInput\">\n"
                             "<!--You may enter the following 5 items in any order-->\n"
                             "<Range>%@</Range>\n"
                             "%@"
                             "<sender_id>%@</sender_id>\n"
                             "<Latitude>%@</Latitude>\n"
                             "<Longitude>%@</Longitude>\n"
                             "</ruthereSenderInput>\n"
                             "</urn:ruthereSender>\n"
                             "</soapenv:Body>\n"
                             "</soapenv:Envelope>\n",Range_String,[self CreateXmlStringWithArray:receiver_id_String KeyWord:@"receiver_id"],sender_id_String,Latitude_String,Longitude_String
                             ];
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",RUTER_SERVICE]];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue:[NSString stringWithFormat:@"%@/ruthereSender",RUTER_SERVICE]  forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]
                                         initWithRequest:theRequest];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation
                                               , id responseObject) {
        
        
        NSString *str = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        
        
        RXMLElement *rxml = [RXMLElement elementFromXMLString:str encoding:NSUTF8StringEncoding];
        
      //  NSLog(@"xml  ::: %@",rxml);
        
        
        [rxml iterateWithRootXPath:@"//return" usingBlock: ^(RXMLElement *player)
        {
          //  NSLog(@"Player: %@ %@ %@",[player child:@"ReturnCode"], [player child:@"ResponseMsg"], [player child:@"ruthere_id"]);
            if ([[NSString stringWithFormat:@"%@",[player child:@"ReturnCode"]] isEqualToString:@"1000"]  )
            {
               [self ruterBtnAction_Id:[NSString stringWithFormat:@"%@",[player child:@"ruthere_id"] ] type:@"request" Latitude:(NSString*)Latitude_String Longitude:(NSString*)Longitude_String];
            }
            else
            {
               
            }
            
            
        }];
        
         [self unLoadView];
        
        
        // code
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // code
      //  NSLog(@"Error: %@", error.localizedDescription);
        [self unLoadView];
    }
     ];
    [operation start];
    }
    
    else
    {
       
        [self showAlertWithMessage:@"It appears your internet connection is offline."  cancelButtonTile:@"Ok"];
    }
}



-(void)ruterBtnAction_Id:(NSString*)ruthere_id type:(NSString*)type_str Latitude:(NSString*)Latitude_String Longitude:(NSString*)Longitude_String
{
    
    NSMutableArray* toAry = [self getReceiverIds:[RUTer_Dict objectForKey:@"Receiver"]];
   
    NSString *messageStr = @"Are you there?";
    if([messageStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length > 0) {
        
        if (Room_Id) {
            

            
            NSString *messageID=[self.xmppStream generateUUID];
            
            NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
            [body setStringValue:messageStr];
            
            NSXMLElement *ruthere = [NSXMLElement elementWithName:@"ruthere"];
            [ruthere addAttributeWithName:@"xmlns" stringValue:@"ruthere"];
            [ruthere addAttributeWithName:@"ruthere_id" stringValue:ruthere_id];
            [ruthere addAttributeWithName:@"type" stringValue:type_str];
            [ruthere addAttributeWithName:@"latitude" stringValue:Latitude_String];
            [ruthere addAttributeWithName:@"longitude" stringValue:Longitude_String];
            [ruthere addAttributeWithName:@"from" stringValue:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME]];
          
            
            
            NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
            [message addAttributeWithName:@"id" stringValue:messageID];
            [message addAttributeWithName:@"type" stringValue:@"groupchat"];
            [message addAttributeWithName:@"to" stringValue:[NSString stringWithFormat:@"%@",Room_Id]];
            [message addChild:body];
            [message addChild:ruthere];
            [self.xmppStream sendElement:message];
        }
        
else
{
    
               for (NSString* str in toAry)
        {
            
            NSString *messageID=[self.xmppStream generateUUID];
            NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
            [body setStringValue:messageStr];
            NSXMLElement *ruthere = [NSXMLElement elementWithName:@"ruthere"];
            [ruthere addAttributeWithName:@"xmlns" stringValue:@"ruthere"];
            [ruthere addAttributeWithName:@"ruthere_id" stringValue:ruthere_id];
            [ruthere addAttributeWithName:@"type" stringValue:type_str];
            [ruthere addAttributeWithName:@"latitude" stringValue:Latitude_String];
            [ruthere addAttributeWithName:@"longitude" stringValue:Longitude_String];
            [ruthere addAttributeWithName:@"from" stringValue:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME]];
            
            NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
            [message addAttributeWithName:@"id" stringValue:messageID];
            [message addAttributeWithName:@"type" stringValue:@"chat"];
            [message addAttributeWithName:@"to" stringValue:[NSString stringWithFormat:@"%@@%@",str,DOMAINAME]];
            [message addChild:body];
            [message addChild:ruthere];
            [self.xmppStream sendElement:message];

        }
}
       
        
        
       
        
        NSMutableDictionary* Ruter_Dict = [NSMutableDictionary new];
        [Ruter_Dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME] forKey:@"from_Id"];
        [Ruter_Dict setObject:toAry forKey:@"to_Id"];
        [Ruter_Dict setObject:Latitude_String forKey:@"latitude"];
        [Ruter_Dict setObject:Longitude_String forKey:@"longitude"];
        [Ruter_Dict setObject:ruthere_id forKey:@"RUTer_Id"];
        [Ruter_Dict setObject:@"0" forKey:@"ruter_status"];
        [Ruter_Dict setObject:@"1" forKey:@"is_Completed"];
        [Ruter_Dict setObject:@"time String" forKey:@"updated_time"];
        [Ruter_Dict setObject:[NSString stringWithFormat:@"%f",SliderValue] forKey:@"range"];
        
        
        DBEditer* dbEditer_Obj = [DBEditer new];
        
        BOOL insertStatus = [dbEditer_Obj InsertRuter_Req_RUTerId:Ruter_Dict];
        if (insertStatus)
        {
            UIAlertView*  alert = [[UIAlertView alloc] initWithTitle:ALERTVIEW_TITLE
                                                             message:@"Are you there request successfully sent"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
            alert.delegate = self;
            alert.tag = 10;
            
            [alert show];
            [self unLoadView];
            [self locUpdate];
        }
        else
        {
            [self unLoadView];
        }
       
    }
    
    
}


#pragma mark - Web Services ReceiveRUTer

-(void)ReceiveRUTer:(NSString*)ruthere_id Range:(NSString*)Range User_id:(NSString*)User_id Is_accept:(NSString*)Is_accept Latitude:(NSString*)Latitude Longitude:(NSString*)Longitude
{
    if (self.singleton.internetConnection)
    {
          [self LoadingWithText:@"Update Response"];
        
    NSString *soapMessage = [NSString stringWithFormat:
                             @"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                             "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:server\">\n"
                             "<soapenv:Header/>\n"
                             "<soapenv:Body>\n"
                             "<urn:ruthereReceiver soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\n"
                             "<ruthereReciveInput xsi:type=\"urn:ruthereReciveInput\">\n"
                             "<!--You may enter the following 6 items in any order-->\n"
                             "<Ruthere_id>%@</Ruthere_id>\n"
                             "<Range>%@</Range>\n"
                             "<User_id>%@</User_id>\n"
                             "<Is_accept>%@</Is_accept>\n"
                             "<Latitude>%@</Latitude>\n"
                             "<Longitude>%@</Longitude>\n"
                             "</ruthereReciveInput>\n"
                             "</urn:ruthereReceiver>\n"
                             "</soapenv:Body>\n"
                             "</soapenv:Envelope>\n",ruthere_id,Range,User_id,Is_accept,Latitude,Longitude
                             ];
   
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",RUTER_SERVICE]];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue:[NSString stringWithFormat:@"%@/ruthereReceiver",RUTER_SERVICE] forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]
                                         initWithRequest:theRequest];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation
                                               , id responseObject) {
        
        
        NSString *str = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        
        
        RXMLElement *rxml = [RXMLElement elementFromXMLString:str encoding:NSUTF8StringEncoding];
        
    //    NSLog(@"xml  ::: %@",rxml);
        
        
        
        [rxml iterateWithRootXPath:@"//return" usingBlock: ^(RXMLElement *player) {
           // NSLog(@"Player: %@ %@ %@",[player child:@"ReturnCode"], [player child:@"ResponseMsg"], [player child:@"ruthere_id"]);
            
            if ([[NSString stringWithFormat:@"%@",[player child:@"ReturnCode"]] isEqualToString:@"1000"]  )
            {
                NSMutableDictionary* tempDict = [NSMutableDictionary new];
                [tempDict setObject:ruthere_id forKey:@"RUTer_Id"];
                [tempDict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME] forKey:@"from_Id"];
                [tempDict setObject:[NSString stringWithFormat:@"%@",[player child:@"completed"]] forKey:@"completed"];
                [tempDict setObject:[NSString stringWithFormat:@"%@",[player child:@"status"]] forKey:@"status"];
                [tempDict setObject:[NSString stringWithFormat:@"%@",[player child:@"recvr_notif"]] forKey:@"recvr_notif"];
               
                [self updateLatLong:[RUTer_Dict objectForKey:@"RUTer_Id"] Is_accept:Is_accept Latitude:Latitude Longitude:Longitude tempDict:tempDict];
            }
            else
            {
               
            }
            
        }];
       [self unLoadView];
        
        //NSLog(@"%@",[rxml attribute:@"Response"]);
        
        
        
        // code
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // code
      //  NSLog(@"Error: %@", error.localizedDescription);
        [self unLoadView];
    }
     ];
    [operation start];
}

else
{
    
    [self showAlertWithMessage:@"It appears your internet connection is offline."  cancelButtonTile:@"Ok"];
}
}


-(void)updateLatLong:(NSString*)ruthere_id Is_accept:(NSString*)Is_accept Latitude:(NSString*)Latitude Longitude:(NSString*)Longitude tempDict:(NSMutableDictionary*)tempDict
{
     NSString * is_Completed ;
     NSString * ruter_status ;
    NSString *messageStr;
    if ([Is_accept isEqualToString:@"1"] )
    {
        Is_accept = @"true";
        ruter_status = @"1";
        is_Completed = @"1";
         messageStr = @"Are you there accepted";
    }
    else
    {
          Is_accept = @"false";
        ruter_status = @"3";
        is_Completed = @"2";
         messageStr = @"Are you there denied";
    }
        
        
    
        if([messageStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length > 0) {
            
            if (Room_Id) {
            NSString *messageID=[self.xmppStream generateUUID];
            
            NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
            [body setStringValue:messageStr];
            NSXMLElement *ruthere = [NSXMLElement elementWithName:@"ruthere"];
            [ruthere addAttributeWithName:@"xmlns" stringValue:@"ruthere"];
            [ruthere addAttributeWithName:@"ruthere_id" stringValue:ruthere_id];
            [ruthere addAttributeWithName:@"type" stringValue:@"response"];
            [ruthere addAttributeWithName:@"latitude" stringValue:Latitude];
            [ruthere addAttributeWithName:@"longitude" stringValue:Longitude];
            [ruthere addAttributeWithName:@"from" stringValue:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME]];
            [ruthere addAttributeWithName:@"accepted" stringValue:Is_accept];
            
            NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
            [message addAttributeWithName:@"id" stringValue:messageID];
            [message addAttributeWithName:@"type" stringValue:@"groupchat"];
            [message addAttributeWithName:@"to" stringValue:Room_Id];
            [message addChild:body];
            [message addChild:ruthere];
            [self.xmppStream sendElement:message];
            }
            else
            {
                NSString *messageID=[self.xmppStream generateUUID];
                
                NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
                [body setStringValue:messageStr];
                NSXMLElement *ruthere = [NSXMLElement elementWithName:@"ruthere"];
                [ruthere addAttributeWithName:@"xmlns" stringValue:@"ruthere"];
                [ruthere addAttributeWithName:@"ruthere_id" stringValue:ruthere_id];
                [ruthere addAttributeWithName:@"type" stringValue:@"response"];
                [ruthere addAttributeWithName:@"latitude" stringValue:Latitude];
                [ruthere addAttributeWithName:@"longitude" stringValue:Longitude];
                [ruthere addAttributeWithName:@"from" stringValue:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME]];
                [ruthere addAttributeWithName:@"accepted" stringValue:Is_accept];
                
                NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
                [message addAttributeWithName:@"id" stringValue:messageID];
                [message addAttributeWithName:@"type" stringValue:@"chat"];
                [message addAttributeWithName:@"to" stringValue:[NSString stringWithFormat:@"%@@%@",[[RUTer_Dict objectForKey:@"Sender"] objectForKey:@"id"],DOMAINAME]];
                [message addChild:body];
                [message addChild:ruthere];
                [self.xmppStream sendElement:message];
            }
            
            
            
            NSMutableDictionary* Ruter_Dict = [NSMutableDictionary new];
            [Ruter_Dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME] forKey:@"from_Id"];
            [Ruter_Dict setObject:Latitude forKey:@"latitude"];
            [Ruter_Dict setObject:Longitude forKey:@"longitude"];
            [Ruter_Dict setObject:ruthere_id forKey:@"RUTer_Id"];
            [Ruter_Dict setObject:ruter_status forKey:@"ruter_status"];
           [Ruter_Dict setObject:is_Completed forKey:@"is_Completed"];
            [Ruter_Dict setObject:[NSString stringWithFormat:@"%f",SliderValue] forKey:@"range"];
            
           DBEditer* dbEditer_Obj = [DBEditer new];
            BOOL insertStatus = [dbEditer_Obj UpdateRuter_Req_RUTerId_response:Ruter_Dict];
            if (insertStatus) {
                [dbEditer_Obj AnyReflectionOnRUTerStatus:ruthere_id];
            }

            if (insertStatus)
            {
                UIAlertView*  alert = [[UIAlertView alloc] initWithTitle:ALERTVIEW_TITLE
                                                                 message:@" Request updated successfully"
                                                                delegate:self
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
                alert.delegate = self;
                alert.tag = 10;
                
                [alert show];
                [self unLoadView];
                [self locUpdate];
            }
            
        }
        
    if ([Is_accept isEqualToString:@"true"])
    {
        [self isReached:tempDict];
    }
    
}

-(void)isReached:(NSMutableDictionary*)TempDict
{
    
    
      //  NSLog(@"%@",TempDict);
        
        NSMutableDictionary* RUTer_Obj = [NSMutableDictionary new];
        [RUTer_Obj setObject:[TempDict objectForKey:@"RUTer_Id"] forKey:@"RUTer_Id"];
        [RUTer_Obj setObject:[TempDict objectForKey:@"from_Id"] forKey:@"from_Id"];
        [RUTer_Obj setObject:[TempDict objectForKey:@"completed"] forKey:@"is_Completed"];
        if(!([[TempDict objectForKey:@"status"] integerValue] > 1) ){
            
            if (([[NSString stringWithFormat:@"%@",[TempDict objectForKey:@"recvr_notif"]] integerValue] > 0)) {
                [RUTer_Obj setObject:@"2" forKey:@"ruter_status"];
                
            }
            else
            {
                [RUTer_Obj setObject:[NSString stringWithFormat:@"%@",[TempDict objectForKey:@"status"]] forKey:@"ruter_status"];
            }
        }
        else
        {
            [RUTer_Obj setObject:@"3" forKey:@"ruter_status"];
        }
    
        DBEditer* dbEditer_Obj = [DBEditer new];
        BOOL updateStatus = [dbEditer_Obj UpdateRuter_Req_RUTerId_response:RUTer_Obj];
        if (updateStatus) {

            [dbEditer_Obj AnyReflectionOnRUTerStatus:[RUTer_Obj objectForKey:@"RUTer_Id"]];
        }
        
    
}


#pragma mark - Web Services ruthereRequest  cancelBtn_CLICKED

-(void)cancelBtn_CLICKED
{
    
    [self ruthereRequestcancel:[RUTer_Dict objectForKey:@"RUTer_Id"]];
}

-(void)ruthereRequestcancel:(NSString*)ruthere_id
{
    if (self.singleton.internetConnection)
    {
        [self LoadingWithText:@"Updating Cancel request"];
    NSString *soapMessage = [NSString stringWithFormat:
                             @"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                             "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:server\">\n"
                             "<soapenv:Header/>\n"
                             "<soapenv:Body>\n"
                             "<urn:ruthereCancel soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\n"
                             "<ruthereCancelInput xsi:type=\"urn:ruthereCancelInput\">\n"
                             "<!--You may enter the following 3 items in any order-->\n"
                            "<Ruthere_id>%@</Ruthere_id>\n"
                             "<User_id>%@</User_id>\n"
                             "<Cancel>1</Cancel>\n"
                             "</ruthereCancelInput>\n"
                             "</urn:ruthereCancel>\n"
                             "</soapenv:Body>\n"
                             "</soapenv:Envelope>\n",ruthere_id,[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME]
                             ];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",RUTER_SERVICE]];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: [NSString stringWithFormat:@"%@/ruthereCancel",RUTER_SERVICE] forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]
                                         initWithRequest:theRequest];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation
                                               , id responseObject) {
        
        
        NSString *str = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        
       // NSLog(@"%@",str);
        
        
        
        
        
        RXMLElement *rxml = [RXMLElement elementFromXMLString:str encoding:NSUTF8StringEncoding];
        
      //  NSLog(@"xml  ::: %@",rxml);
        
        
        
        [rxml iterateWithRootXPath:@"//return" usingBlock: ^(RXMLElement *player) {
            if ([[NSString stringWithFormat:@"%@",[player child:@"ReturnCode"]] isEqualToString:@"1000"]  )
            {
                [self updateCancelStatus:[RUTer_Dict objectForKey:@"RUTer_Id"]];
            }
            else{
               
            }

        }];
        [self unLoadView];
        //NSLog(@"%@",[rxml attribute:@"Response"]);
        
        
        
        // code
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // code
       // NSLog(@"Error: %@", error.localizedDescription);
        [self unLoadView];
    }
     ];
    [operation start];
    }
    
    else
    {
        [self unLoadView];
        [self showAlertWithMessage:@"It appears your internet connection is offline."  cancelButtonTile:@"Ok"];
    }

    
}


-(void)updateCancelStatus:(NSString*)ruthere_id
{
    NSString * complete ;
    NSString * ruter_status ;
   
        complete = @"1";
        ruter_status = @"4";
  
   
    
    NSString *messageStr = @"Are you there request cancelled";
    if([messageStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length > 0) {
        
        NSString *messageID=[self.xmppStream generateUUID];
        
        
        if (Room_Id) {
            NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
            [body setStringValue:messageStr];
            NSXMLElement *ruthere = [NSXMLElement elementWithName:@"ruthere"];
            [ruthere addAttributeWithName:@"xmlns" stringValue:@"ruthere"];
            [ruthere addAttributeWithName:@"ruthere_id" stringValue:ruthere_id];
            [ruthere addAttributeWithName:@"type" stringValue:@"cancelled"];
            
            [ruthere addAttributeWithName:@"from" stringValue:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME]];
            
            
            NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
            [message addAttributeWithName:@"id" stringValue:messageID];
            [message addAttributeWithName:@"type" stringValue:@"groupchat"];
            
            
            [message addAttributeWithName:@"to" stringValue:Room_Id];
            
            [message addChild:body];
            [message addChild:ruthere];
            [self.xmppStream sendElement:message];
        }
        else
        {
        if ([[RUTer_Dict objectForKey:@"iAmSender"] isEqualToString:@"NO"])
        {
            NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
            [body setStringValue:messageStr];
            NSXMLElement *ruthere = [NSXMLElement elementWithName:@"ruthere"];
            [ruthere addAttributeWithName:@"xmlns" stringValue:@"ruthere"];
            [ruthere addAttributeWithName:@"ruthere_id" stringValue:ruthere_id];
            [ruthere addAttributeWithName:@"type" stringValue:@"cancelled"];
            
            [ruthere addAttributeWithName:@"from" stringValue:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME]];
            
            
            NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
            [message addAttributeWithName:@"id" stringValue:messageID];
            [message addAttributeWithName:@"type" stringValue:@"chat"];
            
            
            [message addAttributeWithName:@"to" stringValue:[NSString stringWithFormat:@"%@@%@",[[RUTer_Dict objectForKey:@"Sender"] objectForKey:@"id"],DOMAINAME]];
            
            [message addChild:body];
            [message addChild:ruthere];
            [self.xmppStream sendElement:message];
        }
        
        else
        {
        NSMutableArray* toAry = [self getReceiverIds:[RUTer_Dict objectForKey:@"Receiver"]];
        for (NSString* str in toAry)
        {
            
            NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
            [body setStringValue:messageStr];
            NSXMLElement *ruthere = [NSXMLElement elementWithName:@"ruthere"];
            [ruthere addAttributeWithName:@"xmlns" stringValue:@"ruthere"];
            [ruthere addAttributeWithName:@"ruthere_id" stringValue:ruthere_id];
            [ruthere addAttributeWithName:@"type" stringValue:@"cancelled"];
            
            [ruthere addAttributeWithName:@"from" stringValue:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME_XMPP]];
            
            
            NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
            [message addAttributeWithName:@"id" stringValue:messageID];
            [message addAttributeWithName:@"type" stringValue:@"chat"];
            
            
            [message addAttributeWithName:@"to" stringValue:[NSString stringWithFormat:@"%@@%@",str,DOMAINAME]];
            
            [message addChild:body];
            [message addChild:ruthere];
            [self.xmppStream sendElement:message];
        }
            
        }
       
        
        }
       
        
        NSMutableDictionary* Ruter_Dict = [NSMutableDictionary new];
        [Ruter_Dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME] forKey:@"from_Id"];
        [Ruter_Dict setObject:@"2" forKey:@"is_Completed"];

        [Ruter_Dict setObject:ruthere_id forKey:@"RUTer_Id"];
        [Ruter_Dict setObject:ruter_status forKey:@"ruter_status"];
        
        
        DBEditer* dbEditer_Obj = [DBEditer new];
        BOOL insertStatus = [dbEditer_Obj UpdateRuter_Req_RUTerId_response:Ruter_Dict];
              if (insertStatus) {
            [dbEditer_Obj AnyReflectionOnRUTerStatus:ruthere_id];
        }
        if (insertStatus)
        {
            UIAlertView*  alert = [[UIAlertView alloc] initWithTitle:ALERTVIEW_TITLE
                                                             message:@"Request updated successfully"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
            alert.delegate = self;
            alert.tag = 10;
            
            [alert show];
        
        }
        }
        [self unLoadView];
    
    
    
    
}


#pragma mark - Web Services ruthereRequestDetail

-(void)ruthereRequestDetail:(NSString*)Ruthere_id User_id:(NSString*)User_id
{
    
    NSString *soapMessage = [NSString stringWithFormat:
                             @"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                             "<SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">\n"
                             "<SOAP-ENV:Body><ns8240:ruthereRequestDetailsInfo xmlns:ns8240=\"http://tempuri.org\">\n"
                             "<input>\n"
                             "<Ruthere_id>%@</Ruthere_id>\n"
                             "<User_id>%@</User_id>\n"
                             "</input>\n"
                             "</ns8240:ruthereRequestDetailsInfo>\n"
                             "</SOAP-ENV:Body>\n"
                             "</SOAP-ENV:Envelope>\n",Ruthere_id,User_id
                             ];
  
    
    NSURL *url = [NSURL URLWithString:RUTER_SERVICE];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
    
    NSString *userLocation = [NSString stringWithFormat:@"%@/%@",SOS_SERVICE,@"ruthereRequestDetailsInfo"];

    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: userLocation forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]
                                         initWithRequest:theRequest];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation
                                               , id responseObject) {
        
        
        NSString *str = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        
      //  NSLog(@"%@",str);
       
        
        
        
        
        RXMLElement *rxml = [RXMLElement elementFromXMLString:str encoding:NSUTF8StringEncoding];
        
       // NSLog(@"xml  ::: %@",rxml);
           DBEditer* dbEditer_Obj = [DBEditer new];
        
        [rxml iterateWithRootXPath:@"//return" usingBlock: ^(RXMLElement *player) {
           
          
            [rxml iterateElements:[player children:@"Receiver_Details"] usingBlock:^(RXMLElement *RuthereDetailsElement)
             {
                 
                 
                      [rxml iterateElements:[RuthereDetailsElement children:@"item"] usingBlock:^(RXMLElement *itemElement)
                       {
                           [rxml iterateElements:[itemElement children:@"item"] usingBlock:^(RXMLElement *itemsElement)
                            {
                                NSString* string = [NSString stringWithFormat:@"Ruthere_id -- >%@ receiver_id -- >%@ Receiver_Latitude -- >%@ Receiver_Longitude -- >%@ ",[itemsElement child:@"Ruthere_id"],[itemsElement child:@"Receiver_ID"],[itemsElement child:@"Receiver_Latitude"],[itemsElement child:@"Receiver_Longitude"]];
                                //NSLog(@"ruthereRequestDetail  %@",string);
                                
                                NSMutableDictionary* RUTer_Obj = [NSMutableDictionary new];
                                [RUTer_Obj setObject:[NSString stringWithFormat:@"%@",[itemsElement child:@"Ruthere_id"]] forKey:@"RUTer_Id"];
                                
                                [RUTer_Obj setObject:[NSString stringWithFormat:@"%@@%@",[itemsElement child:@"Receiver_ID"],DOMAINAME ] forKey:@"from_Id"];
                                
                                [RUTer_Obj setObject:[NSString stringWithFormat:@"%@",[itemsElement child:@"Receiver_Latitude"]] forKey:@"latitude"];
                                [RUTer_Obj setObject:[NSString stringWithFormat:@"%@",[itemsElement child:@"Receiver_Longitude"]] forKey:@"longitude"];
                                
                                
                                BOOL updateStatus = [dbEditer_Obj UpdateRuter_Req_RUTerId_response:RUTer_Obj];
                                if (updateStatus) {
                                    CLLocationCoordinate2D location;
                                    location.latitude = [[NSString stringWithFormat:@"%@",[itemsElement child:@"Receiver_Latitude"]] doubleValue];
                                    location.longitude = [[NSString stringWithFormat:@"%@",[itemsElement child:@"Receiver_Longitude"]] doubleValue];
                                    FrndLoc.coordinate = location;
                                    [self zoomLevel:NO];

                                }
                                
                               
                          //      NSLog(@"%@",string);
                                
                            }];
                           
                           
                       }];
                 
                 [rxml iterateElements:[RuthereDetailsElement children:@"as_receiver"] usingBlock:^(RXMLElement *as_receiverElement)
                  {
                //      NSLog(@"as_receiverElement");
                      
                      [rxml iterateElements:[as_receiverElement children:@"item"] usingBlock:^(RXMLElement *itemElement)
                       {
                           [rxml iterateElements:[itemElement children:@"item"] usingBlock:^(RXMLElement *itemsElement)
                            {
                                
                            }];
                           
                           
                       }];
                  }];
             }];
            
            [rxml iterateElements:[player children:@"Sender_Details"] usingBlock:^(RXMLElement *SenderDetailsElement)
             {
                 
                 
                 [rxml iterateElements:[SenderDetailsElement children:@"item"] usingBlock:^(RXMLElement *itemElement)
                  {
                      [rxml iterateElements:[itemElement children:@"item"] usingBlock:^(RXMLElement *itemsElement)
                       {
                           NSString* string = [NSString stringWithFormat:@"Ruthere_id -- >%@ receiver_id -- >%@ Receiver_Latitude -- >%@ Receiver_Longitude -- >%@ ",[itemsElement child:@"Ruthere_id"],[itemsElement child:@"Sender_ID"],[itemsElement child:@"Sender_Latitude"],[itemsElement child:@"Sender_Longitude"]];
                           
                           NSMutableDictionary* RUTer_Obj = [NSMutableDictionary new];
                           [RUTer_Obj setObject:[NSString stringWithFormat:@"%@",[itemsElement child:@"Ruthere_id"]] forKey:@"RUTer_Id"];
                           
                           [RUTer_Obj setObject:[NSString stringWithFormat:@"%@@%@",[itemsElement child:@"Sender_ID"],DOMAINAME ] forKey:@"from_Id"];
                           
                           [RUTer_Obj setObject:[NSString stringWithFormat:@"%@",[itemsElement child:@"Sender_Latitude"]] forKey:@"latitude"];
                           [RUTer_Obj setObject:[NSString stringWithFormat:@"%@",[itemsElement child:@"Sender_Longitude"]] forKey:@"longitude"];
                           
                           
                           BOOL updateStatus = [dbEditer_Obj UpdateRuter_Req_RUTerId_response:RUTer_Obj];
                           if (updateStatus) {
                               CLLocationCoordinate2D location;
                               location.latitude = [[NSString stringWithFormat:@"%@",[itemsElement child:@"Sender_Latitude"]] doubleValue];
                               location.longitude = [[NSString stringWithFormat:@"%@",[itemsElement child:@"Sender_Longitude"]] doubleValue];
                               FrndLoc.coordinate = location;
                               [self zoomLevel:NO];
                               
                           }
                           
                           
                        //   NSLog(@"%@",string);
                           
                       }];
                      
                      
                  }];
                 
                 
             }];
        }];
        
        //NSLog(@"%@",[rxml attribute:@"Response"]);
        
        
        
        // code
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // code
     //   NSLog(@"Error: %@", error.localizedDescription);
    }
     ];
    [operation start];
    
}


-(void)locUpdate
{
   
    locSingleton = [LocationManagerSingleton sharedSingleton];
    [locSingleton startUpdatingCurrentLocation];
    NSDate *currentDate = [NSDate date];
    [[NSUserDefaults standardUserDefaults] setObject:[currentDate dateByAddingTimeInterval:-120] forKey:@"LastDate"];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 10)
    {
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else if (alertView.tag == 100)
   {
            if (buttonIndex == 1)
            {
                
                [[UIApplication sharedApplication] openURL:[NSURL  URLWithString:UIApplicationOpenSettingsURLString]];
            }
            else if (buttonIndex == 0)
            {
            
            }
    }
    else  if (alertView.tag == 101)
    {
    
    }

    
}

#pragma mark ALERT MESSAGE
-(void)showAlertWithMessage:(NSString *)message cancelButtonTile:(NSString *)cancelTitle {
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ALERTVIEW_TITLE message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    alert = nil;
    
}
-(void)LoadingWithText:(NSString*)String
{
    [DejalBezelActivityView activityViewForView:self.view withLabel:String width:120];
}

-(void)unLoadView
{
    [DejalBezelActivityView removeViewAnimated:YES];
    
}


-(NSString*)friendName:(NSString*)str
{
    XMPPUserCoreDataStorageObject* frd_obj = [self friendDetailFetch:[NSString stringWithFormat:@"%@@shaili",str]];
    if (frd_obj.displayName.length == 0)
    {
        frd_obj.displayName = str;
    }
    return frd_obj.displayName;
   
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark DataBase Methods For Recent Chats
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (XMPPUserCoreDataStorageObject *)friendDetailFetch:(NSString *)Str
{
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:[self managedObjectContext_roster]];
    [request setEntity:entity];
    NSString *predicateFrmt = @"jidStr = %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, Str];
    [request setPredicate:predicate];
    NSArray *empArray=[[self managedObjectContext_roster] executeFetchRequest:request error:nil];
    if ([empArray count] == 1)
    {
        for (XMPPUserCoreDataStorageObject *user in  empArray)
        {
            return user;
        }
    }
    return nil;
}
- (NSManagedObjectContext *)managedObjectContext_roster
{
	return [[[self appDelegate] xmppRosterStorage] mainThreadManagedObjectContext];
}

-(NSString*)CreateXmlStringWithArray:(NSMutableArray*)objects KeyWord:(NSString*)KeyWord
{
    NSMutableString* FinalStr = [NSMutableString new];
    
    for (NSString* obj in objects)
    {
        [FinalStr appendString:[NSString stringWithFormat:@"<%@>%@</%@>\n",KeyWord,obj,KeyWord]];
    }
    return FinalStr;
}

-(NSMutableArray*)getReceiverIds:(NSMutableArray*)receivers
{
    NSMutableArray* ids = [NSMutableArray new];
    
    for (NSMutableDictionary* rec in receivers)
    {
        [ids addObject:[rec objectForKey:@"id"]];
    }
    
    return ids;
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
