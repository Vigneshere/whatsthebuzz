//
//  passcodeViewController.m
//  Buzz
//
//  Created by shaili_macMini02 on 13/02/14.
//  Copyright (c) 2014 shaili_macMini02. All rights reserved.
//

#import "passcodeViewController.h"
#import "profileFormViewController.h"
#import "AFJSONRequestOperation.h"
#import "RXMLElement.h"

#define LOGO_IMG_RADIUS  54
#define BOX_WIDTH  248

#define DEMO_USER1 @"9100682103"
#define DEMO_USER2 @"9100682106"


@interface passcodeViewController ()<UITextFieldDelegate>
{
    UIScrollView* base_ScrollVw;
    
    UIImageView* logoImgVw;
    UILabel* statusTxtVw;
    UITextField* hiddenPwdTxtFld;
    UIView* PassCodeVW;
    UIButton* goBtn;
    UIButton* editBtn;
    int randomID;
    
}

@end

@implementation passcodeViewController
@synthesize phoneNumberStr;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if(IS_IPHONE_4)
    {
        self.view.backgroundColor = BACKGROUNDIMAGE_4;
        
    }
    else
    {
        self.view.backgroundColor = BACKGROUNDIMAGE;
        
    }
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBarHidden = YES;
    
    
    self.singleton = [Singleton sharedMySingleton];
    
    
    
    
    
    
    base_ScrollVw = [[UIScrollView alloc]init];
    logoImgVw = [[UIImageView alloc]init];
    PassCodeVW = [[UIView alloc]init];
    hiddenPwdTxtFld = [[UITextField alloc]init];
    statusTxtVw = [[UILabel alloc]init];
    
    goBtn = [UIButton buttonWithType:UIButtonTypeSystem];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    if ([phoneNumberStr isEqualToString:DEMO_USER1] || [phoneNumberStr isEqualToString:DEMO_USER2])
    {
        NSString *DEMOPWD = @"1234";
        
        randomID = [DEMOPWD intValue];
    }
    else
    {
      //  randomID  = arc4random() % 9000 + 1000;
        
        NSString *DEMOPWD = @"1234";
        
        randomID = [DEMOPWD intValue];

    }
    
  //  [self sendSMS :phoneNumberStr userRandomNumber:randomID];
    
    
    hiddenPwdTxtFld.keyboardType = UIKeyboardTypeNumberPad;
    hiddenPwdTxtFld.delegate = self;
    [base_ScrollVw addSubview:hiddenPwdTxtFld];
    
    base_ScrollVw.frame = CGRectMake(0, 0, self.view.frame.size.width,self.view.frame.size.height);
    base_ScrollVw.backgroundColor = [UIColor clearColor];
    base_ScrollVw.contentSize = CGSizeMake(self.view.frame.size.width,self.view.frame.size.height);
    base_ScrollVw.scrollEnabled = NO;
    [self.view addSubview:base_ScrollVw];
    
    
    editBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    editBtn.frame = CGRectMake(self.view.frame.size.width - 70, 15, 60, 28);
    [editBtn setTitle:@"Edit" forState:UIControlStateNormal];
    [editBtn addTarget:self action:@selector(EDIT_BTN_CLICKED) forControlEvents:UIControlEventTouchUpInside];
    [editBtn setTitleColor:RGBA(53, 152, 219, 1) forState:UIControlStateNormal];
    editBtn.backgroundColor = [UIColor clearColor];
    editBtn.titleLabel.font = [UIFont fontWithName:FontString size:15];//[UIFont fontWithName:FontString size:14];
    editBtn.layer.cornerRadius = 28/2;
    editBtn.layer.borderColor =  RGBA(53, 152, 219, 1).CGColor;
    editBtn.layer.borderWidth = 1.2f;
    editBtn.enabled = YES;
    [base_ScrollVw addSubview:editBtn];
    
    logoImgVw.frame = CGRectMake((self.view.frame.size.width/2) - (LOGOIMAGE.size.width/2), 20, LOGOIMAGE.size.width, LOGOIMAGE.size.height);
    logoImgVw.image = LOGOIMAGE;
    [base_ScrollVw addSubview:logoImgVw];
    
    NSAttributedString *labelText = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat: @"We have sent you an SMS with a code to +91%@. To complete your phone number verification please enter the default Verification code 1234.",phoneNumberStr]];
    
    NSString *myString=[NSString stringWithFormat: @"We have sent you an SMS with a code to +91%@. To complete your phone number verification please enter the default Verification code 1234.",phoneNumberStr];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:labelText];

    
    NSRange rangeForPassCode = [myString rangeOfString:@"1234" options:NSBackwardsSearch range:NSMakeRange(0, [myString length])];
    NSRange rangeForPhoneNum = [myString rangeOfString:[NSString stringWithFormat:@"+91%@",phoneNumberStr] options:NSBackwardsSearch range:NSMakeRange(0, [myString length])];

    // Adding PassCode Color
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:rangeForPassCode];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:20] range:rangeForPassCode];
    
    // Adding PhoneNum Color
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:rangeForPhoneNum];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:20] range:rangeForPhoneNum];

    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:3];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelText length])];
    
    statusTxtVw.backgroundColor = [UIColor clearColor];
    statusTxtVw.frame = CGRectMake(10, logoImgVw.frame.origin.y + logoImgVw.frame.size.height, self.view.frame.size.width-20, 100);
    statusTxtVw.textAlignment = NSTextAlignmentJustified;
    statusTxtVw.attributedText = attributedString;
    statusTxtVw.font =  [UIFont fontWithName:FontString size:15];
    //  statusTxtVw.textColor = [[UIColor blackColor] colorWithAlphaComponent:0.75];
    statusTxtVw.numberOfLines = 4;
    [base_ScrollVw addSubview:statusTxtVw];
    
    PassCodeVW = [self create_circles:4 radius:25 space:10 view:PassCodeVW];
    PassCodeVW.frame = CGRectMake((self.view.frame.size.width - PassCodeVW.frame.size.width)/2, statusTxtVw.frame.origin.y + statusTxtVw.frame.size.height, PassCodeVW.frame.size.width, PassCodeVW.frame.size.height);
    [base_ScrollVw addSubview:PassCodeVW];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    [PassCodeVW addGestureRecognizer:tapGesture];
    
    UITapGestureRecognizer *tapGesture_keyBord = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGesture)];
    [self.view addGestureRecognizer:tapGesture_keyBord];
    
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGesture)];
    swipeGesture.direction = UISwipeGestureRecognizerDirectionDown;
    [base_ScrollVw addGestureRecognizer:swipeGesture];
    
    goBtn.frame = CGRectMake((self.view.frame.size.width - 80)/2, PassCodeVW.frame.origin.y + PassCodeVW.frame.size.height + 30, 80, 30);
    [goBtn setTitle:@"Submit" forState:UIControlStateNormal];
    [goBtn setTintColor:[UIColor whiteColor]];
    [goBtn addTarget:self action:@selector(GO_BTN_CLICKED) forControlEvents:UIControlEventTouchUpInside];
    [goBtn setTitleColor:RGBA(53, 152, 219, 1) forState:UIControlStateNormal];
    goBtn.backgroundColor = [UIColor clearColor];
    goBtn.titleLabel.font = [UIFont fontWithName:FontString size:17];//[UIFont fontWithName:FontString size:14];
    goBtn.layer.cornerRadius = 30/2;
    goBtn.layer.borderColor =  RGBA(53, 152, 219, 1).CGColor;
    goBtn.layer.borderWidth = 1.2f;
    
    [base_ScrollVw addSubview:goBtn];
}

-(void)viewDidAppear:(BOOL)animated
{
    //self.navigationController.navigationBar.topItem.title = phoneNumberStr;
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    //  [editBtn removeFromSuperview];
}

-(void)handleTapGesture:(UITapGestureRecognizer *) sender
{
    [hiddenPwdTxtFld becomeFirstResponder];
}

-(void)handleSwipeGesture
{
    
    
    [hiddenPwdTxtFld resignFirstResponder];
}

-(void)GO_BTN_CLICKED
{
    if ([hiddenPwdTxtFld.text isEqualToString:[NSString stringWithFormat:@"%d",randomID]])
    {
        [self registration:phoneNumberStr];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Verification key didn't match!" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

-(void)EDIT_BTN_CLICKED
{
    fromPassCodeVC = YES;
    [self.navigationController popViewControllerAnimated:YES];
}


-(UIView*)create_circles:(int)count radius:(int)radius space:(int)space view:(UIView*)view
{
    
    view.frame = CGRectMake(0, 0, (space * (count - 1) + (count * 2 * radius)), 2*radius);
    
    for (int i = 0; i < count; i++) {
        
        UILabel* lbl = [[UILabel alloc]init];
        lbl.frame = CGRectMake(i*((2*radius)+space), 0, 2*radius, 2*radius);
        lbl.backgroundColor = [UIColor whiteColor];
        lbl.tag = i+1;
        lbl.layer.cornerRadius = radius;
        lbl.layer.borderColor = [RGBA(214, 214, 214, 1) CGColor];
        lbl.layer.borderWidth =1.5;
        
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.layer.masksToBounds = YES;
        [view addSubview:lbl];
    }
    
    return view;
}


#pragma mark - textField
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (goBtn.frame.origin.y + goBtn.frame.size.height > self.view.frame.size.height - 250) {
        
        [UIView animateWithDuration:0.25 animations:^
         {
             self.view.frame = CGRectMake(0, -64, self.view.frame.size.width,self.view.frame.size.height);
         }];
        
    }
    
    return YES;
}


- (void) textFieldDidBeginEditing:(UITextField *)textField {
    
    
    [UIView animateWithDuration:0.25 animations:^
     {
         editBtn.frame = CGRectMake(self.view.frame.size.width - 70, 70, 60, 28);
         
     }];
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    [UIView animateWithDuration:0.25 animations:^
     {
         self.view.frame = CGRectMake(0, 0, self.view.frame.size.width,self.view.frame.size.height);
         editBtn.frame = CGRectMake(self.view.frame.size.width - 70, 15, 60, 28);
         
     }];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
    {
        
        return NO;
    }
    
    // verify max length has not been exceeded
    NSUInteger newLength = textField.text.length + string.length - range.length;
    
    if ([string isEqualToString:@""] ) {
        UILabel* passcode = (UILabel*)[PassCodeVW viewWithTag:newLength+1];
        passcode.text = @"";
        return YES;
    }
    if (newLength < 5) {
        UILabel* passcode = (UILabel*)[PassCodeVW viewWithTag:newLength];
        passcode.text = string;
        [NSTimer scheduledTimerWithTimeInterval: 0.2 target: self
                                       selector: @selector(substitude:) userInfo:passcode repeats: NO];
        return YES;
    }
    else
    {
        return NO;
    }
    
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    return YES;
}

-(void)substitude :(NSTimer*)theTimer
{
    UILabel* passcodeLbl = (UILabel*)[theTimer userInfo];
    
    passcodeLbl.text = @"\u25cf";
    
}


-(void)sendSMS :(NSString*)phoneNum userRandomNumber:(int)randomNumber
{
    if (self.singleton.internetConnection)
    {
        //** Random Number Generation
        
        NSString *msgStr = [NSString stringWithFormat:@"Here is your Code! Start BuZZing: %d",randomNumber];
        msgStr = [msgStr stringByReplacingOccurrencesOfString:@" " withString:@"+"];
        NSLog(@"Random Number ---------> %d",randomNumber);
        
        NSString *urlStr = [NSString stringWithFormat:@"http://%@/api/web2sms.php?workingkey=%@&to=%@%@&sender=%@&message=%@",SMS_SERVICE,SMS_API_KEY,[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"],phoneNum,SMS_SENDERID,msgStr];
        
        NSURL *url = [NSURL URLWithString:urlStr];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]
                                             initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation
                                                   , id responseObject) {
            
            NSString *str = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            
            //    NSLog(@"SMS Response  ::: %@",str);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            //  NSLog(@"Error: %@", error.localizedDescription);
        }
         ];
        
        [operation start];
    }
    
    else
    {
        [self unLoadView];
        [self showAlertWithMessage:@"It appears your internet connection is offline."  cancelButtonTile:@"Ok"];
    }
}


-(void)registration :(NSString*)phoneNum
{
    
    if (self.singleton.internetConnection)
    {
        
        [self LoadingWithText:@"Registration process"];
        NSString *urlStr = [NSString stringWithFormat:@"http://%@:9090/plugins/userService/userservice?type=add&secret=shaili&username=%@%@&password=123&name=&email=",HOSTNAME,[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"],phoneNum];
        urlStr = [urlStr stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
        
        
        NSURL *url = [NSURL URLWithString:urlStr];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]
                                             initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation
                                                   , id responseObject) {
            
            
            NSString *str = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            
            
            RXMLElement *rxml = [RXMLElement elementFromXMLString:str encoding:NSUTF8StringEncoding];
            
            NSLog(@"xml  ::: %@",rxml);
            [self unLoadView];
            
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"],phoneNum ] forKey:USERNAME];
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@%@@%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"],phoneNum,DOMAINAME ] forKey:USERNAME_XMPP];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self UDID_Reg:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"],phoneNum] userModel:@"iPhone" deviceToken:[[NSUserDefaults standardUserDefaults]objectForKey:@"DEVICE_TOKEN"]];
            profileFormViewController* profileVwCtrl = [[profileFormViewController alloc]init];
            profileVwCtrl.fromPasswordScreen = YES;
            [hiddenPwdTxtFld resignFirstResponder];
            [self.navigationController pushViewController:profileVwCtrl animated:YES];
            
            
            // code
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             //code
            NSLog(@"Error: %@", error.localizedDescription);
        }
         ];
        [operation start];
    }
    
    else
    {
        [self unLoadView];
        [self showAlertWithMessage:@"It appears your internet connection is offline."  cancelButtonTile:@"Ok"];
    }
}

#pragma mark - Web Services UDID Reg
-(void)UDID_Reg:(NSString*)phoneNum userModel:(NSString*)userModel_String deviceToken:(NSString*)deviceToken_String
{
    if (self.singleton.internetConnection) {
        
        if (deviceToken_String.length == 0)
        {
            deviceToken_String = @"9bb6b15bc3637d37f4e8945e47a4d9b3926741c676e37bcf48908aab8bc14aa3";
        }
        NSString *soapMessage = [NSString stringWithFormat:
                                 @"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                                 "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:server\">\n"
                                 "<soapenv:Header/>\n"
                                 "<soapenv:Body>\n"
                                 "<urn:ruthereReg soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\n"
                                 "<ruthereRegInput xsi:type=\"urn:ruthereRegInput\">\n"
                                 "<!--You may enter the following 3 items in any order-->\n"
                                 "<mobileNum>%@</mobileNum>\n"
                                 "<userModel>%@</userModel>\n"
                                 "<deviceToken>%@</deviceToken>\n"
                                 "</ruthereRegInput>\n"
                                 "</urn:ruthereReg>\n"
                                 "</soapenv:Body>\n"
                                 "</soapenv:Envelope>\n",phoneNum,userModel_String,deviceToken_String
                                 ];
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",RUTER_SERVICE]];
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
        NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
        
        [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [theRequest addValue: [NSString stringWithFormat:@"%@/ruthereReg",RUTER_SERVICE]  forHTTPHeaderField:@"SOAPAction"];
        [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]
                                             initWithRequest:theRequest];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation
                                                   , id responseObject) {
            
            
            NSString *str = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            
            
            RXMLElement *rxml = [RXMLElement elementFromXMLString:str encoding:NSUTF8StringEncoding];
            
            // NSLog(@"xml  ::: %@",rxml);
            
            
            
            [rxml iterateWithRootXPath:@"//return" usingBlock: ^(RXMLElement *player) {
                //   NSLog(@"Player: %@ %@ ",[player child:@"ReturnCode"], [player child:@"ResponseMsg"]);
            }];
            
            // code
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            // code
            //   NSLog(@"Error: %@", error.localizedDescription);
        }
         ];
        [operation start];
    }
    
    else
    {
        [self showAlertWithMessage:@"It appears your internet connection is offline."  cancelButtonTile:@"Ok"];
    }
    
}


#pragma mark ALERT MESSAGE
-(void)showAlertWithMessage:(NSString *)message cancelButtonTile:(NSString *)cancelTitle {
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ALERTVIEW_TITLE message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    alert = nil;
    
}

-(void)LoadingWithText:(NSString*)String
{
    [DejalBezelActivityView activityViewForView:self.view withLabel:String width:120];
}

-(void)unLoadView
{
    [DejalBezelActivityView removeViewAnimated:YES];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
