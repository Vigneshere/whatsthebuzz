//
//  BeeCollectionView.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 08/05/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "BeeCollectionView.h"
#import "UIImage+animatedGIF.h"

@implementation BeeCollectionView

- (id)initWithFrame:(CGRect)frame imgAry:(NSArray*)imgAry
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
          beeAry = imgAry;
         self.backgroundColor  = [UIColor clearColor];
        _bgIMgVw = [UIImageView new];
        _bgIMgVw.frame = CGRectMake(5, 0, self.frame.size.width, self.frame.size.height);
        _bgIMgVw.userInteractionEnabled = YES;
        [self addSubview:_bgIMgVw];
        
        
        UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
        _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(5, 0, self.frame.size.width, self.frame.size.height) collectionViewLayout:layout];
        layout.minimumInteritemSpacing = 10;
        _collectionView.backgroundColor=[UIColor clearColor];
        [_collectionView setDataSource:self];
        [_collectionView setDelegate:self];
        _collectionView.userInteractionEnabled = YES;
        [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
        [self addSubview:_collectionView];
        
        
    }
    return self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [beeAry count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
   
    cell.backgroundColor=[UIColor clearColor];
    
    UIImageView* bee = [UIImageView new];
    bee.frame = CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height);
    
    
//    if (indexPath.row == [beeAry count]-1 )
//    {
//        UIImage *angryImages = [UIImage animatedImageWithAnimatedGIFURL:[[NSBundle mainBundle] URLForResource:@"blue-smile" withExtension:@"gif"]];
//        bee.animationImages = angryImages.images;
//        bee.animationDuration = 2.5f;
//        [bee startAnimating];
//    }
//    else if (indexPath.row == [beeAry count]-2)
//    {
//        UIImage *angryImages = [UIImage animatedImageWithAnimatedGIFURL:[[NSBundle mainBundle] URLForResource:@"red_Angry_Bird" withExtension:@"gif"]];
//        bee.animationImages = angryImages.images;
//        bee.animationDuration = 2.5f;
//        [bee startAnimating];
//
//    }
//    else
//    {
        bee.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png",[beeAry objectAtIndex:indexPath.row]]];

//    }

    
    bee.userInteractionEnabled = YES;
    bee.tag = indexPath.row;
    [cell addSubview:bee];
    
   
    
    UITapGestureRecognizer *tapPress = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPress:)];
    [bee addGestureRecognizer:tapPress];
//    [tapPress setCancelsTouchesInView:NO];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(50, 45);
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5, 5, 5, 5);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{

   [self.BeeCollectionDelegate didSelectBeeInCollectionView:(long)indexPath.row];

}

- (void)tapPress:(UITapGestureRecognizer*)gesture
{
    UIImageView* imgVw = (UIImageView*)gesture.view;
    UICollectionViewCell* cell = (UICollectionViewCell*)imgVw.superview;
    NSIndexPath* index = [_collectionView indexPathForCell:cell];
    
    CGPoint location = [gesture locationInView:self];
           
        [self.BeeCollectionDelegate tapGestureState_xpoint:location.x ypoint:location.y index:(long)index.row];
    
}


@end
