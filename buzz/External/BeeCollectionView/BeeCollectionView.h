//
//  BeeCollectionView.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 08/05/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol BeeCollectionViewdelegate<NSObject>

@required
-(void)didSelectBeeInCollectionView:(NSInteger )index;
-(void)dragGestureState:(NSString*)state xpoint:(int)xpoint ypoint:(int)ypoint index:(long)index;
-(void)tapGestureState_xpoint:(int)xpoint ypoint:(int)ypoint index:(long)index;

@end
@interface BeeCollectionView : UIView<UICollectionViewDataSource,UICollectionViewDelegate>
{
    NSArray* beeAry;
    UICollectionView * _collectionView;
}
@property (nonatomic, weak)  id<BeeCollectionViewdelegate> BeeCollectionDelegate;
@property (nonatomic, strong) UIImageView *bgIMgVw;

- (id)initWithFrame:(CGRect)frame imgAry:(NSArray*)imgAry;

@end
