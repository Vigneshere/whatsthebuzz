//
//  privateChatDbEditer.m
//  buzz
//
//  Created by shaili_solutions_MacPro3 on 11/06/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "privateChatDbEditer.h"
#import "PrivateSection.h"
#import "private_timer.h"

@implementation privateChatDbEditer
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark privateChat check And Insert Value
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(BOOL)checkAndInsert:(NSMutableDictionary*)privateChatInfoDict
{
    
    if ([privateChatInfoDict objectForKey:@"iD"])
    {
        NSManagedObjectContext *context = [self managedObjectContext];
        
        // to insert
        
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"PrivateSection"
                                                   inManagedObjectContext:context];
        [request setEntity:entity1];
        
        NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(iD = \"%@\")",[privateChatInfoDict objectForKey:@"iD"]]];
        [request setPredicate:pred];
        
        NSArray *empArray=[self.managedObjectContext executeFetchRequest:request error:nil];
        if (![empArray count]) {
            return [self insertPrivateSection:privateChatInfoDict];
        }
        else
        {
            return  [self updatePrivateSection:privateChatInfoDict];
        }
        
    }
    else
    {
        return NO;
    }
    
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark privateChat Core Data UpdateValue
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(BOOL)updatePrivateSection:(NSMutableDictionary*)privateChatInfoDict
{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // to insert
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"PrivateSection"
                                               inManagedObjectContext:context];
    [request setEntity:entity1];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(iD = \"%@\")",[privateChatInfoDict objectForKey:@"iD"]]];
    [request setPredicate:pred];
    
    NSArray *empArray=[self.managedObjectContext executeFetchRequest:request error:nil];
    
    for (PrivateSection *PrivateSection_Info in empArray)
    {
        if ([privateChatInfoDict objectForKey:@"iD"]) {
            PrivateSection_Info.iD = [privateChatInfoDict objectForKey:@"iD"];
        }
        
        if ([privateChatInfoDict objectForKey:@"timer"]) {
            PrivateSection_Info.timer = [[privateChatInfoDict objectForKey:@"timer"] doubleValue];
        }
        
        if ([privateChatInfoDict objectForKey:@"status"]) {
            PrivateSection_Info.status = [privateChatInfoDict objectForKey:@"status"];
        }
        
        if ([privateChatInfoDict objectForKey:@"is_Sender"]) {
            PrivateSection_Info.is_Sender = [[privateChatInfoDict objectForKey:@"is_Sender"] boolValue];
        }
        
        PrivateSection_Info.time_updated = [NSDate date];

    }
    
    
    
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        return NO;
    }
    if ([[privateChatInfoDict objectForKey:@"status"] isEqualToString:@"request"] || [[privateChatInfoDict objectForKey:@"status"] isEqualToString:@"pending"])
    {

        private_timer* addTimer = [private_timer new];
        [addTimer createTimer_id:[privateChatInfoDict objectForKey:@"iD"] seconds:privateTimer_int];
    }
    return YES;
    
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Media Core Data InsertValue
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(BOOL)insertPrivateSection:(NSMutableDictionary*)privateChatInfoDict
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    PrivateSection *PrivateSection_Info = [NSEntityDescription
                           insertNewObjectForEntityForName:@"PrivateSection"
                           inManagedObjectContext:context];
    
    if ([privateChatInfoDict objectForKey:@"iD"]) {
        PrivateSection_Info.iD = [privateChatInfoDict objectForKey:@"iD"];
    }
    
    if ([privateChatInfoDict objectForKey:@"timer"]) {
        PrivateSection_Info.timer = [[privateChatInfoDict objectForKey:@"timer"] doubleValue];
    }
    
    if ([privateChatInfoDict objectForKey:@"status"]) {
        PrivateSection_Info.status = [privateChatInfoDict objectForKey:@"status"];
    }
    
    if ([privateChatInfoDict objectForKey:@"is_Sender"]) {
        PrivateSection_Info.is_Sender = [[privateChatInfoDict objectForKey:@"is_Sender"] boolValue];
    }
    PrivateSection_Info.time_updated = [NSDate date];
    
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        return NO;
    }
    
    if ([[privateChatInfoDict objectForKey:@"status"] isEqualToString:@"request"] || [[privateChatInfoDict objectForKey:@"status"] isEqualToString:@"pending"])
    {
        private_timer* addTimer = [private_timer new];
        [addTimer createTimer_id:[privateChatInfoDict objectForKey:@"iD"] seconds:privateTimer_int];
    }
    return YES;
    
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark delete Private Section
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(BOOL)deletePrivateChat:(NSString *)ID
{
    
    
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSFetchRequest *deleterequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *delentity = [NSEntityDescription entityForName:@"PrivateSection"
                                                 inManagedObjectContext:context];
    [deleterequest setEntity:delentity];
    
    
    NSPredicate *delpred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"iD = \"%@\"",ID]];
    [deleterequest setPredicate:delpred];
    
    
    NSArray *delArray=[self.managedObjectContext executeFetchRequest:deleterequest error:nil];
    
    
    for (PrivateSection *PrivateSection_status in delArray)
    {
        
        
        [context deleteObject:PrivateSection_status];
        [context save:nil];
        
        NSError *error;
        
        if (![context save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            return NO;
        }
        
    }
    return YES;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Media Core Data Fetch DB Value
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(NSMutableArray*)fetchprivateChat
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSFetchRequest *fetchrequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *fetchentity = [NSEntityDescription entityForName:@"PrivateSection"
                                                   inManagedObjectContext:context];
    [fetchrequest setEntity:fetchentity];
    
    
    
    
    NSArray *fetchArray=[self.managedObjectContext executeFetchRequest:fetchrequest error:nil];
    
    
    NSMutableArray* mediaDb = [NSMutableArray new];
    
    for (PrivateSection *PrivateSection_info in fetchArray)
    {
        NSMutableDictionary* dict = [NSMutableDictionary new];
        [dict setObject:PrivateSection_info.iD forKey:@"iD"];
        [dict setObject:PrivateSection_info.status forKey:@"status"];
        [dict setObject:[NSString stringWithFormat:@"%f",PrivateSection_info.timer]  forKey:@"timer"];
        [dict setObject:[NSNumber numberWithBool:PrivateSection_info.is_Sender] forKey:@"is_Sender"];
        
        [mediaDb addObject:dict];
        
    }
    

    return mediaDb;
    
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Media Core Data Fetch media info with msg id
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(NSMutableDictionary*)fetchprivateChat:(NSString*)ID
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSFetchRequest *fetchrequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *fetchentity = [NSEntityDescription entityForName:@"PrivateSection"
                                                   inManagedObjectContext:context];
    [fetchrequest setEntity:fetchentity];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(iD = \"%@\")",ID]];
    [fetchrequest setPredicate:pred];
    
    
    NSArray *fetchArray=[self.managedObjectContext executeFetchRequest:fetchrequest error:nil];
    
    
    NSMutableDictionary* dict = [NSMutableDictionary new];
    
    for (PrivateSection *PrivateSection_info in fetchArray)
    {
        if (PrivateSection_info.iD) {
            [dict setObject:PrivateSection_info.iD forKey:@"iD"];
        }
        
        if (PrivateSection_info.status) {
            [dict setObject:PrivateSection_info.status forKey:@"status"];
        }
        if (PrivateSection_info.timer) {
            [dict setObject:[NSString stringWithFormat:@"%f",PrivateSection_info.timer] forKey:@"timer"];
        }
//        if (PrivateSection_info.is_Sender)
//        {
//            [dict setObject:[NSNumber numberWithBool:PrivateSection_info.is_Sender] forKey:@"is_Sender"];
//        }
        if (PrivateSection_info.time_updated) {
            [dict setObject:PrivateSection_info.time_updated forKey:@"time_updated"];
        }
        
        [dict setObject:[NSNumber numberWithBool:PrivateSection_info.is_Sender] forKey:@"is_Sender"];

    }
    
    return dict;
    
}

-(void)checkAndStartPrivateTimer
{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSFetchRequest *fetchrequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *fetchentity = [NSEntityDescription entityForName:@"PrivateSection"
                                                   inManagedObjectContext:context];
    [fetchrequest setEntity:fetchentity];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(status = \"%@\") OR (status = \"%@\")",@"request",@"pending"]];
    [fetchrequest setPredicate:pred];
    
    
    NSArray *fetchArray=[self.managedObjectContext executeFetchRequest:fetchrequest error:nil];
    
    
    
    for (PrivateSection *PrivateSection_info in fetchArray)
    {
        NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:PrivateSection_info.time_updated];

        if (secondsBetween >= privateTimer_int || secondsBetween < 0)
        {
            [context deleteObject:PrivateSection_info];
            [context save:nil];
            
            NSError *error;
            
            if (![context save:&error]) {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }

            
        }
        else
        {
            private_timer* addTimer = [private_timer new];
            [addTimer createTimer_id:PrivateSection_info.iD seconds:(privateTimer_int - secondsBetween)];

        }
        
    }
    

}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Core Data saveContext
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
	// This is a private method.
	//
	// NSManagedObjectContext is NOT thread-safe.
	// Therefore it is VERY VERY BAD to use our private managedObjectContext outside our private storageQueue.
	//
	// You should NOT remove the assert statement below!
	// You should NOT give external classes access to the storageQueue! (Excluding subclasses obviously.)
	//
	// When you want a managedObjectContext of your own (again, excluding subclasses),
	// you can use the mainThreadManagedObjectContext (below),
	// or you should create your own using the public persistentStoreCoordinator.
	//
	// If you even comtemplate ignoring this warning,
	// then you need to go read the documentation for core data,
	// specifically the section entitled "Concurrency with Core Data".
	//
	//
	// Do NOT remove the assert statment above!
	// Read the comments above!
	//
    
 //   NSAssert([NSThread isMainThread], @"Context reserved for main thread only");

	
	if (_managedObjectContext)
	{
		return _managedObjectContext;
	}
	
	NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
	if (coordinator)
	{
		
		
		if ([NSManagedObjectContext instancesRespondToSelector:@selector(initWithConcurrencyType:)])
			_managedObjectContext =
            [[NSManagedObjectContext alloc] initWithConcurrencyType:NSConfinementConcurrencyType];
		else
			_managedObjectContext = [[NSManagedObjectContext alloc] init];
		
		_managedObjectContext.persistentStoreCoordinator = coordinator;
		_managedObjectContext.undoManager = nil;
		
		
	}
	
	return _managedObjectContext;
}



// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"PrivateChat" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"PrivateChat.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end


