//
//  ACStatusBarOverlayWindow.m
//  buzz
//
//  Created by shaili_solutions_MacPro3 on 04/08/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "ACStatusBarOverlayWindow.h"
#import "notiVw.h"

@implementation ACStatusBarOverlayWindow

+ (ACStatusBarOverlayWindow *)sharedInstance
{
    static dispatch_once_t pred;
    static ACStatusBarOverlayWindow *instance = nil;
    dispatch_once(&pred, ^{
        instance = [[self alloc] init];
    });
	return instance;
}

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Place the window on the correct level and position
        self.windowLevel = UIWindowLevelStatusBar+1.0f;
        [self setWindowLevel:UIWindowLevelStatusBar+1];
        self.frame = CGRectMake(0, 0, 0, 0);;
        
        
        // TODO: Insert subviews (labels, imageViews, etc...)
    }
    return self;
}

//[ACStatusBarOverlayWindow sharedInstance]

- (void)initWithNoti_jidStr:(NSString*)jidStr NameLbl:(NSString*)nameLbl MsgLbl:(NSString*)msgLbl showClose:(BOOL)showClose;
{
    
    UIImage* bgimage = [UIImage imageNamed:@"notiSlider"];
    HEIGHT = bgimage.size.height;
    
    bgimage = nil;
    
    AppDelegate* myDelegate = (((AppDelegate*) [UIApplication sharedApplication].delegate));
    self.frame = CGRectMake(0, 0, myDelegate.window.frame.size.width, HEIGHT);
    
    notiVw* noti = [[notiVw alloc]initWithNoti_jidStr:jidStr NameLbl:jidStr MsgLbl:msgLbl window:self showClose:YES];
    
    // Create an image view with an image to make it look like a status bar.
//    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.frame];
//    backgroundImageView.image = [[UIImage imageNamed:@"notiSlider"] stretchableImageWithLeftCapWidth:2.0f topCapHeight:0.0f];
//    [self addSubview:backgroundImageView];
}

- (void)initWithNoti_jidStr:(NSString*)jidStr NameLbl:(NSString*)nameLbl MsgLbl:(NSString*)msgLbl showClose:(BOOL)showClose showImp:(BOOL)showImp
{
    UIImage* bgimage = [UIImage imageNamed:@"notiSlider"];
    HEIGHT = bgimage.size.height;
    
    bgimage = nil;
    
    AppDelegate* myDelegate = (((AppDelegate*) [UIApplication sharedApplication].delegate));
    self.frame = CGRectMake(0, 0, myDelegate.window.frame.size.width, HEIGHT);
    
    notiVw* noti = [[notiVw alloc]initWithNoti_jidStr:jidStr NameLbl:jidStr MsgLbl:msgLbl window:self showClose:YES showImp:showImp];

    
}


@end