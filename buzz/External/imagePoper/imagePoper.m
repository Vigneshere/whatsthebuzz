//
//  imagePoper.m
//  drag
//
//  Created by shaili_solutions_MacPro3 on 17/07/14.
//  Copyright (c) 2014 JemsNets. All rights reserved.
//

#import "imagePoper.h"

#define stickerZoomIn 0.4

#define stickerZoomOut 1.2

@implementation imagePoper

- (id)initFrame_Image:(UIImage*)Image onView:(UIView*)onview
{
    self = [super initWithFrame:CGRectMake(0, 0, onview.frame.size.width, onview.frame.size.height)];
    
    if(self = [super init])
    {
        self.onView = onview;
        
        self.backgroundColor = [UIColor clearColor];
        
    }
    [self showImage:Image];
    return self;
}

-(void)showImage:(UIImage*)image
{
    UIImageView* imageView = [UIImageView new];
    imageView.image = image;
    imageView.frame = CGRectMake((self.frame.size.width - (image.size.width*stickerZoomIn))/2, (self.frame.size.height - (image.size.height*stickerZoomIn))/2, (image.size.width*stickerZoomIn), (image.size.height*stickerZoomIn));
    [self.onView addSubview:imageView];
    
    [UIView transitionWithView:self.onView
                      duration:0.4f
                       options:UIViewAnimationOptionTransitionNone
                    animations:^(void)
                   {
                        imageView.frame = CGRectMake((self.frame.size.width - (image.size.width * stickerZoomOut))/2, (self.frame.size.height - (image.size.height*stickerZoomOut))/2, (image.size.width * stickerZoomOut), (image.size.height*stickerZoomOut));
                        
                    } completion:^(BOOL finished){
                        
                        [UIView transitionWithView:self.onView
                                          duration:0.4f
                                           options:UIViewAnimationOptionTransitionNone
                                        animations:^(void)
                         {
                             imageView.frame = CGRectMake((self.frame.size.width - (image.size.width*stickerZoomIn))/2, (self.frame.size.height - (image.size.height*stickerZoomIn))/2, (image.size.width*stickerZoomIn), (image.size.height*stickerZoomIn));
                             
                         } completion:^(BOOL finished){
                             [imageView removeFromSuperview];
                             [self removeFromSuperview];
                         }];
                    }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
