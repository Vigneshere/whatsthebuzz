//  Created by Thavasidurai on 14/08/13.
//  Copyright (c) 2013 MobilityMinds Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import <netinet/in.h>

#define PRINT_REACHABILITY_FLAGS 0

typedef enum : unsigned char
{
    GCNetworkReachabilityStatusNotReachable = 1 << 0,
    GCNetworkReachabilityStatusWWAN         = 1 << 1,
    GCNetworkReachabilityStatusWiFi         = 1 << 2
} GCNetworkReachabilityStatus;

extern NSString * const kGCNetworkReachabilityDidChangeNotification;
extern NSString * const kGCNetworkReachabilityStatusKey;

@interface GCNetworkReachability : NSObject

+ (instancetype)reachabilityWithHostName:(NSString *)hostName;

+ (instancetype)reachabilityWithHostAddress:(const struct sockaddr *)hostAddress;

+ (instancetype)reachabilityWithInternetAddress:(in_addr_t)internetAddress;

+ (instancetype)reachabilityWithInternetAddressString:(NSString *)internetAddress;

+ (instancetype)reachabilityWithIPv6Address:(const struct in6_addr)internetAddress;

+ (instancetype)reachabilityWithIPv6AddressString:(NSString *)internetAddress;

+ (instancetype)reachabilityForInternetConnection;

+ (instancetype)reachabilityForLocalWiFi;


- (id)initWithHostAddress:(const struct sockaddr *)hostAddress;

- (id)initWithHostName:(NSString *)hostName;

- (id)initWithReachability:(SCNetworkReachabilityRef)reachability;


- (void)startMonitoringNetworkReachabilityWithHandler:(void(^)(GCNetworkReachabilityStatus status))block;

- (void)startMonitoringNetworkReachabilityWithNotification;

- (void)stopMonitoringNetworkReachability;


- (GCNetworkReachabilityStatus)currentReachabilityStatus;


- (BOOL)isReachable;

- (BOOL)isReachableViaWiFi;

#if TARGET_OS_IPHONE
- (BOOL)isReachableViaWWAN;
#endif

- (SCNetworkReachabilityFlags)reachabilityFlags;

@end
