//
//  RuterData.m
//  buzz
//
//  Created by shaili_macMini02 on 20/03/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "RuterData_ud.h"
#import "RuterStatus_ud.h"


@implementation RuterData_ud


@dynamic latitude;
@dynamic longitude;

@dynamic updated_time;
@dynamic range;

@dynamic status;

@end
