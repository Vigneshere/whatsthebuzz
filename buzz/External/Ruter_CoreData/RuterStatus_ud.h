//
//  RuterStatus.h
//  buzz
//
//  Created by shaili_macMini02 on 20/03/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class RuterData_ud;

@interface RuterStatus_ud: NSManagedObject

@property (nonatomic, retain) NSString * ruter_Id;
@property (nonatomic, retain) NSString * ruter_status;
@property (nonatomic, retain) NSString * is_Completed;
@property (nonatomic, retain) NSString * iS_Sender;
@property (nonatomic, retain) RuterData_ud *info;
@property (nonatomic, retain) NSString * iD;
@end
