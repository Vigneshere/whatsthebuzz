//
//  RuterData.h
//  buzz
//
//  Created by shaili_macMini02 on 20/03/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class RuterStatus_ud;

@interface RuterData_ud : NSManagedObject


@property (nonatomic) NSString * latitude;
@property (nonatomic) NSString * longitude;

@property (nonatomic, retain) NSString * updated_time;
@property (nonatomic, retain) NSString * range;

@property (nonatomic, retain) RuterStatus_ud *status;

@end
