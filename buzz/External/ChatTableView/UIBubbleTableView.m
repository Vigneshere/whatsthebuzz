//
//  UIBubbleTableView.m
//
//  Created by Alex Barinov
//  Project home page: http://alexbarinov.github.com/UIBubbleTableView/
//
//  This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License.
//  To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/
//

#import "UIBubbleTableView.h"
#import "NSBubbleData.h"
#import "NSHeaderData.h"
#import "UIBubbleHeaderTableViewCell.h"
#import "UIBubbleTypingTableViewCell.h"
#import "UpdateDataBase.h"
#import <CoreText/CoreText.h>


@interface UIBubbleTableView ()
{
    UpdateDataBase *databaseUpdate;
}

@property (nonatomic, retain) NSMutableArray *bubbleSection;
@property (nonatomic, retain) UILabel *textLabel;


@end

@implementation UIBubbleTableView

@synthesize bubbleDataSource = _bubbleDataSource;
@synthesize snapInterval = _snapInterval;
@synthesize bubbleSection = _bubbleSection;
@synthesize typingBubble = _typingBubble;
@synthesize showAvatars = _showAvatars;
@synthesize sectionData = _sectionData;

#pragma mark - Initializators

- (void)initializator
{
    // UITableView properties
    
    self.backgroundColor = [UIColor clearColor];
    self.separatorStyle = UITableViewCellSeparatorStyleNone;
    assert(self.style == UITableViewStylePlain);
    
    self.delegate = self;
    self.dataSource = self;
    databaseUpdate = [UpdateDataBase new];
    
    // UIBubbleTableView default properties
    
    //    self.snapInterval = 120;
    //    self.typingBubble = NSBubbleTypingTypeNobody;
    UITapGestureRecognizer *swipeGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cellWasSwiped:)];
    [self addGestureRecognizer:swipeGesture];

}

- (id)init
{
    self = [super init];
    if (self) [self initializator];
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) [self initializator];
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) [self initializator];
    return self;
}

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    self = [super initWithFrame:frame style:UITableViewStylePlain];
    if (self) [self initializator];
    return self;
    
}

#if !__has_feature(objc_arc)
- (void)dealloc
{
    [_bubbleSection release];
	_bubbleSection = nil;
	_bubbleDataSource = nil;
    [super dealloc];
}
#endif

#pragma mark - Override

- (void)reloadData
{
    self.showsVerticalScrollIndicator = NO;
    self.showsHorizontalScrollIndicator = NO;
    
    // Cleaning up
	self.bubbleSection = nil;
    
    // Loading new data
    int count = 0;
#if !__has_feature(objc_arc)
    self.bubbleSection = [[[NSMutableArray alloc] init] autorelease];
#else
    self.bubbleSection = [[NSMutableArray alloc] init];
#endif
    
    if (self.bubbleDataSource && (count = (uint32_t)[self.bubbleDataSource rowsForBubbleTable:self]) > 0)
    {
        NSMutableArray *bubbleData = [[NSMutableArray alloc] initWithCapacity:count];
        for (int i = 0; i < count; i++)
        {
            NSObject *object = [self.bubbleDataSource bubbleTableView:self dataForRow:i];
            [bubbleData addObject:object];
        }
//        NSArray *sortedArray = [bubbleData sortedArrayUsingDescriptors:[NSSortDescriptor sortValues:@"date" ascending:YES]];
        [self.bubbleSection addObject:bubbleData];
    }
    [super reloadData];
}

#pragma mark - UITableViewDelegate implementation

#pragma mark - UITableViewDataSource implementation

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    int result = (uint32_t)[self.bubbleSection count];
    if (self.typingBubble != NSBubbleTypingTypeNobody) result++;
    return result;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // This is for now typing bubble
	if (section >= [self.bubbleSection count]) return 1;
    
    return [[self.bubbleSection objectAtIndex:section] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Now typing
	if (indexPath.section >= (uint32_t)[self.bubbleSection count])
    {
        return (CGFloat)MAX([UIBubbleTypingTableViewCell height], self.showAvatars ? 60 : 0);
    }
    
    if ([[[self.bubbleSection objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] isKindOfClass:[NSBubbleData class]])
    {
        NSBubbleData *data = [[self.bubbleSection objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        CGFloat height = (CGFloat)MAX(data.insets.top + data.view.frame.size.height + data.insets.bottom+20, self.showAvatars ? data.insets.top + data.view.frame.size.height + data.insets.bottom+20 : 0);
        return height;
    }
    else
    {
        return (CGFloat)[UIBubbleHeaderTableViewCell height];
    }
    
    return 0;;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Now typing
	if (indexPath.section >= [self.bubbleSection count])
    {
        static NSString *cellId = @"tblBubbleTypingCell";
        UIBubbleTypingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        
        if (cell == nil) cell = [[UIBubbleTypingTableViewCell alloc] init];
        cell.type = self.typingBubble;
        cell.showAvatar = self.showAvatars;
        cell.avatarImage.image = self.typingAvatarImage;
        return cell;
    }
    
    static  NSString *CellIdentifier;
    UITableViewCell *Tablecell;
    if([[[self.bubbleSection objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] isKindOfClass:[NSHeaderData class]])
    {
        CellIdentifier = @"tblBubbleHeaderCell";
        UIBubbleHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        NSHeaderData *data = [[self.bubbleSection objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        if (cell == nil) cell = [[UIBubbleHeaderTableViewCell alloc] init];
        cell.backgroundColor = [UIColor clearColor];
        cell.date = data.text;
        Tablecell = cell;
    }
    else if ( [[[self.bubbleSection objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] isKindOfClass:[NSBubbleData class]])
    {
        NSBubbleData *data = [[self.bubbleSection objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        CellIdentifier = @"thebubbletable";
        UIBubbleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) cell = [[UIBubbleTableViewCell alloc] init];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.data = data;
        cell.showAvatar = self.showAvatars;
        Tablecell =  cell;
    }
    return Tablecell;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
        //end of loading
        //for example [activityIndicator stopAnimating];
    }
}
#pragma mark - Public interface
- (void) scrollBubbleViewToBottomAnimated:(BOOL)animated
{
    NSInteger lastSectionIdx = [self numberOfSections] - 1;
    if (lastSectionIdx >= 0)
    {
    	[self scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:([self numberOfRowsInSection:lastSectionIdx] - 1) inSection:lastSectionIdx] atScrollPosition:UITableViewScrollPositionBottom animated:animated];
    }
}
- (BOOL)isIndex:(CFIndex)index inRange:(NSRange)range {
    return index > range.location && index < range.location+range.length;
}



-(void)tappedCell:(UITapGestureRecognizer *)sender
{
    CGPoint location = [sender locationInView:self];
    NSIndexPath *swipedIndexPath = [self indexPathForRowAtPoint:location];
    UITableViewCell *swipedCell  = (UITableViewCell*)[self cellForRowAtIndexPath:swipedIndexPath];
    if ([swipedCell isKindOfClass:[UIBubbleTableViewCell class]]) {
        UIBubbleTableViewCell *bubbleCell = (UIBubbleTableViewCell *)swipedCell;
        if ([bubbleCell.data.view isKindOfClass:[UILabel class]]) {
            self.textLabel = (UILabel *)bubbleCell.data.view;
            CFIndex index = [self characterIndexAtPoint:[sender locationInView:self.textLabel]];
            NSError *error = NULL;
            NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
            NSArray *matchArray = [detector matchesInString:self.textLabel.text options:0 range:NSMakeRange(0, self.textLabel.text.length)];
            if(matchArray.count)
            {
                for (NSTextCheckingResult *match in matchArray) {
                    if ([match resultType] == NSTextCheckingTypeLink) {
                        NSRange matchRange = [match range];
                        if ([self isIndex:index inRange:matchRange]) {
                            [[UIApplication sharedApplication] openURL:match.URL];
                            break;
                        }
                        else if(bubbleCell.data.msgID && [bubbleCell.data.chatType isEqualToString:@"singlechat"] && bubbleCell.data.type == BubbleTypeSomeoneElse)
                        {
                            [self.bubbleDataSource didSelectThebubbleCell:bubbleCell.data.msgID Date:bubbleCell.data.date JID:bubbleCell.data.jidStr];
                            
                        }
                    }
                }
            }
        }
    }
    [self.bubbleDataSource resignkeyBoard];
    
}


- (CFIndex)characterIndexAtPoint:(CGPoint)point {
    
    NSMutableAttributedString* optimizedAttributedText = [self.textLabel.attributedText mutableCopy];
    
    // use label's font and lineBreakMode properties in case the attributedText does not contain such attributes
    [self.textLabel.attributedText enumerateAttributesInRange:NSMakeRange(0, [self.textLabel.attributedText length]) options:0 usingBlock:^(NSDictionary *attrs, NSRange range, BOOL *stop) {
        
        if (!attrs[(NSString*)kCTFontAttributeName]) {
            
            [optimizedAttributedText addAttribute:(NSString*)kCTFontAttributeName value:self.textLabel.font range:NSMakeRange(0, [self.textLabel.attributedText length])];
        }
        
        if (!attrs[(NSString*)kCTParagraphStyleAttributeName]) {
            
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineBreakMode:self.textLabel.lineBreakMode];
            
            [optimizedAttributedText addAttribute:(NSString*)kCTParagraphStyleAttributeName value:paragraphStyle range:range];
        }
    }];
    
    // modify kCTLineBreakByTruncatingTail lineBreakMode to kCTLineBreakByWordWrapping
    [optimizedAttributedText enumerateAttribute:(NSString*)kCTParagraphStyleAttributeName inRange:NSMakeRange(0, [optimizedAttributedText length]) options:0 usingBlock:^(id value, NSRange range, BOOL *stop) {
        
        NSMutableParagraphStyle* paragraphStyle = [value mutableCopy];
        
        if ([paragraphStyle lineBreakMode] == kCTLineBreakByTruncatingTail) {
            [paragraphStyle setLineBreakMode:kCTLineBreakByWordWrapping];
        }
        
        [optimizedAttributedText removeAttribute:(NSString*)kCTParagraphStyleAttributeName range:range];
        [optimizedAttributedText addAttribute:(NSString*)kCTParagraphStyleAttributeName value:paragraphStyle range:range];
    }];
    
    ////////
    
    if (!CGRectContainsPoint(self.textLabel.bounds, point)) {
        return NSNotFound;
    }
    
    CGRect textRect = [self textRect];
    
    if (!CGRectContainsPoint(textRect, point)) {
        return NSNotFound;
    }
    
    // Offset tap coordinates by textRect origin to make them relative to the origin of frame
    point = CGPointMake(point.x - textRect.origin.x, point.y - textRect.origin.y);
    // Convert tap coordinates (start at top left) to CT coordinates (start at bottom left)
    point = CGPointMake(point.x, textRect.size.height - point.y);
    
    //////
    
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)optimizedAttributedText);
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, textRect);
    
    CTFrameRef frame = CTFramesetterCreateFrame(framesetter, CFRangeMake(0, [self.textLabel.attributedText length]), path, NULL);
    
    if (frame == NULL) {
        CFRelease(path);
        return NSNotFound;
    }
    
    CFArrayRef lines = CTFrameGetLines(frame);
    
    NSInteger numberOfLines = self.textLabel.numberOfLines > 0 ? MIN(self.textLabel.numberOfLines, CFArrayGetCount(lines)) : CFArrayGetCount(lines);
    
    if (numberOfLines == 0) {
        CFRelease(frame);
        CFRelease(path);
        return NSNotFound;
    }
    
    NSUInteger idx = NSNotFound;
    
    CGPoint lineOrigins[numberOfLines];
    CTFrameGetLineOrigins(frame, CFRangeMake(0, numberOfLines), lineOrigins);
    
    for (CFIndex lineIndex = 0; lineIndex < numberOfLines; lineIndex++) {
        
        CGPoint lineOrigin = lineOrigins[lineIndex];
        CTLineRef line = CFArrayGetValueAtIndex(lines, lineIndex);
        
        // Get bounding information of line
        CGFloat ascent, descent, leading, width;
        width = CTLineGetTypographicBounds(line, &ascent, &descent, &leading);
        CGFloat yMin = floor(lineOrigin.y - descent);
        CGFloat yMax = ceil(lineOrigin.y + ascent);
        
        // Check if we've already passed the line
        if (point.y > yMax) {
            break;
        }
        
        // Check if the point is within this line vertically
        if (point.y >= yMin) {
            
            // Check if the point is within this line horizontally
            if (point.x >= lineOrigin.x && point.x <= lineOrigin.x + width) {
                
                // Convert CT coordinates to line-relative coordinates
                CGPoint relativePoint = CGPointMake(point.x - lineOrigin.x, point.y - lineOrigin.y);
                idx = CTLineGetStringIndexForPosition(line, relativePoint);
                
                break;
            }
        }
    }
    
    CFRelease(frame);
    CFRelease(path);
    
    return idx;
}
- (void) cellWasSwiped:(UITapGestureRecognizer *)sender
{
    [ self tappedCell:sender ];
}

#pragma mark --

- (CGRect)textRect {
    
    CGRect textRect = [self.textLabel textRectForBounds:self.textLabel.bounds limitedToNumberOfLines:self.textLabel.numberOfLines];
    textRect.origin.y = (self.textLabel.bounds.size.height - textRect.size.height)/2;
    
    if (self.textLabel.textAlignment == NSTextAlignmentCenter) {
        textRect.origin.x = (self.textLabel.bounds.size.width - textRect.size.width)/2;
    }
    if (self.textLabel.textAlignment == NSTextAlignmentRight) {
        textRect.origin.x = self.textLabel.bounds.size.width - textRect.size.width;
    }
    
    return textRect;
}@end
