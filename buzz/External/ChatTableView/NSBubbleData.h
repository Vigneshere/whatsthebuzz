//
//  NSBubbleData.h
//
//  Created by Alex Barinov
//  Project home page: http://alexbarinov.github.com/UIBubbleTableView/
//
//  This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License.
//  To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/
//

#import <Foundation/Foundation.h>



typedef enum _NSBubbleType
{
    BubbleTypeMine = 0,
    BubbleTypeSomeoneElse = 1,
    BubbleTypeBeeMine = 2,
    BubbleTypeBeeSomeoneElse = 3,
    BubbleTypePrivateSomeoneElse  = 4,
    BubbleTypePrivateMine = 5
} NSBubbleType;


@interface NSBubbleData : NSObject

@property (readonly, nonatomic, strong) NSDate *date;
@property (readonly, nonatomic) NSBubbleType type;
@property (readonly, nonatomic, strong) UIView *view;
@property (readonly, nonatomic) UIEdgeInsets insets;
@property (nonatomic, strong) UIImage *avatar;
@property (readonly, nonatomic) NSString *jidStr;
@property (readonly, nonatomic) NSString *ruderID;
@property (readonly, nonatomic) NSString *deliveryStr;
@property (readonly, nonatomic) BOOL isSender;
@property (readonly, nonatomic) NSString *chatType;
@property (readonly, nonatomic) NSString *msgID;
@property (strong, nonatomic) UILabel *label;
@property(nonatomic, strong) NSArray* matches;
@property (readonly, nonatomic) BOOL isImp;




- (id)initWithText:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type jidString:(NSString *)jidStr deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType messageID:(NSString *)msgID;
+ (id)dataWithText:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type jidString:(NSString *)jidStr deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType messageID:(NSString *)msgID;

- (id)initWithImage:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type jidString:(NSString *)jidStr deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType messageID:(NSString *)msgID;
+ (id)dataWithImage:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type jidString:(NSString *)jidStr deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType messageID:(NSString *)msgID;

- (id)initWithView:(UIView *)view date:(NSDate *)date type:(NSBubbleType)type insets:(UIEdgeInsets)insets jidString:(NSString *)jidStr RuderID:(NSString *)ruderID deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType isSender:(BOOL)isSender messageID:(NSString *)msgID;
+ (id)dataWithView:(UIView *)view date:(NSDate *)date type:(NSBubbleType)type insets:(UIEdgeInsets)insets jidString:(NSString *)jidStr RuderID:(NSString *)ruderID deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType isSender:(BOOL)isSender messageID:(NSString *)msgID;

+ (id)dataWithTextWithTag:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type jidString:(NSString *)jidStr deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType messageID:(NSString *)msgID impMsg:(BOOL)isImp;
- (id)initWithTextWithTag:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type jidString:(NSString *)jidStr deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType messageID:(NSString *)msgID impMsg:(BOOL)isImp;

+ (id)dataWithImage:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type jidString:(NSString *)jidStr deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType messageID:(NSString *)msgID userName:(NSString *)DisplayName;
- (id)dataWithImage:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type jidString:(NSString *)jidStr deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType messageID:(NSString *)msgID userName:(NSString *)DisplayName;



@end
