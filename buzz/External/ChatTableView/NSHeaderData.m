//
//  NSHeaderData.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 27/03/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "NSHeaderData.h"

@implementation NSHeaderData
@synthesize displayName = _displayName;
@synthesize text = _text;
@synthesize date = _date;



+ (id)dataWithText:(NSString *)text date:(NSDate *)date jidStr:(NSString *)jidStr name:(NSString *)displayName roomJid:(NSString *)roomJidStr
{

   return [[NSHeaderData alloc] initWithText:(NSString *)text date:(NSDate *)date jidStr:(NSString *)jidStr name:(NSString *)displayName roomJid:(NSString *)roomJidStr];
}
- (id)initWithText:(NSString *)text date:(NSDate *)date jidStr:(NSString *)jidStr name:(NSString *)displayName roomJid:(NSString *)roomJidStr{
    
    _text = text;
    _date = date;
    
    return self;
}
@end
