//
//  NSHeaderData.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 27/03/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSHeaderData : NSObject
@property (readonly, nonatomic) NSString *displayName;
@property (readonly, nonatomic) NSString *text;
@property (readonly, nonatomic) NSDate *date;

- (id)initWithText:(NSString *)text date:(NSDate *)date jidStr:(NSString *)jidStr name:(NSString *)displayName roomJid:(NSString *)roomJidStr;
+ (id)dataWithText:(NSString *)text date:(NSDate *)date jidStr:(NSString *)jidStr name:(NSString *)displayName roomJid:(NSString *)roomJidStr;
@end
