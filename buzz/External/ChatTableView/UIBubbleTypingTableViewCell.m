//
//  UIBubbleTypingTableCell.m
//  UIBubbleTableViewExample
//
//  Created by Александр Баринов on 10/7/12.
//  Copyright (c) 2012 Stex Group. All rights reserved.
//

#import "UIBubbleTypingTableViewCell.h"

@interface UIBubbleTypingTableViewCell ()

@property (nonatomic, retain) UIImageView *typingImageView;

@end

@implementation UIBubbleTypingTableViewCell

@synthesize type = _type;
@synthesize typingImageView = _typingImageView;
@synthesize showAvatar = _showAvatar;

+ (CGFloat)height
{
    return 40.0;
}

- (void)setType:(NSBubbleTypingType)value {
    if (!self.typingImageView)
    {
        
        [self setBackgroundColor:[UIColor clearColor]];
        self.typingImageView = [[UIImageView alloc] init];
        [self addSubview:self.typingImageView];
    }
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIImage *bubbleImage = nil;
    CGFloat x = 0;
    
    if (value == NSBubbleTypingTypeMe)
    {
        bubbleImage = [UIImage imageNamed:@"typingMine.png"];
        x = self.frame.size.width - bubbleImage.size.width;
    }
    else
    {
        bubbleImage = [UIImage imageNamed:@"bubbleSomeone.png"];
        x = 0;
    }
    UIImage *img = [UIImage imageNamed:@"1.png"];
    NSArray *animationArray=[NSArray arrayWithObjects:
                             [UIImage imageNamed:@"1.png"],
                             [UIImage imageNamed:@"2.png"],
                             [UIImage imageNamed:@"3.png"],
                             nil];
    UIImageView *animationView=[[UIImageView alloc]initWithFrame:CGRectMake(10, 10,img.size.width ,img.size.height)];
    animationView.animationImages=animationArray;
    animationView.animationDuration=1.5;
    animationView.animationRepeatCount=0;
    [animationView startAnimating];
    [self.typingImageView addSubview:animationView];
    
    
    
    [self.avatarImage removeFromSuperview];
  
    self.typingImageView.image = bubbleImage;
    self.typingImageView.frame = CGRectMake(5, 5, 40, 25);
}

-(void)setTypeString:(NSString *)typeString
{
    if (!self.typingImageView)
    {
        
        [self setBackgroundColor:[UIColor clearColor]];
        self.typingImageView = [[UIImageView alloc] init];
        [self addSubview:self.typingImageView];
    }
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIImage *bubbleImage = nil;
    CGFloat x = 0;
    
    if (![typeString isEqualToString:@"SomeOne"])
    {
        bubbleImage = [self balloonImageForSending];
        x = self.frame.size.width - bubbleImage.size.width;
    }
    else
    {
        bubbleImage = [self balloonImageForReceiving];
        x = 0;
    }
    UIImage *img = [UIImage imageNamed:@"1.png"];
    NSArray *animationArray=[NSArray arrayWithObjects:
                             [UIImage imageNamed:@"1.png"],
                             [UIImage imageNamed:@"2.png"],
                             [UIImage imageNamed:@"3.png"],
                             nil];
    UIImageView *animationView=[[UIImageView alloc]initWithFrame:CGRectMake(10, 10,img.size.width ,img.size.height)];
    animationView.animationImages=animationArray;
    animationView.animationDuration=1.5;
    animationView.animationRepeatCount=0;
    [animationView startAnimating];
    [self.typingImageView addSubview:animationView];
    
    
    self.typingImageView.image = bubbleImage;
    self.typingImageView.frame = CGRectMake(5, 5, 40, 25);
    
}

- (UIImage *)balloonImageForSending
{
    UIImage *bubble = [UIImage imageNamed:@"bubblenew.png"];
    UIColor *color = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];;
    bubble = [self tintImage:bubble withColor:color];
    return [bubble resizableImageWithCapInsets:UIEdgeInsetsMake(17, 21, 16, 27) resizingMode:UIImageResizingModeStretch];
}
- (UIImage *)balloonImageForReceiving
{
    UIImage *bubble = [UIImage imageNamed:@"bubbleReceive.png"];
    UIColor *color = [UIColor colorWithRed:223.0/255.0 green:223.0/255.0 blue:223.0/255.0 alpha:1.0];
    bubble = [self tintImage:bubble withColor:color];
    
    return [bubble resizableImageWithCapInsets:UIEdgeInsetsMake(17, 27, 21, 17) resizingMode:UIImageResizingModeStretch];
}


- (UIImage *)tintImage:(UIImage *)image withColor:(UIColor *)color
{
    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, image.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    CGContextClipToMask(context, rect, image.CGImage);
    [color setFill];
    CGContextFillRect(context, rect);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
