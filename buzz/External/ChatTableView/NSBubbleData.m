//
//  NSBubbleData.m
//
//  Created by Alex Barinov
//  Project home page: http://alexbarinov.github.com/UIBubbleTableView/
//
//  This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License.
//  To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/
//

#import "NSBubbleData.h"
#import <QuartzCore/QuartzCore.h>
#import "UIColor+RandomColor.h"
#import "ColorUtils.h"
#import "UIColor+MLPFlatColors.h"


@implementation NSBubbleData

#pragma mark - Properties

@synthesize date = _date;
@synthesize type = _type;
@synthesize view = _view;
@synthesize insets = _insets;
@synthesize avatar = _avatar;
@synthesize jidStr = _jidStr;
@synthesize ruderID = _ruderID;
@synthesize deliveryStr = _deliveryStr;
@synthesize isSender = _isSender;
@synthesize chatType  = _chatType;
@synthesize msgID  = _msgID;
@synthesize isImp = _isImp;


#pragma mark - Lifecycle

#if !__has_feature(objc_arc)
- (void)dealloc
{
    [_date release];
	_date = nil;
    [_view release];
    _view = nil;
    
    self.avatar = nil;
    
    [super dealloc];
}
#endif

#pragma mark - Text bubble

const UIEdgeInsets textInsetsMine = {5, 10, 5, 17};
const UIEdgeInsets textInsetsSomeone = {5, 15, 5, 10};


+ (id)dataWithText:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type jidString:(NSString *)jidStr deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType messageID:(NSString *)msgID
{
    
    return [[NSBubbleData alloc] initWithText:text date:date type:type jidString:(NSString *)jidStr deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType messageID:msgID];
}

- (id)initWithText:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type jidString:(NSString *)jidStr deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType messageID:(NSString *)msgID
{
    int textSizeValue = 11;
    NSInteger textsize = [[NSUserDefaults standardUserDefaults] integerForKey:@"textSize"];
    if (textsize == 0)
        textSizeValue = 11;
    else if (textsize == 1)
        textSizeValue = 14;
    else if (textsize == 2)
        textSizeValue = 16;
    
    
    
    
    UIFont *font = [UIFont fontWithName:FontString size:textSizeValue];
    CGRect textRect = [(text ? text : @"") boundingRectWithSize:CGSizeMake(220, 9999)
                                                        options:NSStringDrawingUsesLineFragmentOrigin
                                                     attributes:@{NSFontAttributeName:font}
                                                        context:nil];
    
    CGSize size = textRect.size;
    
   // NSLog(@"TextRect 1  ->> %@",NSStringFromCGSize(size));
    
    UIFont *font2 = [UIFont fontWithName:FontString size:textSizeValue];
    
    CGRect textRect2 = [([self dateToString:date] ? [self dateToString:date]:@"") boundingRectWithSize:CGSizeMake(220, 9999)
                                                                                               options:NSStringDrawingUsesLineFragmentOrigin
                                                                                            attributes:@{NSFontAttributeName:font2}
                                                                                               context:nil];
    CGSize size2 =textRect2.size;
    
   // NSLog(@"TextRect 2  ->> %@",NSStringFromCGSize(size2));

    size2 = [self frameForText:[self dateToString:date] sizeWithFont:font2 constrainedToSize:CGSizeMake(220, 9999) lineBreakMode:self.label.lineBreakMode];

    BOOL isFound = NO;
    NSMutableAttributedString *attributedString  = nil;
    if ([chatType isEqualToString:@"group"])
    {
        
        NSRange range = [text rangeOfString:@"\n"];
        isFound = (range.location != NSNotFound);
        if (isFound)
        {
             attributedString = [[NSMutableAttributedString alloc]initWithString:text];
            
              NSString *userName = [text substringWithRange: NSMakeRange (0, range.location)];
          
            if (![UIColor colorWithString:userName])
            {
                [UIColor registerColor:[UIColor randomFlatDarkColor] forName:userName];
            }
            
            if ([UIColor colorWithString:userName])
            {
               [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithString:userName]  range:NSMakeRange(0, range.location)];
            }
            else
            {
             [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor randomFlatLightColor]  range:NSMakeRange(0, range.location)];
            }
            
            [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(range.location,text.length-range.location)];
        }
    }
    
    self.label = [UILabel new];
    if (size.width < size2.width)
        self.label.frame =  CGRectMake(0, 0, size2.width+10,size.height);\
    else
        self.label.frame  = CGRectMake(0, 0, size.width,size.height);
    self.label.numberOfLines = 0;
    self.label.lineBreakMode = NSLineBreakByWordWrapping;
    if (!isFound)
        self.label.text = (text ? text : @"");
    else
        self.label.attributedText = attributedString;
    
    self.label.font = font;
    self.label.backgroundColor = [UIColor clearColor];
    if (type == BubbleTypeMine || type == BubbleTypePrivateMine)
        self.label.textColor = [UIColor whiteColor];
    else
    {
        if (!isFound)
            self.label.textColor = [UIColor whiteColor];
    }
       NSError *error = NULL;
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
    self.matches = [detector matchesInString:self.label .text options:0 range:NSMakeRange(0, self.label.text.length)];
    [self highlightLinksWithIndex];
    
    UIEdgeInsets insets = (type == BubbleTypeMine ? textInsetsMine : textInsetsSomeone);
    return [self initWithView:self.label date:date type:type insets:insets jidString:jidStr RuderID:nil deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType isSender:NO messageID:msgID];
}

+ (id)dataWithTextWithTag:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type jidString:(NSString *)jidStr deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType messageID:(NSString *)msgID impMsg:(BOOL)isImp
{
    
    return [[NSBubbleData alloc] initWithTextWithTag:text date:date type:type jidString:(NSString *)jidStr deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType messageID:msgID impMsg:isImp];
}

- (id)initWithTextWithTag:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type jidString:(NSString *)jidStr deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType messageID:(NSString *)msgID impMsg:(BOOL)isImp
{
    
    
    _isImp = isImp;

    int textSizeValue = 11;
    NSInteger textsize = [[NSUserDefaults standardUserDefaults] integerForKey:@"textSize"];
    if (textsize == 0)
        textSizeValue = 11;
    else if (textsize == 1)
        textSizeValue = 14;
    else if (textsize == 2)
        textSizeValue = 16;
    
    
    
    
    UIFont *font = [UIFont fontWithName:FontString size:textSizeValue];
    CGRect textRect = [(text ? text : @"") boundingRectWithSize:CGSizeMake(220, 9999)
                                                        options:NSStringDrawingUsesLineFragmentOrigin
                                                     attributes:@{NSFontAttributeName:font}
                                                        context:nil];
    
    CGSize size = textRect.size;
    
    // NSLog(@"TextRect 1  ->> %@",NSStringFromCGSize(size));
    
    UIFont *font2 = [UIFont fontWithName:FontString size:textSizeValue];
    
    CGRect textRect2 = [([self dateToString:date] ? [self dateToString:date]:@"") boundingRectWithSize:CGSizeMake(220, 9999)
                                                                                               options:NSStringDrawingUsesLineFragmentOrigin
                                                                                            attributes:@{NSFontAttributeName:font2}
                                                                                               context:nil];
    CGSize size2 =textRect2.size;
    
    // NSLog(@"TextRect 2  ->> %@",NSStringFromCGSize(size2));
    
    size2 = [self frameForText:[self dateToString:date] sizeWithFont:font2 constrainedToSize:CGSizeMake(220, 9999) lineBreakMode:self.label.lineBreakMode];
    
    BOOL isFound = NO;
    NSMutableAttributedString *attributedString  = nil;
    if ([chatType isEqualToString:@"group"]) {
        
        NSRange range = [text rangeOfString:@"\n"];
        isFound = (range.location != NSNotFound);
            if (isFound)
            {
                attributedString = [[NSMutableAttributedString alloc]initWithString:text];
                
                NSString *userName = [text substringWithRange: NSMakeRange (0, range.location)];
                
                if (![UIColor colorWithString:userName])
                {
                    [UIColor registerColor:[UIColor randomFlatDarkColor] forName:userName];
                }
                
                if ([UIColor colorWithString:userName])
                {
                    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithString:userName]  range:NSMakeRange(0, range.location)];
                }
                else
                {
                    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor randomFlatLightColor]  range:NSMakeRange(0, range.location)];
                }
                
                [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(range.location,text.length-range.location)];
            }

        
    }
    
    self.label = [UILabel new];
    if (size.width < size2.width)
        self.label.frame =  CGRectMake(0, 0, size2.width+10,size.height);
    else
        self.label.frame  = CGRectMake(0, 0, size.width,size.height);
    self.label.numberOfLines = 0;
    self.label.lineBreakMode = NSLineBreakByWordWrapping;
    if (!isFound)
        self.label.text = (text ? text : @"");
    else
        self.label.attributedText = attributedString;
    
    self.label.font = font;
    self.label.backgroundColor = [UIColor clearColor];
    if (type == BubbleTypeMine || type == BubbleTypePrivateMine)
        self.label.textColor = [UIColor whiteColor];
    else
    {
        if (!isFound)
            self.label.textColor = [UIColor whiteColor];
    }
    NSError *error = NULL;
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
    self.matches = [detector matchesInString:self.label .text options:0 range:NSMakeRange(0, self.label.text.length)];
    [self highlightLinksWithIndex];
    
    UIEdgeInsets insets = (type == BubbleTypeMine ? textInsetsMine : textInsetsSomeone);
    return [self initWithView:self.label date:date type:type insets:insets jidString:jidStr RuderID:nil deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType isSender:NO messageID:msgID isImp:isImp];
}


-(CGSize)frameForText:(NSString*)text sizeWithFont:(UIFont*)font constrainedToSize:(CGSize)size lineBreakMode:(NSLineBreakMode)lineBreakMode  {
    
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.lineBreakMode = lineBreakMode;
    
    NSDictionary * attributes = @{NSFontAttributeName:font,
                                  NSParagraphStyleAttributeName:paragraphStyle
                                  };
    
    
    CGRect textRect = [text boundingRectWithSize:size
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:attributes
                                         context:nil];
    
    //Contains both width & height ... Needed: The height
    return textRect.size;
}


//---->HighLight Link Bubble Label
- (void)highlightLinksWithIndex
{
    
    NSMutableAttributedString* attributedString = [self.label.attributedText mutableCopy];
    for (NSTextCheckingResult *match in self.matches) {
        if ([match resultType] == NSTextCheckingTypeLink) {
            NSRange matchRange = [match range];
            [attributedString addAttribute:NSForegroundColorAttributeName value:RGBA(3, 60, 205, 1) range:matchRange];
        }
    }
    self.label.attributedText = attributedString;
}
#pragma mark - Image bubble

const UIEdgeInsets imageInsetsMine = {11, 13, 16, 22};
const UIEdgeInsets imageInsetsSomeone = {11, 21, 16, 14};

+ (id)dataWithImage:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type jidString:(NSString *)jidStr deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType messageID:(NSString *)msgID
{
    
    return [[NSBubbleData alloc] initWithImage:image date:date type:type jidString:(NSString *)jidStr deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType messageID:msgID];
}

- (id)initWithImage:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type jidString:(NSString *)jidStr deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType messageID:(NSString *)msgID;
{
    CGSize size = image.size;
    if (size.width > 220)
    {
        size.height /= (size.width / 220);
        size.width = 220;
    }
    
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, size.width + 10, size.height + 10)];
    bgView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, size.width , size.height)];
    imageView.image = image;
    imageView.layer.cornerRadius = 5.0;
    imageView.layer.masksToBounds = YES;
    imageView.backgroundColor = [UIColor clearColor];
    [bgView addSubview:imageView];
    
    
    
    
    
#if !__has_feature(objc_arc)
    [imageView autorelease];
#endif
    
    UIEdgeInsets insets = (type == BubbleTypeMine ? imageInsetsMine : imageInsetsSomeone);
    return [self initWithView:bgView date:date type:type insets:insets jidString:jidStr RuderID:nil deliveryStr:(NSString *)delyStr   chattype:(NSString *)chatType isSender:NO messageID:msgID];
}

+ (id)dataWithImage:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type jidString:(NSString *)jidStr deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType messageID:(NSString *)msgID userName:(NSString *)DisplayName
{
    
    return [[NSBubbleData alloc] initWithImage:image date:date type:type jidString:(NSString *)jidStr deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType messageID:msgID userName:DisplayName];
}

- (id)initWithImage:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type jidString:(NSString *)jidStr deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType messageID:(NSString *)msgID userName:(NSString *)DisplayName
{
    CGSize size = image.size;
    if (size.width > 220)
    {
        size.height /= (size.width / 220);
        size.width = 220;
    }
    
    
    UIFont *font2 = [UIFont fontWithName:FontString size:10];
    
    CGRect textRect2 = [(DisplayName ? DisplayName:@"") boundingRectWithSize:CGSizeMake(220, 9999)
                                                                                               options:NSStringDrawingUsesLineFragmentOrigin
                                                                                            attributes:@{NSFontAttributeName:font2}
                                                                                               context:nil];

    
    
    
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, textRect2.size.width+10 , textRect2.size.width+10)];
    bgView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(bgView.frame.size.width / 2 - ( size.width / 2), bgView.frame.size.width / 2 - ( size.height / 2), size.width , size.height)];
    imageView.image = image;
    imageView.layer.cornerRadius = 5.0;
    imageView.layer.masksToBounds = YES;
    imageView.backgroundColor = [UIColor clearColor];
    [bgView addSubview:imageView];
    
    UILabel* nameLbl = [UILabel new];
    nameLbl.frame = CGRectMake(5, 5, textRect2.size.width , 15);
    nameLbl.text = DisplayName;
    nameLbl.textColor = [UIColor whiteColor];
    nameLbl.backgroundColor = RGBA(1.0, 1.0, 1.0, 0.6);
    nameLbl.font = [UIFont fontWithName:FontString size:10];
    [bgView addSubview:nameLbl];
    
        
#if !__has_feature(objc_arc)
    [imageView autorelease];
#endif
    
    UIEdgeInsets insets = (type == BubbleTypeMine ? imageInsetsMine : imageInsetsSomeone);
    return [self initWithView:bgView date:date type:type insets:insets jidString:jidStr RuderID:nil deliveryStr:(NSString *)delyStr   chattype:(NSString *)chatType isSender:NO messageID:msgID];
}




#pragma mark - Custom view bubble

+ (id)dataWithView:(UIView *)view date:(NSDate *)date type:(NSBubbleType)type insets:(UIEdgeInsets)insets jidString:(NSString *)jidStr RuderID:(NSString *)ruderID deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType isSender:(BOOL)isSender messageID:(NSString *)msgID
{
    

    return [[NSBubbleData alloc] initWithView:view date:date type:type insets:insets jidString:jidStr RuderID:(NSString *)ruderID deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType isSender:(BOOL)isSender messageID:msgID];
}

- (id)initWithView:(UIView *)view date:(NSDate *)date type:(NSBubbleType)type insets:(UIEdgeInsets)insets jidString:(NSString *)jidStr RuderID:(NSString *)ruderID deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType isSender:(BOOL)isSender messageID:(NSString *)msgID
{
    self = [super init];
    if (self)
    {
#if !__has_feature(objc_arc)
        _view = [view retain];
        _date = [date retain];
#else
        _view = view;
        _date = date;
#endif
        _type = type;
        _insets = insets;
        _jidStr = jidStr;
        _ruderID  = ruderID;
        _deliveryStr = delyStr;
        _isSender = isSender;
        _chatType = chatType;
        _msgID  = msgID;
    }
    return self;
}

- (id)initWithView:(UIView *)view date:(NSDate *)date type:(NSBubbleType)type insets:(UIEdgeInsets)insets jidString:(NSString *)jidStr RuderID:(NSString *)ruderID deliveryStr:(NSString *)delyStr chattype:(NSString *)chatType isSender:(BOOL)isSender messageID:(NSString *)msgID isImp :(BOOL)isImp
{
    self = [super init];
    if (self)
    {
#if !__has_feature(objc_arc)
        _view = [view retain];
        _date = [date retain];
#else
        _view = view;
        _date = date;
#endif
        _type = type;
        _insets = insets;
        _jidStr = jidStr;
        _ruderID  = ruderID;
        _deliveryStr = delyStr;
        _isSender = isSender;
        _chatType = chatType;
        _msgID  = msgID;
        _isImp = isImp;
    }
    return self;
}



-(NSString *)dateToString:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [   formatter setDateFormat:@"hh:mma"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    return stringFromDate;
}






@end
