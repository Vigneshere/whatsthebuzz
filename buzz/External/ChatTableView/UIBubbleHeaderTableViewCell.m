//
//  UIBubbleHeaderTableViewCell.m
//  UIBubbleTableViewExample
//
//  Created by Александр Баринов on 10/7/12.
//  Copyright (c) 2012 Stex Group. All rights reserved.
//

#import "UIBubbleHeaderTableViewCell.h"

@interface UIBubbleHeaderTableViewCell ()

@property (nonatomic, retain) UILabel *label;

@end

@implementation UIBubbleHeaderTableViewCell

@synthesize label = _label;
@synthesize date = _date;

+ (CGFloat)height
{
    return 30.0;
}

- (void)setDate:(NSString *)text
{
    
    UIFont *font = [UIFont fontWithName:FontString size:11];
    
    CGRect Screen_size  = [[UIScreen mainScreen]bounds];
    
    CGRect contentViewFrame = self.contentView.frame;
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    contentViewFrame.size.width = screenRect.size.width;
    self.contentView.frame = contentViewFrame;
    
    
    CGRect textRect = [(text ? text : @"") boundingRectWithSize:CGSizeMake(320, 9999)
                                                        options:NSStringDrawingUsesLineFragmentOrigin
                                                     attributes:@{NSFontAttributeName:font}
                                                        context:nil];
    
    self.backgroundColor = [UIColor clearColor];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self.label removeFromSuperview];
    
    
    self.label = [[UILabel alloc] initWithFrame:CGRectMake((Screen_size.size.width/2) - (textRect.size.width+20)/2, 3, textRect.size.width+20, 20)];
    self.label.text = text;
    self.label.font = [UIFont fontWithName:FontString size:10.5];
    self.label.textAlignment = NSTextAlignmentCenter;
    self.label.textColor = [UIColor darkGrayColor];
    self.label.layer.cornerRadius = 5;
    self.label.clipsToBounds = YES;
   self.label.backgroundColor = RGBA(53, 152, 219, 0.2);
  //  self.contentView.backgroundColor = [UIColor greenColor];

    [self addSubview:self.label];
    //[self.label setCenter:self.center];
    
}
@end
