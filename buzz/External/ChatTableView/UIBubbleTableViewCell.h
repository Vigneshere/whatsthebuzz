//
//  singlechattableViewcell.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 23/05/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSBubbleData.h"
#import <QuartzCore/QuartzCore.h>



@interface UIBubbleTableViewCell : UITableViewCell
@property (nonatomic, strong) NSBubbleData *data;
@property (nonatomic) BOOL showAvatar;
@property (nonatomic) BOOL deleteCell;
@property (nonatomic) BOOL deleteCell_selected;


@property (nonatomic, retain) UIView *customView;
@property (nonatomic, retain) UIImageView *bubbleImage;
@property (nonatomic, retain) UIImageView *avatarImage;
@property (nonatomic, retain) UIImageView *deleteImage;
@property (nonatomic, retain) UIImageView *impImage;


@property (nonatomic, retain) UILabel *dateLabel;
@property (nonatomic, retain) NSString *stringData;
@property (nonatomic, retain) UIImageView *deliveryView;




@end
