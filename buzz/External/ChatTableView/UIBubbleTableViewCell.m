//
//  singlechattableViewcell.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 23/05/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "UIBubbleTableViewCell.h"


@implementation UIBubbleTableViewCell


const UIEdgeInsets imageInsetsMine1 = {11, 13, 16, 22};
const UIEdgeInsets imageInsetsSomeone1 = {11, 21, 16, 14};


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        //[self setupInternalData];
        
    }
    return self;
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    
    [self setupInternalData];
}
- (void) setupInternalData
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
  
        [self.bubbleImage removeFromSuperview];

        self.bubbleImage = [[UIImageView alloc] init];
        [self.contentView addSubview:self.bubbleImage];

    NSBubbleType type = self.data.type;
    CGFloat width = self.data.view.frame.size.width;
    CGFloat height = self.data.view.frame.size.height;
    
    CGFloat x = (type == BubbleTypeSomeoneElse || type == BubbleTypeBeeSomeoneElse || type == BubbleTypePrivateSomeoneElse) ? 0 : self.frame.size.width - width - self.data.insets.left - self.data.insets.right;
    CGFloat y = 0;
    
    // Adjusting the x coordinate for avatar
    [self.avatarImage removeFromSuperview];
/*
    if (self.showAvatar)
    {
        [self.avatarImage removeFromSuperview];
        self.avatarImage = [[UIImageView alloc] initWithImage:(self.data.avatar ? self.data.avatar : [UIImage imageNamed:@"defaultPerson.png"])];
        self.avatarImage.layer.cornerRadius = 15.0;
        self.avatarImage.layer.masksToBounds = YES;
        self.avatarImage.layer.borderColor = [UIColor colorWithWhite:0.0 alpha:0.2].CGColor;
        self.avatarImage.layer.borderWidth = 1.0;
        
        CGFloat avatarX = (type == BubbleTypeSomeoneElse || type == BubbleTypeBeeSomeoneElse || type == BubbleTypePrivateSomeoneElse) ? 2 : self.frame.size.width - 32;
        CGFloat avatarY = -15;
        
        if (type == BubbleTypeMine || type == BubbleTypeBeeMine || type == BubbleTypePrivateMine)
        {
            avatarY = self.frame.size.height - 45;
        }
        self.avatarImage.frame = CGRectMake(avatarX, avatarY, 30, 30);
        [self addSubview:self.avatarImage];
        
        CGFloat delta = self.frame.size.height - (self.data.insets.top + self.data.insets.bottom + self.data.view.frame.size.height);
        if (delta > 0) y = delta-35;
        
        if (type == BubbleTypeSomeoneElse || type == BubbleTypeBeeSomeoneElse || type == BubbleTypePrivateSomeoneElse) x += 34;
        if (type == BubbleTypeMine || type == BubbleTypeBeeMine || type == BubbleTypePrivateMine) x -= 34;
    }
    */
    [self.deleteImage removeFromSuperview];

    if (self.deleteCell || self.deleteCell_selected)
    {
        [self.deleteImage removeFromSuperview];
        self.deleteImage = [[UIImageView alloc] init];
        if (self.deleteCell)
        {
            self.deleteImage.image = [UIImage imageNamed:@"check_U"];

        }
        if (self.deleteCell_selected)
        {
            self.deleteImage.image = [UIImage imageNamed:@"check_S"];

        }
        
        CGFloat avatarX = (type == BubbleTypeSomeoneElse || type == BubbleTypeBeeSomeoneElse || type == BubbleTypePrivateSomeoneElse) ? 12 : self.frame.size.width - 27;
        
        self.deleteImage.frame = CGRectMake(avatarX, (46 - 17)/2, 17, 17);
        [self addSubview:self.deleteImage];
        
        CGFloat delta = self.frame.size.height - (self.data.insets.top + self.data.insets.bottom + self.data.view.frame.size.height);
        if (delta > 0) y = delta-15;
        
        if (type == BubbleTypeSomeoneElse || type == BubbleTypeBeeSomeoneElse || type == BubbleTypePrivateSomeoneElse) x += 34;
        if (type == BubbleTypeMine || type == BubbleTypeBeeMine || type == BubbleTypePrivateMine) x -= 34;
    }
    [self.customView removeFromSuperview];
    self.customView = self.data.view;
   // self.customView.backgroundColor = [UIColor greenColor];
    self.customView.frame = CGRectMake(x + self.data.insets.left, y+self.data.insets.top, width, height);
    [self addSubview:self.customView];
    
    
    
    
    if (type == BubbleTypeSomeoneElse)
    {
        self.bubbleImage.image = [self balloonImageForReceiving];
    }
    else if (type == BubbleTypeBeeSomeoneElse)
    {
        UIImage *blank = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        
        self.bubbleImage.image = [blank resizableImageWithCapInsets:UIEdgeInsetsMake(20.0f,30.0f, 40.0f, 30.0f) resizingMode:UIImageResizingModeStretch];
        
    }
    else if (type == BubbleTypeMine)
    {
        self.bubbleImage.image = [self balloonImageForSending];
    }
    else if(type == BubbleTypeBeeMine)
    {
        UIImage *blank = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        self.bubbleImage.image = [blank resizableImageWithCapInsets:UIEdgeInsetsMake(17, 21, 16, 27) resizingMode:UIImageResizingModeStretch];
    }
    else if(type == BubbleTypePrivateMine)
    {
        self.bubbleImage.image = [self balloonImageForPrivateSending];
    }
    else if(type == BubbleTypePrivateSomeoneElse)
    {
        self.bubbleImage.image = [self balloonImageForPrivateReceiving];
    }
    self.bubbleImage.frame = CGRectMake(x, y, width + self.data.insets.left + self.data.insets.right, height + self.data.insets.top + self.data.insets.bottom + 8);
    
    [self.impImage removeFromSuperview];
    if (self.data.isImp)
    {
        self.impImage = [UIImageView new];
        [self addSubview:self.impImage];
        
        UIImage *starImg = [UIImage imageNamed:@"impMsg"];
        
        
        if (type == BubbleTypeSomeoneElse)
        {
            self.impImage.image = [UIImage imageNamed:@"impMsg"];

            self.impImage.frame = CGRectMake(self.bubbleImage.frame.origin.x+self.bubbleImage.frame.size.width - 20,self.bubbleImage.frame.origin.y - 5,starImg.size.width/2,starImg.size.height/2);
        }
        else
        {
            self.impImage.image = [UIImage imageNamed:@"impMsgFlip"];


            self.impImage.frame = CGRectMake(self.bubbleImage.frame.origin.x + ((self.bubbleImage.frame.size.width - 25)),self.bubbleImage.frame.origin.y - 10 ,starImg.size.width/2,starImg.size.height/2);
        }
        
        //star_icon
        
    }

    
    
    if (!self.dateLabel)
    {
        self.dateLabel = [UILabel new];
    }
    self.dateLabel.font = [UIFont fontWithName:FontString size:10];
    
    if (type == BubbleTypeSomeoneElse)
    {
        
        self.dateLabel.frame = CGRectMake(self.customView.frame.origin.x + self.customView.frame.size.width  - 50,self.customView.frame.origin.y+self.customView.frame.size.height,50,self.dateLabel.font.lineHeight);
        
        if (UIEdgeInsetsEqualToEdgeInsets(_data.insets, imageInsetsSomeone1))
        {
            self.dateLabel.frame = CGRectMake(self.customView.frame.origin.x + self.customView.frame.size.width + 30,self.customView.frame.origin.y+self.customView.frame.size.height,50,self.dateLabel.font.lineHeight);
        }
    }
    else
    {
        self.dateLabel.frame = CGRectMake(self.customView.frame.origin.x + self.customView.frame.size.width - 65,self.customView.frame.origin.y+self.customView.frame.size.height,50,self.dateLabel.font.lineHeight);
        if (UIEdgeInsetsEqualToEdgeInsets(_data.insets, imageInsetsMine1))
        {
            self.dateLabel.frame = CGRectMake(self.customView.frame.origin.x + self.customView.frame.size.width - 65,self.customView.frame.origin.y+self.customView.frame.size.height,50,self.dateLabel.font.lineHeight);

        }
    }
    
    self.dateLabel.backgroundColor = [UIColor clearColor];
    self.dateLabel.textColor = [UIColor whiteColor];
    self.dateLabel.text = [self dateToString:self.data.date];
    self.dateLabel.textAlignment = NSTextAlignmentCenter;
    
    [self.contentView addSubview:self.dateLabel];
    if (type == BubbleTypeMine || type == BubbleTypeBeeMine || type == BubbleTypePrivateMine) {
        
        
        self.deliveryView = [UIImageView new];
        self.deliveryView.layer.cornerRadius = 14/2;
        self.deliveryView.clipsToBounds = YES;
        _deliveryView.clearsContextBeforeDrawing = YES;
        [self.contentView addSubview:self.deliveryView];
        if ([self.data.deliveryStr isEqualToString:@"sent"])
        {
              self.deliveryView.frame = CGRectMake(self.dateLabel.frame.origin.x + self.dateLabel.frame.size.width , self.dateLabel.frame.origin.y, 14, 10);
            
            [self.deliveryView setImage:[self tintImage:[UIImage imageNamed:@"sending"] withColor:[UIColor whiteColor]]];
        }
        else if([self.data.deliveryStr isEqualToString:@"delivered"])
        {
              self.deliveryView.frame = CGRectMake(self.dateLabel.frame.origin.x + self.dateLabel.frame.size.width , self.dateLabel.frame.origin.y, 19, 10);
            [self.deliveryView setImage:[self tintImage:[UIImage imageNamed:@"delivery"] withColor:[UIColor whiteColor]]];
            
        }
        else
        {
              self.deliveryView.frame = CGRectMake(self.dateLabel.frame.origin.x + self.dateLabel.frame.size.width , self.dateLabel.frame.origin.y, 14, 10);
            [self.deliveryView setImage:[self tintImage:[UIImage imageNamed:@"pending"] withColor:[UIColor darkGrayColor]]];
        }
        
        
    }
}
- (void)prepareForReuse {
    [super prepareForReuse];
    for(UIView *subview in [self.contentView subviews]) {
        [subview removeFromSuperview];
    }
}
-(void)dealloc
{
    self.dateLabel = nil;
    self.data = nil;
    self.customView = nil;
    self.bubbleImage = nil;
    self.avatarImage = nil;
    self.deliveryView = nil;
    self.stringData = nil;
}

- (UIImage *)balloonImageForReceiving
{
    UIImage *bubble = [UIImage imageNamed:@"bubbleReceive.png"];
    UIColor *color = [UIColor lightGrayColor];
    bubble = [self tintImage:bubble withColor:color];
    
    return [bubble resizableImageWithCapInsets:UIEdgeInsetsMake(17, 27, 21, 17) resizingMode:UIImageResizingModeStretch];
}

- (UIImage *)balloonImageForSending
{
    UIImage *bubble = [UIImage imageNamed:@"bubblenew.png"];
    UIColor *color = [UIColor colorWithRed:0.0/255.0 green:150.0/255.0 blue:255.0/255.0 alpha:1.0];
    bubble = [self tintImage:bubble withColor:color];
    return [bubble resizableImageWithCapInsets:UIEdgeInsetsMake(17, 21, 16, 27) resizingMode:UIImageResizingModeStretch];
}
- (UIImage *)balloonImageForPrivateReceiving
{
    UIImage *bubble = [UIImage imageNamed:@"bubbleReceive.png"];
    UIColor *color = [UIColor colorWithRed:89.0/255.0 green:222.0/255.0 blue:9.0/255.0 alpha:1.0];
    bubble = [self tintImage:bubble withColor:color];
    return [bubble resizableImageWithCapInsets:UIEdgeInsetsMake(17, 27, 21, 17) resizingMode:UIImageResizingModeStretch];
}

- (UIImage *)balloonImageForPrivateSending
{
    UIImage *bubble = [UIImage imageNamed:@"bubblenew.png"];
    UIColor *color = [UIColor colorWithRed:0.0/255.0 green:150.0/255.0 blue:255.0/255.0 alpha:1.0];
    bubble = [self tintImage:bubble withColor:color];
    return [bubble resizableImageWithCapInsets:UIEdgeInsetsMake(17, 21, 16, 27) resizingMode:UIImageResizingModeStretch];
}
- (UIImage *)tintImage:(UIImage *)image withColor:(UIColor *)color
{
    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, image.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    CGContextClipToMask(context, rect, image.CGImage);
    [color setFill];
    CGContextFillRect(context, rect);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


-(NSString *)dateToString:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [   formatter setDateFormat:@"hh:mma"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    return stringFromDate;
}
@end
