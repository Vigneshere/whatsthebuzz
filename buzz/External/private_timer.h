//
//  private_timer.h
//  buzz
//
//  Created by shaili_solutions_MacPro3 on 13/06/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Singleton.h"
#import "privateChatDbEditer.h"

@protocol privateTimerDelegate <NSObject>

-(void)timerDead:(NSString*)ID;
@end

@interface private_timer : NSObject
{
}
@property (strong, nonatomic) Singleton *singleton;
@property (nonatomic, weak) id<privateTimerDelegate> delegate;

-(void)createTimer_id:(NSString*)user_id seconds:(float)seconds;

@end
