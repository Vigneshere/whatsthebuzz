//
//  PasswordPin.h
//  buzz
//
//  Created by shaili_solutions_MacPro3 on 26/06/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PasswordPin : UIView<UITextFieldDelegate>
{
    UITextField* hiddenPwdTxtFld;
    UIView* PassCodeVW;
}

@property (strong, nonatomic) UIView *onView;
@property(nonatomic,copy) NSString *lblString;
@property(nonatomic,copy) NSString *pwdString;
@property(nonatomic,copy) NSString *JIDStr;
@property(nonatomic,copy) NSString *modeType;


- (id)initWithFrame_onView:(UIView*)onview LabelString:(NSString*)lblStr password:(NSString*)pwdStr Hint:(NSString *)mode JID:(NSString *)JIDStr;
- (id)initWithFrame_onView:(UIView*)onview LabelString:(NSString*)lblStr password:(NSString*)pwdStr;

-(void)createPasswordVw;
@end


