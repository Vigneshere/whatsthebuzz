//
//  FileInfo.h
//  buzz
//
//  Created by shaili_macMini02 on 15/05/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface FileInfo : NSManagedObject

@property (nonatomic, retain) NSString * msgId;
@property (nonatomic, retain) NSString * userId;
@property (nonatomic, retain) NSString * fileName;
@property (nonatomic, retain) NSString * url;
@property (nonatomic) BOOL loc_cloud;
@property (nonatomic) BOOL isPrivate;
@property (nonatomic) BOOL sent_received;
@property (nonatomic, retain) NSString* success_failure;
@property (nonatomic) BOOL sender_receiver;
@property (nonatomic, retain) NSString * video_img;
@property (nonatomic, retain) NSString * imgData;
@property (nonatomic) float  percentLoaded;
@end
