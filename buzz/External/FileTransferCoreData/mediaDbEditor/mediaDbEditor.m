//
//  mediaDbEditor.m
//  buzz
//
//  Created by shaili_macMini02 on 15/05/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "mediaDbEditor.h"
#import "FileInfo.h"



//loc_cloud        0 --> localUrl     1 -- > webUrl
//sent_received    0 --> Sent         1 -- > Received
//success_failure  0 --> pending      1 -- > uploading/downloading  2 --> success  3 --> failure
//sender_receiver  0 --> sender       1 -- > receiver
//video_img        0 --> video        1 -- > img


@implementation mediaDbEditor
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;


+(mediaDbEditor *)sharedInstance
{
    static mediaDbEditor *_sharedInstance = nil;
    static dispatch_once_t dispatchOnce;
    dispatch_once(&dispatchOnce, ^{
        _sharedInstance = [[mediaDbEditor alloc] init];
    });
    return _sharedInstance;
    
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Media check And Insert Value
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(BOOL)checkAndInsert:(NSMutableDictionary*)fileInfoDict
{
    
    if ([fileInfoDict objectForKey:@"msgId"])
    {
        NSManagedObjectContext *context = [self managedObjectContext];
        
        // to insert
        
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"FileInfo"
                                                   inManagedObjectContext:context];
        [request setEntity:entity1];
        
        NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(msgId = \"%@\")",[fileInfoDict objectForKey:@"msgId"]]];
        [request setPredicate:pred];
        
        NSArray *empArray=[self.managedObjectContext executeFetchRequest:request error:nil];
        if (![empArray count]) {
           return [self insertMedia:fileInfoDict];
        }
        else
        {
           return  [self updateMedia:fileInfoDict];
        }

    }
    else
    {
        return NO;
    }
    
}
-(BOOL)checkAndInsertGroup:(NSMutableDictionary*)fileInfoDict
{
    
    if ([fileInfoDict objectForKey:@"msgId"])
    {
        NSManagedObjectContext *context = [self managedObjectContext];
        
        // to insert
        
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"FileInfo"
                                                   inManagedObjectContext:context];
        [request setEntity:entity1];
        
        NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(msgId = \"%@\")",[fileInfoDict objectForKey:@"msgId"]]];
        [request setPredicate:pred];
        
        NSArray *empArray=[self.managedObjectContext executeFetchRequest:request error:nil];
        if (![empArray count]) {
            return [self insertMedia:fileInfoDict];
        }
        else
        {
            return  NO;
        }
        
    }
    else
    {
        return NO;
    }
    
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Media Core Data UpdateValue
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(BOOL)updateMedia:(NSMutableDictionary*)fileInfoDict
{
  //  NSLog(@"%@",fileInfoDict);
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // to insert
    
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"FileInfo"
                                               inManagedObjectContext:context];
    [request setEntity:entity1];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(msgId = \"%@\")",[fileInfoDict objectForKey:@"msgId"]]];
    [request setPredicate:pred];
    
    NSArray *empArray=[self.managedObjectContext executeFetchRequest:request error:nil];
    
     for (FileInfo *file_Info in empArray)
    {
        if ([fileInfoDict objectForKey:@"msgId"]) {
            file_Info.msgId = [fileInfoDict objectForKey:@"msgId"];
        }
        
        if ([fileInfoDict objectForKey:@"isPrivate"]) {
            file_Info.isPrivate = [[fileInfoDict objectForKey:@"isPrivate"] boolValue];
        }

        
        if ([fileInfoDict objectForKey:@"userId"]) {
            file_Info.userId = [fileInfoDict objectForKey:@"userId"];
        }
        
        if ([fileInfoDict objectForKey:@"fileName"]) {
            file_Info.fileName = [fileInfoDict objectForKey:@"fileName"];
        }
        
        if ([fileInfoDict objectForKey:@"url"]) {
            file_Info.url = [fileInfoDict objectForKey:@"url"];
        }
        
        if ([fileInfoDict objectForKey:@"loc_cloud"]) {
            file_Info.loc_cloud = [[fileInfoDict objectForKey:@"loc_cloud"] boolValue];
        }
        
        if ([fileInfoDict objectForKey:@"sent_received"]) {
            file_Info.sent_received = [[fileInfoDict objectForKey:@"sent_received"] boolValue];
        }
        
        if ([fileInfoDict objectForKey:@"success_failure"]) {
            file_Info.success_failure = [fileInfoDict objectForKey:@"success_failure"];
        }
        
        if ([fileInfoDict objectForKey:@"sender_receiver"]) {
            file_Info.sender_receiver = [[fileInfoDict objectForKey:@"sender_receiver"] boolValue];
        }
        
        if ([fileInfoDict objectForKey:@"percentLoaded"]) {
            file_Info.percentLoaded = [[fileInfoDict objectForKey:@"percentLoaded"] floatValue];
        }
        if ([fileInfoDict objectForKey:@"video_img"]) {
            file_Info.video_img = [fileInfoDict objectForKey:@"video_img"];
        }
        
        if ([fileInfoDict objectForKey:@"imgData"]) {
            file_Info.imgData = [fileInfoDict objectForKey:@"imgData"];
        }
        
        
        
    }



NSError *error;
if (![context save:&error]) {
    NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    return NO;
}
return YES;
    
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Media Core Data InsertValue
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(BOOL)insertMedia:(NSMutableDictionary*)fileInfoDict
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    FileInfo *file_Info = [NSEntityDescription
                           insertNewObjectForEntityForName:@"FileInfo"
                           inManagedObjectContext:context];
    
    if ([fileInfoDict objectForKey:@"msgId"]) {
        file_Info.msgId = [fileInfoDict objectForKey:@"msgId"];
    }
    
    if ([fileInfoDict objectForKey:@"isPrivate"]) {
        file_Info.isPrivate = [[fileInfoDict objectForKey:@"isPrivate"] boolValue];
    }
    if ([fileInfoDict objectForKey:@"userId"]) {
        file_Info.userId = [fileInfoDict objectForKey:@"userId"];
    }
    
    if ([fileInfoDict objectForKey:@"fileName"]) {
        file_Info.fileName = [fileInfoDict objectForKey:@"fileName"];
    }
    
    if ([fileInfoDict objectForKey:@"url"]) {
        file_Info.url = [fileInfoDict objectForKey:@"url"];
    }
    
    if ([fileInfoDict objectForKey:@"loc_cloud"]) {
        file_Info.loc_cloud = [[fileInfoDict objectForKey:@"loc_cloud"] boolValue];
    }
    
    if ([fileInfoDict objectForKey:@"sent_received"]) {
        file_Info.sent_received = [[fileInfoDict objectForKey:@"sent_received"]  boolValue];
    }
    
    if ([fileInfoDict objectForKey:@"success_failure"]) {
        file_Info.success_failure = [fileInfoDict objectForKey:@"success_failure"];
    }
    
    if ([fileInfoDict objectForKey:@"sender_receiver"]) {
        file_Info.sender_receiver = [[fileInfoDict objectForKey:@"sender_receiver"]  boolValue];
    }
    
    if ([[fileInfoDict objectForKey:@"percentLoaded"] floatValue]) {
        file_Info.percentLoaded = [[fileInfoDict objectForKey:@"percentLoaded"] floatValue];
    }
    if ([fileInfoDict objectForKey:@"video_img"]) {
        file_Info.video_img = [fileInfoDict objectForKey:@"video_img"];
    }
    
    if ([fileInfoDict objectForKey:@"imgData"]) {
        file_Info.imgData = [fileInfoDict objectForKey:@"imgData"];
    }

    
    NSError *error;
    if (![context save:&error]) {
      //  NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        return NO;
    }
    [self fetchMedia:[fileInfoDict objectForKey:@"msgId"]];
    return YES;
    
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Media Core Data DeleteValue
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(BOOL)deleteMedia:(NSString*)msgId
{
    
   
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSFetchRequest *deleterequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *delentity = [NSEntityDescription entityForName:@"FileInfo"
                                                 inManagedObjectContext:context];
    [deleterequest setEntity:delentity];
    
    
    NSPredicate *delpred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"msgId = \"%@\"",msgId]];
    [deleterequest setPredicate:delpred];
    
    
    NSArray *delArray=[self.managedObjectContext executeFetchRequest:deleterequest error:nil];
    
    
    for (FileInfo *file_status in delArray)
    {
        
        
        [context deleteObject:file_status];
        [context save:nil];
        
        NSError *error;
        
        if (![context save:&error]) {
          //  NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            return NO;
        }
        
    }
    return YES;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Media Core Data Fetch DB Value
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(NSMutableArray*)fetchMedia
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSFetchRequest *fetchrequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *fetchentity = [NSEntityDescription entityForName:@"FileInfo"
                                                    inManagedObjectContext:context];
    [fetchrequest setEntity:fetchentity];
    
    
    
    
    NSArray *fetchArray=[self.managedObjectContext executeFetchRequest:fetchrequest error:nil];
    
    
    NSMutableArray* mediaDb = [NSMutableArray new];
    
    for (FileInfo *fileInfo in fetchArray)
    {
         NSMutableDictionary* dict = [NSMutableDictionary new];
        [dict setObject:fileInfo.msgId forKey:@"msgId"];
        [dict setObject:fileInfo.userId forKey:@"userId"];
        [dict setObject:fileInfo.fileName forKey:@"fileName"];
        [dict setObject:fileInfo.url forKey:@"url"];
        [dict setObject:[NSNumber numberWithBool:fileInfo.loc_cloud] forKey:@"loc_cloud"];
        [dict setObject:[NSNumber numberWithBool:fileInfo.sent_received] forKey:@"sent_received"];
        [dict setObject:fileInfo.success_failure forKey:@"success_failure"];
        [dict setObject:[NSNumber numberWithBool:fileInfo.sender_receiver] forKey:@"sender_receiver"];
        [dict setObject:[NSNumber numberWithFloat:fileInfo.percentLoaded] forKey:@"percentLoaded"];
        [dict setObject:fileInfo.video_img forKey:@"video_img"];
        [dict setObject:fileInfo.imgData forKey:@"imgData"];
        
        [mediaDb addObject:dict];
        
    }
    
    return mediaDb;
   
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Media Core Data Fetch media info with msg id
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(NSMutableDictionary*)fetchMedia:(NSString*)msgId
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSFetchRequest *fetchrequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *fetchentity = [NSEntityDescription entityForName:@"FileInfo"
                                                   inManagedObjectContext:context];
    [fetchrequest setEntity:fetchentity];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(msgId = \"%@\")",msgId]];
    [fetchrequest setPredicate:pred];
    
    
    NSArray *fetchArray=[self.managedObjectContext executeFetchRequest:fetchrequest error:nil];
    
    
     NSMutableDictionary* dict = [NSMutableDictionary new];
    
    for (FileInfo *fileInfo in fetchArray)
    {
        if (fileInfo.msgId) {
            [dict setObject:fileInfo.msgId forKey:@"msgId"];
        }
        
        if (fileInfo.userId) {
           [dict setObject:fileInfo.userId forKey:@"userId"];

        }
        if (fileInfo.userId) {
            [dict setObject:fileInfo.fileName forKey:@"fileName"];
        }
        
        
        if (fileInfo.url) {
            [dict setObject:fileInfo.url forKey:@"url"];
            
        }
        
        if (fileInfo.loc_cloud) {
            [dict setObject:[NSNumber numberWithBool:fileInfo.loc_cloud] forKey:@"loc_cloud"];
        }
        
        if (fileInfo.sent_received) {
             [dict setObject:[NSNumber numberWithBool:fileInfo.sent_received] forKey:@"sent_received"];
        }
        
        if (fileInfo.success_failure) {
             [dict setObject:fileInfo.success_failure forKey:@"success_failure"];
        }
        
        if (fileInfo.sender_receiver) {
            [dict setObject:[NSNumber numberWithBool:fileInfo.sender_receiver] forKey:@"sender_receiver"];

        }
        
        if (fileInfo.percentLoaded) {
             [dict setObject:[NSNumber numberWithFloat:fileInfo.percentLoaded] forKey:@"percentLoaded"];
        }
        
        if (fileInfo.video_img) {
             [dict setObject:fileInfo.video_img forKey:@"video_img"];
        }
        
        if (fileInfo.imgData) {
            [dict setObject:fileInfo.imgData forKey:@"imgData"];
        }
        
               
    }
    
    return dict;
    
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Core Data saveContext
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         //   NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
	// This is a private method.
	//
	// NSManagedObjectContext is NOT thread-safe.
	// Therefore it is VERY VERY BAD to use our private managedObjectContext outside our private storageQueue.
	//
	// You should NOT remove the assert statement below!
	// You should NOT give external classes access to the storageQueue! (Excluding subclasses obviously.)
	//
	// When you want a managedObjectContext of your own (again, excluding subclasses),
	// you can use the mainThreadManagedObjectContext (below),
	// or you should create your own using the public persistentStoreCoordinator.
	//
	// If you even comtemplate ignoring this warning,
	// then you need to go read the documentation for core data,
	// specifically the section entitled "Concurrency with Core Data".
	//
	//
	// Do NOT remove the assert statment above!
	// Read the comments above!
	//
    
//    NSAssert([NSThread isMainThread], @"Context reserved for main thread only");

	
	if (_managedObjectContext)
	{
		return _managedObjectContext;
	}
	
	NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
	if (coordinator)
	{
		
		
		if ([NSManagedObjectContext instancesRespondToSelector:@selector(initWithConcurrencyType:)])
			_managedObjectContext =
            [[NSManagedObjectContext alloc] initWithConcurrencyType:NSConfinementConcurrencyType];
		else
			_managedObjectContext = [[NSManagedObjectContext alloc] init];
		
		_managedObjectContext.persistentStoreCoordinator = coordinator;
		_managedObjectContext.undoManager = nil;
		
		
	}
	
	return _managedObjectContext;
}



// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"FileTrasferCoreData" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"FileTrasferCoreData.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
      //  NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end

