//
//  mediaDbEditor.h
//  buzz
//
//  Created by shaili_macMini02 on 15/05/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface mediaDbEditor : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+(mediaDbEditor *)sharedInstance;


//pragma mark check And Insert File Info
-(BOOL)checkAndInsert:(NSMutableDictionary*)fileInfoDict;

//pragma mark Insert With File Info
-(BOOL)insertMedia:(NSMutableDictionary*)fileInfoDict;

//pragma mark update Media File Info
-(BOOL)updateMedia:(NSMutableDictionary*)fileInfoDict;

-(BOOL)checkAndInsertGroup:(NSMutableDictionary*)fileInfoDict;
//pragma mark fetchMedia File Info
-(NSMutableArray*)fetchMedia;

//pragma mark fetchMedia with msg Id
-(NSMutableDictionary*)fetchMedia:(NSString*)msgId;

//pragma mark deleteMedia with msg Id
-(BOOL)deleteMedia:(NSString*)msgId;


@end
