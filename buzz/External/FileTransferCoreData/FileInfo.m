//
//  FileInfo.m
//  buzz
//
//  Created by shaili_macMini02 on 15/05/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "FileInfo.h"

//loc_cloud        0 --> localUrl     1 -- > webUrl
//sent_received    0 --> Sent         1 -- > Received
//success_failure  0 --> pending      1 -- > uploading/downloading  2 --> success  3 --> failure
//sender_receiver  0 --> sender       1 -- > receiver
//video_img        0 --> video        1 -- > img


@implementation FileInfo

@dynamic msgId;
@dynamic userId;
@dynamic fileName;
@dynamic url;
@dynamic loc_cloud;
@dynamic sent_received;
@dynamic success_failure;
@dynamic sender_receiver;
@dynamic video_img;
@dynamic imgData;
@dynamic percentLoaded;
@dynamic isPrivate;

@end
