//
//  sosAllContacts.m
//  buzz
//
//  Created by shaili_solutions_MacPro3 on 25/08/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "sosAllContacts.h"
#import "ContactCell.h"
#import "contactData.h"
#import "sosAllContactCellTableViewCell.h"
#import "sosContactObject.h"

@implementation sosAllContacts
@synthesize delegate;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Appdelegate Values
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (AppDelegate *)appDelegate
{
	return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Initialization
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        UILabel* label = [UILabel new];
        label.frame = CGRectMake(0, 30, self.frame.size.width, 25);
        label.text = @"Select Contact";
        label.font = [UIFont fontWithName:FontString size:21.0f];
        label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:label];
        
        UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        doneBtn.frame = CGRectMake(10,30,54,25);
        [doneBtn setTitleColor:RGBA(53, 152, 219, 1) forState:UIControlStateNormal];
        doneBtn.layer.borderColor = RGBA(53, 152, 219, 1).CGColor;
        doneBtn.layer.borderWidth = 1.2f;
        doneBtn.layer.cornerRadius = 25/2;
        doneBtn.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
        [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
        doneBtn.titleLabel.font = [UIFont fontWithName:FontString size:14.0f];
        [doneBtn addTarget:self action:@selector(doneBtnCliked) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:doneBtn];
        
        //-->Search Bar for TableView
        _searchBar = [UISearchBar new];
        _searchBar.delegate = self;
        _searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
        _searchBar.backgroundColor = [UIColor clearColor];
        _searchBar.placeholder = @"Search";
        _searchBar.tag = 1;
        _searchBar.tintColor = RGBA(53, 152, 219, 1);
        _searchBar.backgroundImage = [UIImage imageWithColor:[UIColor clearColor]];
        [_searchBar sizeToFit];

        CGRect barFrame = self.searchBar.frame;
        barFrame.origin.y = doneBtn.frame.origin.y + doneBtn.frame.size.height + 5;
        barFrame.origin.x = 10;
        barFrame.size.width = self.frame.size.width - 20;
        self.searchBar.frame = barFrame;
        
        self.allContacts = [UITableView new];
        self.allContacts.frame = CGRectMake(0, _searchBar.frame.origin.y + 44, self.frame.size.width, self.frame.size.height-(_searchBar.frame.origin.y + _searchBar.frame.size.height));
        self.allContacts.delegate = self;
        self.allContacts.dataSource = self;
        self.allContacts.backgroundColor = [UIColor clearColor];
        self.allContacts.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.allContacts.showsVerticalScrollIndicator  = NO;
        [self addSubview: self.allContacts];


        
        NSString *ver = [[UIDevice currentDevice] systemVersion];
        int ver_int = [ver intValue];
        
        if (ver_int < 7) {
            _searchBar.barStyle = UIBarStyleDefault;
            [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setFont:[UIFont fontWithName:FontString size:18]];

        }
        
        else {
            [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setDefaultTextAttributes:@{
                                                                                                         NSFontAttributeName: [UIFont fontWithName:FontString size:18],
                                                                                                         }];
            _searchBar.searchBarStyle = UISearchBarStyleProminent;
            _searchBar.barTintColor =[UIColor clearColor];
        }
        
        [self addSubview: _searchBar];
        
        
        
        
        
        self.searchAry = [NSMutableArray new];
        self.searchSecAry = [NSMutableArray new];
 self.visibleContactsDict = [NSMutableDictionary new];
        
        isSearching = NO;
        
        
        
        
    }
    return self;
}
-(void)setNeedsLayout
{
    
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Done Button Action in Contact TableView
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(void)doneBtnCliked
{
    if (delegate) {
        
        [delegate doneBtnCliked:self.SelectedContactArray];
    }
    
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark popup Done Button Action in Contact detail view
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(void)PopUpdoneBtnCliked:(contactData*)contact
{
    contactPredicate = [NSPredicate predicateWithFormat:@"uniqueId == %@", contact.uniqueId];
    if (![[NSMutableArray arrayWithArray:[self.SelectedContactArray filteredArrayUsingPredicate:contactPredicate]] count])
    {
        sosContactObject* tempContact = [sosContactObject new];
        tempContact.name = [NSString stringWithFormat:@"%@ %@",contact.firstNames,contact.lastNames];
        tempContact.selectedNumber = contact.selectedNumber;
        tempContact.selectedEmail = contact.selectedEmail;
        tempContact.uniqueId = contact.uniqueId;
        
        [self.SelectedContactArray addObject:tempContact];
        
    }
    [self.allContacts reloadData];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - TableView DataSource Method
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    [self.visibleContactsDict removeAllObjects];
    
    if (isSearching == YES) {
        return self.searchSecAry.count;
    }
    else
        return self.contactSecArray.count;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView=[[UIView alloc]initWithFrame:CGRectMake(0,0,320,23)];
    headerView.backgroundColor=RGBA(191, 211, 225, 0.8);
    
    UILabel *headerLabel=[[UILabel alloc]initWithFrame:CGRectMake(15,0,320,23)];
    headerLabel.backgroundColor=[UIColor clearColor];
    headerLabel.textAlignment = NSTextAlignmentLeft;
    headerLabel.font = [UIFont fontWithName:FontString size:18];
    headerLabel.textColor = [UIColor blackColor];
    if (isSearching == YES) {
        headerLabel.text = [self.searchSecAry objectAtIndex:section];
    }
    else
        headerLabel.text = [self.contactSecArray objectAtIndex:section];
    [headerView addSubview:headerLabel];
    
    return headerView;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSMutableArray* tempAry ;
    int rowCount;
    if (isSearching == YES) {
        
        
        tempAry = [NSMutableArray arrayWithArray:[self.searchAry filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"firstLetter == %@", [self.searchSecAry objectAtIndex:section]]]];
        rowCount = (int)tempAry.count;
        [self.visibleContactsDict setObject:tempAry forKey:[self.searchSecAry objectAtIndex:section]];
        tempAry = nil;
        
    }
    else
    {
        tempAry = [NSMutableArray arrayWithArray:[self.contactArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"firstLetter == %@", [self.contactSecArray objectAtIndex:section]]]];
        [self.visibleContactsDict setObject:tempAry forKey:[self.contactSecArray objectAtIndex:section]];

    rowCount = (int)tempAry.count;
        tempAry = nil;
    }
    
    return  rowCount;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - TableView DataSource cellForRowAtIndexPath
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    sosAllContactCellTableViewCell *cell = (sosAllContactCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[sosAllContactCellTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    
    
     contactData *data;
    NSMutableArray* tempAry;
    
     #pragma mark - CHECK IF ON SEARCHING MODE
    if (isSearching == YES) {
        tempAry = [self.visibleContactsDict objectForKey:[self.searchSecAry objectAtIndex:indexPath.section]];
        data = [tempAry objectAtIndex:indexPath.row];
        

    }
    else
    {
        tempAry = [self.visibleContactsDict objectForKey:[self.contactSecArray objectAtIndex:indexPath.section]];
        data = [tempAry objectAtIndex:indexPath.row];

    }
    
     #pragma mark -  HIDING UNRELATED DATA
    cell.appUsersLbl.hidden = YES;
    cell.selIcon.hidden = YES;
    
    
     #pragma mark -  CHECK IF CONTACT CONTAINS IM DATA
    if ([data.IMAry count])
     {
       cell.appUsersLbl.hidden = NO;
        
        UIFont *font = [UIFont fontWithName:FontString size:11];
        
        CGRect textRect = [(@"Buzz!" ? @"Buzz!" : @"") boundingRectWithSize:CGSizeMake(320, 9999)
                                                                                        options:NSStringDrawingUsesLineFragmentOrigin
                                                                                     attributes:@{NSFontAttributeName:font}
                                                                                        context:nil];
        cell.appUsersLbl .frame = CGRectMake(cell.selIcon.frame.origin.x - (textRect.size.width+23) , cell.appUsersLbl .frame.origin.y, textRect.size.width+20, cell.appUsersLbl .frame.size.height);
        
        cell.contactNameLbl.frame =CGRectMake(15, 0, cell.appUsersLbl.frame.origin.x - 15, cell.contactNameLbl.frame.size.height);
     }
    else
    {
        cell.contactNameLbl.frame =CGRectMake(15, 0, cell.selIcon.frame.origin.x - 15, cell.contactNameLbl.frame.size.height);
    }
    
    
    contactPredicate = [NSPredicate predicateWithFormat:@"uniqueId == %@", data.uniqueId];
    if ([[NSMutableArray arrayWithArray:[self.SelectedContactArray filteredArrayUsingPredicate:contactPredicate]] count])
    {
        cell.selIcon.hidden = NO;

    }
    
     #pragma mark -  CHECK IF NAME IS EMPTY
    cell.contactNameLbl.text = [NSString stringWithFormat:@"%@  %@",data.firstNames,data.lastNames];
    
    NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
    NSString *trimmedString = [ cell.contactNameLbl.text stringByTrimmingCharactersInSet:charSet];
    
    if ([trimmedString isEqualToString:@""]) {
         cell.contactNameLbl.text = @"No Name";
    }
    
    
    
    
    #pragma mark - the last row in section of tableView.
    NSInteger totalRow = [tableView numberOfRowsInSection:indexPath.section];
    if(indexPath.row == totalRow -1){
        cell.line.hidden = YES;
    }
    else
    {
        cell.line.hidden = NO;
    }
    
    return cell;
}

#pragma mark - Reload All Contact Table

-(void)reloadAllContactTable
{
    [self.allContacts reloadData];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Search Implementation
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
    
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    //Remove all objects first.
    if([searchText length] != 0) {
        isSearching = YES;
        [self searchTableList];
    }
    else
        isSearching = NO;
    [self.allContacts reloadData];
    [searchBar becomeFirstResponder];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    isSearching = NO;
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
    [self.allContacts reloadData];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self searchTableList];
    [self.allContacts reloadData];
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
    
}

//Search the array
- (void)searchTableList {
    [self.searchAry removeAllObjects];
    NSString *searchString = _searchBar.text;
    contactPredicate = [NSPredicate predicateWithFormat:@"firstNames CONTAINS[cd] %@ OR lastNames CONTAINS[cd] %@", searchString,searchString];
    self.searchAry = [NSMutableArray arrayWithArray:[self.contactArray filteredArrayUsingPredicate:contactPredicate]];
    self.searchSecAry = [self.searchAry valueForKeyPath:@"@distinctUnionOfObjects.firstLetter"];

}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - TableView Delegate didSelectRowAtIndexPath
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    contactData *data;
     NSMutableArray* tempAry;
    
    
    if (isSearching == YES) {
        tempAry = [self.visibleContactsDict objectForKey:[self.searchSecAry objectAtIndex:indexPath.section]];
        data = [tempAry objectAtIndex:indexPath.row];
        [self resignFirstResponder];

        
       
    }
    else
    {
        tempAry = [self.visibleContactsDict objectForKey:[self.contactSecArray objectAtIndex:indexPath.section]];
        data = [tempAry objectAtIndex:indexPath.row];

       
    }
    
    contactPredicate = [NSPredicate predicateWithFormat:@"uniqueId == %@", data.uniqueId];
    if ([[NSMutableArray arrayWithArray:[self.SelectedContactArray filteredArrayUsingPredicate:contactPredicate]] count])
    {
        for (sosContactObject* sosContact in[NSMutableArray arrayWithArray:[self.SelectedContactArray filteredArrayUsingPredicate:contactPredicate]]) {
            data.selectedEmail = @"";
            data.selectedNumber = @"";
            [self.SelectedContactArray removeObject:sosContact];
        }
        
        [self.allContacts reloadData];
    }
    else
    {
    
        if (self.SelectedContactArray.count >= 5)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ALERTVIEW_TITLE message:@"Maximum 5 members only!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            alert = nil;
        }
        else
        {
    sosContactPopUp* contactPopUp;
    
    contactPopUp = [[sosContactPopUp alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height) view:self.superview contactData:data];
    contactPopUp.delegate = self;
            
        }
    }
    
    [_searchBar resignFirstResponder];
    
     }



-(NSString*)checkPhoneNum:(NSString*)number
{
    if (number.length == 10)
    {
        number = [NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"],number];
        return number;
    }
    else if (number.length == 11)
    {
        NSString* subNumber=[number substringToIndex:1];
        if ([subNumber isEqualToString:@"0"])
        {
            NSString *newStr = [number substringWithRange:NSMakeRange(1, [number length]-1)];
            number =  [NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"],newStr];
            return number;
        }
        else
        {
            return nil;
        }
    }
    else
    {
        return number;
    }
}

@end
