//
//  sosContactPopUpCellTableViewCell.h
//  buzz
//
//  Created by shaili_solutions_MacPro3 on 26/08/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface sosContactPopUpCell : UITableViewCell

@property(nonatomic,retain)UILabel *contactNameLbl;
@property(nonatomic,retain)UIView *SelectIcon;
@property(nonatomic,retain)UIImageView *appIcon;

@end
