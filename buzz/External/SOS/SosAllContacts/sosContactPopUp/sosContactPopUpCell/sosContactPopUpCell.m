//
//  sosContactPopUpCellTableViewCell.m
//  buzz
//
//  Created by shaili_solutions_MacPro3 on 26/08/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "sosContactPopUpCell.h"

@implementation sosContactPopUpCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier

{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        // Initialization code
        self.contactNameLbl = [UILabel new];
        self.SelectIcon = [UIView new];
        self.appIcon = [UIImageView new];

        
        [self.contentView addSubview:self.contactNameLbl];
        [self.contentView addSubview:self.SelectIcon];
        [self.contentView addSubview:self.appIcon];

        
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    UIImage* appIcon = [UIImage imageNamed:@"sosAppIcon"];
    
    self.appIcon.frame = CGRectMake(5, (self.frame.size.height - appIcon.size.height)/2, appIcon.size.width, appIcon.size.height);
    self.appIcon.image = appIcon;
    
    self.contactNameLbl.frame =CGRectMake(self.appIcon.frame.origin.x +self.appIcon.frame.size.width + 5 , 0, 220 - (self.appIcon.frame.origin.x +self.appIcon.frame.size.width + 5), self.frame.size.height);
    self.contactNameLbl.font = [UIFont fontWithName:FontString size:16];
    self.contactNameLbl.textColor  = [UIColor blackColor];
    self.contactNameLbl.backgroundColor = [UIColor clearColor];
    
    self.SelectIcon.frame =CGRectMake(245, (self.frame.size.height - 16)/2, 16, 16);
    self.SelectIcon.backgroundColor = [UIColor whiteColor];
    self.SelectIcon.layer.borderColor = RGBA(177, 177, 177, 1).CGColor;
    self.SelectIcon.layer.borderWidth = 1;
    
   
    
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


@end
