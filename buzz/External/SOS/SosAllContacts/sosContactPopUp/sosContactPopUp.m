//
//  sosContactPopUp.m
//  buzz
//
//  Created by shaili_solutions_MacPro3 on 26/08/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "sosContactPopUp.h"
#import "sosContactPopUpCell.h"

@implementation sosContactPopUp
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame view:(UIView *)view contactData:(contactData*)ContactData
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = RGBA(0, 0, 0, 0.0);
        
        contactName = [NSString stringWithFormat:@"%@ %@",ContactData.firstNames,ContactData.lastNames];
        contactAry = ContactData.phoneNumbers;
        emailAry = ContactData.emailIds;
        IMAry = ContactData.IMAry;
        baseView = view;
        contData = ContactData;
        
       tempSelNumber = contData.selectedNumber;
       tempSelEmail =  contData.selectedEmail;
        
        headerLbl = [UILabel new];
        tapVw = [UIView new];
        
        mainBox= [UIView new];
        phoneNumberTable = [UITableView new];
        emailTable = [UITableView new];
      lineVw = [UIView new];
        doneBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        [self createVw];
        
        }
    return self;
}

-(void)createVw
{
    tapVw.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    [self addSubview:tapVw];
    
    UITapGestureRecognizer *tapPress = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPress:)];
    [tapVw addGestureRecognizer:tapPress];
    
    mainBox.frame = CGRectMake(20, 0, self.frame.size.width - 40, 0);
    mainBox.backgroundColor = RGBA(255, 255, 255, 0.9);
    mainBox.layer.cornerRadius = 15;
    [self addSubview:mainBox];
    
    headerLbl.frame = CGRectMake(0, 5, mainBox.frame.size.width, 35);
    headerLbl.text  = contactName;
    headerLbl.textAlignment = NSTextAlignmentCenter;
    headerLbl.font = [UIFont fontWithName:FontString size:18];
    headerLbl.textColor = RGBA(0, 104, 174, 1);
    [mainBox addSubview:headerLbl];
    
    lineVw.frame = CGRectMake(0, headerLbl.frame.origin.y + headerLbl.frame.size.height , mainBox.frame.size.width, 1);
    lineVw.backgroundColor = RGBA(59, 155, 220, 1);
    [mainBox addSubview:lineVw];
    
    int tableHeight;
    if (contactAry.count < 3) {
        tableHeight = (int)contactAry.count * 44;
    }
    else
    {
        tableHeight = 3*44;

    }
    phoneNumberTable.frame = CGRectMake(self.frame.origin.x,lineVw.frame.origin.y + lineVw.frame.size.height+15, self.frame.size.width - (2*mainBox.frame.origin.x), tableHeight );
    phoneNumberTable.backgroundColor = RGBA(230, 230, 230, 1);
    phoneNumberTable.delegate = self;
    phoneNumberTable.dataSource = self;
    [phoneNumberTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [mainBox addSubview:phoneNumberTable];
    
    if (emailAry.count < 3) {
        tableHeight = (int)emailAry.count * 44;
    }
    else
    {
        tableHeight = 3*44;
        
    }
    
    emailTable.frame = CGRectMake(self.frame.origin.x,phoneNumberTable.frame.origin.y + phoneNumberTable.frame.size.height+10, self.frame.size.width - (2*mainBox.frame.origin.x), tableHeight );
    emailTable.backgroundColor = RGBA(230, 230, 230, 1);
    emailTable.delegate = self;
    emailTable.dataSource = self;
    [emailTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [mainBox addSubview:emailTable];
    
	doneBtn.frame = CGRectMake((mainBox.frame.size.width-70)/2,emailTable.frame.origin.y + emailTable.frame.size.height+15,80,35);
	[doneBtn setTitle:@"Done" forState:UIControlStateNormal];
    doneBtn.titleLabel.font = [UIFont fontWithName:FontString size:18.0f];
    [doneBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    doneBtn.backgroundColor = RGBA(53, 152, 219, 1);
	[doneBtn addTarget:self action:@selector(doneBtnCliked:) forControlEvents:UIControlEventTouchUpInside];
    doneBtn.layer.cornerRadius = 15;
	[mainBox addSubview:doneBtn];
    
    mainBox.frame = CGRectMake(20, (self.frame.size.height - (doneBtn.frame.origin.y + doneBtn.frame.size.height+5))/2, self.frame.size.width - 40, (doneBtn.frame.origin.y + doneBtn.frame.size.height+10));

    
    [baseView addSubview:self];
    mainBox.transform = CGAffineTransformMakeScale(0.3, 0.3); // (1)

    [UIView transitionWithView:self.superview
                      duration:0.3
                       options:UIViewAnimationOptionTransitionNone
                    animations:^{
                        mainBox.transform = CGAffineTransformIdentity; // (2)
                        self.backgroundColor = RGBA(0, 0, 0, 0.3);

                        
                    } completion:nil];
}

- (void)tapPress:(UITapGestureRecognizer*)gesture
{
    [UIView transitionWithView:self.superview
                      duration:0.3
                       options:UIViewAnimationOptionTransitionNone
                    animations:^{
                        mainBox.transform =  CGAffineTransformMakeScale(0.3, 0.3); // (2)
                        self.backgroundColor = RGBA(0, 0, 0, 0);
                        
                        
                    } completion:^(BOOL finished){
                        [self removeFromSuperview];
                    }];
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Done Button Action in Contact TableView
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(void)doneBtnCliked:(id)sender
{
    if (tempSelNumber.length != 0 || tempSelEmail.length != 0)
    {
        contData.selectedNumber = tempSelNumber;
        contData.selectedEmail = tempSelEmail;
        if (delegate) {
            
            [delegate PopUpdoneBtnCliked:contData];
        }
        
        
        [UIView transitionWithView:self.superview
                          duration:0.3
                           options:UIViewAnimationOptionTransitionNone
                        animations:^{
                            mainBox.transform =  CGAffineTransformMakeScale(0.3, 0.3);
                            self.backgroundColor = RGBA(0, 0, 0, 0);
                            
                            
                        } completion:^(BOOL finished){
                            [self removeFromSuperview];
                        }];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ALERTVIEW_TITLE message:@"Select atleast one Phone number or Email id" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        alert = nil;
    }
   
    
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - TableView DataSource Method
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == phoneNumberTable)
    {
        return contactAry.count;
    }
    else
    {
    return  emailAry.count;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - TableView DataSource cellForRowAtIndexPath
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self respondsToSelector:@selector(setSectionIndexColor:)])
    {
        tableView.sectionIndexBackgroundColor = [UIColor clearColor];
        tableView.sectionIndexTrackingBackgroundColor = [UIColor clearColor];
    }
    static NSString *CellIdentifier = @"Cell";
    sosContactPopUpCell *cell = (sosContactPopUpCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[sosContactPopUpCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
 
    if (tableView == phoneNumberTable) {
        cell.contactNameLbl.text = [contactAry objectAtIndex:indexPath.row];
        
        cell.appIcon.hidden = YES;
        NSString*  number = [[[contactAry objectAtIndex:indexPath.row] componentsSeparatedByCharactersInSet:
                              [[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"]
                               invertedSet]]
                             componentsJoinedByString:@""];
        if ([IMAry containsObject:[self checkPhoneNum:number]])
        {
            cell.appIcon.hidden = NO;
        }
        if ([[contactAry objectAtIndex:indexPath.row] isEqualToString:tempSelNumber])
        {
            cell.SelectIcon.backgroundColor = [UIColor blackColor];
        }
        else
        {
             cell.SelectIcon.backgroundColor = [UIColor whiteColor];
        }
    }
    else
    {
         cell.appIcon.hidden = YES;
        cell.contactNameLbl.text = [emailAry objectAtIndex:indexPath.row];
        
        if ([[emailAry objectAtIndex:indexPath.row] isEqualToString:tempSelEmail])
        {
            cell.SelectIcon.backgroundColor = [UIColor blackColor];
        }
        else
        {
            cell.SelectIcon.backgroundColor = [UIColor whiteColor];
        }

    }
    return cell;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - TableView Delegate didSelectRowAtIndexPath
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == phoneNumberTable)
    {
        if (tempSelNumber == [contactAry objectAtIndex:indexPath.row]) {
            tempSelNumber = @"";
        }
        else
        {
        tempSelNumber = [contactAry objectAtIndex:indexPath.row];
        }
       
    }
    else
    {
        if (tempSelEmail == [emailAry objectAtIndex:indexPath.row]) {
            tempSelEmail = @"";
        }
        else
        {
            tempSelEmail = [emailAry objectAtIndex:indexPath.row];
        }

        
    }

    [tableView reloadData];
    
}


-(NSString*)checkPhoneNum:(NSString*)number
{
    if (number.length == 10)
    {
        number = [NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"],number];
        return number;
    }
    else if (number.length == 11)
    {
        NSString* subNumber=[number substringToIndex:1];
        if ([subNumber isEqualToString:@"0"])
        {
            NSString *newStr = [number substringWithRange:NSMakeRange(1, [number length]-1)];
            number =  [NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"],newStr];
            return number;
        }
        else
        {
            return nil;
        }
    }
    else
    {
        return number;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
