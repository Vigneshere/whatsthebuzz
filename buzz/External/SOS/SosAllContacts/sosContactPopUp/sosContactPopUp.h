//
//  sosContactPopUp.h
//  buzz
//
//  Created by shaili_solutions_MacPro3 on 26/08/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "contactData.h"

@protocol sosContactPopUpDelegate <NSObject>
-(void)PopUpdoneBtnCliked:(contactData*)contact;
@end

@interface sosContactPopUp : UIView<UITableViewDelegate,UITableViewDataSource>
{
    UITableView* phoneNumberTable;
    UITableView* emailTable;
    UILabel* headerLbl;
    UIView* mainBox;
    UIView* tapVw;

    UIView* lineVw ;
    UIButton* doneBtn;
    UIView* baseView;
    NSMutableArray *contactAry;
    NSMutableArray *emailAry;
    NSMutableArray *IMAry;

    NSString *contactName;
    contactData *contData;

    
    NSString *tempSelNumber;
    NSString *tempSelEmail;
}

@property(nonatomic,assign) id <sosContactPopUpDelegate> delegate;

- (id)initWithFrame:(CGRect)frame view:(UIView *)view contactData:(contactData*)contactData;

@end
