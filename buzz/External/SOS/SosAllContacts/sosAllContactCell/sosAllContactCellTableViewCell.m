//
//  sosAllContactCellTableViewCell.m
//  buzz
//
//  Created by shaili_solutions_MacPro3 on 26/08/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "sosAllContactCellTableViewCell.h"

@implementation sosAllContactCellTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;

        // Initialization code
        self.contactNameLbl = [UILabel new];
        self.line = [UIView new];
        self.selIcon = [UIImageView new];
        self.appUsersLbl = [UILabel new];
        
        
        [self.contentView addSubview:self.appUsersLbl];
        [self.contentView addSubview:self.selIcon];
        [self.contentView addSubview:self.contactNameLbl];
//        [self.contentView addSubview:self.line];

        [self createUI];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect contentViewFrame = self.contentView.frame;
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    contentViewFrame.size.width = screenRect.size.width;
    self.contentView.frame = contentViewFrame;
    [self createUI];
}

- (void)createUI
{
    
   
    self.contactNameLbl.font = [UIFont fontWithName:FontString size:16];
    self.contactNameLbl.textColor  = [UIColor blackColor];
    self.contactNameLbl.backgroundColor = [UIColor clearColor];
    
    self.line.frame = CGRectMake(0, self.contentView.frame.size.height - 1, self.contentView.frame.size.width, 1);
    self.line.backgroundColor = RGBA(164, 182, 194, 1); //RGBA(102, 102, 102, 0.8);
    
    UIImage* appIcon = [UIImage imageNamed:@"SoscontSel"];
    
    self.selIcon.frame = CGRectMake(self.contentView.frame.size.width - (appIcon.size.width + 6)  , (self.contentView.frame.size.height - appIcon.size.height)/2, appIcon.size.width, appIcon.size.height);
    self.selIcon.image = appIcon;
    
    
    
    
    self.appUsersLbl.text = @"Buzz";
    self.appUsersLbl.font = [UIFont fontWithName:FontString size:10.5];
    self.appUsersLbl.textAlignment = NSTextAlignmentCenter;
    self.appUsersLbl.textColor =  RGBA(53, 152, 219, 1);
    self.appUsersLbl.layer.cornerRadius = appIcon.size.height/2;
    self.appUsersLbl.clipsToBounds = YES;
    self.appUsersLbl.backgroundColor = RGBA(255, 255, 255, 0.4);
    
    UIFont *font = [UIFont fontWithName:FontString size:11];
    
    CGRect textRect = [(@"Buzz!" ? @"Buzz!" : @"") boundingRectWithSize:CGSizeMake(320, 9999)
                                                                                    options:NSStringDrawingUsesLineFragmentOrigin
                                                                                 attributes:@{NSFontAttributeName:font}
                                                                                    context:nil];
    self.appUsersLbl .frame = CGRectMake(self.selIcon.frame.origin.x - (textRect.size.width+23) , (self.contentView.frame.size.height - appIcon.size.height)/2, textRect.size.width+20, appIcon.size.height);

     self.contactNameLbl.frame =CGRectMake(15, 0, self.appUsersLbl.frame.origin.x - 15, self.contentView.frame.size.height);
   
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
