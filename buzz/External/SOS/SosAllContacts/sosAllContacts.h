//
//  sosAllContacts.h
//  buzz
//
//  Created by shaili_solutions_MacPro3 on 25/08/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sosContactPopUp.h"

@protocol sosAllContactsDelegate <NSObject>
-(void)doneBtnCliked:(NSMutableArray*)selectedAry;
@end

@interface sosAllContacts :UIView<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,NSFetchedResultsControllerDelegate,sosContactPopUpDelegate>
{
    AvatarImage *avatarImg;
       BOOL isSearching;
    NSPredicate *contactPredicate;
}
@property(nonatomic,assign) id <sosAllContactsDelegate> delegate;

@property(nonatomic,retain)UITableView *allContacts;

@property(nonatomic,retain)UISearchBar *searchBar;
@property (nonatomic, assign)  NSString *viewControllerStr;

@property (nonatomic, retain)  NSMutableArray *contactArray;
@property (nonatomic, retain)  NSMutableArray *searchAry;
@property (nonatomic, retain)  NSMutableArray *searchSecAry;
@property (nonatomic, retain)  NSMutableArray *contactSecArray;
@property (nonatomic, retain)  NSMutableDictionary *visibleContactsDict;
@property (nonatomic, retain)  NSMutableArray *SelectedContactArray;
@property (nonatomic)  int  MaxMember;

-(void)reloadAllContactTable;
@end