//
//  sosContactObject.m
//  buzz
//
//  Created by shaili_solutions_MacPro3 on 11/09/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "sosContactObject.h"

@implementation sosContactObject

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.uniqueId forKey:@"uniqueId"];
    [aCoder encodeObject:self.selectedEmail forKey:@"selectedEmail"];
    [aCoder encodeObject:self.selectedNumber forKey:@"selectedNumber"];
    [aCoder encodeObject:[NSNumber numberWithBool:self.doesExistOnPhoneBook] forKey:@"doesExistOnPhoneBook"];

}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super init]) {
        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.uniqueId = [aDecoder decodeObjectForKey:@"uniqueId"];
        self.selectedEmail = [aDecoder decodeObjectForKey:@"selectedEmail"];
        self.selectedNumber = [aDecoder decodeObjectForKey:@"selectedNumber"];
        self.doesExistOnPhoneBook = [[aDecoder decodeObjectForKey:@"doesExistOnPhoneBook"] boolValue];

    }
    return self;
}
@end
