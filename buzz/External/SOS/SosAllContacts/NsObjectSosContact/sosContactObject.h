//
//  sosContactObject.h
//  buzz
//
//  Created by shaili_solutions_MacPro3 on 11/09/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface sosContactObject : NSObject

@property (retain, nonatomic) NSString *name;
@property (retain, nonatomic) NSString *uniqueId;
@property (retain, nonatomic) NSString* selectedNumber;
@property (retain, nonatomic) NSString* selectedEmail;
@property (nonatomic) BOOL doesExistOnPhoneBook;


@end
