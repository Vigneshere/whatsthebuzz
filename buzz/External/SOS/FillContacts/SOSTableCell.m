//
//  SOSTableCell.m
//  buzz
//
//  Created by shaili_solutions_MacPro3 on 18/08/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "SOSTableCell.h"

@implementation SOSTableCell
@synthesize delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        self.mainBox = [UIImageView new];
        self.nameLbl = [UILabel new];
        self.numberLbl = [UILabel new];
        self.emailTxtFld = [UITextField new];
        self.minusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [self createCell];
        
        [self.mainBox addSubview:self.nameLbl];
        [self.mainBox addSubview:self.numberLbl];
        [self.mainBox addSubview:self.emailTxtFld];
        [self.mainBox addSubview:self.minusBtn];
        [self.contentView addSubview:self.mainBox];
        

    }
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    CGRect contentViewFrame = self.contentView.frame;
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    contentViewFrame.size.width = screenRect.size.width;
    self.contentView.frame = contentViewFrame;

    [self createCell];

}


- (void) createCell
{

    UIImage* mainBox_Img = [UIImage imageNamed:@"sos_mainBox"];
    
    self.mainBox.image = mainBox_Img;
    self.mainBox.userInteractionEnabled = YES;
    self.mainBox.frame = CGRectMake((self.contentView.frame.size.width - mainBox_Img.size.width)/2, (120 -mainBox_Img.size.height)/2, mainBox_Img.size.width, mainBox_Img.size.height);
    
    self.nameLbl.frame = CGRectMake(66, 0, 191, 36);
    self.nameLbl.font = [UIFont fontWithName:FontString size:14];
    
    self.numberLbl.frame = CGRectMake(66, 36.6, 191, 36);
    self.numberLbl.font = [UIFont fontWithName:FontString size:14];
    
    self.emailTxtFld.frame = CGRectMake(66, 2*36.6, 191, 36);
    self.emailTxtFld.font = [UIFont fontWithName:FontString size:14];
    self.emailTxtFld.userInteractionEnabled = YES;
    self.emailTxtFld.keyboardType = UIKeyboardTypeEmailAddress;
    self.emailTxtFld.delegate = self;
    
    
    self.minusBtn.frame = CGRectMake(self.numberLbl.frame.origin.x + self.numberLbl.frame.size.width + 10, 36, 36, 36);
    [self.minusBtn addTarget:self action:@selector(Del_Btn_Method:) forControlEvents:UIControlEventTouchUpInside];
    
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
 
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField
{

    
    if (textField.text.length==0){
        [textField resignFirstResponder];
        [delegate SOSEmailTextFieldDidBeginEditing:textField];

    }
    else{
        if ([self validateEmail:textField.text]){
            [textField resignFirstResponder];
            [delegate SOSEmailTextFieldDidBeginEditing:textField];
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Invalid Email" message:@"Email not in format." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            textField.text = @"";
        }
        
    }
    
    return YES;
}

- (BOOL)validateEmail:(NSString *)inputText {
    NSString *emailRegex = @"[A-Z0-9a-z][A-Z0-9a-z._%+-]*@[A-Za-z0-9][A-Za-z0-9.-]*\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    NSRange aRange;
    if([emailTest evaluateWithObject:inputText]) {
        aRange = [inputText rangeOfString:@"." options:NSBackwardsSearch range:NSMakeRange(0, [inputText length])];
        int indexOfDot = aRange.location;
        //NSLog(@"aRange.location:%d - %d",aRange.location, indexOfDot);
        if(aRange.location != NSNotFound) {
            NSString *topLevelDomain = [inputText substringFromIndex:indexOfDot];
            topLevelDomain = [topLevelDomain lowercaseString];
            //NSLog(@"topleveldomains:%@",topLevelDomain);
            NSSet *TLD;
            TLD = [NSSet setWithObjects:@".aero", @".asia", @".biz", @".cat", @".com", @".coop", @".edu", @".gov", @".info", @".int", @".jobs", @".mil", @".mobi", @".museum", @".name", @".net", @".org", @".pro", @".tel", @".travel", @".ac", @".ad", @".ae", @".af", @".ag", @".ai", @".al", @".am", @".an", @".ao", @".aq", @".ar", @".as", @".at", @".au", @".aw", @".ax", @".az", @".ba", @".bb", @".bd", @".be", @".bf", @".bg", @".bh", @".bi", @".bj", @".bm", @".bn", @".bo", @".br", @".bs", @".bt", @".bv", @".bw", @".by", @".bz", @".ca", @".cc", @".cd", @".cf", @".cg", @".ch", @".ci", @".ck", @".cl", @".cm", @".cn", @".co", @".cr", @".cu", @".cv", @".cx", @".cy", @".cz", @".de", @".dj", @".dk", @".dm", @".do", @".dz", @".ec", @".ee", @".eg", @".er", @".es", @".et", @".eu", @".fi", @".fj", @".fk", @".fm", @".fo", @".fr", @".ga", @".gb", @".gd", @".ge", @".gf", @".gg", @".gh", @".gi", @".gl", @".gm", @".gn", @".gp", @".gq", @".gr", @".gs", @".gt", @".gu", @".gw", @".gy", @".hk", @".hm", @".hn", @".hr", @".ht", @".hu", @".id", @".ie", @" No", @".il", @".im", @".in", @".io", @".iq", @".ir", @".is", @".it", @".je", @".jm", @".jo", @".jp", @".ke", @".kg", @".kh", @".ki", @".km", @".kn", @".kp", @".kr", @".kw", @".ky", @".kz", @".la", @".lb", @".lc", @".li", @".lk", @".lr", @".ls", @".lt", @".lu", @".lv", @".ly", @".ma", @".mc", @".md", @".me", @".mg", @".mh", @".mk", @".ml", @".mm", @".mn", @".mo", @".mp", @".mq", @".mr", @".ms", @".mt", @".mu", @".mv", @".mw", @".mx", @".my", @".mz", @".na", @".nc", @".ne", @".nf", @".ng", @".ni", @".nl", @".no", @".np", @".nr", @".nu", @".nz", @".om", @".pa", @".pe", @".pf", @".pg", @".ph", @".pk", @".pl", @".pm", @".pn", @".pr", @".ps", @".pt", @".pw", @".py", @".qa", @".re", @".ro", @".rs", @".ru", @".rw", @".sa", @".sb", @".sc", @".sd", @".se", @".sg", @".sh", @".si", @".sj", @".sk", @".sl", @".sm", @".sn", @".so", @".sr", @".st", @".su", @".sv", @".sy", @".sz", @".tc", @".td", @".tf", @".tg", @".th", @".tj", @".tk", @".tl", @".tm", @".tn", @".to", @".tp", @".tr", @".tt", @".tv", @".tw", @".tz", @".ua", @".ug", @".uk", @".us", @".uy", @".uz", @".va", @".vc", @".ve", @".vg", @".vi", @".vn", @".vu", @".wf", @".ws", @".ye", @".yt", @".za", @".zm", @".zw", nil];
            if(topLevelDomain != nil && ([TLD containsObject:topLevelDomain])) {
                //NSLog(@"TLD contains topLevelDomain:%@",topLevelDomain);
                return TRUE;
            }
            /*else {
             NSLog(@"TLD DOEST NOT contains topLevelDomain:%@",topLevelDomain);
             }*/
            
        }
    }
    return FALSE;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}




-(void)Del_Btn_Method:(id)sender
{
    
    /*
    UIButton* btn = (UIButton*)sender;
    UITableViewCell *cell = (UITableViewCell*) [[[[btn superview] superview] superview] superview];
    
    if (delegate) {
    [delegate SOSTableCellDeleteIndexSelected:cell];
       

    }
   
     */
}

-(void)dealloc
{
    
    
}

@end
