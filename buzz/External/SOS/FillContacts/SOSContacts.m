//
//  SOSContacts.m
//  SOS
//
//  Created by shaili_macMini02 on 11/02/14.
//  Copyright (c) 2014 shaili_macMini02. All rights reserved.
//

#import "SOSContacts.h"
#import "TTTOrdinalNumberFormatter.h"
#import "AFHTTPClient.h"
#import "AFJSONRequestOperation.h"
#import "RXMLElement.h"
#import "contactData.h"
#import "sosAllContacts.h"


@implementation SOSContacts
@synthesize delegate;

#pragma mark - remove DomainName from String

-(NSString*)removeDomainName_String:(NSString*)string
{
    return [string stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"@%@",DOMAINAME] withString:@""];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(UIView*)createView :(NSMutableArray*)contAry :(NSMutableArray*)emailIdAry :(NSString*)code
{
    // Do any additional setup after loading the view, typically from a nib.
    self.backgroundColor = RGBA(249, 249, 249, 1);
    

    contactView = [UIView new];

//    self.contactTable = [[sosAllContacts alloc]init];
//    self.contactTable.MaxMember = 5;
//    
//    
//        
//    self.contactTable.contactArray = [[[[self appDelegate].contactsAry mutableCopy] sortedArrayUsingDescriptors:[NSSortDescriptor sortValues:@"firstNames" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]] mutableCopy];
//   
//    self.contactTable.contactSecArray  = [self.contactTable.contactArray valueForKeyPath:@"@distinctUnionOfObjects.firstLetter"];
    
    self.contactTable = [[contactTableView alloc]init];
    self.contactTable.MaxMember = 5;
    
    settingsBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    contacts_TableVw = [[UITableView alloc]init];
    deactCodeLbl = [[UILabel alloc]init];
    passCodeVw = [[UIView alloc]init];
    submitBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    
    baseScrollVw = [[UIScrollView alloc]init];
    baseScrollVw.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    baseScrollVw.scrollEnabled = NO;
    baseScrollVw.contentSize = CGSizeMake(self.frame.size.width, self.frame.size.height + 80);
    [self addSubview:baseScrollVw];
    
    
    
    [self refreshDataAry];
    
    self.contactTable.groupMemberArray = [self fetchUserContactsArrayFromJidAry:contAry];
    emailAry = [emailIdAry mutableCopy];
    
    
    contacts_TableVw.frame = CGRectMake(self.frame.origin.x,0, self.frame.size.width, self.frame.size.height - 100 );
    contacts_TableVw.backgroundColor = [UIColor clearColor];
    contacts_TableVw.delegate = self;
    contacts_TableVw.dataSource = self;
    [contacts_TableVw setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [baseScrollVw addSubview:contacts_TableVw];
    
    deactCodeLbl.frame = CGRectMake(0, contacts_TableVw.frame.size.height + 20, (self.frame.size.width/2) -10 , 20);
    deactCodeLbl.text = @"Deactivation code";
    deactCodeLbl.font = [UIFont fontWithName:FontString size:10];
    deactCodeLbl.textAlignment = NSTextAlignmentRight;
    [baseScrollVw addSubview:deactCodeLbl];
    
    passCodeVw = [self create_circles:4 radius:10 space:3 view:passCodeVw];
    passCodeVw.frame = CGRectMake(deactCodeLbl.frame.size.width + 10, contacts_TableVw.frame.size.height + 20, passCodeVw.frame.size.width, passCodeVw.frame.size.height);
    [baseScrollVw addSubview:passCodeVw];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    tapGesture.numberOfTapsRequired=1;
    [passCodeVw addGestureRecognizer:tapGesture];
    
    UITapGestureRecognizer *tapGesture_keyBord = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGesture)];
    tapGesture_keyBord.numberOfTapsRequired=1;
    [baseScrollVw addGestureRecognizer:tapGesture_keyBord];
    
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGesture)];
    swipeGesture.direction = UISwipeGestureRecognizerDirectionDown;
    [self addGestureRecognizer:swipeGesture];
    
    submitBtn.frame = CGRectMake((self.frame.size.width - 60)/2, passCodeVw.frame.origin.y + passCodeVw.frame.size.height + 20, 60, 20);
    [submitBtn setTitle:@"Submit" forState:UIControlStateNormal];
    [submitBtn setTintColor:[UIColor whiteColor]];
    [submitBtn addTarget:self action:@selector(GO_BTN_CLICKED) forControlEvents:UIControlEventTouchUpInside];
    submitBtn.backgroundColor = RGBA(46, 141, 214, 1);
    submitBtn.titleLabel.font = [UIFont fontWithName:FontString size:12];
    //submitBtn.layer.cornerRadius = 10;
    [baseScrollVw addSubview:submitBtn];
    
    hiddenPwdTxtFld = [[UITextField alloc]init];
    hiddenPwdTxtFld.keyboardType = UIKeyboardTypeNumberPad;
    hiddenPwdTxtFld.delegate = self;
    [baseScrollVw addSubview:hiddenPwdTxtFld];
    
    if (code.length == 4) {
        //[self fillPassword:code];
    }
    
    return self;
}

-(void)fillPassword :(NSString*)code
{
    hiddenPwdTxtFld.text = code;
    for (int i =0; i < code.length; i++) {
        UILabel* passcode = (UILabel*)[passCodeVw viewWithTag:i+1];
        [NSTimer scheduledTimerWithTimeInterval: 0 target: self
                                       selector: @selector(substitude:) userInfo:passcode repeats: NO];
        
    }
}

-(void)GO_BTN_CLICKED
{
    
     [emailAry removeObject:@""];
    
    if ([self checkEmailValidation:emailAry:YES])
    {
        
        for (id str in emailAry )
        {
            if ([self checkWhiteSpace:str]) {
                [emailAry removeObject:str];
                
            }
            
        }
    }
    
    if ([self.contactTable.groupMemberArray count] + [emailAry count])
    {
        
        if (hiddenPwdTxtFld.text.length == 4) {

            
            [self WebServicesCONTACTS];
        }
        else
        {
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Buzz!"
                                                              message:@"please fill the deactivation code"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
            [message show];
        }
    }
    else
    {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Buzz!"
                                                          message:@"please fill atleast one contact"
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
    }
    
    
    
}


-(UIView*)create_circles:(int)count radius:(int)radius space:(int)space view:(UIView*)view
{
    
    view.frame = CGRectMake(0, 0, (space * (count - 1) + (count * 2 * radius)), 2*radius);
    
    for (int i = 0; i < count; i++) {
        
        UILabel* lbl = [[UILabel alloc]init];
        lbl.frame = CGRectMake(i*((2*radius)+space), 0, 2*radius, 2*radius);
        lbl.backgroundColor = [UIColor whiteColor];
        lbl.tag = i+1;
        lbl.layer.cornerRadius = radius;
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.layer.borderColor = RGBA(203, 203, 203, 1).CGColor;
        lbl.layer.borderWidth = 1.0f;
        lbl.layer.masksToBounds = YES;
        [view addSubview:lbl];
    }
    
    return view;
}

-(void)handleTapGesture:(UITapGestureRecognizer *) sender
{
    [hiddenPwdTxtFld becomeFirstResponder];
}

-(void)handleSwipeGesture{
    
    [hiddenPwdTxtFld resignFirstResponder];
}
-(void)refreshDataAry
{
    contactsAry = [[NSMutableArray alloc]init];
    emailAry = [[NSMutableArray alloc]init];
    
}


-(void)handleTapGesture1:(UITapGestureRecognizer *) sender
{
    UIView * mainView=sender.view;
    UITableViewCell *cell = (UITableViewCell*) [[[mainView superview] superview] superview];
    NSIndexPath* index = [contacts_TableVw indexPathForCell:cell];
    
    if (index.section == 0) {
     //   NSLog(@"tapped index = %ld",(long)index.row);
        [self dummyAry:index.row];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;    //count of section
}


//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView* vw = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
//    vw.backgroundColor = [UIColor greenColor];
//    return vw;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger count;
    if (section == 0)
    {
        if (!([self.contactTable.groupMemberArray count] > 4)) {
          count =  [self.contactTable.groupMemberArray count] +2;
        }
        else
        {
            count = [self.contactTable.groupMemberArray count]+1;
        }
       

        
    }
    else
    {
        if (!([emailAry count] > 4)) {
            count =  [emailAry count] +2;
        }
        else
        {
            count = [emailAry count]+1;
        }
    }
    
   
    return count;
    //count number of row from counting array hear cataGorry is An Array
}
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 50;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 ) {
        return 40;
    }
    else
    {
        if (indexPath.section == 0) {
            return 50;
        }
        else
        {
            return 30;
        }
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier] ;
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = [UIColor clearColor];
        UIView* main_Box = [[UIView alloc]init];//CGRectMake(30, 3, 250, 47)];
        main_Box.tag = 99;
        
        
        
        UIView* headerVw = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 40)];
        headerVw.backgroundColor = [UIColor clearColor];
        
        UILabel* header_Lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, self.frame.size.width, 25)];
        header_Lbl.backgroundColor = [UIColor clearColor];
        header_Lbl.font = [UIFont fontWithName:FontString size:12];
        header_Lbl.textColor = RGBA(45, 142, 213, 1);
        header_Lbl.textAlignment = NSTextAlignmentCenter;
        header_Lbl.tag = 97;
        headerVw.tag = 98;
        [headerVw addSubview:header_Lbl];
        
        //
        UILabel* contact_Lbl = [[UILabel alloc]init];
        contact_Lbl.tag = 100;
        contact_Lbl.font = [UIFont fontWithName:FontString size:12];
        
        UILabel* phone_Contact_Lbl = [[UILabel alloc]init];
        phone_Contact_Lbl.tag = 101;
        phone_Contact_Lbl.font = [UIFont fontWithName:FontString size:12];
        
        UIButton *Add_Btn = [UIButton buttonWithType:UIButtonTypeCustom];
        Add_Btn.tag = 102;
        [Add_Btn addTarget:self
                    action:@selector(AddBtn_Method:)
          forControlEvents:UIControlEventTouchDown];
        [Add_Btn setImage:[UIImage imageNamed:@"plus.png"] forState:UIControlStateNormal];
        
        
        UIButton *Del_Btn = [UIButton buttonWithType:UIButtonTypeCustom];
        Del_Btn.tag = 103;
        [Del_Btn addTarget:self
                    action:@selector(Del_Btn_Method:)
          forControlEvents:UIControlEventTouchDown];
        [Del_Btn setImage:[UIImage imageNamed:@"minus.png"] forState:UIControlStateNormal];
        //
        
        UITextField* emailTxtFld = [[UITextField alloc]init];
        emailTxtFld.tag = 104;
        emailTxtFld.delegate = self;
        emailTxtFld.keyboardType = UIKeyboardTypeEmailAddress;
        emailTxtFld.font = [UIFont fontWithName:FontString size:12];
        
        [cell.contentView addSubview:main_Box];
        [cell.contentView addSubview:headerVw];
        [main_Box addSubview:Add_Btn];
        [main_Box addSubview:Del_Btn];
        [main_Box addSubview:contact_Lbl];
        [main_Box addSubview:emailTxtFld];
        [main_Box addSubview:phone_Contact_Lbl];
    }
    UIView* main_Box = (UIView*)[cell.contentView viewWithTag:99];
    UIView* headerVw = (UIView*)[cell.contentView viewWithTag:98];
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showContactTableView:)];
    
    if (indexPath.row == 0) {
        main_Box.hidden = YES;
        headerVw.hidden = NO;
        UILabel* header_Lbl = (UILabel*)[headerVw viewWithTag:97];
        if (indexPath.section == 0) {
            header_Lbl.text = @"Add Contact";
        }
        else
        {
            header_Lbl.text = @"Add Emails";
        }
        
    }
    else
    {
        main_Box.hidden = NO;
        headerVw.hidden = YES;
        
        
        UILabel* contact_Lbl = (UILabel*)[cell.contentView viewWithTag:100];
        UILabel* phone_Contact_Lbl = (UILabel*)[cell.contentView viewWithTag:101];
        
        UIButton* Add_Btn = (UIButton*)[cell.contentView viewWithTag:102];
        UIButton* Del_Btn = (UIButton*)[cell.contentView viewWithTag:103];
        
        UITextField* emailTxtFld = (UITextField*)[cell.contentView viewWithTag:104];
     //   emailTxtFld.text = @"";
        
        if (indexPath.section == 0)
        {
            
            main_Box.frame = CGRectMake(35, 1.5, 250, 47);
            contact_Lbl .frame = CGRectMake(35, 0, main_Box.frame.size.width - 65, main_Box.frame.size.height/2);
            phone_Contact_Lbl.frame = CGRectMake(35, main_Box.frame.size.height/2, main_Box.frame.size.width - 65, main_Box.frame.size.height/2);
            Add_Btn.frame = CGRectMake(225, 10, 26, 27);
            Del_Btn.frame = CGRectMake(225, 10, 26, 27);
            
            main_Box.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"box.png"]];
            
            emailTxtFld.hidden = YES;
            Add_Btn.hidden = YES;
            Del_Btn.hidden = NO;
            
            contact_Lbl.hidden = NO;
            phone_Contact_Lbl.hidden = NO;
            [main_Box addGestureRecognizer:tapGesture1];
       //     NSLog(@"%d  %d",[self.contactTable.groupMemberArray count],indexPath.row);
             if (indexPath.row <= [self.contactTable.groupMemberArray count] )
             {
                
                 XMPPUserCoreDataStorageObject *user = [self.contactTable.groupMemberArray objectAtIndex:indexPath.row - 1];
                 
                 contact_Lbl.text = user.displayName;
                 phone_Contact_Lbl.text = [self removeDomainName_String:user.jidStr];
                 
            }
           else
           {
               contact_Lbl.text = @"";
               phone_Contact_Lbl.text = @"";
           }
            
            
            
           
            
            
        }
        else
        {
            main_Box.frame = CGRectMake(35, 1.5, 250, 27);
            Add_Btn.frame = CGRectMake(225, 0, 26, 27);
            Del_Btn.frame = CGRectMake(225, 0, 26, 27);
            emailTxtFld.frame = CGRectMake(35, 0, main_Box.frame.size.width - 65, main_Box.frame.size.height);
            main_Box.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"mail-box.png"]];
            
            emailTxtFld.hidden = NO;
            
            if (emailAry.count >= indexPath.row)
            {
                emailTxtFld.text = [emailAry objectAtIndex:indexPath.row-1];
            }
            
           
                Add_Btn.hidden = YES;
                Del_Btn.hidden = NO;
            
            
            contact_Lbl.hidden = YES;
            phone_Contact_Lbl.hidden = YES;
            [main_Box removeGestureRecognizer:tapGesture1];
        }
        
    }
    // Here we use the provided setImageWithURL: method to load the web image
    // Ensure you use a placeholder image otherwise cells will be initialized with no image
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //    if (indexPath.section == 0) {
    //        [self dummyAry];
    //    }
    
}


-(void)AddBtn_Method:(id)sender
{
    contacts_TableVw.contentInset =  UIEdgeInsetsMake(0, 0, 0, 0);
    [currentTextFld resignFirstResponder];
    UIButton* btn = (UIButton*)sender;
    UITableViewCell *cell = (UITableViewCell*) [[[[btn superview] superview] superview] superview];
    NSIndexPath* index = [contacts_TableVw indexPathForCell:cell];
    
    if (index.section == 0)
    {
        if (![contactsAry containsObject:@""])
        {
            if ([contactsAry count] < 5) {
                [contactsAry addObject:@""];
                [self  reloadData:YES];
            }
        }
        else
        {
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Buzz!"
                                                              message:@"Please fill all the contacts before adding one"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
            [message show];
            
        }
        
        
    }
    else
    {
        if ([self checkEmailValidation:emailAry:NO]) {
            
            
            if ([emailAry count] < 5) {
               
                [self reloadData:YES];
                NSIndexPath* ipath = [NSIndexPath indexPathForRow: [emailAry count]-1 inSection: 1];
                [contacts_TableVw scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];
                
            }
        }
    }
}

-(void)replaceObjectString:(NSInteger)indexPoint :(NSString*)string
{
    if ([emailAry count] <= indexPoint)
    {
        [emailAry addObject:@""];
    }
    [emailAry replaceObjectAtIndex:indexPoint withObject:string];
}

-(BOOL)checkEmailValidation:(NSMutableArray *)tempEmailAry :(BOOL)checkWhiteSpace
{
    int i = 1;
    NSString* invalidEmail = @"" ;
    BOOL status;
    status = YES;
    
    NSMutableArray* tempAry = [[NSMutableArray alloc]init];
    
    for(NSString* email_Str in tempEmailAry)
    {
        
        if (![self validateEmailWithString:email_Str:checkWhiteSpace]) {
            [tempAry addObject:[self ordinalsStr:i]];
            status = NO;
        }
        i++;
    }
    
    if ([tempAry count])
    {
        i =1;
        for(NSString* num_th_Str in tempAry)
        {
            if (i == 1 && [tempAry count] >1 ) {
                invalidEmail = [invalidEmail stringByAppendingString:[NSString stringWithFormat:@"%@",num_th_Str]];
            }
            else if (i > 1 && i  == [tempAry count])
            {
                invalidEmail = [invalidEmail stringByAppendingString:[NSString stringWithFormat:@" and %@ emails are invalid.",num_th_Str]];
            }
            else if (i  == [tempAry count])
            {
                if ([tempAry count] >1) {
                    invalidEmail = [invalidEmail stringByAppendingString:[NSString stringWithFormat:@" %@ emails are invalid.",num_th_Str]];
                }
                else
                {
                    invalidEmail = [invalidEmail stringByAppendingString:[NSString stringWithFormat:@" %@ email is invalid.",num_th_Str]];
                }
                
            }
            else{
                invalidEmail = [invalidEmail stringByAppendingString:[NSString stringWithFormat:@",%@",num_th_Str]];
            }
            i = i+1;
        }
    }
    if (invalidEmail.length > 0) {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Buzz!"
                                                          message:invalidEmail
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        
        [message show];
    }
    return status;
}
-(void)Del_Btn_Method:(id)sender
{
    contacts_TableVw.contentInset =  UIEdgeInsetsMake(0, 0, 0, 0);
    [currentTextFld resignFirstResponder];
    UIButton* btn = (UIButton*)sender;
    UITableViewCell *cell = (UITableViewCell*) [[[[btn superview] superview] superview] superview];
    NSIndexPath* index = [contacts_TableVw indexPathForCell:cell];
    
    if (index.section == 0)
    {
        if (index.row <= [self.contactTable.groupMemberArray count] ) {
            [self.contactTable.groupMemberArray removeObjectAtIndex:index.row-1];
            //[self.contactTable reloadData];
            [self  reloadData:YES];
        }
        else
        {
            
        }
        
    }
    else
    {
         if (index.row <= [emailAry count] ) {
            [emailAry removeObjectAtIndex:index.row-1];
            [self  reloadData:YES];
        }
    }
}

- (void)reloadData:(BOOL)animated
{
    
    [contacts_TableVw reloadData];
    
    
    if (animated) {
        
        CATransition *animation = [CATransition animation];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCAAnimationPaced];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [animation setFillMode:kCAFillModeBoth];
        [animation setDuration:.3];
        [[contacts_TableVw layer] addAnimation:animation forKey:@"UITableViewReloadDataAnimationKey"];
        
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    currentTextFld = textField;
    if (textField == hiddenPwdTxtFld) {
        baseScrollVw.contentOffset = CGPointMake(0, 170);
        contacts_TableVw.scrollEnabled = NO;
    }
    return YES;
}


- (void) textFieldDidBeginEditing:(UITextField *)textField {
    
    if (!(textField == hiddenPwdTxtFld)) {
        UITableViewCell *cell = (UITableViewCell*) [[[[textField superview] superview] superview] superview];
        contacts_TableVw.contentInset =  UIEdgeInsetsMake(0, 0, 130, 0);
        
        [contacts_TableVw scrollToRowAtIndexPath:[contacts_TableVw indexPathForCell:cell] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    if (textField == hiddenPwdTxtFld) {
        baseScrollVw.contentOffset = CGPointMake(0, 0);
        contacts_TableVw.scrollEnabled = YES;
    }
    else
    {
        [textField resignFirstResponder];
        UITableViewCell *cell = (UITableViewCell*) [[[[textField superview] superview] superview] superview];
        NSIndexPath* index = [contacts_TableVw indexPathForCell:cell];
        
        int row = index.row;
        [self replaceObjectString:row - 1 :textField.text];
        
        

        
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField == hiddenPwdTxtFld) {
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
        {
            
            
            return NO;
        }
        // verify max length has not been exceeded
        NSUInteger newLength = textField.text.length + string.length - range.length;
        
        if ([string isEqualToString:@""] ) {
            UILabel* passcode = (UILabel*)[passCodeVw viewWithTag:newLength+1];
            passcode.text = @"";
            return YES;
        }
        if (newLength < 5) {
            UILabel* passcode = (UILabel*)[passCodeVw viewWithTag:newLength];
            passcode.text = string;
            [NSTimer scheduledTimerWithTimeInterval: 0.2 target: self
                                           selector: @selector(substitude:) userInfo:passcode repeats: NO];
            return YES;
        }
        else
        {
            return NO;
        }
    }
    else
    {
        if (textField.tag == 101) {
            if ((textField.text.length > 10) && (![string isEqualToString:@""])) {
                return NO;
            }
            else if([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
            {
                return NO;
            }
            else
            {
                return YES;
            }
            
        }
        return YES;
    }
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [contacts_TableVw reloadData];
    contacts_TableVw.contentInset =  UIEdgeInsetsMake(0, 0, 0, 0);
    [textField resignFirstResponder];
    return YES;
}

-(void)substitude :(NSTimer*)theTimer
{
    UILabel* passcodeLbl = (UILabel*)[theTimer userInfo];
    
    passcodeLbl.text = @"\u25cf";
    
}
- (BOOL)validateEmailWithString:(NSString*)email :(BOOL)checkWhiteSpace
{
    if (checkWhiteSpace)
    {
        if ([self checkWhiteSpace:email]) {
            return YES;
        }
    }
    
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

-(BOOL)checkWhiteSpace :(NSString*)email
{
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[email stringByTrimmingCharactersInSet: set] length] == 0)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

-(NSString*)ordinalsStr:(int)num
{
    TTTOrdinalNumberFormatter *ordinalNumberFormatter = [[TTTOrdinalNumberFormatter alloc] init];
    [ordinalNumberFormatter setLocale:[NSLocale currentLocale]];
    [ordinalNumberFormatter setGrammaticalGender:TTTOrdinalNumberFormatterMaleGender];
    NSNumber *number = [NSNumber numberWithInteger:num];
    return [ordinalNumberFormatter stringFromNumber:number];
}



-(void)dummyAry:(NSInteger)index
{
    NSMutableArray* dummyAry = [[NSMutableArray alloc]initWithObjects:@"prabu",@"deepak",@"Bob",@"Joe",@"Belzebub",@"prabu",@"deepak",@"Bob",@"Joe",@"Belzebub",@"prabu",@"deepak",@"Bob",@"Joe",@"Belzebub",@"prabu",@"deepak",@"Bob",@"Joe",@"Belzebub",@"prabu",@"deepak",@"Bob",@"Joe",@"Belzebub",@"prabu",@"deepak",@"Bob",@"Joe",@"Belzebub",@"prabu",@"deepak",@"Bob",@"Joe",@"Belzebub", nil];
    [self showListVw:dummyAry :@"Contact" :index];
}
-(void)showListVw:(NSMutableArray*)listAry :(NSString*)searchType :(NSInteger)index
{
    [searchLish_vw removeFromSuperview];
    
    
    searchVw* searchView = [[searchVw alloc]initWithFrame:CGRectMake(0, self.frame.size.height, self.frame.size.width, self.frame.size.height)];
    searchView.delegate = self;
    
    searchLish_vw = [searchView createVw:listAry:searchType:index];
    searchLish_vw.frame = CGRectMake(0, self.frame.size.height,searchLish_vw.frame.size.width,searchLish_vw.frame.size.height);
    [self addSubview:searchLish_vw];
    
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         searchLish_vw.frame = CGRectMake(0,  0,searchLish_vw.frame.size.width,searchLish_vw.frame.size.height);
                     }
                     completion:^(BOOL finished){//this block starts only when
                         
                     }];
    
}

-(void)searchResult:(NSString *)str :(NSInteger)returnIndex
{
   // NSLog(@"%@",str);
    [contactsAry replaceObjectAtIndex:returnIndex-1 withObject:str];
    if (!([contactsAry count] > 4))
    {
        [contactsAry addObject:@""];
    }
    [contacts_TableVw reloadData];
    
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         searchLish_vw.frame = CGRectMake(0,  self.frame.size.height ,searchLish_vw.frame.size.width,searchLish_vw.frame.size.height);
                     }
                     completion:^(BOOL finished){//this block starts only when
                         [searchLish_vw removeFromSuperview];
                     }];
}

-(void)closeSearchVw
{
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         searchLish_vw.frame = CGRectMake(0,  self.frame.size.height ,searchLish_vw.frame.size.width,searchLish_vw.frame.size.height);
                     }
                     completion:^(BOOL finished){//this block starts only when
                         [searchLish_vw removeFromSuperview];
                     }];
}


- (void)setDelegate:(id<SOSContactDelegate>)val {
    
    delegate = val;
    singleton = [Singleton sharedMySingleton];
    singleton.sosContactsDelegate = delegate;
}


-(void)showContactTableView:(id)sender
{
    [contactView removeFromSuperview];
  [currentTextFld resignFirstResponder];
    contactView.frame = CGRectMake(0, self.frame.size.height+10, self.frame.size.width, self.frame.size.height);
    contactView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg"]];
    UIViewController* viewController = (UIViewController*)delegate;
    if (viewController)
    {
        [viewController.navigationController setNavigationBarHidden:YES animated:YES];
        
    }
    
    [UIView animateWithDuration:0.25 animations:^{
        contactView.frame =  CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
        [self addSubview:contactView];
    }];
    
    UILabel* label = [UILabel new];
    label.frame = CGRectMake(0, contactView.frame.origin.y+15, contactView.frame.size.width, 25);
    label.text = @"Select Contact";
    label.font = [UIFont fontWithName:FontString size:21.0f];
    label.textAlignment = NSTextAlignmentCenter;
	[contactView addSubview:label];

    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
	doneBtn.frame = CGRectMake(10,contactView.frame.origin.y+15,60,25);
    doneBtn.layer.borderColor = RGBA(53, 152, 219, 1).CGColor;
    doneBtn.layer.borderWidth = 1.2f;
    doneBtn.layer.cornerRadius = 25/2;
    doneBtn.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
	[doneBtn setTitle:@"Done" forState:UIControlStateNormal];
    doneBtn.titleLabel.font = [UIFont fontWithName:FontString size:17.0f];
    [doneBtn setTitleColor:RGBA(53, 152, 219, 1) forState:UIControlStateNormal];
	[doneBtn addTarget:self action:@selector(doneBtnCliked:) forControlEvents:UIControlEventTouchUpInside];
	[contactView addSubview:doneBtn];
    
    self.contactTable.frame = CGRectMake(0, 50, self.frame.size.width, self.frame.size.height-50);
    self.contactTable.viewControllerStr = @"createGroupController";
    contactView.userInteractionEnabled = YES;
    [contactView addSubview:self.contactTable];
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Dont Button Action in Contact TableView
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)doneBtnCliked:(id)sender
{
    
    UIViewController* viewController = (UIViewController*)delegate;
    if (viewController)
    {
        [viewController.navigationController setNavigationBarHidden:NO animated:YES];

    }

    [self  endEditing:YES];
    [UIView animateWithDuration:0.25 animations:^{
        contactView.frame =  CGRectMake(0, self.frame.size.height+10, self.frame.size.width, self.frame.size.height);
    }];
    [contacts_TableVw reloadData];
    
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark DataBase Methods For Recent Chats
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (XMPPUserCoreDataStorageObject *)RecentcontactsFetch:(NSString *)Str
{
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:[self managedObjectContext_roster]];
    [request setEntity:entity];
    NSString *predicateFrmt = @"jidStr = %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, Str];
    [request setPredicate:predicate];
    NSArray *empArray=[[self managedObjectContext_roster] executeFetchRequest:request error:nil];
    if ([empArray count] == 1)
    {
        for (XMPPUserCoreDataStorageObject *user in  empArray)
        {
            return user;
        }
    }
    return nil;
}

-(NSMutableArray*)fetchUserContactsArrayFromJidAry:(NSMutableArray*)arrray
{
    NSMutableArray* UserContactAry = [NSMutableArray new];
    
    for (NSString* str in arrray)
    {
        XMPPUserCoreDataStorageObject* userObj = [self RecentcontactsFetch:str];
        if (userObj != nil)
        {
            [UserContactAry addObject:userObj];
 
        }
    }
    return UserContactAry;
}

- (NSManagedObjectContext *)managedObjectContext_roster
{
	return [[[self appDelegate] xmppRosterStorage] mainThreadManagedObjectContext];
}

- (AppDelegate *)appDelegate
{
	return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

#pragma mark - Web Services CONTACTS

-(void)WebServicesCONTACTS
{
    // NSLog(@"%@",imgStr);
    NSMutableArray* innercontactsAry = [[NSMutableArray alloc]init];
    NSMutableArray* StoredContactsAry = [[NSMutableArray alloc]init];
    for (int i = 0; i < 5; i++) {
        
        if ([self.contactTable.groupMemberArray count] > i) {
            
            XMPPUserCoreDataStorageObject *user = [self.contactTable.groupMemberArray objectAtIndex:i];
            [innercontactsAry addObject:[self removeDomainName_String:user.jidStr]];
            [StoredContactsAry addObject:user.jidStr];
        }
        else
        {
            [innercontactsAry addObject:@""];
        }
    }
    NSMutableArray* inneremailAry = [[NSMutableArray alloc]init];
    for (int i = 0; i < 5; i++) {
        
        if (emailAry.count > i) {
            [inneremailAry addObject:[emailAry objectAtIndex:i]];
        }
        else
        {
            [inneremailAry addObject:@""];
        }
    }
    
    NSString *soapMessage = [NSString stringWithFormat:
                             @"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                             "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"http://123.176.47.85/\">\n"
                             "<soapenv:Header/><soapenv:Body>\n"
                             "<urn:emergencyContactsInfo soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\n"
                             "<emergencyContactsInfoInput xsi:type=\"urn:emergencyContactsInfoInput\">\n"
                             "<userMobileNo xsi:type=\"xsd:string\">%@</userMobileNo>\n"
                             "<mobileNoz xsi:type=\"xsd:string\">%@</mobileNoz>\n"
                             "<mobileNoz xsi:type=\"xsd:string\">%@</mobileNoz>\n"
                             "<mobileNoz xsi:type=\"xsd:string\">%@</mobileNoz>\n"
                             "<mobileNoz xsi:type=\"xsd:string\">%@</mobileNoz>\n"
                             "<mobileNoz xsi:type=\"xsd:string\">%@</mobileNoz>\n"
                             "<EmailIdz xsi:type=\"xsd:string\">%@</EmailIdz>\n"
                             "<EmailIdz xsi:type=\"xsd:string\">%@</EmailIdz>\n"
                             "<EmailIdz xsi:type=\"xsd:string\">%@</EmailIdz>\n"
                             "<EmailIdz xsi:type=\"xsd:string\">%@</EmailIdz>\n"
                             "<EmailIdz xsi:type=\"xsd:string\">%@</EmailIdz>\n"
                             "</emergencyContactsInfoInput>\n"
                             "</urn:emergencyContactsInfo>\n"
                             "</soapenv:Body>\n"
                             "</soapenv:Envelope>\n",[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME],[innercontactsAry objectAtIndex:0],[innercontactsAry objectAtIndex:1],[innercontactsAry objectAtIndex:2],[innercontactsAry objectAtIndex:3],[innercontactsAry objectAtIndex:4],[inneremailAry objectAtIndex:0],[inneremailAry objectAtIndex:1],[inneremailAry objectAtIndex:2],[inneremailAry objectAtIndex:3],[inneremailAry objectAtIndex:4]
                             ];
    
    
    NSURL *url = [NSURL URLWithString:SOS_SERVICE];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: [NSString stringWithFormat:@"%@,%@",SOS_SERVICE,SOS_EGY_INFO]forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]
                                         initWithRequest:theRequest];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation
                                               , id responseObject) {
        
        
        NSString *str = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        
        // NSDictionary *xmlDoc = [NSDictionary dictionaryWithXMLString:str];
        
        
        
        RXMLElement *rxml = [RXMLElement elementFromXMLString:str encoding:NSUTF8StringEncoding];
        
        
        [rxml iterateWithRootXPath:@"//return" usingBlock: ^(RXMLElement *player) {
          //  NSLog(@"Player: %@ %@",[player child:@"ReturnCode"], [player child:@"ResponseMsg"]);
        }];
        
        
        
        [[NSUserDefaults standardUserDefaults] setObject:StoredContactsAry forKey: SOS_CONTACTS];
        [[NSUserDefaults standardUserDefaults] setObject:emailAry forKey:SOS_EMAILS];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setObject:hiddenPwdTxtFld.text forKey:SOS_CODE];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        singleton = [Singleton sharedMySingleton];
        delegate = singleton.sosContactsDelegate;
        [delegate closeContactVw];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // code
      //  NSLog(@"Error: %@", error.localizedDescription);
    }
     ];
    [operation start];
    
}
@end
