//
//  SOSContacts.h
//  SOS
//
//  Created by shaili_macMini02 on 11/02/14.
//  Copyright (c) 2014 shaili_macMini02. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "searchVw.h"
#import "Singleton.h"
#import "contactTableView.h"
#import "sosAllContacts.h"


@protocol SOSContactDelegate;

@interface SOSContacts : UIView <UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,searchVwDelegate>
{
    UITableView* contacts_TableVw;
    UIButton* settingsBtn;
    
    NSMutableArray* contactsAry;
    NSMutableArray* emailAry;
    
    NSMutableArray* contactTxtFld_Ary;
    NSMutableArray* emailTxtFld_Ary;
    
    int contactCount;
    int emailCount;
    UITextField* currentTextFld;
    
    UILabel* deactCodeLbl;
    UIView* passCodeVw;
    UIButton* submitBtn;
    
    UIView*searchLish_vw;
    UITextField* hiddenPwdTxtFld;
    UIScrollView* baseScrollVw;
    
    UIButton* doneButton;
    Singleton* singleton;
    UIView*  contactView;
}

-(UIView*)createView :(NSMutableArray*)contAry :(NSMutableArray*)emailIdAry :(NSString*)code;
@property(nonatomic,assign) id <SOSContactDelegate> delegate;
//@property(nonatomic,retain) sosAllContacts *contactTable;
@property(nonatomic,retain) contactTableView *contactTable;

@end


@protocol SOSContactDelegate <NSObject>

-(void)closeContactVw;
@end
