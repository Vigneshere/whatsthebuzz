//
//  SOSContacts.h
//  SOS
//
//  Created by shaili_macMini02 on 11/02/14.
//  Copyright (c) 2014 shaili_macMini02. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "searchVw.h"
#import "Singleton.h"
#import "contactTableView.h"
#import "sosAllContacts.h"
#import "SOSTableCell.h"
#import "PasswordPin.h"

@protocol SOSContactDelegate <NSObject>

-(void)closeContactVw;
@end

@interface SOSNEWContacts : UIView <UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,searchVwDelegate,sosAllContactsDelegate,SOSTableCellDelegate,sosContactPopUpDelegate>
{
    UITableView* contacts_TableVw;
    UIButton* settingsBtn;
    
    NSMutableArray* selContactsAry;
    
    NSIndexPath *selectedRow;
    NSMutableDictionary *contactsDic;
    
   
    UIButton* submitBtn;
    
    UIScrollView* baseScrollVw;
    
    UIButton* doneButton;
    Singleton* singleton;
    NSPredicate *contactPredicate;
    PasswordPin* passwordVw;


}

-(UIView*)createView;
@property(nonatomic,assign) id <SOSContactDelegate> delegate;

@property(nonatomic,retain) sosAllContacts*  contactView;
@property (nonatomic, retain)  NSMutableArray *contactArray;
@property (nonatomic, retain)  NSMutableArray *searchAry;
@end



