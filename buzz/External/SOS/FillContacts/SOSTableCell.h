//
//  SOSTableCell.h
//  buzz
//
//  Created by shaili_solutions_MacPro3 on 18/08/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SOSTableCellDelegate <NSObject>

-(void)SOSTableCellDeleteIndexSelected:(UITableViewCell*)cell;
-(void)SOSEmailTextFieldDidBeginEditing:(UITextField*)emailTextField;


@end


@interface SOSTableCell : UITableViewCell <UITextFieldDelegate>

@property (nonatomic, retain) UIImageView *mainBox;
@property (nonatomic, retain) UILabel *nameLbl;
@property (nonatomic, retain) UILabel *numberLbl;
@property (nonatomic, retain) UITextField *emailTxtFld;
@property (nonatomic, retain) UIButton *minusBtn;
@property(nonatomic,assign) id <SOSTableCellDelegate> delegate;

@end
