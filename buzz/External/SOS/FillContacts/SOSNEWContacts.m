//
//  SOSContacts.m
//  SOS
//
//  Created by shaili_macMini02 on 11/02/14.
//  Copyright (c) 2014 shaili_macMini02. All rights reserved.
//

#import "SOSNEWContacts.h"
#import "TTTOrdinalNumberFormatter.h"
#import "AFHTTPClient.h"
#import "AFJSONRequestOperation.h"
#import "RXMLElement.h"
#import "sosAllContacts.h"
#import "sosContactObject.h"
#import "PasswordPin.h"

@implementation SOSNEWContacts
@synthesize delegate;

#pragma mark - remove DomainName from String

-(NSString*)removeDomainName_String:(NSString*)string
{
    return [string stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"@%@",DOMAINAME] withString:@""];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    
    
    
    
    
    return self;
}

-(void)moveUp:(NSNotification *)notification
{
    CGSize kbSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    NSTimeInterval duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, kbSize.height + 20, 0);
        
        [contacts_TableVw setContentInset:edgeInsets];
        [contacts_TableVw setScrollIndicatorInsets:edgeInsets];
    }];
    
}

-(void)moveDown:(NSNotification *)notification
{
    NSTimeInterval duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsZero;
        [contacts_TableVw setContentInset:edgeInsets];
        [contacts_TableVw setScrollIndicatorInsets:edgeInsets];
    }];
}

-(UIView*)createView
{
    // Do any additional setup after loading the view, typically from a nib.
    if(IS_IPHONE_4)
    {
        self.backgroundColor = BACKGROUNDIMAGE_4;
        
    }
    else
    {
        self.backgroundColor = BACKGROUNDIMAGE;
        
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeView:) name:@"privatePassword" object:nil];
     [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(resignPasswordView:) name:@"SOSPasswordOK" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(moveUp:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(moveDown:) name:UIKeyboardWillHideNotification object:nil];

   
    
    contactsDic  = [[NSMutableDictionary alloc]init];

    NSMutableArray * EncodedSelContactsAry = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:SOS_CONTACTDATA]];
    selContactsAry = [NSMutableArray new];
    for (NSData *personEncodedObject in EncodedSelContactsAry) {
        sosContactObject *personObject = [NSKeyedUnarchiver unarchiveObjectWithData:personEncodedObject];
      //  NSLog(@"%@-------->%@",personObject.selectedNumber,personObject.selectedEmail);
        
        [contactsDic setObject:personObject.selectedEmail forKey:[NSString stringWithFormat:@"%@",personObject.selectedNumber]];
        [selContactsAry addObject:personObject];
    }

    
    self.contactArray = [[[[self appDelegate].contactsAry mutableCopy] sortedArrayUsingDescriptors:[NSSortDescriptor sortValues:@"firstNames" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]] mutableCopy];
    self.searchAry  = [self.contactArray valueForKeyPath:@"@distinctUnionOfObjects.firstLetter"];

   
    
    baseScrollVw = [[UIScrollView alloc]init];
    baseScrollVw.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    baseScrollVw.scrollEnabled = NO;
//    baseScrollVw.contentSize = CGSizeMake(self.frame.size.width, self.frame.size.height);
    [self addSubview:baseScrollVw];
    
    contacts_TableVw = [[UITableView alloc]initWithFrame:CGRectMake(baseScrollVw.frame.origin.x,5, baseScrollVw.frame.size.width, baseScrollVw.frame.size.height - 80 -64) style:UITableViewStylePlain];

    
    
    
    
    
    contacts_TableVw.frame = CGRectMake(baseScrollVw.frame.origin.x,67, baseScrollVw.frame.size.width, baseScrollVw.frame.size.height - 80 -67);
    contacts_TableVw.clipsToBounds = NO;
    contacts_TableVw.backgroundColor = [UIColor clearColor];
    contacts_TableVw.delegate = self;
    contacts_TableVw.dataSource = self;
    [contacts_TableVw setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [baseScrollVw addSubview:contacts_TableVw];
    
    UIImage* bottomImg = [UIImage imageNamed:@"SOS_SettingBottom"];
    UIView* bottomVw = [UIView new];
    bottomVw.userInteractionEnabled = NO;
    bottomVw.frame = CGRectMake(0, baseScrollVw.frame.size.height - (bottomImg.size.height/1.2),  self.frame.size.width,  bottomImg.size.height);
    bottomVw.backgroundColor = [UIColor colorWithPatternImage:bottomImg];
    [baseScrollVw addSubview:bottomVw];
    
  
    
    submitBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    if ([UIScreen mainScreen].bounds.size.height == 667.0 || [UIScreen mainScreen].bounds.size.height == 736.0)
    {
        submitBtn.frame = CGRectMake((self.frame.size.width - 90)/2,baseScrollVw.frame.origin.y + baseScrollVw.frame.size.height - 120, 90, 30);
    }
    else
    {
        submitBtn.frame = CGRectMake((self.frame.size.width - 90)/2, baseScrollVw.frame.size.height - 100, 90, 30);
        
    }
    [submitBtn setTitle:@"Submit" forState:UIControlStateNormal];
    [submitBtn addTarget:self action:@selector(GO_BTN_CLICKED) forControlEvents:UIControlEventTouchUpInside];
    [submitBtn setTintColor:RGBA(119, 186, 240, 1)];
    submitBtn.titleLabel.font = [UIFont fontWithName:FontString size:18];
    submitBtn.layer.borderColor = RGBA(119, 186, 240, 1).CGColor;
    submitBtn.layer.borderWidth = 1.4f;
    submitBtn.layer.cornerRadius = 30/2;
    [self addSubview:submitBtn];
    
    return self;
}


-(void)GO_BTN_CLICKED
{

    
    [self WebServicesCONTACTS:selContactsAry];
    
    
    NSString *sosPasswordStr = [[NSUserDefaults standardUserDefaults]valueForKey:@"SOSPASSWORD"];
    
    if (sosPasswordStr.length>0)
    {
        
    [self saveContactDataArray:selContactsAry];
    if (delegate)
    {
        [delegate closeContactVw];
    }
    }
    else
    {
    fromSOS = YES;
    passwordVw = [[PasswordPin alloc]initWithFrame_onView:self LabelString:@"Set a deactivation code" password:@""];
    [passwordVw createPasswordVw];
    }
    
    }

-(void)closeView:(NSNotification *)notification
{
    [passwordVw removeFromSuperview];
}

-(void)resignPasswordView:(NSNotification *)notification
{
    
    [self saveContactDataArray:selContactsAry];
    if (delegate)
    {
        [delegate closeContactVw];
    }
}

- (void)saveContactDataArray:(NSMutableArray *)selectedContactAry {
    
    NSMutableArray *archiveArray = [NSMutableArray arrayWithCapacity:selectedContactAry.count];
    for (sosContactObject *personObject in selectedContactAry) {
        NSData *personEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:personObject];
        
        contactsDic = [[NSUserDefaults standardUserDefaults]valueForKey:@"NUMBEREMAIL"];
        if ([contactsDic valueForKey:personObject.selectedNumber])
        {
            personObject.selectedEmail = [contactsDic valueForKey:personObject.selectedNumber];
        }
        
        [archiveArray addObject:personEncodedObject];
    }
    
    NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
    [userData setObject:archiveArray forKey:SOS_CONTACTDATA];
    [userData synchronize];
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *HeaderIdentifier = @"header";
    UITableViewHeaderFooterView *header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:HeaderIdentifier];
    
    if(!header) {
        header = [[UITableViewHeaderFooterView alloc] init];
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(1, 1), NO, 0.0);
        UIImage *blank = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        header.backgroundView = [[UIImageView alloc] initWithImage:blank];
        UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(0,0,self.frame.size.width,tableView.sectionHeaderHeight)];
        
        // create the label objects
        UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        headerLabel.backgroundColor = RGBA(119, 186, 240, 0.7);
        headerLabel.font = [UIFont fontWithName:FontString size:14];
        headerLabel.frame = CGRectMake((tableView.frame.size.width-90)/2,((tableView.sectionHeaderHeight+20 )/2) - 11.5,90,23);
        headerLabel.textColor = [UIColor whiteColor];
        headerLabel.textAlignment = NSTextAlignmentCenter;
        headerLabel.layer.cornerRadius = 23/2;
        headerLabel.clipsToBounds = YES;
        
        
        headerLabel.text = @"Add Contact";
        
        [customView addSubview:headerLabel];
        [header addSubview:customView];

    }
    
   
    return header;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger count;
    
        if (!([selContactsAry count] > 4)) {
            count =  [selContactsAry count] +1;
        }
        else
        {
            count = [selContactsAry count];
        }
    
    return count;
    //count number of row from counting array hear cataGorry is An Array
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return tableView.sectionHeaderHeight+ 23;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    return 120;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOSTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if( cell == nil)
    {
        cell = [[SOSTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell.delegate = self;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if( indexPath.row < selContactsAry.count)
    {
        sosContactObject* contact = (sosContactObject*)[selContactsAry objectAtIndex:indexPath.row];
        cell.nameLbl.text = contact.name;
        cell.numberLbl.text = contact.selectedNumber;
        
        NSMutableDictionary *dict = [[NSUserDefaults standardUserDefaults]valueForKey:@"NUMBEREMAIL"];
        if (dict>0)
        {
            NSString *selectedContact = [dict valueForKey:contact.selectedNumber];
            
            if (selectedContact.length>1)
            {
                contact.selectedEmail = [dict valueForKey:contact.selectedNumber];
            }
            
        }
            cell.emailTxtFld.text = contact.selectedEmail;
        
    }
    else
    {
        cell.nameLbl.text = @"";
        cell.numberLbl.text = @"";
        cell.emailTxtFld.text = @"";
    }
    
    UITapGestureRecognizer *tap  = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeThisSOSData:)];
    [cell.minusBtn addGestureRecognizer:tap];

    return cell;
    
}


-(void)removeThisSOSData:(UITapGestureRecognizer *)gesture
{
    NSLog(@"%@",gesture);
    CGPoint swipeLocation = [gesture locationInView:contacts_TableVw];
    NSIndexPath *swipedIndexPath = [contacts_TableVw indexPathForRowAtPoint:swipeLocation];
    NSLog(@"%d",swipedIndexPath.row);
    
    if (swipedIndexPath.row < [selContactsAry count] ){
        sosContactObject* selectedContact = [selContactsAry objectAtIndex:swipedIndexPath.row];
        
        contactPredicate = [NSPredicate predicateWithFormat:@"uniqueId == %@", selectedContact.uniqueId];
        for (contactData* tempContact in [NSMutableArray arrayWithArray:[self.contactArray filteredArrayUsingPredicate:contactPredicate]]){
            tempContact.selectedNumber = @"";
            tempContact.selectedEmail = @"";
        }
        
        [selContactsAry removeObject:selectedContact];
        [self  reloadData:YES];}
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)index{
    
  //  NSLog(@"selected %ld",(long)index.row);
    
    selectedRow = index;
   

   
    if (index.row < [selContactsAry count] )
    {
        sosContactObject* selectedContact = [selContactsAry objectAtIndex:index.row];
        
        contactPredicate = [NSPredicate predicateWithFormat:@"uniqueId == %@", selectedContact.uniqueId];
        for (contactData* tempContact in [NSMutableArray arrayWithArray:[self.contactArray filteredArrayUsingPredicate:contactPredicate]])
        {
            sosContactPopUp* contactPopUp;
            
            contactPopUp = [[sosContactPopUp alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height) view:self.superview contactData:tempContact];
            contactPopUp.delegate = self;
            
        }
        
        
    }
    else
    {
    [self showContactTableView];
    }
}

-(void)SOSEmailTextFieldDidBeginEditing:(UITextField *)emailTextField
{
    
        UITextField *textField = (UITextField *)emailTextField;
        CGPoint buttonPosition = [textField convertPoint:CGPointZero toView:contacts_TableVw];
        NSIndexPath *indexPath = [contacts_TableVw indexPathForRowAtPoint:buttonPosition];
        selectedRow = indexPath;
    
    sosContactObject  *selectedContact = [selContactsAry objectAtIndex:selectedRow.row];
    
    selectedContact.selectedEmail = [NSString stringWithFormat:@"%@",emailTextField.text];
    
    [contactsDic removeObjectForKey:selectedContact.selectedNumber];
    [contactsDic setObject:[NSString stringWithFormat:@"%@",emailTextField.text] forKey:selectedContact.selectedNumber];
    
  //  NSLog(@"Phone & Email ID %@",contactsDic);
    [[NSUserDefaults standardUserDefaults]setValue:contactsDic forKey:@"NUMBEREMAIL"];
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark popup Done Button Action in Contact detail view
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(void)PopUpdoneBtnCliked:(contactData*)contact
{
    contactPredicate = [NSPredicate predicateWithFormat:@"uniqueId == %@", contact.uniqueId];
    for (sosContactObject* tempContact in [NSMutableArray arrayWithArray:[selContactsAry filteredArrayUsingPredicate:contactPredicate]])
    {
        tempContact.selectedNumber = contact.selectedNumber;
     //   tempContact.selectedEmail = contact.selectedEmail;
        
    }
    [self  reloadData:YES];
}


-(void)SOSTableCellDeleteIndexSelected:(UITableViewCell*)cell
{
   
    NSIndexPath* index = [contacts_TableVw indexPathForCell:cell];
    if (index.row < [selContactsAry count] )
    {
        sosContactObject* selectedContact = [selContactsAry objectAtIndex:index.row];
        
        contactPredicate = [NSPredicate predicateWithFormat:@"uniqueId == %@", selectedContact.uniqueId];
        for (contactData* tempContact in [NSMutableArray arrayWithArray:[self.contactArray filteredArrayUsingPredicate:contactPredicate]])
        {
            tempContact.selectedNumber = @"";
            tempContact.selectedEmail = @"";
        }
        
        [selContactsAry removeObject:selectedContact];
        [self  reloadData:YES];
    }
    }

- (void)reloadData:(BOOL)animated
{
    
    [contacts_TableVw reloadData];
    
    
    if (animated) {
        
        CATransition *animation = [CATransition animation];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCAAnimationPaced];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [animation setFillMode:kCAFillModeBoth];
        [animation setDuration:.3];
        [[contacts_TableVw layer] addAnimation:animation forKey:@"UITableViewReloadDataAnimationKey"];
        
    }
}


-(void)showContactTableView
{
    
    UIViewController* viewController = (UIViewController*)delegate;
    if (viewController)
    {
        [viewController.navigationController setNavigationBarHidden:YES animated:YES];
        
        
        if (!self.contactView)
        {
            self.contactView = [[sosAllContacts alloc]initWithFrame:CGRectMake(0, self.frame.size.height+20, self.frame.size.width, self.frame.size.height)];
            self.contactView.contactArray = self.contactArray;
            self.contactView.delegate = self;
            self.contactView.contactSecArray  = self.searchAry;
            self.contactView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg"]];
            self.contactView.SelectedContactArray = selContactsAry;
            [self addSubview:self.contactView];
            
            [UIView animateWithDuration:0.30 animations:^{
                self.contactView.frame =  CGRectMake(0, 0, self.frame.size.width, self.contactView.frame.size.height);
                self.contactView.alpha = 1;
                
                
            }];
        }
        else
        {
            self.contactView.SelectedContactArray = selContactsAry;
            [self.contactView reloadAllContactTable];
            [UIView animateWithDuration:0.30 animations:^{
                self.contactView.frame =  CGRectMake(0, 0, self.frame.size.width, self.contactView.frame.size.height);
                self.contactView.alpha = 1;
                
                
            }];
        }
    }

}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Dont Button Action in Contact TableView
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)doneBtnCliked:(NSMutableArray*)selectedAry
{
    selContactsAry = selectedAry;
    
    UIViewController* viewController = (UIViewController*)delegate;
    if (viewController)
    {
        [viewController.navigationController setNavigationBarHidden:NO animated:YES];
        
    }

    
    [self  endEditing:YES];
    [UIView animateWithDuration:0.30 animations:^{
        self.contactView.frame =  CGRectMake(0, self.contactView.frame.size.height+10, self.frame.size.width, self.contactView.frame.size.height);
        self.contactView.alpha = 0;
    }];
    [contacts_TableVw reloadData];
    
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark DataBase Methods For Recent Chats
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (XMPPUserCoreDataStorageObject *)RecentcontactsFetch:(NSString *)Str
{
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:[self managedObjectContext_roster]];
    [request setEntity:entity];
    NSString *predicateFrmt = @"jidStr = %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, Str];
    [request setPredicate:predicate];
    NSArray *empArray=[[self managedObjectContext_roster] executeFetchRequest:request error:nil];
    if ([empArray count] == 1)
    {
        for (XMPPUserCoreDataStorageObject *user in  empArray)
        {
            return user;
        }
    }
    return nil;
}


- (NSManagedObjectContext *)managedObjectContext_roster
{
	return [[[self appDelegate] xmppRosterStorage] mainThreadManagedObjectContext];
}

- (AppDelegate *)appDelegate
{
	return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

#pragma mark - Web Services CONTACTS

-(void)WebServicesCONTACTS:(NSMutableArray*)ContactAry
{
    // NSLog(@"%@",imgStr);
    NSMutableArray* innercontactsAry = [[NSMutableArray alloc]init];
    NSMutableArray* inneremailAry = [[NSMutableArray alloc]init];

    for (int i = 0; i < 5; i++) {
        

        if (selContactsAry.count > i) {
            
            sosContactObject* selectedContact = [selContactsAry objectAtIndex:i];

            if (!selectedContact.selectedEmail) {
                selectedContact.selectedEmail = @"";
            }
            
            if (!selectedContact.selectedNumber) {
                selectedContact.selectedNumber = @"";
            }
            [inneremailAry addObject:selectedContact.selectedEmail];

            [innercontactsAry addObject:selectedContact.selectedNumber];
        }
        else
        {
            [innercontactsAry addObject:@""];
            [inneremailAry addObject:@""];

        }
    }
    
    NSString *soapMessage = [NSString stringWithFormat:
                             @"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                             "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"http://123.176.47.85/\">\n"
                             "<soapenv:Header/><soapenv:Body>\n"
                             "<urn:emergencyContactsInfo soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\n"
                             "<emergencyContactsInfoInput xsi:type=\"urn:emergencyContactsInfoInput\">\n"
                             "<userMobileNo xsi:type=\"xsd:string\">%@</userMobileNo>\n"
                             "<mobileNoz xsi:type=\"xsd:string\">%@</mobileNoz>\n"
                             "<mobileNoz xsi:type=\"xsd:string\">%@</mobileNoz>\n"
                             "<mobileNoz xsi:type=\"xsd:string\">%@</mobileNoz>\n"
                             "<mobileNoz xsi:type=\"xsd:string\">%@</mobileNoz>\n"
                             "<mobileNoz xsi:type=\"xsd:string\">%@</mobileNoz>\n"
                             "<EmailIdz xsi:type=\"xsd:string\">%@</EmailIdz>\n"
                             "<EmailIdz xsi:type=\"xsd:string\">%@</EmailIdz>\n"
                             "<EmailIdz xsi:type=\"xsd:string\">%@</EmailIdz>\n"
                             "<EmailIdz xsi:type=\"xsd:string\">%@</EmailIdz>\n"
                             "<EmailIdz xsi:type=\"xsd:string\">%@</EmailIdz>\n"
                             "</emergencyContactsInfoInput>\n"
                             "</urn:emergencyContactsInfo>\n"
                             "</soapenv:Body>\n"
                             "</soapenv:Envelope>\n",[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME],[innercontactsAry objectAtIndex:0],[innercontactsAry objectAtIndex:1],[innercontactsAry objectAtIndex:2],[innercontactsAry objectAtIndex:3],[innercontactsAry objectAtIndex:4],[inneremailAry objectAtIndex:0],[inneremailAry objectAtIndex:1],[inneremailAry objectAtIndex:2],[inneremailAry objectAtIndex:3],[inneremailAry objectAtIndex:4]
                             ];
    
    
    NSURL *url = [NSURL URLWithString:SOS_SERVICE];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
    
    NSString *userLocation = [NSString stringWithFormat:@"%@/%@",SOS_SERVICE,SOS_EGY_INFO];

    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: userLocation forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]
                                         initWithRequest:theRequest];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation
                                               , id responseObject) {
        
        
        NSString *str = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        
        // NSDictionary *xmlDoc = [NSDictionary dictionaryWithXMLString:str];
        
        
        
        RXMLElement *rxml = [RXMLElement elementFromXMLString:str encoding:NSUTF8StringEncoding];
        
        
        [rxml iterateWithRootXPath:@"//return" usingBlock: ^(RXMLElement *player) {
          //  NSLog(@"Player: %@ %@",[player child:@"ReturnCode"], [player child:@"ResponseMsg"]);
        }];
        
        
        [self saveContactDataArray:selContactsAry];

        
        
        if (delegate)
        {
       //     [delegate closeContactVw];

        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // code
      //  NSLog(@"Error: %@", error.localizedDescription);
    }
     ];
    [operation start];
    
}




//-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
//{
//    if (!decelerate) {
//        NSArray *visibleRows = [contacts_TableVw indexPathsForVisibleRows];
//        NSMutableIndexSet *sections = [[NSMutableIndexSet alloc] init];
//        for (NSIndexPath *indexPath in visibleRows) {
//            [sections addIndex:indexPath.section];
//        }
//
//
//        [sections enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
//            UIView *headerView = [contacts_TableVw headerViewForSection:idx];
//            NSIndexPath* indexPath = [NSIndexPath indexPathForRow:0 inSection:idx];
//            UITableViewCell *cellView = [contacts_TableVw cellForRowAtIndexPath:indexPath];
//
//            if (headerView.frame.origin.y+ headerView.frame.size.height > cellView.frame.origin.y)
//            {
//                headerView.hidden = YES;
//
//            }
//            else
//            {
//                headerView.hidden = NO;
//
//            }
//
//
//            NSLog(@"%f   %f",headerView.frame.origin.y+ headerView.frame.size.height , cellView.frame.origin.y);
//        }];
//    }
//}
//
//-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    NSArray *visibleRows = [contacts_TableVw indexPathsForVisibleRows];
//    NSMutableIndexSet *sections = [[NSMutableIndexSet alloc] init];
//    for (NSIndexPath *indexPath in visibleRows) {
//        [sections addIndex:indexPath.section];
//    }
//
//
//    [sections enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
//        UIView *headerView = [contacts_TableVw headerViewForSection:idx];
//        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:0 inSection:idx];
//        UITableViewCell *cellView = [contacts_TableVw cellForRowAtIndexPath:indexPath];
//
//
//        if (headerView.frame.origin.y+ headerView.frame.size.height > cellView.frame.origin.y)
//        {
//            headerView.hidden = YES;
//
//        }
//        else
//        {
//            headerView.hidden = NO;
//
//        }
//
//        NSLog(@"%f   %f",headerView.frame.origin.y+ headerView.frame.size.height , cellView.frame.origin.y);
//    }];}

-(void)dealloc
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}


@end
