//
//  searchVw.h
//  listVwCarSnap
//
//  Created by Prabu on 21/12/13.
//  Copyright (c) 2013 JemsNets. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMIndexBar.h"
#import "Singleton.h"

@protocol searchVwDelegate;
@interface searchVw : UIView<UITableViewDelegate,UITableViewDataSource,CMIndexBarDelegate,UITextFieldDelegate,UISearchBarDelegate>
{
    UITableView*tableView;
    NSArray *content;
    NSArray *indices;
    UITextField* searchFld;
    Singleton* singleton;
    NSMutableArray* dynamicAry;
    CMIndexBar *indexBar;
    NSInteger returnIndex;
    UISearchBar* mySearchBar;
}
@property(nonatomic,assign) id <searchVwDelegate> delegate;

-(UIView*)createVw :(NSMutableArray*)receivedAry :(NSString*)searchType :(NSInteger)index;
@end 

@protocol searchVwDelegate <NSObject>
-(void)searchResult:(NSString*)str :(NSInteger)returnIndex;
-(void)closeSearchVw;
@end
