//
//  searchVw.m
//  listVwCarSnap
//
//  Created by Prabu on 21/12/13.
//  Copyright (c) 2013 JemsNets. All rights reserved.
//

#import "searchVw.h"
#import "CMIndexBar.h"
#import "Singleton.h"

@implementation searchVw
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor  = [UIColor colorWithRed:247/250.0 green:247/255.0 blue:247/255.0 alpha:1.0];
        
        tableView = [[UITableView alloc]init];
        searchFld = [[UITextField alloc]init];
         indexBar = [[CMIndexBar alloc] init];
    
    }
    return self;
}


-(UIView*)createVw :(NSMutableArray*)receivedAry :(NSString*)searchType :(NSInteger)index
{
    
    returnIndex = index;
    dynamicAry = [receivedAry mutableCopy];
    
    content =  [self alignAry:dynamicAry];
    indices = [content valueForKey:@"headerTitle"];
    
    UIView* vw = [[UIView alloc]initWithFrame:CGRectMake(5,7.5, 320 - 10, 25)];
    vw.backgroundColor = [UIColor greenColor];
    vw.layer.cornerRadius = 4;
    [self addSubview:vw];
    
    mySearchBar = [[UISearchBar alloc] init];
    mySearchBar.frame = CGRectMake(0,0, 320 - 60, 25);
    mySearchBar.placeholder = @"Type a search term";
    mySearchBar.tintColor = [UIColor blackColor];
    mySearchBar.backgroundColor = [UIColor grayColor];
    mySearchBar.delegate = self;
    [mySearchBar setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [mySearchBar sizeToFit];
    //[vw addSubview:mySearchBar];
    
    searchFld.frame = CGRectMake(0,0, 320 - 60, 25);
    searchFld.backgroundColor = [UIColor clearColor];
    searchFld.textAlignment = NSTextAlignmentCenter;
    searchFld.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    searchFld.placeholder = @"Search";
    searchFld.font = [UIFont fontWithName:FontString size:13];
    searchFld.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    searchFld.delegate = self;
    [searchFld setValue:[UIColor darkGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    [vw addSubview:searchFld];
    
   UIButton* dropVw_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    dropVw_btn.frame = CGRectMake(320 - 60,0,40, 25);
    [dropVw_btn addTarget:self action:@selector(dropVw_btn_clicked) forControlEvents:UIControlEventTouchUpInside];
    [vw addSubview:dropVw_btn];
    
    UIView* dropVwBtn_Img = [[UIView alloc]initWithFrame:CGRectMake((dropVw_btn.frame.size.width - 16)/2, (dropVw_btn.frame.size.height - 8)/2, 16, 8)];
    dropVwBtn_Img.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"down-arrowVw.png"]];
    dropVwBtn_Img.userInteractionEnabled = NO;
    [dropVw_btn addSubview:dropVwBtn_Img];
    
    
    UILabel* headingLbl = [[UILabel alloc]initWithFrame:CGRectMake(0,vw.frame.origin.y+ vw.frame.size.height + 2.5, 320, 40)];
    headingLbl.textAlignment = NSTextAlignmentLeft;
    headingLbl.text = [NSString stringWithFormat:@"    Select a %@",searchType];
    headingLbl.backgroundColor = [UIColor whiteColor];
    headingLbl.adjustsFontSizeToFitWidth = YES;
    [self addSubview:headingLbl];
    
    tableView.frame = CGRectMake(0,headingLbl.frame.origin.y +headingLbl.frame.size.height, self.frame.size.width,self.frame.size.height - (headingLbl.frame.origin.y +headingLbl.frame.size.height));
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.backgroundColor = [UIColor clearColor];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self addSubview:tableView];
    
   
    indexBar.frame = CGRectMake(tableView.frame.size.width-20,tableView.frame.origin.y , 20, tableView.frame.size.height);
    indexBar.delegate = self;
    indexBar.backgroundColor = [UIColor whiteColor];
    [indexBar setIndexes:indices];
    [self addSubview:indexBar];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    tapGesture.numberOfTapsRequired = 1;
    [self addGestureRecognizer:tapGesture];
    
    
    
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGesture:)];
    swipeGesture.direction = UISwipeGestureRecognizerDirectionDown;
    [self addGestureRecognizer:swipeGesture];
    
    return self;
}


-(void)generateDynamicAry:(NSString*)string
{
    NSMutableArray* tempAry =[[NSMutableArray alloc]init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",string];
    tempAry = [NSMutableArray arrayWithArray:[dynamicAry filteredArrayUsingPredicate:predicate]];
//
//    for (int i =0; i < [dynamicAry count]; i++) {
//        if([[[dynamicAry objectAtIndex:i] lowercaseString] hasPrefix:[string lowercaseString]] || string.length == 0)
//        {
//            [tempAry addObject:[dynamicAry objectAtIndex:i]];
//        }
//    }
    
    if ([tempAry count] >0) {
        content =  [self alignAry:tempAry];
        indices = [content valueForKey:@"headerTitle"];
        [indexBar setIndexes:indices];
    }
    else
    {
        content = [[NSArray alloc]init];
        indices = [[NSArray alloc]init];
        [indexBar setIndexes:indices];
    }
    
    [tableView reloadData];
    
}
- (void)indexSelectionDidChange:(CMIndexBar *)indexBar index:(NSInteger)index title:(NSString*)title
{
  //  NSLog(@"%ld",(long)index);
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:index];
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}


-(void)handleSwipeGesture:(UISwipeGestureRecognizer *) sender
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.3];
    
    [searchFld resignFirstResponder];
    tableView.scrollEnabled = YES;
    [UIView commitAnimations];
    
    
    
}

-(void)handleTapGesture:(UITapGestureRecognizer *) sender
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.3];
    
    [searchFld resignFirstResponder];
    tableView.scrollEnabled = YES;
    [UIView commitAnimations];

}

- (void)setDelegate:(id<searchVwDelegate>)val {
    
    delegate = val;
    singleton = [Singleton sharedMySingleton];
    singleton.searchVwDelegate = delegate;
}

#pragma mark - UITextfieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    tableView.scrollEnabled = NO;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString * proposedNewString = [[textField text] stringByReplacingCharactersInRange:range withString:string];
    
    [self generateDynamicAry:proposedNewString];
    
    
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    tableView.scrollEnabled = YES;
    return YES;
}


-(NSArray*)alignAry :(NSMutableArray *)ary
{
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:nil
                                                  ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray;
    sortedArray = [ary sortedArrayUsingDescriptors:sortDescriptors];
    NSLog(@"sortedArray%@",sortedArray);
    return [self createAry:sortedArray];
    
}

-(NSArray*)createAry:(NSArray*)ary
{
    NSMutableArray* finalAry = [[NSMutableArray alloc]init];
    NSString* str = [[ary objectAtIndex:0] substringToIndex:1];
    NSMutableArray* tempAry = [[NSMutableArray alloc]init];
    for (int i = 0; i < [ary count]; i++) {
        
        
        if ([str caseInsensitiveCompare:[[ary objectAtIndex:i] substringToIndex:1]] == NSOrderedSame) {
            [tempAry addObject:[ary objectAtIndex:i]];
        }
        else
        {
            if ([tempAry count] > 0) {
                NSMutableDictionary* tempDict  = [[NSMutableDictionary alloc]init];
                [tempDict setObject:[str uppercaseString] forKey:@"headerTitle"];
                [tempDict setObject:tempAry forKey:@"rowValues"];
                [finalAry addObject:tempDict];
                tempAry = [[NSMutableArray alloc]init];
                [tempAry addObject:[ary objectAtIndex:i]];
            }
            
            str = [[ary objectAtIndex:i] substringToIndex:1];
            
            
        }
        if (i+1 == [ary count]) {
            NSMutableDictionary* tempDict  = [[NSMutableDictionary alloc]init];
            [tempDict setObject:str forKey:@"headerTitle"];
            [tempDict setObject:tempAry forKey:@"rowValues"];
            [finalAry addObject:tempDict];
            
        }
    }
    
    return finalAry;
}

-(void)dropVw_btn_clicked
{
    singleton = [Singleton sharedMySingleton];
    delegate = singleton.searchVwDelegate;
    [delegate closeSearchVw];
}



#pragma mark -
#pragma mark Table view data source

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [content count];
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[[content objectAtIndex:section] objectForKey:@"rowValues"] count] ;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableVieww cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableVieww dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UIView* line = [[UIView alloc]init];
        line.tag = 100;
        line.frame = CGRectMake(15, cell.frame.size.height - 1, cell.frame.size.width - 30, 1);
        line.backgroundColor = RGBA(201, 199, 204, 1);
        [cell.contentView addSubview:line];
        
    }
    
      UIView *line=(UIView*)[cell.contentView viewWithTag:100];
    if ([[[content objectAtIndex:indexPath.section] objectForKey:@"rowValues"] count] == indexPath.row + 1) {
        line.hidden = YES;
    }
    else
    {
        line.hidden = NO;
    }
    
    cell.textLabel.text = [[[content objectAtIndex:indexPath.section] objectForKey:@"rowValues"]
                           objectAtIndex:indexPath.row];
    
    for(UIView *view in [tableVieww subviews])
    {
        if([[[view class] description] isEqualToString:@"UITableViewIndex"])
        {
            
            [view setBackgroundColor:[UIColor clearColor]];
        }
    }
    return cell;
}


//- (NSString *)tableView:(UITableView *)aTableView titleForHeaderInSection:(NSInteger)section {
//	return [[content objectAtIndex:section] objectForKey:@"headerTitle"];
//    
//}
-(UIView *)tableView:(UITableView *)tableVieww viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableVieww.frame.size.width, 18)];
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 3, tableVieww.frame.size.width, 18)];
    [label setFont:[UIFont fontWithName:FontString size:16]];
    

    NSString *string =[[content objectAtIndex:section] objectForKey:@"headerTitle"];
    /* Section header is in 0th index... */
    [label setText:string];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor colorWithRed:247/255.0 green:247/255.0 blue:247/255.0 alpha:1.0]]; //your background color...
    return view;
}





#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//	NSLog(@"%@",[[[content objectAtIndex:indexPath.section] objectForKey:@"rowValues"]
//                 objectAtIndex:indexPath.row]);
    singleton = [Singleton sharedMySingleton];
    delegate = singleton.searchVwDelegate;
    [delegate searchResult:[[[content objectAtIndex:indexPath.section] objectForKey:@"rowValues"]
                            objectAtIndex:indexPath.row]:returnIndex];
}





#pragma mark - Search Implementation

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
   
    
    [self generateDynamicAry:searchText];
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
  //  NSLog(@"Cancel clicked");
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
  //  NSLog(@"Search Clicked");
    
}

@end
