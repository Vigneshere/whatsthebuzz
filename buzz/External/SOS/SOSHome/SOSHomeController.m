//
//  SOSHomeController.m
//  SOS
//
//  Created by shaili_macMini02 on 11/02/14.
//  Copyright (c) 2014 shaili_macMini02. All rights reserved.
//

#import "SOSHomeController.h"
#import "SOSContacts.h"
#import "SOSNEWContacts.h"

#import <MessageUI/MFMessageComposeViewController.h>
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import "AFHTTPClient.h"
#import "AFJSONRequestOperation.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVCaptureSession.h>
#import <CoreLocation/CoreLocation.h>
#import "DIYCam.h"
#import "RXMLElement.h"
#import "MSViewControllerSlidingPanel.h"
#import "UIImage+Resize.h"
#import "messageMethods.h"
#import "LocationManagerSingleton.h"
#import "RMDownloadIndicator.h"
#import <QuartzCore/QuartzCore.h>
#import "mediaDbEditor.h"
#import "sosContactObject.h"

@interface SOSHomeController ()<UITextFieldDelegate,DIYCamDelegate,CLLocationManagerDelegate,SOSContactDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,MFMessageComposeViewControllerDelegate>
{
    UIView* navVw;
    UILabel* NavLbl;
    
    UIButton* settingsBtn;
    UIView* ContactsForm;
    BOOL contactFormVisible;
    UIImageView* activeStatus_ImgVw ;
    UIButton* activate_btn ;
     BOOL deactive_status;
    UIView* PassCodeVW;
    UITextField* hiddenPwdTxtFld;
     CLLocationManager *locationManager;
    CLLocation* internalNewLocation;
    CLLocation* internalOldLocation;
    UIScrollView* baseScrollVw;
    
    //
    DIYCam* cam;
    UIImagePickerController* picker;

    UIView* dummyVw;
    BOOL auto_Capture;
    
    UIImageView* frontVw;
    UIImageView* backVw;
    
    BOOL front_pic;
    BOOL Got_Location_At_First_time;
    UISwipeGestureRecognizer* SwipeLeft_Reg;
    
    NSString* frontImg_Str;
    NSString* rearImg_Str;
    
    UIButton* chatBtn;
    MessageMethods* messageMethods;
    LocationManagerSingleton* locSingleton;
    
    RMDownloadIndicator* Loder;
    NSTimer *_timer;
    NSData* frontImg_Data_30;
    NSData* rearImg_Data_30;
    
    NSData* frontImg_Data_150;
    NSData* rearImg_Data_150;
    UILabel* btnLbl;
}

@end

@implementation SOSHomeController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Appdelegate Values
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (AppDelegate *)appDelegate
{
	return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (XMPPStream *)xmppStream
{
	return [[self appDelegate] xmppStream];
}

#pragma mark - view life cycle


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
  
    
    
    self.navigationController.navigationBar.translucent  = YES;

    baseScrollVw = [[UIScrollView alloc]init];
    hiddenPwdTxtFld = [[UITextField alloc]init];
    settingsBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    chatBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    activeStatus_ImgVw = [[UIImageView alloc]init];
    
    activate_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btnLbl = [UILabel new];
    
    
    PassCodeVW = [[UIView alloc]init];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
   

    if(IS_IPHONE_4)
    {
        self.view.backgroundColor = BACKGROUNDIMAGE_4;
    }
    else
    {
        self.view.backgroundColor = BACKGROUNDIMAGE;
        
    }

    baseScrollVw.backgroundColor = [UIColor clearColor];
    self.navigationItem.hidesBackButton = YES;
    contactFormVisible = NO;


    if( [UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceFront ] || [UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceRear ])
    {


    picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
    if( [UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceFront ])
    {
        picker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        front_pic = YES;

    }
    else
    {
        picker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        front_pic = NO;
    }
    
    picker.showsCameraControls = NO;
    picker.allowsEditing = NO;
    picker.navigationBarHidden = YES;
    picker.toolbarHidden = YES;
    picker.delegate = self;
    picker.cameraViewTransform = CGAffineTransformIdentity;
    [self.view addSubview:picker.view];
    

    picker.view.backgroundColor = [UIColor greenColor];
    picker.view.frame = CGRectMake((self.view.frame.size.width - 155+50)/2, (self.view.frame.size.height -210+50)/2, 100, 100);
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(cameraIsReady)
                                                 name:AVCaptureSessionDidStartRunningNotification object:nil];
    
    auto_Capture = NO;
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"OOPS!" message:@" No camera found." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    baseScrollVw.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:baseScrollVw];
    
    hiddenPwdTxtFld.keyboardType = UIKeyboardTypeNumberPad;
    hiddenPwdTxtFld.delegate = self;
    [baseScrollVw addSubview:hiddenPwdTxtFld];
    
    settingsBtn.frame = CGRectMake(5, (self.navigationController.navigationBar.frame.size.height - 38)/2, 64, 38);
    [settingsBtn setImage:[UIImage imageNamed:@"settings.png"] forState:UIControlStateNormal];
    [settingsBtn addTarget:self
                    action:@selector(settingsBtn_method)
          forControlEvents:UIControlEventTouchDown];
    
    [self.navigationController.navigationBar addSubview:settingsBtn];
    
   
    chatBtn.frame = CGRectMake(self.navigationController.navigationBar.frame.size.width - 64, (self.navigationController.navigationBar.frame.size.height - 25)/2, 54, 25);
    [chatBtn setTitle:@"Chat" forState:UIControlStateNormal];
    [chatBtn addTarget:self action:@selector(chatBtn_CLICKED) forControlEvents:UIControlEventTouchUpInside];
    chatBtn.titleLabel.font = [UIFont fontWithName:FontString size:14.0f];
    [chatBtn setTitleColor:RGBA(53, 152, 219, 1) forState:UIControlStateNormal];
    chatBtn.layer.borderColor = RGBA(53, 152, 219, 1).CGColor;
    chatBtn.layer.borderWidth = 1.2f;
    chatBtn.layer.cornerRadius = 0.5f;
    [self.navigationController.navigationBar addSubview:chatBtn];
    
    if ([PassCodeVW.subviews count] <= 0)
    {
        PassCodeVW = [self create_circles:4 radius:20 space:10 view:PassCodeVW];

    }
   
     PassCodeVW.hidden = YES;
    [baseScrollVw addSubview:PassCodeVW];
    
   
    
    
    
    
    activeStatus_ImgVw.frame = CGRectMake((self.view.frame.size.width - 155)/2, (self.view.frame.size.height -210)/2, 155, 155);
    activeStatus_ImgVw.clipsToBounds = YES;
    
    btnLbl.frame = CGRectMake(0, (activeStatus_ImgVw.frame.size.height - 40)/2, activeStatus_ImgVw.frame.size.width, 40);
    btnLbl.font = [UIFont systemFontOfSize:22];
    btnLbl.textAlignment = NSTextAlignmentCenter;
    btnLbl.backgroundColor = [UIColor clearColor];
    btnLbl.textColor = [UIColor whiteColor];
    [activeStatus_ImgVw addSubview:btnLbl];
    
    activeStatus_ImgVw.layer.cornerRadius = 155/2;
    activeStatus_ImgVw.backgroundColor = RGBA(90, 165, 1, 1);
    btnLbl.text = @"Activate";
    PassCodeVW.frame = CGRectMake((self.view.frame.size.width - PassCodeVW.frame.size.width)/2, activeStatus_ImgVw.frame.origin.y - PassCodeVW.frame.size.height - 40, PassCodeVW.frame.size.width, PassCodeVW.frame.size.height);
    
    activate_btn.frame = CGRectMake(95, 157, 130,130);
    activate_btn.backgroundColor = [UIColor clearColor];
    [activate_btn addTarget:self action:@selector(activate_btn_method) forControlEvents:UIControlEventTouchUpInside];
    activate_btn.layer.cornerRadius = 130/2;
   
    
    deactive_status = NO;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    [PassCodeVW addGestureRecognizer:tapGesture];
    
 //   UITapGestureRecognizer *tapGesture_keyBord = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGesture)];
    //[baseScrollVw addGestureRecognizer:tapGesture_keyBord];
    
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGesture)];
    swipeGesture.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:swipeGesture];
    
    Loder = [self addDownloadIndicators];

    
    
        [baseScrollVw addSubview:activeStatus_ImgVw];
    [baseScrollVw addSubview:Loder];
    [baseScrollVw addSubview:activate_btn];
    activate_btn.enabled = YES;
    
    SwipeLeft_Reg = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    [SwipeLeft_Reg setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [[self view] addGestureRecognizer:SwipeLeft_Reg];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:SOS_STATUS] isEqualToString:@"activate"]) {
        [self activeStatus];
    }
    
    
    [self appDelegate].navigationBarViewLine.hidden = YES;
    
    }


- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    
    self.navigationController.navigationBar.topItem.title = @"SOS";
    
    
    
}

- (RMDownloadIndicator*)addDownloadIndicators
{
    
    
    RMDownloadIndicator *filledIndicator = [[RMDownloadIndicator alloc]initWithFrame:CGRectMake((self.view.frame.size.width - 165)/2, (self.view.frame.size.height -220)/2, 165, 165) type:kRMClosedIndicator];
    [filledIndicator setBackgroundColor:[UIColor clearColor]];
    [filledIndicator setFillColor:RGBA(25, 245, 245, 1)];
    [filledIndicator setStrokeColor:RGBA(200, 200, 200, 1)];
    filledIndicator.radiusPercent = 0.91;
    filledIndicator.coverWidth = 10;
    [filledIndicator loadIndicator];
    filledIndicator.userInteractionEnabled = YES;
    return filledIndicator;
    
}
- (void)updateView:(CGFloat)val indicatorView:(RMDownloadIndicator*)indicatorView
{
    
    [indicatorView updateWithTotalBytes:100 downloadedBytes:val];
    
}

- (void)cameraIsReady{
    
    if (auto_Capture == YES) {
       
        if (frontImg_Str && rearImg_Str) {
            
        }
        else
        {
        [picker takePicture];
        }
    }
    
}




- (void)imagePickerController:(UIImagePickerController *)picker3 didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage* image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    NSDictionary *metaData = [info objectForKey:@"UIImagePickerControllerMediaMetadata"];
    
    switch ([[metaData objectForKey:@"Orientation"]integerValue])
    {
        case UIInterfaceOrientationLandscapeLeft:
         //   NSLog(@"Image Orientation is LandcapeLeft");

            break;

        case UIInterfaceOrientationLandscapeRight:
         //   NSLog(@"Image Orientation is LandcapeRight");
            
            break;
            
        case UIInterfaceOrientationPortrait:
          //  NSLog(@"Image Orientation is Portait");
            
            break;


        default:
            break;
    }
 
  
  
   image = [image resizedImage:CGSizeMake(image.size.width, image.size.height) interpolationQuality:kCGInterpolationHigh];
    
   
     UIImage* scaledImg = [UIImage imageWithImage:image scaledToWidth:self.view.frame.size.width];
    
   
    if (picker3.cameraDevice == UIImagePickerControllerCameraDeviceFront)
    {
        if( [UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceRear ])
        {
            picker.cameraDevice=UIImagePickerControllerCameraDeviceRear;
            [self performSelector:@selector(cameraIsReady) withObject:nil afterDelay:3];
            front_pic = NO;
            
        }
        
      
       frontImg_Data_30 = UIImageJPEGRepresentation([UIImage imageWithImage:scaledImg scaledToWidth:30], 0.1);
        frontImg_Data_150 = UIImageJPEGRepresentation([UIImage imageWithImage:scaledImg scaledToWidth:320], 0.7);

                frontImg_Str = [[NSString alloc]init];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:SOS_ID])
        {
             frontImg_Str = [self base64forData:UIImageJPEGRepresentation(scaledImg, 0.5)];
            [self imgUpload:[[NSUserDefaults standardUserDefaults] objectForKey:SOS_ID] :[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME] :frontImg_Str :YES];
           
        }
        else
        {
            frontImg_Str = [self base64forData:UIImageJPEGRepresentation(scaledImg, 0.5)];
        }
        
        
    }
    else
    {
         picker.cameraDevice=UIImagePickerControllerCameraDeviceFront;
        
        front_pic = YES;
        
        rearImg_Data_30  =   UIImageJPEGRepresentation([UIImage imageWithImage:scaledImg scaledToWidth:30], 0.1);
        rearImg_Data_150  =   UIImageJPEGRepresentation([UIImage imageWithImage:scaledImg scaledToWidth:320], 0.1);

        
           rearImg_Str = [[NSString alloc]init];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:SOS_ID])
        {
            rearImg_Str = [self base64forData:UIImageJPEGRepresentation(scaledImg, 0.5)];
            [self imgUpload:[[NSUserDefaults standardUserDefaults] objectForKey:SOS_ID] :[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME] :rearImg_Str :NO];
        }
        else
        {
            rearImg_Str = [self base64forData:UIImageJPEGRepresentation(scaledImg, 0.5)];
        }
    }
    
     image = nil;
    
}

-(NSString *)getUniqueFilenameInFolder:(NSString *)folder forFileExtension:(NSString *)fileExtension {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *existingFiles = [fileManager contentsOfDirectoryAtPath:folder error:nil];
    NSString *uniqueFilename;
    
    do {
        CFUUIDRef newUniqueId = CFUUIDCreate(kCFAllocatorDefault);
        CFStringRef newUniqueIdString = CFUUIDCreateString(kCFAllocatorDefault, newUniqueId);
        
        uniqueFilename = [[folder stringByAppendingPathComponent:(__bridge NSString *)newUniqueIdString] stringByAppendingPathExtension:fileExtension];
        
        CFRelease(newUniqueId);
        CFRelease(newUniqueIdString);
    } while ([existingFiles containsObject:uniqueFilename]);
    
    return uniqueFilename;
}



+ (NSString*)base64forData:(NSData*)theData {
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] ;
}
-(void)viewDidDisappear:(BOOL)animated
{
    if (contactFormVisible) {
        [self closeFormVw];
    }
    [picker removeFromParentViewController];
    picker = nil;
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}




#pragma mark - handleGesture

-(void)handleTapGesture:(UITapGestureRecognizer *) sender
{
    [hiddenPwdTxtFld becomeFirstResponder];
}

-(void)handleSwipeGesture
{
    
    
    [hiddenPwdTxtFld resignFirstResponder];
}

#pragma mark - btn methods

-(void)activate_btn_method

{
    [hiddenPwdTxtFld resignFirstResponder];
    NSMutableArray * EncodedSelContactsAry = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:SOS_CONTACTDATA]];
    
    if (_timer) {
        btnLbl.text = @"Activate";
        [self stopTimer];
        [Loder setIndicatorAnimationDuration:0.0];
        [Loder updateWithTotalBytes:100 downloadedBytes:0];
    }
    else
    {
    if (EncodedSelContactsAry.count) {
        
    
    if (deactive_status)
    {
        
        
        if (1 )
        {
            
                [self deactive_BTN_CLICKED];
            }
       else
       {
           UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Buzz!"
                                                             message:@"Wrong deactivation code"
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
            [message show];
       }
    }
    else
    {
        
        [self startTimer];
        auto_Capture = YES;
        [self cameraIsReady];
       
    }
    
    }
    else
    {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Buzz!"
                                                          message:@"please fill the SOS settings"
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
    }
    }
}

-(void)clearPassword
{
    hiddenPwdTxtFld.text = @"";
    for (int i =0; i < 4; i++) {
        UILabel* passcode = (UILabel*)[PassCodeVW viewWithTag:i+1];
        passcode.text = @"";
        
    }
}
-(void)settingsBtn_method
{
    
    [hiddenPwdTxtFld resignFirstResponder];
    if (!contactFormVisible) {
        
        [settingsBtn setImage:[UIImage imageNamed:@"back_btn"] forState:UIControlStateNormal];
        
        [ContactsForm removeFromSuperview];
        
                SOSNEWContacts* sosContactForm = [[SOSNEWContacts alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        sosContactForm.delegate = self;
        
        //to be deleted
        if (![[NSUserDefaults standardUserDefaults] objectForKey:SOS_CONTACTS]) {
            [[NSUserDefaults standardUserDefaults] setObject:[[NSMutableArray alloc]init] forKey:SOS_CONTACTS];
            [[NSUserDefaults standardUserDefaults] setObject:[[NSMutableArray alloc]init] forKey:SOS_EMAILS];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:SOS_CODE];
        }
       
               
         ContactsForm = [sosContactForm createView];

        ContactsForm.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width,ContactsForm.frame.size.height);
        
        [baseScrollVw addSubview:ContactsForm];
        
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             
                             ContactsForm.frame = CGRectMake(0, 0,self.view.frame.size.width,ContactsForm.frame.size.height);
                         }
                         completion:^(BOOL finished){//this block starts only when
                             
                         }];
        


    }
    else
    {
        [settingsBtn setImage:[UIImage imageNamed:@"settings.png"] forState:UIControlStateNormal];
        [self closeFormVw];
    }
    contactFormVisible = !contactFormVisible;
    

}





-(void)closeFormVw
{
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         ContactsForm.frame = CGRectMake(0,  self.view.frame.size.height ,self.view.frame.size.width,ContactsForm.frame.size.height);
                     }
                     completion:^(BOOL finished)
                    {
                         //this block starts only when
                         [ContactsForm removeFromSuperview];
                        ContactsForm = nil;
                        
                         
                     }];
}


#pragma mark - create password view

-(UIView*)create_circles:(int)count radius:(int)radius space:(int)space view:(UIView*)view
{
    
    view.frame = CGRectMake(0, 0, (space * (count - 1) + (count * 2 * radius)), 2*radius);
    
    for (int i = 0; i < count; i++) {
        
        UILabel* lbl = [[UILabel alloc]init];
        lbl.frame = CGRectMake(i*((2*radius)+space), 0, 2*radius, 2*radius);
        lbl.backgroundColor = [UIColor whiteColor];
        lbl.tag = i+1;
        lbl.layer.cornerRadius = radius;
        lbl.layer.borderColor = [RGBA(214, 214, 214, 1) CGColor];
        lbl.layer.borderWidth =1.5;
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.layer.masksToBounds = YES;
        [view addSubview:lbl];
    }
    
    return view;
}


#pragma mark - textField
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    
    return YES;
}


- (void) textFieldDidBeginEditing:(UITextField *)textField {
    
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    
    [textField resignFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
   
    // verify max length has not been exceeded
    NSUInteger newLength = textField.text.length + string.length - range.length;
    
    if ([string isEqualToString:@""] ) {
        UILabel* passcode = (UILabel*)[PassCodeVW viewWithTag:newLength+1];
        passcode.text = @"";
        return YES;
    }
    if (newLength < 5) {
        UILabel* passcode = (UILabel*)[PassCodeVW viewWithTag:newLength];
        passcode.text = string;
        [NSTimer scheduledTimerWithTimeInterval: 0.2 target: self
                                       selector: @selector(substitude:) userInfo:passcode repeats: NO];
        return YES;
    }
    else
    {
        return NO;
    }
    
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    return YES;
}

-(void)substitude :(NSTimer*)theTimer
{
    UILabel* passcodeLbl = (UILabel*)[theTimer userInfo];
    
    passcodeLbl.text = @"\u25cf";
    
}

#pragma mark - BTN_CLICKED

- (void)startTimer {
    
    [self stopTimer];
    if (!_timer) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:5.0f
                                                  target:self
                                                selector:@selector(_timerFired:)
                                                userInfo:nil
                                                 repeats:NO];
        [Loder setIndicatorAnimationDuration:5.0];
        [Loder updateWithTotalBytes:100 downloadedBytes:100];
        btnLbl.text = @"Cancel";

    }
}

- (void)stopTimer {
    if ([_timer isValid]) {
        [_timer invalidate];
    }
    _timer = nil;
    
}

- (void)_timerFired:(NSTimer *)timer {
    [self stopTimer];
    [self active_BTN_CLICKED];
}
-(void)deactive_BTN_CLICKED
{
    
    NSString *hiddenPasswordText = hiddenPwdTxtFld.text;
    NSString *sosPassword = [[NSUserDefaults standardUserDefaults]valueForKey:@"SOSPASSWORD"];
    if ([hiddenPasswordText isEqualToString:sosPassword])
    {

    [[NSUserDefaults standardUserDefaults] setObject:@"deactivate" forKey:SOS_STATUS];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self stopTimer];
    [Loder setIndicatorAnimationDuration:0.0];
    [Loder updateWithTotalBytes:100 downloadedBytes:0];

   // activeStatus_Imgw.image = [UIImage imageNamed:@"activate.png"];
    activeStatus_ImgVw.backgroundColor = RGBA(90, 165, 1, 1);
    btnLbl.text = @"Activate";
    PassCodeVW.hidden = YES;
    settingsBtn.hidden = NO;
    
    deactive_status = !deactive_status;
    hiddenPwdTxtFld.text = @"";
    
    [self clearPassword];
    [self stopCurrentLocation];
    frontImg_Str = nil;
    rearImg_Str = nil;
    [self deactivation:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME] :[[NSUserDefaults standardUserDefaults] objectForKey:SOS_ID]];
    }
    else if (hiddenPasswordText.length == 0)
    {
        UIAlertView *wrongPasswordAlert = [[UIAlertView alloc]initWithTitle:@"OOPS!" message:@"Please enter the password" delegate:self cancelButtonTitle:@"Let me try again!" otherButtonTitles:nil, nil];
        [wrongPasswordAlert show];
        
    }
    else
    {
        hiddenPwdTxtFld.text = @"";
        for (int i =0; i < 4; i++) {
            UILabel* passcode = (UILabel*)[PassCodeVW viewWithTag:i+1];
            passcode.text = @"";
            
        }
        
        UIAlertView *wrongPasswordAlert = [[UIAlertView alloc]initWithTitle:@"OOPS!" message:@"Your password is wrong." delegate:self cancelButtonTitle:@"Let me try again!" otherButtonTitles:nil, nil];
        [wrongPasswordAlert show];
    }
}

-(void)active_BTN_CLICKED
{
    [[NSUserDefaults standardUserDefaults] setObject:@"activate" forKey:SOS_STATUS];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
   

  //  NSLog(@"active_BTN_CLICKED");
    //activeStatus_ImgVw.image = [UIImage imageNamed:@"deactivate.png"];
    activeStatus_ImgVw.backgroundColor = RGBA(207, 4, 4, 1);
    btnLbl.text = @"Deactivate";
    PassCodeVW.hidden = NO;
    settingsBtn.hidden = YES;
    deactive_status = !deactive_status;
    [self activation:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME]];
}

-(void)activeStatus
{
    //activeStatus_ImgVw.image = [UIImage imageNamed:@"deactivate.png"];
    activeStatus_ImgVw.backgroundColor = RGBA(207, 4, 4, 1);
btnLbl.text = @"Deactivate";
    PassCodeVW.hidden = NO;
    settingsBtn.hidden = YES;
    deactive_status = !deactive_status;
}

-(void)closeContactVw
{
    [self closeFormVw];

contactFormVisible = !contactFormVisible;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Action For handleSwipeFrom
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(void)handleSwipeFrom :(UISwipeGestureRecognizer *)recognizer
{

    [[self slidingPanelController] closePanel];
    [self appDelegate].navigationBarViewLine.hidden = NO;


}
-(void)chatBtn_CLICKED
{

    fromSOS = NO;
     [[self slidingPanelController] closePanel];
    [self appDelegate].navigationBarViewLine.hidden = NO;

}

#pragma mark - Web Services activation

-(void)deactivation :(NSString*)mobileNum :(NSString*)sosId
{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:SOS_ID];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:SOS_URL];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *soapMessage = [NSString stringWithFormat:
                             @"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                             "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:server\">\n"
                             "<soapenv:Header/>\n"
                             "<soapenv:Body>\n"
                             "<urn:SOSDeactivation soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\n"
                             "<SOSDeactivateInput xsi:type=\"urn:SOSDeactivateInput\">\n"
                             "<sosId>%@</sosId>\n"
                             "<userMobileNo>%@</userMobileNo>\n"
                             "</SOSDeactivateInput>\n"
                             "</urn:SOSDeactivation>\n"
                             "</soapenv:Body>\n"
                             "</soapenv:Envelope>\n",sosId,mobileNum
                             ];
    
    
    NSURL *url = [NSURL URLWithString:SOS_SERVICE];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: [NSString stringWithFormat:@"%@/%@",SOS_SERVICE,SOS_ACTIVE ] forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]
                                         initWithRequest:theRequest];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation
                                               , id responseObject) {
        
        
        NSString *str = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        
        
        
        
        
        
        RXMLElement *rxml = [RXMLElement elementFromXMLString:str encoding:NSUTF8StringEncoding];
        
        [rxml iterateWithRootXPath:@"//return" usingBlock: ^(RXMLElement *player) {
            
            
            if ([[NSString stringWithFormat:@"%@",[player child:@"ResponseMsg"]] isEqual:@"Success"]) {
                
            }
            else
            {
            }
            
            
        }];
        
        
        
        
        
        
        // code
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // code
      //  NSLog(@"Error: %@", error.localizedDescription);
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Buzz"
                                                          message: error.localizedDescription
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        
    }
     ];
    [operation start];
    
}

#pragma mark - Web Services deactivation


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark DataBase Methods For Recent Chats
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (XMPPUserCoreDataStorageObject *)RecentcontactsFetch:(NSString *)Str
{
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:[self managedObjectContext_roster]];
    [request setEntity:entity];
    NSString *predicateFrmt = @"jidStr = %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, Str];
    [request setPredicate:predicate];
    NSArray *empArray=[[self managedObjectContext_roster] executeFetchRequest:request error:nil];
    if ([empArray count] == 1)
    {
        for (XMPPUserCoreDataStorageObject *user in  empArray)
        {
            return user;
        }
    }
    return nil;
}

-(void)activation :(NSString*)mobileNum
{
    
    NSDate *currentDate = [NSDate date];
    [[NSUserDefaults standardUserDefaults] setObject:[currentDate dateByAddingTimeInterval:-120] forKey:@"LastDate"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:SOS_ID];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:SOS_URL];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSMutableArray* inneremailAry = [self fetchUserEmailsArrayFromEmergencyContacts];
    
    
    NSString *soapMessage = [NSString stringWithFormat:
                             @"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                             "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:server\">\n"
                             "<soapenv:Header/>\n"
                             "<soapenv:Body>\n"
                             "<urn:SOSActivation soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\n"
                             "<SOSActiveInput xsi:type=\"urn:SOSActiveInput\">\n"
                             "<userMobileNo xsi:type=\"xsd:string\">%@</userMobileNo>\n"
                             "<EmailIdz xsi:type=\"xsd:string\">%@</EmailIdz>\n"
                             "<EmailIdz xsi:type=\"xsd:string\">%@</EmailIdz>\n"
                             "<EmailIdz xsi:type=\"xsd:string\">%@</EmailIdz>\n"
                             "<EmailIdz xsi:type=\"xsd:string\">%@</EmailIdz>\n"
                             "<EmailIdz xsi:type=\"xsd:string\">%@</EmailIdz>\n"
                             "</SOSActiveInput>\n"
                             "</urn:SOSActivation>\n"
                             "</soapenv:Body>\n"
                             "</soapenv:Envelope>\n",mobileNum,[inneremailAry objectAtIndex:0],[inneremailAry objectAtIndex:1],[inneremailAry objectAtIndex:2],[inneremailAry objectAtIndex:3],[inneremailAry objectAtIndex:4]
                             ];
    
    NSURL *url = [NSURL URLWithString:SOS_SERVICE];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: [NSString stringWithFormat:@"%@/%@",SOS_SERVICE,SOS_ACTIVE ] forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]
                                         initWithRequest:theRequest];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation
                                               , id responseObject) {
        
        
        NSString *str = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        
        
        
        
        
        
        RXMLElement *rxml = [RXMLElement elementFromXMLString:str encoding:NSUTF8StringEncoding];
        
        [rxml iterateWithRootXPath:@"//return" usingBlock: ^(RXMLElement *player) {
            
            
            if ([[NSString stringWithFormat:@"%@",[player child:@"ResponseMsg"]] isEqual:@"Success"]) {
                [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",[player child:@"Sos_id"]] forKey:SOS_ID];
                [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",[player child:@"PortalURL"]] forKey:SOS_URL];
                [[NSUserDefaults standardUserDefaults] setObject:@"false" forKey:SOS_IM_STATUS];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                
              //  NSLog(@"%@",[NSString stringWithFormat:@"%@ %@",[player child:@"Sos_id"],[player child:@"PortalURL"]]);
                
                if (frontImg_Str)
                {
                    [self imgUpload:[[NSUserDefaults standardUserDefaults] objectForKey:SOS_ID] :[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME] :frontImg_Str :YES];
                }
                
                if (rearImg_Str)
                {
                    [self imgUpload:[[NSUserDefaults standardUserDefaults] objectForKey:SOS_ID] :[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME] :rearImg_Str :NO];
                }
                
                [self getCurrentLocation];
            }
            else
            {
                UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Buzz"
                                                                  message:[NSString stringWithFormat:@"%@",[player child:@"ResponseMsg"]]
                                                                 delegate:nil
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles:nil];
                [message show];
                [self deactive_BTN_CLICKED];
            }
            
            
        }];
        
        
        
        
        
        
        // code
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // code
    //    NSLog(@"Activation Error: %@", error.localizedDescription);
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Buzz"
                                                          message: error.localizedDescription
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        [self deactive_BTN_CLICKED];
    }
     ];
    [operation start];
    
}



-(NSMutableArray*)fetchUserEmailsArrayFromEmergencyContacts
{
    NSMutableArray* UserEmailAry = [NSMutableArray new];
    
    NSMutableArray * EncodedSelContactsAry = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:SOS_CONTACTDATA]];
    NSMutableArray* selContactsAry = [NSMutableArray new];
    
    for (NSData *personEncodedObject in EncodedSelContactsAry) {
        sosContactObject *personObject = [NSKeyedUnarchiver unarchiveObjectWithData:personEncodedObject];
        [selContactsAry addObject:personObject];
    }
    
    for (sosContactObject * person in selContactsAry)
    {
        NSString*  email = person.selectedEmail;
        if ([self validateEmailWithString:email])
        {
            [UserEmailAry addObject:email];
        }
        
    }
    for (int i = (int)[UserEmailAry count]; i < 5; i++)
    {
        
        [UserEmailAry addObject:@""];
        
    }

    return UserEmailAry;
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

-(NSMutableArray*)fetchUserContactsArrayFromJidAry
{
    NSMutableArray* UserContactAry = [NSMutableArray new];
    
    NSMutableArray * EncodedSelContactsAry = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:SOS_CONTACTDATA]];
   NSMutableArray* selContactsAry = [NSMutableArray new];
    
    for (NSData *personEncodedObject in EncodedSelContactsAry) {
        sosContactObject *personObject = [NSKeyedUnarchiver unarchiveObjectWithData:personEncodedObject];
        [selContactsAry addObject:personObject];
    }
    
    for (sosContactObject * person in selContactsAry)
    {
        NSString*  number = [[person.selectedNumber componentsSeparatedByCharactersInSet:
                              [[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"]
                               invertedSet]]
                             componentsJoinedByString:@""];
        
       
             number = [self checkPhoneNum:number];
        
        if (number)
        {
            if ([[self appDelegate] RecentcontactsFetch:[NSString stringWithFormat:@"%@@%@",number,DOMAINAME]] != nil)
            {
                [UserContactAry addObject:person];
                
            }
        }
        
    }
        return UserContactAry;
}
-(NSString*)checkPhoneNum:(NSString*)number
{
    if (number.length == 10)
    {
        number = [NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"],number];
        return number;
    }
    else if (number.length == 11)
    {
        NSString* subNumber=[number substringToIndex:1];
        if ([subNumber isEqualToString:@"0"])
        {
            NSString *newStr = [number substringWithRange:NSMakeRange(1, [number length]-1)];
            number =  [NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"],newStr];
            return number;
        }
        else
        {
            return nil;
        }
    }
    else
    {
        return number;
    }
}


- (NSManagedObjectContext *)managedObjectContext_roster
{
	return [[[self appDelegate] xmppRosterStorage] mainThreadManagedObjectContext];
}


-(void)sendImMsg_address:(NSString*)address latLong:(NSString*)latLong
{
    NSMutableArray* contactsAry = [NSMutableArray new];
    contactsAry = [self fetchUserContactsArrayFromJidAry];
    
    for (sosContactObject* person in contactsAry) {
        
        NSString*  number = [[person.selectedNumber componentsSeparatedByCharactersInSet:
                              [[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"]
                               invertedSet]]
                             componentsJoinedByString:@""];
        
        
        number = [self checkPhoneNum:number];
        NSString *lastName = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserLastName"];
        
        if (lastName.length != 0)
        {
            [self SendButtonAction:[NSString stringWithFormat:@"%@@%@",number,DOMAINAME] address:address latLong:latLong username:[NSString stringWithFormat:@"%@ %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserFirtName"],[[NSUserDefaults standardUserDefaults] objectForKey:@"UserLastName"]]];
        }
        else
        {
        [self SendButtonAction:[NSString stringWithFormat:@"%@@%@",number,DOMAINAME] address:address latLong:latLong username:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserFirtName"]]];
        }
    }
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Action for sending Message
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)SendButtonAction:(NSString*)userString address:(NSString*)address latLong:(NSString*)latLong username:(NSString*)username
{
   
     messageMethods = [MessageMethods new];
    [messageMethods textmessage:[NSString stringWithFormat:@"This is Emergency for %@ ......\nAddress : %@ MAP Point : https://maps.google.com/maps?daddr=%@",username,address,latLong] jid:userString];
}


- (NSString *)base64String :(UIImage*)img
{
    NSData *imageData = UIImageJPEGRepresentation(img, 1.0);
    
    NSString *encodedString = [self base64forData:imageData];
    return encodedString;
}

- (NSString*)base64forData:(NSData*)theData {
    
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

#pragma mark - Web Services imgUpload

-(void)imgUpload :(NSString*)sosId :(NSString*)mobNum :(NSString*)imgStr :(BOOL)isFront
{
   // NSLog(@"%@",imgStr);
    
    NSString *soapMessage = [NSString stringWithFormat:
                             @"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                             "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:server\">\n"
                             "<soapenv:Header/>\n"
                             "<soapenv:Body>\n"
                             "<urn:imageUpload soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\n"
                             "<imageUploadInput xsi:type=\"urn:imageUploadInput\">\n"
                             "<!--You may enter the following 3 items in any order-->\n"
                             "<sosId xsi:type=\"xsd:int\">%@</sosId>\n"
                             "<userMobileNo xsi:type=\"xsd:string\">%@</userMobileNo>\n"
                             "<base64String xsi:type=\"xsd:string\">%@</base64String>\n"
                             "</imageUploadInput>\n"
                             "</urn:imageUpload>\n"
                             "</soapenv:Body>\n"
                             "</soapenv:Envelope>\n",sosId,mobNum,imgStr
                             ];
    
    NSString *imageUpload = [NSString stringWithFormat:@"%@/%@",SOS_SERVICE,@"imageUpload"];
    
    NSURL *url = [NSURL URLWithString:SOS_SERVICE];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: imageUpload forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]
                                         initWithRequest:theRequest];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation
                                               , id responseObject) {
        
        
        NSString *str = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
     
        
        
        
        RXMLElement *rxml = [RXMLElement elementFromXMLString:str encoding:NSUTF8StringEncoding];
        
        
        [rxml iterateWithRootXPath:@"//return" usingBlock: ^(RXMLElement *player) {
         //   NSLog(@"Player: %@ %@",[player child:@"ReturnCode"], [player child:@"ResponseMsg"]);
            if ([[NSString stringWithFormat:@"%@",[player child:@"ReturnCode"]] isEqualToString:@"1000"]  )
            {
               // NSLog(@"Player: %@ %@ %@",[NSString stringWithFormat:@"%@",[player child:@"UrlString"]], [NSString stringWithFormat:@"%@",[player child:@"FileId"]],[NSString stringWithFormat:@"%@",[player child:@"FileSize"]]);
                NSString* path = [self getUniqueFilenameInFolder:@"mediaFile" forFileExtension:@"jpg"];
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *newFilePath = [[paths lastObject] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",path]];
                
                
                if (isFront) {
                    if (frontImg_Data_30 != nil) {
                        [frontImg_Data_150 writeToFile:newFilePath atomically:YES];
                        [self sendMediaMsg:[NSString stringWithFormat:@"%@",[player child:@"UrlString"]] file_id:[NSString stringWithFormat:@"%@",[player child:@"FileId"]] file_type:@"image" file_size:[NSString stringWithFormat:@"%@",[player child:@"FileSize"]] file_preview:[self encodeToBase64String:frontImg_Data_30] locUrl:newFilePath ];
                    }
                }
                else
                {
                    if (rearImg_Data_30 != nil) {
                        [rearImg_Data_150 writeToFile:newFilePath atomically:YES];
                        [self sendMediaMsg:[NSString stringWithFormat:@"%@",[player child:@"UrlString"]] file_id:[NSString stringWithFormat:@"%@",[player child:@"FileId"]] file_type:@"image" file_size:[NSString stringWithFormat:@"%@",[player child:@"FileSize"]] file_preview:[self encodeToBase64String:rearImg_Data_30] locUrl:newFilePath ];
                    }
                }
                
                

            }
            
            
            
            
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // code
     //   NSLog(@"Image Upload Error: %@", error.localizedDescription);
    }
     ];
    [operation start];
    
}

- (NSString *)encodeToBase64String:(NSData *)imageData {
    return [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

-(void)sendMediaMsg:(NSString*)file_url file_id:(NSString*)file_id file_type:(NSString*)file_type file_size:(NSString*)file_size file_preview:(NSString*)file_preview locUrl:(NSString*)locUrl
{
    NSMutableArray* contactsAry = [NSMutableArray new];
    contactsAry = [self fetchUserContactsArrayFromJidAry];
    
    for (sosContactObject* person in contactsAry) {
        
        
        NSString *messageStr = file_type;
        if([messageStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length > 0) {
            
            NSString *messageID= [self.xmppStream generateUUID];
            
            NSString*  number = [[person.selectedNumber componentsSeparatedByCharactersInSet:
                                  [[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"]
                                   invertedSet]]
                                 componentsJoinedByString:@""];
            
            
            number = [self checkPhoneNum:number];
            
            [self mediaDbEditor_msgId:messageID fileName:@"image" url:locUrl userJid:[NSString stringWithFormat:@"%@@%@",number,DOMAINAME]];
            
            NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
            [body setStringValue:messageStr];
            NSXMLElement *file = [NSXMLElement elementWithName:@"file"];
            [file addAttributeWithName:@"xmlns" stringValue:@"shaili:file"];
            [file addAttributeWithName:@"file_url" stringValue:file_url];
            [file addAttributeWithName:@"file_id" stringValue:file_id];
            [file addAttributeWithName:@"file_type" stringValue:file_type];
            [file addAttributeWithName:@"file_size" stringValue:file_size];
            
            [file addAttributeWithName:@"from" stringValue:[[self xmppStream] myJID].bare];
            
            NSXMLElement *filepreview = [NSXMLElement elementWithName:@"file_preview"];
            [filepreview setStringValue:file_preview];
            [file addChild:filepreview];
            
            NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
            [message addAttributeWithName:@"id" stringValue:messageID];
            [message addAttributeWithName:@"type" stringValue:@"chat"];
            [message addAttributeWithName:@"to" stringValue:[NSString stringWithFormat:@"%@@%@",number,DOMAINAME]];
            [message addChild:body];
            [message addChild:file];
            [self.xmppStream sendElement:message];
            
            
        }

        
    }
}

-(void)mediaDbEditor_msgId:(NSString*)msgId fileName:(NSString*)fileName url:(NSString*)url userJid:(NSString*)userJid
{
    
    
    mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
    NSMutableDictionary* dict = [NSMutableDictionary new];
    [dict setObject:msgId forKey:@"msgId"];
    [dict setObject:userJid forKey:@"userId"];
    [dict setObject:fileName forKey:@"fileName"];
    [dict setObject:url forKey:@"url"];
    [dict setObject:[NSNumber numberWithBool:NO] forKey:@"loc_cloud"];
    [dict setObject:[NSNumber numberWithBool:NO] forKey:@"sent_received"];
    [dict setObject:@"2" forKey:@"success_failure"];
    [dict setObject:[NSNumber numberWithBool:NO] forKey:@"sender_receiver"];
    [dict setObject:[NSNumber numberWithFloat:0.0] forKey:@"percentLoaded"];
    [dict setObject:fileName forKey:@"video_img"];
    
    
    
    [mediaDb checkAndInsert:dict];
    
    
}

#pragma mark - Web Services LAT LONG

-(void)UpdateLatLong :(CLLocation*)location :(NSString*)addressStr :(NSString*)mobileNum :(NSString*)sosId
{
    
    NSString *soapMessage = [NSString stringWithFormat:
                             @"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                             "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:server\">\n"
                             "<soapenv:Header/>\n"
                             "<soapenv:Body>\n"
                             "<urn:userLocation soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\n"
                             "<userLocationInput xsi:type=\"urn:userLocationInput\">\n"
                             "<!--You may enter the following 5 items in any order-->\n"
                             "<sosId xsi:type=\"xsd:int\">%@</sosId>\n"
                             "<userMobileNo xsi:type=\"xsd:string\">%@</userMobileNo>\n"
                             "<latitude xsi:type=\"xsd:float\">%f</latitude>\n"
                             "<longitude xsi:type=\"xsd:float\">%f</longitude>\n"
                             "<address xsi:type=\"xsd:string\">%@</address>\n"
                             "</userLocationInput>\n"
                             "</urn:userLocation>\n"
                             "</soapenv:Body>\n"
                             "</soapenv:Envelope>\n",sosId,mobileNum,location.coordinate.latitude,location.coordinate.longitude,addressStr
                             ];
    
    NSURL *url = [NSURL URLWithString:SOS_SERVICE];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%d", [soapMessage length]];
    
    NSString *userLocation = [NSString stringWithFormat:@"%@/%@",SOS_SERVICE,@"userLocation"];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: userLocation forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]
                                         initWithRequest:theRequest];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation
                                               , id responseObject) {
        
        
        NSString *str = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        
        
        
        
        
        RXMLElement *rxml = [RXMLElement elementFromXMLString:str encoding:NSUTF8StringEncoding];
        
        
        [rxml iterateWithRootXPath:@"//return" usingBlock: ^(RXMLElement *player) {
          //  NSLog(@"Player: %@ %@",[player child:@"ReturnCode"], [player child:@"ResponseMsg"]);
        }];
        
        //NSLog(@"%@",[rxml attribute:@"Response"]);
        
        
        
        // code
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // code
      //  NSLog(@"Lat Long Update Error: %@", error.localizedDescription);
    }
     ];
    [operation start];
    
}

-(void)getAddressWebService :(CLLocation*)currentLocation
{
    //http://maps.googleapis.com/maps/api/geocode/json?address=13.07279,80.254898&sensor=true
    
    NSString *urlStr = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?address=%f,%f&sensor=true",currentLocation.coordinate.latitude,currentLocation.coordinate.longitude];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    
    
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: request
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        
                                                        NSDictionary *dict = (NSDictionary *)JSON;
                                                        
                                                       
                                                        
                                                        if ([[dict objectForKey:@"status"] isEqualToString:@"OK"]) {
                                                            
                                                            
                                                            NSString* AddressStr = [NSString stringWithFormat:@"%@ [Accurate to %.0fm]",[[[dict objectForKey:@"results"] objectAtIndex:0] objectForKey:@"formatted_address"],currentLocation.horizontalAccuracy];
                                                            
                                                            
                                                            if (AddressStr.length > 0) {
                                                                [self UpdateLatLong:currentLocation :AddressStr :[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME] :[[NSUserDefaults standardUserDefaults] objectForKey:SOS_ID]];
                                                            }
                                                            else
                                                            {
                                                                [self UpdateLatLong:currentLocation :@"ADDRESS UNKNOWN" :[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME] :[[NSUserDefaults standardUserDefaults] objectForKey:SOS_ID]];
                                                            }
                                                            
                                                            [self sendImMsg_address:AddressStr latLong:[NSString stringWithFormat:@"%f,%f",currentLocation.coordinate.latitude,currentLocation.coordinate.longitude]];
                                                        }
                                                        
                                                        
                                                        
                                                        
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        
                                                        
                                                       //  NSLog(@"Get Address Web Service Error: %@", error.localizedDescription);
                                                        
                                                    }];
    
    
    [operation start];
    
}

#pragma mark - getCurrentLocation method


-(void)getCurrentLocation
{
   /* if([CLLocationManager locationServicesEnabled]){
        
        
        locationManager = [[CLLocationManager alloc] init];
        
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        [locationManager startUpdatingLocation];
    }*/
//    locSingleton = [LocationManagerSingleton sharedSingleton];
//    [locSingleton startUpdatingCurrentLocation];
    
    if([CLLocationManager locationServicesEnabled]){
        
        
        
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        {
            [locationManager requestWhenInUseAuthorization];
        }
        [locationManager startUpdatingLocation];
        Got_Location_At_First_time = YES;
        
        
    }

    
}

-(void)stopCurrentLocation
{
    [locationManager stopUpdatingLocation];
}



#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
   // NSLog(@"Loction Error %@",error);
    
    
}


#pragma mark - CLLocationManagerDelegate

/*
 *  locationManager:didUpdateToLocation:fromLocation:
 *
 *  Discussion:
 *    Invoked when a new location is available. oldLocation may be nil if there is no previous location
 *    available.
 *
 *    This method is deprecated. If locationManager:didUpdateLocations: is
 *    implemented, this method will not be called.
 */
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    internalNewLocation = newLocation;
    

    float distInMeter = [newLocation distanceFromLocation:oldLocation]; // which returns in meters
    if (!internalOldLocation) {
        internalOldLocation= oldLocation;
    }
    
    CLLocationDistance internaldistInMeter = [internalNewLocation distanceFromLocation:internalOldLocation]; // which returns in meters

    
    if (Got_Location_At_First_time)
    {
      //  NSLog(@"fist distInMeter %f",internaldistInMeter);

       // NSLog(@"distInMeter %f",internaldistInMeter);
        internalOldLocation = newLocation;
        if (UIApplication.sharedApplication.applicationState == UIApplicationStateActive)
        {
           // NSLog(@"App is foreground. New location is %@", newLocation);
        }
        else
        {
         //   NSLog(@"App is backgrounded. New location is %@", newLocation);
          //  NSLog(@"backgrounded distInMeter  %f",distInMeter);
            
        }
        [self getAddressWebService:newLocation];
        Got_Location_At_First_time = NO;
        
    }
    else if (internaldistInMeter > 500)
    {
      //  NSLog(@"Second distInMeter %f",internaldistInMeter);
        internalOldLocation = newLocation;
        if (UIApplication.sharedApplication.applicationState == UIApplicationStateActive)
        {
          //  NSLog(@"Second App is foreground. New location is %@", newLocation);
        }
        else
        {
           // NSLog(@"Second App is backgrounded. New location is %@", newLocation);
          //  NSLog(@"Second backgrounded distInMeter  %f",distInMeter);
            
        }
        [self getAddressWebService:newLocation];
   }
   
    
   
}



- (void)showInvitation {
    
    if (![MFMessageComposeViewController canSendText]) {
        
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    NSString *message = [NSString stringWithFormat:@"Download this game!"];
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setBody:message];
    
    if ([MFMessageComposeViewController canSendAttachments]) {
      //  NSLog(@"Attachments Can Be Sent.");
        NSData *imgData = [NSData dataWithContentsOfFile:@"water"];
        BOOL didAttachImage = [messageController addAttachmentData:imgData typeIdentifier:(NSString *)kUTTypePNG filename:@"image.png"];
        
        if (didAttachImage) {
           // NSLog(@"Image Attached.");
            
        } else {
          //  NSLog(@"Image Could Not Be Attached.");
        }
    }
    
    [self presentViewController:messageController animated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if (result == MessageComposeResultCancelled)
    {
        
    }
     //   NSLog(@"Message cancelled");
        else if (result == MessageComposeResultSent)
        {
            
        }
         //   NSLog(@"Message sent");
            else
            {
             //   NSLog(@"Message failed")  ;
            }
    
                }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
