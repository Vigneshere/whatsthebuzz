//
//  PrivateSection.m
//  buzz
//
//  Created by shaili_solutions_MacPro3 on 11/06/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "PrivateSection.h"


@implementation PrivateSection

@dynamic iD;
@dynamic is_Sender;
@dynamic status;
@dynamic timer;
@dynamic time_updated;

@end
