//
//  privateChatDbEditer.h
//  buzz
//
//  Created by shaili_solutions_MacPro3 on 11/06/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface privateChatDbEditer : NSObject

@property (strong, nonatomic) Singleton *singleton;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

//pragma mark check And Insert private Chat Info
-(BOOL)checkAndInsert:(NSMutableDictionary*)privateChatInfoDict;

//pragma mark Insert With private Chat Info
-(BOOL)insertPrivateSection:(NSMutableDictionary*)privateChatInfoDict;

//pragma mark update private Chat Info
-(BOOL)updatePrivateSection:(NSMutableDictionary*)privateChatInfoDict;

-(BOOL)checkAndInsertGroup:(NSMutableDictionary*)privateChatInfoDict;

//pragma mark  private Chat Info
-(NSMutableArray*)fetchprivateChat;

//pragma mark private Chat with  Id
-(NSMutableDictionary*)fetchprivateChat:(NSString*)ID;

//pragma mark deleteMedia with  Id
-(BOOL)deletePrivateChat:(NSString*)ID;

//pragma mark checkAndStartPrivateTimer 
-(void)checkAndStartPrivateTimer;


@end

