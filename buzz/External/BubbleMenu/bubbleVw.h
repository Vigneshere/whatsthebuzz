//
//  bubbleVw.h
//  drag
//
//  Created by shaili_solutions_MacPro3 on 10/06/14.
//  Copyright (c) 2014 JemsNets. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface bubbleVw : UIButton
{
    CGSize screenSize;
    CGPoint restorePoint;
}

+ (bubbleVw *)sharedInstance:(CGRect)frame size:(CGSize)size;

- (id)initFrame:(CGRect)frame size:(CGSize)size;
- (id)init ;
-(void)restoreBtn;


@end
