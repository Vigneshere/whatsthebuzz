//
//  menuView.h
//  drag
//
//  Created by shaili_solutions_MacPro3 on 17/06/14.
//  Copyright (c) 2014 JemsNets. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol bubbleViewDelegate <NSObject>

-(void)selectBubbleMenuButton:(NSInteger)index;

@end
@interface menuView : UIView
{
    CGSize screeenSize;
    CGRect buttonOriginFrame_;
    UIView *menu_;
    UIButton * centerButton_;
    BOOL       isOpening_;
    BOOL       isInProcessing_;
    BOOL       isClosed_;
}

@property(nonatomic,weak)id<bubbleViewDelegate>delegte;


+ (menuView *)sharedInstance:(NSArray*)btnImages btnTitleAry:(NSArray*)btnTitleAry screeenSize:(CGSize)screeenSize;

- (id)initFrame_btnImages:(NSArray*)btnImages btnTitleAry:(NSArray*)btnTitleAry screeenSize:(CGSize)screeenSize;

-(void)open;
- (void)_close:(NSNotification *)notification;

-(void)setImage:(UIImage *)image;

@end
