//
//  menuView.m
//  drag
//
//  Created by shaili_solutions_MacPro3 on 17/06/14.
//  Copyright (c) 2014 JemsNets. All rights reserved.
//

#import "menuView.h"

#define XPOINT 25
#define BTN_BG_WIDTH 80
#define BTN_BG_HEIGHT 60
#define SENCIBLE_GAP 15
#define TITLE_HEIGHT 17
#define Center_BTN_BG_WIDTH 98

#define kKYNCircleMenuClose @"KYNCircleMenuClose"

#define kKYCircleMenuViewHeight CGRectGetHeight([UIScreen mainScreen].applicationFrame)
#define kKYCircleMenuViewWidth  CGRectGetWidth([UIScreen mainScreen].applicationFrame)
#define kKYCircleMenuNavigationBarHeight 44.f


#define kKYButtonInMiniSize   16.f
#define kKYButtonInSmallSize  32.f
#define kKYButtonInNormalSize 64.f

#pragma mark - KYCircleMenu Configuration

// Number of buttons around the circle menu
#define kKYCCircleMenuButtonsCount 6
// Circle Menu
// Basic constants
#define kKYCircleMenuSize             280.f
#define kKYCircleMenuButtonSize       kKYButtonInNormalSize
#define kKYCircleMenuCenterButtonSize kKYButtonInNormalSize


#define menuImageArray @[@"nearby",@"clear",@"chat_Imp",@"video",@"arc_menu_secure_gallery",@"theme"]
#define menuArray @[@"Are You There?",@"Clear chat",@"Important",@"Video",@"Secure gallery",@"Set background"]


static CGFloat menuSize_,buttonSize_,centerButtonSize_; // size of center button
static CGFloat defaultTriangleHypotenuse_, minBounceOfTriangleHypotenuse_,
maxBounceOfTriangleHypotenuse_,
maxTriangleHypotenuse_;



@implementation menuView

+ (menuView *)sharedInstance:(NSArray*)btnImages btnTitleAry:(NSArray*)btnTitleAry  screeenSize:(CGSize)screeenSize
{
    static dispatch_once_t pred;
    static menuView *instance = nil;
 
    dispatch_once(&pred, ^{
        instance = [[self alloc] initFrame_btnImages:btnImages btnTitleAry:btnTitleAry  screeenSize:screeenSize];
        
        
        // Buttons' origin frame
           });
    
  
    
    return instance;
}


- (id)initFrame_btnImages:(NSArray*)btnImages btnTitleAry:(NSArray*)btnTitleAry  screeenSize:(CGSize)screeen_Size
{
    self = [super initWithFrame:CGRectMake(0, 0, screeen_Size.width, screeen_Size.height)];
    
   
    
    
    defaultTriangleHypotenuse_     = (kKYCircleMenuSize - kKYCircleMenuButtonSize) * .5f;
    minBounceOfTriangleHypotenuse_ = defaultTriangleHypotenuse_ - 12.f;
    maxBounceOfTriangleHypotenuse_ = defaultTriangleHypotenuse_ + 12.f;
    maxTriangleHypotenuse_         = kKYCircleMenuViewHeight * .5f;
    
    
    CGFloat originX = (kKYCircleMenuSize - kKYCircleMenuCenterButtonSize) * .5f;
    buttonOriginFrame_ =
    (CGRect){{originX, originX}, {kKYCircleMenuCenterButtonSize, kKYCircleMenuCenterButtonSize}};



    if(self = [super init])
    {
        screeenSize = screeen_Size;
        
        self.backgroundColor = [UIColor clearColor];
        
        UIView* selfVw_none = [UIView new];
        selfVw_none.frame = self.frame;
        selfVw_none.backgroundColor = [UIColor whiteColor];
        selfVw_none.alpha = 0.8;
        [self addSubview:selfVw_none];
        
        CGFloat viewHeight = CGRectGetHeight(self.frame);
        CGFloat viewWidth  = CGRectGetWidth(self.frame);
        
        // Center Menu View
        CGRect centerMenuFrame =
        CGRectMake((viewWidth - kKYCircleMenuSize) * .5f, (viewHeight - kKYCircleMenuSize) * .5f, kKYCircleMenuSize, kKYCircleMenuSize);
        menu_ = [[UIView alloc] initWithFrame:centerMenuFrame];
        [menu_ setAlpha:0.f];
        [self addSubview:menu_];

        UIImage *bubble = [UIImage imageNamed:@"bubble"];
        
        CGRect mainButtonFrame =
        CGRectMake((CGRectGetWidth(self.frame) - bubble.size.height) * .5f,
                   (CGRectGetHeight(self.frame) - bubble.size.height) * .5f,
                   bubble.size.height, bubble.size.height);
        centerButton_ = [[UIButton alloc] initWithFrame:mainButtonFrame];
        [centerButton_ setBackgroundImage:[UIImage imageNamed:@"bubble"] forState:UIControlStateNormal];
        [centerButton_ setClipsToBounds: YES];
        [centerButton_.layer setCornerRadius:bubble.size.width/2];
//        [centerButton_ setBackgroundColor:[UIColor redColor]];
//        [centerButton_ setImage:[UIImage imageNamed:@"bubble"]
//                       forState:UIControlStateNormal];
        [centerButton_ addTarget:self
                          action:@selector(handleTapOutOfBoxGesture:)
                forControlEvents:UIControlEventTouchUpInside];
        [self addSubview: centerButton_];

        
        
       [self placeButtons_btnImages];
        
        
    }
       return self;
}

-(void)placeButtons_btnImages
{
    for (int i = 1; i <= kKYCCircleMenuButtonsCount; ++i) {
        UIButton * button = [[UIButton alloc] initWithFrame:buttonOriginFrame_];
        [button setOpaque:NO];
        [button setTag:i];
        [button setImage:[UIImage imageNamed:menuImageArray[i-1]]
                forState:UIControlStateNormal];
        
        UILabel *labelForButton = [UILabel new];
        labelForButton.frame = CGRectMake(-15, button.frame.size.height , 100, 18);
        labelForButton.text = menuArray[i - 1];
        labelForButton.font = [UIFont fontWithName:FontString size:10];
        labelForButton.textAlignment = NSTextAlignmentCenter;
        labelForButton.backgroundColor = [UIColor clearColor];
        labelForButton.textColor = [UIColor blackColor];
        [button addSubview:labelForButton];
        
        

        [button addTarget:self action:@selector(runButtonActions:) forControlEvents:UIControlEventTouchUpInside];
        [menu_ addSubview:button];
    }
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOutOfBoxGesture:)];
    [self addGestureRecognizer:tapGesture];
    
}

-(void)setImage:(UIImage *)image
{
    [centerButton_ setBackgroundImage:image
                             forState:UIControlStateNormal];
}

-(void)drawline:(UIView*)boardView
{
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    [shapeLayer setBounds:CGRectMake(-115, -30, boardView.frame.size.width, boardView.frame.size.height)];
    [shapeLayer setPosition:CGPointMake(0, 0)];
    [shapeLayer setFillColor:[[UIColor clearColor] CGColor]];
    [shapeLayer setStrokeColor:[RGBA(46, 47, 49, 1) CGColor]];
    [shapeLayer setLineWidth:1.5f];
    [shapeLayer setLineJoin:kCALineJoinRound];
    
    // Setup the path
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL,boardView.frame.origin.x + 40, boardView.frame.origin.y +30);
    CGPathAddLineToPoint(path, NULL, boardView.frame.origin.x + boardView.frame.size.width-30,boardView.frame.origin.y+boardView.frame.size.height-30);
    [shapeLayer setPath:path];
    CGPathRelease(path);
    
    
    [[boardView layer] addSublayer:shapeLayer];
}

-(void)drawline1:(UIView*)boardView
{
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    [shapeLayer setBounds:CGRectMake(-115, -30, boardView.frame.size.width, boardView.frame.size.height)];
    [shapeLayer setPosition:CGPointMake(0, 0)];
    [shapeLayer setFillColor:[[UIColor clearColor] CGColor]];
    [shapeLayer setStrokeColor:[RGBA(46, 47, 49, 1) CGColor]];
    [shapeLayer setLineWidth:1.5f];
    [shapeLayer setLineJoin:kCALineJoinRound];
    
    CGMutablePathRef path1 = CGPathCreateMutable();
    CGPathMoveToPoint(path1, NULL, boardView.frame.origin.x + boardView.frame.size.width-55,boardView.frame.origin.y+30);
    CGPathAddLineToPoint(path1, NULL,boardView.frame.origin.x + 25, boardView.frame.origin.y+boardView.frame.size.height-30);
    [shapeLayer setPath:path1];
    CGPathRelease(path1);
    
    [[boardView layer] addSublayer:shapeLayer];
}

-(void)handleTapOnBoardGesture:(UITapGestureRecognizer *) sender
{
    
}
-(void)handleTapOutOfBoxGesture:(UITapGestureRecognizer *) sender
{
 
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject: @"dismiss"  forKey: @"menuBtnClicked"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"menuBtnClicked" object:nil userInfo:dict];
    

    
}

- (void) wheelDidChangeValue:(NSString *)newValue {
    
  //  NSLog(@"%@",newValue);
}


- (void)_setupNotificationObserver
{
    // Add Observer for close self
    // If |centerMainButton_| post cancel notification, do it
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_close:)
                                                 name:kKYNCircleMenuClose
                                               object:nil];
}

// Toggle Circle Menu
- (void)_toggle:(id)sender
{
    (isClosed_ ? [self open] : [self _close:nil]);
}


-(void)open
{
//    if (isOpening_) return;
//    
//    isInProcessing_ = YES;
    // Show buttons with animation
    [UIView animateWithDuration:.3f
                          delay:0.f
                        options:UIViewAnimationCurveEaseInOut
                     animations:^{
                         [menu_ setAlpha:1.f];
                         [self setAlpha:1.f];

                         // Compute buttons' frame and set for them, based on |buttonCount|
                         [self _updateButtonsLayoutWithTriangleHypotenuse:maxBounceOfTriangleHypotenuse_];
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:.1f
                                               delay:0.f
                                             options:UIViewAnimationCurveEaseInOut
                                          animations:^{
                                              [self _updateButtonsLayoutWithTriangleHypotenuse:defaultTriangleHypotenuse_];
                                          }
                                          completion:^(BOOL finished) {
                                              isOpening_ = YES;
                                              isClosed_ = NO;
                                              isInProcessing_ = NO;
                                          }];
                     }];
}



// Close menu to hide all buttons around
- (void)_close:(NSNotification *)notification
{
//    if (isClosed_)
//        return;
    
    isInProcessing_ = YES;
    // Hide buttons with animation
    [UIView animateWithDuration:.3f
                          delay:0.f
                        options:UIViewAnimationCurveEaseIn
                     animations:^{
                         for (UIButton * button in [menu_ subviews])
                             [button setFrame:buttonOriginFrame_];
                         [self setAlpha:0.f];
                     }
                     completion:^(BOOL finished) {
                         isClosed_       = YES;
                         isOpening_      = NO;
                         isInProcessing_ = NO;
                     }];
}


- (void)_updateButtonsLayoutWithTriangleHypotenuse:(CGFloat)triangleHypotenuse
{
    //
    //  Triangle Values for Buttons' Position
    //
    //      /|      a: triangleA = c * cos(x)
    //   c / | b    b: triangleB = c * sin(x)
    //    /)x|      c: triangleHypotenuse
    //   -----      x: degree
    //     a
    //
    CGFloat centerBallMenuHalfSize = kKYCircleMenuSize         * .5f;
    CGFloat buttonRadius           = kKYCircleMenuCenterButtonSize * .5f;
    if (! triangleHypotenuse) triangleHypotenuse = defaultTriangleHypotenuse_; // Distance to Ball Center

  
    CGFloat degree    = M_PI / 3.0f; // = 60 * M_PI / 180
    CGFloat triangleA = triangleHypotenuse * cosf(degree);
    CGFloat triangleB = triangleHypotenuse * sinf(degree);
    [self _setButtonWithTag:1 origin:CGPointMake(centerBallMenuHalfSize - triangleB - buttonRadius,
                                                 centerBallMenuHalfSize - triangleA - buttonRadius)];
    [self _setButtonWithTag:2 origin:CGPointMake(centerBallMenuHalfSize - buttonRadius,
                                                 centerBallMenuHalfSize - triangleHypotenuse - buttonRadius)];
    [self _setButtonWithTag:3 origin:CGPointMake(centerBallMenuHalfSize + triangleB - buttonRadius,
                                                 centerBallMenuHalfSize - triangleA - buttonRadius)];
    [self _setButtonWithTag:4 origin:CGPointMake(centerBallMenuHalfSize - triangleB - buttonRadius,
                                                 centerBallMenuHalfSize + triangleA - buttonRadius)];
    [self _setButtonWithTag:5 origin:CGPointMake(centerBallMenuHalfSize - buttonRadius,
                                                 centerBallMenuHalfSize + triangleHypotenuse - buttonRadius)];
    [self _setButtonWithTag:6 origin:CGPointMake(centerBallMenuHalfSize + triangleB - buttonRadius,
                                                 centerBallMenuHalfSize + triangleA - buttonRadius)];
}

// Set Frame for button with special tag
- (void)_setButtonWithTag:(NSInteger)buttonTag origin:(CGPoint)origin
{
    UIButton * button = (UIButton *)[menu_ viewWithTag:buttonTag];
    [button setFrame:CGRectMake(origin.x, origin.y, kKYCircleMenuCenterButtonSize, kKYCircleMenuCenterButtonSize)];
    button = nil;
}


- (void)runButtonActions:(UIButton *)sender
{

 //   NSLog(@"%@", sender);
    
    if([self.delegte respondsToSelector:@selector(selectBubbleMenuButton:)])
    {
        [self.delegte selectBubbleMenuButton:sender.tag];
    }

    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject: @"dismiss"  forKey: @"menuBtnClicked"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"menuBtnClicked" object:nil userInfo:dict];
    

}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
