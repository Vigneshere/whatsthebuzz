//
//  bubbleVw.m
//  drag
//
//  Created by shaili_solutions_MacPro3 on 10/06/14.
//  Copyright (c) 2014 JemsNets. All rights reserved.
//

#import "bubbleVw.h"
#define navBarHeight 64
#define reduceBottomHeight 40


@implementation bubbleVw


+ (bubbleVw *)sharedInstance:(CGRect)frame size:(CGSize)size
{
    static dispatch_once_t pred;
    static bubbleVw *instance = nil;
    dispatch_once(&pred, ^{
        instance = [[self alloc] initFrame:frame size:size];
    });
	return instance;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Initialization
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (id)initFrame:(CGRect)frame size:(CGSize)size
{
    if( self = [super init] )
    {
        screenSize = size;
        UIImage* btnimg = [UIImage imageNamed:@"bubble"];

        self.frame = CGRectMake(0, 2*navBarHeight, btnimg.size.width, btnimg.size.height);
        self.backgroundColor = [UIColor clearColor];
        [self setImage:btnimg forState:UIControlStateNormal];
        [self setImage:btnimg forState:UIControlStateSelected];
        [self setImage:btnimg forState:UIControlStateSelected | UIControlStateHighlighted];
        self.adjustsImageWhenDisabled = NO;
        [self addTarget:self action:@selector(stopBtnClicked:withEvent:) forControlEvents:UIControlEventTouchUpInside];
        UIPanGestureRecognizer *panRecognizer;
        panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                action:@selector(wasDragged:)];
        // cancel touches so that touchUpInside touches are ignored
        panRecognizer.cancelsTouchesInView = YES;
        [self addGestureRecognizer:panRecognizer];
        self.clipsToBounds = YES;
        [self setUpYourObserver];
    }
    return self;
}
- (void)setUpYourObserver
{
    // This could be in an init method.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(myNotificationMethod:) name:UIKeyboardDidShowNotification object:nil];
}

- (void)myNotificationMethod:(NSNotification*)notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    [self whenKeyboardRaises:keyboardFrameBeginRect.origin.y];
}

-(void)whenKeyboardRaises:(int)yPoint
{
    
    
                        __block int tempYpoint = yPoint;

                             [UIView animateWithDuration:0.4
                                                   delay:0.0
                                                 options:UIViewAnimationOptionBeginFromCurrentState
                                              animations:^{
                                                  int selfYPosition = self.center.y;
                                                  if (self.frame.origin.y + self.frame.size.height > tempYpoint - reduceBottomHeight)
                                                      {
                                                          selfYPosition = tempYpoint - 2*reduceBottomHeight ;
                                                      }
                                                  
                              
                                                  self.center = CGPointMake(self.center.x, selfYPosition);
                                              }
                                              completion:^(BOOL finished){//this block starts only when
                                                  //the animation in the upper block ends
                                                  //so you know when exactly the animation ends
                                              }];
    
        
        
        
    
}

- (void)wasDragged:(UIPanGestureRecognizer *)recognizer {
    UIButton *button = (UIButton *)recognizer.view;
    CGPoint translation = [recognizer translationInView:button];
    
    button.center = CGPointMake(button.center.x + translation.x, button.center.y + translation.y);
    [recognizer setTranslation:CGPointZero inView:button];
    
    if(recognizer.state == UIGestureRecognizerStateEnded)
    {
        CGPoint point1=CGPointMake(button.center.x,button.center.y);
        
        if (button.center.x > screenSize.width/2 ) {
            point1=CGPointMake(screenSize.width ,button.center.y);
            
            
        }
        else if(button.center.x<=160){
            point1=CGPointMake(  0,button.center.y);
            
        }
        
        
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             button.center = point1;
                         }
                         completion:^(BOOL finished){//this block starts only when
                             //the animation in the upper block ends
                             //so you know when exactly the animation ends
                             
                             [UIView animateWithDuration:0.4
                                                   delay:0.0
                                                 options:UIViewAnimationOptionBeginFromCurrentState
                                              animations:^{
                                                  CGPoint temppoint;
                                                  if (point1.x < 160)
                                                  {
                                                   //   NSLog(@"%@",NSStringFromCGPoint (button.center));
                                                      
                                                      if (button.center.y < button.frame.size.height + navBarHeight)
                                                      {
                                                          temppoint = CGPointMake((button.frame.size.width/2),(button.frame.size.height/2) + navBarHeight );
                                                      }
                                                      else if (button.center.y > screenSize.height - button.frame.size.height- reduceBottomHeight)
                                                      {
                                                          temppoint = CGPointMake((button.frame.size.width/2), screenSize.height -(button.frame.size.height/2)- reduceBottomHeight );
                                                      }
                                                      else
                                                      {
                                                          temppoint = CGPointMake((button.frame.size.width/2),button.center.y );
                                                      }
                                                  }
                                                  else
                                                  {
                                                      
                                                      if (button.center.y < button.frame.size.height + navBarHeight)
                                                      {
                                                          temppoint = CGPointMake(screenSize.width - (button.frame.size.width/2),(button.frame.size.height/2) + navBarHeight) ;
                                                      }
                                                      else if (button.center.y > screenSize.height - button.frame.size.height - reduceBottomHeight)
                                                      {
                                                          temppoint = CGPointMake(screenSize.width - (button.frame.size.width/2), screenSize.height -(button.frame.size.height/2)- reduceBottomHeight );
                                                      }
                                                      else
                                                      {
                                                          temppoint = CGPointMake(screenSize.width - (button.frame.size.width/2),button.center.y );
                                                      }
                                                  }
                                                  button.center = temppoint;
                                              }
                                              completion:^(BOOL finished){//this block starts only when
                                                  //the animation in the upper block ends
                                                  //so you know when exactly the animation ends
                                              }];
                         }];
        
        
        
        
    }
}

-(void)stopBtnClicked:(UIButton *)button withEvent:(UIEvent *)event
{
        CGPoint point1=CGPointMake(button.center.x,button.center.y);
    
    restorePoint = point1;
    
    point1 = CGPointMake((screenSize.width)/2,(screenSize.height)/2 );
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         button.center = point1;
                         NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                         [dict setObject: @"Clicked"  forKey: @"bubbleStatus"];
                         
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"bubbleStatus" object:nil userInfo:dict];
                     }
                     completion:^(BOOL finished){//this block starts only when
                         //the animation in the upper block ends
                         //so you know when exactly the animation ends
                         
                        
                     }];
    
    
    
    
    }
-(void)restoreBtn
{
    CGPoint point1 = restorePoint;
    
    self.backgroundColor = [UIColor clearColor];

    
    
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.center = point1;
                     }
                     completion:^(BOOL finished){//this block starts only when
                                             }];
    
    
    
    
}



@end

