#import "XMPPMessageArchivingCoreDataStorage.h"
#import "XMPPCoreDataStorageProtected.h"
#import "XMPPLogging.h"
#import "NSXMLElement+XEP_0203.h"
#import "XMPPMessage+XEP_0085.h"
#import "XMPPMessage+XEP_0184.h"
#import "mediaDbEditor.h"


#if ! __has_feature(objc_arc)
#warning This file must be compiled with ARC. Use -fobjc-arc flag (or convert project to ARC).
#endif

// Log levels: off, error, warn, info, verbose
// Log flags: trace
#if DEBUG
static const int xmppLogLevel = XMPP_LOG_LEVEL_WARN; // VERBOSE; // | XMPP_LOG_FLAG_TRACE;
#else
static const int xmppLogLevel = XMPP_LOG_LEVEL_WARN;
#endif

@interface XMPPMessageArchivingCoreDataStorage ()
{
    NSString *messageEntityName;
    NSString *contactEntityName;
    Singleton *singletonObj;
   __block mediaDbEditor* mediaDb;
}

@end

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@implementation XMPPMessageArchivingCoreDataStorage

static XMPPMessageArchivingCoreDataStorage *sharedInstance;

+ (XMPPMessageArchivingCoreDataStorage *)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        sharedInstance = [[XMPPMessageArchivingCoreDataStorage alloc] initWithDatabaseFilename:nil storeOptions:nil];
        
        
    });
    
    return sharedInstance;
}

/**
 * Documentation from the superclass (XMPPCoreDataStorage):
 *
 * If your subclass needs to do anything for init, it can do so easily by overriding this method.
 * All public init methods will invoke this method at the end of their implementation.
 *
 * Important: If overriden you must invoke [super commonInit] at some point.
 **/
- (void)commonInit
{
    [super commonInit];
    
    messageEntityName = @"XMPPMessageArchiving_Message_CoreDataObject";
    contactEntityName = @"XMPPMessageArchiving_Contact_CoreDataObject";
}

/**
 * Documentation from the superclass (XMPPCoreDataStorage):
 *
 * Override me, if needed, to provide customized behavior.
 * For example, you may want to perform cleanup of any non-persistent data before you start using the database.
 *
 * The default implementation does nothing.
 **/
- (void)didCreateManagedObjectContext
{
    // If there are any "composing" messages in the database, delete them (as they are temporary).
    
    NSManagedObjectContext *moc = [self managedObjectContext];
    NSEntityDescription *messageEntity = [self messageEntity:moc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"composing == YES"];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = messageEntity;
    fetchRequest.predicate = predicate;
    fetchRequest.fetchBatchSize = saveThreshold;
    
    NSError *error = nil;
    NSArray *messages = [moc executeFetchRequest:fetchRequest error:&error];
    
    if (messages == nil)
    {
        XMPPLogError(@"%@: %@ - Error executing fetchRequest: %@", [self class], THIS_METHOD, error);
        return;
    }
    
    NSUInteger count = 0;
    
    for (XMPPMessageArchiving_Message_CoreDataObject *message in messages)
    {
        [moc deleteObject:message];
        
        if (++count > saveThreshold)
        {
            if (![moc save:&error])
            {
                XMPPLogWarn(@"%@: Error saving - %@ %@", [self class], error, [error userInfo]);
                [moc rollback];
            }
        }
    }
    
    if (count > 0)
    {
        if (![moc save:&error])
        {
            XMPPLogWarn(@"%@: Error saving - %@ %@", [self class], error, [error userInfo]);
            [moc rollback];
        }
    }
}


-(void)sendingVirtualMediaMessage:(XMPPMessage *)message XmppStream:(id)xmppStream
{
    
    [self archiveMessage:message outgoing:YES xmppStream:xmppStream];
}


-(XMPPMessageArchiving_Message_CoreDataObject *)checkMessagePresent:(XMPPMessage *)message
                                                           outgoing:(BOOL)isOutgoing xmppStream:(XMPPStream *)xmppStream
                                               managedObjectContext:(NSManagedObjectContext *)moc
{
    
    if ([[message attributeForName:@"id"]stringValue] == nil) {
        return nil;
    }
    
    //    XMPPJID *myJid = [self myJIDForXMPPStream:xmppStream];
    //
    //    XMPPJID *messageJid = isOutgoing ? [message to] : [message from];
    
    
    XMPPMessageArchiving_Message_CoreDataObject *result = nil;
    
    NSEntityDescription *messageEntity = [self messageEntity:moc];
    
    // Order matters:
    // 1. composing - most likely not many with it set to YES in database
    // 2. bareJidStr - splits database by number of conversations
    // 3. outgoing - splits database in half
    // 4. streamBareJidStr - might not limit database at all
    
    NSString *predicateFrmt = @"messageID == %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,[[message attributeForName:@"id"] stringValue]];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = messageEntity;
    fetchRequest.predicate = predicate;
    fetchRequest.sortDescriptors = [NSSortDescriptor sortValues:@"timestamp" ascending:NO];
    fetchRequest.fetchLimit = 1;
    
    NSError *error = nil;
    NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
    
    if (results == nil)
    {
        XMPPLogError(@"%@: %@ - Error executing fetchRequest: %@", THIS_FILE, THIS_METHOD, fetchRequest);
    }
    else
    {
        result = (XMPPMessageArchiving_Message_CoreDataObject *)[results lastObject];
    }
    
    
    return result;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Internal API
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)willInsertMessage:(XMPPMessageArchiving_Message_CoreDataObject *)message
{
    // Override hook
}

- (void)didUpdateMessage:(XMPPMessageArchiving_Message_CoreDataObject *)message
{
    // Override hook
}

- (void)willDeleteMessage:(XMPPMessageArchiving_Message_CoreDataObject *)message
{
    // Override hook
}

- (void)willInsertContact:(XMPPMessageArchiving_Contact_CoreDataObject *)contact
{
    // Override hook
}

- (void)didUpdateContact:(XMPPMessageArchiving_Contact_CoreDataObject *)contact
{
    // Override hook
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Private API
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (XMPPMessageArchiving_Message_CoreDataObject *)composingMessageWithJid:(XMPPJID *)messageJid
                                                               streamJid:(XMPPJID *)streamJid
                                                                outgoing:(BOOL)isOutgoing
                                                    managedObjectContext:(NSManagedObjectContext *)moc
{
    XMPPMessageArchiving_Message_CoreDataObject *result = nil;
    
    NSEntityDescription *messageEntity = [self messageEntity:moc];
    
    // Order matters:
    // 1. composing - most likely not many with it set to YES in database
    // 2. bareJidStr - splits database by number of conversations
    // 3. outgoing - splits database in half
    // 4. streamBareJidStr - might not limit database at all
    
    NSString *predicateFrmt = @"composing == YES AND bareJidStr == %@ AND outgoing == %@ AND streamBareJidStr == %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,
                              [messageJid bare], [NSNumber numberWithBool:isOutgoing], [streamJid bare]];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = messageEntity;
    fetchRequest.predicate = predicate;
    fetchRequest.sortDescriptors = [NSSortDescriptor sortValues:@"timestamp" ascending:NO];
    fetchRequest.fetchLimit = 1;
    
    NSError *error = nil;
    NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
    
    if (results == nil)
    {
        XMPPLogError(@"%@: %@ - Error executing fetchRequest: %@", THIS_FILE, THIS_METHOD, fetchRequest);
    }
    else
    {
        result = (XMPPMessageArchiving_Message_CoreDataObject *)[results lastObject];
    }
    
    return result;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Public API
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (XMPPMessageArchiving_Contact_CoreDataObject *)contactForMessage:(XMPPMessageArchiving_Message_CoreDataObject *)msg
{
    // Potential override hook
    
    return [self contactWithBareJidStr:msg.bareJidStr
                      streamBareJidStr:msg.streamBareJidStr
                  managedObjectContext:msg.managedObjectContext];
}

- (XMPPMessageArchiving_Contact_CoreDataObject *)contactWithJid:(XMPPJID *)contactJid
                                                      streamJid:(XMPPJID *)streamJid
                                           managedObjectContext:(NSManagedObjectContext *)moc
{
    return [self contactWithBareJidStr:[contactJid bare]
                      streamBareJidStr:[streamJid bare]
                  managedObjectContext:moc];
}

- (XMPPMessageArchiving_Contact_CoreDataObject *)contactWithBareJidStr:(NSString *)contactBareJidStr
                                                      streamBareJidStr:(NSString *)streamBareJidStr
                                                  managedObjectContext:(NSManagedObjectContext *)moc
{
    NSEntityDescription *entity = [self contactEntity:moc];
    
    NSPredicate *predicate;
    if (streamBareJidStr)
    {
        predicate = [NSPredicate predicateWithFormat:@"bareJidStr == %@ AND streamBareJidStr == %@",
                     contactBareJidStr, streamBareJidStr];
    }
    else
    {
        predicate = [NSPredicate predicateWithFormat:@"bareJidStr == %@", contactBareJidStr];
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    [fetchRequest setFetchLimit:1];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
    
    if (results == nil)
    {
        XMPPLogError(@"%@: %@ - Fetch request error: %@", THIS_FILE, THIS_METHOD, error);
        return nil;
    }
    else
    {
        return (XMPPMessageArchiving_Contact_CoreDataObject *)[results lastObject];
    }
}

- (NSString *)messageEntityName
{
    __block NSString *result = nil;
    
    dispatch_block_t block = ^{
        result = messageEntityName;
    };
    
    if (dispatch_get_specific(storageQueueTag))
        block();
    else
        dispatch_sync(storageQueue, block);
    
    return result;
}

- (void)setMessageEntityName:(NSString *)entityName
{
    dispatch_block_t block = ^{
        messageEntityName = entityName;
    };
    
    if (dispatch_get_specific(storageQueueTag))
        block();
    else
        dispatch_async(storageQueue, block);
}

- (NSString *)contactEntityName
{
    __block NSString *result = nil;
    
    dispatch_block_t block = ^{
        result = contactEntityName;
    };
    
    if (dispatch_get_specific(storageQueueTag))
        block();
    else
        dispatch_sync(storageQueue, block);
    
    return result;
}

- (void)setContactEntityName:(NSString *)entityName
{
    dispatch_block_t block = ^{
        contactEntityName = entityName;
    };
    
    if (dispatch_get_specific(storageQueueTag))
        block();
    else
        dispatch_async(storageQueue, block);
}

- (NSEntityDescription *)messageEntity:(NSManagedObjectContext *)moc
{
    // This is a public method, and may be invoked on any queue.
    // So be sure to go through the public accessor for the entity name.
    
    return [NSEntityDescription entityForName:[self messageEntityName] inManagedObjectContext:moc];
}

- (NSEntityDescription *)contactEntity:(NSManagedObjectContext *)moc
{
    // This is a public method, and may be invoked on any queue.
    // So be sure to go through the public accessor for the entity name.
    
    return [NSEntityDescription entityForName:[self contactEntityName] inManagedObjectContext:moc];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Storage Protocol
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (BOOL)configureWithParent:(XMPPMessageArchiving *)aParent queue:(dispatch_queue_t)queue
{
    return [super configureWithParent:aParent queue:queue];
}

- (void)archiveMessage:(XMPPMessage *)message outgoing:(BOOL)isOutgoing xmppStream:(XMPPStream *)xmppStream
{
    if (!singletonObj) {
        singletonObj = [Singleton sharedMySingleton];
    }
    
    // Message should either have a body, or be a composing notification
    
    __block	NSString *messageBody = [[message elementForName:@"body"] stringValue];
    __block	BOOL isComposing = NO;
    __block BOOL shouldDeleteComposingMessage = NO;
    __block BOOL isRequestReceipt = NO;
    __block BOOL isReceivedReceipt = NO;
    __block BOOL isCheckPrivateChat = NO;
    XMPPJID *messageJid = isOutgoing ? [message to] : [message from];
    
    if ([message isGroupChatMessage]) {
        NSString *resourceStr = messageJid.resource;
        
        if ([resourceStr rangeOfString:DOMAINAME].location==NSNotFound )
            resourceStr = [resourceStr stringByAppendingString:[NSString stringWithFormat:@"@%@",DOMAINAME]];
        
        messageJid = [XMPPJID jidWithString:resourceStr];
    }
    
    [self getDisplayName:messageJid compeletion:^(BOOL value,NSString *displayName)
     {
         if (value) {
             
             if ([messageBody length] == 0)
             {
                 // Message doesn't have a body.
                 // Check to see if it has a chat state (composing, paused, etc).
                 
                 isComposing = [message hasComposingChatState];
                 
                 if (isComposing && isOutgoing) {
                     return ;
                     
                 }
                 else if (!isComposing)
                 {
                     if ([message hasChatState])
                     {
                         // Message has non-composing chat state.
                         // So if there is a current composing message in the database,
                         // then we need to delete it.
                         shouldDeleteComposingMessage = YES;
                     }
                     
                     else if([message hasReceiptResponse])
                     {
                         isReceivedReceipt  = YES;
                         [self updatedeliveryReport:message];
                         return;
                         
                     }
                     else if([message isGroupChatMessageWithSubject])
                     {
                         messageBody = [message subject];
                     }
                     else
                     {
                         // Message has no body and no chat state.
                         // Nothing to do with it.
                         return;
                     }
                 }
                 
                 
             }
             else if([message hasReceiptRequest])
             {
                 isRequestReceipt = YES;
             }
             
             
             [self scheduleBlock:^{
                 
                 NSManagedObjectContext *moc = [self managedObjectContext];
                 
                 XMPPJID *myJid = [self myJIDForXMPPStream:xmppStream];
                 
                 XMPPJID *messageJid = isOutgoing ? [message to] : [message from];
                 
                 // Fetch-n-Update OR Insert new message
                 
                 XMPPMessageArchiving_Message_CoreDataObject *archivedMessage =
                 [self composingMessageWithJid:messageJid
                                     streamJid:myJid
                                      outgoing:isOutgoing
                          managedObjectContext:moc];
                 
                 if (shouldDeleteComposingMessage)
                 {
                     if (archivedMessage)
                     {
                         [self willDeleteMessage:archivedMessage]; // Override hook
                         [moc deleteObject:archivedMessage];
                     }
                     else
                     {
                         // Composing message has already been deleted (or never existed)
                     }
                 }
                 else
                 {
                     XMPPLogVerbose(@"Previous archivedMessage: %@", archivedMessage);
                     
                     BOOL didCreateNewArchivedMessage = NO;
                     if (archivedMessage == nil)
                     {
                         archivedMessage = (XMPPMessageArchiving_Message_CoreDataObject *)
                         [[NSManagedObject alloc] initWithEntity:[self messageEntity:moc]
                                  insertIntoManagedObjectContext:nil];
                         
                         
                         didCreateNewArchivedMessage = YES;
                     }
                     
                     XMPPMessageArchiving_Message_CoreDataObject *checkArchMessage =
                     [self checkMessagePresent:message outgoing:isOutgoing xmppStream:xmppStream managedObjectContext:moc];
                     
                     if (checkArchMessage  !=  nil) {
                         
                         
                         //                checkArchMessage.message = message;
                         //                checkArchMessage.body = messageBody;
                         //                NSError *error;
                         //                if (![moc save:&error]) {
                         //                    NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                         //                }
                         
                         if (isRequestReceipt)
                             checkArchMessage.delivery = @"sent";
                         [moc save:nil];
                         return ;
                         
                     }
                     archivedMessage.message = message;
                     archivedMessage.body = messageBody;
                     if ([message isGroupChatMessage]) {
                         archivedMessage.delivery = @"sent";
                         
                         NSXMLElement *groupJoinLeave = [message elementForName:@"group"];
                         if ([[[groupJoinLeave attributeForName:@"type"]stringValue] isEqualToString:@"1"] ) {
                             
                             archivedMessage.body = [NSString stringWithFormat:@"%@ joined",displayName];
                         }
                         else if ([[[groupJoinLeave attributeForName:@"type"]stringValue] isEqualToString:@"0"])
                         {
                             archivedMessage.body = [NSString stringWithFormat:@"%@ left",displayName];
                             
                         }
                         else if ([[[groupJoinLeave attributeForName:@"type"]stringValue] isEqualToString:@"3"])
                         {
                             archivedMessage.body = [NSString stringWithFormat:@"%@ changed the group icon",displayName];
                             
                         }
                         else if ([[[groupJoinLeave attributeForName:@"type"]stringValue] isEqualToString:@"4"])
                         {
                            
                             

                             archivedMessage.body = [NSString stringWithFormat:@"invited"];
                         }

                     }
                     
                     archivedMessage.bareJid = [messageJid bareJID];
                     archivedMessage.streamBareJidStr = [myJid bare];
                     if (isRequestReceipt)
                         archivedMessage.delivery = @"sent";
                     archivedMessage.messageID = [[message attributeForName:@"id"] stringValue];
                     
                     NSDate *timestamp = [message delayedDeliveryDate];
                     if (timestamp)
                         archivedMessage.timestamp = timestamp;
                     else
                         archivedMessage.timestamp = [[NSDate alloc] init];
                     
                     archivedMessage.thread = [[message elementForName:@"thread"] stringValue];
                     archivedMessage.isOutgoing = isOutgoing;
                     if ([message isGroupChatMessage])//==>Check whether message sent from This User in groupChat
                     {
                         
                         if ([[message from].resource isEqualToString:xmppStream.myJID.user]) {
                             archivedMessage.isOutgoing = YES;
                             
                         }
                     }
                     
                     archivedMessage.isComposing = isComposing;
                     if ([message isGroupChatMessage])
                         archivedMessage.type = @"groupChat";
                     else if([message isChatMessage])
                         archivedMessage.type = @"singleChat";
                     NSXMLElement *private = [message elementForName:@"private"];
                     NSXMLElement *file = [message elementForName:@"file"];

                     if (private && ( [[[private attributeForName:@"type"] stringValue] isEqualToString:@"message"] || file ))
                     {
                         
                         archivedMessage.isPrivate = YES;
                         isCheckPrivateChat = YES;
                         if ([singletonObj.chatScreenUser isEqualToString:[message to].bare] || [singletonObj.chatScreenUser isEqualToString:[message from].bare])
                             archivedMessage.privateStatus = NO;
                         else
                             archivedMessage.privateStatus = YES;
                         
                     }
                     else
                     {
                         archivedMessage.privateStatus  = NO;
                     }
                     
                     
                     archivedMessage.sectionDate = [self convertDate:archivedMessage.timestamp];
                     
                     XMPPLogVerbose(@"New archivedMessage: %@", archivedMessage);
                     
                     if (didCreateNewArchivedMessage) // [archivedMessage isInserted] doesn't seem to work
                     {
                         XMPPLogVerbose(@"Inserting message...");
                         
                         [archivedMessage willInsertObject];       // Override hook
                         [self willInsertMessage:archivedMessage]; // Override hook
                         [moc insertObject:archivedMessage];
                     }
                     else
                     {
                         XMPPLogVerbose(@"Updating message...");
                         
                         [archivedMessage didUpdateObject];       // Override hook
                         [self didUpdateMessage:archivedMessage]; // Override hook
                     }
                     
                     // Create or update contact (if message with actual content)
                     
                     if ([messageBody length] > 0 )
                     {
                         if (!isCheckPrivateChat) {
                             
                             
                             BOOL didCreateNewContact = NO;
                             
                             XMPPMessageArchiving_Contact_CoreDataObject *contact = [self contactForMessage:archivedMessage];
                             XMPPLogVerbose(@"Previous contact: %@", contact);
                             
                             if (contact == nil)
                             {
                                 contact = (XMPPMessageArchiving_Contact_CoreDataObject *)
                                 [[NSManagedObject alloc] initWithEntity:[self contactEntity:moc]
                                          insertIntoManagedObjectContext:nil];
                                 
                                 didCreateNewContact = YES;
                             }
                             
                             contact.streamBareJidStr = archivedMessage.streamBareJidStr;
                             contact.bareJid = archivedMessage.bareJid;
                             
                             contact.mostRecentMessageTimestamp = archivedMessage.timestamp;
                             contact.mostRecentMessageBody = archivedMessage.body;
                             contact.mostRecentMessageOutgoing = [NSNumber numberWithBool:isOutgoing];
                             if (!isOutgoing && [message  isChatMessage]) {
                                 NSNumber *number = [NSNumber numberWithInt:0];
                                 if (contact.unreadedMsg != nil) {
                                     number = contact.unreadedMsg;
                                     int value = [number intValue];
                                     number = [NSNumber numberWithInt:value + 1];
                                 }
                                 contact.unreadedMsg = number;
                             }
                             else if (![[message from].resource isEqualToString:[xmppStream myJID].user] && [message isGroupChatMessage])
                             {
                                 
                                 NSNumber *number = [NSNumber numberWithInt:0];
                                 if (contact.unreadedMsg != nil) {
                                     number = contact.unreadedMsg;
                                     int value = [number intValue];
                                     number = [NSNumber numberWithInt:value + 1];
                                 }
                                 contact.unreadedMsg = number;
                             }
                             
                             
                             if ([archivedMessage.message isGroupChatMessage])
                             {
                                 contact.type = @"groupChat";
                             }
                             else if([archivedMessage.message isChatMessage])
                             {
                                 contact.type = @"singleChat";
                                 if (displayName) {
                                     contact.displayName = displayName;
                                 }
                                 
                             }
                             if ([archivedMessage.message isGroupChatMessageWithSubject]) {
                                 contact.displayName = [archivedMessage.message subject];
                             }
                             
                             XMPPLogVerbose(@"New contact: %@", contact);
                             
                             if (didCreateNewContact) // [contact isInserted] doesn't seem to work
                             {
                                 XMPPLogVerbose(@"Inserting contact...");
                                 
                                 [contact willInsertObject];       // Override hook
                                 [self willInsertContact:contact]; // Override hook
                                 [moc insertObject:contact];
                             }
                             else
                             {
                                 XMPPLogVerbose(@"Updating contact...");
                                 
                                 [contact didUpdateObject];       // Override hook
                                 [self didUpdateContact:contact]; // Override hook
                             }
                         }
                     }
                 }
             }];
         }
         
     }];
    
   // NSLog(@"GOT Message");
    
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark DataBase Methods For Recent Chats
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- ( void)getDisplayName:(XMPPJID *)Str compeletion:(void (^)(BOOL,NSString *))completion
{
    
    
    __block NSString *stringName = nil;
    dispatch_async(dispatch_get_main_queue(), ^{
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                                  inManagedObjectContext:[self managedObjectContext_roster]];
        [request setEntity:entity];
        NSString *predicateFrmt = @"jidStr = %@";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, Str.bareJID];
        [request setPredicate:predicate];
        NSArray *empArray=[[self managedObjectContext_roster] executeFetchRequest:request error:nil];
        if ([empArray count] == 1)
        {
            for (XMPPUserCoreDataStorageObject *user in  empArray)
            {
                stringName = user.displayName;
            }
        }
        
        
        if (stringName)
            completion(YES,stringName);
        if ([Str.bareJID isEqualToJID: [self Userstream].myJID.bareJID])
            completion(YES,@"You");
        else
            completion(YES,Str.bareJID.user);
        
        
    });
    
    
}
- (NSManagedObjectContext *)managedObjectContext_roster
{
    return [[[self appDelegate] xmppRosterStorage] mainThreadManagedObjectContext];
}
- (AppDelegate *)appDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
-(XMPPStream *)Userstream
{
    return [self appDelegate].xmppStream;
}
- (void)updateUnreadMessage:(XMPPJID *)user clearValue:(BOOL)clear
{
    [self scheduleBlock:^{
        NSManagedObjectContext *moc = [self managedObjectContext];
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Contact_CoreDataObject"
                                                  inManagedObjectContext:moc];
        [request setEntity:entity];
        NSString *predicateFrmt = @"bareJidStr = %@";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, [user bareJID]];
        [request setPredicate:predicate];
        [request setFetchLimit:1];
        NSArray *empArray=[moc executeFetchRequest:request error:nil];
        if ([empArray count] == 1){
            XMPPMessageArchiving_Contact_CoreDataObject *user = (XMPPMessageArchiving_Contact_CoreDataObject *)empArray[0];
            NSNumber *number = user.unreadedMsg;
            if (clear)
                user.unreadedMsg = [NSNumber numberWithInt:0];
            else
            {
                int value = [number intValue];
                number = [NSNumber numberWithInt:value + 1];
                user.unreadedMsg = number;
            }
        }
        else
        {
         //   NSLog(@"Nothin");
        }
        NSError *error;
        if (![moc save:&error]) {
          //  NSLog(@"%@",error);
        }
    }];
    
    
    [self scheduleBlock:^{
        NSManagedObjectContext *moc = [self managedObjectContext];
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject"
                                                  inManagedObjectContext:moc];
        [request setEntity:entity];
        NSString *predicateFrmt = @"bareJidStr = %@ AND privateStatus == NO AND isPrivate == YES";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, [user bareJID]];
        [request setPredicate:predicate];
        NSArray *empArray=[moc executeFetchRequest:request error:nil];
        if ([empArray count]  > 0){
            
            for (XMPPMessageArchiving_Message_CoreDataObject *message in empArray)
            {
                
                NSXMLElement *private = [message.message elementForName:@"private"];
                
                NSXMLElement *file = [message.message elementForName:@"file"];
                
                if (private && ( [[[private attributeForName:@"type"] stringValue] isEqualToString:@"message"] || file ))
                    message.privateStatus  = YES;
            }
            NSError *error;
            if (![moc save:&error]) {
               // NSLog(@"%@",error);
            }
            
        }
    }];
    
}
-(void)updatedeliveryReport:(XMPPMessage *)message
{
    [self scheduleBlock:^{
        NSManagedObjectContext *moc = [self managedObjectContext];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject"
                                                  inManagedObjectContext:moc];
        [request setEntity:entity];
        NSString *predicateFrmt = @"messageID = %@";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, [message receiptResponseID]];
        [request setPredicate:predicate];
        NSArray *empArray=[moc executeFetchRequest:request error:nil];
        if ([empArray count] == 1){
            for (XMPPMessageArchiving_Message_CoreDataObject *user in  empArray)
            {
                user.delivery = @"delivered";
            }
        }
        [[self managedObjectContext] save:nil];
    }];
}
-(NSString *)convertDate:(NSDate *)date
{
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setDateStyle:NSDateFormatterMediumStyle];
    NSString* myString = [df stringFromDate:date];
    return myString;
}


-(void)updateMessageOnRecentChat:(NSString *)jidstring
{
    
    [self scheduleBlock:^{

        NSManagedObjectContext *context= [self managedObjectContext];
        XMPPMessageArchiving_Contact_CoreDataObject *modelObj2;
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        
        NSFetchRequest *request2 = [[NSFetchRequest alloc] init];
        [request2 setEntity:[NSEntityDescription entityForName:@"XMPPMessageArchiving_Contact_CoreDataObject" inManagedObjectContext:context]];
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"bareJidStr == %@",jidstring];
        [request2 setPredicate:predicate2];
        
        NSError *error = nil;
        NSArray *Contactresults2 = [context executeFetchRequest:request2 error:&error];
        
        
        if ([Contactresults2 count] > 0)
        {
            modelObj2 = (XMPPMessageArchiving_Contact_CoreDataObject *)[Contactresults2 objectAtIndex:0];
            
            [request setEntity:[NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:context]];
            NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"bareJidStr == %@",jidstring];
            [request setSortDescriptors:[NSSortDescriptor sortValues:@"timestamp" ascending:NO]];
            [request setPredicate:predicate2];
            NSArray *results2 = [context executeFetchRequest:request error:NULL];
            if ([results2 count] > 0)
            {
                XMPPMessageArchiving_Message_CoreDataObject *modelObj3 = (XMPPMessageArchiving_Message_CoreDataObject *)[results2 objectAtIndex:0];
                if (modelObj3.body != nil) {
                    
                    [modelObj2 setValue: modelObj3.bareJidStr forKey:@"bareJidStr"];
                    [modelObj2 setValue: modelObj3.bareJid forKey:@"bareJid"];
                    [modelObj2 setValue: modelObj3.body forKey:@"mostRecentMessageBody"];
                    [modelObj2 setValue: modelObj3.streamBareJidStr forKey:@"streamBareJidStr"];
                    [modelObj2 setValue: modelObj3.timestamp forKey:@"mostRecentMessageTimestamp"];
                    
                    if (![context save:&error])
                    {
                        NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
                    }
                    
                    
                }
            }
            else
            {
                modelObj2 = (XMPPMessageArchiving_Contact_CoreDataObject *)[Contactresults2 objectAtIndex:0];
                
                [modelObj2 setValue: modelObj2.bareJidStr forKey:@"bareJidStr"];
                [modelObj2 setValue: modelObj2.bareJid forKey:@"bareJid"];
                [modelObj2 setValue: @" " forKey:@"mostRecentMessageBody"];
                [modelObj2 setValue: modelObj2.streamBareJidStr forKey:@"streamBareJidStr"];
                [modelObj2 setValue: modelObj2.mostRecentMessageTimestamp forKey:@"mostRecentMessageTimestamp"];
                
                if (![context save:&error])
                {
                    NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
                }
                
                
            }
            
        }
    }];
    
    
    
    
}



- (void)clearChatWithID:(NSString*)JidStr
{
   [self scheduleBlock:^{
    NSManagedObjectContext *moc = [self managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:moc];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    NSString *predicateFrmt = @"bareJidStr == %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,JidStr];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:[NSSortDescriptor sortValues:@"timestamp" ascending:YES]];
    NSArray *delArray=[moc executeFetchRequest:fetchRequest error:nil];
    
    
    for (XMPPMessageArchiving_Message_CoreDataObject *CoreDataObject in delArray)
    {
        
        [moc deleteObject:CoreDataObject];
        
        NSXMLElement *file = [CoreDataObject.message elementForName:@"file"];
        
        if (file)
        {
             mediaDb = [mediaDbEditor sharedInstance];
            [mediaDb deleteMedia:CoreDataObject.messageID];
        }
        
        
        NSError *error = nil;
        
        if (![moc save:&error])
        {
            //  NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
        }
    }
       
   }];
   
    [self updateMessageOnRecentChat:JidStr];

}


- (void)clearSingleChatWithID:(NSString*)JidStr isPrivate:(BOOL)isPrivate
{
    [self scheduleBlock:^{

    NSManagedObjectContext *moc = [self managedObjectContext];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:moc];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:entity];
        NSString *predicateFrmt = @"bareJidStr == %@";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,JidStr];
        [fetchRequest setPredicate:predicate];
        [fetchRequest setSortDescriptors:[NSSortDescriptor sortValues:@"timestamp" ascending:YES]];
        NSArray *delArray=[moc executeFetchRequest:fetchRequest error:nil];
        
        for (XMPPMessageArchiving_Message_CoreDataObject *CoreDataObject in delArray)
        {
            if (!CoreDataObject.isPrivate)
            {
                NSXMLElement *file = [CoreDataObject.message elementForName:@"file"];
                
                if (file)
                {
                    mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
                    [mediaDb deleteMedia:CoreDataObject.messageID];
                }
                
                [moc deleteObject:CoreDataObject];
            }
            
            NSError *error = nil;
            if (![moc save:&error])
            {
                //  NSLog(@"Sorry, couldn't delete values %@", [error slocalizedDescription]);
            }
        }
    }];
    
    [self updateMessageOnRecentChat:JidStr];
}




   










@end
