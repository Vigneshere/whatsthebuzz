#import "XMPPRoomCoreDataStorage.h"
#import "XMPPCoreDataStorageProtected.h"
#import "NSXMLElement+XEP_0203.h"
#import "XMPPLogging.h"
#define domainName @"@128.199.136.36"
#if ! __has_feature(objc_arc)
#warning This file must be compiled with ARC. Use -fobjc-arc flag (or convert project to ARC).
#endif

// Log levels: off, error, warn, info, verbose
#if DEBUG
static const int xmppLogLevel = XMPP_LOG_LEVEL_WARN; // | XMPP_LOG_FLAG_TRACE;
#else
static const int xmppLogLevel = XMPP_LOG_LEVEL_WARN;
#endif

#define AssertPrivateQueue() \
NSAssert(dispatch_get_specific(storageQueueTag), @"Private method: MUST run on storageQueue");

@interface XMPPRoomCoreDataStorage ()
{
	/* Inherited from XMPPCoreDataStorage
     
     NSString *databaseFileName;
     NSUInteger saveThreshold;
     
     dispatch_queue_t storageQueue;
     
     */
	
	NSString *messageEntityName;
	NSString *occupantEntityName;
    NSString *messageDetailsEntityName;
    
	
	NSTimeInterval maxMessageAge;
	NSTimeInterval deleteInterval;
	
	NSMutableSet *pausedMessageDeletion;
	
	dispatch_time_t lastDeleteTime;
	dispatch_source_t deleteTimer;
}

- (NSEntityDescription *)messageEntity:(NSManagedObjectContext *)moc;
- (NSEntityDescription *)occupantEntity:(NSManagedObjectContext *)moc;
- (NSEntityDescription *)messageDetailsEntity:(NSManagedObjectContext *)moc;

- (void)performDelete;
- (void)destroyDeleteTimer;
- (void)updateDeleteTimer;
- (void)createAndStartDeleteTimer;

- (void)clearAllOccupantsFromRoom:(XMPPJID *)roomJID;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@implementation XMPPRoomCoreDataStorage

static XMPPRoomCoreDataStorage *sharedInstance;

+ (XMPPRoomCoreDataStorage *)sharedInstance
{
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		
		sharedInstance = [[XMPPRoomCoreDataStorage alloc] initWithDatabaseFilename:nil storeOptions:nil];
	});
	
	return sharedInstance;
}

- (void)commonInit
{
	XMPPLogTrace();
	[super commonInit];
	
	// This method is invoked by all public init methods of the superclass
	
	messageEntityName = NSStringFromClass([XMPPRoomMessageCoreDataStorageObject class]);
	occupantEntityName = NSStringFromClass([XMPPRoomOccupantCoreDataStorageObject class]);
    messageDetailsEntityName = NSStringFromClass([XMPPRoomMessageDetails class]);
    
	
	maxMessageAge  = (60 * 60 * 24 * 7); // 7 days
	deleteInterval = (60 * 5);           // 5 days
	
	pausedMessageDeletion = [[NSMutableSet alloc] init];
}

- (void)dealloc
{
	[self destroyDeleteTimer];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Configuration
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSString *)messageEntityName
{
	__block NSString *result = nil;
	
	dispatch_block_t block = ^{
		result = messageEntityName;
	};
	
	if (dispatch_get_specific(storageQueueTag))
		block();
	else
		dispatch_sync(storageQueue, block);
	
	return result;
}

- (void)setMessageEntityName:(NSString *)newMessageEntityName
{
	dispatch_block_t block = ^{
		messageEntityName = newMessageEntityName;
	};
	
	if (dispatch_get_specific(storageQueueTag))
		block();
	else
		dispatch_async(storageQueue, block);
}

- (NSString *)occupantEntityName
{
	__block NSString *result = nil;
	
	dispatch_block_t block = ^{
		result = occupantEntityName;
	};
	
	if (dispatch_get_specific(storageQueueTag))
		block();
	else
		dispatch_sync(storageQueue, block);
	
	return result;
}

- (void)setOccupantEntityName:(NSString *)newOccupantEntityName
{
	dispatch_block_t block = ^{
		occupantEntityName = newOccupantEntityName;
	};
	
	if (dispatch_get_specific(storageQueueTag))
		block();
	else
		dispatch_async(storageQueue, block);
}

- (NSString *)messageDetailsEntityName
{
	__block NSString *result = nil;
	
	dispatch_block_t block = ^{
		result = messageDetailsEntityName;
	};
	
	if (dispatch_get_specific(storageQueueTag))
		block();
	else
		dispatch_sync(storageQueue, block);
	
	return result;
}

- (void)setMessageDetailsEntityName:(NSString *)newmessageDetailsEntityNameEntityName
{
	dispatch_block_t block = ^{
		messageDetailsEntityName = newmessageDetailsEntityNameEntityName;
	};
	
	if (dispatch_get_specific(storageQueueTag))
		block();
	else
		dispatch_async(storageQueue, block);
}

- (NSTimeInterval)maxMessageAge
{
	__block NSTimeInterval result = 0;
	
	dispatch_block_t block = ^{
		result = maxMessageAge;
	};
	
	if (dispatch_get_specific(storageQueueTag))
		block();
	else
		dispatch_sync(storageQueue, block);
	
	return result;
}

- (void)setMaxMessageAge:(NSTimeInterval)age
{
	dispatch_block_t block = ^{ @autoreleasepool {
		
		NSTimeInterval oldMaxMessageAge = maxMessageAge;
		NSTimeInterval newMaxMessageAge = age;
		
		maxMessageAge = age;
		
		// There are several cases we need to handle here.
		//
		// 1. If the maxAge was previously enabled and it just got disabled,
		//    then we need to stop the deleteTimer. (And we might as well release it.)
		//
		// 2. If the maxAge was previously disabled and it just got enabled,
		//    then we need to setup the deleteTimer. (Plus we might need to do an immediate delete.)
		//
		// 3. If the maxAge was increased,
		//    then we don't need to do anything.
		//
		// 4. If the maxAge was decreased,
		//    then we should do an immediate delete.
		
		BOOL shouldDeleteNow = NO;
		
		if (oldMaxMessageAge > 0.0)
		{
			if (newMaxMessageAge <= 0.0)
			{
				// Handles #1
				[self destroyDeleteTimer];
			}
			else if (oldMaxMessageAge > newMaxMessageAge)
			{
				// Handles #4
				shouldDeleteNow = YES;
			}
			else
			{
				// Handles #3
				// Nothing to do now
			}
		}
		else if (newMaxMessageAge > 0.0)
		{
			// Handles #2
			shouldDeleteNow = YES;
		}
		
		if (shouldDeleteNow)
		{
			[self performDelete];
			
			if (deleteTimer)
				[self updateDeleteTimer];
			else
				[self createAndStartDeleteTimer];
		}
	}};
	
	if (dispatch_get_specific(storageQueueTag))
		block();
	else
		dispatch_async(storageQueue, block);
}

- (NSTimeInterval)deleteInterval
{
	__block NSTimeInterval result = 0;
	
	dispatch_block_t block = ^{
		result = deleteInterval;
	};
	
	if (dispatch_get_specific(storageQueueTag))
		block();
	else
		dispatch_sync(storageQueue, block);
	
	return result;
}

- (void)setDeleteInterval:(NSTimeInterval)interval
{
	dispatch_block_t block = ^{ @autoreleasepool {
		
		deleteInterval = interval;
		
		// There are several cases we need to handle here.
		//
		// 1. If the deleteInterval was previously enabled and it just got disabled,
		//    then we need to stop the deleteTimer. (And we might as well release it.)
		//
		// 2. If the deleteInterval was previously disabled and it just got enabled,
		//    then we need to setup the deleteTimer. (Plus we might need to do an immediate delete.)
		//
		// 3. If the deleteInterval increased, then we need to reset the timer so that it fires at the later date.
		//
		// 4. If the deleteInterval decreased, then we need to reset the timer so that it fires at an earlier date.
		//    (Plus we might need to do an immediate delete.)
		
		if (deleteInterval > 0.0)
		{
			if (deleteTimer == NULL)
			{
				// Handles #2
				//
				// Since the deleteTimer uses the lastDeleteTime to calculate it's first fireDate,
				// if a delete is needed the timer will fire immediately.
				
				[self createAndStartDeleteTimer];
			}
			else
			{
				// Handles #3
				// Handles #4
				//
				// Since the deleteTimer uses the lastDeleteTime to calculate it's first fireDate,
				// if a save is needed the timer will fire immediately.
				
				[self updateDeleteTimer];
			}
		}
		else if (deleteTimer)
		{
			// Handles #1
			
			[self destroyDeleteTimer];
		}
	}};
	
	if (dispatch_get_specific(storageQueueTag))
		block();
	else
		dispatch_async(storageQueue, block);
}

- (void)pauseOldMessageDeletionForRoom:(XMPPJID *)roomJID
{
	dispatch_block_t block = ^{ @autoreleasepool {
		
		[pausedMessageDeletion addObject:[roomJID bareJID]];
	}};
	
	if (dispatch_get_specific(storageQueueTag))
		block();
	else
		dispatch_async(storageQueue, block);
}

- (void)resumeOldMessageDeletionForRoom:(XMPPJID *)roomJID
{
	dispatch_block_t block = ^{ @autoreleasepool {
		
		[pausedMessageDeletion removeObject:[roomJID bareJID]];
		[self performDelete];
	}};
	
	if (dispatch_get_specific(storageQueueTag))
		block();
	else
		dispatch_async(storageQueue, block);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Overrides
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)didCreateManagedObjectContext
{
	XMPPLogTrace();
	
	//[self clearAllOccupantsFromRoom:nil];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Internal API
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)performDelete
{
	if (maxMessageAge <= 0.0) return;
	
	NSDate *minLocalTimestamp = [NSDate dateWithTimeIntervalSinceNow:(maxMessageAge * -1.0)];
	
	NSManagedObjectContext *moc = [self managedObjectContext];
	NSEntityDescription *messageEntity = [self messageEntity:moc];
	
	NSPredicate *predicate;
	if ([pausedMessageDeletion count] > 0)
	{
		predicate = [NSPredicate predicateWithFormat:@"localTimestamp <= %@ AND NOT roomJIDStr IN %@",
                     minLocalTimestamp, pausedMessageDeletion];
	}
	else
	{
		predicate = [NSPredicate predicateWithFormat:@"localTimestamp <= %@", minLocalTimestamp];
	}
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	[fetchRequest setEntity:messageEntity];
	[fetchRequest setPredicate:predicate];
	[fetchRequest setFetchBatchSize:saveThreshold];
	
	NSError *error = nil;
	NSArray *oldMessages = [moc executeFetchRequest:fetchRequest error:&error];
	
	if (error)
	{
		XMPPLogWarn(@"%@: %@ - fetch error: %@", THIS_FILE, THIS_METHOD, error);
	}
	
	NSUInteger unsavedCount = [self numberOfUnsavedChanges];
	
	for (XMPPRoomMessageCoreDataStorageObject *oldMessage in oldMessages)
	{
		[moc deleteObject:oldMessage];
		
		if (++unsavedCount >= saveThreshold)
		{
			[self save];
			unsavedCount = 0;
		}
	}
	
	lastDeleteTime = dispatch_time(DISPATCH_TIME_NOW, 0);
}

- (void)destroyDeleteTimer
{
	if (deleteTimer)
	{
		dispatch_source_cancel(deleteTimer);
#if !OS_OBJECT_USE_OBJC
		dispatch_release(deleteTimer);
#endif
		deleteTimer = NULL;
	}
}

- (void)updateDeleteTimer
{
	if ((deleteTimer != NULL) && (deleteInterval > 0.0) && (maxMessageAge > 0.0))
	{
		uint64_t interval = deleteInterval * NSEC_PER_SEC;
		dispatch_time_t startTime;
		
		if (lastDeleteTime > 0)
			startTime = dispatch_time(lastDeleteTime, interval);
		else
			startTime = dispatch_time(DISPATCH_TIME_NOW, interval);
		
		dispatch_source_set_timer(deleteTimer, startTime, interval, 1.0);
	}
}

- (void)createAndStartDeleteTimer
{
	if ((deleteTimer == NULL) && (deleteInterval > 0.0) && (maxMessageAge > 0.0))
	{
		deleteTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, storageQueue);
		
		dispatch_source_set_event_handler(deleteTimer, ^{ @autoreleasepool {
			
			[self performDelete];
			
		}});
		
		[self updateDeleteTimer];
		
		if(deleteTimer != NULL)
		{
			dispatch_resume(deleteTimer);
		}
	}
}

- (void)clearAllOccupantsFromRoom:(XMPPJID *)roomJID
{
	XMPPLogTrace();
	AssertPrivateQueue();
	
	NSManagedObjectContext *moc = [self managedObjectContext];
	NSEntityDescription *entity = [self occupantEntity:moc];
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	[fetchRequest setEntity:entity];
	[fetchRequest setFetchBatchSize:saveThreshold];
	
	if (roomJID)
	{
		NSPredicate *predicate;
		predicate = [NSPredicate predicateWithFormat:@"roomJIDStr == %@", [roomJID bare]];
		
		[fetchRequest setPredicate:predicate];
	}
	
	NSArray *allOccupants = [moc executeFetchRequest:fetchRequest error:nil];
	
	NSUInteger unsavedCount = [self numberOfUnsavedChanges];
	
	for (XMPPRoomOccupantCoreDataStorageObject *occupant in allOccupants)
	{
		[moc deleteObject:occupant];
		
		if (++unsavedCount >= saveThreshold)
		{
			[self save];
			unsavedCount = 0;
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//#pragma mark Protected API
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
///**
// * Optional override hook.
//**/
//- (BOOL)existsMessage:(XMPPMessage *)message forRoom:(XMPPRoom *)room stream:(XMPPStream *)xmppStream
//{
//    NSDate *timeStampDate = nil;
//	NSDate *remoteTimestamp = [message delayedDeliveryDate];
//
//	if (remoteTimestamp != nil)
//	{
//		// When the xmpp server sends us a room message, it will always timestamp delayed messages.
//		// For example, when retrieving the discussion history, all messages will include the original timestamp.
//		// If a message doesn't include such timestamp, then we know we're getting it in "real time".
//		timeStampDate = remoteTimestamp;
//	}
//    else
//        timeStampDate = [[NSDate alloc]init];
//
//
//	// Does this message already exist in the database?
//	// How can we tell if two XMPPRoomMessages are the same?
//	//
//	// 1. Same streamBareJidStr
//	// 2. Same jid
//	// 3. Same text
//	// 4. Approximately the same timestamps
//	//
//	// This is actually a rather difficult question.
//	// What if the same user sends the exact same message multiple times?
//	//
//	// If we first received the message while already in the room, it won't contain a remoteTimestamp.
//	// Returning to the room later and downloading the discussion history will return the same message,
//	// this time with a remote timestamp.
//	//
//	// So if the message doesn't have a remoteTimestamp,
//	// but it's localTimestamp is approximately the same as the remoteTimestamp,
//	// then this is enough evidence to consider the messages the same.
//	//
//	// Note: Predicate order matters. Most unique key should be first, least unique should be last.
//
//	NSManagedObjectContext *moc = [self managedObjectContext];
//	NSEntityDescription *messageEntity = [self messageEntity:moc];
//
//	NSString *streamBareJidStr = [[self myJIDForXMPPStream:xmppStream] bare];
//
//	XMPPJID *messageJID = [message from];
//	NSString *messageBody = [[message elementForName:@"body"] stringValue];
//
//	NSDate *minLocalTimestamp = [remoteTimestamp dateByAddingTimeInterval:-3000];
//	NSDate *maxLocalTimestamp = [remoteTimestamp dateByAddingTimeInterval: 3000];
//
//	NSString *predicateFormat = @"    body == %@ "
//	                            @"AND jidStr == %@ "
//	                            @"AND streamBareJidStr == %@ "
//                                @"AND messageID == %@";
//
//
//	NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFormat,
//	                             messageBody, messageJID, streamBareJidStr,[[message attributeForName:@"id"] stringValue]];
//
//	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//	[fetchRequest setEntity:messageEntity];
//	[fetchRequest setPredicate:predicate];
//	[fetchRequest setFetchLimit:1];
//
//	NSError *error = nil;
//	NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
//
//	if (error)
//	{
//		XMPPLogError(@"%@: %@ - Fetch error: %@", THIS_FILE, THIS_METHOD, error);
//	}
//
//	return ([results count] > 0);
//}
-(BOOL)existsMessage:(XMPPMessage *)message forRoom:(XMPPRoom *)room stream:(XMPPStream *)xmppStream
{
    NSManagedObjectContext *moc = [self managedObjectContext];
	NSEntityDescription *messageEntity = [self messageEntity:moc];
	NSString *messageBody = [[message elementForName:@"body"] stringValue];
    NSString *predicateFormat = @"body == %@ "
    @"AND messageID == %@"
    @"AND roomJIDStr == %@";
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFormat,
                              messageBody,[[message attributeForName:@"id"] stringValue],room.roomJID.bare];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	[fetchRequest setEntity:messageEntity];
	[fetchRequest setPredicate:predicate];
	[fetchRequest setFetchLimit:1];
    
	NSError *error = nil;
	NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
	
	if (error)
	{
		XMPPLogError(@"%@: %@ - Fetch error: %@", THIS_FILE, THIS_METHOD, error);
	}
	return ([results count] > 0);
}

/**
 * Optional override hook for general extensions.
 *
 * @see insertMessage:outgoing:forRoom:stream:
 **/
- (void)didInsertMessage:(XMPPRoomMessageCoreDataStorageObject *)message
{
	// Override me if you're extending the XMPPRoomMessageCoreDataStorageObject class to add additional properties.
	// You can update your additional properties here.
	//
	// At this point the standard properties have already been set.
	// So you can, for example, access the XMPPMessage via message.message.
}

/**
 * Optional override hook for complete customization.
 * Override me if you need to do specific custom work when inserting a message in a room.
 *
 * @see didInsertMessage:
 **/
- (void)insertMessage:(XMPPMessage *)message
             outgoing:(BOOL)isOutgoing
              forRoom:(XMPPRoom *)room
               stream:(XMPPStream *)xmppStream
{
	// Extract needed information
	
	XMPPJID *roomJID = room.roomJID;
	XMPPJID *messageJID = isOutgoing ? room.myRoomJID : [message from];
	
	NSDate *localTimestamp;
	NSDate *remoteTimestamp;
	
	if (isOutgoing)
	{
		localTimestamp = [[NSDate alloc] init];
		remoteTimestamp = nil;
	}
	else
	{
		remoteTimestamp = [message delayedDeliveryDate];
		if (remoteTimestamp) {
			localTimestamp = remoteTimestamp;
		}
		else {
			localTimestamp = [[NSDate alloc] init];
		}
	}
    
   	
	NSString *messageBody = [[message elementForName:@"body"] stringValue];
    
	NSManagedObjectContext *moc = [self managedObjectContext];
	NSString *streamBareJidStr = [[self myJIDForXMPPStream:xmppStream] bare];
	
	NSEntityDescription *messageEntity = [self messageEntity:moc];
	
	// Add to database
	XMPPRoomMessageCoreDataStorageObject *roomMessage = (XMPPRoomMessageCoreDataStorageObject *)
    [[NSManagedObject alloc] initWithEntity:messageEntity insertIntoManagedObjectContext:nil];
	
	roomMessage.message = message;
	roomMessage.roomJID = roomJID;
	roomMessage.jid = messageJID;
	roomMessage.nickname = [messageJID resource];
    if (messageBody != nil) {
        roomMessage.body = messageBody;
    }
    else if([message isGroupChatMessageWithSubject])
    {
        roomMessage.subject = [message subject];
        [self updateSubjectInMessagedetails:message];
        
    }
	roomMessage.localTimestamp = localTimestamp;
	roomMessage.remoteTimestamp = remoteTimestamp;
	roomMessage.isFromMe = isOutgoing;
	roomMessage.streamBareJidStr = streamBareJidStr;
    roomMessage.messageID = [[message attributeForName:@"id"] stringValue];
	
	[moc insertObject:roomMessage];
    if (!isOutgoing && message.from.resource != nil) {
        [self groupunreadedMsg:roomJID clearValue:NO];
    }
    // Hook if subclassing XMPPRoomMessageCoreDataStorageObject (awakeFromInsert)
	[self didInsertMessage:roomMessage]; // Hook if subclassing XMPPRoomCoreDataStorage
}

/**
 * Optional override hook for general extensions.
 *
 * @see insertOccupantWithPresence:room:stream:
 **/
- (void)didInsertOccupant:(XMPPRoomOccupantCoreDataStorageObject *)occupant
{
	// Override me if you're extending the XMPPRoomOccupantCoreDataStorageObject class to add additional properties.
	// You can update your additional properties here.
	//
	// At this point the standard XMPPRoomOccupantCDSO properties have already been set.
	// So you can, for example, access the XMPPPresence via occupant.presence.
}

/**
 * Optional override hook for general extensions.
 *
 * @see updateOccupant:withPresence:room:stream:
 **/
- (void)didUpdateOccupant:(XMPPRoomOccupantCoreDataStorageObject *)occupant
{
	// Override me if you're extending the XMPPRoomOccupantCoreDataStorageObject class to add additional properties.
	// You can update your additional properties here.
	//
	// At this point the standard XMPPRoomOccupantCDSO properties have already been updated.
	// So you can, for example, access the XMPPPresence via occupant.presence.
}

/**
 * Optional override hook for complete customization.
 * Override me if you need to do specific custom work when inserting an occupant in a room.
 *
 * @see didInsertOccupant:
 **/
- (void)insertOccupantWithPresence:(XMPPPresence *)presence
                              room:(XMPPRoom *)room
                            stream:(XMPPStream *)xmppStream
{
	// Extract needed information
	
	XMPPJID *roomJID = room.roomJID;
	XMPPJID *presenceJID = [presence from];
	
	NSString *role = nil;
	NSString *affiliation = nil;
	XMPPJID *realJID = nil;
	
	NSXMLElement *x = [presence elementForName:@"x" xmlns:@"http://jabber.org/protocol/muc#user"];
	NSXMLElement *item = [x elementForName:@"item"];
	if (item)
	{
		role = [[item attributeStringValueForName:@"role"] lowercaseString];
		affiliation = [[item attributeStringValueForName:@"affiliation"] lowercaseString];
		
		NSString *realJIDStr = [item attributeStringValueForName:@"jid"];
		if (realJIDStr)
		{
			realJID = [XMPPJID jidWithString:realJIDStr];
		}
	}
	
	// Add to database
	
	NSManagedObjectContext *moc = [self managedObjectContext];
	NSString *streamBareJidStr = [[self myJIDForXMPPStream:xmppStream] bare];
	
	NSEntityDescription *occupantEntity = [self occupantEntity:moc];
	
	XMPPRoomOccupantCoreDataStorageObject *occupant = (XMPPRoomOccupantCoreDataStorageObject *)
    [[NSManagedObject alloc] initWithEntity:occupantEntity insertIntoManagedObjectContext:nil];
	
	occupant.presence = presence;
	occupant.roomJID = roomJID;
	occupant.jid = presenceJID;
	occupant.nickname = [presenceJID resource];
	occupant.role = role;
	occupant.affiliation = affiliation;
	occupant.realJID = realJID.bareJID;
    occupant.realJIDStr = realJID.bare;
	occupant.createdAt = [NSDate date];
	occupant.streamBareJidStr = streamBareJidStr;
    NSString *userStr = [self getDisplayName:[presenceJID resource]];
    if (userStr == nil) {
        userStr = [[presenceJID resource] stringByReplacingOccurrencesOfString:domainName withString:@""];
    }
    if ([[presence type] isEqualToString:@"unavailable"]) {
        occupant.presencetype = @2;
        occupant.presencetext  = [NSString stringWithFormat:@"%@ has left",userStr];
    }
    else
    {
        occupant.presencetype = @1;

        if (userStr == xmppStream.myJID.bare)
            occupant.presencetext = [NSString stringWithFormat:@"You has joined"];
        else
            occupant.presencetext = [NSString stringWithFormat:@"%@ has joined",userStr];
        
    }
	[moc insertObject:occupant];       // Hook if subclassing XMPPRoomOccupantCoreDataStorageObject (awakeFromInsert)
	[self didInsertOccupant:occupant]; // Hook if subclassing XMPPRoomCoreDataStorage
}

/**
 * Optional override hook for complete customization.
 * Override me if you need to do specific custom work when updating an occupant in a room.
 *
 * @see didUpdateOccupant:
 **/
- (void)updateOccupant:(XMPPRoomOccupantCoreDataStorageObject *)occupant
          withPresence:(XMPPPresence *)presence
                  room:(XMPPRoom *)room
                stream:(XMPPStream *)stream
{
	// Extract needed information
	
	NSString *role = nil;
	NSString *affiliation = nil;
	XMPPJID *realJID = nil;
	
	NSXMLElement *x = [presence elementForName:@"x" xmlns:@"http://jabber.org/protocol/muc#user"];
	NSXMLElement *item = [x elementForName:@"item"];
	if (item)
	{
		role = [[item attributeStringValueForName:@"role"] lowercaseString];
		affiliation = [[item attributeStringValueForName:@"affiliation"] lowercaseString];
		
		NSString *realJIDStr = [item attributeStringValueForName:@"jid"];
		if (realJIDStr)
		{
			realJID = [XMPPJID jidWithString:realJIDStr];
		}
	}
	
	// Update database
	
	occupant.presence = presence;
	occupant.role = role;
	occupant.affiliation = affiliation;
	occupant.realJID = realJID.bareJID;
    [self didUpdateOccupant:occupant]; // Hook if subclassing XMPPRoomCoreDataStorage
}

-(void)removeOccupantWithMessage:(XMPPMessage *)message
{
    [self executeBlock:^{
        
        NSString *predicateFrmt = @"roomJIDStr == %@ AND nickname == %@";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, [message from].bare,[message from].resource];
        NSManagedObjectContext *moc = [self managedObjectContext];
        NSEntityDescription *entityDescription = [NSEntityDescription entityForName:self.occupantEntityName
                                                             inManagedObjectContext:moc];
        NSFetchRequest *request = [[NSFetchRequest alloc]init];
        request.predicate = predicate;
        [request setEntity:entityDescription];
        
        // Now it should yield an NSArray of distinct values in dictionaries.
        NSArray *detailArray = [moc executeFetchRequest:request error:nil];
        if (detailArray.count == 1) {
            
            XMPPRoomOccupantCoreDataStorageObject *occupant = (XMPPRoomOccupantCoreDataStorageObject *)detailArray[0];
            [[occupant managedObjectContext] deleteObject:occupant];
            
        }
        [moc save:nil];
    }];
}

/**
 * Optional override hook.
 **/
- (void)removeOccupant:(XMPPRoomOccupantCoreDataStorageObject *)occupant
          withPresence:(XMPPPresence *)presence
                  room:(XMPPRoom *)room
                stream:(XMPPStream *)stream
{
	// Delete from database
	
	//[[occupant managedObjectContext] deleteObject:occupant];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Public API
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSEntityDescription *)messageEntity:(NSManagedObjectContext *)moc
{
	// This method should be thread-safe.
	// So be sure to access the entity name through the property accessor.
	
	return [NSEntityDescription entityForName:[self messageEntityName] inManagedObjectContext:moc];
}

- (NSEntityDescription *)occupantEntity:(NSManagedObjectContext *)moc
{
	// This method should be thread-safe.
	// So be sure to access the entity name through the property accessor.
	
	return [NSEntityDescription entityForName:[self occupantEntityName] inManagedObjectContext:moc];
}
- (NSEntityDescription *)messageDetailsEntity:(NSManagedObjectContext *)moc
{
	// This method should be thread-safe.
	// So be sure to access the entity name through the property accessor.
	
	return [NSEntityDescription entityForName:[self messageDetailsEntityName] inManagedObjectContext:moc];
}

- (NSDate *)mostRecentMessageTimestampForRoom:(XMPPJID *)roomJID
                                       stream:(XMPPStream *)xmppStream
                                    inContext:(NSManagedObjectContext *)inMoc
{
	if (roomJID == nil) return nil;
	
	// It's possible to use our internal managedObjectContext only because we're not returning a NSManagedObject.
	
	__block NSDate *result = nil;
	
	dispatch_block_t block = ^{ @autoreleasepool {
		
		NSManagedObjectContext *moc = inMoc ? inMoc : [self managedObjectContext];
		
		NSEntityDescription *entity = [self messageEntity:moc];
		
		NSPredicate *predicate;
		if (xmppStream)
		{
			NSString *streamBareJidStr = [[self myJIDForXMPPStream:xmppStream] bare];
			
			NSString *predicateFormat = @"roomJIDStr == %@ AND streamBareJidStr == %@";
			predicate = [NSPredicate predicateWithFormat:predicateFormat, roomJID, streamBareJidStr];
		}
		else
		{
			predicate = [NSPredicate predicateWithFormat:@"roomJIDStr == %@", roomJID];
		}
		NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
		[fetchRequest setEntity:entity];
		[fetchRequest setPredicate:predicate];
		[fetchRequest setSortDescriptors:[NSSortDescriptor sortValues:@"localTimestamp" ascending:NO]];
		[fetchRequest setFetchLimit:1];
		
		NSError *error = nil;
		XMPPRoomMessageCoreDataStorageObject *message = [[moc executeFetchRequest:fetchRequest error:&error] lastObject];
        
		if (error)
		{
			XMPPLogError(@"%@: %@ - fetchRequest error: %@", THIS_FILE, THIS_METHOD, error);
		}
		else
		{
			result = [message.localTimestamp copy];
		}
	}};
	
	if (inMoc == nil)
		dispatch_sync(storageQueue, block);
	else
		block();
	
	return result;
}

- (XMPPRoomOccupantCoreDataStorageObject *)occupantForJID:(XMPPJID *)jid
                                                   stream:(XMPPStream *)xmppStream
                                                inContext:(NSManagedObjectContext *)moc
{
	if (jid == nil) return nil;
	if (moc == nil) return nil;
	
	NSEntityDescription *entity = [self occupantEntity:moc];
	
	NSPredicate *predicate;
	if (xmppStream)
	{
		NSString *streamBareJidStr = [[self myJIDForXMPPStream:xmppStream] bare];
		
		NSString *predicateFormat = @"jidStr == %@ AND streamBareJidStr == %@";
		predicate = [NSPredicate predicateWithFormat:predicateFormat, jid, streamBareJidStr];
	}
	else
	{
		predicate = [NSPredicate predicateWithFormat:@"jidStr == %@", jid];
	}
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	[fetchRequest setEntity:entity];
	[fetchRequest setPredicate:predicate];
	[fetchRequest setFetchBatchSize:1];
	
	NSError *error = nil;
	NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
	
	if (error)
	{
		XMPPLogWarn(@"%@: %@ - fetch error: %@", THIS_FILE, THIS_METHOD, error);
	}
	
	return (XMPPRoomOccupantCoreDataStorageObject *)[results lastObject];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark XMPPRoomStorage Protocol
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)handlePresence:(XMPPPresence *)presence room:(XMPPRoom *)room
{
	XMPPLogTrace();
	
	
	XMPPJID *presenceJID = [presence from];
	XMPPStream *xmppStream = room.xmppStream;
	
	[self executeBlock:^{
		
		NSManagedObjectContext *moc = [self managedObjectContext];
		
		// Is occupant already in database?
		
		XMPPRoomOccupantCoreDataStorageObject *occupant =
        [self occupantForJID:presenceJID stream:xmppStream inContext:moc];
		
		// Is occupant available or unavailable?
		
		if ([[presence type] isEqualToString:@"unavailable"])
		{
			// Remove occupant record from database
			
			if ([occupant.presencetype  isEqual: @1])
			{
				[self insertOccupantWithPresence:presence room:room stream:xmppStream];
			}
		}
		else
		{
			// Insert or update occupant in database
			
			if ([occupant.presencetype  isEqual: @1])
			{
				[self updateOccupant:occupant withPresence:presence room:room stream:xmppStream];
			}
			else
			{
				[self insertOccupantWithPresence:presence room:room stream:xmppStream];
			}
		}
	}];
}

- (void)handleOutgoingMessage:(XMPPMessage *)message room:(XMPPRoom *)room
{
	XMPPLogTrace();
	
	XMPPStream *xmppStream = room.xmppStream;
	
	[self scheduleBlock:^{
        
        if ([self existsMessage:message forRoom:room stream:xmppStream])
			XMPPLogVerbose(@"%@: %@ - Duplicate message", THIS_FILE, THIS_METHOD);
        
		else
            [self insertMessage:message outgoing:YES forRoom:room stream:xmppStream];
	}];
}

- (void)handleIncomingMessage:(XMPPMessage *)message room:(XMPPRoom *)room
{
	XMPPLogTrace();
	
	XMPPJID *myRoomJID = room.myRoomJID;
	XMPPJID *messageJID = [message from];
	
	if ([myRoomJID isEqualToJID:messageJID])
	{
		if (![message wasDelayed])
		{
			// Ignore - we already stored message in handleOutgoingMessage:room:
			return;
		}
	}
	
	XMPPStream *xmppStream = room.xmppStream;
	
	[self scheduleBlock:^{
		
		if ([self existsMessage:message forRoom:room stream:xmppStream])
		{
			XMPPLogVerbose(@"%@: %@ - Duplicate message", THIS_FILE, THIS_METHOD);
		}
		else
		{
			[self insertMessage:message outgoing:NO forRoom:room stream:xmppStream];
            
            BOOL results = [self getroomJID:[[message from] bare]];
            if (!results) {
                [self insertMessageDetails:[message from].bare];
            }
            else if([message isGroupChatMessageWithSubject] && results)
            {
                [self updateSubjectInMessagedetails:message];
            }
		}
	}];
}
-(void)insertRoomDetails:(XMPPMessage *)message room:(XMPPRoom *)room
{
    [self scheduleBlock:^{
        
        BOOL results = [self getroomJID:[[message from] bare]];
        if (!results) {
            [self insertSubject:message];
        }
        else if([message isGroupChatMessageWithSubject] && results)
        {
            [self updateSubjectInMessagedetails:message];
        }
        
	}];
}

-(BOOL)getroomJID:(NSString *)fromStr
{
    NSManagedObjectContext *moc = [self managedObjectContext];
	NSEntityDescription *messageEntity = [self messageDetailsEntity:moc];
    NSString *predicateFormat = @"roomJIDStr == %@";
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFormat,
                              fromStr];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	[fetchRequest setEntity:messageEntity];
	[fetchRequest setPredicate:predicate];
	[fetchRequest setFetchLimit:1];
    
	NSError *error = nil;
	NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
	
	if (error)
	{
		XMPPLogError(@"%@: %@ - Fetch error: %@", THIS_FILE, THIS_METHOD, error);
	}
    
	return ([results count] > 0);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Method to update unreaded message for user
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)groupunreadedMsg:(XMPPJID *)user clearValue:(BOOL)clear
{
    if (clear == YES)
    {
        [self scheduleBlock:^{
            NSManagedObjectContext *moc = [self managedObjectContext];
            NSEntityDescription *messageEntity = [self messageDetailsEntity:moc];
            
            NSFetchRequest *request = [[NSFetchRequest alloc] init];
            [request setEntity:messageEntity];
            NSString *predicateFrmt = @"roomJIDStr = %@";
            NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, [user bareJID]];
            [request setPredicate:predicate];
            NSArray *empArray=[moc executeFetchRequest:request error:nil];
            if ([empArray count] == 1){
                for (XMPPRoomMessageDetails *user in  empArray){
                    if (clear)
                        user.unreadMessages = [NSNumber numberWithInt:0];
                }
            }
            else
            {
              //  NSLog(@"ERROR IN SAVING GROUP BADGE COUNT");
            }
            [[self managedObjectContext] save:nil];
            
        }];
    }
    else
    {
        NSManagedObjectContext *moc = [self managedObjectContext];
    	NSEntityDescription *messageEntity = [self messageDetailsEntity:moc];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:messageEntity];
        NSString *predicateFrmt = @"roomJIDStr = %@";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, [user bareJID]];
        [request setPredicate:predicate];
        NSError *erro = nil;
        NSArray *arrayVales=[moc executeFetchRequest:request error:&erro];
        
        if ([arrayVales count] == 1)
        {
            for (XMPPRoomMessageDetails *user in  arrayVales)
            {
                NSNumber *number = user.unreadMessages;
                int value = [number intValue];
                number = [NSNumber numberWithInt:value + 1];
                user.unreadMessages = number;
            }
        }
        else
        {
          //  NSLog(@"ERROR IN SAVING GROUP BADGE COUNT");
        }
        [[self managedObjectContext] save:nil];
        
    }
}

-(void)insertMessageDetails:(NSString *)fromString
{
    NSManagedObjectContext *moc = [self managedObjectContext];
	NSEntityDescription *occupantEntity = [self messageDetailsEntity:moc];
	XMPPRoomMessageDetails *details = (XMPPRoomMessageDetails *)
    [[NSManagedObject alloc] initWithEntity:occupantEntity insertIntoManagedObjectContext:nil];
    details.roomJIDStr = fromString;
	[moc insertObject:details];
}
-(void)insertSubject:(XMPPMessage *)message
{
    NSManagedObjectContext *moc = [self managedObjectContext];
	NSEntityDescription *occupantEntity = [self messageDetailsEntity:moc];
	XMPPRoomMessageDetails *details = (XMPPRoomMessageDetails *)
    [[NSManagedObject alloc] initWithEntity:occupantEntity insertIntoManagedObjectContext:nil];
    details.roomJIDStr = [[message from] bare];
    details.subject = [message subject];
	[moc insertObject:details];
    [moc save:nil];
    
}
-(void)updateSubjectInMessagedetails:(XMPPMessage *)message
{
    if ([message isGroupChatMessageWithSubject]) {
        BOOL result = [self getroomJID:[[message from] bare]];
        if (!result) {
            [self insertSubject:message];
        }
        else
        {
            NSManagedObjectContext *moc = [self managedObjectContext];
            NSEntityDescription *messageEntity = [self messageDetailsEntity:moc];
            NSString *predicateFormat = @"roomJIDStr == %@";
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFormat,
                                      [message from].bare];
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            [fetchRequest setEntity:messageEntity];
            [fetchRequest setPredicate:predicate];
            [fetchRequest setFetchLimit:1];
            
            NSError *error = nil;
            NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
            if (results.count) {
                XMPPRoomMessageDetails *details = results[0];
                details.subject = [message subject];
                [moc save:nil];
            }
        }
    }
    
}
- (void)handleDidLeaveRoom:(XMPPRoom *)room
{
	XMPPLogTrace();
	
	XMPPJID *roomJID = room.roomJID;
	
	[self scheduleBlock:^{
		
		[self clearAllOccupantsFromRoom:roomJID];
	}];
}

-(NSString *)getDisplayName:(NSString *)resourceStr
{
    if ([resourceStr rangeOfString:domainName].location==NSNotFound )
        resourceStr = [resourceStr stringByAppendingString:domainName];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:[self managedObjectContext_roster]];
    [request setEntity:entity];
    NSString *predicateFrmt = @"jidStr = %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, resourceStr];
    [request setPredicate:predicate];
    NSArray *empArray=[[self managedObjectContext_roster] executeFetchRequest:request error:nil];
    if ([empArray count] == 1)
    {
        for (XMPPUserCoreDataStorageObject *user in  empArray)
        {
            return user.displayName;
        }
    }
    return nil;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Core Data
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (NSManagedObjectContext *)managedObjectContext_roster
{
	return [[[self appDelegate] xmppRosterStorage] mainThreadManagedObjectContext];
}- (AppDelegate *)appDelegate
{
	return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
@end
