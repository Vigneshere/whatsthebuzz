//
//  XMPPRoomMessageDetails.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 25/03/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface XMPPRoomMessageDetails : NSManagedObject

@property (nonatomic, retain) NSString * roomJIDStr;
@property (nonatomic, retain) NSNumber * unreadMessages;
@property(nonatomic,strong)NSString *subject;


@end
