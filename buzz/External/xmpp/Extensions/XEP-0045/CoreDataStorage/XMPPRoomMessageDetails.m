//
//  XMPPRoomMessageDetails.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 25/03/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "XMPPRoomMessageDetails.h"


@implementation XMPPRoomMessageDetails

@dynamic roomJIDStr;
@dynamic unreadMessages;
@dynamic subject;

@end
