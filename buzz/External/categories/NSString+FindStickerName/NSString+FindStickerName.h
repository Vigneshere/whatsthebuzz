//
//  NSString+FindStickerName.h
//  buzz
//
//  Created by Shriram Rajendran on 07/05/15.
//  Copyright (c) 2015 Shailisolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (FindStickerName)
+(NSString *)findStickerName:(NSString*)StrickerName;

@end
