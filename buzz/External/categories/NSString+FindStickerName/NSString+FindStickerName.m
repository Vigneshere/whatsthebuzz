//
//  NSString+FindStickerName.m
//  buzz
//
//  Created by Shriram Rajendran on 07/05/15.
//  Copyright (c) 2015 Shailisolutions. All rights reserved.
//

#import "NSString+FindStickerName.h"

@implementation NSString (FindStickerName)

+(NSString *)findStickerName:(NSString*)StrickerName
{
    NSString *OriginalStringName;
    
    if([StrickerName isEqualToString: @"sticker_01"])
    {
        OriginalStringName = @"Pow!";
    }
    else if([StrickerName isEqualToString: @"sticker_02"])
    {
        OriginalStringName = @"Heart";
    }
    else if([StrickerName isEqualToString: @"sticker_03"])
    {
        OriginalStringName = @"Awesome";
    }
    else if([StrickerName isEqualToString: @"sticker_04"])
    {
        OriginalStringName = @"Grrrr!";
    }
    else if([StrickerName isEqualToString: @"sticker_05"])
    {
        OriginalStringName = @"Yay!";
    }
    else
    {
        OriginalStringName = @"";
        
    }
    return OriginalStringName;
}

@end
