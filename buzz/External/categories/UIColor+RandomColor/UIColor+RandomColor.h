//
//  UIColor+RandomColor.h
//  buzz
//
//  Created by Shriram Rajendran on 07/05/15.
//  Copyright (c) 2015 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (RandomColor)

+(UIColor *)getRandomRGB;

+(UIColor *)colorFromString:(NSString *)stringValue;
+ (UIColor*) getDominantColor:(UIImage*)image;



@end
