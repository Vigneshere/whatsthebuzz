//
//  ACStatusBarOverlayWindow.h
//  buzz
//
//  Created by shaili_solutions_MacPro3 on 04/08/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACStatusBarOverlayWindow : UIWindow
{
      int HEIGHT;
}

+ (ACStatusBarOverlayWindow *)sharedInstance;

- (void)initWithNoti_jidStr:(NSString*)jidStr NameLbl:(NSString*)nameLbl MsgLbl:(NSString*)msgLbl showClose:(BOOL)showClose;
- (void)initWithNoti_jidStr:(NSString*)jidStr NameLbl:(NSString*)nameLbl MsgLbl:(NSString*)msgLbl showClose:(BOOL)showClose showImp:(BOOL)showImp;

@end
