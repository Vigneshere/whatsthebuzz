//
//  PrivateSection.h
//  buzz
//
//  Created by shaili_solutions_MacPro3 on 11/06/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PrivateSection : NSManagedObject

@property (nonatomic, retain) NSString * iD;
@property (nonatomic) BOOL is_Sender;
@property (nonatomic, retain) NSString * status;
@property (nonatomic) double timer;
@property (nonatomic) NSDate* time_updated;

@end
