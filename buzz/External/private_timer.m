//
//  private_timer.m
//  buzz
//
//  Created by shaili_solutions_MacPro3 on 13/06/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "private_timer.h"
#import "privateChatDbEditer.h"
#import "chatViewController.h"

@implementation private_timer

@synthesize delegate;

- (void)setDelegate:(id <privateTimerDelegate>)aDelegate {
    if (delegate != aDelegate) {
        delegate = aDelegate;
        
       
    }
}
-(void)createTimer_id:(NSString*)user_id seconds:(float)seconds
{
    
    NSMutableDictionary* privateDict = [NSMutableDictionary new];
    [privateDict setObject:user_id forKey:@"user_id"];
    
    self.singleton = [Singleton sharedMySingleton];

    if ([self.singleton.privateReqIndicatorDict objectForKey:user_id])
    {
        NSTimer* timer = [self.singleton.privateReqIndicatorDict objectForKey:user_id];
        [timer invalidate];
        [self.singleton.privateReqIndicatorDict removeObjectForKey:user_id];
    }
    
   NSTimer* timer = [NSTimer scheduledTimerWithTimeInterval:seconds target:self selector:@selector(timerDead:) userInfo:privateDict repeats:YES];
        [self.singleton.privateReqIndicatorDict setObject:timer forKey:user_id];
    
}

- (void)timerDead:(NSTimer *)timer {
    NSMutableDictionary *dict = [timer userInfo];
    

    if (dict) {
       
            privateChatDbEditer* privateSec = [privateChatDbEditer new];
            [privateSec deletePrivateChat:[dict objectForKey:@"user_id"]];
            [timer invalidate];
            
        [[NSNotificationCenter defaultCenter] postNotificationName:@"privateTimer" object:nil userInfo:dict];

        
    }
  
}
@end
