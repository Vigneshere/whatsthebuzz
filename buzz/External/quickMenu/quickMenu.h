//
//  quickMenu.h
//  drag
//
//  Created by shaili_solutions_MacPro3 on 22/07/14.
//  Copyright (c) 2014 JemsNets. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface quickMenu : UIView

@property (nonatomic,strong) NSString *fromVC;
- (id)initFrame:(UIView*)onView topPoint:(int)top bottomPoint:(int)bottom TypeOfMenu:(NSString *)type;
@end
