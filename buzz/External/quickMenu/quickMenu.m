//
//  quickMenu.m
//  drag
//
//  Created by shaili_solutions_MacPro3 on 22/07/14.
//  Copyright (c) 2014 JemsNets. All rights reserved.
//

#import "quickMenu.h"

#define ARROWHEIGHT 12
#define WIDTH 240
#define HEIGHT 60
#define XPoint 45

@implementation quickMenu



- (id)initFrame:(UIView*)onView topPoint:(int)top bottomPoint:(int)bottom TypeOfMenu:(NSString *)type
{
    self = [super initWithFrame:CGRectMake(0, 0, onView.frame.size.width, onView.frame.size.height)];
    if (self) {
        
        // Initialization code
        UIImageView* quickMenu;
        if (onView.frame.size.height - bottom >= HEIGHT+ARROWHEIGHT)
        {
         quickMenu =   [self createQuickMenu_Down:YES bottomPoint:bottom topPoint:top TypeOfMenu:type];

        }
        else
        {
          quickMenu =  [self createQuickMenu_Down:NO bottomPoint:bottom topPoint:top TypeOfMenu:type];


        }
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOuterVw:)];
        [self addGestureRecognizer:tapGesture];

        [UIView transitionWithView:quickMenu
                          duration:0.3f
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^(void) {
                            [self addSubview:quickMenu];
                            
                        } completion:NULL];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HideQuickMenu:) name:@"HideQuickMenu" object:nil];

    return self;
}

-(UIImageView*)createQuickMenu_Down:(BOOL)isDown bottomPoint:(int)bottom topPoint:(int)top TypeOfMenu:(NSString *)type
{
    UIImage *bubbleImg;
    UIView* BtnBG_Vw = [UIView new];
    
    if (isDown) {
        bubbleImg = [UIImage imageNamed:@"QuickBubbleBtm"];

    }
    else
    {
        bubbleImg = [UIImage imageNamed:@"QuickBubbleTop"];
 
    }
    
    bubbleImg = [self tintImage:bubbleImg withColor:RGBA(0, 0, 0, 0.8)];
    bubbleImg =     [bubbleImg resizableImageWithCapInsets:UIEdgeInsetsMake(17, 27, 27, 27) resizingMode:UIImageResizingModeStretch];
    
    if ([type isEqualToString:@"singleChat"])
    {
        
    
    UIImageView *bubbleImgView = [[UIImageView alloc] initWithImage:bubbleImg];
    bubbleImgView.userInteractionEnabled = YES;
    if (isDown)
    {
        bubbleImgView.frame = CGRectMake(XPoint,bottom , 231, bubbleImg.size.height);
        BtnBG_Vw.frame = CGRectMake(22, 17, bubbleImgView.frame.size.width - 40, 47);

    }
    else
    {
        bubbleImgView.frame = CGRectMake(XPoint,top -bubbleImg.size.height , 231, bubbleImg.size.height);
        BtnBG_Vw.frame = CGRectMake(22,9, bubbleImgView.frame.size.width - 40, 47);

    }
    bubbleImgView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    //Add quick btns bg vw


   BtnBG_Vw.backgroundColor = [UIColor clearColor];
   [bubbleImgView addSubview:BtnBG_Vw];

    //Adding buttons
    UIImage *callButtonImage = [UIImage imageNamed:@"call"];
    UIButton* callButton = [UIButton buttonWithType:UIButtonTypeCustom];
    callButton.frame = CGRectMake((((BtnBG_Vw.frame.size.width/3) - callButtonImage.size.width)/2), (BtnBG_Vw.frame.size.height - callButtonImage.size.height)/2, callButtonImage.size.width, callButtonImage.size.height);
    [callButton setImage:callButtonImage forState:UIControlStateNormal];
    callButton.tag = 1;
    [callButton addTarget:self action:@selector(hideBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [BtnBG_Vw addSubview:callButton];
    
    UIView* line1 = [UIView new];
    line1.frame = CGRectMake((BtnBG_Vw.frame.size.width/3)-1, 7, 1, BtnBG_Vw.frame.size.height - 14);
    line1.backgroundColor = [UIColor whiteColor];
    [BtnBG_Vw addSubview:line1];
    
    UIImage *msgButtonImage = [UIImage imageNamed:@"msg"];
    UIButton* msgButton = [UIButton buttonWithType:UIButtonTypeCustom];
    msgButton.frame = CGRectMake((((BtnBG_Vw.frame.size.width/3) - msgButtonImage.size.width)/2)+(1*(BtnBG_Vw.frame.size.width/3)), (BtnBG_Vw.frame.size.height - msgButtonImage.size.height)/2, msgButtonImage.size.width, msgButtonImage.size.height);
    [msgButton setImage:msgButtonImage forState:UIControlStateNormal];
    msgButton.tag = 2;
    [msgButton addTarget:self action:@selector(hideBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [BtnBG_Vw addSubview:msgButton];
    
    UIView* line2 = [UIView new];
    line2.frame = CGRectMake((2*(BtnBG_Vw.frame.size.width/3))-1, 7, 1, BtnBG_Vw.frame.size.height - 14);
    line2.backgroundColor = [UIColor whiteColor];
    [BtnBG_Vw addSubview:line2];
    
    UIImage *profButtonImage = [UIImage imageNamed:@"profil"];
    UIButton* profButton = [UIButton buttonWithType:UIButtonTypeCustom];
    profButton.frame = CGRectMake((((BtnBG_Vw.frame.size.width/3) - profButtonImage.size.width)/2)+(2*(BtnBG_Vw.frame.size.width/3)), (BtnBG_Vw.frame.size.height - profButtonImage.size.height)/2, profButtonImage.size.width, profButtonImage.size.height);
    [profButton setImage:profButtonImage forState:UIControlStateNormal];
    profButton.tag = 3;
    [profButton addTarget:self action:@selector(hideBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [BtnBG_Vw addSubview:profButton];
    
    
    return bubbleImgView;
    }
    else
    {
        UIImageView *bubbleImgView = [[UIImageView alloc] initWithImage:bubbleImg];
        bubbleImgView.userInteractionEnabled = YES;
        if (isDown)
        {
            bubbleImgView.frame = CGRectMake(XPoint,bottom , 231, bubbleImg.size.height);
            BtnBG_Vw.frame = CGRectMake(22, 17, bubbleImgView.frame.size.width - 40, 47);
            
        }
        else
        {
            bubbleImgView.frame = CGRectMake(XPoint,top -bubbleImg.size.height , 231, bubbleImg.size.height);
            BtnBG_Vw.frame = CGRectMake(22,9, bubbleImgView.frame.size.width - 40, 47);
            
        }
        bubbleImgView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        
        //Add quick btns bg vw
        
        
        BtnBG_Vw.backgroundColor = [UIColor clearColor];
        [bubbleImgView addSubview:BtnBG_Vw];
        
        //Adding buttons
//        UIImage *callButtonImage = [UIImage imageNamed:@"call"];
//        UIButton* callButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        callButton.frame = CGRectMake((((BtnBG_Vw.frame.size.width/3) - callButtonImage.size.width)/2), (BtnBG_Vw.frame.size.height - callButtonImage.size.height)/2, callButtonImage.size.width, callButtonImage.size.height);
//        [callButton setImage:callButtonImage forState:UIControlStateNormal];
//        callButton.tag = 1;
//        [callButton addTarget:self action:@selector(hideBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
//        [BtnBG_Vw addSubview:callButton];
//        
//        UIView* line1 = [UIView new];
//        line1.frame = CGRectMake((BtnBG_Vw.frame.size.width/3)-1, 7, 1, BtnBG_Vw.frame.size.height - 14);
//        line1.backgroundColor = [UIColor whiteColor];
//        [BtnBG_Vw addSubview:line1];
        
        UIImage *msgButtonImage = [UIImage imageNamed:@"msg"];
        UIButton* msgButton = [UIButton buttonWithType:UIButtonTypeCustom];
        msgButton.frame = CGRectMake((((BtnBG_Vw.frame.size.width/2) - msgButtonImage.size.width)/2), (BtnBG_Vw.frame.size.height - msgButtonImage.size.height)/2, msgButtonImage.size.width, msgButtonImage.size.height);
        [msgButton setImage:msgButtonImage forState:UIControlStateNormal];
        msgButton.tag = 2;
        [msgButton addTarget:self action:@selector(hideBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [BtnBG_Vw addSubview:msgButton];
        
        UIView* line2 = [UIView new];
        line2.frame = CGRectMake((BtnBG_Vw.frame.size.width/2), 7, 1, BtnBG_Vw.frame.size.height - 14);
        line2.backgroundColor = [UIColor whiteColor];
        [BtnBG_Vw addSubview:line2];
        
        UIImage *profButtonImage = [UIImage imageNamed:@"profil"];
        UIButton* profButton = [UIButton buttonWithType:UIButtonTypeCustom];
        profButton.frame = CGRectMake((((BtnBG_Vw.frame.size.width/2) - profButtonImage.size.width)/2) + line2.frame.origin.x, (BtnBG_Vw.frame.size.height - profButtonImage.size.height)/2, profButtonImage.size.width, profButtonImage.size.height);
        [profButton setImage:profButtonImage forState:UIControlStateNormal];
        profButton.tag = 3;
        [profButton addTarget:self action:@selector(hideBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [BtnBG_Vw addSubview:profButton];

       
        return bubbleImgView;

    }
}

-(void)createBackGroundVw:(BOOL)isBottom menuView:(UIView*)menuVw
{
    UIImage *arrowImg = [UIImage imageNamed:@"contact-arrow"];
    

    UIImageView* arrowImgVw = [UIImageView new];
    [menuVw addSubview:arrowImgVw];
    
    UIView* BG_Vw = [UIView new];
    BG_Vw.backgroundColor = RGBA(172, 203, 224, 1);
    BG_Vw.layer.cornerRadius = 6;
    //[menuVw addSubview:BG_Vw];
    
    
    if (!isBottom) {
        arrowImg = [self imageRotatedByDegrees:arrowImg deg:180];
        arrowImgVw.image = arrowImg;
        
        BG_Vw.frame = CGRectMake(0, 0, menuVw.frame.size.width, HEIGHT);
        
        arrowImgVw.frame = CGRectMake(10, BG_Vw.frame.size.height, arrowImg.size.width, arrowImg.size.height);
        
    }
    else
    {
        arrowImgVw.frame = CGRectMake(10, 0, arrowImg.size.width, arrowImg.size.height);
        arrowImgVw.image = arrowImg;
        BG_Vw.frame = CGRectMake(0, ARROWHEIGHT, menuVw.frame.size.width, HEIGHT);

    }
    UIView* BtnBG_Vw = [UIView new];
    BtnBG_Vw.frame = CGRectMake(5, 5, BG_Vw.frame.size.width - 10, BG_Vw.frame.size.height - 10);
    BtnBG_Vw.backgroundColor = [UIColor whiteColor];
    BtnBG_Vw.layer.cornerRadius = 6;
    //[BG_Vw addSubview:BtnBG_Vw];
    
    
    //For Btn ONE
    UIImage *callButtonImage = [UIImage imageNamed:@"call"];
   UIButton* callButton = [UIButton buttonWithType:UIButtonTypeCustom];
    callButton.frame = CGRectMake((((BtnBG_Vw.frame.size.width/3) - callButtonImage.size.width)/2), (BtnBG_Vw.frame.size.height - callButtonImage.size.height)/2, callButtonImage.size.width, callButtonImage.size.height);
    [callButton setImage:callButtonImage forState:UIControlStateNormal];
    callButton.tag = 1;
    [callButton addTarget:self action:@selector(hideBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [BtnBG_Vw addSubview:callButton];
    
    UIView* line1 = [UIView new];
    line1.frame = CGRectMake((BtnBG_Vw.frame.size.width/3)-1, 7, 1, BtnBG_Vw.frame.size.height - 14);
    line1.backgroundColor = RGBA(222, 222, 222, 1);
    [BtnBG_Vw addSubview:line1];
    
    UIImage *msgButtonImage = [UIImage imageNamed:@"msg"];
    UIButton* msgButton = [UIButton buttonWithType:UIButtonTypeCustom];
    msgButton.frame = CGRectMake((((BtnBG_Vw.frame.size.width/3) - msgButtonImage.size.width)/2)+(1*(BtnBG_Vw.frame.size.width/3)), (BtnBG_Vw.frame.size.height - msgButtonImage.size.height)/2, msgButtonImage.size.width, msgButtonImage.size.height);
    [msgButton setImage:msgButtonImage forState:UIControlStateNormal];
    msgButton.tag = 2;
    [msgButton addTarget:self action:@selector(hideBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [BtnBG_Vw addSubview:msgButton];
    
    UIView* line2 = [UIView new];
    line2.frame = CGRectMake((2*(BtnBG_Vw.frame.size.width/3))-1, 7, 1, BtnBG_Vw.frame.size.height - 14);
    line2.backgroundColor = RGBA(222, 222, 222, 1);
    [BtnBG_Vw addSubview:line2];
    
    UIImage *profButtonImage = [UIImage imageNamed:@"profil"];
    UIButton* profButton = [UIButton buttonWithType:UIButtonTypeCustom];
    profButton.frame = CGRectMake((((BtnBG_Vw.frame.size.width/3) - profButtonImage.size.width)/2)+(2*(BtnBG_Vw.frame.size.width/3)), (BtnBG_Vw.frame.size.height - profButtonImage.size.height)/2, profButtonImage.size.width, profButtonImage.size.height);
    [profButton setImage:profButtonImage forState:UIControlStateNormal];
    profButton.tag = 3;
    [profButton addTarget:self action:@selector(hideBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [BtnBG_Vw addSubview:profButton];
    
    
    ////testing
    
   
    
}

- (UIImage *)imageRotatedByDegrees:(UIImage*)oldImage deg:(CGFloat)degrees{
    // calculate the size of the rotated view's containing box for our drawing space
    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0,oldImage.size.width, oldImage.size.height)];
    CGAffineTransform t = CGAffineTransformMakeRotation(degrees * M_PI / 180);
    rotatedViewBox.transform = t;
    CGSize rotatedSize = rotatedViewBox.frame.size;
    // Create the bitmap context
    UIGraphicsBeginImageContext(rotatedSize);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    // Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
    
    //   // Rotate the image context
    CGContextRotateCTM(bitmap, (degrees * M_PI / 180));
    
    // Now, draw the rotated/scaled image into the context
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-oldImage.size.width / 2, -oldImage.size.height / 2, oldImage.size.width, oldImage.size.height), [oldImage CGImage]);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (UIImage *)tintImage:(UIImage *)image withColor:(UIColor *)color
{
    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, image.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    CGContextClipToMask(context, rect, image.CGImage);
    [color setFill];
    CGContextFillRect(context, rect);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}




-(void)hideBtnClicked:(id)sender
{
    [UIView transitionWithView:self
                      duration:0.3f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"HideQuickMenu" object:nil];

                        [self removeFromSuperview];
                        
                    } completion:^(BOOL finished){
                        
                        UIButton* btn = (UIButton*)sender;
                        if ([_fromVC isEqual:@"Recent"])
                        {
                            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                            [dict setObject: [NSString stringWithFormat:@"%ld",(long)btn.tag]  forKey: QUICK_MENU_FOR_RECENT];
                            [[NSNotificationCenter defaultCenter] postNotificationName:QUICK_MENU_FOR_RECENT object:nil userInfo:dict];

                        } else if ([_fromVC isEqualToString:@"Contact"])
                        {
                            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                            [dict setObject: [NSString stringWithFormat:@"%ld",(long)btn.tag]  forKey: QUICK_MENU_FOR_CONTACT];
                            [[NSNotificationCenter defaultCenter] postNotificationName:QUICK_MENU_FOR_CONTACT object:nil userInfo:dict];

                        }
                        
                                            }];
    
    
    
}

-(void)HideQuickMenu:(NSNotification *)notification
{
    [UIView transitionWithView:self.superview
                      duration:0.3f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"HideQuickMenu" object:nil];

                        [self removeFromSuperview];
                        
                    } completion:NULL];
    
    
}

-(void)tappedOuterVw:(UITapGestureRecognizer *)sender
{
    CGPoint touchLocation = [sender locationInView:sender.view];

    for (UIView *view in self.subviews)
    {
        if (CGRectContainsPoint(view.frame, touchLocation))
        {
            
        }
        else
        {
            [UIView transitionWithView:self.superview
                              duration:0.3f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^(void) {
                                
                                
                                [[NSNotificationCenter defaultCenter] removeObserver:self name:@"HideQuickMenu" object:nil];
                                
                                [self removeFromSuperview];
                                
                            } completion:NULL];

            
        }
    }

    }
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self];
    
    for (UIView *view in self.subviews)
    {
        if (CGRectContainsPoint(view.frame, touchLocation))
        {
            
        }
        else
        {
            [UIView transitionWithView:self.superview
                              duration:0.3f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^(void) {
                                [[NSNotificationCenter defaultCenter] removeObserver:self name:@"HideQuickMenu" object:nil];
                                
                                [self removeFromSuperview];
                                
                            } completion:NULL];

        }
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
