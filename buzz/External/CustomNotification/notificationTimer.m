//
//  notificationTimer.m
//  buzz
//
//  Created by shaili_solutions_MacPro3 on 30/07/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "notificationTimer.h"
#import "notiVw.h"

@implementation notificationTimer
-(void)createTimer_id:(UIView*)view seconds:(float)seconds
{
    
    NSMutableDictionary* privateDict = [NSMutableDictionary new];
    [privateDict setObject:view forKey:@"NotificationVw"];
    
    self.singleton = [Singleton sharedMySingleton];
    
    
    NSTimer* timer = [NSTimer scheduledTimerWithTimeInterval:seconds target:self selector:@selector(timerDead:) userInfo:privateDict repeats:YES];
    
}

- (void)timerDead:(NSTimer *)timer {
    NSMutableDictionary *dict = [timer userInfo];
    
    
    if (dict) {
        
        notiVw* noti = [dict objectForKey:@"NotificationVw"];
        [noti removeFromWindow:noti];
        
        [timer invalidate];
        
        
        
    }
    
}


@end
