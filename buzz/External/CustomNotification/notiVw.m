//
//  notiVw.m
//  buzz
//
//  Created by shaili_solutions_MacPro3 on 30/07/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

//#define HEIGHT 77
#define ANIMATIONSPEED 0.4

//PROFILE PIC DEFINE
#define PROFILEPIC_RADIUS 48
#define PROFILEPIC_YAXIS 23

//CLOSE BTN DEFINE
#define CLOSEBTN_YAXIS 23
#define NAVBAR_HEIGHT 64

//COMMON DEFINE
#define STARTXPOINT 20
//#define ENDXPOINT 320-10

//NOTIFICATION DELAY SEC
#define NOTIFICATIONDELAY 5

//NAME LABEL DEFINE
#define NAMELABEL_YAXIS 26
#define COMMON_GAP 15

#import "notiVw.h"
#import "notificationTimer.h"
#import "UIColor+HexColors.h"

@implementation notiVw

- (id)initWithNoti_jidStr:(NSString*)jidStr NameLbl:(NSString*)nameLbl MsgLbl:(NSString*)msgLbl window:(UIWindow*)window showClose:(BOOL)showClose
{
    UIImage* bgimage = [UIImage imageNamed:@"notiSlider"];
    HEIGHT = bgimage.size.height;
    
    
    AppDelegate* myDelegate = (((AppDelegate*) [UIApplication sharedApplication].delegate));
    self = [super initWithFrame:CGRectMake(0, 0-HEIGHT, myDelegate.window.frame.size.width, HEIGHT)];
    
    
    if (self) {
        // Initialization code
        
//        UIImage *coloredBgImg  =  [self tintImage:bgimage withColor:[UIColor colorWithHexString:@"db4735"]];
        self.backgroundColor = [UIColor clearColor];
        
        
        self.jidString = jidStr;
        
        self.window = window;
        startXPoint = STARTXPOINT;
        
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        endXPoint = screenRect.size.width;
        
        [self createMsgView_jidStr:jidStr NameLbl:nameLbl MsgLbl:msgLbl showClose:YES];
        
        [self addtowindow];
    }
    return self;
}

- (id)initWithNoti_jidStr:(NSString*)jidStr NameLbl:(NSString*)nameLbl MsgLbl:(NSString*)msgLbl window:(UIWindow*)window showClose:(BOOL)showClose showImp:(BOOL)showImp
{
    
    UIImage* bgimage = [UIImage imageNamed:@"notiSlider"];
    HEIGHT = bgimage.size.height;
    
    
    AppDelegate* myDelegate = (((AppDelegate*) [UIApplication sharedApplication].delegate));
    self = [super initWithFrame:CGRectMake(0, 0-HEIGHT, myDelegate.window.frame.size.width, HEIGHT)];
    
    
    if (self) {
        // Initialization code
        
        //        UIImage *coloredBgImg  =  [self tintImage:bgimage withColor:[UIColor colorWithHexString:@"db4735"]];
       self.backgroundColor = [UIColor clearColor];
      
        
        self.jidString = jidStr;
        
        self.window = window;
        startXPoint = STARTXPOINT;
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        endXPoint = screenRect.size.width;
        [self createMsgView_jidStr:jidStr NameLbl:nameLbl MsgLbl:msgLbl showClose:YES showImp:showImp];
        
        [self addtowindow];
    }
    return self;
    
}


- (AppDelegate *)appDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (UIImage *)tintImage:(UIImage *)image withColor:(UIColor *)color
{
    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, image.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    CGContextClipToMask(context, rect, image.CGImage);
    [color setFill];
    CGContextFillRect(context, rect);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Get Display Name for JID
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(NSString *)getDisplayName:(NSString *)resourceStr
{
    if ([resourceStr rangeOfString:DOMAINAME].location==NSNotFound )
        resourceStr = [resourceStr stringByAppendingString:[NSString stringWithFormat:@"@%@",DOMAINAME]];
    
    if ([resourceStr isEqualToString:[[self appDelegate] xmppStream].myJID.bare ])
        return @"You";
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:[self managedObjectContext_roster]];
    [request setEntity:entity];
    NSString *predicateFrmt = @"jidStr = %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, resourceStr];
    [request setPredicate:predicate];
    NSArray *empArray=[[self managedObjectContext_roster] executeFetchRequest:request error:nil];
    if ([empArray count] == 1)
    {
        for (XMPPUserCoreDataStorageObject *userdetails in  empArray)
        {
            
            return [userdetails.displayName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        }
    }
    [resourceStr componentsSeparatedByString:@"@"];
    return [[resourceStr componentsSeparatedByString:@"@"] objectAtIndex:0];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark DataBase Methods For Recent Chats
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (XMPPUserCoreDataStorageObject *)RecentcontactsFetch:(XMPPJID *)Str
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:[self managedObjectContext_roster]];
    [request setEntity:entity];
    NSString *predicateFrmt = @"jidStr = %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, Str.bare];
    [request setPredicate:predicate];
    NSArray *empArray=[[self managedObjectContext_roster] executeFetchRequest:request error:nil];
    if ([empArray count] == 1)
    {
        for (XMPPUserCoreDataStorageObject *user in  empArray)
        {
            return user;
        }
    }
    return nil;
}

#pragma mark - Roster managedObjectContext
- (NSManagedObjectContext *)managedObjectContext_roster
{
    return [[(AppDelegate *)[[UIApplication sharedApplication]delegate] xmppRosterStorage] mainThreadManagedObjectContext];
}

-(void)createMsgView_jidStr:(NSString*)jidStr NameLbl:(NSString*)nameLbl MsgLbl:(NSString*)msgLbl showClose:(BOOL)showClose
{
    UIImage* bgimage = [UIImage imageNamed:@"notiSlider"];
    
    UIImageView *bgView = [UIImageView new];
    bgView.image = bgimage;
    bgView.frame = CGRectMake(0, 0, self.frame.size.width, HEIGHT);
    [self addSubview:bgView];
    
    if (jidStr)
    {
        [self AddProfilePic:jidStr];
        
    }
    
    if (showClose)
    {
        [self addCloseBtn];
    }
    
    [self add2CloseBtn];
    
    if (nameLbl) {
        [self addNameLbl:jidStr];
    }
    
    if (msgLbl) {
        [self addMsgLbl:msgLbl];
    }
    
}

-(void)createMsgView_jidStr:(NSString*)jidStr NameLbl:(NSString*)nameLbl MsgLbl:(NSString*)msgLbl showClose:(BOOL)showClose showImp:(BOOL)showImp
{
    UIImage* bgimage = [UIImage imageNamed:@"notiSlider"];
    
    UIImageView *bgView = [UIImageView new];
    bgView.image = bgimage;
    bgView.frame = CGRectMake(0, 0, self.frame.size.width, HEIGHT);
    [self addSubview:bgView];
    
    if (jidStr)
    {
        [self AddProfilePic:jidStr];
        
    }
    
    if (showClose)
    {
        [self addCloseBtn];
    }
    
    [self add2CloseBtn];
    
    if (nameLbl) {
        [self addNameLbl:jidStr];
    }
    
    if (msgLbl) {
        [self addMsgLbl:msgLbl];
    }

    if (showImp) {
        [self addImpMsgLbl:@"Imp"];
    }

}

-(void)addtowindow
{

    
    
    [self.window addSubview:self];
    
    UISwipeGestureRecognizer *swipeUpDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGesture:)];
    [swipeUpDown setDirection:UISwipeGestureRecognizerDirectionUp];
    [self addGestureRecognizer:swipeUpDown];
    
    UITapGestureRecognizer *tapDown = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDownGesture:)];
    [tapDown setNumberOfTouchesRequired : 1];
    [self addGestureRecognizer:tapDown];
    
    self.singleton = [Singleton sharedMySingleton];
    [self.singleton.notificationVwAry addObject:self];
    
    [self animate:self frame:CGRectMake(0, 0, self.frame.size.width, HEIGHT) canRemove:NO];
    
    
    notificationTimer* notiTimer = [notificationTimer new];
    [notiTimer createTimer_id:self seconds:NOTIFICATIONDELAY];
    
}



-(void)removeFromWindow:(UIView*)view
{
    [self animate:view frame:CGRectMake(0, 0-HEIGHT, view.frame.size.width, HEIGHT) canRemove:YES];
    
}

-(void)animate:(UIView*)view frame:(CGRect)frame canRemove:(BOOL)canRemove
{
    [UIView animateWithDuration:ANIMATIONSPEED
                     animations:^{
                         [view setFrame:frame];
                     } completion:^(BOOL finished)
     {
         if (canRemove) {
             [view removeFromSuperview];
             self.singleton = [Singleton sharedMySingleton];
             if ([self.singleton.notificationVwAry containsObject:view])
             {
                 [self.singleton.notificationVwAry removeObject:view];
             }
             if ([self.singleton.notificationVwAry count] == 0) {
                 self.window.frame = CGRectMake(0, 0, 0, 0);
                 
             }
         }
         else
         {
             
         }
         
     }];
}




-(void)AddProfilePic:(NSString*)jidStr
{
    UIImage* img;
    
    NSData *photoData = [[[self appDelegate] xmppvCardAvatarModule] photoDataForJID:[XMPPJID jidWithString:jidStr]];
    if (photoData != nil)
        img = [UIImage imageWithData:photoData];
    else
        img = [UIImage imageNamed:@"defaultPerson"];
    
    UIImageView* profile_pic = [UIImageView new];
    profile_pic.frame = CGRectMake(startXPoint, (NAVBAR_HEIGHT - PROFILEPIC_RADIUS)/2, PROFILEPIC_RADIUS, PROFILEPIC_RADIUS);
    profile_pic.image = img;
    profile_pic.layer.borderWidth = 2;
    profile_pic.layer.borderColor = [[UIColor whiteColor] CGColor];
    profile_pic.layer.cornerRadius  = PROFILEPIC_RADIUS/2;
    profile_pic.layer.masksToBounds = YES;
    [self addSubview:profile_pic];
    
    startXPoint = startXPoint + profile_pic.frame.size.width + COMMON_GAP;
}


-(void)addNameLbl:(NSString*)jidStr
{
    UILabel* titleLbl = [UILabel new];
    titleLbl.frame = CGRectMake(startXPoint,((NAVBAR_HEIGHT - PROFILEPIC_RADIUS)/2)+2, endXPoint - startXPoint, 20);
    titleLbl.text = [self getDisplayName:jidStr];
    titleLbl.font = [UIFont boldSystemFontOfSize:18];
    titleLbl.textColor =  [UIColor whiteColor];
    titleLbl.backgroundColor = [UIColor clearColor];
    titleLbl.textAlignment = NSTextAlignmentLeft;
    [self addSubview:titleLbl];
}

-(void)addMsgLbl:(NSString*)msgLbl
{
    UILabel* titleLbl = [UILabel new];
    titleLbl.frame = CGRectMake(startXPoint,((NAVBAR_HEIGHT - PROFILEPIC_RADIUS)/2)+24, endXPoint - startXPoint, 20);
    titleLbl.text = msgLbl;
    titleLbl.font = [UIFont fontWithName:FontString size:16];
    titleLbl.textColor = [UIColor whiteColor];
    titleLbl.backgroundColor = [UIColor clearColor];
    titleLbl.textAlignment = NSTextAlignmentLeft;
    [self addSubview:titleLbl];
}

-(void)addImpMsgLbl:(NSString*)msgLbl
{
    AppDelegate* myDelegate = (((AppDelegate*) [UIApplication sharedApplication].delegate));

    UIImage *star = [UIImage imageNamed:@"impMsgTV"];
    UIImageView *impImage = [UIImageView new];
    impImage.image = [UIImage imageNamed:@"impMsgTV"];

    impImage.frame = CGRectMake(myDelegate.window.frame.size.width - (star.size.width/2 + startXPoint),((NAVBAR_HEIGHT - PROFILEPIC_RADIUS)/2)+24, star.size.width/2,  star.size.height/2);
    [self addSubview:impImage];
}


-(void)addCloseBtn
{
    UIImage* closeImg = [UIImage imageNamed:@"notiClose"];
    
   // UIImage *coloredBgImg  =  [self tintImage:closeImg withColor:[UIColor whiteColor]];
    
    AppDelegate* myDelegate = (((AppDelegate*) [UIApplication sharedApplication].delegate));

    UIButton* cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(myDelegate.window.frame.size.width - (closeImg.size.width + 20), (NAVBAR_HEIGHT - closeImg.size.height)/2 , closeImg.size.width, closeImg.size.height);
    [cancelBtn setImage:closeImg forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancel_BTN_CLICKED:) forControlEvents:UIControlEventTouchUpInside];
    cancelBtn.backgroundColor = [UIColor clearColor];
    [self addSubview:cancelBtn];
    
    endXPoint = cancelBtn.frame.origin.x;
}

-(void)add2CloseBtn
{
    UIButton* cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake((self.frame.size.width - 70)/2, (self.frame.size.height - 18), 70,18);
    [cancelBtn addTarget:self action:@selector(cancel_BTN_CLICKED:) forControlEvents:UIControlEventTouchUpInside];
    cancelBtn.backgroundColor = [UIColor clearColor];
    [self addSubview:cancelBtn];
    
    
}

-(void)cancel_BTN_CLICKED:(id)sender
{
    UIButton* btn = (UIButton*)sender;
    
    [self removeFromWindow:btn.superview];
}


- (void)handleSwipeGesture:(UISwipeGestureRecognizer *)recognizer
{
    self.singleton = [Singleton sharedMySingleton];
    for (notiVw* noti in self.singleton.notificationVwAry)
    {
        [self animate:noti frame:CGRectMake(0, 0-HEIGHT, noti.frame.size.width, HEIGHT) canRemove:YES];
        
    }
}

- (void)tapDownGesture:(UITapGestureRecognizer *)recognizer
{
    notiVw* noti = (notiVw*)recognizer.view;
    
    if (noti)
    {
        [self animate:recognizer.view frame:CGRectMake(0, 0-HEIGHT, noti.frame.size.width, HEIGHT) canRemove:YES];
        [self handleTouch:noti.jidString];
    }
}


-(void)handleTouch:(NSString*)jidstr
{
    XMPPUserCoreDataStorageObject *userdetails = [self RecentcontactsFetch:[XMPPJID jidWithString:jidstr]];
    NSMutableDictionary *dict = [NSMutableDictionary new];
    
    if (dict) {
        
        if (userdetails) {
            [dict setObject:userdetails forKey:@"userdetails"];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TEMPNOTI" object:nil userInfo:dict];
        
    }
    
}

- (UIWindow *)_topAppWindow
{
    UIWindow *topWindow = [[[UIApplication sharedApplication].windows sortedArrayUsingComparator:^NSComparisonResult(UIWindow *win1, UIWindow *win2) {
        return win1.windowLevel - win2.windowLevel;
    }] lastObject];
    return topWindow;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
