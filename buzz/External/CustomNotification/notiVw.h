//
//  notiVw.h
//  buzz
//
//  Created by shaili_solutions_MacPro3 on 30/07/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface notiVw : UIView
{
    int startXPoint;
    int endXPoint;
    
    int HEIGHT;

}
@property (strong, nonatomic) Singleton* singleton;

@property (nonatomic) UIWindowLevel windowLevel;
@property (nonatomic,retain) NSString* jidString;
@property (nonatomic,retain) UIWindow* window;



- (id)initWithNoti_jidStr:(NSString*)jidStr NameLbl:(NSString*)nameLbl MsgLbl:(NSString*)msgLbl window:(UIWindow*)window showClose:(BOOL)showClose;
- (id)initWithNoti_jidStr:(NSString*)jidStr NameLbl:(NSString*)nameLbl MsgLbl:(NSString*)msgLbl window:(UIWindow*)window showClose:(BOOL)showClose showImp:(BOOL)showImp;
-(void)removeFromWindow:(UIView*)view;
@end
