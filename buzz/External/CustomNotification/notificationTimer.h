//
//  notificationTimer.h
//  buzz
//
//  Created by shaili_solutions_MacPro3 on 30/07/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface notificationTimer : NSObject

@property (strong, nonatomic) Singleton *singleton;

-(void)createTimer_id:(UIView*)view seconds:(float)seconds;

@end
