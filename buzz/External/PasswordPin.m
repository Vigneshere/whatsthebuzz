//
//  PasswordPin.m
//  buzz
//
//  Created by shaili_solutions_MacPro3 on 26/06/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "PasswordPin.h"
#import "Header.h"
#import "MasterPinView.h"

# define keyboardHeight 220

@implementation PasswordPin

- (id)initWithFrame_onView:(UIView*)onview LabelString:(NSString*)lblStr password:(NSString*)pwdStr
{
    self = [super initWithFrame:CGRectMake(0, 0, onview.frame.size.width, onview.frame.size.height)];
    if (self) {
        // Initialization code
        self.onView = onview;
        self.lblString = lblStr;
        self.pwdString  = pwdStr;
        self.backgroundColor = [UIColor clearColor];
        PassCodeVW = [[UIView alloc]init];
        hiddenPwdTxtFld = [[UITextField alloc]init];
        
        
    }
    return self;
}


- (id)initWithFrame_onView:(UIView*)onview LabelString:(NSString*)lblStr password:(NSString*)pwdStr Hint:(NSString *)mode JID:(NSString *)JIDStr
{
    self = [super initWithFrame:CGRectMake(0, 0, onview.frame.size.width, onview.frame.size.height)];
    if (self) {
        // Initialization code
        _onView = onview;
        _lblString = lblStr;
        _pwdString  = pwdStr;
        _JIDStr = JIDStr;
        _modeType = mode;
        
        self.backgroundColor = [UIColor clearColor];
        PassCodeVW = [[UIView alloc]init];
        hiddenPwdTxtFld = [[UITextField alloc]init];
    }
    return self;
}


-(void)createPasswordVw
{
    
    UIView* overlayVw = [UIView new];
    overlayVw.frame = self.frame;
   // overlayVw.backgroundColor = RGBA(160, 160, 160, 1.0);
    [self addSubview:overlayVw];
    
    //** Blur Effect for Header
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
        UIVisualEffect *blurEffect;
        blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        UIVisualEffectView *visualEffectView;
        visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        visualEffectView.frame = overlayVw.bounds;
        [overlayVw addSubview:visualEffectView];
    }
    else{
        overlayVw.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.9];
    }
    
    PassCodeVW = [self create_circles:4 radius:20 space:10 view:PassCodeVW];
    PassCodeVW.frame = CGRectMake((self.frame.size.width - PassCodeVW.frame.size.width)/2, (self.frame.size.height - keyboardHeight)/2, PassCodeVW.frame.size.width, PassCodeVW.frame.size.height);
    [self addSubview:PassCodeVW];
    
    UILabel* titleLbl = [UILabel new];
    titleLbl.frame = CGRectMake(0,PassCodeVW.frame.origin.y - 50, self.frame.size.width, 20);
    
    NSString *tem =self.lblString;
    if (tem != nil && ![tem isEqualToString:@""]) {
        NSMutableAttributedString *temString=[[NSMutableAttributedString alloc]initWithString:tem];
        [temString addAttribute:NSUnderlineStyleAttributeName
                          value:[NSNumber numberWithInt:1]
                          range:(NSRange){0,[temString length]}];
        titleLbl.attributedText = temString;}
    
    //titleLbl.text = self.lblString;
    titleLbl.font = [UIFont boldSystemFontOfSize:18];
    //titleLbl.textColor = RGBA(255, 255, 255, 1);
    titleLbl.textColor =[UIColor blackColor];
    titleLbl.backgroundColor = [UIColor clearColor];
    titleLbl.textAlignment = NSTextAlignmentCenter;
    [self addSubview:titleLbl];
    
    
    UIButton* okBtn;
    
    if (fromSOS){
        
        okBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        okBtn.frame = CGRectMake(((self.frame.size.width/2) - 80)/3, PassCodeVW.frame.origin.y + PassCodeVW.frame.size.height + 30, 80, 30);
        [okBtn setTitle:@"OK" forState:UIControlStateNormal];
        [okBtn setTintColor:[UIColor whiteColor]];
        [okBtn addTarget:self action:@selector(SOS_OK_BTN_CLICKED) forControlEvents:UIControlEventTouchUpInside];
        okBtn.backgroundColor = RGBA(46, 141, 214, 1);
        okBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
        okBtn.layer.cornerRadius = 10;
        [self addSubview:okBtn];
    }
    else{
        okBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        okBtn.frame = CGRectMake(((self.frame.size.width/2) - 80)/3, PassCodeVW.frame.origin.y + PassCodeVW.frame.size.height + 30, 80, 30);
        [okBtn setTitle:@"OK" forState:UIControlStateNormal];
        [okBtn setTintColor:[UIColor whiteColor]];
        [okBtn addTarget:self action:@selector(OK_BTN_CLICKED) forControlEvents:UIControlEventTouchUpInside];
        okBtn.backgroundColor = RGBA(46, 141, 214, 1);
        okBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
        okBtn.layer.cornerRadius = 10;
        [self addSubview:okBtn];
    }
    
    
    UIButton* forgotBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    forgotBtn.frame = CGRectMake((CGRectGetMaxX(okBtn.frame)+ 20), PassCodeVW.frame.origin.y + PassCodeVW.frame.size.height + 30, 80, 30);
    [forgotBtn setTitle:@"Forgot" forState:UIControlStateNormal];
    [forgotBtn setTintColor:[UIColor whiteColor]];
    [forgotBtn addTarget:self action:@selector(forgotBtnTapped) forControlEvents:UIControlEventTouchUpInside];
    forgotBtn.backgroundColor = RGBA(46, 141, 214, 1);
    forgotBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    forgotBtn.layer.cornerRadius = 10;
    [self addSubview:forgotBtn];
    
    UIButton* cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake((CGRectGetMaxX(forgotBtn.frame)+ 20), PassCodeVW.frame.origin.y + PassCodeVW.frame.size.height + 30, 80, 30);
    [cancelBtn setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelBtn setTintColor:[UIColor whiteColor]];
    [cancelBtn addTarget:self action:@selector(cancel_BTN_CLICKED) forControlEvents:UIControlEventTouchUpInside];
    cancelBtn.backgroundColor = RGBA(46, 141, 214, 1);
    cancelBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    cancelBtn.layer.cornerRadius = 10;
    [self addSubview:cancelBtn];
    
    
    if (fromSOS)
    {
        
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:@"SOSPASSWORD"];
        if (str.length == 0)
        {
            forgotBtn.hidden = YES;
            okBtn.frame = CGRectMake(((self.frame.size.width/2) - 80)/2, PassCodeVW.frame.origin.y + PassCodeVW.frame.size.height + 30, 80, 30);
            cancelBtn.frame = CGRectMake((((self.frame.size.width/2) - 80)/2)+(self.frame.size.width)/2, PassCodeVW.frame.origin.y + PassCodeVW.frame.size.height + 30, 80, 30);
        }
        
    }
    // else
    
    if ([_modeType isEqualToString:SET_PRIVATE_PWD ] || [_modeType isEqualToString:CONFRM_PRIVATE_PWD])
    {
        forgotBtn.hidden = YES;
        okBtn.frame = CGRectMake(((self.frame.size.width/2) - 80)/2, PassCodeVW.frame.origin.y + PassCodeVW.frame.size.height + 30, 80, 30);
        cancelBtn.frame = CGRectMake((((self.frame.size.width/2) - 80)/2)+(self.frame.size.width)/2, PassCodeVW.frame.origin.y + PassCodeVW.frame.size.height + 30, 80, 30);
    }
    
    
    hiddenPwdTxtFld.keyboardType = UIKeyboardTypeNumberPad;
    hiddenPwdTxtFld.delegate = self;
    [self addSubview:hiddenPwdTxtFld];
    
    [hiddenPwdTxtFld becomeFirstResponder];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    [PassCodeVW addGestureRecognizer:tapGesture];
    
    UITapGestureRecognizer *tapGesture_keyBord = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGesture)];
    [self addGestureRecognizer:tapGesture_keyBord];
    [self.onView addSubview:self];
    
}

-(void)forgotBtnTapped
{
    [hiddenPwdTxtFld resignFirstResponder];
    MasterPinView *masterPin = [[MasterPinView alloc]initWithFrame_onView:self Title:@"Answer The Question" Entry:@"Check"];
    [masterPin createMasterView];
}


-(void)handleTapGesture:(UITapGestureRecognizer *) sender
{
    [hiddenPwdTxtFld becomeFirstResponder];
}

-(void)handleSwipeGesture
{
    
    [hiddenPwdTxtFld resignFirstResponder];
}
-(UIView*)create_circles:(int)count radius:(int)radius space:(int)space view:(UIView*)view
{
    
    view.frame = CGRectMake(0, 0, (space * (count - 1) + (count * 2 * radius)), 2*radius);
    
    for (int i = 0; i < count; i++) {
        
        UILabel* lbl = [[UILabel alloc]init];
        lbl.frame = CGRectMake(i*((2*radius)+space), 0, 2*radius, 2*radius);
        lbl.backgroundColor = [UIColor whiteColor];
        lbl.tag = i+1;
        lbl.layer.cornerRadius = radius;
        lbl.layer.borderColor = [RGBA(214, 214, 214, 1) CGColor];
        lbl.layer.borderWidth =0.5;
        
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.layer.masksToBounds = YES;
        [view addSubview:lbl];
    }
    
    return view;
}


-(void)OK_BTN_CLICKED
{
    NSString *pwdString = [[NSUserDefaults standardUserDefaults] valueForKey:MASTER_PIN];
    
   // NSLog(@"Password %@ = ", pwdString);
    
    if (hiddenPwdTxtFld.text.length >= 4)
    {
        if ([_modeType isEqualToString:SET_PRIVATE_PWD])
        {
            [[NSUserDefaults standardUserDefaults] setValue:hiddenPwdTxtFld.text forKey:MASTER_PIN];
            [self isPasswordCorrect:YES];
            
        } else if ([hiddenPwdTxtFld.text isEqualToString:pwdString])
        {
            [self chekForModePassword:pwdString];
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:ALERTVIEW_TITLE message:@"Incorrect password" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
            [self clearPassword];
        }
        
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:ALERTVIEW_TITLE message:@"Password cannot be empty" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
    }
    
    
}



-(void)chekForModePassword:(NSString *)pwdString
{
    if ([_modeType isEqualToString:CONFRM_PRIVATE_PWD])
    {
        if ([hiddenPwdTxtFld.text isEqualToString:pwdString])
        {
            [self isPasswordCorrect:YES];
            
            
        }
    }
    else if ([_modeType isEqualToString:ENTER_PRIVATE_PWD])
    {
        if ([hiddenPwdTxtFld.text isEqualToString:pwdString])
        {
            [self isPasswordCorrect:YES];
            
            
        }
    }
}

-(void)clearPassword
{
    hiddenPwdTxtFld.text = @"";
    for (int i =0; i < 4; i++) {
        UILabel* passcode = (UILabel*)[PassCodeVW viewWithTag:i+1];
        passcode.text = @"";
        
    }
}


-(void)SOS_OK_BTN_CLICKED
{
    
  //  NSLog(@"OK FROM SOS");
    
    if (hiddenPwdTxtFld.text.length>=4)
    {
        
        [[NSUserDefaults standardUserDefaults]setObject:hiddenPwdTxtFld.text forKey:@"SOSPASSWORD"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SOSPasswordOK" object:nil userInfo:nil];
        
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:ALERTVIEW_TITLE message:@"Password cannot be empty" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
    }
}

-(void)cancel_BTN_CLICKED
{
    [self isPasswordCorrect:NO];
}

#pragma mark - textField
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    
    return YES;
}


- (void) textFieldDidBeginEditing:(UITextField *)textField {
    
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    [textField resignFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
    {
        
        
        return NO;
    }
    // verify max length has not been exceeded
    NSUInteger newLength = textField.text.length + string.length - range.length;
    
    if ([string isEqualToString:@""] ) {
        UILabel* passcode = (UILabel*)[PassCodeVW viewWithTag:newLength+1];
        passcode.text = @"";
        return YES;
    }
    if (newLength < 5) {
        UILabel* passcode = (UILabel*)[PassCodeVW viewWithTag:newLength];
        passcode.text = string;
        [NSTimer scheduledTimerWithTimeInterval: 0.2 target: self
                                       selector: @selector(substitude:) userInfo:passcode repeats: NO];
        return YES;
    }
    else
    {
        return NO;
    }
    
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    return YES;
}

-(void)substitude :(NSTimer*)theTimer
{
    UILabel* passcodeLbl = (UILabel*)[theTimer userInfo];
    
    passcodeLbl.text = @"\u25cf";
    
}

-(void)isPasswordCorrect:(BOOL)isCorrect
{
    NSMutableDictionary* dict = [NSMutableDictionary new];
    
    if (fromSOS)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"privatePassword" object:nil userInfo:dict];
    }
    else
    {
        if (isCorrect)
        {
            [dict setObject:@"YES" forKey:@"isCorrect"];
            [dict setObject:_modeType forKey:@"Mode"];
            
        }
        else
        {
            [dict setObject:@"NO" forKey:@"isCorrect"];
            [dict setObject:_modeType forKey:@"Mode"];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"privatePassword" object:nil userInfo:dict];
    }
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
