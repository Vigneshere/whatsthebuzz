//
//  SOSMapVwCtrl.m
//  buzz
//
//  Created by shaili_macMini02 on 05/05/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "SOSMapVwCtrl.h"
#import "GoogleRoute.h"
#import "RouteAnnotation.h"
#import "NSString+HTML.h"

@interface SOSMapVwCtrl ()
{
    UIButton* me_Btn;
     UIButton* route_Btn;
    UIButton* back_Btn;

    MKUserLocation* userLoc;
    BOOL firstIsntance;

}

@end

@implementation SOSMapVwCtrl
@synthesize userId,userName,latStr,longStr,sosId;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Appdelegate Values
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (AppDelegate *)appDelegate
{
	return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (XMPPStream *)xmppStream
{
	return [[self appDelegate] xmppStream];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    firstIsntance = YES;
    
    mapView = [[MKMapView alloc] initWithFrame:self.view.frame];
    me_Btn = [UIButton buttonWithType:UIButtonTypeSystem];
     route_Btn = [UIButton buttonWithType:UIButtonTypeSystem];
     back_Btn = [UIButton buttonWithType:UIButtonTypeSystem];
    
    mapView.delegate = self;
    mapView.showsUserLocation = YES;
    [self.view addSubview:mapView];
	// Do any additional setup after loading the view, typically from a nib.
    
    
}



- (void)viewDidUnload
{
    mapView = nil;
   
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    me_Btn.frame = CGRectMake(10, mapView.frame.size.height -30, 54, 20);
    [me_Btn setTitle:@"ME" forState:UIControlStateNormal];
    [me_Btn addTarget:self action:@selector(me_Btn_CLICKED) forControlEvents:UIControlEventTouchUpInside];
    [me_Btn setTintColor:[UIColor whiteColor]];
    me_Btn.backgroundColor = [UIColor redColor];
    me_Btn.titleLabel.font = [UIFont fontWithName:FontString size:12];
    me_Btn.layer.cornerRadius = 10;
    [mapView addSubview:me_Btn];

    route_Btn.frame = CGRectMake(mapView.frame.size.width - 84, mapView.frame.size.height -30, 74, 20);
    [route_Btn setTitle:@"ROUTE" forState:UIControlStateNormal];
    [route_Btn addTarget:self action:@selector(route_Btn_CLICKED) forControlEvents:UIControlEventTouchUpInside];
    [route_Btn setTintColor:[UIColor whiteColor]];
    route_Btn.backgroundColor = [UIColor redColor];
    route_Btn.titleLabel.font = [UIFont fontWithName:FontString size:12];
    route_Btn.layer.cornerRadius = 10;
    [mapView addSubview:route_Btn];
    
   mapView.layer.shadowColor = [[UIColor clearColor] CGColor];
    
    [self createNavView];
}


-(void)createNavView
{
    UIView* navView = [UIView new];
    navView.frame = CGRectMake(5, 20, 310, 60);
    [navView setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.6]];
    [mapView addSubview:navView];
    
    back_Btn.frame = CGRectMake(-15,5, 50, 50);
    [back_Btn addTarget:self action:@selector(back_Btn_CLICKED) forControlEvents:UIControlEventTouchUpInside];
    [back_Btn setBackgroundImage:[UIImage imageNamed:@"sos_Back"] forState:UIControlStateNormal];
    [navView addSubview:back_Btn];
    back_Btn = nil;
    
    AvatarImage* avatarImg = [AvatarImage new];
    UIImageView* profileImg = [UIImageView new];
    profileImg.frame = CGRectMake(40, 5, 50, 50);
    profileImg.layer.cornerRadius = 25;
    profileImg.layer.masksToBounds = YES;
    profileImg.image = [avatarImg profileImage:[self friendDetailFetch:[NSString stringWithFormat:@"%@@%@",userId,DOMAINAME]]];
    [navView addSubview:profileImg];
    profileImg = nil;
    
    UILabel* nameLbl = [UILabel new];
    nameLbl.frame = CGRectMake(100, 5, navView.frame.size.width - 100, 50);
    nameLbl.backgroundColor = [UIColor clearColor];
    nameLbl.textColor = [UIColor brownColor];
    nameLbl.text = [NSString stringWithFormat:@"%@ is \nUnder Emergency!",userName];
    nameLbl.lineBreakMode = NSLineBreakByWordWrapping;
    nameLbl.numberOfLines = 0;
    nameLbl.font = [UIFont fontWithName:FontString size:20];
    [nameLbl sizeToFit];

    [navView addSubview:nameLbl];

    
}

-(void)me_Btn_CLICKED
{

[self setCenterCoordinate:userLoc.coordinate zoomLevel:16 animated:YES];
    
}

-(void)route_Btn_CLICKED
{
    
    [mapView showAnnotations:mapView.annotations animated:YES];
    
}
-(void)back_Btn_CLICKED
{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark DataBase Methods For Recent Chats
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (XMPPUserCoreDataStorageObject *)friendDetailFetch:(NSString *)Str
{
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:[self managedObjectContext_roster]];
    [request setEntity:entity];
    NSString *predicateFrmt = @"jidStr = %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, Str];
    [request setPredicate:predicate];
    NSArray *empArray=[[self managedObjectContext_roster] executeFetchRequest:request error:nil];
    if ([empArray count] == 1)
    {
        for (XMPPUserCoreDataStorageObject *user in  empArray)
        {
            return user;
        }
    }
    return nil;
}

- (NSManagedObjectContext *)managedObjectContext_roster
{
	return [[[self appDelegate] xmppRosterStorage] mainThreadManagedObjectContext];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)setCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                  zoomLevel:(NSUInteger)zoomLevel animated:(BOOL)animated {
    MKCoordinateSpan span = MKCoordinateSpanMake(0, 360/pow(2, zoomLevel)*mapView.frame.size.width/256);
    [mapView setRegion:MKCoordinateRegionMake(centerCoordinate, span) animated:animated];
}

- (void)mapView:(MKMapView *)mapVieww didUpdateUserLocation:(MKUserLocation *)userLocation
{
    
    
    
    userLoc = userLocation;
    if (firstIsntance) {
        [self createRoute:[latStr floatValue]:[longStr floatValue]];
        firstIsntance = NO;
    }
    
}
-(void)createRoute:(float)latitude :(float)longitude
{
    CLLocation *from = userLoc.location;
    CLLocation *to = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    
    GooglePolylineRequest *gpl = [[GooglePolylineRequest alloc] init];
    gpl.delegate = self;
    [gpl requestPolylineFromPoint:from toPoint:to];

}
- (void)googlePolylineRequest:(GooglePolylineRequest*)request didFailWithError:(NSError*)error
{
    [[[UIAlertView alloc] initWithTitle:ALERTVIEW_TITLE
                                message:@"Failed to fetch route"
                               delegate:nil
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
}



- (void)googlePolylineRequest:(GooglePolylineRequest*)request didFindRoutes:(NSArray*)routes
{
    GoogleRoute *route = [routes lastObject];
    
    [mapView setRegion:[route region] animated:YES];
    
    NSMutableArray *polylines = [[NSMutableArray alloc] init];
    
    Leg *leg = [route.legs lastObject];
    
    for (Step *step in [leg steps])
    {
        

        RouteAnnotation *ann = [[RouteAnnotation alloc] initWithCoordinate:step.startLocation.coordinate
                                                                  andTitle:[[[step.htmlInstructions stringByStrippingTags] stringByRemovingNewLinesAndWhitespace] stringByDecodingHTMLEntities]];
        
        [mapView addAnnotation:ann];
        [polylines addObjectsFromArray:step.polyline];
    }
    
    /*
     NSArray *points = route.overviewPolyline;
     int pointsCount = [points count];
     */
    
    NSArray *points = polylines;
    int pointsCount = [polylines count];
    
    CLLocationCoordinate2D *coords = malloc(sizeof(CLLocationCoordinate2D) * pointsCount);
    
    for (int i=0; i< pointsCount; ++i)
    {
        CLLocation *loc = [points objectAtIndex:i];
        coords[i] = loc.coordinate;
    }
    
    MKPolyline *polyline = [MKPolyline polylineWithCoordinates:coords count:pointsCount];
    
    free(coords);
    
    [mapView addOverlay:polyline];
    
    Step *step = [[leg steps] lastObject];
    RouteAnnotation *ann = [[RouteAnnotation alloc] initWithCoordinate:step.endLocation.coordinate
                                                              andTitle:@"End of route"];
    [mapView addAnnotation:ann];
    
   // leg.distanceText;
    //leg.durationText;
}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay
{
    if ([overlay isKindOfClass:[MKPolyline class]])
    {
        MKPolylineView *view = [[MKPolylineView alloc] initWithPolyline:overlay];
        view.fillColor = [UIColor brownColor];
        view.strokeColor = [UIColor brownColor];
        view.lineWidth = 3.0f;
        return view;
    }
    
    return nil;
}

- (MKAnnotationView*)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
     MKPinAnnotationView *view = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"annotation"];
    if ([annotation isKindOfClass:[MKUserLocation class]])
    {
         view.pinColor = MKPinAnnotationColorPurple;
        
    
    }
    else if ([annotation isKindOfClass:[RouteAnnotation class]])
    {
        RouteAnnotation* routeAnno = annotation;
       // NSLog(@"title  %@",routeAnno.subtitle);
        if ([routeAnno.subtitle isEqualToString:@"End of route"])
        {
            view.pinColor = MKPinAnnotationColorRed;
        }
        else
        {
        view.pinColor = MKPinAnnotationColorGreen;
        }
        
    }
   else {
    view.pinColor = MKPinAnnotationColorGreen;
    }
    

   
    
    view.canShowCallout = YES;
    return view;
}

@end
