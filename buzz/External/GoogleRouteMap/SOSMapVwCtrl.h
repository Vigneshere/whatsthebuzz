//
//  SOSMapVwCtrl.h
//  buzz
//
//  Created by shaili_macMini02 on 05/05/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GooglePolylineRequest.h"
#import <MapKit/MapKit.h>

@interface SOSMapVwCtrl : UIViewController<GooglePolylineRequestDelegate, MKMapViewDelegate>
{
    MKMapView *mapView;
    NSString* sosId;
    NSString* userName;
    NSString* userId;
    NSString* latStr;
    NSString* longStr;
    
}

@property(nonatomic,retain)NSString *sosId;
@property(nonatomic,retain)NSString *userName;
@property(nonatomic,retain)NSString *userId;
@property(nonatomic,retain)NSString *latStr;
@property(nonatomic,retain)NSString *longStr;

@end
