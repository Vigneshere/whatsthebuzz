//
//  settingViewController.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 18/03/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "SettingViewController.h"
#import "profileFormViewController.h"
#import "ChatSettingViewController.h"
#import "InviteSettingViewController.h"
#import "AccountSettingViewController.h"
#define settingArray @[@"Profile",@"Account",@"Chat",@"Invite",@"Network status"]
#define SettingimgAry @[@"profile.png",@"account.png",@"chat.png",@"invite.png",@"network.png"]


@interface SettingViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *settingtableView;
    UIButton *backButton;
}
@end

@implementation SettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.title = @"Setting";
    self.navigationItem.hidesBackButton = YES;
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    UIImage *backButtonImage = [UIImage imageNamed:@"back"];
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 10, backButtonImage.size.width, backButtonImage.size.height);
    [backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    settingtableView = [UITableView new];
    settingtableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    settingtableView.delegate = self;
    settingtableView.dataSource = self;
    settingtableView.backgroundColor = [UIColor clearColor];
    settingtableView.separatorColor = [UIColor clearColor];
    [self.view addSubview:settingtableView];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController.navigationBar addSubview:backButton];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [backButton removeFromSuperview];
}
-(void)backbuttonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Tableview DataSource AND Delgate Methods
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    return settingArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MyIdentifier"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(cell.frame.origin.x+10, 49, cell.frame.size.width-20, 1)];
        separatorLineView.backgroundColor = RGBA(204, 204, 204, 0.4);
        [cell.contentView addSubview:separatorLineView];
    }
    cell.textLabel.text = [settingArray objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    cell.textLabel.textColor = RGBA(130, 130, 130, 1);
    cell.imageView.image = [UIImage imageNamed:[SettingimgAry objectAtIndex:indexPath.row]];
    if (indexPath.row == 4) {
        if ([[self xmppStream] isConnected]) {

        cell.detailTextLabel.text = @"Connected";
     
        cell.detailTextLabel.textColor = [UIColor greenColor];
            
        }
        else
        {
            cell.detailTextLabel.text = @"Not connected";
            cell.detailTextLabel.textColor = [UIColor redColor];
        }
        
    }
 return  cell;
}
- (AppDelegate *)appDelegate
{
	return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (XMPPStream *)xmppStream
{
	return [[self appDelegate] xmppStream];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (indexPath.row) {
        case 0:
        {
            profileFormViewController *profile = [profileFormViewController new];
            profile.viewControllerString = @"userSetting";
            [self.navigationController pushViewController:profile animated:YES];
        }
            
            break;
        case 1:
        {
            AccountSettingViewController *accountSetting = [AccountSettingViewController new];
            [self.navigationController pushViewController:accountSetting animated:YES];
        }
            
            break;
        case 2:
        {
            ChatSettingViewController *chatSetting = [ChatSettingViewController new ];
            [self.navigationController pushViewController:chatSetting animated:YES];
        }
            
            break;
        case 3:
        {
            InviteSettingViewController *inviteSetting = [InviteSettingViewController new ];
            [self.navigationController pushViewController:inviteSetting animated:YES];
        }
            break;
               default:
            break;
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
