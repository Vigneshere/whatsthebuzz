//
//  ChatSettingViewController.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 12/06/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "ChatSettingViewController.h"
#import "TextSizeSettingViewController.h"
#import "MediaSettingViewController.h"
#define ChatSettingArray @[@"Media download options",@"Text size",@"Save incoming media",@"Clear history"]

@interface ChatSettingViewController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation ChatSettingViewController
{
    UITableView *chatSettingtableView;
    UIButton *backButton;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.title = @"Chat Setting";
    //self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    UIImage *backButtonImage = [UIImage imageNamed:@"back"];
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 10, backButtonImage.size.width, backButtonImage.size.height);
    [backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    chatSettingtableView = [UITableView new];
    chatSettingtableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    chatSettingtableView.delegate = self;
    chatSettingtableView.dataSource = self;
    chatSettingtableView.backgroundColor = [UIColor clearColor];
    chatSettingtableView.separatorColor = [UIColor clearColor];
    [self.view addSubview:chatSettingtableView];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    // [self.navigationController.navigationBar addSubview:backButton];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [backButton removeFromSuperview];
}
-(void)backbuttonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Tableview DataSource AND Delgate Methods
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    return ChatSettingArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"chatSettingtableViewIdentifier"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"chatSettingtableViewIdentifier"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(cell.frame.origin.x+10, 49, cell.frame.size.width-20, 1)];
        separatorLineView.backgroundColor = RGBA(204, 204, 204, 0.4);
        [cell.contentView addSubview:separatorLineView];
        
        UISwitch *switchButton = [[UISwitch alloc] initWithFrame:CGRectMake(cell.frame.size.width-70, 10, 25, 25)];
        switchButton.tag = 100;
        [switchButton addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
        [cell.contentView addSubview:switchButton];
    }
    cell.textLabel.text = [ChatSettingArray objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    cell.textLabel.textColor = RGBA(130, 130, 130, 1);
    
    if (indexPath.row != 2) {
        [[cell.contentView viewWithTag:100] removeFromSuperview];
    }
    else{
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"saveIncomingMedia"] == YES)
            [(UISwitch *)[cell.contentView viewWithTag:100] setOn:YES];
        else
            [(UISwitch *)[cell.contentView viewWithTag:100] setOn:NO];
    }
    if (indexPath.row == 3) {
        cell.textLabel.textColor  = [UIColor redColor];
    }
    
    return  cell;
}
-(void)switchChanged:(id)sender
{
    UISwitch *switchButton  = (UISwitch *)sender;
    
    if (switchButton.isOn)
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"saveIncomingMedia"];
    else
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"saveIncomingMedia"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    switch (indexPath.row) {
        case 0:
        {
            MediaSettingViewController *mediaDownloadSetting  = [MediaSettingViewController new];
            [self.navigationController pushViewController:mediaDownloadSetting animated:YES];
        }
            
            break;
        case 1:
        {
            TextSizeSettingViewController *textSizeSetting  = [TextSizeSettingViewController new];
            [self.navigationController pushViewController:textSizeSetting animated:YES];
        }
            
            break;
        
        case 3:
            [self clearAllHistory];
            break;
            
        default:
            break;
    }
    
}
-(void)clearAllHistory
{
   
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Clear history"
                                                    message:@"Are you sure want to clear history!"
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    [alert show];

}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // the user clicked OK
    if (buttonIndex == 1) {
        XMPPMessageArchivingCoreDataStorage *storage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
        NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
        NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:moc];
        NSFetchRequest *request = [[NSFetchRequest alloc]init];
       [request setEntity:entityDescription];
        NSError *error;
        NSArray *messages = [moc executeFetchRequest:request error:&error];
        
        for (XMPPMessageArchiving_Message_CoreDataObject *coremessage in messages) {
            
            [moc deleteObject:coremessage];
        }
        
        NSEntityDescription *contactEnityDes = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Contact_CoreDataObject"
                                                             inManagedObjectContext:moc];
        NSFetchRequest *contactRequest = [[NSFetchRequest alloc]init];
        [contactRequest setEntity:contactEnityDes];
        NSArray *recentContactMessage = [moc executeFetchRequest:contactRequest error:&error];
        
        for (XMPPMessageArchiving_Contact_CoreDataObject *recentCoremessage in recentContactMessage) {
            
            [moc deleteObject:recentCoremessage];
        }

        [moc save:nil];
    
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
