//
//  TextSizeSettingViewController.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 13/06/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "TextSizeSettingViewController.h"
#define TextSizeSettingArray @[@"Small",@"Medium",@"Large"]

@interface TextSizeSettingViewController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation TextSizeSettingViewController
{
    
    
    UITableView *textSizeSettingtableView;
    UIButton *backButton;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.title = @"Text Size";
    // self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIImage *backButtonImage = [UIImage imageNamed:@"back"];
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 10, backButtonImage.size.width, backButtonImage.size.height);
    [backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    textSizeSettingtableView = [UITableView new];
    textSizeSettingtableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    textSizeSettingtableView.delegate = self;
    textSizeSettingtableView.dataSource = self;
    textSizeSettingtableView.backgroundColor = [UIColor clearColor];
    textSizeSettingtableView.separatorColor = [UIColor clearColor];
    [self.view addSubview:textSizeSettingtableView];
    

    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    // [self.navigationController.navigationBar addSubview:backButton];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [backButton removeFromSuperview];
}
-(void)backbuttonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Tableview DataSource AND Delgate Methods
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    return TextSizeSettingArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"accountSettingtableViewIdentifier"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"accountSettingtableViewIdentifier"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(cell.frame.origin.x+10, 49, cell.frame.size.width-20, 1)];
        separatorLineView.backgroundColor = RGBA(204, 204, 204, 0.4);
        [cell.contentView addSubview:separatorLineView];
    }
    cell.textLabel.text = [TextSizeSettingArray objectAtIndex:indexPath.row];
    cell.textLabel.textColor = RGBA(130, 130, 130, 1);
    if (indexPath.row == 0)
        cell.textLabel.font = [UIFont systemFontOfSize:11];

    else if(indexPath.row == 1)
        cell.textLabel.font = [UIFont systemFontOfSize:14];
    
    else
        cell.textLabel.font = [UIFont systemFontOfSize:17];
    if (indexPath.row == [[NSUserDefaults standardUserDefaults] integerForKey:@"textSize"]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;

    }


    return  cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [[NSUserDefaults standardUserDefaults] setInteger:indexPath.row forKey:@"textSize"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
