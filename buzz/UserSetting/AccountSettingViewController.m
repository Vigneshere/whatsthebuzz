//
//  AccountSettingViewController.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 12/06/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "AccountSettingViewController.h"
#import <malloc/malloc.h>
#import <objc/runtime.h>
#import "NetworkUsageViewController.h"
#import "NotificationSettingViewController.h"
#import "AFHTTPClient.h"
#import "AFJSONRequestOperation.h"
#import "RXMLElement.h"
#import "numberRegViewController.h"
#define AccountSettingArray @[@"Notification",@"Delete Account",@"Message usage"]
@interface AccountSettingViewController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation AccountSettingViewController
{
    UITableView *accountSettingtableView;
    UIButton *backButton;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.title = @"Account";
    // self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIImage *backButtonImage = [UIImage imageNamed:@"back"];
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 10, backButtonImage.size.width, backButtonImage.size.height);
    [backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    accountSettingtableView = [UITableView new];
    accountSettingtableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    accountSettingtableView.delegate = self;
    accountSettingtableView.dataSource = self;
    accountSettingtableView.backgroundColor = [UIColor clearColor];
    accountSettingtableView.separatorColor = [UIColor clearColor];
    [self.view addSubview:accountSettingtableView];
          
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    // [self.navigationController.navigationBar addSubview:backButton];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [backButton removeFromSuperview];
}
-(void)backbuttonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Tableview DataSource AND Delgate Methods
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    return AccountSettingArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"accountSettingtableViewIdentifier"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"accountSettingtableViewIdentifier"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(cell.frame.origin.x+10, 49, cell.frame.size.width-20, 1)];
        separatorLineView.backgroundColor = RGBA(204, 204, 204, 0.4);
        [cell.contentView addSubview:separatorLineView];
    }
    cell.textLabel.text = [AccountSettingArray objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    cell.textLabel.textColor = RGBA(130, 130, 130, 1);
    return  cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
            
        case 0:
        {
            NotificationSettingViewController *notificationSetting  = [NotificationSettingViewController new];
            [self.navigationController pushViewController:notificationSetting animated:YES];
        }
            break;
        case 1:
        {
          
            [self deleteAccount];
        }
            break;

        case 2:
        {
            NetworkUsageViewController *networkUsage  = [NetworkUsageViewController new];
            [self.navigationController pushViewController:networkUsage animated:YES];
        }
            break;
            
        default:
            break;
    }
    
}
-(void)deleteAccount
{
    
   NSString *urlStr = [NSString stringWithFormat:@"http://%@:9090/plugins/userService/userservice?type=delete&secret=shaili&username=%@",HOSTNAME,[[self xmppStream] myJID].user];
        NSString *properlyEscapedURL = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        properlyEscapedURL = [properlyEscapedURL stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
        
        NSURL *url = [NSURL URLWithString:properlyEscapedURL];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]
                                             initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSString *str = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            
            
            RXMLElement *rxml = [RXMLElement elementFromXMLString:str encoding:NSUTF8StringEncoding];
            
            NSLog(@"xml  ::: %@",rxml);
            NSString *string = [NSString stringWithFormat:@"%@",rxml];
            if ([string isEqualToString:@"ok"]) {
                [self clearAllHistory];
                [self clearContacts];
                [[self xmppStream] disconnect];
                [[NSUserDefaults standardUserDefaults] setObject:nil forKey:USERNAME];
                numberRegViewController *num = [numberRegViewController new];
                [self.navigationController pushViewController:num animated:YES];
            }

        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    
    [operation start];
    
}
-(void)clearAllHistory
{
    NSManagedObjectContext *moc = [[self appDelegate].xmppMessageArchivingStorage mainThreadManagedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entityDescription];
    NSError *error;
    NSArray *messages = [moc executeFetchRequest:request error:&error];
    
    for (XMPPMessageArchiving_Message_CoreDataObject *coremessage in messages) {
        
        [moc deleteObject:coremessage];
    }
    
    NSEntityDescription *contactEnityDes = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Contact_CoreDataObject"
                                                       inManagedObjectContext:moc];
    NSFetchRequest *contactRequest = [[NSFetchRequest alloc]init];
    [contactRequest setEntity:contactEnityDes];
    NSArray *recentContactMessage = [moc executeFetchRequest:contactRequest error:&error];
    
    for (XMPPMessageArchiving_Contact_CoreDataObject *recentCoremessage in recentContactMessage) {
        
        [moc deleteObject:recentCoremessage];
    }
    
    [moc save:nil];

}
-(void)clearContacts
{
    [[[self appDelegate] xmppRosterStorage] clearAllUsersAndResourcesForXMPPStream:[self xmppStream]];
    
}
-(AppDelegate *)appDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (XMPPStream *)xmppStream
{
        return [[self appDelegate] xmppStream];
}
         
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
