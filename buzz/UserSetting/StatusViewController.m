//
//  StatusViewController.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 18/03/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "StatusViewController.h"
#define intialStatusAry @[@"At Work",@"Busy",@"Hi",@"Im Using Whats the Buzz App"]
#define checkIndex @"checkIndex"
#define statusAry @"statusArray"
#define status @"status"


@interface StatusViewController ()<UITextViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UILabel *titleLabel;
    UITextView *statusTextView;
    UITableView *statusTableView;
    NSMutableArray *statusArray;
    NSUserDefaults* statusDefaults;
    UIButton *doneBtn,*deleteAllBtn;
    UILabel *currentStatusLabel;
    UILabel *newStatusLabel;

}
@property (nonatomic, retain) NSIndexPath* checkedIndexPath;

@end

@implementation StatusViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Appdelegate Values
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (AppDelegate *)appDelegate
{
	return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (XMPPStream *)xmppStream
{
	return [[self appDelegate] xmppStream];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    titleLabel = [UILabel new];
    statusTextView = [UITextView new];
    statusTableView = [UITableView new];
    currentStatusLabel = [UILabel new];
    newStatusLabel = [UILabel new];
    self.view.backgroundColor = [UIColor whiteColor];
     statusDefaults = [NSUserDefaults standardUserDefaults];
    self.view.backgroundColor = RGBA(249, 249, 249, 1);

    if ([statusDefaults mutableArrayValueForKey:statusAry].count == 0) {
        [[NSUserDefaults standardUserDefaults] setObject:intialStatusAry forKey:statusAry];
    }
    [statusDefaults synchronize];

    UIBarButtonItem *myButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editBarButtonPressed:)];
    self.navigationItem.rightBarButtonItem = myButton;
}
-(void)viewWillAppear:(BOOL)animated
{
    titleLabel.frame = CGRectMake(0, 10, 320, 30);
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"Status";
    titleLabel.font = [UIFont systemFontOfSize:20];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.navigationController.navigationBar addSubview:titleLabel];
    
    currentStatusLabel.font = [UIFont systemFontOfSize:15];
    currentStatusLabel.frame = CGRectMake(10, 20, 320, currentStatusLabel.font.lineHeight);
    currentStatusLabel.backgroundColor = [UIColor clearColor];
    currentStatusLabel.textColor = RGBA(53, 152, 219, 1);
    currentStatusLabel.text = @"Your Current Status";
    currentStatusLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:currentStatusLabel];
    
    
    statusTextView.frame = CGRectMake(10, 40, 300, 40);
    statusTextView.backgroundColor = [UIColor whiteColor];
    statusTextView.alpha = 0.8;
    statusTextView.textColor = [UIColor darkGrayColor];
    statusTextView.font = [UIFont systemFontOfSize:15];
    statusTextView.delegate = self;
    statusTextView.keyboardType = UIKeyboardTypeDefault;
    statusTextView.layer.cornerRadius = 10;
    [self.view addSubview:statusTextView];
   
    doneBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    doneBtn.frame = CGRectMake(statusTextView.frame.origin.x+statusTextView.frame.size.width+60, statusTextView.frame.origin.y+5, 50, 30);
    [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
    [doneBtn setTitleColor:RGBA(53, 152, 219, 1) forState:UIControlStateNormal];
    doneBtn.backgroundColor = [UIColor clearColor];
    [doneBtn addTarget:self action:@selector(doneBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:doneBtn];
    
    newStatusLabel.font = [UIFont systemFontOfSize:15];
    newStatusLabel.frame = CGRectMake(10, statusTextView.frame.origin.y+statusTextView.frame.size.height+10, 200, newStatusLabel.font.lineHeight);
    newStatusLabel.backgroundColor = [UIColor clearColor];
    newStatusLabel.textColor = RGBA(53, 152, 219, 1);
    newStatusLabel.text = @"Select your new status";
    newStatusLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:newStatusLabel];
    
    deleteAllBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    deleteAllBtn.frame = CGRectMake(newStatusLabel.frame.origin.x+newStatusLabel.frame.size.width+10, statusTextView.frame.origin.y+statusTextView.frame.size.height+5, 100, 30);
    [deleteAllBtn setTitle:@"DeleteAll" forState:UIControlStateNormal];
    [deleteAllBtn setTitleColor:RGBA(53, 152, 219, 1) forState:UIControlStateNormal];
    deleteAllBtn.backgroundColor = [UIColor clearColor];
    [deleteAllBtn addTarget:self action:@selector(DeleteAllBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:deleteAllBtn];
    deleteAllBtn.hidden = YES;
    
    statusTextView.text = [[NSUserDefaults standardUserDefaults] objectForKey:status];
    
    statusTableView.frame = CGRectMake(10, statusTextView.frame.origin.y+statusTextView.frame.size.height+30, self.view.frame.size.width-20, self.view.frame.size.height-(statusTextView.frame.origin.y+statusTextView.frame.size.height+40));
    statusTableView.delegate = self;
    statusTableView.dataSource = self;
    statusTableView.backgroundColor = [UIColor whiteColor];
    statusTableView.opaque = YES;
    statusTableView.alpha = 0.8;
    statusTableView.layer.cornerRadius  = 10;
    [self.view addSubview:statusTableView];
   
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [titleLabel removeFromSuperview];
    [statusDefaults synchronize];
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark TableView DataSource and Delegate Methods
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    return [statusDefaults mutableArrayValueForKey:statusAry].count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MyIdentifier"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
    }

    if ([statusDefaults objectForKey:checkIndex] && [[statusDefaults objectForKey:checkIndex] intValue] == indexPath.row)
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
       else
        cell.accessoryType = UITableViewCellAccessoryNone;
    
    cell.textLabel.text = [[statusDefaults mutableArrayValueForKey:statusAry] objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont systemFontOfSize:15];
    return  cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [statusTextView resignFirstResponder];
    statusTextView.text = [[statusDefaults mutableArrayValueForKey:statusAry] objectAtIndex:indexPath.row];
    [self updateStatus:statusTextView.text];
    if([statusDefaults objectForKey:checkIndex])
    {
        UITableViewCell* uncheckCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[[statusDefaults objectForKey:checkIndex] intValue] inSection:0]];
        uncheckCell.accessoryType = UITableViewCellAccessoryNone;
    }
    if([[statusDefaults objectForKey:checkIndex] intValue] == indexPath.row)
    {
        UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    [statusDefaults setObject:[NSNumber numberWithInteger:indexPath.row] forKey:checkIndex];
    [statusDefaults setObject:statusTextView.text forKey:status];

}
-(void)doneBtnAction:(id)sender
{
    if ([statusTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length >0) {
        
    [statusTextView resignFirstResponder];
    [self updateStatus:statusTextView.text];
    [statusDefaults setObject:statusTextView.text forKey:status];
    if (![[statusDefaults mutableArrayValueForKey:statusAry] containsObject:statusTextView.text])
    {
         [[statusDefaults mutableArrayValueForKey:statusAry] insertObject:statusTextView.text atIndex:0];
         [statusDefaults setObject:@0 forKey:checkIndex];
    }
    else
            [statusDefaults setObject:[NSNumber numberWithInteger:[[statusDefaults mutableArrayValueForKey:statusAry] indexOfObject: statusTextView.text]] forKey:checkIndex];
    }
         [statusTableView reloadData];
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [[statusDefaults mutableArrayValueForKey:statusAry] removeObjectAtIndex:indexPath.row];
        if (indexPath.row == [[statusDefaults objectForKey:checkIndex] intValue])
            [statusDefaults removeObjectForKey:checkIndex];
    }
        [statusTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
       
}
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    [[statusDefaults mutableArrayValueForKey:statusAry] exchangeObjectAtIndex:fromIndexPath.row withObjectAtIndex:toIndexPath.row];
    [statusDefaults setObject:[NSNumber numberWithInteger:toIndexPath.row] forKey:checkIndex];
}
- (void)editBarButtonPressed:(UIBarButtonItem *)sender{
    if (statusTableView.editing == NO) {
        UIBarButtonItem *doneBarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(editBarButtonPressed:)];
        self.navigationItem.rightBarButtonItem = doneBarBtn;
        [statusTableView setEditing:YES animated:YES];
        deleteAllBtn.hidden = NO;
        
    } else {
        UIBarButtonItem *editBtm = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editBarButtonPressed:)];
        self.navigationItem.rightBarButtonItem = editBtm;
        [statusTableView setEditing:NO animated:YES];
        deleteAllBtn.hidden = YES;
    }
}
-(void)DeleteAllBtnAction:(id)sender
{
    [statusDefaults removeObjectForKey:statusAry];
    [statusDefaults removeObjectForKey:checkIndex];
    [statusTableView reloadData];

}
-(void)updateStatus:(NSString *)str
{
    XMPPPresence *presence = [XMPPPresence presence];
    NSXMLElement *show = [NSXMLElement elementWithName:@"show" stringValue:@"Available"];
    NSXMLElement *statusVal = [NSXMLElement elementWithName:@"status" stringValue:statusTextView.text];
    [presence addChild:show];
    [presence addChild:statusVal];
    [[self xmppStream] sendElement:presence];
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    
    [self animateTextView:YES];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return textView.text.length + (text.length - range.length) <= 30;
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    [self animateTextView:NO];

}
-(void)animateTextView:(BOOL)value
{
    if (value == YES) {
        [UIView animateWithDuration:.3
                         animations:^ {
                             CGRect newBounds = statusTextView.frame;
                             newBounds.size.width -= 40;
                             statusTextView.frame = newBounds;
                             [statusTextView layoutSubviews];
                             CGRect btnBounds  = doneBtn.frame;
                             btnBounds.origin.x = statusTextView.frame.origin.x+statusTextView.frame.size.width;
                             doneBtn.frame =btnBounds;
                             
                         }];
    }
    else{
        [UIView animateWithDuration:.3
                         animations:^ {
                             CGRect newBounds = statusTextView.frame;
                             newBounds.size.width += 40;
                             statusTextView.frame = newBounds;
                             [statusTextView layoutSubviews];
                             CGRect btnBounds  = doneBtn.frame;
                             btnBounds.origin.x = statusTextView.frame.origin.x+statusTextView.frame.size.width+10;
                             doneBtn.frame =btnBounds;
                             
                         }];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
