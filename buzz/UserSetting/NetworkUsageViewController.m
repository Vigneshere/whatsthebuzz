//
//  NetworkUsageViewController.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 12/06/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "NetworkUsageViewController.h"
#define NetworkUsageArray @[@"Message sent:",@"Message received:"]

@interface NetworkUsageViewController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation NetworkUsageViewController
{
    
    UITableView *networkUsageTableView;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Network usage";

    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.
    networkUsageTableView = [UITableView new];
    networkUsageTableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    networkUsageTableView.delegate = self;
    networkUsageTableView.dataSource = self;
    networkUsageTableView.backgroundColor = [UIColor clearColor];
    networkUsageTableView.separatorColor = [UIColor clearColor];
    [self.view addSubview:networkUsageTableView];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}
-(void)backbuttonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Tableview DataSource AND Delgate Methods
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    return NetworkUsageArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"inviteSettingtableViewIdentifier"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"inviteSettingtableViewIdentifier"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(cell.frame.origin.x+10, 49, cell.frame.size.width-20, 1)];
        separatorLineView.backgroundColor = RGBA(204, 204, 204, 0.4);
        [cell.contentView addSubview:separatorLineView];
    }
    cell.textLabel.text = [NetworkUsageArray objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    cell.textLabel.textColor = RGBA(130, 130, 130, 1);
    cell.detailTextLabel.textColor = RGBA(130, 130, 130, 1);
    if (indexPath.row == 0)
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ message",[self messageCount:YES]];
    else
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ message",[self messageCount:NO]];
    return  cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(NSString *)messageCount:(BOOL)type
{
    XMPPMessageArchivingCoreDataStorage *storage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject"
                                                         inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
   NSString *predicateFrmt;
    if (type)//If YES :Get sent Message, NO:Get received message COUNT
        predicateFrmt = @"outgoing == yes";
    else
       predicateFrmt = @"outgoing == no";

    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt];
    [request setPredicate:predicate];
    [request setEntity:entityDescription];
    NSError *error;
    NSUInteger count = [moc countForFetchRequest:request error:&error];
    return [NSString stringWithFormat:@"%lu",(unsigned long)count];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
