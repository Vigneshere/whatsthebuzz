//
//  ImageDownloader.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 28/07/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "FileDownloader.h"


@interface FileDownloader ()
@property (nonatomic, readwrite, strong) NSIndexPath *indexPathInTableView;
@property (nonatomic, readwrite, strong) FileRecord *fileRecord;
@property (nonatomic, readwrite, strong) NSString *msgId;

@end


@implementation FileDownloader
@synthesize delegate = _delegate;
@synthesize indexPathInTableView = _indexPathInTableView;
@synthesize msgId = _msgId;

@synthesize fileRecord = _fileRecord;

#pragma mark -
#pragma mark - Life Cycle

- (id)initWithFileRecord:(FileRecord *)record atIndexPath:(NSIndexPath *)indexPath delegate:(id<FileDownloaderDelegate>)theDelegate {
    
    if (self = [super init]) {
        // 2: Set the properties.
        self.delegate = theDelegate;
        self.indexPathInTableView = indexPath;
        self.fileRecord = record;
    }
    return self;
}

- (id)initWithFileRecord:(FileRecord *)record atIndexPath:(NSIndexPath *)indexPath msgId:(NSString *)msgId delegate:(id<FileDownloaderDelegate>) theDelegate
{
  //  NSLog(@"record  ---   %@",record);
    if (self = [super init]) {
        // 2: Set the properties.
        self.delegate = theDelegate;
        self.msgId = msgId;
        self.indexPathInTableView = indexPath;
        self.fileRecord = record;
    }
    return self;
    
}
#pragma mark -
#pragma mark - Downloading image

- (void)main {
 
    @autoreleasepool {
        
        if (self.isCancelled)
            return;
        if ([self.fileRecord.type isEqualToString:@"image"]) {
            NSData *data = [NSData dataWithContentsOfURL:self.fileRecord.URL];
            
            if (self.isCancelled) {
                data = nil;
                return;
            }
            
            if (data) {
                UIImage *downloadedImage = [UIImage imageWithData:data];
                self.fileRecord.image = downloadedImage ;
            }
            else
                self.fileRecord.failed = YES;
            data = nil;

        }
        else if([self.fileRecord.type isEqualToString:@"video"])
        {
            self.fileRecord.image = [self constructImageFromVidUrl:self.fileRecord.URL];
            if (self.fileRecord.image == nil)
                return;
            else
                self.fileRecord.failed = YES;

        }
        else if([self.fileRecord.type isEqualToString:@"base64"])
        {
            self.fileRecord.image = [self decodeBase64ToImage:self.fileRecord.base64];
            if (self.fileRecord.image == nil)
                return;
            else
                self.fileRecord.failed = YES;
            
        }
       
        if (self.isCancelled)
            return;
        
        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(DownloaderDidFinish:) withObject:self waitUntilDone:NO];
        
    }
}

#pragma mark - Getting Single Image from Video File
-(UIImage*)constructImageFromVidUrl:(NSURL*)url
{
    AVAsset *asset = [AVAsset assetWithURL:url];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    CMTime time = CMTimeMake(1, 1);
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
    return thumbnail;
}

- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}
@end


