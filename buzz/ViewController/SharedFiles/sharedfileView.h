//
//  sharedfileView.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 23/07/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PendingOperations.h"
#import "FileDownloader.h"
#import "GroupInformationViewController.h"
#import "FetchsharedFile.h"
#import "FileRecord.h"
#import "SharedCollectionViewCell.h"

@protocol SharedFiledelegate <NSObject>

@optional
-(void)didSelectShareFile:(NSIndexPath *)indexpath Array:(NSMutableArray *)ary;
@end


@interface sharedfileView : UIView<UICollectionViewDataSource,UICollectionViewDelegate,FileDownloaderDelegate>
{
    
}
@property(nonatomic,weak)id<SharedFiledelegate>delegate;
@property(nonatomic,strong)NSMutableArray *photos;
@property (nonatomic, strong) PendingOperations *pendingOperations;
@property(nonatomic,strong)UICollectionView * collectionView;
-(void)getData:(XMPPJID *)roomjid;
- (void)cancelAllOperations;
@end
