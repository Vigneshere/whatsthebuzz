//
//  sharedfileView.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 23/07/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "sharedfileView.h"

@implementation sharedfileView

#pragma mark  -PendingOperations
- (PendingOperations *)pendingOperations {
    if (!_pendingOperations) {
        _pendingOperations = [[PendingOperations alloc] init];
    }
    return _pendingOperations;
}

- (id)initWithFrame:(CGRect)frame;
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor  = [UIColor clearColor];
        self.photos = [NSMutableArray new];
        UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
        _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height) collectionViewLayout:layout];
        [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        [_collectionView setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.0]];
        [_collectionView setDataSource:self];
        [_collectionView setDelegate:self];
        [_collectionView setShowsHorizontalScrollIndicator:NO];
        [_collectionView registerClass:[SharedCollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
        [self addSubview:_collectionView];
    }
    return self;
}


#pragma mark - CollectionView DataSource And Delegate Methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (!self.photos.count) {
        return 0;
    }
    return self.photos.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SharedCollectionViewCell *cell= (SharedCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    cell.backgroundColor=[UIColor clearColor];
    [[cell.imageview subviews] makeObjectsPerformSelector: @selector(removeFromSuperview)];
    FileRecord *aRecord = [self.photos objectAtIndex:indexPath.row];
    if (aRecord.hasFile) {
       
        cell.imageview.image =  [UIImage generatePhotoThumbnail:aRecord.image withSide:50];
        if ([aRecord.type isEqualToString:@"image"])
            [cell.imageview.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        else
            [cell.imageview addSubview:cell.blackTransVw];
        
    }
    else if (aRecord.isFailed)
        cell.imageview.image = [UIImage imageNamed:@"placeholder"];
    else {
         cell.imageview.image = [UIImage imageNamed:@"placeholder"];
        
        if (!collectionView.dragging && !collectionView.decelerating) {
            [self startImageDownloadingForRecord:aRecord atIndexPath:indexPath];
        }
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(50, 50);
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.delegate respondsToSelector:@selector(didSelectShareFile:Array:)])
        [self.delegate didSelectShareFile:indexPath Array:self.photos];
    
}

#pragma mark - Get Data from Database
-(void)getData:(XMPPJID *)roomjid
{
    [self.photos removeAllObjects];
    [[FetchsharedFile sharedMediaServer] fetchMediaFiles:roomjid.bare From:@"sharedFileView" block:^(NSArray *items, NSError *error) {
        if (!error && items.count)
        {
            for (NSDictionary *dic in items)
            {
              //  NSNumber*boolean = [dic objectForKey:@"isPrivate"];
                
                NSNumber * boolean = (NSNumber *)[dic objectForKey:@"isPrivate"];
                if([boolean boolValue] == YES)
                {
                  //  NSLog(@"YES --->> isPrivate");
                    // this is the YES case
                }
                else
                {
                 //   NSLog(@"NO --->> isPrivate");
                    
                    FileRecord *record = [[FileRecord alloc] init];
                    record.URL = [NSURL fileURLWithPath:[self getFullDocumentUrl:[dic objectForKey:@"url"]]];
                    record.type = [dic objectForKey:@"video_img"];
                    record.isPrivate = NO;
                    record.msgId = [dic objectForKey:@"msgId"];
                    [self.photos addObject:record];
                    record = nil;

                }
                
                if (boolean)
                {
                  
                }
             
            }
            [_collectionView reloadData];
            
        }
        
    }];
}

- (NSString *)getFullDocumentUrl:(NSString *)fileName
{
    
    NSRange range = [fileName rangeOfString:@"Documents"];
    
    NSString *newString = [fileName substringFromIndex:range.location+9];
  //  NSLog(@"new string %@",newString);
    
    return [NSString stringWithFormat:@"%@/%@",[self applicationDocumentsDirectory],newString];
}

- (NSString *)applicationDocumentsDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}


- (void)startImageDownloadingForRecord:(FileRecord *)record atIndexPath:(NSIndexPath *)indexPath {
    if (![self.pendingOperations.downloadsInProgress.allKeys containsObject:indexPath]) {
        FileDownloader *imageDownloader = [[FileDownloader alloc] initWithFileRecord:record atIndexPath:indexPath delegate:self];
        [self.pendingOperations.downloadsInProgress setObject:imageDownloader forKey:indexPath];
        [self.pendingOperations.downloadQueue addOperation:imageDownloader];
    }
}

#pragma mark - ImageDownloader delegate
- (void)DownloaderDidFinish:(FileDownloader *)downloader {
    NSIndexPath *indexPath = downloader.indexPathInTableView;
    FileRecord *theRecord = downloader.fileRecord;
    [self.photos replaceObjectAtIndex:indexPath.row withObject:theRecord];
    [_collectionView reloadItemsAtIndexPaths:@[indexPath]];
    [self.pendingOperations.downloadsInProgress removeObjectForKey:indexPath];
}

#pragma mark - UIScrollView delegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    // 1: As soon as the user starts scrolling, you will want to suspend all operations and take a look at what the user wants to see.
    [self suspendAllOperations];
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    // 2: If the value of decelerate is NO, that means the user stopped dragging the table view. Therefore you want to resume suspended operations, cancel operations for offscreen cells, and start operations for onscreen cells.
    if (!decelerate) {
        [self loadImagesForOnscreenCells];
        [self resumeAllOperations];
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    // 3: This delegate method tells you that table view stopped scrolling, so you will do the same as in #2.
    [self loadImagesForOnscreenCells];
    [self resumeAllOperations];
}

#pragma mark - Cancelling, suspending, resuming queues / operations
- (void)suspendAllOperations {
    [self.pendingOperations.downloadQueue setSuspended:YES];
}
- (void)resumeAllOperations {
    [self.pendingOperations.downloadQueue setSuspended:NO];
}
- (void)cancelAllOperations {
    [self.pendingOperations.downloadQueue cancelAllOperations];
}
- (void)loadImagesForOnscreenCells {
    
    // 1: Get a set of visible rows.
    NSSet *visibleRows = [NSSet setWithArray:[_collectionView indexPathsForVisibleItems]];
    
    // 2: Get a set of all pending operations (download and filtration).
    NSMutableSet *pendingOperations = [NSMutableSet setWithArray:[self.pendingOperations.downloadsInProgress allKeys]];
    
    NSMutableSet *toBeCancelled = [pendingOperations mutableCopy];
    NSMutableSet *toBeStarted = [visibleRows mutableCopy];
    
    // 3: Rows (or indexPaths) that need an operation = visible rows ñ pendings.
    [toBeStarted minusSet:pendingOperations];
    
    // 4: Rows (or indexPaths) that their operations should be cancelled = pendings ñ visible rows.
    [toBeCancelled minusSet:visibleRows];
    
    // 5: Loop through those to be cancelled, cancel them, and remove their reference from PendingOperations.
    for (NSIndexPath *anIndexPath in toBeCancelled) {
        
        FileDownloader *pendingDownload = [self.pendingOperations.downloadsInProgress objectForKey:anIndexPath];
        [pendingDownload cancel];
        [self.pendingOperations.downloadsInProgress removeObjectForKey:anIndexPath];
    }
    toBeCancelled = nil;
    
    // 6: Loop through those to be started, and call startOperationsForFileRecord:atIndexPath: for each.
    for (NSIndexPath *anIndexPath in toBeStarted) {
        
        FileRecord *recordToProcess = [self.photos objectAtIndex:anIndexPath.row];
        if (!recordToProcess.image) {
            [self startImageDownloadingForRecord:recordToProcess atIndexPath:anIndexPath];
        }
    }
    toBeStarted = nil;
    
}


@end
