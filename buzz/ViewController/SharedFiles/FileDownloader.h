//
//  ImageDownloader.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 28/07/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

// 1: Import FileRecord.h so that you can independently set the image property of a FileRecord once it is successfully downloaded. If downloading fails, set its failed value to YES.
#import "FileRecord.h"

// 2: Declare a delegate so that you can notify the caller once the operation is finished.
@protocol FileDownloaderDelegate;

@interface FileDownloader : NSOperation

@property (nonatomic, assign) id <FileDownloaderDelegate> delegate;

// 3: Declare indexPathInTableView for convenience so that once the operation is finished, the caller has a reference to where this operation belongs to.
@property (nonatomic, readonly, strong) NSIndexPath *indexPathInTableView;
@property (nonatomic, readonly, strong) FileRecord *fileRecord;
@property (nonatomic, readonly, strong) NSString *msgId;


// 4: Declare a designated initializer.
- (id)initWithFileRecord:(FileRecord *)record atIndexPath:(NSIndexPath *)indexPath delegate:(id<FileDownloaderDelegate>) theDelegate;
- (id)initWithFileRecord:(FileRecord *)record atIndexPath:(NSIndexPath *)indexPath msgId:(NSString *)msgId delegate:(id<FileDownloaderDelegate>) theDelegate;

@end

@protocol FileDownloaderDelegate <NSObject>

// 5: In your delegate method, pass the whole class as an object back to the caller so that the caller can access both indexPathInTableView and FileRecord. Because you need to cast the operation to NSObject and return it on the main thread, the delegate method can�t have more than one argument.
- (void)DownloaderDidFinish:(FileDownloader *)downloader;
@end