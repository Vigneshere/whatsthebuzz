//
//  ViewController.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 18/02/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XMPPRoomCoreDataStorage.h"
#import "MSSlidingPanelController.h"
#import "RecentChatTableView.h"
#import "ACStatusBarOverlayWindow.h"

@interface ViewController : UIViewController<MSSlidingPanelControllerDelegate,ABUnknownPersonViewControllerDelegate>

@property (nonatomic, retain) ACStatusBarOverlayWindow* overlayWindow;
@property (nonatomic, retain) RecentChatTableView *recentChatTableview ;


@end
