//
//  MasterPinView.h
//  buzz
//
//  Created by Vignesh on 05/05/15.
//  Copyright (c) 2015 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PasswordPin.h"

@interface MasterPinView : UIView<UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    UITextField *keyTextField;
    NSArray  *securityQuestionListArr;
    UIView *securityQuestionView;
    UITextField  *pickerTxtField;
    UIButton  *securitySubmitBtn;
    UIPickerView  *selectionPickr;
    BOOL pickerSelected;
    UITapGestureRecognizer *tapToHide;
    NSString *entryStr;
    NSString *titleString;
    PasswordPin* passwordVw;


}
- (id)initWithFrame_onView:(UIView*)onview Title:(NSString*)titleStr Entry:(NSString *)Password;
-(void)createMasterView;

@property (strong, nonatomic) UIView *onView;


@end
