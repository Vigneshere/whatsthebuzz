//
//  FavoriteTableViewController.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 15/04/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "FavoriteTableViewController.h"
#import "MSViewControllerSlidingPanel.h"
#import "ContactCell.h"
#import "AvatarImage.h"
#import "chatViewController.h"

@interface FavoriteTableViewController ()<NSFetchedResultsControllerDelegate>
{
    NSFetchedResultsController *fetchedResultsController;
}
@end


@implementation FavoriteTableViewController
{
    UISwipeGestureRecognizer *swipeRight;
    AvatarImage *avatarImg;
    
}
@synthesize favoriteDelegate;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    if(IS_IPHONE_4)
    {
        self.parentViewController.view.backgroundColor = BACKGROUNDIMAGE_4;
        
    }
    else
    {
        self.parentViewController.view.backgroundColor = BACKGROUNDIMAGE;
        
    }
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator  = NO;
    swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    [swipeRight setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [[self view] addGestureRecognizer:swipeRight];
    self.title = @"Favorites";
    avatarImg = [AvatarImage new];
    
    
}
-(void)handleSwipeFrom:(UISwipeGestureRecognizer *)gesture
{
    [[self slidingPanelController] closePanel];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self fetchedResultsController].fetchedObjects.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 53;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"favoritecell";
    
    ContactCell *cell = (ContactCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        cell = [[ContactCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    XMPPUserCoreDataStorageObject *user;
    user  = [[self fetchedResultsController] objectAtIndexPath:indexPath];
    
    cell.contactNameLbl.text = user.displayName;
    cell.contactImageView.image = [avatarImg profileImage:user];
    [cell.favoriteButton removeFromSuperview];
    [cell.checkView removeFromSuperview];
    XMPPResourceCoreDataStorageObject *userResource;
    if ([user isOnline]) {
        for (userResource in user.resources) {
            
            if (userResource.status != nil)
                cell.statusLabel.text = userResource.status;
            else
                cell.statusLabel.text = userResource.type;
        }
    }
    else
        cell.statusLabel.text = @"unavailable";
    return cell;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - TableView Editing Method
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        XMPPUserCoreDataStorageObject *user = [[self fetchedResultsController] objectAtIndexPath:indexPath];
        [self updateFavorite:user.jid favorite:NO completion:^(BOOL compeleted)
         {
             if (compeleted)
             {
                 
             }
         }];
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - TableView Delegate didSelectRowAtIndexPath
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    XMPPUserCoreDataStorageObject *user = [[self fetchedResultsController] objectAtIndexPath:indexPath];

    [[self slidingPanelController] closePanelWithCompletion:^{
        NSMutableDictionary *dict = [NSMutableDictionary new];
        
        if (dict) {
            
            [dict setObject:user forKey:@"userdetails"];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TEMPNOTI" object:nil userInfo:dict];
            
            
        }

    }];

    


}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark DataBase Methods For Contacts
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (NSFetchedResultsController *)fetchedResultsController
{
    if (fetchedResultsController != nil) {
        return  fetchedResultsController;
    }
    NSManagedObjectContext *moc = [self  managedObjectContext_roster];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:moc];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:[NSSortDescriptor sortValues:@"displayName" ascending:YES]];
    NSString *fr = @"from";
    NSString *predicateFrmt = @"subscription != %@ AND isFavorite == YES";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, fr];
    [fetchRequest setPredicate:predicate];
    fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:moc  sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsController setDelegate:self];
    
    NSError *error = nil;
    if (![fetchedResultsController performFetch:&error])
    {
    }
	return fetchedResultsController;
}
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    
    [self.tableView beginUpdates];
}
- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    switch(type)
    {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
            
        case NSFetchedResultsChangeMove:
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
    }
}
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Core Data
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (NSManagedObjectContext *)managedObjectContext_roster
{
	return [[[self appDelegate] xmppRosterStorage] mainThreadManagedObjectContext];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Appdelegate Values
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (AppDelegate *)appDelegate
{
	return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (XMPPRosterCoreDataStorage *)xmppRosterStorage
{
	return [[self appDelegate] xmppRosterStorage];
}
- (XMPPStream *)xmppStream
{
	return [[self appDelegate] xmppStream];
}
- (XMPPRoster *)xmppRoster
{
	return [[self appDelegate] xmppRoster];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Method to update unreaded message for user
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)updateFavorite:(XMPPJID *)user favorite:(BOOL)isfavorite completion:(void (^)(BOOL))completed
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:[self managedObjectContext_roster]];
    [request setEntity:entity];
    NSString *predicateFrmt = @"jidStr = %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, [user bareJID]];
    [request setPredicate:predicate];
    NSArray *empArray=[[self managedObjectContext_roster] executeFetchRequest:request error:nil];
    if ([empArray count] == 1){
        for (XMPPUserCoreDataStorageObject *user in  empArray)
        {
            if (isfavorite)
                
                user.isFavorite = YES;
            else
                user.isFavorite = NO;
            user.timestamp = [NSDate date];
        }
    }
    else
    {
      //  NSLog(@"Nothin");
    }
    if ([[self managedObjectContext_roster] save:nil]) {
        
        completed(YES);
    }
}

@end
