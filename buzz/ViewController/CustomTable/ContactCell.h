//
//  ContactCell.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 18/02/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactCell : UITableViewCell
{
    DotLineView *line;
}
@property(nonatomic,retain)UIImageView *contactImageView;
@property(nonatomic,retain)UILabel *contactNameLbl;
@property(nonatomic,retain)UILabel *unreadMessageLabel;
@property(nonatomic,retain)UIView *checkView;
@property(nonatomic,retain)UILabel *statusLabel;
@property(nonatomic,strong)UIButton *favoriteButton;

@end
