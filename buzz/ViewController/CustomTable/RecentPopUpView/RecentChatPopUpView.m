//
//  RecentChatPopUpView.m
//  buzz
//
//  Created by Shriram Rajendran on 21/04/15.
//  Copyright (c) 2015 Shailisolutions. All rights reserved.
//

#import "RecentChatPopUpView.h"

@implementation RecentChatPopUpView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (id)initWithFrame:(CGRect)frame view:(UIView *)view contactData:(NSArray *)ContactData
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = RGBA(0, 0, 0, 0.0);
        baseView = view;
        _optionsData = ContactData;
        
        
        
        
        [baseView addSubview:self];
        
        [self createVw];
        
    }
    return self;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45.0f;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_optionsData count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.userInteractionEnabled  = YES;
        
        UILabel *optionsLbl = [UILabel new];
        optionsLbl.frame = CGRectMake(0, cell.frame.origin.y+10, cell.contentView.frame.size.width, 30);
        optionsLbl.backgroundColor = [UIColor clearColor];
        optionsLbl.textColor =[UIColor yellowColor];
        optionsLbl.textAlignment = NSTextAlignmentLeft;
        optionsLbl.font = [UIFont fontWithName:FontString size:11];
        optionsLbl.tag = 2;
        [cell.contentView addSubview:optionsLbl];
        
        cell.textLabel.text = _optionsData[indexPath.row];
    }
    
    UILabel *optnLbl = (UILabel *)[cell viewWithTag:1];
    
    optnLbl.text = _optionsData[indexPath.row];
    
    
    return cell;
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@" %ld",(long)indexPath.row);
    
    [_Delegate PopUpdidSelectItem:indexPath popUpcoreMessage:_popUpcoreMessage Action:[_optionsData objectAtIndex:indexPath.row]];
    [self performSelector:@selector(tapPress:) withObject:self afterDelay:0.2];
    
    
}

- (void) attachPopUpAnimation:(UIView *)view
{
    CAKeyframeAnimation *animation = [CAKeyframeAnimation
                                      animationWithKeyPath:@"transform"];
    
    CATransform3D scale1 = CATransform3DMakeScale(0.5, 0.5, 1);
    CATransform3D scale2 = CATransform3DMakeScale(1.2, 1.2, 1);
    CATransform3D scale3 = CATransform3DMakeScale(0.9, 0.9, 1);
    CATransform3D scale4 = CATransform3DMakeScale(1.0, 1.0, 1);
    
    NSArray *frameValues = [NSArray arrayWithObjects:
                            [NSValue valueWithCATransform3D:scale1],
                            [NSValue valueWithCATransform3D:scale2],
                            [NSValue valueWithCATransform3D:scale3],
                            [NSValue valueWithCATransform3D:scale4],
                            nil];
    [animation setValues:frameValues];
    
    NSArray *frameTimes = [NSArray arrayWithObjects:
                           [NSNumber numberWithFloat:0.0],
                           [NSNumber numberWithFloat:0.5],
                           [NSNumber numberWithFloat:0.9],
                           [NSNumber numberWithFloat:1.0],
                           nil];
    [animation setKeyTimes:frameTimes];
    
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    animation.duration = .2;
    
    [view.layer addAnimation:animation forKey:@"popup"];
}


- (void)tapPress:(UITapGestureRecognizer*)gesture
{
    
    self.backgroundColor = [UIColor clearColor];

    
    [UIView transitionWithView:self.superview
                      duration:0.2
                       options:UIViewAnimationOptionTransitionNone
                    animations:^{
                        self.transform =  CGAffineTransformMakeScale(0.3, 0.3);
                        self.backgroundColor = RGBA(0, 0, 0, 0);
                        
                        
                    } completion:^(BOOL finished)
     
     {
         [self removeFromSuperview];
     }];
}

-(void)createVw
{
    
    
    tapVw.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    tapVw.backgroundColor = [UIColor clearColor];
    [self addSubview:tapVw];
    
    UITapGestureRecognizer *tapPress = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapPress:)];
    tapPress.delegate = self;
    [self addGestureRecognizer:tapPress];
    
    optionsTblVw = [[UITableView alloc]initWithFrame:CGRectMake(20, (self.frame.size.height/2) - (_optionsData.count*45/2), self.frame.size.width - (20*2), _optionsData.count*45) style:UITableViewStylePlain];
    optionsTblVw.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.8];
    optionsTblVw.separatorInset = UIEdgeInsetsZero;
    optionsTblVw.delegate = self;
    optionsTblVw.dataSource = self;
    optionsTblVw.bounces = NO;
    [optionsTblVw.layer setCornerRadius:5.0f];
    optionsTblVw.clipsToBounds = YES;
    optionsTblVw.userInteractionEnabled = YES;
    
    [self addSubview:optionsTblVw];
    [self attachPopUpAnimation:optionsTblVw];
    
    [UIView transitionWithView:self.superview
                      duration:0.3
                       options:UIViewAnimationOptionTransitionNone
                    animations:^{
                        self.backgroundColor = RGBA(0, 0, 0, 0.3);
                        
                        
                    } completion:nil];
}

- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if (CGRectContainsPoint(optionsTblVw.bounds, [touch locationInView:optionsTblVw]))
        return NO;
    
    return YES;
}



@end
