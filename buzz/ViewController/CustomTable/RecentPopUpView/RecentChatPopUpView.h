//
//  RecentChatPopUpView.h
//  buzz
//
//  Created by Shriram Rajendran on 21/04/15.
//  Copyright (c) 2015 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol PopUpTabledelegate <NSObject>
-(void)PopUpdidSelectItem:(NSIndexPath *)indexpath popUpcoreMessage:(XMPPMessageArchiving_Contact_CoreDataObject *)coreMessage Action:(NSString *)actionString;
@end

@interface RecentChatPopUpView : UIView <UITableViewDataSource , UITableViewDelegate ,UIGestureRecognizerDelegate>
{
    UIView *baseView,*tapVw;
    UITableView *optionsTblVw;
}
@property (nonatomic,strong)NSArray *optionsData;
@property (nonatomic,assign) id<PopUpTabledelegate> Delegate;
@property (nonatomic,assign)XMPPMessageArchiving_Contact_CoreDataObject *popUpcoreMessage;


- (id)initWithFrame:(CGRect)frame view:(UIView *)view contactData:(NSArray *)contactData;




@end
