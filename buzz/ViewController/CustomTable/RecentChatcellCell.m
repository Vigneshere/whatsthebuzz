//
//  RecentChatcellCell.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 06/03/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "RecentChatcellCell.h"

@implementation RecentChatcellCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // Initialization code
        
        self.contactImageView = [UIImageView new];
        self.contactImageView.frame = CGRectMake(self.contentView.frame.origin.x+10, self.contentView.frame.origin.y+5, 40, 40);
        self.contactImageView.layer.cornerRadius = 20;
        self.contactImageView.clipsToBounds = YES;
        [self.contentView addSubview:self.contactImageView];
        
        self.contactNameLbl = [UILabel new];
        self.contactNameLbl.font = [UIFont fontWithName:FontString size:15];
        self.contactNameLbl.frame = CGRectMake(self.contactImageView.frame.origin.x+self.contactImageView.frame.size.width+20, self.frame.origin.y+5, 220, self.contactNameLbl.font.lineHeight);
        self.contactNameLbl.backgroundColor = [UIColor clearColor];
        self.contactNameLbl.textColor = RGBA(0, 0,0, 1);
        self.contactNameLbl.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:self.contactNameLbl];
        
        self.msgLabel = [UILabel new];
        self.msgLabel.font = [UIFont fontWithName:FontString size:11];
        self.msgLabel.frame = CGRectMake(self.contactNameLbl.frame.origin.x, self.contactNameLbl.frame.origin.y+self.contactNameLbl.frame.size.height, 170, self.msgLabel.font.lineHeight);
        self.msgLabel.backgroundColor = [UIColor clearColor];
        self.msgLabel.textColor = RGBA(102,102,102, 1);
        [self.contentView addSubview:self.msgLabel];
        
        self.unreadMessageLabel = [UILabel new];
        self.unreadMessageLabel.frame = CGRectMake(self.contactImageView.frame.origin.x+self.contactImageView.frame.size.width-8, self.contactImageView.frame.origin.y-5, 14, 14);
        self.unreadMessageLabel.backgroundColor = [UIColor redColor];
        self.unreadMessageLabel.textColor = RGBA(136, 141, 147, 1);
        self.unreadMessageLabel.layer.cornerRadius = 7;
        self.unreadMessageLabel.font = [UIFont fontWithName:FontString size:9];
        self.unreadMessageLabel.textColor = [UIColor whiteColor];
        self.unreadMessageLabel.layer.masksToBounds = YES;
        self.unreadMessageLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.unreadMessageLabel];
        
        
        self.dateLabel = [UILabel new];
        self.dateLabel.font = [UIFont fontWithName:FontString size:11];
        self.dateLabel.frame = CGRectMake(self.contentView.frame.size.width-70, self.contactNameLbl.frame.origin.y+self.contactNameLbl.frame.size.height, 70, self.dateLabel.font.lineHeight);
        self.dateLabel.backgroundColor = [UIColor clearColor];
        self.dateLabel.textColor = RGBA(102,102,102, 1);
        self.dateLabel.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:self.dateLabel];
        
        UIImage* callImg = [UIImage imageNamed:@"call"];
        self.callBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.callBtn.frame = CGRectMake((((self.contentView.frame.size.width/3) - callImg.size.width)/2), 55 + 2.5, callImg.size.width, callImg.size.height);
        [self.callBtn setImage:callImg forState:UIControlStateNormal];
        [self.contentView addSubview:self.callBtn];

        UIImage* msgImg = [UIImage imageNamed:@"msg"];
        self.chatBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.chatBtn.frame = CGRectMake((self.contentView.frame.size.width/3) + (((self.contentView.frame.size.width/3) - msgImg.size.width)/2), 55 + 2.5, msgImg.size.width, msgImg.size.height);
        [self.chatBtn setImage:msgImg forState:UIControlStateNormal];
        [self.contentView addSubview:self.chatBtn];
        
        UIImage* profImg = [UIImage imageNamed:@"profile"];
        self.profileBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.profileBtn.frame = CGRectMake((self.contentView.frame.size.width/3) + (((self.contentView.frame.size.width/3) - profImg.size.width)/2), 55 + 2.5, profImg.size.width, profImg.size.height);
        [self.profileBtn setImage:profImg forState:UIControlStateNormal];
        [self.contentView addSubview:self.profileBtn];
        
  
            UIImage *starImg = [UIImage imageNamed:@"impMsgTV"];
            _tagImg = [UIImageView new];
            _tagImg.image = starImg;
             _tagImg.frame = CGRectMake(self.contentView.frame.size.width-(70 + starImg.size.width/2 + 5), self.contactNameLbl.frame.origin.y+self.contactNameLbl.frame.size.height, starImg.size.width/2, starImg.size.height/2);
        
            
            [self.contentView addSubview:_tagImg];
        
       
        

     //  self.lineVw = [DotLineView new];
        

    }
    return self;
}

-(void)startAnimationBatch
{
    [UIView animateKeyframesWithDuration:0.3 delay:0.05 options: UIViewKeyframeAnimationOptionRepeat  animations:^{
        
        _tagImg.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);

            [UIView animateWithDuration:0.3/1.5 animations:^{
                _tagImg.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.3/2 animations:^{
                    _tagImg.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.3/2 animations:^{
                        _tagImg.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
                    }];
                }];
            }];

        
    } completion:^(BOOL finished) {
        
        _tagImg.transform = CGAffineTransformIdentity;


    }];
    

    
}

- (void)layoutSubviews
{
    [super layoutSubviews];

    CGRect contentViewFrame = self.contentView.frame;
    CGRect screenRect = [[UIScreen mainScreen] bounds];

    contentViewFrame.size.width = screenRect.size.width;
    self.contentView.frame = contentViewFrame;
    
    self.contactImageView.frame = CGRectMake(self.contentView.frame.origin.x+10, self.contentView.frame.origin.y+5, 40, 40);
    self.unreadMessageLabel.frame = CGRectMake(self.contactImageView.frame.origin.x+self.contactImageView.frame.size.width-8, self.contactImageView.frame.origin.y-5, 14, 14);


    self.contactNameLbl.frame = CGRectMake(self.contactImageView.frame.origin.x+self.contactImageView.frame.size.width+20, self.contentView.frame.origin.y+5, 220, self.contactNameLbl.font.lineHeight);
    self.msgLabel.frame = CGRectMake(self.contactNameLbl.frame.origin.x, self.contactNameLbl.frame.origin.y+self.contactNameLbl.frame.size.height, 170, self.msgLabel.font.lineHeight);
    self.dateLabel.frame = CGRectMake(self.contentView.frame.size.width-70, self.contactNameLbl.frame.origin.y+self.contactNameLbl.frame.size.height, 70, self.dateLabel.font.lineHeight);



    self.dateLabel.frame = CGRectMake(self.contentView.frame.size.width-70, self.contactNameLbl.frame.origin.y+self.contactNameLbl.frame.size.height, 70, self.dateLabel.font.lineHeight);

    UIImage* msgImg = [UIImage imageNamed:@"msg"];
    self.chatBtn.frame = CGRectMake((self.contentView.frame.size.width/3) + (((self.contentView.frame.size.width/3) - msgImg.size.width)/2), 55 + 2.5, msgImg.size.width, msgImg.size.height);
    
    UIImage* profImg = [UIImage imageNamed:@"profile"];
      self.profileBtn.frame = CGRectMake((self.contentView.frame.size.width/3) + (((self.contentView.frame.size.width/3) - profImg.size.width)/2), 55 + 2.5, profImg.size.width, profImg.size.height);
    
    UIImage *starImg = [UIImage imageNamed:@"impMsgTV"];
    _tagImg.frame = CGRectMake(self.contentView.frame.size.width-(70 + starImg.size.width/2 + 5), self.contactNameLbl.frame.origin.y+self.contactNameLbl.frame.size.height, starImg.size.width/2, starImg.size.height/2);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
