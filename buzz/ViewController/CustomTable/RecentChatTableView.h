//
//  RecentChatTableView.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 28/05/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AvatarImage.h"
#import "ProfileViewController.h"
#import "RecentChatPopUpView.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>




@class AvatarImage;
@protocol RecentChatTabledelegate <NSObject>
- (void)didSelectRecentChat:(XMPPUserCoreDataStorageObject *)user;
- (void)didSelectRecentGroupChat:(XMPPJID *)user;
- (void)didSelectContacts:(XMPPUserCoreDataStorageObject *)userdetails;
-(void)didSelectGroup:(XMPPJID *)roomJID :(NSString *)roomTitle;
-(void)didSelectUnkownContact:(XMPPMessageArchiving_Contact_CoreDataObject *)coreMessage;


@end
@interface RecentChatTableView : UITableView<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,NSFetchedResultsControllerDelegate,PopUpTabledelegate,UIAlertViewDelegate>
{
    AvatarImage *avatarImg;
    BOOL isSearching;
    NSFetchedResultsController *fetchedResultsController;
    NSFetchedResultsController *searchFetchControl;
    NSPredicate *contactPredicate;
    NSIndexPath *selectedPrifilePic;
    NSIndexPath *prevSelPrifilePic;
    NSIndexPath *quickMenuSelectedIndex;
    


}
@property(nonatomic,strong)UISearchBar *searchBar;
@property (nonatomic, assign) id<RecentChatTabledelegate> recentChatdelegate;
@property (nonatomic, assign) ABAddressBookRef addressBook;




@end