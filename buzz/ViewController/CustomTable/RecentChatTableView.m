//
//  RecentChatTableView.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 28/05/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "RecentChatTableView.h"
#import "RecentChatcellCell.h"
#import "quickMenu.h"
#import "NSString+FindStickerName.h"
#import "mediaDbEditor.h"

@implementation RecentChatTableView
{
    UIImageView  *tableEmptyAlertImgViw;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.delegate = self;
        self.dataSource = self;
        self.showsVerticalScrollIndicator = NO;
        
        UIRefreshControl *refreshControl = [UIRefreshControl new];
        [refreshControl addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
        [refreshControl setTintColor:[UIColor colorWithRed:255.0f/255.0f green:55.0f/255.0f blue:152.0f/255.0f alpha:1.0]];
        
        [self addSubview:refreshControl];
        
        //-->Search Bar for TableView
        _searchBar = [UISearchBar new];
        _searchBar.frame = CGRectMake(0, 0, self.frame.size.width, 50);
        _searchBar.delegate = self;
        [_searchBar sizeToFit];
        _searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
        _searchBar.backgroundColor = [UIColor clearColor];
        _searchBar.placeholder = @"Find your Buddies..";
        _searchBar.tintColor = RGBA(53, 152, 219, 1);
        _searchBar.backgroundImage = [UIImage imageWithColor:[UIColor clearColor]];
        
        NSString *ver = [[UIDevice currentDevice] systemVersion];
        int ver_int = [ver intValue];
        
        if (ver_int < 7) {
            _searchBar.barStyle = UIBarStyleDefault;
        }
        
        else {
            _searchBar.searchBarStyle = UISearchBarStyleProminent;
            _searchBar.barTintColor =[UIColor clearColor];
        }
        _searchBar.translucent = YES;
        
        UITextField *txfSearchField = [_searchBar valueForKey:@"_searchField"];
        txfSearchField.layer.cornerRadius = 13;
        txfSearchField.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.7];
        
        
        self.tableHeaderView = _searchBar;
        avatarImg = [AvatarImage new];
        isSearching = NO;
        [[self xmppStream] addDelegate:self delegateQueue:dispatch_get_main_queue()];
        
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(quickMenuClicked:) name:QUICK_MENU_FOR_RECENT object:nil];
    
    return self;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITableView datasources Methods
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!isSearching)
    {
        tableEmptyAlertImgViw.hidden = YES;
        
        return [self fetchedResultsController].fetchedObjects.count;
    }
    else
    {
        
        if ([self searchFetchControl:contactPredicate].fetchedObjects.count == 0)
        {
            
            tableEmptyAlertImgViw  = [[UIImageView alloc]initWithFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height)];
            tableEmptyAlertImgViw.hidden = NO;
            
            //** No Orders to Display Label
            
            UILabel *noOrderToDisplayLabl = [[UILabel alloc]initWithFrame:CGRectMake(tableEmptyAlertImgViw.frame.origin.x , tableEmptyAlertImgViw.frame.origin.y + 200, tableEmptyAlertImgViw.frame.size.width, 75)];
            noOrderToDisplayLabl.textColor = [UIColor lightGrayColor];
            noOrderToDisplayLabl.text = @"No Matches Found..";
            noOrderToDisplayLabl.textAlignment = NSTextAlignmentCenter;
            [tableEmptyAlertImgViw addSubview:noOrderToDisplayLabl];
            
            self.backgroundView = tableEmptyAlertImgViw;
        }
        
        return [self searchFetchControl:contactPredicate].fetchedObjects.count;
    }
    
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (selectedPrifilePic == [NSIndexPath indexPathForItem:(long)indexPath.row inSection:(long)indexPath.section]) {
        return  110;
    }
    else
    {
        return 55;
    }
    
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITableView Delegate Methods
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    RecentChatcellCell *cell = (RecentChatcellCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    [cell setExclusiveTouch:YES];
    [cell.contentView setExclusiveTouch:YES];
    
    if (cell == nil)
    {
        cell = [[RecentChatcellCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    [self configurecell:cell Indexpath:indexPath];
    return cell;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Configuring Cell
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(void)configurecell:(RecentChatcellCell *)cell Indexpath:(NSIndexPath *)indexpath
{
    XMPPMessageArchiving_Contact_CoreDataObject *coreMessage;
    if (!isSearching)
        coreMessage =[[self fetchedResultsController] objectAtIndexPath:indexpath];
    else
        coreMessage =[[self searchFetchControl:contactPredicate] objectAtIndexPath:indexpath];
    
    
    XMPPUserCoreDataStorageObject *userdetails = [self RecentcontactsFetch:coreMessage.bareJid];
    
    
    XMPPMessageArchiving_Message_CoreDataObject *lastMessage = [self MsgWithJID:coreMessage.bareJidStr];
    
    NSXMLElement *private = [lastMessage.message elementForName:@"private"];
    NSXMLElement *ruder = [lastMessage.message elementForName:@"ruthere"];
    NSXMLElement *file = [lastMessage.message elementForName:@"file"];
    NSXMLElement *imp = [lastMessage.message elementForName:@"imp"];
    
    //  NSLog(@"Private is %@ , ruthere is %@ ,file is %@ , Imp is %@",private,ruder,file,imp);
    
    
    
    NSString *unreadmsgStr = [NSString stringWithFormat:@"%@",coreMessage.unreadedMsg];
    
    if ([coreMessage.type isEqualToString:@"groupChat"])
    {
        cell.contactImageView.image = [avatarImg groupProfileimage:coreMessage.bareJid];
        cell.contactNameLbl.text = coreMessage.displayName;
        
    }
    else if([coreMessage.type isEqualToString:@"singleChat"])
    {
        cell.contactImageView.image = [avatarImg groupProfileimage:coreMessage.bareJid];
        cell.contactNameLbl.text  = coreMessage.displayName;
        
        if(userdetails)
        {
            if (userdetails.displayName != NULL)
            {
                cell.contactNameLbl.text = userdetails.displayName;
                //  NSLog(@"Display Name :: %@",userdetails.displayName);
            }
            else
            {
                cell.contactNameLbl.text = coreMessage.displayName;
                // NSLog(@"User Details Name :: %@",coreMessage.displayName);
                
            }
        }
        else
        {
            
            double delayInSeconds = 1.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void)
                           {
                               XMPPMessageArchivingCoreDataStorage *storage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
                               
                               NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
                               [moc performBlockAndWait:^{
                                   [moc deleteObject:coreMessage];
                                   [moc save:nil];
                               }];
                               
                            });

            
            
            
        }
    }
    
    UITapGestureRecognizer *tapPress = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPress:)];
    cell.contactImageView.userInteractionEnabled = YES;
    [cell.contactImageView addGestureRecognizer:tapPress];
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
    cell.userInteractionEnabled = YES;
    longPress.minimumPressDuration = 1.0; //seconds
    [cell addGestureRecognizer:longPress];
    
    UITapGestureRecognizer *cellTapPress = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cellTapPress:)];
    cell.userInteractionEnabled = YES;
    [cell addGestureRecognizer:cellTapPress];
    
    
    cell.dateLabel.text = [self datetoString:coreMessage.mostRecentMessageTimestamp];
    NSArray *beginMatch;
    
    if (coreMessage.mostRecentMessageBody.length != 0)
    {
        
        
        
        NSArray *filterArray = [NSArray arrayWithObject:coreMessage.mostRecentMessageBody];
        beginMatch = [filterArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self BEGINSWITH[cd] %@", @"sticker_"]];
    }
    
    if(beginMatch.count == 1)
    {
        // NSLog(@"Stickers : %@",coreMessage.mostRecentMessageBody);
        cell.msgLabel.text = [NSString findStickerName:coreMessage.mostRecentMessageBody];
    }
    //    else if ([coreMessage.mostRecentMessageBody isEqualToString:BuzzStr])
    //    {
    //        cell.msgLabel.textColor = [UIColor greenColor];
    //        cell.msgLabel.text = coreMessage.mostRecentMessageBody;
    //
    //    }
    else
    {
        NSData *newdata=[coreMessage.mostRecentMessageBody dataUsingEncoding:NSUTF8StringEncoding
                                                        allowLossyConversion:YES];
        NSString *Bubblestring=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];
        
        if (file)
        {
            // NSString *fileType = [[file attributeForName:@"file_type"] stringValue];
            
            NSMutableDictionary *mediaDict =  [[mediaDbEditor sharedInstance] fetchMedia:lastMessage.messageID];
            
            if ([[mediaDict objectForKey:@"video_img"]  isEqualToString:@"image"])
            {
                cell.msgLabel.text =  [NSString stringWithFormat:@"📷 Image"];
            }
            else
            {
                cell.msgLabel.text =  [NSString stringWithFormat:@"📹 Video"];
            }
        }
        else if(Bubblestring)
        {
            
            cell.msgLabel.text = Bubblestring;
            
        }
        else
        {
            cell.msgLabel.text = coreMessage.mostRecentMessageBody;
            
        }
        
        
        cell.msgLabel.textColor = RGBA(102,102,102, 1);
    }
    
    
    if (unreadmsgStr != nil && ![unreadmsgStr isEqual:@"0"]) {
        cell.unreadMessageLabel.hidden = NO;
        
        UIFont *font = [UIFont fontWithName:FontString size:9];
        CGRect textrect = [(unreadmsgStr ? unreadmsgStr:@"") boundingRectWithSize:CGSizeMake(230,9999) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
        if (textrect.size.width > 14) {
            cell.unreadMessageLabel.frame = CGRectMake(cell.contactImageView.frame.origin.x+cell.contactImageView.frame.size.width-8, cell.contactImageView.frame.origin.y-5, textrect.size.width+5, 14);
        }
        else{
            cell.unreadMessageLabel.frame = CGRectMake(cell.contactImageView.frame.origin.x+cell.contactImageView.frame.size.width-8, cell.contactImageView.frame.origin.y-5, 14, 14);
        }
        cell.unreadMessageLabel.text = unreadmsgStr;
        
        [[NSUserDefaults standardUserDefaults]setObject:coreMessage.unreadedMsg forKey:[NSString stringWithFormat:@"%@%@",KEY_UNREAD,coreMessage.bareJidStr]];
        
    }
    else
    {
        cell.unreadMessageLabel.hidden = YES;
        cell.unreadMessageLabel.text = @"";
    }
    
    cell.tagImg.hidden = YES;
    
    if (imp)
    {
        cell.showImp = YES;
        cell.tagImg.hidden = NO;
        [cell startAnimationBatch];
    }
    
    cell.callBtn.hidden = YES;
    cell.chatBtn.hidden = YES;
    cell.profileBtn.hidden = YES;
    cell.lineVw = nil;
    if (selectedPrifilePic == indexpath)
    {
        cell.callBtn.hidden = NO;
        cell.chatBtn.hidden = NO;
        cell.profileBtn.hidden = NO;
        cell.lineVw = [DotLineView new];
    }
    else
    {
        cell.lineVw = [DotLineView new];
    }
    coreMessage = nil;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSFetchedResultsController Delegate Methods
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    if (!isSearching)
        [self beginUpdates];
}
- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    if (!isSearching)
    {
        switch(type)
        {
            case NSFetchedResultsChangeInsert:
                [self insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationNone];
                break;
                
            case NSFetchedResultsChangeDelete:
                [self deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                break;
                
            case NSFetchedResultsChangeUpdate:
                [self reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                break;
                
            case NSFetchedResultsChangeMove:
                [self deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                [self insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationNone];
                break;
        }
    }
}
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    if (!isSearching)
        [self endUpdates];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Date to string
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(NSString *)datetoString:(NSDate *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd, yyyy"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    formatter = nil;
    return stringFromDate;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Search Implementation
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:YES animated:YES];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    //Remove all objects first.
    if([searchText length] != 0) {
        isSearching = YES;
        [self searchTableList];
    }
    else
        isSearching = NO;
    [self reloadData];
    [searchBar becomeFirstResponder];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    isSearching = NO;
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
    [self reloadData];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self searchTableList];
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
    
}
//Search the array
- (void)searchTableList {
    NSString *searchString = _searchBar.text;
    contactPredicate = [NSPredicate predicateWithFormat:@"displayName contains[c] %@",searchString];
    [self searchFetchControl:contactPredicate];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark FetchController for Recent Chat History
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (NSFetchedResultsController *)fetchedResultsController {
    
    if (fetchedResultsController)
        return fetchedResultsController;
    XMPPMessageArchivingCoreDataStorage *storage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Contact_CoreDataObject" inManagedObjectContext:moc];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//    NSString *predicateFrmt = @"displayName.length = nil";
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt];
//    [fetchRequest setPredicate:predicate];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:[NSSortDescriptor sortValues:@"mostRecentMessageTimestamp" ascending:NO]];
    fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:moc sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsController setDelegate:self];
    NSError *error = nil;
    if (![fetchedResultsController performFetch:&error])
        NSLog(@"ERROR IN CHAT FETCHCONTROLL:::::%@",error);
    
    return fetchedResultsController;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark DataBase Methods For Searching
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (NSFetchedResultsController *)searchFetchControl:(NSPredicate *)predicate
{
    searchFetchControl = nil;
    XMPPMessageArchivingCoreDataStorage *storage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Contact_CoreDataObject"
                                              inManagedObjectContext:moc];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    fetchRequest.predicate = predicate;
    [fetchRequest setSortDescriptors:[NSSortDescriptor sortValues:@"displayName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]];
    
    searchFetchControl = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:moc                     sectionNameKeyPath:nil  cacheName:nil];
    NSError *error = nil;
    if (![searchFetchControl performFetch:&error])
    {
        
    }
    return searchFetchControl;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Method to Refreshing RecentChat Tableview
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////


-(void)refreshView:(UIRefreshControl *)refresh
{
    // custom refresh logic would be placed here...
    [refresh beginRefreshing];
    [self reloadData];
    [refresh endRefreshing];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - TableView Delegate didSelectRowAtIndexPath
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)cellTapPress:(UITapGestureRecognizer *)gesture
{
    
    CGPoint tapLocation = [gesture locationInView:self];
    NSIndexPath *cellTapPressIndexPath = [self indexPathForRowAtPoint:tapLocation];
    
    if(cellTapPressIndexPath != nil)
    {
        RecentChatcellCell *buddyCell  = (RecentChatcellCell*)[self cellForRowAtIndexPath:quickMenuSelectedIndex];
        // NSLog(@"%@",buddyCell.contactNameLbl.text);
        
        
        [_searchBar resignFirstResponder];
        XMPPMessageArchiving_Contact_CoreDataObject *coreMessage;
        
        if (!isSearching)
            coreMessage =[[self fetchedResultsController] objectAtIndexPath:cellTapPressIndexPath];
        else
            coreMessage =[[self searchFetchControl:contactPredicate] objectAtIndexPath:cellTapPressIndexPath];
        
        if ([coreMessage.type isEqualToString:@"singleChat"])
        {
            XMPPUserCoreDataStorageObject *userdetails = [self RecentcontactsFetch:coreMessage.bareJid];
            if (userdetails)
            {
                if([self.recentChatdelegate respondsToSelector:@selector(didSelectRecentChat:)])
                    [self.recentChatdelegate didSelectRecentChat:userdetails];
                userdetails = nil;
            }
            
        }
        else if([coreMessage.type isEqualToString:@"groupChat"])
        {
            if([self.recentChatdelegate respondsToSelector:@selector(didSelectRecentGroupChat:)])
                [self.recentChatdelegate didSelectRecentGroupChat:coreMessage.bareJid];
        }
        
        
        coreMessage = nil;
        
    }
    
    
}



- (void)longPress:(UILongPressGestureRecognizer *)gesture
{
    
    CGPoint tapLocation = [gesture locationInView:self];
    NSIndexPath *longPressIndexPath = [self indexPathForRowAtPoint:tapLocation];
    
    XMPPMessageArchiving_Contact_CoreDataObject *coreMessage;
    
    if (!isSearching)
        coreMessage =[[self fetchedResultsController] objectAtIndexPath:longPressIndexPath];
    else
        coreMessage =[[self searchFetchControl:contactPredicate] objectAtIndexPath:longPressIndexPath];
    
    if (longPressIndexPath == nil)
    {
        
    }
    else if (gesture.state == UIGestureRecognizerStateBegan)
    {
        //   RecentChatcellCell *buddyCell  = (RecentChatcellCell*)[self cellForRowAtIndexPath:longPressIndexPath];
        
        NSArray *optionArray;
        if ([coreMessage.type isEqualToString:@"singleChat"])
        {
            XMPPUserCoreDataStorageObject *userdetails = [self RecentcontactsFetch:coreMessage.bareJid];
            
            NSString *JIDStr = userdetails.jidStr;
            JIDStr = [JIDStr stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"@%@",DOMAINAME] withString:@""];
            // NSLog(@"JID after Removing Domain %@",JIDStr);
            
            if ([userdetails.displayName isEqualToString:JIDStr])
            {
                optionArray = [NSArray arrayWithObjects:@"Clear Conversation",@"Delete Chat",@"Add to Contacts",nil];
                
            }
            else
            {
                optionArray = [NSArray arrayWithObjects:@"Clear Conversation",@"Delete Chat",nil];
            }
        }
        else
        {
            optionArray = [NSArray arrayWithObjects:@"Clear Conversation",@"Exit group",nil];
        }
        
        RecentChatPopUpView *popUpView = [[RecentChatPopUpView alloc]initWithFrame:self.frame view:self.superview contactData:optionArray];
        popUpView.Delegate = self;
        popUpView.popUpcoreMessage = coreMessage;
        
        
        
    }
    else
    {
        // NSLog(@"gestureRecognizer.state = %ld", (long)gesture.state);
    }
    coreMessage = nil;
    
    
    
}

-(void)PopUpdidSelectItem:(NSIndexPath *)indexpath popUpcoreMessage:(XMPPMessageArchiving_Contact_CoreDataObject *)coreMessage Action:(NSString *)actionString
{
    switch (indexpath.row)
    {
        case 0:
        {
            [self clearChatWithID:coreMessage.bareJidStr];
            [[XMPPMessageArchivingCoreDataStorage sharedInstance]updateUnreadMessage:coreMessage.bareJid clearValue:YES];
            
            
//            XMPPMessageArchivingCoreDataStorage *storage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
//            NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
//            [moc performBlockAndWait:^{
//                [coreMessage setValue:@"" forKey:@"mostRecentMessageBody"];
//                NSError *error = nil;
//                if (![moc save:&error])
//                {
//                    // NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
//                }
//            }];
            
        }
            break;
            
        case 1:
        {
            if ([coreMessage.type isEqualToString:@"groupChat"])
            {
                [self deleteAndExitGroup:coreMessage.bareJid];
            }
            else
            {
                
                XMPPMessageArchivingCoreDataStorage *storage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
                
                NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
                [moc performBlockAndWait:^{
                    [moc deleteObject:coreMessage];
                    [moc save:nil];
                }];
            }
        }
        case 2:
        {
            if ([coreMessage.type isEqualToString:@"singleChat"])
            {
                if ([actionString isEqualToString:@"Add to Contacts"])
                {
                    [self checkAddressBookAccess:coreMessage];
                    
                }
                else
                {
                    
                }
            }
            
        }
            break;
            
        default:
            
            break;
    }
}

-(void)checkAddressBookAccess:(XMPPMessageArchiving_Contact_CoreDataObject *)coreMesaage
{
    switch (ABAddressBookGetAuthorizationStatus())
    {
            // Update our UI if the user has granted access to their Contacts
        case  kABAuthorizationStatusAuthorized:
            [self accessGrantedForAddressBook:coreMesaage];
            break;
            // Prompt the user for access to Contacts if there is no definitive answer
        case  kABAuthorizationStatusNotDetermined :
            [self requestAddressBookAccess:coreMesaage];
            break;
            // Display a message if the user has denied or restricted access to Contacts
        case  kABAuthorizationStatusDenied:
        case  kABAuthorizationStatusRestricted:
        {
            //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Privacy Warning"
            //                                                            message:@"Permission was not granted for Contacts."
            //                                                           delegate:nil
            //                                                  cancelButtonTitle:@"OK"
            //                                                  otherButtonTitles:nil];
            //            [alert show];
            //
            //
            [self requestAddressBookAccess:coreMesaage];
            
            
        }
            break;
        default:
            break;
    }
}


-(void)requestAddressBookAccess:(XMPPMessageArchiving_Contact_CoreDataObject *)coreMessage
{
    RecentChatTableView * __weak weakSelf = self;
    ABAddressBookRequestAccessWithCompletion(self.addressBook, ^(bool granted, CFErrorRef error)
                                             {
                                                 if (granted)
                                                 {
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         [weakSelf accessGrantedForAddressBook:coreMessage];
                                                         
                                                     });
                                                 }
                                             });
}



-(void)accessGrantedForAddressBook:(XMPPMessageArchiving_Contact_CoreDataObject *)coreMessage
{
    if([self.recentChatdelegate respondsToSelector:@selector(didSelectUnkownContact:)])
        [self.recentChatdelegate didSelectUnkownContact:coreMessage];
}



-(void)deleteAndExitGroup :(XMPPJID *)roomJID
{
    //this is to delete the group
    
    XMPPMessage *groupJoinMessage = [[XMPPMessage alloc]init];
    [groupJoinMessage addAttributeWithName:@"id" stringValue:[NSString stringWithFormat:@"%@",[[self xmppStream] generateUUID]]];
    [groupJoinMessage addAttributeWithName:@"to" stringValue:[roomJID full]];
    
    [groupJoinMessage addAttributeWithName:@"type" stringValue:@"groupchat"];
    NSXMLElement *groupJoinLeave = [NSXMLElement elementWithName:@"group"];
    [groupJoinLeave addAttributeWithName:@"xmlns" stringValue:[NSString stringWithFormat:@"%@:group",DOMAINAME]];
    [groupJoinLeave addAttributeWithName:@"type" stringValue:@"0"];
    [groupJoinMessage addChild:groupJoinLeave];
    NSXMLElement *bodyMessage = [NSXMLElement elementWithName:@"body" stringValue:@"left"];
    [groupJoinMessage addChild:bodyMessage];
    [[self xmppStream] sendElement:groupJoinMessage];
    
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void)
                   {
                       //this is to inform other members who has left the group
                       XMPPPresence *presence = [XMPPPresence presence];
                       [presence addAttributeWithName:@"to" stringValue:[roomJID full]];
                       [presence addAttributeWithName:@"type" stringValue:@"unavailable"];
                       [[self xmppStream] sendElement:presence];
                       [self clearAllMessageHistory:roomJID];
                       [self cleargroupDetails:roomJID];
                   });
    
}

-(void)clearAllMessageHistory:(XMPPJID *)roomJID
{
     NSManagedObjectContext *moc = [[self appDelegate].xmppMessageArchivingStorage mainThreadManagedObjectContext];
    [moc performBlockAndWait:^{
        NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:moc];
        NSFetchRequest *request = [[NSFetchRequest alloc]init];
        NSString *predicateFrmt = @"bareJidStr = %@";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,roomJID.bare];
        [request setPredicate:predicate];
        [request setEntity:entityDescription];
        NSError *error;
        NSArray *messages = [moc executeFetchRequest:request error:&error];
        for (XMPPMessageArchiving_Message_CoreDataObject *coremessage in messages)
        {
            [moc deleteObject:coremessage];
        }
        
        NSEntityDescription *contactEnityDes = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Contact_CoreDataObject"  inManagedObjectContext:moc];
        NSFetchRequest *contactRequest = [[NSFetchRequest alloc]init];
        [contactRequest setEntity:contactEnityDes];
        NSString *predicateFrmt2 = @"bareJidStr = %@";
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:predicateFrmt2, roomJID.bare];
        [contactRequest setPredicate:predicate2];
        NSArray *recentContactMessage = [moc executeFetchRequest:contactRequest error:&error];
        
        for (XMPPMessageArchiving_Contact_CoreDataObject *recentCoremessage in recentContactMessage)
        {
            [moc deleteObject:recentCoremessage];
        }
        if (![moc save:&error]) {
            
            //  NSLog(@"DB SAVE ERROR in clearAllHistory%@",error);
        }
        
    }];
    
}
-(void)cleargroupDetails:(XMPPJID *)roomJID
{
    XMPPRoomCoreDataStorage *storage = [XMPPRoomCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
    [moc performBlockAndWait:^{
        NSEntityDescription *entityDescription = [NSEntityDescription entityForName:storage.occupantEntityName
                                                             inManagedObjectContext:moc];
        NSFetchRequest *request = [[NSFetchRequest alloc]init];
        [request setEntity:entityDescription];
        NSString *predicateFrmt = @"roomJIDStr = %@";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,roomJID.bare];
        [request setPredicate:predicate];
        NSError *error;
        
        NSArray *groupDetails = [moc executeFetchRequest:request error:&error];
        for (XMPPRoomMessageDetails *details in groupDetails)
        {
            [moc deleteObject:details];
        }
        
        NSEntityDescription *occupantentityDescription = [NSEntityDescription entityForName:storage.messageDetailsEntityName  inManagedObjectContext:moc];
        NSFetchRequest *occupantrequest = [[NSFetchRequest alloc]init];
        [occupantrequest setEntity:occupantentityDescription];
        NSString *predicateFrmt2 = @"roomJIDStr = %@";
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:predicateFrmt2,roomJID.bare];
        [occupantrequest setPredicate:predicate2];
        NSArray *occupantgroupDetails = [moc executeFetchRequest:occupantrequest error:&error];
        for (XMPPRoomOccupantCoreDataStorageObject *occupantdetails in occupantgroupDetails) {
            [moc deleteObject:occupantdetails];
        }
    }];
    
    NSError *error;
    if (![moc save:&error]) {
        //  NSLog(@"DB SAVE ERROR in CleargroupDetails%@",error);
    }
}

- (void)clearChatWithID:(NSString*)JidStr
{
    XMPPMessageArchivingCoreDataStorage *storage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
    [moc performBlockAndWait:^{
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:moc];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:entity];
        NSString *predicateFrmt = @"bareJidStr == %@";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,JidStr];
        [fetchRequest setPredicate:predicate];
        [fetchRequest setSortDescriptors:[NSSortDescriptor sortValues:@"timestamp" ascending:YES]];
        NSArray *delArray=[moc executeFetchRequest:fetchRequest error:nil];
        
        for (XMPPMessageArchiving_Message_CoreDataObject *CoreDataObject in delArray)
        {
            if (!CoreDataObject.isPrivate)
            {
                NSXMLElement *file = [CoreDataObject.message elementForName:@"file"];
                
                if (file)
                {
                    mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
                    [mediaDb deleteMedia:CoreDataObject.messageID];
                }
                
                [moc deleteObject:CoreDataObject];
            }
            
            NSError *error = nil;
            if (![moc save:&error])
            {
                //  NSLog(@"Sorry, couldn't delete values %@", [error slocalizedDescription]);
            }
        }
    }];
    
    [[self xmppMessageArchivingCoreDataStorage]updateMessageOnRecentChat:JidStr];
}


- (void)tapPress:(UITapGestureRecognizer *)gesture
{
    
    CGPoint tapLocation = [gesture locationInView:self];
    quickMenuSelectedIndex = [self indexPathForRowAtPoint:tapLocation];
    //  UIButton *button = (UIButton *)sender;
    
    //  NSLog(@"%ld",(long)quickMenuSelectedIndex.row);
    
    if(quickMenuSelectedIndex != nil)
    {
        RecentChatcellCell *buddyCell  = (RecentChatcellCell*)[self cellForRowAtIndexPath:quickMenuSelectedIndex];
        //  NSLog(@"%@",buddyCell.contactNameLbl.text);
        
    }
    
    XMPPMessageArchiving_Contact_CoreDataObject *coreMessage;
    
    if (!isSearching)
        coreMessage =[[self fetchedResultsController] objectAtIndexPath:quickMenuSelectedIndex];
    else
        coreMessage =[[self searchFetchControl:contactPredicate] objectAtIndexPath:quickMenuSelectedIndex];
    
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{     [self scrollRectToVisible:[self rectForRowAtIndexPath:quickMenuSelectedIndex] animated:NO]; }
                     completion:^(BOOL finished){
                         UIView* view = gesture.view;
                         
                         CGPoint p = [view convertPoint:view.frame.origin toView:self.superview];
                         
                         quickMenu* quiMenu = [[quickMenu alloc] initFrame:self.superview topPoint:p.y + 4 bottomPoint:p.y+view.frame.size.height-13 TypeOfMenu:coreMessage.type];
                         quiMenu.fromVC = @"Recent";
                         
                         [self.superview addSubview:quiMenu];
                         //NSLog(@"top  %f  bottom  %f",p.y,p.y+view.frame.size.height);
                         
                         
                     }];
    coreMessage = nil;
    
    
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark DataBase Methods For Recent Chats
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (XMPPUserCoreDataStorageObject *)RecentcontactsFetch:(XMPPJID *)Str
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:[self managedObjectContext_roster]];
    [request setEntity:entity];
    NSString *predicateFrmt = @"jidStr = %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, Str.bare];
    [request setPredicate:predicate];
    NSArray *empArray=[[self managedObjectContext_roster] executeFetchRequest:request error:nil];
    if ([empArray count] == 1)
    {
        for (XMPPUserCoreDataStorageObject *user in  empArray)
        {
            return user;
        }
    }
    return nil;
}





- (XMPPMessageArchiving_Message_CoreDataObject*)MsgWithJID:(NSString*)JID
{
    NSManagedObjectContext *moc = [[self xmppMessageArchivingCoreDataStorage] mainThreadManagedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:moc];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    NSString *predicateFrmt = @"bareJidStr == %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,JID];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:[NSSortDescriptor sortValues:@"timestamp" ascending:NO]];
    NSArray *delArray=[moc executeFetchRequest:fetchRequest error:nil];
    
    if (delArray.count > 0) {
        XMPPMessageArchiving_Message_CoreDataObject *CoreDataObject = [delArray objectAtIndex:0];
        if (CoreDataObject) {
            return CoreDataObject;
            
        }
        
        
    }
    
    
    
    return nil;
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Xmppstream delegate Methods
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq
{
    if (![[[iq attributeForName:@"type"]stringValue] isEqualToString:@"error"]) {
        NSXMLElement *elemnt = [iq elementForName:@"vCard" xmlns:@"vcard-temp"];
        if (elemnt) {
            double delayInSeconds = 1.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self reloadData];
            });
        }
    }
    return NO;
}

//quickMenuClicked Notification
-(void)quickMenuClicked:(NSNotification *)notification
{
    NSDictionary *dict = [notification userInfo];
    
    
    NSInteger selectedButtonNo = [[dict objectForKey:QUICK_MENU_FOR_RECENT]integerValue];
    
    XMPPMessageArchiving_Contact_CoreDataObject *coreMessage;
    
    if (!isSearching)
        coreMessage =[[self fetchedResultsController] objectAtIndexPath:quickMenuSelectedIndex];
    else
        coreMessage =[[self searchFetchControl:contactPredicate] objectAtIndexPath:quickMenuSelectedIndex];
    
    XMPPUserCoreDataStorageObject *userdetails = [self RecentcontactsFetch:coreMessage.bareJid];
    
    // NSLog(@"Uer Details: %@",userdetails);
    
    
    switch (selectedButtonNo)
    {
        case IsEqualCall:
            
            
            if ([coreMessage.type isEqualToString:@"singleChat"])
            {
                //NSLog(@"Calling %@",userdetails.jid.user);
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",userdetails.jid.user]]];
                
            }
            else
            {
                //  NSLog(@"quickMenuClicked Group Call %@",[self getMemberlist:coreMessage.bareJid]);
                
            }
            
            //
            break;
        case IsEqualChat:
            
            //  NSLog(@"quickMenuClicked Chat");
            
            if ([coreMessage.type isEqualToString:@"singleChat"])
            {
                if (userdetails)
                {
                    if([self.recentChatdelegate respondsToSelector:@selector(didSelectRecentChat:)])
                        [self.recentChatdelegate didSelectRecentChat:userdetails];
                }
            }
            else if([coreMessage.type isEqualToString:@"groupChat"])
            {
                if([self.recentChatdelegate respondsToSelector:@selector(didSelectRecentGroupChat:)])
                    [self.recentChatdelegate didSelectRecentGroupChat:coreMessage.bareJid];
            }
            
            
            //
            break;
        case IsEqualProfile:
            
            if ([coreMessage.type isEqualToString:@"singleChat"])
            {
                if (userdetails)
                {
                    if([self.recentChatdelegate respondsToSelector:@selector(didSelectRecentChat:)])
                        [self.recentChatdelegate didSelectContacts:userdetails];
                }
            }
            else if([coreMessage.type isEqualToString:@"groupChat"])
            {
                if([self.recentChatdelegate respondsToSelector:@selector(didSelectRecentGroupChat:)])
                    [self.recentChatdelegate didSelectGroup:coreMessage.bareJid :coreMessage.displayName];
            }
            
            //
            break;
        default:
            //
            break;
    }
    userdetails = nil;
    coreMessage = nil;
    
}

-(NSMutableArray *)getMemberlist:(XMPPJID *)roomJID
{
    NSMutableArray *memberArray = [NSMutableArray new];
    XMPPRoomCoreDataStorage *storage = [XMPPRoomCoreDataStorage sharedInstance];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"roomJIDStr == %@", roomJID.bare];
    NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:storage.occupantEntityName
                                                         inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    request.predicate = predicate;
    [request setEntity:entityDescription];
    request.resultType = NSDictionaryResultType;
    request.propertiesToFetch = [NSArray arrayWithObjects:[[entityDescription propertiesByName] objectForKey:@"nickname"],[[entityDescription propertiesByName] objectForKey:@"affiliation"],nil];
    request.returnsDistinctResults = YES;
    
    // Now it should yield an NSArray of distinct values in dictionaries.
    NSArray *dictionaries = [moc executeFetchRequest:request error:nil];
    for (NSMutableDictionary* element in dictionaries)
    {
        NSMutableDictionary *detailsDict = [NSMutableDictionary new];
        [detailsDict setObject:[self getDisplayName:[element objectForKey:@"nickname"]] forKey:@"nickname"];
        [detailsDict setObject:[self getmemberJid:[element objectForKey:@"nickname"]] forKey:@"jid"];
        [detailsDict setObject:[element objectForKey:@"affiliation"] forKey:@"affiliation"];
        [memberArray addObject:detailsDict];
    }
    
    // NSLog(@"MemberArray -->> %@",memberArray);
    return memberArray;
}


-(NSString *)getDisplayName:(NSString *)resourceStr
{
    
    NSMutableArray *userdetailsArray = [NSMutableArray new];
    if ([resourceStr rangeOfString:DOMAINAME].location==NSNotFound )
        resourceStr = [resourceStr stringByAppendingString:[NSString stringWithFormat:@"@%@",DOMAINAME]];
    
    if ([resourceStr isEqualToString:[self xmppStream].myJID.bare])
        return @"You";
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:[self managedObjectContext_roster]];
    [request setEntity:entity];
    NSString *predicateFrmt = @"jidStr = %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, resourceStr];
    [request setPredicate:predicate];
    NSArray *empArray=[[self managedObjectContext_roster] executeFetchRequest:request error:nil];
    if ([empArray count] == 1)
    {
        for (XMPPUserCoreDataStorageObject *userdetails in empArray)
        {
            [userdetailsArray addObject:userdetails.jid.user];
            return userdetails.displayName;
        }
    }
    return [resourceStr stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"@%@",DOMAINAME] withString:@""];;
}

-(NSString *)getmemberJid:(NSString *)resourceStr
{
    if ([resourceStr rangeOfString:DOMAINAME].location==NSNotFound )
        resourceStr = [resourceStr stringByAppendingString:[NSString stringWithFormat:@"@%@",DOMAINAME]];
    
    return resourceStr;
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Appdelegate Values
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (AppDelegate *)appDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (NSManagedObjectContext *)managedObjectContext_roster
{
    return [[[self appDelegate] xmppRosterStorage] mainThreadManagedObjectContext];
}
- (XMPPStream *)xmppStream
{
    return [[self appDelegate] xmppStream];
}
-(XMPPMessageArchivingCoreDataStorage *)xmppMessageArchivingCoreDataStorage
{
    
    return [[self appDelegate] xmppMessageArchivingStorage];
}



@end
