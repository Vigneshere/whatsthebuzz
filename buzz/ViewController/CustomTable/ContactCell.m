//
//  ContactCell.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 18/02/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "ContactCell.h"

@implementation ContactCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        CGRect contentViewFrame = self.contentView.frame;
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        
        contentViewFrame.size.width = screenRect.size.width;
        self.contentView.frame = contentViewFrame;
        
        self.checkView = [UIView new];
        self.checkView.frame = CGRectMake(screenRect.size.width-50, 15, 20, 20);
        self.checkView.backgroundColor = [UIColor whiteColor];
        self.checkView.layer.cornerRadius = 10;
        [self.contentView addSubview:self.checkView];
        
        UIImage *favoriteImage = [UIImage imageNamed:@"star_icon_normal.png"];
        self.favoriteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.favoriteButton.frame = CGRectMake(self.contentView.frame.size.width-60, 5, 50, 40);
        [self.favoriteButton setImage:favoriteImage forState:UIControlStateNormal];
        [self.favoriteButton setImageEdgeInsets:UIEdgeInsetsMake( 0.0, 0.0, 0.0, 0.0)];
        [self.contentView addSubview:self.favoriteButton];
        
        // Initialization code
        self.contactImageView = [UIImageView new];
        self.contactImageView.frame = CGRectMake(self.frame.origin.x+20, self.frame.origin.y+10, 40, 40);
        self.contactImageView.layer.cornerRadius = 20;
        self.contactImageView.clipsToBounds = YES;
        [self.contentView addSubview:self.contactImageView];
        
        self.contactNameLbl = [UILabel new];
        self.contactNameLbl.frame = CGRectMake(self.contactImageView.frame.origin.x+self.contactImageView.frame.size.width+30, self.frame.origin.y+5, 220, 25);
        self.contactNameLbl.backgroundColor = [UIColor clearColor];
        self.contactNameLbl.textColor = RGBA(98, 99, 103, 1);
        self.contactNameLbl.textAlignment = NSTextAlignmentLeft;
        self.contactNameLbl.font = [UIFont fontWithName:FontString size:18];
        [self.contentView addSubview:self.contactNameLbl];
        
        self.statusLabel = [UILabel new];
        self.statusLabel.frame = CGRectMake(self.contactImageView.frame.origin.x+self.contactImageView.frame.size.width+30, self.contactNameLbl.frame.origin.y+self.contactNameLbl.frame.size.height+5, 100, 20);
        self.statusLabel.backgroundColor = [UIColor clearColor];
        self.statusLabel.textColor = RGBA(136, 141, 147, 1);
        self.statusLabel.font = [UIFont fontWithName:FontString size:10];
        self.statusLabel.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:self.statusLabel];

        line = [DotLineView new];
        [line drawLine:CGRectMake(0, 53-0.2, self.contentView.frame.size.width, 0.2) :self :[UIColor grayColor]];
        line = nil;
        
        
        
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
@end