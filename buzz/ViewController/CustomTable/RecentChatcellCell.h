//
//  RecentChatcellCell.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 06/03/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecentChatTableView.h"

@interface RecentChatcellCell : UITableViewCell
{
    RecentChatTableView *recentChat;
}
@property(nonatomic,retain)UIImageView *contactImageView;
@property(nonatomic,retain)UILabel *contactNameLbl;
@property(nonatomic,retain)UILabel *msgLabel;
@property(nonatomic,retain)UILabel *dateLabel;
@property(nonatomic,retain)UILabel *unreadMessageLabel;
@property(nonatomic,retain)UIButton *callBtn;
@property(nonatomic,retain)UIButton *chatBtn;
@property(nonatomic,retain)UIButton *profileBtn;
@property(nonatomic,retain)DotLineView *lineVw;
@property (nonatomic) BOOL showImp;
@property(nonatomic,retain)UIImageView *tagImg;

-(void)startAnimationBatch;

@end
