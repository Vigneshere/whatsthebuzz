//
//  contactTableViewController.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 15/04/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AvatarImage.h"
#import "quickMenu.h"

@protocol ContactTabledelegate <NSObject>
- (void)didSelectContacts:(XMPPUserCoreDataStorageObject *)user;
@end
@interface contactTableViewController : UITableViewController<UISearchBarDelegate,NSFetchedResultsControllerDelegate,UIGestureRecognizerDelegate>
{
    
    AvatarImage *avatarImg;
    NSFetchedResultsController *fetchedResultsController;
    NSFetchedResultsController *searchFetchControl;
    BOOL isSearching;
    NSPredicate *contactPredicate;
    NSIndexPath *quickMenuSelectedIndex;
}
@property(nonatomic,retain)UISearchBar *searchBar;
@property (nonatomic, weak)  id<ContactTabledelegate> contactsdelegate;
@property (nonatomic, assign)  NSString *viewControllerStr;
@property (nonatomic, readonly)  NSMutableArray *groupMemberArray;


@end
