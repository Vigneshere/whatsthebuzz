//
//  contactTableViewController.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 15/04/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "contactTableViewController.h"
#import "ContactCell.h"
#import "XMPPUserCoreDataStorageObject.h"
#import "ProfileViewController.h"
#import "chatViewController.h"
@interface contactTableViewController ()
{
    UILabel* titleLabel;
}

@end

@implementation contactTableViewController
{
    UIButton *cancelButton;
}
@synthesize contactsdelegate;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Appdelegate Values
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (AppDelegate *)appDelegate
{
	return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (XMPPRosterCoreDataStorage *)xmppRosterStorage
{
	return [[self appDelegate] xmppRosterStorage];
}
- (XMPPStream *)xmppStream
{
	return [[self appDelegate] xmppStream];
}
- (XMPPRoster *)xmppRoster
{
	return [[self appDelegate] xmppRoster];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *ver = [[UIDevice currentDevice] systemVersion];
    int ver_int = [ver intValue];
    
   
      cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    //-->Search Bar for TableView
    _searchBar = [UISearchBar new];
    _searchBar.delegate = self;
    [_searchBar sizeToFit];
    _searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    _searchBar.backgroundColor = [UIColor clearColor];
    _searchBar.placeholder = @"Search";
    _searchBar.tag = 1;
    _searchBar.tintColor = RGBA(53, 152, 219, 1);
    _searchBar.backgroundImage = [UIImage imageWithColor:[UIColor clearColor]];
    
    if (ver_int < 7) {
        _searchBar.barStyle = UIBarStyleDefault;
    }
    
    else {
        _searchBar.searchBarStyle = UISearchBarStyleProminent;
        _searchBar.barTintColor =[UIColor clearColor];
    }
    _searchBar.translucent = YES;
    
    UITextField *txfSearchField = [_searchBar valueForKey:@"_searchField"];
    txfSearchField.layer.cornerRadius = 13;
    txfSearchField.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.7];
    self.tableView.tableHeaderView = _searchBar;
    
    UIRefreshControl *refreshControl = [UIRefreshControl new];
    [refreshControl addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
    [refreshControl setTintColor:[UIColor colorWithRed:255.0f/255.0f green:55.0f/255.0f blue:152.0f/255.0f alpha:1.0]];
    [self.tableView addSubview:refreshControl];
    
    

    
    avatarImg = [AvatarImage new];
    isSearching = NO;
    _groupMemberArray = [NSMutableArray new];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(quickMenuClicked:) name:QUICK_MENU_FOR_CONTACT object:nil];

}
-(void)viewWillAppear:(BOOL)animated
{
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.bounces = YES;
    if(IS_IPHONE_4)
    {
        self.parentViewController.view.backgroundColor = BACKGROUNDIMAGE_4;
        
    }
    else
    {
        self.parentViewController.view.backgroundColor = BACKGROUNDIMAGE;
        
    }
    
    titleLabel = [UILabel new];
    titleLabel.frame = CGRectMake(0, 10, self.view.frame.size.width, 30);
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = RGBA(53, 152, 219, 1);
    titleLabel.text = @"Select Contact";
    titleLabel.font = [UIFont fontWithName:FontString size:20];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.navigationController.navigationBar addSubview:titleLabel];
    
    UIImage *backButtonImage = [UIImage imageNamed:@"back_btn.png"];
    cancelButton.frame = CGRectMake(0, 0, backButtonImage.size.width, backButtonImage.size.height);
    [cancelButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
       [self.navigationController.navigationBar addSubview:cancelButton];
    [[self xmppStream] addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
}
-(void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    CGRect barFrame = self.searchBar.frame;
    barFrame.origin.x = 10;
    barFrame.size.width = self.view.frame.size.width - 20;
    self.searchBar.frame = barFrame;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [cancelButton removeFromSuperview];
    [titleLabel removeFromSuperview];
    
    [[self xmppStream] removeDelegate:self delegateQueue:dispatch_get_main_queue()];
}

-(void)refreshView:(UIRefreshControl *)refresh
{
    // custom refresh logic would be placed here...
    [refresh beginRefreshing];
    [[self appDelegate]StartUpdateRoster];
    [refresh endRefreshing];
}



-(void)cancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - TableView DataSource Method
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    
    // Background color
    view.tintColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.3];
    //[UIColor colorWithRed:77.0/255.0 green:162.0/255.0 blue:217.0/255.0 alpha:1.0];
    // Text Color
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor orangeColor]];

    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (!isSearching)
    {
     //   NSLog(@"%lu",(unsigned long)[[[self fetchedResultsController] sections] count]);
        return [[[self fetchedResultsController] sections] count];
    }
    else
        return 1;
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (!isSearching)
    {
        if ([self.tableView.dataSource tableView:tableView numberOfRowsInSection:section] == 0) {
            return nil;
        }
        NSArray *sections = [[self fetchedResultsController] sections];
        if (section < [sections count])
        {
            id <NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
            return sectionInfo.name;
        }
    }
    return nil;
}
-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    
    if (!isSearching)
    {
        NSArray *sections = [[self fetchedResultsController] sections];
        NSMutableArray *arrayfilter = [NSMutableArray new];
        for (int i = 0; i < sections.count; i++) {
            id <NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:i];
            [arrayfilter addObject:sectionInfo.name];
        }
        return arrayfilter;
    }
    else
        return 0;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(!isSearching)
    {
        NSArray *sections = [[self fetchedResultsController] sections];
        if (section < [sections count])
        {
            id <NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
            return sectionInfo.numberOfObjects;
        }
    }
    else
        return searchFetchControl.fetchedObjects.count;
    
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 53;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - TableView DataSource cellForRowAtIndexPath
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.tableView respondsToSelector:@selector(setSectionIndexColor:)])
    {
        self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
        self.tableView.sectionIndexTrackingBackgroundColor = [UIColor clearColor];
    }
    static NSString *CellIdentifier = @"Cell";
    ContactCell *cell = (ContactCell *)[self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[ContactCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    XMPPUserCoreDataStorageObject *user;
    if(!isSearching)
        user  = [[self fetchedResultsController] objectAtIndexPath:indexPath];
    else
        user = [[self searchFetchControl:contactPredicate] objectAtIndexPath:indexPath];
    cell.contactNameLbl.text = user.displayName;
    cell.contactImageView.image = [avatarImg profileImage:user];
    UITapGestureRecognizer *tapPress = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPress:)];
    cell.contactImageView.userInteractionEnabled = YES;
    tapPress.delegate = self;
    // tapPress.cancelsTouchesInView = YES;
    tapPress.numberOfTapsRequired = 1;
    [cell.contactImageView addGestureRecognizer:tapPress];

    XMPPResourceCoreDataStorageObject *userResource;
    if ([user isOnline]) {
        for (userResource in user.resources) {
            
            if (userResource.status != nil)
                cell.statusLabel.text = userResource.status;
            else
                cell.statusLabel.text = userResource.type;
        }
    }
    else
        cell.statusLabel.text = @"unavailable";
    
    if ([self.viewControllerStr isEqualToString:@"createGroupController"])
    {
        cell.favoriteButton.hidden = YES;
        cell.unreadMessageLabel.hidden = YES;
        if ([_groupMemberArray containsObject:user])
            cell.checkView.backgroundColor = RGBA(53, 152, 219, 1);
        else
            cell.checkView.backgroundColor = [UIColor whiteColor];
    }
    else if([self.viewControllerStr isEqualToString:@"ViewController"])
    {
        cell.favoriteButton.hidden =  NO;
        [cell.favoriteButton addTarget:self action:@selector(favoriteButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.checkView removeFromSuperview];
        cell.checkView = nil;
        if (user.isFavorite) {
            [cell.favoriteButton setImage:[UIImage imageNamed:@"star_icon.png"] forState:UIControlStateNormal];
            
        }
        else
            [cell.favoriteButton setImage:[UIImage imageNamed:@"star_icon_normal.png"] forState:UIControlStateNormal];
        
        
        
    }
    return cell;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//#pragma mark - TableView Editing Method
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
//    return NO;
//}
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (editingStyle == UITableViewCellEditingStyleDelete)
//    {
//            XMPPUserCoreDataStorageObject *user = [[self fetchedResultsController] objectAtIndexPath:indexPath];
//            [[self xmppRoster] unsubscribePresenceFromUser:user.jid];
//    }
//}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Core Data
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (NSManagedObjectContext *)managedObjectContext_roster
{
	return [[[self appDelegate] xmppRosterStorage] mainThreadManagedObjectContext];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark DataBase Methods For Contacts
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (NSFetchedResultsController *)fetchedResultsController
{
    if (fetchedResultsController != nil) {
        return  fetchedResultsController;
    }
    NSManagedObjectContext *moc = [self  managedObjectContext_roster];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:moc];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:[NSSortDescriptor sortValues:@"displayName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]];
    NSString *fr = @"from";
    NSString *predicateFrmt = @"subscription != %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, fr];
    [fetchRequest setPredicate:predicate];
    fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:moc  sectionNameKeyPath:@"sectionName" cacheName:nil];
    [fetchedResultsController setDelegate:self];
    
    NSError *error = nil;
    if (![fetchedResultsController performFetch:&error])
    {
    }
	return fetchedResultsController;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark DataBase Methods For Searching
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (NSFetchedResultsController *)searchFetchControl:(NSPredicate *)predicate
{
    searchFetchControl = nil;
    NSManagedObjectContext *moc = [self  managedObjectContext_roster];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:moc];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    fetchRequest.predicate = predicate;
    [fetchRequest setSortDescriptors:[NSSortDescriptor sortValues:@"displayName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]];
    
    searchFetchControl = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:moc                     sectionNameKeyPath:nil  cacheName:nil];
    NSError *error = nil;
    if (![searchFetchControl performFetch:&error])
    {
    }
	return searchFetchControl;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    
    if (!isSearching) {
        [self.tableView beginUpdates];
    }
    
}
- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    if (!isSearching) {
        switch(type)
        {
            case NSFetchedResultsChangeInsert:
                [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationNone];
                break;
                
            case NSFetchedResultsChangeDelete:
                [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                break;
                
            case NSFetchedResultsChangeUpdate:
                [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                break;
                
            case NSFetchedResultsChangeMove:
                [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationNone];
                break;
        }
    }
}
- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    if (!isSearching) {
        [self.tableView endUpdates];
    }
    else
    {
        [self.tableView reloadData];
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Search Implementation
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    //Remove all objects first.
    if([searchText length] != 0) {
        isSearching = YES;
        [self searchTableList];
    }
    else
        isSearching = NO;
    [self.tableView reloadData];
    [searchBar becomeFirstResponder];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    isSearching = NO;
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
    [self.tableView reloadData];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self searchTableList];
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
    
}
//Search the array
- (void)searchTableList {
    NSString *searchString = _searchBar.text;
    NSString *fr = @"from";
    contactPredicate = [NSPredicate predicateWithFormat:@"displayName contains[c] %@ AND subscription != %@",searchString,fr];
    [self searchFetchControl:contactPredicate];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - TableView Delegate didSelectRowAtIndexPath
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.viewControllerStr isEqualToString:@"ViewController"])
    {
        XMPPUserCoreDataStorageObject *userdetails;
        if(!isSearching)
        {
            userdetails = [[self fetchedResultsController] objectAtIndexPath:indexPath];
        }
        else
        {
            userdetails = [[self searchFetchControl:contactPredicate] objectAtIndexPath:indexPath];
        }
        
        [_searchBar resignFirstResponder];
        [_searchBar setShowsCancelButton:NO animated:YES];

        chatViewController* singleChatView = [chatViewController new];
        singleChatView.user = userdetails;
        [self.navigationController pushViewController:singleChatView animated:YES];
        
    }
    else if([self.viewControllerStr isEqualToString:@"createGroupController"])
    {
        XMPPUserCoreDataStorageObject *user;
        if(!isSearching)
            user  = [[self fetchedResultsController] objectAtIndexPath:indexPath];
        else
            user = [[self searchFetchControl:contactPredicate] objectAtIndexPath:indexPath];
        
        if ([_groupMemberArray containsObject:user])
        {
            [_groupMemberArray removeObject:user];
        }
        else
        {
            
            [_groupMemberArray addObject:user];
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation: UITableViewRowAnimationFade];
        }
    }
}

-(void)favoriteButtonClicked:(UIButton *)sender
{
    CGPoint pos = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:pos];
    XMPPUserCoreDataStorageObject *user;
    
    if(!isSearching)
        user  = [[self fetchedResultsController] objectAtIndexPath:indexPath];
    else
        user = [[self searchFetchControl:contactPredicate] objectAtIndexPath:indexPath];
    
    BOOL value;
    if (user.isFavorite)
        value = NO;
    else
        value = YES;
    
    [self updateFavorite:user.jid favorite:value completion:^(BOOL compeleted)
     {
         if (compeleted)
             [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation: UITableViewRowAnimationNone];
     }];
    
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Method to update unreaded message for user
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)updateFavorite:(XMPPJID *)user favorite:(BOOL)isfavorite completion:(void (^)(BOOL))completed
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:[self managedObjectContext_roster]];
    [request setEntity:entity];
    NSString *predicateFrmt = @"jidStr = %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, [user bareJID]];
    [request setPredicate:predicate];
    NSArray *empArray=[[self managedObjectContext_roster] executeFetchRequest:request error:nil];
    if ([empArray count] == 1){
        for (XMPPUserCoreDataStorageObject *user in  empArray)
        {
            if (isfavorite)
                
                user.isFavorite = YES;
            else
                user.isFavorite = NO;
            user.timestamp = [NSDate date];
        }
    }
    else
    {
       // NSLog(@"Nothin");
    }
    if ([[self managedObjectContext_roster] save:nil]) {
        
        completed(YES);
    }
}

- (void)tapPress:(UITapGestureRecognizer *)gesture
{
    
    CGPoint tapLocation = [gesture locationInView:self.view];
    quickMenuSelectedIndex = [self.tableView indexPathForRowAtPoint:tapLocation];
    //  UIButton *button = (UIButton *)sender;
    
    //  NSLog(@"%ld",(long)quickMenuSelectedIndex.row);
    
    
    
    
    
    if(quickMenuSelectedIndex != nil)
    {
        ContactCell *buddyCell  = (ContactCell *)[self.tableView cellForRowAtIndexPath:quickMenuSelectedIndex];
      //  NSLog(@"%@",buddyCell.contactNameLbl.text);
        
    }
    
    XMPPUserCoreDataStorageObject *userdetails;
    
    if (!isSearching)
        userdetails =[[self fetchedResultsController] objectAtIndexPath:quickMenuSelectedIndex];
    else
        userdetails =[[self searchFetchControl:contactPredicate] objectAtIndexPath:quickMenuSelectedIndex];
    
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
                     {
                         [self.tableView scrollRectToVisible:[self.tableView rectForRowAtIndexPath:quickMenuSelectedIndex] animated:NO];
                     }
                         completion:^(BOOL finished){
                         UIView* view = gesture.view;
                         
                         CGPoint p = [view convertPoint:view.frame.origin toView:self.view.superview];
                         
                         quickMenu* quiMenu = [[quickMenu alloc] initFrame:self.view.superview topPoint:p.y + 4 bottomPoint:p.y+view.frame.size.height-13 TypeOfMenu:@"singleChat"];
                             quiMenu.fromVC = @"Contact";
                         
                         [self.view.superview addSubview:quiMenu];
                     //    NSLog(@"top  %f  bottom  %f",p.y,p.y+view.frame.size.height);
                         
                         
                     }];
    userdetails = nil;
    
    
}
-(void)quickMenuClicked:(NSNotification *)notification
{
    NSDictionary *dict = [notification userInfo];
    
    
    NSInteger selectedButtonNo = [[dict objectForKey:QUICK_MENU_FOR_CONTACT]integerValue];
    
    XMPPUserCoreDataStorageObject *userdetails;
    
    if (!isSearching)
        userdetails =[[self fetchedResultsController] objectAtIndexPath:quickMenuSelectedIndex];
    else
        userdetails =[[self searchFetchControl:contactPredicate] objectAtIndexPath:quickMenuSelectedIndex];
    
    
  //  NSLog(@"Uer Details: %@",userdetails);
    
    
    switch (selectedButtonNo)
    {
        case IsEqualCall:
            
            
        {
                //NSLog(@"Calling %@",userdetails.jid.user);
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",userdetails.jid.user]]];
           //     NSLog(@"Calling %@",[NSString stringWithFormat:@"telprompt://%@",userdetails.jid.user]);

        }
            
            //
            break;
        case IsEqualChat:
            
       //     NSLog(@"quickMenuClicked Chat");
            
          
                if (userdetails)
                {
                    chatViewController* singleChatView = [chatViewController new];
                    singleChatView.user = userdetails;
                    [self.navigationController pushViewController:singleChatView animated:YES];
                }
            
            
            
            //
            break;
        case IsEqualProfile:
            {
                if (userdetails)
                {
                    ProfileViewController * profile = [ProfileViewController new];
                    profile.userDetails = userdetails;
                    [self.navigationController pushViewController:profile animated:YES];
                }
             }
    
            //
            break;
        default:
            //
            break;
    }
    userdetails = nil;

}






@end
