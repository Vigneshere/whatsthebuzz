//
//  FavoriteTableViewController.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 15/04/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FavoriteTabledelegate <NSObject>
- (void)didSelectContacts:(XMPPUserCoreDataStorageObject *)user;
@end
@interface FavoriteTableViewController : UITableViewController
@property (nonatomic, weak)  id<FavoriteTabledelegate> favoriteDelegate;

@end
