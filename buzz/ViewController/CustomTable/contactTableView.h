//
//  contactTableView.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 06/03/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AvatarImage.h"
@protocol ContactTabledelegate <NSObject>
- (void)didSelectContacts:(XMPPUserCoreDataStorageObject *)user;
@end
@interface contactTableView : UITableView<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,NSFetchedResultsControllerDelegate>
{
    AvatarImage *avatarImg;
    NSFetchedResultsController *fetchedResultsController;
    NSFetchedResultsController *searchFetchControl;
    BOOL isSearching;
    NSPredicate *contactPredicate;
}
@property(nonatomic,retain)UISearchBar *searchBar;
@property (nonatomic, weak)  id<ContactTabledelegate> contactsdelegate;
@property (nonatomic, assign)  NSString *viewControllerStr;
@property (nonatomic, retain)  NSMutableArray *groupMemberArray;
@property (nonatomic, retain)  NSMutableArray *groupMemberList;
@property (nonatomic)  int  MaxMember;






@end
