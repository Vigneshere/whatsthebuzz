//
//  contactTableView.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 06/03/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "contactTableView.h"
#import "ContactCell.h"
#import "XMPPUserCoreDataStorageObject.h"

@implementation contactTableView
@synthesize contactsdelegate;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Appdelegate Values
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (AppDelegate *)appDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (XMPPRosterCoreDataStorage *)xmppRosterStorage
{
    return [[self appDelegate] xmppRosterStorage];
}
- (XMPPStream *)xmppStream
{
    return [[self appDelegate] xmppStream];
}
- (XMPPRoster *)xmppRoster
{
    return [[self appDelegate] xmppRoster];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Initialization
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.delegate = self;
        self.dataSource = self;
        self.backgroundColor = [UIColor clearColor];
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.showsVerticalScrollIndicator  = NO;
        
        //-->Search Bar for TableView
        _searchBar = [UISearchBar new];
        _searchBar.delegate = self;
        [_searchBar sizeToFit];
        _searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
        _searchBar.backgroundColor = [UIColor clearColor];
        _searchBar.placeholder = @"Search";
        _searchBar.tag = 1;
        
        _searchBar.tintColor = RGBA(53, 152, 219, 1);
        
        _searchBar.backgroundImage = [UIImage imageWithColor:[UIColor clearColor]];
        
        NSString *ver = [[UIDevice currentDevice] systemVersion];
        int ver_int = [ver intValue];
        
        if (ver_int < 7) {
            _searchBar.barStyle = UIBarStyleDefault;
        }
        
        else {
            _searchBar.searchBarStyle = UISearchBarStyleProminent;
            _searchBar.barTintColor =[UIColor clearColor];
        }
        self.tableHeaderView = _searchBar;
        
        
        avatarImg = [AvatarImage new];
        isSearching = NO;
        _groupMemberArray = [NSMutableArray new];
        [[self xmppStream] addDelegate:self delegateQueue:dispatch_get_main_queue()];
        [self setNeedsLayout];
        
    }
    return self;
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect barFrame = self.searchBar.frame;
    barFrame.origin.x = 10;
    barFrame.size.width = self.frame.size.width-10;
    self.searchBar.frame = barFrame;
    
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - TableView DataSource Method
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    if (!isSearching)
        return [[[self fetchedResultsController] sections] count];
    else
        return 1;
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (!isSearching)
    {
        if ([self.dataSource tableView:tableView numberOfRowsInSection:section] == 0) {
            return nil;
        }
        NSArray *sections = [[self fetchedResultsController] sections];
        if (section < [sections count])
        {
            id <NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
            return sectionInfo.name;
        }
    }
    return nil;
}
-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    
    if (!isSearching)
    {
        NSArray *sections = [[self fetchedResultsController] sections];
        NSMutableArray *arrayfilter = [NSMutableArray new];
        for (int i = 0; i < sections.count; i++) {
            id <NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:i];
            [arrayfilter addObject:sectionInfo.name];
        }
        return arrayfilter;
    }
    else
        return 0;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(!isSearching)
    {
        NSArray *sections = [[self fetchedResultsController] sections];
        if (section < [sections count])
        {
            id <NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
            return sectionInfo.numberOfObjects;
        }
    }
    else
        return searchFetchControl.fetchedObjects.count;
    
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    
    // Background color
    view.tintColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.3];
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor orangeColor]];
    
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - TableView DataSource cellForRowAtIndexPath
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self respondsToSelector:@selector(setSectionIndexColor:)])
    {
        tableView.sectionIndexBackgroundColor = [UIColor clearColor];
        tableView.sectionIndexTrackingBackgroundColor = [UIColor clearColor];
    }
    static NSString *CellIdentifier = @"Cell";
    ContactCell *cell = (ContactCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[ContactCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    XMPPUserCoreDataStorageObject *user;
    if(!isSearching)
        user  = [[self fetchedResultsController] objectAtIndexPath:indexPath];
    else
        user = [[self searchFetchControl:contactPredicate] objectAtIndexPath:indexPath];
    cell.contactNameLbl.text = user.displayName;
    cell.contactImageView.image = [avatarImg profileImage:user];
    XMPPResourceCoreDataStorageObject *userResource;
    if ([user isOnline]) {
        for (userResource in user.resources) {
            
            if (userResource.status != nil)
                cell.statusLabel.text = userResource.status;
            else
                cell.statusLabel.text = userResource.type;
        }
    }
    else
        cell.statusLabel.text = @"unavailable";
    
    
    if ([self.viewControllerStr isEqualToString:@"createGroupController"])
    {
        cell.unreadMessageLabel.hidden = YES;
        if ([_groupMemberArray containsObject:user])
            cell.checkView.backgroundColor = RGBA(53, 152, 219, 1);
        else
            cell.checkView.backgroundColor = [UIColor whiteColor];
    }
    else if([self.viewControllerStr isEqualToString:@"ViewController"])
    {
        [cell.checkView removeFromSuperview];
        cell.checkView = nil;
    }
    [cell.favoriteButton removeFromSuperview];
    return cell;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//#pragma mark - TableView Editing Method
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
//    return NO;
//}
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (editingStyle == UITableViewCellEditingStyleDelete)
//    {
//            XMPPUserCoreDataStorageObject *user = [[self fetchedResultsController] objectAtIndexPath:indexPath];
//            [[self xmppRoster] unsubscribePresenceFromUser:user.jid];
//    }
//}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Core Data
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (NSManagedObjectContext *)managedObjectContext_roster
{
    return [[[self appDelegate] xmppRosterStorage] mainThreadManagedObjectContext];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark DataBase Methods For Contacts
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (NSFetchedResultsController *)fetchedResultsController
{
    if([self.viewControllerStr isEqualToString:@"createGroupController"])
    {
        fetchedResultsController = nil;
    }
    
    if (fetchedResultsController != nil) {
        return  fetchedResultsController;
    }
    NSManagedObjectContext *moc = [self  managedObjectContext_roster];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:moc];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:[NSSortDescriptor sortValues:@"displayName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]];
    fetchRequest.returnsDistinctResults = YES;
    
    
    if([self.viewControllerStr isEqualToString:@"createGroupController"])
    {
        NSString *fr = @"from";
        NSMutableArray *prepredicateArray = [NSMutableArray new];

        if (_groupMemberList.count >0)
        {
        
        for (NSString *jidStr in _groupMemberList)
        {
            NSString *predicateFrmt = @"subscription != %@ AND jidStr != %@";
            NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, fr , jidStr];
            [prepredicateArray addObject:predicate];
        }
            
            fetchRequest.predicate=[NSCompoundPredicate andPredicateWithSubpredicates:prepredicateArray];

        }
        else
        {
            NSString *fr = @"from";
            NSString *predicateFrmt = @"subscription != %@";
            NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, fr];
            [fetchRequest setPredicate:predicate];
        }
        
    }
    else
    {
        NSString *fr = @"from";
        NSString *predicateFrmt = @"subscription != %@";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, fr];
        [fetchRequest setPredicate:predicate];
        
    }
    
    fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                   managedObjectContext:moc
                                                                     sectionNameKeyPath:@"sectionName"
                                                                              cacheName:nil];
    [fetchedResultsController setDelegate:self];
    
    NSError *error = nil;
    if (![fetchedResultsController performFetch:&error])
    {
        
    }
    
    return fetchedResultsController;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark DataBase Methods For Searching
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (NSFetchedResultsController *)searchFetchControl:(NSPredicate *)predicate
{
    searchFetchControl = nil;
    NSManagedObjectContext *moc = [self  managedObjectContext_roster];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:moc];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    fetchRequest.predicate = predicate;
    [fetchRequest setSortDescriptors:[NSSortDescriptor sortValues:@"displayName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]];
    
    searchFetchControl = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest  managedObjectContext:moc sectionNameKeyPath:nil  cacheName:nil];
    NSError *error = nil;
    if (![searchFetchControl performFetch:&error])
    {
    }
    return searchFetchControl;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self beginUpdates];
}
- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    switch(type)
    {
        case NSFetchedResultsChangeInsert:
            [self insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
            
        case NSFetchedResultsChangeMove:
            [self deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            [self insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
    }
}
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self endUpdates];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Search Implementation
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    //Remove all objects first.
    if([searchText length] != 0)
    {
        isSearching = YES;
        [self searchTableList];
    }
    else
        isSearching = NO;
    [self reloadData];
    [searchBar becomeFirstResponder];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    isSearching = NO;
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
    [self reloadData];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self searchTableList];
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
    
}
//Search the array
- (void)searchTableList {
    NSString *searchString = _searchBar.text;
    NSString *fr = @"from";
    contactPredicate = [NSPredicate predicateWithFormat:@"displayName contains[c] %@ AND subscription != %@",searchString,fr];
    [self searchFetchControl:contactPredicate];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - TableView Delegate didSelectRowAtIndexPath
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if([self.viewControllerStr isEqualToString:@"ViewController"])
    {
        XMPPUserCoreDataStorageObject *userdetails;
        if(!isSearching)
            userdetails = [[self fetchedResultsController] objectAtIndexPath:indexPath];
        else
            userdetails = [[self searchFetchControl:contactPredicate] objectAtIndexPath:indexPath];
        if([contactsdelegate respondsToSelector:@selector(didSelectContacts:)])
            [contactsdelegate didSelectContacts:userdetails];
    }
    else if([self.viewControllerStr isEqualToString:@"createGroupController"])
    {
        XMPPUserCoreDataStorageObject *user;
        if(!isSearching)
            user  = [[self fetchedResultsController] objectAtIndexPath:indexPath];
        else
            user = [[self searchFetchControl:contactPredicate] objectAtIndexPath:indexPath];
        
        if ([_groupMemberArray containsObject:user])
        {
            [_groupMemberArray removeObject:user];
        }
        else if (_MaxMember) {
            
            if ([self.groupMemberArray count] < _MaxMember)
            {
                [_groupMemberArray addObject:user];
                [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation: UITableViewRowAnimationFade];
                
            }
            
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ALERTVIEW_TITLE message:@"Maximum members selected" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                alert = nil;
            }
            
        }
        
        
        else
            [_groupMemberArray addObject:user];
        [self reloadRowsAtIndexPaths:@[indexPath] withRowAnimation: UITableViewRowAnimationFade];
    }
}
@end