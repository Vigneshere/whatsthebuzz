//
//  ViewController.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 18/02/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "ViewController.h"
#import "ContactCell.h"
#import "ProfileViewController.h"
#import "AppDelegate.h"
//#import "contactTableView.h"
#import "chatViewController.h"
#import "ProfileViewController.h"
#import "GroupChatViewController.h"
#import "MSViewControllerSlidingPanel.h"
#import "SettingViewController.h"
#import "contactTableViewController.h"
#import "ExpandableNavigation.h"
#import "AboutViewController.h"
#import "StatusViewController.h"
#import "profileFormViewController.h"
#import "createGroupController.h"
#import "FavoriteTableViewController.h"
#import "InviteSettingViewController.h"
#import "SecureGalleryViewController.h"
#import "Header.h"
#import "UIColor+HexColors.h"
#import "MasterPinView.h"
#import "Singleton.h"
#import "ACStatusBarOverlayWindow.h"
@class AppDelegate;

@interface ViewController ()<RecentChatTabledelegate,ContactTabledelegate,UIGestureRecognizerDelegate,FavoriteTabledelegate,UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    UISwipeGestureRecognizer* SwipeRight_Reg;
    UISwipeGestureRecognizer* swipeLeftGesture;
    UIActivityIndicatorView* activityView;
    UIView *securityQuestionView;
    NSArray *securityQuestionListArr;
    UITextField *pickerTxtField,*keyTextField;
    UIPickerView *selectionPickr;
    UIButton *securitySubmitBtn;
    UILabel *securityTitleLbl;
    Singleton  *singleton;
    
}

@property(nonatomic,strong)contactTableViewController *contactTable;
@property(nonatomic,strong)chatViewController *singleChatView;
@property(nonatomic,strong)GroupChatViewController *groupChatView;
@property (retain) ExpandableNavigation* navigation;
@property(nonatomic,strong)GroupInformationViewController *groupInformation;


@end
@implementation ViewController{
    UIButton* sosBtn,*contactButton;
    UIButton *menuButton;
    BOOL isRightPanelShowing;
}

@synthesize recentChatTableview;
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Appdelegate Values
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (AppDelegate *)appDelegate
{
	return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (XMPPStream *)xmppStream
{
	return [[self appDelegate] xmppStream];
}
- (XMPPRoster *)xmppRoster
{
	return [[self appDelegate] xmppRoster];
}
- (XMPPRosterCoreDataStorage *)xmppRosterStorage
{
	return [[self appDelegate] xmppRosterStorage];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - View Life Cycle
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(IS_IPHONE_4)
    {
        self.view.backgroundColor = BACKGROUNDIMAGE_4;
        
    }
    else
    {
        self.view.backgroundColor = BACKGROUNDIMAGE;
        
    }
    
    self.navigationItem.hidesBackButton = YES;
    if (![[self appDelegate] xmppStream])
    {
        [[self appDelegate] setupStream];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:@"123" forKey:PASSWORD_XMPP];
    [[self appDelegate] connect];
    
    singleton = [Singleton sharedMySingleton];

    if ([singleton Avatar_Img] != nil)

    {
    AvatarImage* avaImg_Obj = [AvatarImage new];
    [avaImg_Obj updateAvatar:singleton.Avatar_Img];
    }
    
    
	[[self xmppStream] addDelegate:self delegateQueue:dispatch_get_main_queue()];
    recentChatTableview = [RecentChatTableView new];
    SwipeRight_Reg = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    swipeLeftGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    
    sosBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    //-->Segment ControlView
    sosBtn.frame = CGRectMake(10, self.navigationController.navigationBar.frame.size.height/2 - 17.5, 35, 35);
    [sosBtn setTitle:@"SOS" forState:UIControlStateNormal];
    [sosBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sosBtn addTarget:self action:@selector(sosBtn_CLICKED) forControlEvents:UIControlEventTouchUpInside];
    //[sosBtn  setTitleColor:RGBA(53, 152, 219, 1) forState:UIControlStateNormal];
    sosBtn.titleLabel.font = [UIFont fontWithName:FontString size:13];//[UIFont fontWithName:FontString size:14];
    sosBtn.clipsToBounds = YES;
    sosBtn.clearsContextBeforeDrawing = YES;
    [sosBtn setBackgroundColor:RGBA(90, 165, 1, 1)];
    sosBtn.layer.cornerRadius = 35/2;
    sosBtn.layer.borderColor =  [UIColor clearColor].CGColor;
    sosBtn.layer.borderWidth = 2.0f;
    
    
    UIImage* contactButtonimage3 = [UIImage imageNamed:@"add-contact.png"];
    CGRect contactButtonimage3frameimg = CGRectMake(self.navigationController.navigationBar.frame.size.width-contactButtonimage3.size.width - 5, (self.navigationController.navigationBar.frame.size.height/2) - 20 , 40 ,40);
    contactButton = [UIButton buttonWithType:UIButtonTypeCustom];
    contactButton.frame = contactButtonimage3frameimg;
    [contactButton setImage:contactButtonimage3 forState:UIControlStateNormal];
    [contactButton addTarget:self action:@selector(contactButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImage *menuImageIcon = [UIImage imageNamed:@"menu-icon"];
    menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    menuButton.backgroundColor = [UIColor clearColor];
    menuButton.frame = CGRectMake(self.view.frame.size.width-60, 0, 60, 40);
    [menuButton setImage:menuImageIcon forState:UIControlStateNormal];
    menuButton.imageEdgeInsets = UIEdgeInsetsMake( 10.0, 5.0, 0.0, 0.0);
    [menuButton addTarget:self action:@selector(settingBtnAction) forControlEvents:UIControlEventTouchDown];
    
    recentChatTableview.recentChatdelegate = self;
    [self.view addSubview:recentChatTableview];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isFirstTime"]) {
        activityView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityView.center= contactButton.center;
        [activityView startAnimating];
        [self.navigationController.navigationBar addSubview:activityView];
        [self performSelector:@selector(addContactButton) withObject:nil afterDelay:10.0];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(TEMPNOTI:) name:@"TEMPNOTI" object:nil];
    
    


}
-(void)viewWillAppear:(BOOL)animated
{
    
    [SwipeRight_Reg setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [recentChatTableview addGestureRecognizer:SwipeRight_Reg];
    [swipeLeftGesture setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [recentChatTableview addGestureRecognizer:swipeLeftGesture];
    [recentChatTableview.searchBar resignFirstResponder];

    [self.navigationController.navigationBar addSubview:sosBtn];
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isFirstTime"])
        [self.navigationController.navigationBar addSubview:contactButton];
    //[self.navigationController.navigationBar addSubview:menuButton];
    recentChatTableview.frame = CGRectMake(0, 0, self.view.frame.size.width,self.view.frame.size.height);

    [recentChatTableview reloadData];
    isRightPanelShowing = NO;
    
    NSString *optedAnswer = [[NSUserDefaults standardUserDefaults]valueForKey:@"MASTER_ANSWER"];
    if (optedAnswer.length == 0)
    {
        [self securityQuestion];
        self.navigationController.navigationBarHidden = YES;
    }
    else
    {
        [self addArcMenu];
        self.navigationController.navigationBarHidden = NO;

    }
    

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addArcMenu) name:@"MASTERPWDSET" object:nil];
}

-(void)securityQuestion
{
    MasterPinView *masterPin = [[MasterPinView alloc]initWithFrame_onView:self.view Title:@"Answer The Question" Entry:@"Setting"];
    [masterPin createMasterView];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == pickerTxtField)
    {
        [self pickerInitializationMethod];
        return NO;
    }
    else if (textField == keyTextField)
    {
        keyTextField.placeholder = @"";
        keyTextField.font = [UIFont systemFontOfSize:13];
    }

    return YES;
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    

    
    return YES;
}


-(void)pickerInitializationMethod
{
    selectionPickr=[[UIPickerView alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)-140, self.view.frame.origin.y+203, 280,30)];
    selectionPickr.showsSelectionIndicator=YES;
    selectionPickr.backgroundColor=[UIColor whiteColor];
    selectionPickr.layer.borderColor=[[UIColor grayColor] CGColor];
    selectionPickr.layer.borderWidth=1.5;
    selectionPickr.layer.cornerRadius=10;
    selectionPickr.showsSelectionIndicator=YES;
    selectionPickr.delegate=self;
    selectionPickr.dataSource=self;
    [securityQuestionView addSubview:selectionPickr];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component

{
    return [securityQuestionListArr count];
}


- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* tView = (UILabel*)view;
    if (!tView)
    {
        tView = [[UILabel alloc] init];
        [tView setFont:[UIFont systemFontOfSize:13]];
        [tView setTextAlignment:NSTextAlignmentCenter];
        tView.numberOfLines=2;
    }
    // Fill the label text here
    tView.text=[securityQuestionListArr objectAtIndex:row];
    return tView;
}


-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
   pickerTxtField.text = [securityQuestionListArr objectAtIndex:row];
    pickerTxtField.font = [UIFont systemFontOfSize:12];
    pickerTxtField.textAlignment = NSTextAlignmentCenter;
    selectionPickr.hidden=YES;
    
}

-(void)showKeyTextField
{
    keyTextField = [[UITextField alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)-150, self.view.frame.origin.y+ 210, 300,50)];
    [keyTextField setBorderStyle:UITextBorderStyleRoundedRect];
    
    keyTextField.layer.borderWidth=0.5f;
    keyTextField.textAlignment = NSTextAlignmentCenter;
    keyTextField.layer.cornerRadius=12;
    keyTextField.font = [UIFont systemFontOfSize:13];
    keyTextField.placeholder = @"Enter a Security Answer..";
    keyTextField.layer.borderColor = [[UIColor whiteColor]CGColor ];
    keyTextField.layer.borderWidth = 0.5f;
    [keyTextField setBackgroundColor:[UIColor whiteColor]];
    keyTextField.delegate = self;
    [securityQuestionView addSubview:keyTextField];
    
}
-(void)viewDidAppear:(BOOL)animated
{
    singleton = [Singleton sharedMySingleton];
    if ([[self xmppStream] isConnected] && singleton.Avatar_Img) {
        AvatarImage* avaImg_Obj = [AvatarImage new];
        [avaImg_Obj updateAvatar:singleton.Avatar_Img];
        singleton.Avatar_Img = nil;
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
  //  NSLog(@"before 1%@",[UIApplication sharedApplication].windows);

    [sosBtn removeFromSuperview];
    [menuButton removeFromSuperview];
    [contactButton removeFromSuperview];
    [self.navigation.mainButton removeFromSuperview];
    for (UIButton* btn in self.navigation.menuItems) {
        [btn removeFromSuperview];
    }
    

}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"TEMPNOTI" object:nil];
}
-(void)addContactButton
{

    [activityView stopAnimating];
    [self.navigationController.navigationBar addSubview:contactButton];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstTime"];
}

-(void)addArcMenu
{
    self.navigationController.navigationBarHidden = NO;

    UIImage* btn1img = [UIImage imageNamed:@"arc_menu_secure_gallery"];
    UIButton* btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn1.frame = CGRectMake(0, 0, btn1img.size.width,  btn1img.size.height);
    [btn1 setImage:btn1img forState:UIControlStateNormal];
    btn1.tag = 1;
    [btn1 addTarget:self action:@selector(touchMenuItem:) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:btn1];
    btn1img = nil;
    
    UIImage* btn2img = [UIImage imageNamed:@"info"];
    UIButton* btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn2.frame = CGRectMake(0, 0, btn2img.size.width,  btn2img.size.height);
    [btn2 setImage:btn2img forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(touchMenuItem:) forControlEvents:UIControlEventTouchDown];
    btn2.tag = 2;
    [self.view addSubview:btn2];
    btn2img = nil;
    
    UIImage* btn3img = [UIImage imageNamed:@"groupchat"];
    UIButton* btn3 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn3.frame = CGRectMake(0, 0, btn3img.size.width,  btn3img.size.height);
    [btn3 setImage:btn3img forState:UIControlStateNormal];
    [btn3 addTarget:self action:@selector(touchMenuItem:) forControlEvents:UIControlEventTouchDown];
    btn3.tag = 3;
    [self.view addSubview:btn3];
    btn3img = nil;
    
    UIImage* btn4img = [UIImage imageNamed:@"setting"];
    UIButton* btn4 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn4.frame = CGRectMake(0, 0, btn4img.size.width,  btn4img.size.height);
    [btn4 setImage:btn4img forState:UIControlStateNormal];
    [btn4 addTarget:self action:@selector(touchMenuItem:) forControlEvents:UIControlEventTouchDown];
    btn4.tag = 4;
    [self.view addSubview:btn4];
    btn4img = nil;
    
    UIImage* btn5img = [UIImage imageNamed:@"adduser"];
    UIButton* btn5 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn5.frame = CGRectMake(0, 0, btn5img.size.width,  btn5img.size.height);
    [btn5 setImage:btn5img forState:UIControlStateNormal];
    [btn5 addTarget:self action:@selector(touchMenuItem:) forControlEvents:UIControlEventTouchDown];
    btn5.tag = 5;
    [self.view addSubview:btn5];
    btn5img = nil;
    
    UIImage* mainbtnimg = [UIImage imageNamed:@"arcmenubtn"];
    UIButton* mainbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    mainbtn.frame = CGRectMake((self.view.frame.size.width - mainbtnimg.size.width)-3 , (self.view.frame.size.height - mainbtnimg.size.height)-3, mainbtnimg.size.width,  mainbtnimg.size.height);
    [mainbtn setImage:mainbtnimg forState:UIControlStateNormal];
    [self.view addSubview:mainbtn];
    mainbtnimg = nil;
    
    NSArray* buttons = [NSArray arrayWithObjects:btn1, btn2, btn3, btn4,btn5, nil];
    
    self.navigation = [[ExpandableNavigation alloc] initWithMenuItems:buttons mainButton:mainbtn radius:140.0];
}

- (void) touchMenuItem:(id)sender {
    
    // if the menu is expanded, then collapse it when an menu item is touched.
        
    UIButton* btn = (UIButton*)sender;
   // NSLog(@"%ld",(long)btn.tag);
    if ((long)btn.tag == 1)
    {
        SecureGalleryViewController *secureGallery = [SecureGalleryViewController new];
        [self.navigationController pushViewController:secureGallery animated:YES];
        
    }
    else if((long)btn.tag ==  2)
    {
        StatusViewController *status = [StatusViewController new];
        [self.navigationController pushViewController:status animated:YES];
        
    }
    else if((long)btn.tag == 3)
    {
        createGroupController *group = [createGroupController new];
        [self.navigationController pushViewController:group animated:YES];

    }
    else if((long)btn.tag == 4)
    {
        SettingViewController *setting = [SettingViewController new];
        [self.navigationController pushViewController:setting animated:YES];
    }
    else if((long)btn.tag == 5)
    {
        InviteSettingViewController *inviteContact = [InviteSettingViewController new];
        [self.navigationController pushViewController:inviteContact animated:YES];

    }
    if( self.navigation.expanded ) {
        [self.navigation collapse];
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Action For handleSwipeFrom
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(void)handleSwipeFrom :(UISwipeGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
    [self hideQuickMenu];


    if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft)
    [[self slidingPanelController] openRightPanel];
    else if(recognizer.direction == UISwipeGestureRecognizerDirectionRight)
    {
        FavoriteTableViewController* fav = [FavoriteTableViewController new];
        fav.favoriteDelegate = self;
    [[self slidingPanelController] openLeftPanel];
    }
}
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;
}





-(void)sosBtn_CLICKED
{
    [self.view endEditing:YES];
    [self hideQuickMenu];


    [[self slidingPanelController] openLeftPanel];
}
-(void)favoriteMenuBtnAction
{
    [self.view endEditing:YES];
    [self hideQuickMenu];

    if (isRightPanelShowing)
    {
        [[self slidingPanelController] closePanel];
        isRightPanelShowing  = NO;
        }
    else
    {
    [[self slidingPanelController] openRightPanel];
        isRightPanelShowing  = YES;
    }

}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark XMPPSreamDelegate
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq
{
    return NO;
}
- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence
{
}
-(void)contactButtonAction:(id)sender
{
//    [[self appDelegate]StartUpdateRoster];
    [self.view endEditing:YES];
    [self hideQuickMenu];
    if (!self.contactTable)
    self.contactTable = [contactTableViewController new];
    self.contactTable.viewControllerStr = @"ViewController";
    self.contactTable.contactsdelegate = self;

    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:self.contactTable];
    [self presentViewController:navigationController animated:YES completion:nil];
}

-(void)TEMPNOTI:(NSNotification *)notification
{
    NSDictionary *dict = [notification userInfo];
    
   // NSLog(@"2");
    if ([dict objectForKey:@"userdetails"] )
    {
        XMPPUserCoreDataStorageObject *userdetails = (XMPPUserCoreDataStorageObject*)[dict objectForKey:@"userdetails"];
        [self didSelectRecentChat1:userdetails];
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Did select Recent Chat item(RecentChat Delegate)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)didSelectRecentChat1:(XMPPUserCoreDataStorageObject *)userdetails
{
    if (self.singleChatView)
        self.singleChatView = nil;
    
        self.singleChatView = [chatViewController new];
    self.singleChatView.user = userdetails;
    

    [self.navigationController pushViewController:self.singleChatView animated:YES];

    if (self.presentedViewController)
    {
        [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
        
    }
    [[self slidingPanelController] closePanel];
    [self appDelegate].navigationBarViewLine.hidden = NO;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Did select Recent Chat item(RecentChat Delegate)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)didSelectRecentChat:(XMPPUserCoreDataStorageObject *)userdetails
{
    [recentChatTableview.searchBar setShowsCancelButton:NO animated:YES];


    if (!self.singleChatView)
        self.singleChatView = [chatViewController new];
    self.singleChatView.user = userdetails;
    [self.navigationController pushViewController:self.singleChatView animated:YES];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Did select Contact (Contact Delegate)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)didSelectContacts:(XMPPUserCoreDataStorageObject *)userdetails
{
    [recentChatTableview.searchBar setShowsCancelButton:NO animated:YES];

    
    ProfileViewController * profile = [ProfileViewController new];
    profile.userDetails = userdetails;
    [self.navigationController pushViewController:profile animated:YES];

    
}
-(void)didSelectGroup:(XMPPJID *)roomJID :(NSString *)roomTitle
{
    [recentChatTableview.searchBar setShowsCancelButton:NO animated:YES];

    if(!self.groupInformation)
        self.groupInformation = [GroupInformationViewController new];
    self.groupInformation.roomJID =  roomJID;
    self.groupInformation.titleString = roomTitle;
    [self.navigationController pushViewController:self.groupInformation animated:YES];
}


-(void)didSelectRecentGroupChat:(XMPPJID *)user
{
    [recentChatTableview.searchBar setShowsCancelButton:NO animated:YES];

    if (!self.groupChatView)
        self.groupChatView = [GroupChatViewController new];
     self.groupChatView.user = user;
    [self.navigationController pushViewController: self.groupChatView animated:YES];
}

-(void)didSelectUnkownContact:(XMPPMessageArchiving_Contact_CoreDataObject *)coreMessage
{
    ABRecordRef aContact = ABPersonCreate();
    CFErrorRef anError = NULL;
    ABMultiValueRef email = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    
    NSString *JIDStr = coreMessage.bareJidStr;
    JIDStr = [JIDStr stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"@%@",DOMAINAME] withString:@""];
    // bool didAdd = ABMultiValueAddValueAndLabel(email,(__bridge CFTypeRef)(JIDStr), kABOtherLabel, NULL);
    
    ABMutableMultiValueRef multiPhone = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    
    bool didAddPhone = ABMultiValueAddValueAndLabel(multiPhone, (__bridge CFTypeRef)(JIDStr), kABPersonPhoneMobileLabel, NULL);
    
    if(didAddPhone){
        
        ABRecordSetValue(aContact, kABPersonPhoneProperty, multiPhone,nil);
        
      //  NSLog(@"Phone Number saved......");
        
        
        ABRecordSetValue(aContact, kABPersonEmailProperty, email, &anError);
        if (anError == NULL)
        {
            ABUnknownPersonViewController *picker = [[ABUnknownPersonViewController alloc] init];
            picker.unknownPersonViewDelegate = self;
            picker.displayedPerson = aContact;
            picker.allowsAddingToAddressBook = YES;
            picker.allowsActions = YES;
            picker.alternateName = @"";
            picker.title = coreMessage.bareJid.user;
            picker.message = @"WhatstheBuzz";
            
            [self.navigationController pushViewController:picker animated:YES];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Could not create unknown user"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Cancel"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
    CFRelease(email);
    CFRelease(aContact);
    
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Setting Method
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)settingBtnAction
{
    SettingViewController *setting = [SettingViewController new];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:setting];
    setting.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:navigationController animated:YES completion:nil];
    setting = Nil;
    navigationController = Nil;
}
#pragma mark MSSlidingPanelControllerDelegate protocol
/** @name MSSlidingPanelControllerDelegate protocol */

/**
 *  Tells the delegate that the specified side begins to bring out.
 *
 *  @param panelController The panel controller.
 *  @param side            The side.
 */
- (void)slidingPanelController:(MSSlidingPanelController *)panelController beginsToBringOutSide:(MSSPSideDisplayed)side
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

/**
 *  Tells the delegate that the specified side has been closed.
 *
 *  @param panelController The panel controller.
 *  @param side            The side.
 */
- (void)slidingPanelController:(MSSlidingPanelController *)panelController hasClosedSide:(MSSPSideDisplayed)side
{
    
    fromSOS = NO;
}

/**
 *  Tells the delegate that the specified side has been opened.
 *
 *  @param panelController The panel controller.
 *  @param side            The side.
 */
- (void)slidingPanelController:(MSSlidingPanelController *)panelController hasOpenedSide:(MSSPSideDisplayed)side
{
    
}

-(void)hideQuickMenu
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"HideQuickMenu" object:nil userInfo:[NSMutableDictionary new]];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
   // NSLog(@"RECEIVED MEMORY WARNINGS IN CONTACTS");
    
    // Dispose of any resources that can be recreated.
}
@end
