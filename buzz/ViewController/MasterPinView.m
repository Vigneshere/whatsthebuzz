//
//  MasterPinView.m
//  buzz
//
//  Created by Vignesh on 05/05/15.
//  Copyright (c) 2015 Shailisolutions. All rights reserved.
//

#import "MasterPinView.h"
#import "UIColor+HexColors.h"
#import "UIImageEffects.h"

@implementation MasterPinView
{
    UIImageView *pickerDropDownImgVw;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

-(id)initWithFrame_onView:(UIView *)onview Title:(NSString *)titleStr Entry:(NSString *)Password
{
    self = [super initWithFrame:CGRectMake(0, 0, onview.frame.size.width, onview.frame.size.height)];
    if (self) {
        self.onView = onview;
        entryStr = Password;
        titleString = titleStr;
    }
    return self;
}

-(void)createMasterView
{
    
    
    //** Security Question View
    securityQuestionView  = [[UIView alloc]initWithFrame:CGRectMake(0, self.frame.size.height, self.frame.size.width, self.frame.size.height - 20)];
    [self addSubview:securityQuestionView];
    
    
    UIView *questionAnswerView = [[UIView alloc]initWithFrame:CGRectMake((self.frame.size.width)/2 - 125 , self.frame.origin.y + 150, 250, 200)];
    questionAnswerView.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.8];
    questionAnswerView.layer.cornerRadius = 9.0;
    
    [securityQuestionView addSubview:questionAnswerView];
    
    //** Array with Security Questions
    
    securityQuestionListArr    = [[NSArray alloc]initWithObjects:@"What was your childhood nickname?",@"Who was your best childhood friend?",@"What was the name of your first pet?",@"What's ur favorite team?",@"What is your favorite movie?",@"What was the model of your first car?",@"Who is your childhood sports hero?", nil];
    
    
    if ([entryStr isEqualToString:@"Setting"])
    {
        
        pickerDropDownImgVw.hidden = NO;
        pickerTxtField = [[UITextField alloc]initWithFrame:CGRectMake(20, 50, questionAnswerView.frame.size.width,40)];
        [pickerTxtField setBorderStyle:UITextBorderStyleNone];
        pickerTxtField.textAlignment = NSTextAlignmentLeft;
        pickerTxtField.font = [UIFont systemFontOfSize:15];
        pickerTxtField.placeholder = @"Select a Question";
        pickerTxtField.layer.borderColor = [[UIColor clearColor]CGColor ];
        pickerTxtField.textColor = [UIColor blackColor];
        [pickerTxtField setBackgroundColor:[UIColor clearColor]];
        pickerTxtField.rightViewMode = UITextFieldViewModeAlways;// Set rightview mode
        pickerTxtField.delegate = self;
        
    }
    else
    {
        pickerTxtField = [[UITextField alloc]initWithFrame:CGRectMake(20, 50, questionAnswerView.frame.size.width,40)];
        [pickerTxtField setBorderStyle:UITextBorderStyleNone];
        pickerTxtField.layer.borderWidth=0.5f;
        pickerTxtField.textAlignment = NSTextAlignmentLeft;
        pickerTxtField.font = [UIFont systemFontOfSize:13];
        pickerTxtField.placeholder = @"CHOOSE QUESTION";
        pickerTxtField.layer.borderColor = [[UIColor clearColor]CGColor ];
        pickerTxtField.textColor = [UIColor blackColor];
        pickerTxtField.rightViewMode = UITextFieldViewModeAlways;// Set rightview mode
        pickerTxtField.text = [[NSUserDefaults standardUserDefaults]valueForKey:@"MASTER_QUESTION"];
        pickerTxtField.delegate = self;
    }
    
    //** Title Label
    UILabel  *securityTitleLbl = [[UILabel alloc]init];
    securityTitleLbl.frame = CGRectMake(0, 10, questionAnswerView.frame.size.width, 30);
    securityTitleLbl.text = @"PRIVACY QUESTION";
    securityTitleLbl.font = [UIFont fontWithName:FontString size:15];
    securityTitleLbl.textColor = [UIColor colorWithHexString:@"727272"];
    securityTitleLbl.textAlignment = NSTextAlignmentCenter;
    [questionAnswerView addSubview:securityTitleLbl];
    
    //** DropDown Box
    UIImage *dropDwnImg = [UIImage imageNamed:@"dDown"];
    pickerDropDownImgVw  = [[UIImageView alloc]initWithFrame:CGRectMake(pickerTxtField.frame.origin.x + pickerTxtField.frame.size.width - 83, 17, dropDwnImg.size.width -3, dropDwnImg.size.height - 3)];
    pickerTxtField.layer.borderWidth=0.5f;
    pickerDropDownImgVw.tag = 2;
    pickerDropDownImgVw.image =dropDwnImg;
    [pickerTxtField addSubview:pickerDropDownImgVw];
    
    //** KeyTextField
    keyTextField = [[UITextField alloc]initWithFrame:CGRectMake(20, pickerTxtField.frame.origin.y + pickerTxtField.frame.size.height + 10, 200,40)];
    [keyTextField setBorderStyle:UITextBorderStyleNone];
    
    keyTextField.layer.borderWidth=0.5f;
    keyTextField.textAlignment = NSTextAlignmentLeft;
    keyTextField.font = [UIFont systemFontOfSize:15];
    keyTextField.placeholder = @"Enter your Answer";
    keyTextField.layer.borderColor = [[UIColor clearColor]CGColor ];
    [keyTextField setBackgroundColor:[UIColor clearColor]];
    keyTextField.delegate = self;
    keyTextField.textColor = [UIColor blackColor];
    [questionAnswerView addSubview:keyTextField];
    
    securitySubmitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [securitySubmitBtn setFrame:CGRectMake((questionAnswerView.frame.size.width/2)- 60 ,(keyTextField.frame.origin.y + keyTextField.frame.size.height + 20), 120, 30)];
    [securitySubmitBtn setBackgroundColor:[UIColor lightGrayColor]];
    [securitySubmitBtn.layer setCornerRadius:5.0];
    securitySubmitBtn.layer.borderWidth = 0.8;
    
    if ([entryStr isEqualToString:@"Setting"])
    {
        [securitySubmitBtn setTitle:@"GET STARTED!" forState:UIControlStateNormal];
        pickerDropDownImgVw.hidden = NO;
    }
    else
    {
        [securitySubmitBtn setTitle:@"CONTINUE!" forState:UIControlStateNormal];
        pickerDropDownImgVw.hidden = YES;
    }
    
    [securitySubmitBtn.titleLabel setFont: [UIFont fontWithName:FontString  size:12]];
    securitySubmitBtn.layer.borderColor = [[UIColor clearColor] CGColor];
    [securitySubmitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [securitySubmitBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    [securitySubmitBtn addTarget:self action:@selector(security_OK_BTN_TAPPED) forControlEvents:UIControlEventTouchUpInside];
    [questionAnswerView addSubview:securitySubmitBtn];
    
    //** Do the Animations
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         securityQuestionView.frame = CGRectMake(0, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
                         
                         UIImage *effectImage = [UIImageEffects imageByApplyingLightEffectToImage:[UIImage imageNamed:@"iPhone5Blur"]];
                         securityQuestionView.backgroundColor = [[UIColor alloc]initWithPatternImage:effectImage];
                         securityTitleLbl.frame = CGRectMake(0, 10, questionAnswerView.frame.size.width, 30);
                         
                     }
                     completion:^(BOOL finished){
                         // NSLog(@"Done!");
                     }];
    
    [questionAnswerView addSubview:pickerTxtField];
    
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(pickerTxtField.frame.origin.x, pickerTxtField.frame.origin.y + pickerTxtField.frame.size.height + 2,200, 0.5)];
    lineView.backgroundColor = [UIColor  lightGrayColor];
    [questionAnswerView addSubview:lineView];
    
    UIView *SecondlineView = [[UIView alloc]initWithFrame:CGRectMake(keyTextField.frame.origin.x, keyTextField.frame.origin.y + keyTextField.frame.size.height + 2,200, 0.5)];
    SecondlineView.backgroundColor = [UIColor  lightGrayColor];
    [questionAnswerView addSubview:SecondlineView];
    
    tapToHide = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapToHide)];
    [self addGestureRecognizer:tapToHide];
    
    [self.onView addSubview:self];
    
    [self resignFirstResponder];
    
}

-(void)tapToHide
{
    selectionPickr.hidden = YES;
    keyTextField.hidden = NO;
    pickerSelected=NO;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == pickerTxtField)
    {
        
        if ([entryStr isEqualToString:@"Setting"])
        {
            
            if (pickerSelected == NO)
            {
                pickerSelected = YES;
                keyTextField.hidden = YES;
                [self pickerInitializationMethod];
                [keyTextField resignFirstResponder];
                return NO;
            }
            else if (pickerSelected == YES)
            {
                keyTextField.hidden = NO;
                pickerSelected=NO;
                [selectionPickr removeFromSuperview];
                return NO;
            }
            
        }
        else
        {
            return NO;
        }
    }
    else if (textField == keyTextField)
    {
        keyTextField.hidden = NO;
        
        if (pickerTxtField.text.length>0)
        {
            keyTextField.placeholder = @"";
            keyTextField.font = [UIFont systemFontOfSize:13];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"OOPS!" message:@"Select a question before you enter an answer" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            return NO;
        }
        
        
    }
    
    return YES;
    
}
-(void)security_OK_BTN_TAPPED
{
    
    if ([entryStr isEqualToString:@"Setting"])
    {
        
        NSString *securityQuestion = pickerTxtField.text;
        NSString *optedSecurityAnswser = keyTextField.text;
        
        
        if (securityQuestion.length>0 && optedSecurityAnswser.length >1)
        {
            
            [UIView animateWithDuration:0.2
                                  delay:0.0
                                options: UIViewAnimationOptionCurveEaseIn
                             animations:^{
                                 securityQuestionView.frame = CGRectMake(0, self.frame.size.height, self.frame.size.width, self.frame.size.height);
                                 
                             }
                             completion:^(BOOL finished){
                                 //  NSLog(@"Done!");
                                 [self removeFromSuperview];
                             }];
            [keyTextField resignFirstResponder];
            
            
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"MASTER_QUESTION"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"MASTER_ANSWER"];
            
            [[NSUserDefaults standardUserDefaults]setObject:securityQuestion forKey:@"MASTER_QUESTION"];
            [[NSUserDefaults standardUserDefaults]setObject:optedSecurityAnswser forKey:@"MASTER_ANSWER"];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"MASTERPWDSET" object:nil userInfo:nil];
            
        }
        else if (securityQuestion.length == 0 && optedSecurityAnswser.length > 1)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"OOPS!" message:@"Please select a question." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else if (securityQuestion.length > 0 && optedSecurityAnswser.length <= 1)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"OOPS!" message:@"Please enter a valid key." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"OOPS!" message:@"Please fill all the fields to proceed." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
        }
    }
    else
    {
        NSString *masterPwd = [[NSUserDefaults standardUserDefaults]valueForKey:@"MASTER_ANSWER"];
        if ([keyTextField.text isEqualToString:masterPwd])
        {
            fromSecurityQuest = YES;
            
            passwordVw = [[PasswordPin alloc]initWithFrame_onView:self LabelString:@"Set your Master Unlock Key" password:@"" Hint:SET_PRIVATE_PWD JID:@""];
            [passwordVw createPasswordVw];
            
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"OOPS!" message:@"Keys Mismatch ;(" delegate:self cancelButtonTitle:@"Let me Try Again!" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    
}

-(void)pickerInitializationMethod
{
    selectionPickr = [[UIPickerView alloc]initWithFrame:CGRectMake((self.frame.size.width/2)-150, pickerTxtField.frame.origin.y + pickerTxtField.frame.size.height + 150, 300,30)];
    selectionPickr.showsSelectionIndicator=YES;
    selectionPickr.backgroundColor=[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    //   selectionPickr.backgroundColor= RGBA(192, 192, 192, 0.7);
    
    selectionPickr.layer.borderColor=[[UIColor clearColor] CGColor];
    selectionPickr.layer.borderWidth=1.5;
    selectionPickr.layer.cornerRadius= 5.0f;
    selectionPickr.showsSelectionIndicator=YES;
    selectionPickr.delegate=self;
    selectionPickr.dataSource=self;
    [selectionPickr selectRow:3 inComponent:0 animated:YES];
    [self addSubview:selectionPickr];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component

{
    return [securityQuestionListArr count];
}


- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* tView = (UILabel*)view;
    if (!tView)
    {
        tView = [[UILabel alloc] init];
        [tView setFont:[UIFont systemFontOfSize:13]];
        [tView setTextAlignment:NSTextAlignmentCenter];
        tView.numberOfLines=2;
    }
    // Fill the label text here
    tView.text=[securityQuestionListArr objectAtIndex:row];
    return tView;
}


-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    keyTextField.hidden = NO;
    pickerTxtField.text = [securityQuestionListArr objectAtIndex:row];
    pickerTxtField.font = [UIFont fontWithName:FontString size:12];
    keyTextField.font = [UIFont fontWithName:FontString size:13];
    pickerTxtField.textAlignment = NSTextAlignmentLeft;
    selectionPickr.hidden=YES;
    pickerSelected = NO;
    
    [pickerDropDownImgVw setHidden:YES];
    
}


-(void)privatePassword:(NSNotification *)notification
{
    
    [passwordVw removeFromSuperview];
    passwordVw = nil;
    
    NSDictionary *dict = [notification userInfo];
    
    if ([[dict objectForKey:@"Mode"] isEqualToString:SET_PRIVATE_PWD])
    {
        if ([[dict objectForKey:@"isCorrect"] isEqualToString:@"YES"])
        {
            passwordVw = [[PasswordPin alloc]initWithFrame_onView:self LabelString:@"Confirm your Master Unlock Key " password:@"" Hint:CONFRM_PRIVATE_PWD JID:@""];
            [passwordVw createPasswordVw];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:MASTER_PIN];
            
        }
        
    }
    else if ([[dict objectForKey:@"Mode"] isEqualToString:CONFRM_PRIVATE_PWD])
    {
        if ([[dict objectForKey:@"isCorrect"] isEqualToString:@"YES"])
        {
            
            [self performSelector:@selector(security_OK_BTN_TAPPED) withObject:self withObject:nil];
            
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:MASTER_PIN];
            
        }
        
        
    }else if ([[dict objectForKey:@"Mode"] isEqualToString:ENTER_PRIVATE_PWD])
    {
        if ([[dict objectForKey:@"isCorrect"] isEqualToString:@"YES"])
        {
            
            
            
        }
        else
        {
        }
        
        
    }
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    
    
    
    return YES;
}



@end
