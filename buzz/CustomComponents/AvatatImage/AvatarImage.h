//
//  AvatarImage.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 24/02/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "XMPPUserCoreDataStorageObject.h"
#import "XMPPvCardAvatarModule.h"
#import "XMPPvCardTemp.h"
@interface AvatarImage : NSObject
-(UIImage *)profileImage:(XMPPUserCoreDataStorageObject*)user2;
-(UIImage *)myProfileImage;
- (void)updateAvatar:(UIImage *)avatar;
-(UIImage *)groupProfileimage:(XMPPJID*)user;
@end
