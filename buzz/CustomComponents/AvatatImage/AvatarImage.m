//
//  AvatarImage.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 24/02/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "AvatarImage.h"
#import "NSData+XMPP.h"

@implementation AvatarImage

- (AppDelegate *)appDelegate
{
	return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Roster Profile Image
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(UIImage *)profileImage:(XMPPUserCoreDataStorageObject*)user
{
    UIImage *img = nil;
//    if (user.photo != nil)
//		img = user.photo;
//	else
//	{
		NSData *photoData = [[[self appDelegate] xmppvCardAvatarModule] photoDataForJID:user.jid];
		if (photoData != nil)
			img = [UIImage imageWithData:photoData];
		else
			img = [UIImage imageNamed:@"defaultPerson"];
	//}
    return img;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Client Profile Image
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(UIImage *)myProfileImage
{
    UIImage *img;
    // XMPPJID *jid =[[self xmppStream] myJID].bareJID;
    XMPPvCardTempModule *vCardTempModule = [[self appDelegate] xmppvCardTempModule];
    XMPPvCardTemp *myVcardTemp = [vCardTempModule myvCardTemp];
    if (myVcardTemp.photo != nil)
        img = [UIImage imageWithData:myVcardTemp.photo];
    else
        img = [UIImage imageNamed:@"defaultPerson"];
    return img;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Roster Profile Image
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(UIImage *)groupProfileimage:(XMPPJID*)user
{
    UIImage *img = nil;
   
		NSData *photoData = [[[self appDelegate] xmppvCardAvatarModule] photoDataForJID:user];
		if (photoData != nil)
			img = [UIImage imageWithData:photoData];
		else
			img = [UIImage imageNamed:@"defaultPerson"];
    return img;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Update Client Profile Image
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)updateAvatar:(UIImage *)avatar
{
    NSData *imageData = UIImagePNGRepresentation(avatar);
    dispatch_queue_t queue = dispatch_queue_create("com.whatsthebuzz.updateAvatar", DISPATCH_QUEUE_PRIORITY_DEFAULT);
    dispatch_async(queue, ^{
        XMPPvCardTempModule *vCardTempModule = [[self appDelegate] xmppvCardTempModule];
        XMPPvCardTemp *myVcardTemp = [vCardTempModule myvCardTemp];
        [myVcardTemp setPhoto:imageData];
        [vCardTempModule updateMyvCardTemp:myVcardTemp];
    });
    
    
//      dispatch_queue_t queue2 = dispatch_queue_create("updateGroupImageViewQueue", DISPATCH_QUEUE_PRIORITY_DEFAULT);
//      dispatch_async(queue2, ^{
//   
//          NSData *imageData = UIImagePNGRepresentation(avatar);
//          if([imageData length])
//          {
//              NSXMLElement *photoelement = [NSXMLElement elementWithName:@"vCard" xmlns:@"vcard-temp"];
//   
//              NSXMLElement *photo = [NSXMLElement elementWithName:@"PHOTO"];
//              NSString *imageType = [imageData xmpp_imageType];
//   
//              if([imageType length])
//              {
//                  NSXMLElement *type = [NSXMLElement elementWithName:@"TYPE"];
//                  [photo addChild:type];
//                  [type setStringValue:imageType];
//              }
//   
//              NSXMLElement *binval = [NSXMLElement elementWithName:@"BINVAL"];
//              [photo addChild:binval];
//              [binval setStringValue:[imageData xmpp_base64Encoded]];
//              [photoelement addChild:photo];
//   
//              NSString *myvCardElementID = [[self appDelegate].xmppStream  generateUUID];
//              NSXMLElement *SETelement = [NSXMLElement elementWithName:@"iq"];
//              [SETelement addAttributeWithName:@"from" stringValue:[NSString stringWithFormat:@"%@",[self appDelegate].xmppStream.myJID]];
//              [SETelement addAttributeWithName:@"type" stringValue:@"set"];
//              [SETelement addAttributeWithName:@"id" stringValue:myvCardElementID];
//              [SETelement addChild:photoelement];
//              [[self appDelegate].xmppStream sendElement:SETelement];
//              
//              XMPPvCardTempModule *vCardTempModule = [[self appDelegate] xmppvCardTempModule];
//              XMPPvCardTemp *myVcardTemp = [vCardTempModule myvCardTemp];
//              [myVcardTemp setPhoto:imageData];
//          }
//   
//      });

    
}
@end
