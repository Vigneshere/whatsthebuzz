//
//  MessageMethods.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 16/03/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "MessageMethods.h"
static NSString *const xmlns_chatstates = @"http://jabber.org/protocol/chatstates";


@implementation MessageMethods
- (AppDelegate *)appDelegate
{
	return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (XMPPStream *)xmppStream
{
	return [[self appDelegate] xmppStream];
}
-(void)stateMessage:(NSString *)type jid:(NSString *)jidStr
{
    NSXMLElement *stateMessage = [NSXMLElement elementWithName:@"message"];
    [stateMessage addAttributeWithName:@"type" stringValue:@"chat"];
    [stateMessage addAttributeWithName:@"to" stringValue:jidStr];
    if ([type isEqualToString:@"composing"])
        [stateMessage addChild:[NSXMLElement elementWithName:@"composing" xmlns:xmlns_chatstates]];
    else if([type isEqualToString:@"paused"])
        [stateMessage addChild:[NSXMLElement elementWithName:@"paused" xmlns:xmlns_chatstates]];
    else if([type isEqualToString:@"inactive"])
        [stateMessage addChild:[NSXMLElement elementWithName:@"inactive" xmlns:xmlns_chatstates]];
    else if([type isEqualToString:@"gone"])
        [stateMessage addChild:[NSXMLElement elementWithName:@"gone" xmlns:xmlns_chatstates]];
    [self.xmppStream sendElement:stateMessage];
}
-(void)textmessage:(NSString *)messageStr jid:(NSString *)jidStr
{
    NSString *messageID=[self.xmppStream generateUUID];
    NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
    [body setStringValue:messageStr];
    NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
    [message addAttributeWithName:@"id" stringValue:messageID];
    [message addAttributeWithName:@"type" stringValue:@"chat"];
    [message addAttributeWithName:@"to" stringValue:jidStr];
    [message addChild:body];
    [self.xmppStream sendElement:message];
    
}

-(void)impTextmessage:(NSString *)messageStr jid:(NSString *)jidStr
{
    NSString *messageID=[self.xmppStream generateUUID];
    NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
    [body setStringValue:messageStr];
    NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
    [message addAttributeWithName:@"id" stringValue:messageID];
    [message addAttributeWithName:@"type" stringValue:@"chat"];
    [message addAttributeWithName:@"msg_type" stringValue:@"imp"];
    [message addAttributeWithName:@"to" stringValue:jidStr];
    [message addChild:body];
    NSXMLElement *imp = [NSXMLElement elementWithName:@"imp"];
    [imp addAttributeWithName:@"xmlns" stringValue:@"imp"];
    [imp addAttributeWithName:@"type" stringValue:@"message"];
    [message addChild:imp];

    [self.xmppStream sendElement:message];
}

@end
