//
//  MessageMethods.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 16/03/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageMethods : NSObject
-(void)stateMessage:(NSString *)type jid:(NSString *)jidStr;
-(void)textmessage:(NSString *)messageStr jid:(NSString *)jidStr;
-(void)impTextmessage:(NSString *)messageStr jid:(NSString *)jidStr;

@end
