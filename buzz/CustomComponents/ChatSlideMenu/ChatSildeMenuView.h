//
//  ChatSildeMenuView.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 14/04/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol chatSlideViewDelegate <NSObject>

-(void)selectSlideMenuButton:(NSInteger)index;

@end
@interface ChatSildeMenuView : UIScrollView
@property(nonatomic,weak)id<chatSlideViewDelegate>delegte;
@property(nonatomic)BOOL menuForGroup;

-(void)addMenuView;

@end
