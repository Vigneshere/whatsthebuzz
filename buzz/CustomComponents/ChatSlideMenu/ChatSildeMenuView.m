//
//  ChatSildeMenuView.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 14/04/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "ChatSildeMenuView.h"

#define menuImageArray @[@"ruthere",@"privatechat.png",@"chat_Imp",@"video_c",@"arc_menu_secure_gallery",@"set_background"]
#define menuArray @[@"Are You There?",@"Clear chat",@"Important",@"Video",@"Secure gallery",@"Set background"]


#define menuImageArrayForGroup @[@"ruthere",@"privatechat.png",@"chat_Imp",@"video_c",@"arc_menu_secure_gallery",@"set_background"]
#define menuArrayForGroup @[@"Are You There?",@"Clear chat",@"Important",@"Video",@"Secure gallery",@"Set background"]




@implementation ChatSildeMenuView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //[self addMenuView];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

-(void)addMenuView
{
    UIView* plainVw = [UIView new];
    plainVw.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    plainVw.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.9];
   // plainVw.alpha = 0.8;
    CGFloat rowHeight = (CGRectGetHeight(self.frame)/6) ;
    
    [self addSubview:plainVw];
    
    if (!_menuForGroup)
    {
        
        for (int i = 0; i < 6; i++) {
        UIButton *_button = [UIButton buttonWithType:UIButtonTypeCustom];
        _button.backgroundColor = [UIColor clearColor];
        
        CGFloat x = 0.0f;
        
        if (i == 0)
        {
            x = 0.2f;
        }
        
        [_button setFrame:CGRectMake(x, i*rowHeight, 100.f, rowHeight)];
        
        [_button setClipsToBounds:false];
        [_button setImage:[UIImage imageNamed:menuImageArray[i]] forState:UIControlStateNormal];
        [_button setTitle:menuArray[i] forState:UIControlStateNormal];
        _button.tag = i+1;
        _button.titleLabel.font = [UIFont fontWithName:FontString size:13];
        _button.titleLabel.adjustsFontSizeToFitWidth =YES;
        [_button setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal]; // SET the colour for your wishes
        [_button addTarget:self action:@selector(buttonTouchedUpInside:) forControlEvents:UIControlEventTouchUpInside];
        CGFloat spacing = 6.0;
        CGSize imageSize = _button.imageView.frame.size;
            
        _button.titleEdgeInsets = UIEdgeInsetsMake( 0.0, - imageSize.width, - (imageSize.height + spacing), 0.0);
        CGSize titleSize = _button.titleLabel.frame.size;
        
        if (i == 0)
        {
            _button.imageEdgeInsets = UIEdgeInsetsMake( - (titleSize.height + spacing), 26 , 0.0,  titleSize.width / 2  - (imageSize.width/2));
            
        }
        else if (i == 1)
          {
              _button.imageEdgeInsets = UIEdgeInsetsMake( - (titleSize.height + spacing), 21.75 , 0.0,  titleSize.width / 2  - (imageSize.width/2));
              
          }
        else if (i == 2)
        {
            _button.imageEdgeInsets = UIEdgeInsetsMake( - (titleSize.height + spacing), 21.75 , 0.0,  titleSize.width / 2  - (imageSize.width/2));
            
        }
        else if (i == 4)
        {
            _button.imageEdgeInsets = UIEdgeInsetsMake( - (titleSize.height + spacing), 25 , 0.0,  titleSize.width / 2 );
            
        }
        else if (i == 5)
        {
            _button.imageEdgeInsets = UIEdgeInsetsMake( - (titleSize.height + spacing), 25 , 0.0,  titleSize.width / 2  - (imageSize.width/2));

        }
        else
        {
            _button.imageEdgeInsets = UIEdgeInsetsMake( - (titleSize.height + spacing), 0.0, 0.0, - titleSize.width+spacing);
        }
        [self addSubview:_button];
        if (i != 5) {
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0.0f,_button.frame.origin.y+_button.frame.size.height+1, 100.f, 1.0f)];
            lineView.backgroundColor = [UIColor clearColor];
            [self addSubview:lineView];
        }
    }
        
    }
    else
    {
        
        for (int i = 0; i < 6; i++) {
            UIButton *_button = [UIButton buttonWithType:UIButtonTypeCustom];
            _button.backgroundColor = [UIColor clearColor];
            
            CGFloat x = 0.0f;
            
            if (i == 0)
            {
                x = 0.2f;
            }
            
            [_button setFrame:CGRectMake(x, i*rowHeight, 100.f, rowHeight)];
            
            [_button setClipsToBounds:false];
            [_button setImage:[UIImage imageNamed:menuImageArrayForGroup[i]] forState:UIControlStateNormal];
            [_button setTitle:menuArrayForGroup[i] forState:UIControlStateNormal];
            _button.tag = i+1;
            _button.titleLabel.font = [UIFont fontWithName:FontString size:13];
            _button.titleLabel.adjustsFontSizeToFitWidth =YES;
            [_button setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal]; // SET the colour for your wishes
            [_button addTarget:self action:@selector(buttonTouchedUpInside:) forControlEvents:UIControlEventTouchUpInside];
            CGFloat spacing = 6.0;
            CGSize imageSize = _button.imageView.frame.size;
            
            
            
            _button.titleEdgeInsets = UIEdgeInsetsMake( 0.0, - imageSize.width, - (imageSize.height + spacing), 0.0);
            CGSize titleSize = _button.titleLabel.frame.size;
            
            if (i == 0)
            {
                _button.imageEdgeInsets = UIEdgeInsetsMake( - (titleSize.height + spacing), 26 , 0.0,  titleSize.width / 2  - (imageSize.width/2));
                
            }
            else if (i == 1)
            {
                _button.imageEdgeInsets = UIEdgeInsetsMake( - (titleSize.height + spacing), 21.75 , 0.0,  titleSize.width / 2  - (imageSize.width/2));
                
            }

            else if (i == 2)
            {
                _button.imageEdgeInsets = UIEdgeInsetsMake( - (titleSize.height + spacing), 21.75 , 0.0,  titleSize.width / 2  - (imageSize.width/2));
                
            }

            else if (i == 4)
            {
                _button.imageEdgeInsets = UIEdgeInsetsMake( - (titleSize.height + spacing), 25 , 0.0,  titleSize.width / 2 );
                
            }
            else if (i == 5)
            {
                _button.imageEdgeInsets = UIEdgeInsetsMake( - (titleSize.height + spacing), 25 , 0.0,  titleSize.width / 2  - (imageSize.width/2));
                
            }
            else
            {
                _button.imageEdgeInsets = UIEdgeInsetsMake( - (titleSize.height + spacing), 0.0, 0.0, - titleSize.width+spacing);
            }
            [self addSubview:_button];
            if (i != 5) {
                UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0.0f,_button.frame.origin.y+_button.frame.size.height+1, 100.f, 1.0f)];
                lineView.backgroundColor = [UIColor clearColor];
                [self addSubview:lineView];
            }

            
            
        
    }
    }
    
    
    
}

-(void)buttonTouchedUpInside:(UIButton *)sender
{
    if([self.delegte respondsToSelector:@selector(selectSlideMenuButton:)])
        [self.delegte selectSlideMenuButton:sender.tag];
}


@end
