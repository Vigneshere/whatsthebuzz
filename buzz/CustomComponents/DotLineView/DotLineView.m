//
//  DotLineView.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 24/02/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "DotLineView.h"
#import <QuartzCore/QuartzCore.h>
@implementation DotLineView
 -(void)drawLine :(CGRect)rect :(UIView*)onView :(UIColor*)color
 {
  CGRect screenRect = [[UIScreen mainScreen] bounds];
     
 CAShapeLayer *shapeLayer = [CAShapeLayer layer];
 [shapeLayer setBounds:CGRectMake(0, 0, screenRect.size.width, 140)];
 [shapeLayer setPosition:CGPointMake(screenRect.size.width/2, 70)];
 [shapeLayer setFillColor:[[UIColor clearColor] CGColor]];
 [shapeLayer setStrokeColor:[[UIColor darkGrayColor] CGColor]];
 [shapeLayer setLineWidth:0.2f];
 [shapeLayer setLineJoin:kCALineJoinRound];
 [shapeLayer setLineDashPattern:
 [NSArray arrayWithObjects:[NSNumber numberWithInt:2],
 [NSNumber numberWithInt:1],nil]];
 // Setup the path
 CGMutablePathRef path = CGPathCreateMutable();
 CGPathMoveToPoint(path, NULL,rect.origin.x, rect.origin.y);
 CGPathAddLineToPoint(path, NULL, rect.origin.x + rect.size.width,rect.origin.y);
 [shapeLayer setPath:path];
 CGPathRelease(path);
 [[onView layer] addSublayer:shapeLayer];
 }


@end
