//
//  DotLineView.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 24/02/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DotLineView :NSObject
-(void)drawLine :(CGRect)rect :(UIView*)onView :(UIColor*)color;

@end
