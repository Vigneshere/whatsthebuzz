//
//  Singleton.h
//  SeatCaster
//
//  Created by Magesh on 05/08/13.
//  Copyright (c) 2013 JEMS Network Solution Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Singleton : NSObject

@property (strong, nonatomic) id searchVwDelegate;
@property (strong, nonatomic) id sosContactsDelegate;
@property (strong, nonatomic) UIImage* Avatar_Img;
@property (nonatomic, assign) BOOL internetConnection;
@property (nonatomic, assign) id ChatViewUpdate;
@property (strong, nonatomic) NSMutableDictionary *mediaFileIndicatorDict;
@property (strong, nonatomic) NSMutableDictionary *privateReqIndicatorDict;
@property (nonatomic, copy) NSString *chatScreenUser;
@property (strong, nonatomic) NSMutableArray *notificationVwAry;


+(Singleton*)sharedMySingleton;

@end
