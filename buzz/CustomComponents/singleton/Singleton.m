//
//  Singleton.m
//  SeatCaster
//
//  Created by Magesh on 05/08/13.
//  Copyright (c) 2013 JEMS Network Solution Pvt Ltd. All rights reserved.
//

#import "Singleton.h"

@implementation Singleton
static Singleton* sharedMySingleton = nil;
+(Singleton*)sharedMySingleton
{
    static Singleton* _sharedMySingleton = nil;
    
    @synchronized(self)
    {
        if(!_sharedMySingleton)
        {
            _sharedMySingleton = [[Singleton alloc] init];
            
        }
        
    }
    return _sharedMySingleton;
}
+(id)alloc
{
    @synchronized([Singleton class])
    {
        NSAssert(sharedMySingleton == nil, @"Attempted to allocate a second instance of a singleton.");
        sharedMySingleton = [super alloc];
        return sharedMySingleton;
    }
    
    return nil;
}
-(id)init {
    self = [super init];
    if (self != nil) {
        // initialize stuff here
    }
    
    return self;
}
@end

