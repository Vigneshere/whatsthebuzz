//
//  NSSortDescriptor+SortValues.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 29/03/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "NSSortDescriptor+SortValues.h"

@implementation NSSortDescriptor (SortValues)
+(NSArray *)sortValues:(NSString *)key ascending:(BOOL)asc;
{
    NSSortDescriptor *sd1 = [[NSSortDescriptor alloc] initWithKey:key ascending:asc selector:nil];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sd1, nil];
    return sortDescriptors;
}
+(NSArray *)sortValues:(NSString *)key ascending:(BOOL)asc selector:(SEL)selector
{
    NSSortDescriptor *sd1 = [[NSSortDescriptor alloc] initWithKey:key ascending:asc selector:selector];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sd1, nil];
    return sortDescriptors;
}
@end
