//
//  UIImage+ReCreate.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 27/05/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ReCreate)
+(UIImage *)imageWithColor:(UIColor *)color;
+(UIImage *)squareImageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
+(UIImage*)imageWithImage:(UIImage*) sourceImage scaledToWidth: (float) i_width;
+(UIImage *)generatePhotoThumbnail:(UIImage *)image withSide:(CGFloat)ratio;
+(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;

@end
