//
//  NSSortDescriptor+SortValues.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 29/03/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSSortDescriptor (SortValues)
+(NSArray *)sortValues:(NSString *)key ascending:(BOOL)asc;
+(NSArray *)sortValues:(NSString *)key ascending:(BOOL)asc selector:(SEL)selector;
@end
