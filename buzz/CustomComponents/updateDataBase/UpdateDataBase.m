//
//  UpdateDataBase.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 24/02/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "UpdateDataBase.h"
#import "AppDelegate.h"
#import "XMPPUserCoreDataStorageObject.h"
#import "XMPPMessageArchivingCoreDataStorage.h"
#import "XMPPCoreDataStorage.h"


@implementation UpdateDataBase

- (AppDelegate *)appDelegate
{
	return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (NSManagedObjectContext *)managedObjecMessageContact
{
	return [[[self appDelegate] xmppMessageArchivingStorage] mainThreadManagedObjectContext];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Core Data
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (NSManagedObjectContext *)managedObjectContext_roster
{
	return [[[self appDelegate] xmppRosterStorage] mainThreadManagedObjectContext];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Method to update unreaded message for user
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)updateUnreadMessage:(XMPPJID *)user clearValue:(BOOL)clear
{
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:[self managedObjectContext_roster]];
    [request setEntity:entity];
    NSString *predicateFrmt = @"jidStr = %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, [user bareJID]];
    [request setPredicate:predicate];
    NSArray *empArray=[[self managedObjectContext_roster] executeFetchRequest:request error:nil];
    if ([empArray count] == 1){
        for (XMPPUserCoreDataStorageObject *user in  empArray)
        {
            NSNumber *number = user.unreadMessages;
            if (clear)
                user.unreadMessages = [NSNumber numberWithInt:0];
            else
            {
                int value = [number intValue];
                number = [NSNumber numberWithInt:value + 1];
                user.unreadMessages = number;
            }
        }
    }
    else
    {
       // NSLog(@"Nothin");
    }
    [[self managedObjectContext_roster] save:nil];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Method to Delete Chat Message and Update in RecentChat
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)deteleMessagejidStr:(NSString *)jidstring
{
    
    NSManagedObjectContext *context= [self managedObjecMessageContact];
    
    [context performBlock:^{
        
        XMPPMessageArchiving_Contact_CoreDataObject *modelObj2;

    NSFetchRequest *request = [[NSFetchRequest alloc] init];
//    [request setEntity:[NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:context]];
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"bareJidStr == %@ AND timestamp == %@ ",jidstring,[NSDate date]];
//    [request setPredicate:predicate];
//    
//    NSError *error = nil;
//    NSArray *results = [context executeFetchRequest:request error:&error];
  
         NSFetchRequest *request2 = [[NSFetchRequest alloc] init];
            [request2 setEntity:[NSEntityDescription entityForName:@"XMPPMessageArchiving_Contact_CoreDataObject" inManagedObjectContext:context]];
            NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"bareJidStr == %@",jidstring];
            [request2 setPredicate:predicate2];
        
            NSError *error = nil;
            NSArray *Contactresults2 = [context executeFetchRequest:request2 error:&error];

            
            if ([Contactresults2 count] > 0)
            {
                modelObj2 = (XMPPMessageArchiving_Contact_CoreDataObject *)[Contactresults2 objectAtIndex:0];
                
                [request setEntity:[NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:context]];
                NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"bareJidStr == %@",jidstring];
                [request setSortDescriptors:[NSSortDescriptor sortValues:@"timestamp" ascending:NO]];
                [request setPredicate:predicate2];
                NSArray *results2 = [context executeFetchRequest:request error:NULL];
                if ([results2 count] > 0)
                {
                    XMPPMessageArchiving_Message_CoreDataObject *modelObj3 = (XMPPMessageArchiving_Message_CoreDataObject *)[results2 objectAtIndex:0];
                    if (modelObj3.body != nil) {
                        
                        [modelObj2 setValue: modelObj3.bareJidStr forKey:@"bareJidStr"];
                        [modelObj2 setValue: modelObj3.bareJid forKey:@"bareJid"];
                        [modelObj2 setValue: modelObj3.body forKey:@"mostRecentMessageBody"];
                        [modelObj2 setValue: modelObj3.streamBareJidStr forKey:@"streamBareJidStr"];
                        [modelObj2 setValue: modelObj3.timestamp forKey:@"mostRecentMessageTimestamp"];
                        
                        if (![context save:&error])
                        {
                               NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
                        }

                            
                        }
                }
                else
                {
                    modelObj2 = (XMPPMessageArchiving_Contact_CoreDataObject *)[Contactresults2 objectAtIndex:0];
                    
                    [modelObj2 setValue: modelObj2.bareJidStr forKey:@"bareJidStr"];
                    [modelObj2 setValue: modelObj2.bareJid forKey:@"bareJid"];
                    [modelObj2 setValue: @" " forKey:@"mostRecentMessageBody"];
                    [modelObj2 setValue: modelObj2.streamBareJidStr forKey:@"streamBareJidStr"];
                    [modelObj2 setValue: modelObj2.mostRecentMessageTimestamp forKey:@"mostRecentMessageTimestamp"];
                    
                    if (![context save:&error])
                    {
                        NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
                    }


                }
                
            }
    }];
    
    

    
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Method to Delete RecentChat Message
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//--> This method delete all the message for particular roster
-(void)deleteRecentChatMsg:(XMPPJID *)jid
{
    XMPPMessageArchiving_Contact_CoreDataObject *modelObj2;
    NSManagedObjectContext *context= [self managedObjecMessageContact];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:context]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"bareJidStr == %@",[jid bare]];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    
    if ([results count] > 0)
    {
        @autoreleasepool {
        for (XMPPMessageArchiving_Message_CoreDataObject * msg in results) {
            [context deleteObject:msg];
            }
        }
    }
    [request setEntity:[NSEntityDescription entityForName:@"XMPPMessageArchiving_Contact_CoreDataObject" inManagedObjectContext:context]];
    predicate = [NSPredicate predicateWithFormat:@"bareJidStr == %@",[jid bare]];
    [request setPredicate:predicate];
    
    NSError *error2 = nil;
    NSArray *results2 = [context executeFetchRequest:request error:&error2];
    if ([results2 count] == 1)
    {
        modelObj2 = (XMPPMessageArchiving_Contact_CoreDataObject *)[results2 objectAtIndex:0];
        [context deleteObject:modelObj2];
    }
    [[self managedObjecMessageContact] save:nil];
    
    results = nil;
    results2 = nil;
    context = nil;
    request = nil;
    predicate = nil;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Method to Delete RecentGroupChat Message
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)deleteRecentGroupChatMsg:(NSString *)roomJidStr
{
    XMPPRoomCoreDataStorage *storage = [XMPPRoomCoreDataStorage sharedInstance];
    NSManagedObjectContext *context = [storage mainThreadManagedObjectContext];
    //Delete Message Entity details
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:storage.messageEntityName inManagedObjectContext:context]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"roomJIDStr == %@",roomJidStr];
    [request setPredicate:predicate];
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    
    if ([results count] > 0)
    {
        @autoreleasepool {
            for (XMPPRoomMessageCoreDataStorageObject * msg in results) {
                [context deleteObject:msg];
            }
        }
    }
    //Delete occupant Entity details
    [request setEntity:[NSEntityDescription entityForName:storage.occupantEntityName inManagedObjectContext:context]];
    predicate = [NSPredicate predicateWithFormat:@"roomJIDStr == %@",roomJidStr];
    [request setPredicate:predicate];
    
    NSError *error2 = nil;
    NSArray *results2 = [context executeFetchRequest:request error:&error2];
    if ([results2 count]) {
        @autoreleasepool {
            for (XMPPRoomOccupantCoreDataStorageObject *data in results2) {
                [context deleteObject:data];
            }
        }
    }
    //Delete Message details Entity details
    [request setEntity:[NSEntityDescription entityForName:storage.messageDetailsEntityName inManagedObjectContext:context]];
    predicate = [NSPredicate predicateWithFormat:@"roomJIDStr == %@",roomJidStr];
    [request setPredicate:predicate];
    
    NSError *error3 = nil;
    NSArray *results3 = [context executeFetchRequest:request error:&error3];
    if ([results3 count]) {
        @autoreleasepool {
            for (XMPPRoomMessageDetails *data in results3) {
               
                [context deleteObject:data];
            }
        }
        
    }
    [context save:nil];
    XMPPPresence *presence = [XMPPPresence presence];
    [presence addAttributeWithName:@"to" stringValue:roomJidStr];
    [presence addAttributeWithName:@"type" stringValue:@"unavailable"];
    [[[self appDelegate] xmppStream] sendElement:presence];
    
    results = nil;
    results2 = nil;
    context = nil;
    request = nil;
    predicate = nil;
    results3 = nil;
}

@end
