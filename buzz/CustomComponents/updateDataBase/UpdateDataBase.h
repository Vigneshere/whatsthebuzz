//
//  UpdateDataBase.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 24/02/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpdateDataBase : NSObject
- (void)updateUnreadMessage:(XMPPJID *)user clearValue:(BOOL)clear;
-(void)deteleMessagejidStr:(NSString *)jidstring;
-(void)deleteRecentChatMsg:(XMPPJID *)jid;
-(void)deleteRecentGroupChatMsg:(NSString *)roomJidStr;
@end
