//
//  SearchUser.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 07/03/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "SearchUser.h"

@implementation SearchUser
@synthesize searchdelegate;
- (id)init
{
    self = [super self];
    if (self) {
        // Custom initialization
        [[self xmppStream] addDelegate:self delegateQueue:dispatch_get_main_queue()];
    }
    return self;
}
- (AppDelegate *)appDelegate
{
	return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (XMPPStream *)xmppStream
{
	return [[self appDelegate] xmppStream];
}
-(void)searchUserName:(NSString*)username
{
    jidString = username;
    NSString *userBare1  = [[[[self appDelegate] xmppStream] myJID] bare];
    NSXMLElement *query = [NSXMLElement elementWithName:@"query"];
    [query addAttributeWithName:@"xmlns" stringValue:@"jabber:iq:search"];
    
    
    NSXMLElement *x = [NSXMLElement elementWithName:@"x" xmlns:@"jabber:x:data"];
    [x addAttributeWithName:@"type" stringValue:@"submit"];
    
    NSXMLElement *formType = [NSXMLElement elementWithName:@"field"];
    [formType addAttributeWithName:@"type" stringValue:@"hidden"];
    [formType addAttributeWithName:@"var" stringValue:@"FORM_TYPE"];
    [formType addChild:[NSXMLElement elementWithName:@"value" stringValue:@"jabber:iq:search" ]];
    
    NSXMLElement *userName = [NSXMLElement elementWithName:@"field"];
    [userName addAttributeWithName:@"var" stringValue:@"Username"];
    [userName addChild:[NSXMLElement elementWithName:@"value" stringValue:@"1" ]];
    
    NSXMLElement *name = [NSXMLElement elementWithName:@"field"];
    [name addAttributeWithName:@"var" stringValue:@"Name"];
    [name addChild:[NSXMLElement elementWithName:@"value" stringValue:@"1"]];
    
    NSXMLElement *email = [NSXMLElement elementWithName:@"field"];
    [email addAttributeWithName:@"var" stringValue:@"Email"];
    [email addChild:[NSXMLElement elementWithName:@"value" stringValue:@"1"]];
    
    NSXMLElement *search = [NSXMLElement elementWithName:@"field"];
    [search addAttributeWithName:@"var" stringValue:@"search"];
    [search addChild:[NSXMLElement elementWithName:@"value" stringValue:username]];
    
    [x addChild:formType];
    [x addChild:userName];
    //[x addChild:name];
    //[x addChild:email];
    [x addChild:search];
    [query addChild:x];
    
    
    NSXMLElement *iq = [NSXMLElement elementWithName:@"iq"];
    [iq addAttributeWithName:@"type" stringValue:@"set"];
    [iq addAttributeWithName:@"id" stringValue:@"search"];
    [iq addAttributeWithName:@"to" stringValue:[NSString stringWithFormat:@"search.%@",DOMAINAME]];
    [iq addAttributeWithName:@"from" stringValue:userBare1];
    [iq addChild:query];
    [[[self appDelegate] xmppStream] sendElement:iq];
}
- (BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq
{
    NSXMLElement *query = [iq elementForName:@"query" xmlns:@"jabber:iq:search"];
    if (query) {
        NSXMLElement *query2 = [query elementForName:@"x" xmlns:@"jabber:x:data"];
         NSArray *itemElements = [query2 elementsForName:@"item"];
       for (NSXMLElement *itemXml in itemElements) {
            NSArray *fields = [itemXml elementsForName:@"field"];
            for (NSXMLElement *fieldInItem in fields) {
                NSString *name = [[fieldInItem attributeForName:@"var"] stringValue];
                NSString *value = [[[fieldInItem elementsForName:@"value"] lastObject] stringValue];
                if ([name isEqualToString:@"jid"]) {
                           if ([searchdelegate respondsToSelector:@selector(userDetails:)])
                            [searchdelegate userDetails:value];
                }
            }

    }
    }
    return NO;
}
-(void)dealloc
{
}

@end
