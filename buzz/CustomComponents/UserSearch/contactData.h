//
//  contactData.h
//  contact_form
//
//  Created by shaili_macMini02 on 06/03/14.
//  Copyright (c) 2014 shaili_macMini02. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface contactData : NSObject

@property (retain, nonatomic) NSString *firstNames;
@property (retain, nonatomic) NSString *lastNames;
@property (retain, nonatomic) NSString *uniqueId;
@property (retain, nonatomic) NSMutableArray *phoneNumbers;
@property (retain, nonatomic) NSMutableArray *emailIds;
@property (retain, nonatomic) NSString *type;
@property (retain, nonatomic) NSString *firstLetter;
@property (retain, nonatomic) NSMutableArray *IMAry;
@property (retain, nonatomic) NSString* selectedNumber;
@property (retain, nonatomic) NSString* selectedEmail;


@end
