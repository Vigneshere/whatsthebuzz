//
//  SearchUser.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 07/03/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol SearchUserDelegate <NSObject>

- (void)userDetails:(NSString *)jidStr;
@end
@interface SearchUser : NSObject
{
    NSString *jidString;
}
@property (nonatomic, assign)  id<SearchUserDelegate> searchdelegate;

-(void)searchUserName:(NSString*)username;

@end
