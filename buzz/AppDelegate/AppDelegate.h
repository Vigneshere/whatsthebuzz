//
//  AppDelegate.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 18/02/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XMPPFramework.h"
#import "SearchUser.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "Singleton.h"
#import "ACStatusBarOverlayWindow.h"
#import "XMPPRosterMemoryStorage.h"
#import "ViewController.h"
#import <Foundation/Foundation.h>
#import "notify.h"
#import <FacebookSDK/FacebookSDK.h>
#import "RMPhoneFormat.h"


@class GCNetworkReachability;

enum {
    NoNetwork,
    TwoG,
    ThreeG,
    FourG,
    LTE,
    Wifi
};
typedef NSInteger NetworkType;

@interface AppDelegate : UIResponder <UIApplicationDelegate,XMPPStreamDelegate,SearchUserDelegate>
{
    XMPPStream *xmppStream;
    XMPPReconnect *xmppReconnect;
    XMPPRosterCoreDataStorage *xmppRosterStorage;
    XMPPRoster *xmppRoster;
   	XMPPCapabilities *xmppCapabilities;
    XMPPCapabilitiesCoreDataStorage *xmppCapabilitiesStorage;
    XMPPvCardTempModule *xmppvCardTempModule;
    XMPPvCardAvatarModule *xmppvCardAvatarModule;
    XMPPvCardCoreDataStorage *xmppvCardStorage;
    XMPPMessageArchiving *xmppMessageArchivingModule;
    XMPPMessageArchivingCoreDataStorage *xmppMessageArchivingStorage;
    XMPPMUC *xmppMuc;
    ViewController        *centerViewController;

        
    BOOL allowSelfSignedCertificates;
    BOOL allowSSLHostNameMismatch;
    BOOL isXmppConnected;

    
    BOOL isContactLoaded;
    BOOL isLastLoginUpdated;

    
    SearchUser *usersearch;
    
    NSString *password;
    NSMutableDictionary* ContactsDict;
    NSDictionary* userInfoDict;
    
    RMPhoneFormat *_phoneFormat;

}

@property (nonatomic,retain) AVAudioPlayer *messagePingPlayer;
@property (strong, nonatomic) GCNetworkReachability *reachability;
@property NetworkType networkType;

@property (strong, nonatomic) Singleton *singleton;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ACStatusBarOverlayWindow *dummywindow;

@property (nonatomic, strong, readonly) XMPPStream *xmppStream;
@property (nonatomic, strong, readonly) XMPPReconnect *xmppReconnect;
@property (nonatomic, strong, readonly) XMPPvCardTempModule *xmppvCardTempModule;
@property (nonatomic, strong, readonly) XMPPvCardAvatarModule *xmppvCardAvatarModule;
@property (nonatomic, strong, readonly) XMPPCapabilities *xmppCapabilities;
@property (nonatomic, strong, readonly) XMPPCapabilitiesCoreDataStorage *xmppCapabilitiesStorage;
@property (nonatomic, strong, readonly) XMPPRoster *xmppRoster;
@property (nonatomic, strong, readonly) XMPPRosterCoreDataStorage *xmppRosterStorage;
@property (nonatomic, strong, readonly) XMPPMessageArchiving *xmppMessageArchivingModule;
@property (nonatomic, strong, readonly) XMPPMessageArchivingCoreDataStorage *xmppMessageArchivingStorage;
@property (nonatomic, strong, readonly) XMPPRosterMemoryStorage *xmppRosterMemoryStorage;
@property (nonatomic, assign) NSInteger BadgeCount;
@property (nonatomic, strong) UIView *navigationBarViewLine;
@property (nonatomic, strong) UIView *statusBarView;
@property (nonatomic, strong) NSArray *contactsAry;
@property(nonatomic,assign)BOOL Background;

@property (nonatomic, strong) NSMutableArray* tempAry;

- (NSManagedObjectContext *)managedObjectContext_roster;
- (XMPPUserCoreDataStorageObject *)RecentcontactsFetch:(NSString *)Str;


-(BOOL)connect;
- (void)setupStream;
- (void)ShowNotification;
- (void)StartUpdateRoster;

@end
