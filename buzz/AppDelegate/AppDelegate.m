//
//  AppDelegate.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 18/02/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//


////check Point


#import "AppDelegate.h"
#import "DDLog.h"
#import "DDTTYLogger.h"
#import <AddressBook/ABAddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "contactData.h"
#import "SOSHomeController.h"
#import "numberRegViewController.h"
#import "Singleton.h"
#import "LocationManagerSingleton.h"
#import "DBEditer.h"
#import "GCNetworkReachability.h"
#import "FavoriteTableViewController.h"
#import "CRToast.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import "mediaDbEditor.h"
#import "privateChatDbEditer.h"
#import "GroupIconService.h"
#import "notiVw.h"
#import "sosContactObject.h"
#import "NSString+FindStickerName.h"
#import "profileFormViewController.h"
#import "NSDate+XMPPDateTimeProfiles.h"
#import "ACStatusBarOverlayWindow.h"
#import "UIImage+ReCreate.h"


// Log levels: off, error, warn, info, verbose
#if DEBUG
static const int ddLogLevel = LOG_LEVEL_VERBOSE;
#else
static const int ddLogLevel = LOG_LEVEL_INFO;
#endif

@implementation AppDelegate
@synthesize xmppStream;
@synthesize xmppReconnect;
@synthesize xmppvCardTempModule;
@synthesize xmppvCardAvatarModule;
@synthesize xmppCapabilities;
@synthesize xmppCapabilitiesStorage;
@synthesize xmppRoster,xmppRosterStorage;
@synthesize xmppMessageArchivingModule;
@synthesize xmppMessageArchivingStorage;



#pragma mark - remove DomainName from String

-(NSString*)removeDomainName_String:(NSString*)string
{
    return [string stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"@%@",DOMAINAME] withString:@""];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.tempAry = [NSMutableArray new];
    
    [self StartUpdateRoster];
    
    
    
    
    
    [DDLog addLogger:[DDTTYLogger sharedInstance]];
    
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        // iOS 8 Notifications
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [application registerForRemoteNotifications];
    }
    else
    {
        // iOS < 8 Notifications
        [application registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    }
    
    
    
    if (application.applicationIconBadgeNumber >= 1) {
        
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    }
    
    
    self.singleton = [Singleton sharedMySingleton];
    self.singleton.internetConnection = YES;
    self.singleton.mediaFileIndicatorDict = [NSMutableDictionary new];
    self.singleton.privateReqIndicatorDict = [NSMutableDictionary new];
    self.singleton.notificationVwAry = [NSMutableArray new];
    
    [self moniterNetwork];
    
    if (![[NSUserDefaults standardUserDefaults] integerForKey:@"textSize"])
    {
        
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"textSize"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    
    //    NSError *error = nil;
    //
    //    NSString *yourFolderPath = [[[NSBundle mainBundle] resourcePath]
    //                                stringByAppendingPathComponent:@"Images"];
    //
    //    NSArray  *yourFolderContents = [[NSFileManager defaultManager]
    //                                    contentsOfDirectoryAtPath:yourFolderPath error:&error];
    
    
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:USERNAME])
    {
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstTime"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UINavigationController *contoller = [[UINavigationController alloc]initWithRootViewController:[ViewController new]];
        UINavigationController *contoller2 = [[UINavigationController alloc]initWithRootViewController:[SOSHomeController new]];
        UINavigationController *contoller3 = [[UINavigationController alloc]initWithRootViewController:[FavoriteTableViewController new]];
        
        SOSHomeController     *leftPanelViewController;
        MSSlidingPanelController    *slidingPanelController;
        //FavoriteTableViewController *favoriteViewController;
        
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
        
        if (!centerViewController)
        {
            centerViewController = [[ViewController alloc] initWithNibName:nil bundle:nil];
        }
        
        leftPanelViewController = [[SOSHomeController alloc] initWithNibName:nil bundle:nil];
        
        slidingPanelController = [[MSSlidingPanelController alloc] initWithCenterViewController:contoller leftPanelController:contoller2 andRightPanelController:contoller3];
        
        [slidingPanelController setDelegate:centerViewController];
        
        [self setWindow:[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]]];
        [[self window] setRootViewController:slidingPanelController];
        [[self window] setBackgroundColor:[UIColor whiteColor]];
        [[self window] makeKeyAndVisible];
    }
    else
    {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstTime"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:TURN_BUBBLE_MENU];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:ONLINE_OFFLINE_VISIBILITY];

        UINavigationController*  contoller = [[UINavigationController alloc]initWithRootViewController:[numberRegViewController new]];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        [self setWindow:[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]]];
        [[self window] setRootViewController:contoller];
        [[self window] setBackgroundColor:[UIColor whiteColor]];
        [[self window] makeKeyAndVisible];
        
    }
    
    
    // Override point for customization after application launch.
    [self createMediaFolder:@"mediaFile"];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    _dummywindow = [[ACStatusBarOverlayWindow alloc] initWithFrame:CGRectZero];
    _dummywindow.hidden = NO;
    
    _statusBarView = [UIView new];
    _statusBarView.frame = CGRectMake(0, 0,self.window.frame.size.width, 20);
    _statusBarView.backgroundColor = [UIColor colorWithRed:53/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];  //RGBA(53, 152, 219, 1)
    [self.window addSubview:_statusBarView];
    
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"nav-bar"] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                                           NSFontAttributeName : [UIFont fontWithName:FontString size:20],
                                                           NSForegroundColorAttributeName : [UIColor colorWithRed:53/255.0 green:152/255.0 blue:219/255.0 alpha:1.0],
                                                           }];
    
    
    [[UINavigationBar appearance] setShadowImage:[UIImage new]];
    UIImage *backButtonImg = [UIImage imageNamed:@"back_btn.png"];
    
    UIImage *backButtonHomeImage = [[UIImage imageNamed:@"back_btn.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, backButtonImg.size.width - 1, 0, 0)];
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:backButtonHomeImage  forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, - backButtonHomeImage.size.height*2) forBarMetrics:UIBarMetricsDefault];
    //[self registerAppforDetectLockState];
    //[self registerforDeviceLockNotif];
    _BadgeCount = 0;
    
    [FBLoginView class];
    _phoneFormat = [[RMPhoneFormat alloc] init];

    
    ABAddressBookRef ntificationaddressbook = ABAddressBookCreate();
    ABAddressBookRegisterExternalChangeCallback(ntificationaddressbook, MyAddressBookExternalChangeCallback, (__bridge void *)(self));
    
    return (YES);
    
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
-(void)createMediaFolder:(NSString*)folderName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"mediaFile"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath]){
        
        NSError* error;
        if(  [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error])
        {
            
        }
        
        else
        {
            NSLog(@"[%@] ERROR: attempting to write create MyFolder directory", [self class]);
            NSAssert( FALSE, @"Failed to create directory maybe out of disk space?");
        }
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    LocationManagerSingleton*locSingleton = [LocationManagerSingleton sharedSingleton];
    [locSingleton.locationManager stopUpdatingLocation];
    
    self.singleton = [Singleton sharedMySingleton];
    for (id key in self.singleton.privateReqIndicatorDict)
    {
        NSTimer* timer = [self.singleton.privateReqIndicatorDict objectForKey:key];
        [timer invalidate];
        
    }
    [self.singleton.privateReqIndicatorDict removeAllObjects];
    _Background = YES;
    
   // DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    
#if TARGET_IPHONE_SIMULATOR
    DDLogError(@"The iPhone simulator does not process background network traffic. "
               @"Inbound traffic is queued until the keepAliveTimeout:handler: fires.");
#endif
    
    if ([application respondsToSelector:@selector(setKeepAliveTimeout:handler:)])
    {
        [application setKeepAliveTimeout:600 handler:^{
            
         //   DDLogVerbose(@"KeepAliveHandler");
            
            xmppStream.enableBackgroundingOnSocket = YES;
            
            
            // Do other keep alive stuff here.
        }];
    }
    
    
    // Get
   
    
    

    
    
    // [self teardownStream];
    
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    if (![self xmppStream])
    {
        [self setupStream];
        [self connect];
        
    }
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    _BadgeCount = 0;
    _Background = NO;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    LocationManagerSingleton*locSingleton = [LocationManagerSingleton sharedSingleton];
    [locSingleton startUpdatingCurrentLocation];
    NSDate *currentDate = [NSDate date];
    [[NSUserDefaults standardUserDefaults] setObject:[currentDate dateByAddingTimeInterval:-120] forKey:@"LastDate"];
    
    privateChatDbEditer* privateSection = [privateChatDbEditer new];
    [privateSection checkAndStartPrivateTimer];
    
    [FBAppEvents activateApp];

}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // attempt to extract a token from the url
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
}



- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
}


void MyAddressBookExternalChangeCallback (ABAddressBookRef ntificationaddressbook,CFDictionaryRef info,void *context)
{
    NSLog(@"AB value changed");
    [(__bridge AppDelegate *)context StartUpdateRoster];
    
}

-(void)StartUpdateRoster
{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^
                   {
                       self.contactsAry  =  [self getAllContacts];
                       isContactLoaded = YES;
                       
                       if (self.xmppStream.isConnected)
                       {
                           [self updateRosters];
                       }
                       
                   });
    
}


-(void)getUserDetails:(NSString*)jidstr
{
    XMPPUserCoreDataStorageObject *userdetails = [self RecentcontactsFetch:jidstr];
    NSMutableDictionary *dict = [NSMutableDictionary new];
    
    if (dict) {
        
        [dict setObject:userdetails forKey:@"userdetails"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TEMPNOTI" object:nil userInfo:dict];
        
        
    }
    
}


- (UIImage*) maskImage:(UIImage *)image withMask:(UIImage *)maskImage {
    
    CGImageRef maskRef = maskImage.CGImage;
    
    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                        CGImageGetHeight(maskRef),
                                        CGImageGetBitsPerComponent(maskRef),
                                        CGImageGetBitsPerPixel(maskRef),
                                        CGImageGetBytesPerRow(maskRef),
                                        CGImageGetDataProvider(maskRef), NULL, false);
    
    CGImageRef masked = CGImageCreateWithMask([image CGImage], mask);
    return [UIImage imageWithCGImage:masked];
}





#pragma mark  OBSERVE INTERNET CONNECTION
#pragma Mark-
#pragma Mark - Find Network Type
- (void)newtworkType {
    
    NSArray *subviews = [[[[UIApplication sharedApplication] valueForKey:@"statusBar"] valueForKey:@"foregroundView"]subviews];
    NSNumber *dataNetworkItemView = nil;
    
    for (id subview in subviews) {
        if([subview isKindOfClass:[NSClassFromString(@"UIStatusBarDataNetworkItemView") class]]) {
            dataNetworkItemView = subview;
            break;
        }
    }
    
    switch ([[dataNetworkItemView valueForKey:@"dataNetworkType"]integerValue]) {
        case 0:
            //NSLog(@"No wifi or cellular");
            _networkType = NoNetwork;
            break;
            
        case 1:
            //  NSLog(@"2G");
            _networkType = TwoG;
            break;
            
        case 2:
            //NSLog(@"3G");
            _networkType = ThreeG;
            break;
            
        case 3:
            // NSLog(@"4G");
            _networkType = FourG;
            break;
            
        case 4:
            //NSLog(@"LTE");
            _networkType = LTE;
            break;
            
        case 5:
            // NSLog(@"Wifi");
            _networkType = Wifi;
            break;
            
            
        default:
            break;
    }
}


-(void)moniterNetwork
{
    self.reachability = [GCNetworkReachability reachabilityWithHostName:@"www.google.com"];
    
    
    [self.reachability startMonitoringNetworkReachabilityWithHandler:^(GCNetworkReachabilityStatus status) {
        switch (status) {
            case GCNetworkReachabilityStatusNotReachable:
                
                if (self.singleton.internetConnection) {
                    
                    self.singleton.internetConnection = NO;
                    
                    if (!_Background)
                    {
                        [self showAlertWithMessage:@"It appears your internet connection is offline."  cancelButtonTile:@"Ok"];

                    }
                    
                    
                    
                }
                break;
            case GCNetworkReachabilityStatusWWAN:
                
                if (!self.singleton.internetConnection) {
                    
                    self.singleton.internetConnection = YES;
                    
                    if ([self xmppStream])
                    {
                        if (![[self xmppStream] isConnecting])
                        {
                            [self connect];
                        }
                        
                    }
                    if (!_Background)
                    {
                        [self showAlertWithMessage:@"App is now using Mobile Data."  cancelButtonTile:@"Ok"];

                    }
                    
                    
                }
                ////NSLog(@"working wwan");
            case GCNetworkReachabilityStatusWiFi:
                if (!self.singleton.internetConnection) {
                    
                    self.singleton.internetConnection = YES;
                    
                    if ([self xmppStream])
                    {
                        if (![[self xmppStream] isConnecting])
                        {
                            [self connect];
                        }
                        
                    }
                    if (!_Background)
                    {
                        [self showAlertWithMessage:@"App is now using Wi-Fi."  cancelButtonTile:@"Ok"];

                    }
                    
                    
                    
                }
                break;
        }
    }];
}
#pragma mark ALERT MESSAGE
-(void)showAlertWithMessage:(NSString *)message cancelButtonTile:(NSString *)cancelTitle
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ALERTVIEW_TITLE message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    alert = nil;
    
}
#pragma mark - Push Notification Method

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    
    // Send the deviceToken to server right HERE!!! (the code for this is below)
    
    NSString* deviceTkn = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    deviceTkn = [deviceTkn stringByReplacingOccurrencesOfString:@" " withString:@""];
    [[NSUserDefaults standardUserDefaults]setObject:deviceTkn forKey:@"DEVICE_TOKEN"];
    
    NSLog(@"device token::::%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"DEVICE_TOKEN"]);
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
    
    UIApplicationState state = [application applicationState];
    
    NSMutableDictionary* messageDict = [NSMutableDictionary new];
    messageDict =  [userInfo objectForKey:@"aps"];
    DBEditer* dbEditer_Obj = [DBEditer new];
    if ([[messageDict objectForKey:@"type"] isEqualToString:@"request"])
    {
        NSMutableArray* toAry = [[NSMutableArray alloc]init];
        [toAry addObject:[self removeDomainName_String:[[self xmppStream] myJID].bare]];
        
        NSMutableDictionary* Ruter_Dict = [NSMutableDictionary new];
        [Ruter_Dict setObject:[NSString stringWithFormat:@"%@",[messageDict objectForKey:@"from_Id"]]    forKey:@"from_Id"];
        [Ruter_Dict setObject:toAry forKey:@"to_Id"];
        [Ruter_Dict setObject:[NSString stringWithFormat:@"%@",[messageDict objectForKey:@"latitude"]]  forKey:@"latitude"];
        [Ruter_Dict setObject:[NSString stringWithFormat:@"%@",[messageDict objectForKey:@"longitude"]]  forKey:@"longitude"];
        [Ruter_Dict setObject:[NSString stringWithFormat:@"%@",[messageDict objectForKey:@"RUTer_Id"]]  forKey:@"RUTer_Id"];
        [Ruter_Dict setObject:@"0" forKey:@"ruter_status"];
        
        
        BOOL insertStatus = [dbEditer_Obj InsertRuter_Req_RUTerId:Ruter_Dict];
        
        if (insertStatus)
        {
            NSLog(@" Successfully inserted");
        }
        
    }
    
    else if ([[messageDict objectForKey:@"type"] isEqualToString:@"response"])
    {
        NSMutableDictionary* Ruter_Dict = [NSMutableDictionary new];
        [Ruter_Dict setObject:[NSString stringWithFormat:@"%@",[messageDict objectForKey:@"from_Id"]] forKey:@"from_Id"];
        [Ruter_Dict setObject:[NSString stringWithFormat:@"%@",[messageDict objectForKey:@"latitude"]] forKey:@"latitude"];
        [Ruter_Dict setObject:[NSString stringWithFormat:@"%@",[messageDict objectForKey:@"longitude"]] forKey:@"longitude"];
        [Ruter_Dict setObject:[NSString stringWithFormat:@"%@",[messageDict objectForKey:@"RUTer_Id"]] forKey:@"RUTer_Id"];
        
        if ([[NSString stringWithFormat:@"%@",[messageDict objectForKey:@"accepted"]] isEqualToString:@"1"])
        {
            [Ruter_Dict setObject:@"1" forKey:@"ruter_status"];
            [Ruter_Dict setObject:@"1" forKey:@"is_Completed"];
        }
        else
        {
            [Ruter_Dict setObject:@"3" forKey:@"ruter_status"];
            [Ruter_Dict setObject:@"2" forKey:@"is_Completed"];
        }
        
        BOOL updateStatus = [dbEditer_Obj UpdateRuter_Req_RUTerId_response:Ruter_Dict];
        if (updateStatus) {
            [dbEditer_Obj AnyReflectionOnRUTerStatus:[NSString stringWithFormat:@"%@",[messageDict objectForKey:@"RUTer_Id"]]];
        }
        
    }
    
    
    
    if (state == UIApplicationStateActive)
    {
                NSString *cancelTitle = @"Close";
                NSString *showTitle = @"Show";
                NSString *message = [NSString stringWithFormat:@" dictionary is =  %@", [userInfo valueForKey:@"aps"]];
        
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]
                                                                    message:message
                                                                   delegate:self
                                                          cancelButtonTitle:cancelTitle
                                                          otherButtonTitles:showTitle, nil];
                alertView.tag = 10;
                [alertView show];
        userInfoDict = nil;
        userInfoDict = [[NSDictionary alloc]init];
        
        userInfoDict = userInfo;
        
        
        
        
    } else {
        //Do stuff that you would do if the application was not active
        NSString *jidStr = [NSString stringWithFormat:@"%@@%@",[messageDict objectForKey:@"from_Id"],DOMAINAME];
        [self getUserDetails:jidStr];
    }
    
    
}
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    
    NSDictionary* userInfo = notification.userInfo;
    UIApplicationState state = [application applicationState];
    NSMutableDictionary* messageDict = [NSMutableDictionary new];
    messageDict =  [userInfo objectForKey:@"aps"];
    
    if (state == UIApplicationStateActive) {
        //            NSString *cancelTitle = @"Close";
        //            NSString *showTitle = @"Show";
        //            NSString *message = [NSString stringWithFormat:@" dictionary is =  %@", [userInfo valueForKey:@"aps"]];
        //
        //        //    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]
        //                                                                message:message
        //                                                               delegate:self
        //                                                      cancelButtonTitle:cancelTitle
        //                                                      otherButtonTitles:showTitle, nil];
        //     alertView.tag = 10;
        //  [alertView show];
        userInfoDict = nil;
        userInfoDict = [[NSDictionary alloc]init];
        
        userInfoDict = userInfo;
    }
    else
    {
        //Do stuff that you would do if the application was not active
        NSString *jidStr = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"from_Id"]];
        NSLog(@"JIDSTR -- >> %@",jidStr);
        [self getUserDetails:jidStr];
    }
    
    
}

-(void)messagePingSoundSetup
{
    
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"Priority" ofType: @"mp3"];
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:soundFilePath ];
    self.messagePingPlayer  = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
    [self.messagePingPlayer prepareToPlay];
    [self.messagePingPlayer play];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag == 10 && buttonIndex == 1 )
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"pushNotification" object:nil userInfo:userInfoDict];
    }
}




- (void)teardownStream
{
    [xmppStream removeDelegate:self];
    [xmppRoster removeDelegate:self];
    
    [xmppReconnect         deactivate];
    [xmppRoster            deactivate];
    [xmppvCardTempModule   deactivate];
    [xmppvCardAvatarModule deactivate];
    [xmppCapabilities      deactivate];
    
    [xmppStream disconnect];
    
    xmppStream = nil;
    xmppReconnect = nil;
    xmppRoster = nil;
    xmppRosterStorage = nil;
    xmppvCardStorage = nil;
    xmppvCardTempModule = nil;
    xmppvCardAvatarModule = nil;
    xmppCapabilities = nil;
    xmppCapabilitiesStorage = nil;
}






////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Private
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)setupStream
{
    NSAssert(xmppStream == nil, @"Method setupStream invoked multiple times");
    
    // Setup xmpp stream
    //
    // The XMPPStream is the base class for all activity.
    // Everything else plugs into the xmppStream, such as modules/extensions and delegates.
    
    xmppStream = [[XMPPStream alloc] init];
    
#if !TARGET_IPHONE_SIMULATOR
    {
        // Want xmpp to run in the background?
        //
        // P.S. - The simulator doesn't support backgrounding yet.
        //        When you try to set the associated property on the simulator, it simply fails.
        //        And when you background an app on the simulator,
        //        it just queues network traffic til the app is foregrounded again.
        //        We are patiently waiting for a fix from Apple.
        //        If you do enableBackgroundingOnSocket on the simulator,
        //        you will simply see an error message from the xmpp stack when it fails to set the property.
        
        xmppStream.enableBackgroundingOnSocket = YES;
    }
#endif
    
    // Setup reconnect
    //
    // The XMPPReconnect module monitors for "accidental disconnections" and
    // automatically reconnects the stream for you.
    // There's a bunch more information in the XMPPReconnect header file.
    
    xmppReconnect = [[XMPPReconnect alloc] init];
    
    // Setup roster
    //
    // The XMPPRoster handles the xmpp protocol stuff related to the roster.
    // The storage for the roster is abstracted.
    // So you can use any storage mechanism you want.
    // You can store it all in memory, or use core data and store it on disk, or use core data with an in-memory store,
    // or setup your own using raw SQLite, or create your own storage mechanism.
    // You can do it however you like! It's your application.
    // But you do need to provide the roster with some storage facility.
    xmppRosterStorage = [[XMPPRosterCoreDataStorage alloc] init];
    xmppRoster = [[XMPPRoster alloc] initWithRosterStorage:xmppRosterStorage dispatchQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
    [xmppRoster addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    xmppRoster.autoAcceptKnownPresenceSubscriptionRequests = YES;
    // Setup vCard support
    //
    // The vCard Avatar module works in conjuction with the standard vCard Temp module to download user avatars.
    // The XMPPRoster will automatically integrate with XMPPvCardAvatarModule to cache roster photos in the roster.
    
    xmppvCardStorage = [XMPPvCardCoreDataStorage sharedInstance];
    xmppvCardTempModule = [[XMPPvCardTempModule alloc] initWithvCardStorage:xmppvCardStorage];
    xmppvCardAvatarModule = [[XMPPvCardAvatarModule alloc] initWithvCardTempModule:xmppvCardTempModule];
    xmppCapabilitiesStorage = [XMPPCapabilitiesCoreDataStorage sharedInstance];
    xmppCapabilities = [[XMPPCapabilities alloc] initWithCapabilitiesStorage:xmppCapabilitiesStorage];
    xmppCapabilities.autoFetchHashedCapabilities = YES;
    xmppCapabilities.autoFetchNonHashedCapabilities = NO;
    _xmppRosterMemoryStorage = [XMPPRosterMemoryStorage new];
    xmppMessageArchivingStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    xmppMessageArchivingModule = [[XMPPMessageArchiving alloc] initWithMessageArchivingStorage:xmppMessageArchivingStorage];
    [xmppMessageArchivingModule setClientSideMessageArchivingOnly:YES];
    
    XMPPMessageDeliveryReceipts* xmppMessageDeliveryRecipts = [[XMPPMessageDeliveryReceipts alloc] initWithDispatchQueue:dispatch_get_main_queue()];
    xmppMessageDeliveryRecipts.autoSendMessageDeliveryReceipts = YES;
    xmppMessageDeliveryRecipts.autoSendMessageDeliveryRequests = YES;
    
    xmppMuc = [[XMPPMUC alloc]init];
    [xmppMuc activate:xmppStream];
    [xmppMuc addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    
    
    // Activate xmpp modules
    
    [xmppReconnect  activate:xmppStream];
    [xmppRoster activate:xmppStream];
    [xmppvCardTempModule   activate:xmppStream];
    [xmppvCardAvatarModule activate:xmppStream];
    [xmppCapabilities      activate:xmppStream];
    [xmppMessageArchivingModule activate:xmppStream];
    [xmppMessageDeliveryRecipts activate:self.xmppStream];
    
    
    
    
    // Add ourself as a delegate to anything we may be interested in
    [xmppStream addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    
    [xmppStream setHostName:HOSTNAME];
    [xmppStream setHostPort:5222];
    
    [self connect];
    
    // You may need to alter these settings depending on the server you're connecting to
    
    allowSelfSignedCertificates = NO;
    allowSSLHostNameMismatch = NO;
    usersearch = [SearchUser new];
    usersearch.searchdelegate = self;
    
    
}
- (BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq
{
    //DDLogVerbose(@"IQ -->> %@", [iq description]);

    return NO;
}

- (void)turnSocket:(TURNSocket *)sender didSucceed:(GCDAsyncSocket *)socket
{
    [socket setDelegate: self delegateQueue: dispatch_get_main_queue ()];
    [socket readDataWithTimeout: -1 tag: 100];
}
- (void)turnSocketDidFail:(TURNSocket *)sender
{
    
}
- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag;
{
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Auto Reconnect
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)xmppReconnect:(XMPPReconnect *)sender shouldAttemptAutoReconnect:(SCNetworkReachabilityFlags)reachabilityFlags
{
   // DDLogVerbose(@"---------- xmppReconnect:shouldAttemptAutoReconnect: ----------");
    return YES;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Capabilities
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)xmppCapabilities:(XMPPCapabilities *)sender didDiscoverCapabilities:(NSXMLElement *)caps forJID:(XMPPJID *)jid
{
    DDLogVerbose(@"---------- xmppCapabilities:didDiscoverCapabilities:forJID: ----------");
    DDLogVerbose(@"jid: %@", jid);
    DDLogVerbose(@"capabilities:\n%@",
                 [caps XMLStringWithOptions:(NSXMLNodeCompactEmptyElement | NSXMLNodePrettyPrint)]);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Connect/disconnect
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)connect
{
    if (![xmppStream isDisconnected]) {
        return YES;
    }
    
    NSString *myJID = [[NSUserDefaults standardUserDefaults] stringForKey:USERNAME_XMPP];
    NSString *myPassword = [[NSUserDefaults standardUserDefaults] stringForKey:PASSWORD_XMPP];
    
    
    if (myJID == nil || myPassword == nil) {
        return NO;
    }
    
    [xmppStream setMyJID:[XMPPJID jidWithString:myJID]];
    password = myPassword;
    
    NSError *error = nil;
    if (![xmppStream connectWithTimeout:XMPPStreamTimeoutNone error:&error])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error connecting"
                                                            message:@"See console for error details."
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
        
        DDLogError(@"Error connecting: %@", error);
        
        return NO;
    }
    
    return YES;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark XMPPStream Delegate
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (void)xmppStream:(XMPPStream *)sender socketWillConnect:(GCDAsyncSocket *)socket
{
    // Tell the socket to stay around if the app goes to the background (only works on apps with the VoIP background flag set)
    [socket performBlock:^{
        [socket enableBackgroundingOnSocket];
    }];
}





- (void)xmppStream:(XMPPStream *)sender socketDidConnect:(GCDAsyncSocket *)socket
{
 //   DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)xmppStream:(XMPPStream *)sender willSecureWithSettings:(NSMutableDictionary *)settings
{
 //   DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    if (allowSelfSignedCertificates)
    {
        [settings setObject:[NSNumber numberWithBool:YES] forKey:(NSString *)kCFStreamSSLValidatesCertificateChain];
    }
    
    if (allowSSLHostNameMismatch)
    {
        [settings setObject:[NSNull null] forKey:(NSString *)kCFStreamSSLPeerName];
    }
    else
    {
        // Google does things incorrectly (does not conform to RFC).
        // Because so many people ask questions about this (assume xmpp framework is broken),
        // I've explicitly added code that shows how other xmpp clients "do the right thing"
        // when connecting to a google server (gmail, or google apps for domains).
        
        NSString *expectedCertName = nil;
        NSString *serverDomain = xmppStream.hostName;
        NSString *virtualDomain = [xmppStream.myJID domain];
        
        if ([serverDomain isEqualToString:HOSTNAME])
        {
            if ([virtualDomain isEqualToString:HOSTNAME])
            {
                expectedCertName = virtualDomain;
            }
            else
            {
                expectedCertName = serverDomain;
            }
        }
        else if (serverDomain == nil)
        {
            expectedCertName = virtualDomain;
        }
        else
        {
            expectedCertName = serverDomain;
        }
        
        if (expectedCertName)
        {
            [settings setObject:expectedCertName forKey:(NSString *)kCFStreamSSLPeerName];
        }
    }
}
- (void)xmppStreamDidSecure:(XMPPStream *)sender
{
  //  DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)xmppStreamDidConnect:(XMPPStream *)sender
{
//    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    isXmppConnected = YES;
    
    NSError *error = nil;
    
    if (![[self xmppStream] authenticateWithPassword:password error:&error])
    {
        DDLogError(@"Error authenticating: %@", error);
    }
}

- (void)xmppStreamDidAuthenticate:(XMPPStream *)sender
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    [self goOnline];
    
    if (isContactLoaded)
    {
        [self updateRosters];
    }
    
    
    if ( [[NSUserDefaults standardUserDefaults] objectForKey:@"isFirstTime"])
    {
     [self getallRoomDetailsAndJoinRoom];
    }
    
    
}


- (void)xmppStreamDidDisconnect:(XMPPStream *)sender withError:(NSError *)error
{
    
    if(!isLastLoginUpdated)
    {
    
    NSDate *myDate = [NSDate date];
    [[NSUserDefaults standardUserDefaults] setObject:myDate forKey:@"XmppLastOffline"];
     NSLog(@"disconnect from XMPP");
        isLastLoginUpdated = YES;
        
    }
}


-(NSString *)getRoomSubjectName:(XMPPJID *)jid
{
    XMPPRoomCoreDataStorage *storage = [XMPPRoomCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:storage.messageDetailsEntityName
                                              inManagedObjectContext:moc];
    [request setEntity:entity];
    NSString *predicateFrmt = @"roomJIDStr = %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, [jid bareJID]];
    [request setPredicate:predicate];
    NSArray *empArray=[moc executeFetchRequest:request error:nil];
    if ([empArray count] == 1){
        for (XMPPRoomMessageDetails *details in  empArray)
        {
            if (details.subject != nil)
                return details.subject;
        }
    }
    return @"Creating Group";
}





/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Join Room
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(void)getallRoomDetailsAndJoinRoom
{
    XMPPRoomCoreDataStorage *storage = [XMPPRoomCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:storage.messageDetailsEntityName inManagedObjectContext:moc];
    [request setEntity:entity];
    NSArray *empArray=[moc executeFetchRequest:request error:nil];
    if ([empArray count] > 0){
        for (XMPPRoomMessageDetails *user in  empArray)
        {
            XMPPRoomCoreDataStorage  * xmppRoomStorage = [XMPPRoomCoreDataStorage sharedInstance];
            XMPPRoom *xmppRoom = [[XMPPRoom alloc] initWithRoomStorage:xmppRoomStorage jid:[XMPPJID jidWithString:user.roomJIDStr]  dispatchQueue:dispatch_get_main_queue()];
            [xmppRoom activate:[self xmppStream]];
            if (![xmppRoom isJoined]) {
                NSXMLElement *history = [NSXMLElement elementWithName:@"history"];
                [history addAttributeWithName:@"maxstanzas" intValue:0];
                [xmppRoom joinRoomUsingNickname:[[self xmppStream] myJID].user history:[self historyElement:[self lastestRoomMessageDate:xmppRoom.roomJID]]];
            }
        }
    }
}

-(NSXMLElement *)historyElement:(NSDate *)date
{
    NSXMLElement *history = [NSXMLElement elementWithName:@"history"];
    if (date) {
        [history addAttributeWithName:@"since" stringValue:[date xmppDateTimeString]];
    }else{
        
        
        
        
        // Get
        NSDate *myDate = (NSDate *)[[NSUserDefaults standardUserDefaults] objectForKey:@"XmppLastOffline"];

        if (myDate)
        {
            NSLog(@"xmppLastOffline : %@ ",myDate);
            [history addAttributeWithName:@"since" stringValue:[myDate xmppDateTimeString]];
        }
        else
        {
        [history addAttributeWithName:@"seconds" stringValue:@"600"];
        }
    }
    return history;
}


-(NSDate *)lastestRoomMessageDate:(XMPPJID *) roomJID
{
    XMPPRoomCoreDataStorage *roomStorage = [XMPPRoomCoreDataStorage sharedInstance];

    NSDate *lastesMessageDate = [roomStorage mostRecentMessageTimestampForRoom:roomJID stream:xmppStream inContext:nil];
    return lastesMessageDate;
}







-(void)notification:(XMPPMessage *)message
{
    _BadgeCount ++;
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif == nil)
        return;
    localNotif.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
    
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
    
    // Notification details
    /// localNotif.alertTitle = [message from].bare;
    
    NSString *displayName = [self getDisplayName:[message from].bare];
    
    NSArray *filterArray = [NSArray arrayWithObject:message.body];
    
    NSArray *beginMatch = [filterArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self BEGINSWITH[cd] %@", @"sticker_"]];
    
    NSXMLElement *file = [message elementForName:@"file"];
    
    
    NSString *ReceviedMessage;
    if(beginMatch.count == 1)
    {
        NSLog(@"Stickers : %@",message.body);
        ReceviedMessage = [NSString findStickerName:message.body];
    }
    else if ([[file attributeForName:@"file_type"] stringValue])
    {
        NSString *fileType = [[file attributeForName:@"file_type"] stringValue];
        if ([fileType isEqualToString:@"image"])
        {
            ReceviedMessage = [NSString stringWithFormat:@"📷 Image"];
        }
        else
        {
            ReceviedMessage = [NSString stringWithFormat:@"📹 Video"];
        }
    }
    else
    {
        
        NSData *newdata=[message.body dataUsingEncoding:NSUTF8StringEncoding
                                      allowLossyConversion:YES];
        ReceviedMessage =[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];

    }
    
    localNotif.alertBody = [NSString stringWithFormat:@"%@ : %@" ,displayName, ReceviedMessage];
    // Set the action button
    localNotif.alertAction = @"View";
    localNotif.alertAction = @"Yes";
    
    localNotif.applicationIconBadgeNumber = _BadgeCount;
    
    // Specify custom data for the notification
    NSDictionary *infoDict = [NSDictionary dictionaryWithObject:[message from].bare forKey:@"from_Id"];
    
    localNotif.userInfo = infoDict;
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    [localNotif setTimeZone:[NSTimeZone  defaultTimeZone]];
    
    // Schedule the notification
    
    [[UIApplication sharedApplication] presentLocalNotificationNow:localNotif];
}



- (void)xmppStream:(XMPPStream *)sender didNotAuthenticate:(NSXMLElement *)error
{
  //  DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Presence Management
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)goOnline
{
    XMPPPresence *presence = [XMPPPresence presence]; // type="available" is implicit
    NSString *domain = [[[self xmppStream] myJID] domain];
    //Google set their presence priority to 24, so we do the same to be compatible.
    if([domain isEqualToString:HOSTNAME])
    {
        NSXMLElement *priority = [NSXMLElement elementWithName:@"priority" stringValue:@"24"];
        [presence addChild:priority];
    }
    [[self xmppStream] sendElement:presence];
    
    isLastLoginUpdated = NO;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Go offline
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)goOffline
{
    NSXMLElement *presence = [NSXMLElement elementWithName:@"presence"];
    [presence addAttributeWithName:@"type" stringValue:@"unavailable"];
    [[self xmppStream] sendElement:presence];
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Did receive Message
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message
{
 //   DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    //
    //    if ([[[message from]bare] isEqualToString:[self xmppStream].myJID.bare])
    //    {
    //        NSLog(@"its Me");
    //    }
    //
    if([message isChatMessageWithBody])
    {
        [self ShowNotification:message];
        
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"SingleChatNotification"] == YES)
        {
            [self messagePingSoundSetup];
            
        }
        
        NSXMLElement *ruder = [message elementForName:@"ruthere"];
        
        if (ruder)
        {
            DBEditer* dbEditer_Obj = [DBEditer new];
            [dbEditer_Obj UpdateRUTer_MsgDb:ruder];
        }
        
        NSXMLElement *imp = [message elementForName:@"imp"];
        
        if (imp)
        {
            NSLog(@"this is imp message");
          
        }

        
        NSXMLElement *file = [message elementForName:@"file"];
        
        if (file)
        {
            mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
            NSMutableDictionary* dict = [NSMutableDictionary new];
            [dict setObject:[[message attributeForName:@"id"] stringValue]  forKey:@"msgId"];
            [dict setObject:[message from].bare forKey:@"userId"];
            [dict setObject:[[file attributeForName:@"file_id"] stringValue] forKey:@"fileName"];
            [dict setObject:[[file attributeForName:@"file_url"] stringValue] forKey:@"url"];
            [dict setObject:[NSNumber numberWithBool:YES] forKey:@"loc_cloud"];
            [dict setObject:[NSNumber numberWithBool:YES] forKey:@"sent_received"];
            [dict setObject:@"0" forKey:@"success_failure"];
            [dict setObject:[NSNumber numberWithBool:YES] forKey:@"sender_receiver"];
            [dict setObject:[NSNumber numberWithFloat:0.0] forKey:@"percentLoaded"];
            [dict setObject:[[file attributeForName:@"file_type"] stringValue] forKey:@"video_img"];
            [dict setObject:[[file elementForName:@"file_preview"] stringValue] forKey:@"imgData"];
            [dict setObject:[NSNumber numberWithBool:NO] forKey:@"isPrivate"];
            
            [mediaDb checkAndInsert:dict];
            
            NSString *fileType = [[file attributeForName:@"file_type"] stringValue];
            NSString *fileTypeStr;
            if ([fileType isEqualToString:@"image"])
            {
                fileTypeStr =  [NSString stringWithFormat:@"📷 Image"];
            }
            else
            {
                fileTypeStr =  [NSString stringWithFormat:@"📹 Video"];
            }
            
            
            double delayInSeconds = 1.5;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
               // [self showTyping:[message from].bare withString:fileTypeStr];
                
            });
            
            
        }
        NSXMLElement *private = [message elementForName:@"private"];
        if (private)
        {
            if (file)
            {
                mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
                NSMutableDictionary* dict = [NSMutableDictionary new];
                [dict setObject:[[message attributeForName:@"id"] stringValue]  forKey:@"msgId"];
                [dict setObject:[NSNumber numberWithBool:YES] forKey:@"isPrivate"];
                [mediaDb updateMedia:dict];
                
            }
            privateChatDbEditer* privateSection = [privateChatDbEditer new];
            
            if ([[[private attributeForName:@"type"] stringValue] isEqualToString:@"request"])
            {
                NSMutableDictionary* dict = [NSMutableDictionary new];
                [dict setObject:[message from].bare  forKey:@"iD"];
                
                NSMutableDictionary* privateSectionDict = [privateSection fetchprivateChat:[message from].bare];
                if ([[privateSectionDict objectForKey:@"status"] isEqualToString:@"pending"])
                {
                    [self timerInvalidate:[message from].bare];
                    [dict setObject:@"accepted" forKey:@"status"];
                    [dict setObject:[NSNumber numberWithDouble:0] forKey:@"timer"];
                    [dict setObject:[NSNumber numberWithBool:NO] forKey:@"is_Sender"];

                }
                
                else
                {
                    [dict setObject:@"request" forKey:@"status"];
                    [dict setObject:[NSNumber numberWithDouble:30] forKey:@"timer"];
                    [dict setObject:[NSNumber numberWithBool:NO] forKey:@"is_Sender"];
                }
                
                BOOL check = [privateSection checkAndInsert:dict];
                if (check )
                {
                    
                }
            }
            else if ([[[private attributeForName:@"type"] stringValue] isEqualToString:@"response"])
            {
                [self timerInvalidate:[message from].bare];
                
                NSMutableDictionary* dict = [NSMutableDictionary new];
                [dict setObject:[message from].bare  forKey:@"iD"];
                if ([[[private attributeForName:@"response"] stringValue] isEqualToString:@"0"])
                {
                    [dict setObject:@"denied" forKey:@"status"];
                    [dict setObject:[NSNumber numberWithDouble:0] forKey:@"timer"];
                }
                else
                {
                    [dict setObject:@"accepted" forKey:@"status"];
                    [dict setObject:[NSNumber numberWithDouble:30] forKey:@"timer"];
                    
                }
                [dict setObject:[NSNumber numberWithBool:YES] forKey:@"is_Sender"];
                
                
                BOOL check = [privateSection checkAndInsert:dict];
                if (check )
                {
                    
                   // NSLog(@"success ");
                }
                
            }
            else if ([[[private attributeForName:@"type"] stringValue] isEqualToString:@"close"])
            {
                
                BOOL check = [privateSection deletePrivateChat:[message from].bare];
                if (check)
                {
                    [self timerInvalidate:[message from].bare];
                    
                }
                
            }
        }
        
    }
    
    else if ([message isGroupChatMessage])
    {
        if (![message isGroupChatMessageWithSubject])
        {
          //  [self ShowNotification:message];

        }

        if (![[message from].resource isEqualToString:xmppStream.myJID.user]) {
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"GroupChatNotification"] == YES)
            {
                [self messagePingSoundSetup];
                
            }
        }
        
        NSXMLElement *ruder = [message elementForName:@"ruthere"];
        
        if (ruder)
        {
            if (![[[ruder attributeForName:@"from"] stringValue] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME]] ) {
                DBEditer* dbEditer_Obj = [DBEditer new];
                [dbEditer_Obj UpdateRUTer_MsgDb:ruder];
            }
            
        }
        
        NSXMLElement *file = [message elementForName:@"file"];
        
        if (file)
        {
            
            
            if (![[[[[message attributeForName:@"from"] stringValue] componentsSeparatedByString:@"/"] lastObject] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME]])
            {
                mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
                NSMutableDictionary* dict = [NSMutableDictionary new];
                [dict setObject:[[message attributeForName:@"id"] stringValue]  forKey:@"msgId"];
                [dict setObject:[message from].bare forKey:@"userId"];
                [dict setObject:[[file attributeForName:@"file_id"] stringValue] forKey:@"fileName"];
                [dict setObject:[[file attributeForName:@"file_url"] stringValue] forKey:@"url"];
                [dict setObject:[NSNumber numberWithBool:YES] forKey:@"loc_cloud"];
                [dict setObject:[NSNumber numberWithBool:YES] forKey:@"sent_received"];
                [dict setObject:@"0" forKey:@"success_failure"];
                [dict setObject:[NSNumber numberWithBool:YES] forKey:@"sender_receiver"];
                [dict setObject:[NSNumber numberWithFloat:0.0] forKey:@"percentLoaded"];
                [dict setObject:[[file attributeForName:@"file_type"] stringValue] forKey:@"video_img"];
                [dict setObject:[[file elementForName:@"file_preview"] stringValue] forKey:@"imgData"];
                
                [mediaDb checkAndInsertGroup:dict];
               // NSMutableDictionary* receiveddict = [mediaDb fetchMedia:@"123456780"];
               // NSLog(@"dict   %@",receiveddict);
                
                NSString *fileType = [[file attributeForName:@"file_type"] stringValue];
                NSString *fileTypeStr;
                if ([fileType isEqualToString:@"image"])
                {
                    fileTypeStr =  [NSString stringWithFormat:@"📷 Image"];
                }
                else
                {
                    fileTypeStr =  [NSString stringWithFormat:@"📹 Video"];
                }

                
                double delayInSeconds = 1.5;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    
                  //  [self showTyping:[message from].bare withString:fileTypeStr];
                    
                });
                
            }
            
            
            
        }
        
        //==>To update group by downloading and save in database====//Start
        NSXMLElement *groupIconUpdate = [message elementForName:@"group"];
        if (groupIconUpdate) {
            if ([[[groupIconUpdate attributeForName:@"type"]stringValue] isEqualToString:@"3"] && ![[message from].resource isEqualToString:[self xmppStream].myJID.user])
                [[GroupIconService sharedInstance] downloadIconAndSave:[message from].bareJID];
        }        //==>To update group by downloading and save in database====//End
        
        
        
    }
    else if ([message hasComposingChatState] == YES)
    {
      //  NSLog(@"Chat State : %@",message.chatState);
        
        if (![self.singleton.chatScreenUser isEqualToString:[message from].bare])
        {
            
            //  [[self showTyping:[message from].bare].Jidstr withString:@"Buzzing"];
            
            //     NSString *jID = [message from].bare;
            //      [self showTyping:jID withString:BuzzStr];
            
        //    [self.dummywindow initWithNoti_jidStr:[message from].bare NameLbl:[message from].bare MsgLbl:@"Buzzing..." showClose:YES];
            
        }
        
    }
    else if ([message hasGoneChatState] == YES)
    {
        NSLog(@"Chat State : %@",message.chatState);
        
        //   NSString *jID = [message from].bare;
        //  [self showTyping:jID withString:[self getLastMsg:jID]];
    }
    else if ([message hasActiveChatState] == YES)
    {
       // NSLog(@"Chat State : %@",message.chatState);
        
        //   NSString *jID = [message from].bare;
        //  [self showTyping:jID withString:[self getLastMsg:jID]];
    }
    else if ([message hasInactiveChatState] == YES)
    {
       // NSLog(@"Chat State : %@",message.chatState);
        
        //   NSString *jID = [message from].bare;
        //  [self showTyping:jID withString:[self getLastMsg:jID]];
    }
    else if ([message hasPausedChatState] == YES)
    {
     //   NSLog(@"Chat State : %@",message.chatState);
        
        //   NSString *jID = [message from].bare;
        //  [self showTyping:jID withString:[self getLastMsg:jID]];
    }
    
    
    
}

-(void)executeAfterDelay
{
    
}

-(void)showTyping:(NSString *)Jidstr withString:(NSString *)UpdateString
{
    
    NSManagedObjectContext *moc = [xmppMessageArchivingStorage mainThreadManagedObjectContext];
    [moc performBlockAndWait:^(void){
        NSEntityDescription *contactEnityDes = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Contact_CoreDataObject" inManagedObjectContext:moc];
        NSFetchRequest *contactRequest = [[NSFetchRequest alloc]init];
        [contactRequest setEntity:contactEnityDes];
        NSString *predicateFrmt = @"bareJidStr == %@";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,Jidstr];
        [contactRequest setPredicate:predicate];
        NSArray *delArray=[moc executeFetchRequest:contactRequest error:nil];
        
        for (XMPPMessageArchiving_Contact_CoreDataObject *coreMessage in delArray)
        {
            [coreMessage setValue:UpdateString forKey:@"mostRecentMessageBody"];
            NSError *error = nil;
            if (![moc save:&error])
            {
                NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
            }
            //   NSLog(@"coreMessage:----> %@ ", coreMessage);
            
            
        }

    }];
    
}


- (NSString *)getLastMsg:(NSString*)JidStr {
    
    
    NSManagedObjectContext *moc = [xmppMessageArchivingStorage mainThreadManagedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:moc];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    NSString *predicateFrmt = @"bareJidStr == %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,JidStr];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:[NSSortDescriptor sortValues:@"timestamp" ascending:YES]];
    NSArray *delArray=[moc executeFetchRequest:fetchRequest error:nil];
    XMPPMessageArchiving_Message_CoreDataObject *coreMessage = [delArray lastObject];
    // NSLog(@"Last Message %@",coreMessage.body);
    return coreMessage.body;
}



-(void)ShowNotification:(XMPPMessage *)message
{
    self.singleton = [Singleton sharedMySingleton];
    NSXMLElement *file = [message elementForName:@"file"];
    NSXMLElement *imp = [message elementForName:@"imp"];

    if (_Background)
    {
        [self notification:message];
    }
    else if ([message isGroupChatMessage])
    {
        if (![message isGroupChatMessageWithSubject])
        {
            
            if ([[file attributeForName:@"file_type"] stringValue])
            {
                NSString *fileType = [[file attributeForName:@"file_type"] stringValue];
                if ([fileType isEqualToString:@"image"])
                {
                    [self.dummywindow initWithNoti_jidStr:[message from].bare NameLbl:[message from].bare MsgLbl:[NSString stringWithFormat:@"📷 Image"] showClose:YES];
                }
                else
                {
                    [self.dummywindow initWithNoti_jidStr:[message from].bare NameLbl:[message from].bare MsgLbl:[NSString stringWithFormat:@"📹 Video"] showClose:YES];
                }
            }
            else if (imp)
            {
                NSString *ReceviedMessage;
                if (![self.singleton.chatScreenUser isEqualToString:[message from].bare])
                {
                    
                    NSArray *filterArray = [NSArray arrayWithObject:message.body];
                    
                    NSArray *beginMatch = [filterArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self BEGINSWITH[cd] %@", @"sticker_"]];
                    
                    if(beginMatch.count == 1)
                    {
                        ReceviedMessage = [NSString findStickerName:message.body];
                    }
                    else
                    {
                        ReceviedMessage = message.body;
                    }
                    
                    NSData *newdata=[ReceviedMessage dataUsingEncoding:NSUTF8StringEncoding
                                                  allowLossyConversion:YES];
                    NSString *mystring=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];
                    
                    [self.dummywindow initWithNoti_jidStr:[message from].bare NameLbl:[message from].bare MsgLbl:mystring showClose:YES showImp:YES];
                }
                
                
            }
            
            
            else
            {
                NSString *ReceviedMessage;
                if (![self.singleton.chatScreenUser isEqualToString:[message from].bare])
                {
                    
                    NSArray *filterArray = [NSArray arrayWithObject:message.body];
                    
                    NSArray *beginMatch = [filterArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self BEGINSWITH[cd] %@", @"sticker_"]];
                    
                    if(beginMatch.count == 1)
                    {
                        ReceviedMessage = [NSString findStickerName:message.body];
                    }
                    else
                    {
                        ReceviedMessage = message.body;
                    }
                    
                    NSData *newdata=[ReceviedMessage dataUsingEncoding:NSUTF8StringEncoding
                                                  allowLossyConversion:YES];
                    NSString *mystring=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];
                    
                    [self.dummywindow initWithNoti_jidStr:[message from].bare NameLbl:[message from].bare MsgLbl:mystring showClose:YES];
                }
            }
            
        }
    }
    else if ([[file attributeForName:@"file_type"] stringValue])
    {
        NSString *fileType = [[file attributeForName:@"file_type"] stringValue];
        if ([fileType isEqualToString:@"image"])
        {
            [self.dummywindow initWithNoti_jidStr:[message from].bare NameLbl:[message from].bare MsgLbl:[NSString stringWithFormat:@"📷 Image"] showClose:YES];
        }
        else
        {
            [self.dummywindow initWithNoti_jidStr:[message from].bare NameLbl:[message from].bare MsgLbl:[NSString stringWithFormat:@"📹 Video"] showClose:YES];
        }
    }
    else if (imp)
    {
        NSString *ReceviedMessage;
        if (![self.singleton.chatScreenUser isEqualToString:[message from].bare])
        {
            
            NSArray *filterArray = [NSArray arrayWithObject:message.body];
            
            NSArray *beginMatch = [filterArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self BEGINSWITH[cd] %@", @"sticker_"]];
            
            if(beginMatch.count == 1)
            {
                ReceviedMessage = [NSString findStickerName:message.body];
            }
            else
            {
                ReceviedMessage = message.body;
            }
            
            NSData *newdata=[ReceviedMessage dataUsingEncoding:NSUTF8StringEncoding
                                          allowLossyConversion:YES];
            NSString *mystring=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];
            
            [self.dummywindow initWithNoti_jidStr:[message from].bare NameLbl:[message from].bare MsgLbl:mystring showClose:YES showImp:YES];
        }

        
    }

    
    else
    {
        NSString *ReceviedMessage;
        if (![self.singleton.chatScreenUser isEqualToString:[message from].bare])
        {
            
            NSArray *filterArray = [NSArray arrayWithObject:message.body];
            
            NSArray *beginMatch = [filterArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self BEGINSWITH[cd] %@", @"sticker_"]];
            
            if(beginMatch.count == 1)
            {
                ReceviedMessage = [NSString findStickerName:message.body];
            }
            else
            {
                ReceviedMessage = message.body;
            }
            
            NSData *newdata=[ReceviedMessage dataUsingEncoding:NSUTF8StringEncoding
                                         allowLossyConversion:YES];
            NSString *mystring=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];
            
            [self.dummywindow initWithNoti_jidStr:[message from].bare NameLbl:[message from].bare MsgLbl:mystring showClose:YES];
        }
    }
    
    
}

-(NSString *)getDisplayName:(NSString *)resourceStr
{
    if ([resourceStr rangeOfString:DOMAINAME].location==NSNotFound )
        resourceStr = [resourceStr stringByAppendingString:[NSString stringWithFormat:@"@%@",DOMAINAME]];
    
    if ([resourceStr isEqualToString:xmppStream.myJID.bare])
        return @"You";
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:[self managedObjectContext_roster]];
    [request setEntity:entity];
    NSString *predicateFrmt = @"jidStr = %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, resourceStr];
    [request setPredicate:predicate];
    NSArray *empArray=[[self managedObjectContext_roster] executeFetchRequest:request error:nil];
    if ([empArray count] == 1)
    {
        for (XMPPUserCoreDataStorageObject *userdetails in  empArray)
        {
            
            return [userdetails.displayName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        }
    }
    [resourceStr componentsSeparatedByString:@"@"];
    return [[resourceStr componentsSeparatedByString:@"@"] objectAtIndex:0];
}


- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence
{
//    presence = [XMPPPresence presence];
//    [[self xmppStream] sendElement:presence];
    
}

-(void)timerInvalidate:(NSString*)ID
{
    self.singleton = [Singleton sharedMySingleton];
    if ([self.singleton.privateReqIndicatorDict objectForKey:ID]) {
        NSTimer* timer = [self.singleton.privateReqIndicatorDict objectForKey:ID];
        [timer invalidate];
        [self.singleton.privateReqIndicatorDict removeObjectForKey:ID];
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark XMPPRosterDelegate
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSManagedObjectContext *)managedObjectContext_roster
{
    return [ xmppRosterStorage mainThreadManagedObjectContext];
}


- (void)xmppRoster:(XMPPRoster *)sender didReceivePresenceSubscriptionRequest:(XMPPPresence *)presence;
{
   // DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    XMPPUserCoreDataStorageObject *user = [[self xmppRosterStorage] userForJID:[presence from]
                                                                    xmppStream:[self xmppStream]
                                                          managedObjectContext:[self managedObjectContext_roster]];
    if (user != nil && [user.subscription isEqualToString:@"from"]) {
        XMPPPresence *response = [XMPPPresence presenceWithType:@"subscribed" to:[presence from]];
        [xmppStream sendElement:response];
        NSString *displayName = [user displayName];
        NSString *jidStrBare = [presence fromStr];
        NSString *body = nil;
        
        if (![displayName isEqualToString:jidStrBare])
            body = [NSString stringWithFormat:@"Buddy request from %@ <%@>", displayName, jidStrBare];
        else
            body = [NSString stringWithFormat:@"Buddy request from %@", displayName];
    }
    else
    {
        XMPPPresence *response = [XMPPPresence presenceWithType:@"subscribed" to:[presence from]];
        [xmppStream sendElement:response];
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Method to Add roster
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)userDetails:(NSString*)jidStr{
    
    if (![self.tempAry containsObject:jidStr]) {
        [self.tempAry addObject:jidStr];
      //  NSLog(@"temp ary %@",self.tempAry);
    }
    
    [xmppRoster addUser:[XMPPJID jidWithString:jidStr] withNickname:[ContactsDict objectForKey:jidStr]];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UpdateRosters
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)updateRosters
{
    CTTelephonyNetworkInfo *info = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = info.subscriberCellularProvider;
    
    
   // NSLog(@"country code is: %@", carrier.mobileCountryCode);
    
    
    
    SearchUser *search_user_obj = [SearchUser new];
    search_user_obj.searchdelegate = self;
    
    ContactsDict = [NSMutableDictionary new];
    
 //   NSLog(@"%lu",(unsigned long)[_contactsAry count]);
    if (self.contactsAry.count)
    {
        for (id contacts in self.contactsAry)
        {
            
            contactData *person = contacts;
            person.IMAry = [NSMutableArray new];
            
            NSString* nickName = [NSString stringWithFormat:@"%@ %@",person.firstNames,person.lastNames];
            
            NSMutableArray* phoneNumberArray = person.phoneNumbers;
            
            for (id phoneNumber in phoneNumberArray)
            {
                
                NSString*  number = [[phoneNumber componentsSeparatedByCharactersInSet:
                                      [[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"]
                                       invertedSet]]
                                     componentsJoinedByString:@""];
                
                if (number.length >= 10) {
                    NSString* selectedNum = [self checkPhoneNum:number];
                    
                    
                    if (selectedNum != nil) {
                        
                        __block NSString *blocknickName = nickName;
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if ([self RecentcontactsFetch:[NSString stringWithFormat:@"%@@%@",selectedNum,DOMAINAME]] == nil) {
                                if ([self checkWhiteSpace:nickName])
                                {
                                    blocknickName = number;
                                    [usersearch searchUserName:selectedNum];
                                    [ContactsDict setObject:nickName forKey:[NSString stringWithFormat:@"%@@%@",selectedNum,DOMAINAME]];
                                    blocknickName =@"";
                                }
                                else{
                                    
                                    [usersearch searchUserName:selectedNum];
                                    [ContactsDict setObject:nickName forKey:[NSString stringWithFormat:@"%@@%@",selectedNum,DOMAINAME]];
                                }
                            }
                            else{
                                
                                if ([self checkWhiteSpace:nickName])
                                {
                                    blocknickName = number;
                                    [usersearch searchUserName:selectedNum];
                                    [ContactsDict setObject:nickName forKey:[NSString stringWithFormat:@"%@@%@",selectedNum,DOMAINAME]];
                                    blocknickName =@"";
                                }
                                else{
                                    
                                    [usersearch searchUserName:selectedNum];
                                    [ContactsDict setObject:nickName forKey:[NSString stringWithFormat:@"%@@%@",selectedNum,DOMAINAME]];
                                }
                                
                                [person.IMAry addObject:selectedNum];
                            }
                        });
                        
                        
                        
                    }
                    else
                    {
                    }
                }
                
            }
        }
    }
    NSXMLElement *query = [NSXMLElement elementWithName:@"query" xmlns:@"jabber:iq:roster"];
    NSXMLElement *iq = [NSXMLElement elementWithName:@"iq"];
    [iq addAttributeWithName:@"type" stringValue:@"get"];
    [iq addChild:query];
    [[self xmppStream] sendElement:iq];
    
}

-(NSString*)checkPhoneNum:(NSString*)number
{
  

    if (number.length == 10)
    {
        number = [NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"],number];
        return number;
    }
    else if (number.length == 11)
    {
      

        
        NSString* subNumber=[number substringToIndex:1];
        if ([subNumber isEqualToString:@"0"])
        {
            NSString *newStr = [number substringWithRange:NSMakeRange(1, [number length]-1)];
            number =  [NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"],newStr];
            return number;
        }
        else
        {
            return nil;
        }
    }
    else
    {
        
        return number;
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Method to get Contacts for Address book
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(NSArray *)getAllContacts
{
    if (![[NSUserDefaults standardUserDefaults] objectForKey:SOS_CONTACTDATA])
    {
        [[NSUserDefaults standardUserDefaults] setObject:[[NSMutableArray alloc]init] forKey:SOS_CONTACTDATA];
        
    }
    
    NSMutableArray * EncodedSelContactsAry = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:SOS_CONTACTDATA]];
    NSMutableArray * selContactsAry = [NSMutableArray new];
    for (NSData *personEncodedObject in EncodedSelContactsAry) {
        sosContactObject *personObject = [NSKeyedUnarchiver unarchiveObjectWithData:personEncodedObject];
        personObject.doesExistOnPhoneBook = NO;
        [selContactsAry addObject:personObject];
    }
    
    //---------------//---------------//
    
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    CFErrorRef *error = nil;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
    NSArray * people;
    BOOL accessGranted = [self __addressBookAccessStatus:addressBook];
    
    if (accessGranted)
    {
        people = (__bridge_transfer NSArray*)ABAddressBookCopyArrayOfAllPeople(addressBook);
    }
    CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);
    
    for (CFIndex i = 0; i < nPeople; i++)
    {
        
        contactData *contacts = [contactData new];
        ABRecordRef record = CFArrayGetValueAtIndex((__bridge CFArrayRef)(people), i);
        NSInteger recordId   = ABRecordGetRecordID((ABRecordRef)record);
        
        NSString *firstName = (__bridge NSString *)ABRecordCopyValue(record, kABPersonFirstNameProperty);
        NSString *lastName = (__bridge NSString *)ABRecordCopyValue(record, kABPersonLastNameProperty);
        
        // if (ABPersonGetTypeOfProperty(kABPersonFirstNameProperty) == kABPersonCompositeNameFormatFirstNameFirst)
        if (firstName)
            contacts.firstNames = [NSString stringWithFormat:@"%@ ",firstName];
        else
            contacts.firstNames = @"";
        if (lastName)
            contacts.lastNames = [NSString stringWithFormat:@"%@",lastName];
        else
            contacts.lastNames = @"";
        
        contacts.uniqueId = [NSString stringWithFormat:@"%ld",(long)recordId];
        
        
        NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
        NSString *trimmedString = [[NSString stringWithFormat:@"%@%@",contacts.firstNames,contacts.lastNames] stringByTrimmingCharactersInSet:charSet];
        
        if ([trimmedString isEqualToString:@""])
        {
            contacts.firstLetter = @"#";
        }
        else
        {
            contacts.firstLetter = [[trimmedString substringToIndex:1] uppercaseString];
        }
        
        
        
        ABMutableMultiValueRef phoneNumbers = ABRecordCopyValue(record, kABPersonPhoneProperty);
        CFIndex phoneNumberCount = ABMultiValueGetCount( phoneNumbers );
        NSMutableArray *phoneNumbersAry = [NSMutableArray new];
        for ( CFIndex k=0; k<phoneNumberCount; k++ )
        {
            CFStringRef phoneNumberLabel = ABMultiValueCopyLabelAtIndex( phoneNumbers, k );
            CFStringRef phoneNumberValue = ABMultiValueCopyValueAtIndex( phoneNumbers, k );
            CFStringRef phoneNumberLocalizedLabel = ABAddressBookCopyLocalizedLabel( phoneNumberLabel );
            [phoneNumbersAry addObject:CFBridgingRelease(phoneNumberValue)];
            
            
            if (phoneNumberLocalizedLabel != nil)
                CFRelease(phoneNumberLocalizedLabel);
            if (phoneNumberLabel != nil)
                CFRelease(phoneNumberLabel);
            if (phoneNumberValue != nil)
                CFRelease(phoneNumberValue);
        }
        contacts.phoneNumbers = phoneNumbersAry;
        
        // for emails
        
        
        
        
        
        ABMultiValueRef emailIds = ABRecordCopyValue(record, kABPersonEmailProperty);
        CFIndex emailIdCount = ABMultiValueGetCount( emailIds );
        NSMutableArray *emailIdAry = [NSMutableArray new];
        for ( CFIndex k=0; k < emailIdCount; k++ )
        {
            
            CFStringRef email = ABMultiValueCopyValueAtIndex(emailIds, k );
            
            if (email) {
                [emailIdAry addObject:CFBridgingRelease(email)];
                
            }
            
            
            
            
            if (email != nil)
                CFRelease(email);
            
        }
        contacts.emailIds = emailIdAry;
        
        
        
        CFRelease(record);
        
        if ([contacts.emailIds count]+[contacts.phoneNumbers count])
        {
            
            NSPredicate* contactPredicate = [NSPredicate predicateWithFormat:@"uniqueId == %@", contacts.uniqueId];
            for (sosContactObject* tempContact in [NSMutableArray arrayWithArray:[selContactsAry filteredArrayUsingPredicate:contactPredicate]])
            {
                tempContact.doesExistOnPhoneBook = YES;
                
                if (!([contacts.phoneNumbers containsObject:tempContact.selectedNumber]))
                {
                    tempContact.selectedNumber = @"";
                }
                else
                {
                    contacts.selectedNumber = tempContact.selectedNumber;
                }
                
                if (!([contacts.emailIds containsObject:tempContact.selectedEmail]))
                {
                    tempContact.selectedEmail = @"";
                }
                else
                {
                    contacts.selectedEmail = tempContact.selectedEmail;
                }
                
                if (!(tempContact.selectedEmail.length || tempContact.selectedNumber.length))
                {
                    [selContactsAry removeObject:tempContact];
                }
                
            }
            [items addObject:contacts];
            
        }
        
        
        emailIdAry = nil;
        emailIds = nil;
        
        contacts = nil;
        phoneNumbersAry = nil;
    }
    
    NSPredicate* contactPredicate = [NSPredicate predicateWithFormat:@"doesExistOnPhoneBook = NO"];
    for (sosContactObject* tempContact in [NSMutableArray arrayWithArray:[selContactsAry filteredArrayUsingPredicate:contactPredicate]])
    {
        [selContactsAry removeObject:tempContact];
        
    }
    [self saveContactDataArray:selContactsAry];
    return items;
    
}

- (void)saveContactDataArray:(NSMutableArray *)selectedContactAry {
    
    
    NSMutableArray *archiveArray = [NSMutableArray arrayWithCapacity:selectedContactAry.count];
    for (sosContactObject *personObject in selectedContactAry) {
        NSData *personEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:personObject];
        [archiveArray addObject:personEncodedObject];
    }
    
    NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
    [userData setObject:archiveArray forKey:SOS_CONTACTDATA];
    [userData synchronize];
}
-(BOOL)__addressBookAccessStatus:(ABAddressBookRef) addressBook
{
    __block BOOL accessGranted = NO;
    
    if (ABAddressBookRequestAccessWithCompletion != NULL) { // we're on iOS 6
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        });
        
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        // dispatch_release(sema);
    }
    else { // we're on iOS 5 or older
        accessGranted = YES;
    }
    return accessGranted;
}

-(BOOL)checkWhiteSpace :(NSString*)string
{
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[string stringByTrimmingCharactersInSet: set] length] == 0)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}
-(NSMutableDictionary*)createDictionary
{
    
    NSMutableDictionary* Ruter_Dict = [NSMutableDictionary new];
    [Ruter_Dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"USERNAME"] forKey:@"from_Id"];
    [Ruter_Dict setObject:@"8144299658" forKey:@"to_Id"];
    [Ruter_Dict setObject:@"17.382095" forKey:@"latitude"];
    [Ruter_Dict setObject:@"70.486221" forKey:@"logitude"];
    [Ruter_Dict setObject:@"7" forKey:@"RUTer_Id"];
    [Ruter_Dict setObject:@"pending" forKey:@"ruter_status"];
    return Ruter_Dict;
    
    
}

-(NSMutableDictionary*)updateDictionary
{
    NSMutableDictionary* Ruter_Dict = [NSMutableDictionary new];
    [Ruter_Dict setObject:@"7.382095" forKey:@"latitude"];
    [Ruter_Dict setObject:@"70.486221" forKey:@"logitude"];
    [Ruter_Dict setObject:@"7" forKey:@"RUTer_Id"];
    [Ruter_Dict setObject:@"declain" forKey:@"ruter_status"];
    return Ruter_Dict;
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Receving GroupInvitation
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)xmppMUC:(XMPPMUC *)sender roomJID:(XMPPJID *) roomJID didReceiveInvitation:(XMPPMessage *)message
{
    XMPPRoomCoreDataStorage  *xmppRoomStorage = [XMPPRoomCoreDataStorage sharedInstance];
    XMPPRoom *newXmppRoom = [[XMPPRoom alloc] initWithRoomStorage:xmppRoomStorage jid:roomJID dispatchQueue:dispatch_get_main_queue()];
    [newXmppRoom activate: xmppStream];
    [newXmppRoom fetchConfigurationForm];
    [newXmppRoom addDelegate:self delegateQueue:dispatch_get_main_queue()];
     NSXMLElement *history = [NSXMLElement elementWithName:@"history"];
    [history addAttributeWithName:@"seconds" stringValue:@"10"];
    [newXmppRoom joinRoomUsingNickname:xmppStream.myJID.user history:history];
}
- (void)xmppMUC:(XMPPMUC *)sender roomJID:(XMPPJID *) roomJID didReceiveInvitationDecline:(XMPPMessage *)message
{
    
}


- (void)xmppRoomDidJoin:(XMPPRoom *)sender
{
   // DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
 //   NSLog(@"ROOM DID JOIN");
    [sender configureRoomUsingOptions:nil];
    [self sendCongfirurationForm:sender];
    XMPPMessage *groupJoinMessage = [[XMPPMessage alloc]init];
    [groupJoinMessage addAttributeWithName:@"id" stringValue:[NSString stringWithFormat:@"%@",[[self xmppStream] generateUUID]]];
    [groupJoinMessage addAttributeWithName:@"to" stringValue:sender.roomJID.bare];
    
    [groupJoinMessage addAttributeWithName:@"type" stringValue:@"groupchat"];
    NSXMLElement *groupJoinLeave = [NSXMLElement elementWithName:@"group"];
    [groupJoinLeave addAttributeWithName:@"xmlns" stringValue:[NSString stringWithFormat:@"%@:group",HOSTNAME]];
    [groupJoinLeave addAttributeWithName:@"type" stringValue:@"1"];
    [groupJoinMessage addChild:groupJoinLeave];
    NSXMLElement *bodyMessage = [NSXMLElement elementWithName:@"body" stringValue:@"joined"];
    [groupJoinMessage addChild:bodyMessage];
    [[self xmppStream] sendElement:groupJoinMessage];
    
    
}
-(void)sendCongfirurationForm:(XMPPRoom*)sender
{
    NSXMLElement *query = [[NSXMLElement alloc] initWithXMLString:@"<query xmlns='http://jabber.org/protocol/muc#owner'/>"
                                                            error:nil];
    NSXMLElement *x = [NSXMLElement elementWithName:@"x"
                                              xmlns:@"jabber:x:data"];
    [x addAttributeWithName:@"type" stringValue:@"submit"];
    NSXMLElement *field1 = [NSXMLElement elementWithName:@"field"];
    [field1 addAttributeWithName:@"var" stringValue:@"FORM_TYPE"];
    NSXMLElement *value1 = [NSXMLElement elementWithName:@"value"
                                             stringValue:@"http://jabber.org/protocol/muc#roomconfig"];
    [field1 addChild:value1];
    
    NSXMLElement *field2 = [NSXMLElement elementWithName:@"field"];
    [field2 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_roomname"];
    NSXMLElement *value2 = [NSXMLElement elementWithName:@"value" stringValue:sender.roomJID.user];
    [field2 addChild:value2];
    
    NSXMLElement *field3 = [NSXMLElement elementWithName:@"field"];
    [field3 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_roomdesc"];
    NSXMLElement *value3 = [NSXMLElement elementWithName:@"value" stringValue:@"new"];
    [field3 addChild:value3];
    
    NSXMLElement *field35 = [NSXMLElement elementWithName:@"field"];
    [field35 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_enablelogging'"];
    NSXMLElement *value35 = [NSXMLElement elementWithName:@"value" stringValue:@"1"];
    [field35 addChild:value35];
    
    
    NSXMLElement *field4 = [NSXMLElement elementWithName:@"field"];
    [field4 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_changesubject"];
    NSXMLElement *value4 = [NSXMLElement elementWithName:@"value" stringValue:@"1"];
    [field4 addChild:value4];
    
    NSXMLElement *field5 = [NSXMLElement elementWithName:@"field"];
    [field5 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_allowinvites"];
    NSXMLElement *value5 = [NSXMLElement elementWithName:@"value" stringValue:@"1"];
    [field5 addChild:value5];
    
    NSXMLElement *field6 = [NSXMLElement elementWithName:@"field"];
    [field6 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_allowpm"];
    NSXMLElement *value6 = [NSXMLElement elementWithName:@"value" stringValue:@"anyone"];
    [field6 addChild:value6];
    
    NSXMLElement *field7 = [NSXMLElement elementWithName:@"field"];
    [field7 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_maxusers"];
    NSXMLElement *value7 = [NSXMLElement elementWithName:@"value" stringValue:@"50"];
    [field7 addChild:value7];
    
    
    NSXMLElement *field8 = [NSXMLElement elementWithName:@"field"];
    [field8 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_publicroom"];
    NSXMLElement *value8 = [NSXMLElement elementWithName:@"value" stringValue:@"1"];
    [field8 addChild:value8];
    
    NSXMLElement *field9 = [NSXMLElement elementWithName:@"field"];
    [field9 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_persistentroom"];
    NSXMLElement *value9 = [NSXMLElement elementWithName:@"value" stringValue:@"1"];
    [field9 addChild:value9];
    
    
    NSXMLElement *field10 = [NSXMLElement elementWithName:@"field"];
    [field10 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_moderatedroom"];
    NSXMLElement *value10 = [NSXMLElement elementWithName:@"value" stringValue:@"1"];
    [field10 addChild:value10];
    
    
    NSXMLElement *field11 = [NSXMLElement elementWithName:@"field"];
    [field11 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_membersonly"];
    NSXMLElement *value11 = [NSXMLElement elementWithName:@"value" stringValue:@"1"];
    [field11 addChild:value11];
    
    NSXMLElement *field12 = [NSXMLElement elementWithName:@"field"];
    [field12 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_passwordprotectedroom"];
    NSXMLElement *value12 = [NSXMLElement elementWithName:@"value" stringValue:@"0"];
    [field12 addChild:value12];
    
    
    NSXMLElement *field13 = [NSXMLElement elementWithName:@"field"];
    [field13 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_roomsecret"];
    NSXMLElement *value13 = [NSXMLElement elementWithName:@"value" stringValue:sender.roomJID.user];
    [field13 addChild:value13];
    
    
    NSXMLElement *field14 = [NSXMLElement elementWithName:@"field"];
    [field14 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_whois"];
    NSXMLElement *value14 = [NSXMLElement elementWithName:@"value" stringValue:@"anyone"];
    [field14 addChild:value14];
    
    NSXMLElement *field15 = [NSXMLElement elementWithName:@"field"];
    [field15 addAttributeWithName:@"var" stringValue:@"muc#maxhistoryfetch"];
    NSXMLElement *value15 = [NSXMLElement elementWithName:@"value" stringValue:@"10"];
    [field15 addChild:value15];
    
    NSXMLElement *field16 = [NSXMLElement elementWithName:@"field"];
    [field16 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_roomadmins"];
    NSXMLElement *value16 = [NSXMLElement elementWithName:@"value" stringValue:[self xmppStream].myJID.bare];
    [field16 addChild:value16];
    
    NSXMLElement *field17 = [NSXMLElement elementWithName:@"field"];
    [field17 addAttributeWithName:@"var" stringValue:@"x-muc#roomconfig_registration"];
    NSXMLElement *value17 = [NSXMLElement elementWithName:@"value" stringValue:@"0"];
    [field17 addChild:value17];
    
    
    NSXMLElement *field18 = [NSXMLElement elementWithName:@"field"];
    [field18 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_presencebroadcast"];
    NSXMLElement *value181 = [NSXMLElement elementWithName:@"value" stringValue:@"moderator"];
    NSXMLElement *value182 = [NSXMLElement elementWithName:@"value" stringValue:@"participant"];
    NSXMLElement *value183 = [NSXMLElement elementWithName:@"value" stringValue:@"visitor"];
    
    [field18 addChild:value181];
    [field18 addChild:value182];
    [field18 addChild:value183];
    
    
    NSXMLElement *field19 = [NSXMLElement elementWithName:@"field"];
    [field19 addAttributeWithName:@"var" stringValue:@"x-muc#roomconfig_reservednick"];
    NSXMLElement *value19 = [NSXMLElement elementWithName:@"value" stringValue:@"1"];
    [field19 addChild:value19];
    
    [x addChild:field1];
    [x addChild:field2];
    [x addChild:field3];
    [x addChild:field35];
    [x addChild:field4];
    [x addChild:field5];
    [x addChild:field6];
    [x addChild:field7];
    [x addChild:field8];
    [x addChild:field9];
    [x addChild:field10];
    [x addChild:field11];
    [x addChild:field12];
    [x addChild:field13];
    [x addChild:field14];
    [x addChild:field15];
    [x addChild:field16];
    [x addChild:field17];
    [x addChild:field18];
    [query addChild:x];
    
    NSXMLElement *roomOptions = [NSXMLElement elementWithName:@"iq"];
    [roomOptions addAttributeWithName:@"from" stringValue:[NSString stringWithFormat:@"%@",[XMPPJID jidWithString:[[self xmppStream] myJID].user resource:[[self xmppStream] myJID].resource]]];
    [roomOptions addAttributeWithName:@"type" stringValue:@"set"];
    [roomOptions addAttributeWithName:@"id" stringValue:[[self xmppStream] generateUUID]];
    [roomOptions addAttributeWithName:@"to" stringValue:sender.roomJID.bare];
    [roomOptions addChild:query];
    [[self xmppStream] sendElement:roomOptions];
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark DataBase Methods For Recent Chats
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (XMPPUserCoreDataStorageObject *)RecentcontactsFetch:(NSString *)Str
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:[self managedObjectContext_roster]];
    [request setEntity:entity];
    NSString *predicateFrmt = @"jidStr = %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, Str];
    [request setPredicate:predicate];
    NSArray *empArray=[[self managedObjectContext_roster] executeFetchRequest:request error:nil];
    if ([empArray count] == 1)
    {
        for (XMPPUserCoreDataStorageObject *user in  empArray)
        {
            return user;
        }
    }
    return nil;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}




@end
