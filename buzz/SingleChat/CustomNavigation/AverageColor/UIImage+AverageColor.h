//
//  UIImage+AverageColor.h
//  buzz
//
//  Created by Shriram Rajendran on 13/07/15.
//  Copyright (c) 2015 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (AverageColor)

+ (UIColor*) getDominantColor:(UIImage*)image;

@end
