//
//  chatViewController.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 22/02/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#define HEIGHT_REDUCE 0

#import <AVFoundation/AVFoundation.h>
#import "chatViewController.h"
#import "AppDelegate.h"
#import "AvatarImage.h"
#import "UpdateDataBase.h"
#import "MessageMethods.h"
#import "XMPPMessage+XEP_0184.h"
#import "DBEditer.h"
#import "RUTerViewController.h"
#import "LocationManagerSingleton.h"
#import "ChatSildeMenuView.h"
#import "SOSMapVwCtrl.h"
#import "BeeCollectionView.h"
#import "mediaDbEditor.h"
#import "RMDownloadIndicator.h"
#import "ViewController.h"
#import "PasswordPin.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "notiVw.h"
#import "menuView.h"
#import "ImpMsgPopUp.h"
#import "UIImage+Resize.h"
#import "AFHTTPClient.h"
#import "AFJSONRequestOperation.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "ProfileViewController.h"
#import "privateChatDbEditer.h"
#import "private_timer.h"
#import "XMPPLastActivity.h"
#import "M13ContextMenu.h"
#import "M13ContextMenuItemIOS7.h"
#import "QBPopupMenu.h"
#import "UpdateDataBase.h"
#import "bubbleVw.h"
#import "imagePoper.h"
#import "FetchsharedFile.h"
#import "DetailGalleryViewController.h"
#import "editImageView.h"
#import <Accelerate/Accelerate.h>
#import "UIImageEffects.h"
#import "UIColor+HexColors.h"
#import "UIColor+RandomColor.h"



#define kMaxIdleTimeSeconds 5.0
#define widthOfBlackTransVw 60

@interface chatViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,Delegate_RUTer,chatSlideViewDelegate,BeeCollectionViewdelegate,UITableViewDataSource,UITableViewDelegate,M13ContextMenuDelegate,privateTimerDelegate,bubbleViewDelegate,Delegate_DBEditer>
{
    UIView *inputView;
    UIView *presenceView;
    UIButton *sendBtn,*simleyBtn;
    AvatarImage *avatarImg;
    UpdateDataBase *updateUnreadMsg;
    MessageMethods *messageMethods;
    ImpMsgPopUp *popUp;
    editImageView *imgEdit;
    UIButton *backButton,*menuButton;
    UIButton *ruterBtn;
    BOOL checkComposing;
    DBEditer* DBEditer_obj;
    UIImage *userProfileImage;
    UIImage *myProfileImage;
    UISwipeGestureRecognizer *swipeLeftGesture;
    UISwipeGestureRecognizer *swipeRightGesture;
    UISwipeGestureRecognizer *slideswipegesture;
    BOOL isMenuShowing;
    
    UITableView* chatTableView;
    
    NSArray* beeArray ;
    NSArray* beeFullArray;
    UIView* Bee_Board;
    UIView* BeeUp_Board;
    UIImageView* DragImgVw,*profileImgNav;
    BeeCollectionView* beeCollVw;
    BOOL canExecuteViewWillAppear;
    BOOL isCamOpened;
    BOOL isGalleryOpen;
    BOOL isvideoPlayed;
    BOOL isPrivateChat;
    BOOL showImpMsgLy;
    BOOL isImageEditorOpend;
    BOOL ifShowingStickers;
    BOOL isFetching;

    M13ContextMenu *menu;
    UILongPressGestureRecognizer *longPress;
    
    NSMutableArray * photoViewerAry;
    
    UIView* privateVw;
    NSString* EditMessageId;
    BOOL shouldScrollToBottom;
    BOOL isPrivateHistoryVw;
    BOOL isEditMode;
    BOOL isImpOn;
    PasswordPin* passwordVw;
    
    NSMutableArray* deleteMsgIdAry;
    UIView* editInputView;
    UIButton* deleteDoneBtn;
    UIButton* impBtn,*cameraBtn,*galleryBtn;

    bubbleVw * bubble;
    menuView* menuVw;
    
    XMPPLastActivity *lastActivity;
    
    UIViewController *imagePickerController;
    UIButton *imagePickerBackButton;
    XMPPResourceCoreDataStorageObject *useResource;
    NSTimer *composingTimer;
    
    UILabel *presenceLabel;
    UILabel *statusLabel;
    UILabel *iconlabel;

    
    UISwitch *switchButton;

}


@property(nonatomic,retain)UIBubbleTableView *bubbleTable;
@property(nonatomic,retain)NSMutableArray *messageData;
@property(nonatomic,retain)HPGrowingTextView *chatTextview;
@property(nonatomic,retain)UIButton *titleLabel;
@property(nonatomic,strong)ChatSildeMenuView *chatSlideMenu;
@property (nonatomic, strong) UILabel *textLabel;
@property (nonatomic, strong) NSOperationQueue *imageDownloadingQueue;
@property (nonatomic, strong) NSCache *imageCache;
@property (nonatomic, strong) QBPopupMenu *popupMenu_txt;
@property (nonatomic, strong) QBPopupMenu *popupMenu_media;
@property (nonatomic, strong) QBPopupMenu *popupMenu_sticker;
@property(nonatomic) CGFloat lastContentOffset;



@end

@implementation chatViewController
@synthesize user;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Appdelegate Values
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (AppDelegate *)appDelegate
{
	return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (XMPPStream *)xmppStream
{
	return [[self appDelegate] xmppStream];
}
-(XMPPMessageArchivingCoreDataStorage *)xmppMessageArchivingCoreDataStorage
{
    
    return [[self appDelegate] xmppMessageArchivingStorage];
}

- (NSManagedObjectContext *)managedObjecMessageContact
{
	return [[[self appDelegate] xmppMessageArchivingStorage] mainThreadManagedObjectContext];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark View Life Cycle
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    self.view.userInteractionEnabled = YES;
    //self.view.backgroundColor = CHAT_BACKGROUNDIMAGE;
    
    if(IS_IPHONE_4)
    {
        self.view.backgroundColor = BACKGROUNDIMAGE_4;
        
    }
    else
    {
        self.view.backgroundColor = BACKGROUNDIMAGE;
        
    }
    
    
    
    
    self.navigationItem.hidesBackButton = YES;
    self.titleLabel = [UIButton buttonWithType:UIButtonTypeCustom];
    chatTableView = [UIBubbleTableView new];
    self.messageData = [NSMutableArray new];
    inputView = [UIView new];
    self.chatTextview = [HPGrowingTextView new];
    avatarImg = [AvatarImage new];
    updateUnreadMsg = [UpdateDataBase new];
    self.photos = [NSMutableArray new];
    messageMethods = [MessageMethods new];
    presenceView = [UIView new];
    presenceLabel = [UILabel new];
    statusLabel = [UILabel new];
    iconlabel = [UILabel new];
    
    
    
    

    
    UIImage *impImageIcon1 = [UIImage imageNamed:@"impMsgTv1"];
    UIImage *impImageIcon2 = [UIImage imageNamed:@"impMsgTV2"];
    
    


    impBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    impBtn.backgroundColor = [UIColor clearColor];
    [impBtn setFrame:CGRectMake(0.0f, 0.0f, impImageIcon1.size.width, impImageIcon1.size.height)];
    [impBtn setImage:impImageIcon1 forState:UIControlStateNormal];
    [impBtn setImage:impImageIcon2 forState:UIControlStateHighlighted];
//    impBtn.imageEdgeInsets = UIEdgeInsetsMake( 5.0, 5.0, 5.0, 5.0);
    [impBtn addTarget:self action:@selector(impButtonAction:) forControlEvents:UIControlEventTouchUpInside];


    
    
    galleryBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    galleryBtn.backgroundColor = [UIColor clearColor];
    [galleryBtn setFrame:CGRectMake(0.0f, 0.0f, 40, 40)];
    [galleryBtn setImage:[UIImage imageNamed:@"galleryO"] forState:UIControlStateNormal];
//    galleryBtn.imageEdgeInsets = UIEdgeInsetsMake( 5.0, 5.0, 5.0, 5.0);
    [galleryBtn addTarget:self action:@selector(galleryButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
      
    

    cameraBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cameraBtn.backgroundColor = [UIColor clearColor];
    [cameraBtn setFrame:CGRectMake(0.0f, 0.0f, 40, 40)];
    [cameraBtn setImage:[UIImage imageNamed:@"cameraTool"] forState:UIControlStateNormal];
//   cameraBtn.imageEdgeInsets = UIEdgeInsetsMake( 5.0, 5.0, 5.0, 5.0);
    [cameraBtn addTarget:self action:@selector(cameraButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    //gallery
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithCustomView:impBtn],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithCustomView:cameraBtn],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithCustomView:galleryBtn],

                           nil];
    [numberToolbar sizeToFit];
   _chatTextview.accessoryView = numberToolbar;
    
    
    
    self.singleton = [Singleton sharedMySingleton];
    // Keyboard events
    
    
    DBEditer_obj = [DBEditer new];
    LocationManagerSingleton* locMang = [LocationManagerSingleton sharedSingleton];
    locMang.delegate_ruter = self;
    
    if ([[DBEditer_obj FetchActive_RUTER_ID] count] > 0)
    {
        
        [locMang startUpdatingCurrentLocation];
    }
    swipeLeftGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGestureAction:)];
    slideswipegesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGestureAction:)];
    swipeRightGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGestureAction:)];
    
    
    UIImage *backButtonImage = [UIImage imageNamed:@"back_btn.png"];
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 0, backButtonImage.size.width, backButtonImage.size.height);
    [backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    deleteDoneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    deleteDoneBtn.frame = CGRectMake(self.view.frame.size.width-90, 10, 80, 30);
    [deleteDoneBtn setTitle:@"Done" forState:UIControlStateNormal];
    [deleteDoneBtn setTintColor:[UIColor whiteColor]];
    [deleteDoneBtn addTarget:self action:@selector(changeViewToNormalMode) forControlEvents:UIControlEventTouchUpInside];
    deleteDoneBtn.backgroundColor = RGBA(46, 141, 214, 1);
    deleteDoneBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    deleteDoneBtn.layer.cornerRadius = 0.5;
    deleteDoneBtn.hidden = YES;
    
    
    self.titleLabel.frame = CGRectMake(backButton.frame.origin.x + backButton.frame.size.width + 44, 10, self.view.frame.size.width - (backButton.frame.origin.x + backButton.frame.size.width + 100) , 30);
    self.titleLabel.backgroundColor = [UIColor clearColor];
    [self.titleLabel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.titleLabel addTarget:self action:@selector(pushToProfileView) forControlEvents:UIControlEventTouchUpInside];
    self.titleLabel.titleLabel.font = [UIFont fontWithName:FontString size:18];
    
    
    UIImage *menuImageIcon = [UIImage imageNamed:@"menu-icon"];

    profileImgNav = [UIImageView new];
//    profileImgNav.frame = CGRectMake(backButton.frame.origin.x + backButton.frame.size.width , self.navigationController.navigationBar.frame.size.height - 22, 44 , 44);
     profileImgNav.frame = CGRectMake(backButton.frame.origin.x + backButton.frame.size.width , 3, 40 , 40);
    profileImgNav.backgroundColor = [UIColor clearColor];
    [profileImgNav.layer setCornerRadius:20];
    [profileImgNav setClipsToBounds:YES];
    profileImgNav.layer.borderWidth = 2.0;
    profileImgNav.layer.borderColor = [UIColor whiteColor].CGColor;

    
    presenceLabel.backgroundColor = [UIColor clearColor];
    [presenceLabel setTextColor:[UIColor darkGrayColor]];
    presenceLabel.textAlignment = NSTextAlignmentLeft;
    presenceLabel.adjustsFontSizeToFitWidth = YES;
    presenceLabel.font = [UIFont fontWithName:FontString size:9];

    statusLabel.backgroundColor = [UIColor clearColor];
    [statusLabel setTextColor:[UIColor whiteColor]];
    statusLabel.textAlignment = NSTextAlignmentLeft;
    statusLabel.adjustsFontSizeToFitWidth = YES;
    statusLabel.font = [UIFont fontWithName:FontString size:9];

    menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    menuButton.backgroundColor = [UIColor clearColor];
    menuButton.frame = CGRectMake(self.view.frame.size.width-60, 0, 60, 40);
    [menuButton setImage:menuImageIcon forState:UIControlStateNormal];
    menuButton.imageEdgeInsets = UIEdgeInsetsMake( 10.0, 5.0, 0.0, 0.0);
    [menuButton addTarget:self action:@selector(menuButtonAction:) forControlEvents:UIControlEventTouchDown];

   
  
    iconlabel.frame = CGRectMake(profileImgNav.frame.origin.x + profileImgNav.frame.size.width -12 ,profileImgNav.frame.origin.y + profileImgNav.frame.size.height - 10, 10 , 10);
    iconlabel.layer.borderWidth = 2.0;
    iconlabel.layer.borderColor = [UIColor whiteColor].CGColor;
    iconlabel.layer.cornerRadius = 13/2;
    iconlabel.clipsToBounds = YES;
    iconlabel.hidden = NO;
    

    
    chatTableView = [UITableView new];
    chatTableView.delegate = self;
    chatTableView.dataSource = self;
    chatTableView.backgroundColor = [UIColor clearColor];
    chatTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    chatTableView.showsVerticalScrollIndicator = NO;
    chatTableView.showsVerticalScrollIndicator = NO;
    chatTableView.clipsToBounds = NO;
    [self.view addSubview:chatTableView];
    
    
    chatTableView.contentInset = UIEdgeInsetsMake(- 62 , 0, 62, 0);

    
    
    self.chatSlideMenu = [[ChatSildeMenuView alloc]initWithFrame:CGRectMake(self.view.frame.size.width+100,self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height, 100, self.view.frame.size.height-40-(self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height))];
    
    self.chatSlideMenu.delegte = self;
    self.chatSlideMenu.menuForGroup = NO;
    [_chatSlideMenu addMenuView];


    
    inputView.backgroundColor = [UIColor clearColor];
    inputView.userInteractionEnabled = YES;
    [self.view addSubview:inputView];
    
    [self.view addSubview:self.chatSlideMenu];
    
    UIImage *smileyImage = [UIImage imageNamed:@"smiley"];
    simleyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    simleyBtn.backgroundColor = [UIColor clearColor];
    [simleyBtn setImage:smileyImage forState:UIControlStateNormal];
   // simleyBtn.imageEdgeInsets = UIEdgeInsetsMake( 7.0, 5.0, 0.0, 0.0);
	[simleyBtn addTarget:self action:@selector(beeBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    self.chatTextview.minNumberOfLines = 1;
	self.chatTextview.maxNumberOfLines = 3;
  	self.chatTextview.returnKeyType = UIKeyboardTypeDefault; //just as an example
	self.chatTextview.delegate = self;
    self.chatTextview.backgroundColor = [UIColor whiteColor];
    self.chatTextview.placeholder = @"Buzz here..";
	self.chatTextview.animateHeightChange = YES;
    self.chatTextview.layer.cornerRadius = 0.5f ;
    self.chatTextview.clipsToBounds = YES;
    [self.chatTextview setTextColor:[UIColor blackColor]];
    
   
    
    sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sendBtn.backgroundColor = [UIColor clearColor];
	[sendBtn addTarget:self action:@selector(SendButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [sendBtn setImage:[UIImage imageNamed:@"send_button"] forState:UIControlStateNormal];
    
 
    
    
    beeArray = [NSArray arrayWithObjects:@"sticker_001",@"sticker_002",@"sticker_003",@"sticker_004",@"sticker_005", nil];
    beeFullArray = [NSArray arrayWithObjects:@"sticker_01",@"sticker_02",@"sticker_03",@"sticker_04",@"sticker_05",nil];

    
    
    
//    beeArray = [NSArray arrayWithObjects:@"sticker_001",@"sticker_002",@"sticker_003",@"sticker_004",@"sticker_005",nil];
//    beeFullArray = [NSArray arrayWithObjects:@"Pow!",@"Heart",@"Awesome",@"Grrrr!",@"Yay!",nil];
    Bee_Board = [UIView new];
    BeeUp_Board= [UIView new];
    
    canExecuteViewWillAppear = YES;
    isCamOpened = NO;
    isGalleryOpen = NO;
    isvideoPlayed = NO;
    
    UITapGestureRecognizer *swipeGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedCell:)];
    [chatTableView addGestureRecognizer:swipeGesture];
    
    
    self.imageDownloadingQueue = [[NSOperationQueue alloc] init];
    self.imageDownloadingQueue.maxConcurrentOperationCount = 4; // many servers limit how many concurrent requests they'll accept from a device, so make sure to set this accordingly
    
    self.imageCache = [[NSCache alloc] init];
    [[self xmppStream] addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    photoViewerAry = [NSMutableArray new];
    isPrivateChat = NO;
    
    isPrivateHistoryVw = NO;
    isEditMode = NO;
    
    _photosDict = [NSMutableDictionary new];
    
    inputView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.size.height - 40, self.view.frame.size.width, 40);
    
    
    
   
    self.chatTextview.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
    self.chatTextview.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    

    
    UIImage *rawEntryBackground = [UIImage imageNamed:@"chatborder"];
    UIImage *entryBackground = [rawEntryBackground stretchableImageWithLeftCapWidth:80 topCapHeight:98-20];
    UIImageView *entryImageView = [[UIImageView alloc] initWithImage:entryBackground];
    entryImageView.frame = CGRectMake(0, -98+inputView.frame.size.height, inputView.frame.size.width, 98);
    entryImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    

    
    [inputView addSubview:entryImageView];
    
    [inputView addSubview:self.chatTextview];

    [inputView addSubview:sendBtn];
	[inputView addSubview:simleyBtn];

    
   // [inputView addSubview:impBtn];
    
    
   


    
    
    
    lastActivity = [[XMPPLastActivity alloc]initWithDispatchQueue:dispatch_get_main_queue()];
    [lastActivity activate:[self xmppStream]];
    [lastActivity addDelegate:self delegateQueue:dispatch_get_main_queue()];

    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[self appDelegate].statusBarView setHidden:NO];
    self.navigationController.navigationBarHidden = NO;
    



    
    chatTableView.frame = CGRectMake(self.view.frame.origin.x,self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height, self.view.frame.size.width, self.view.frame.size.height- (40 + self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height));

    
    NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@_BG",user.jidStr]];
    if (imageData)
    {
        UIImage* imageScaled = [UIImage imageWithImage:[UIImage imageWithData:imageData] scaledToSize:self.view.frame.size];

       self.view.backgroundColor = [UIColor colorWithPatternImage:imageScaled];
    }
    else
    {
       // self.view.backgroundColor = CHAT_BACKGROUNDIMAGE;
        
        if(IS_IPHONE_4)
        {
            self.view.backgroundColor = BACKGROUNDIMAGE_4;
            
        }
        else
        {
            self.view.backgroundColor = BACKGROUNDIMAGE;
            
        }
        
        

    }
    
    
    showImpMsgLy = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];

    
    if(!isPrivateChat)
    {
        
    [[self xmppMessageArchivingCoreDataStorage] updateUnreadMessage:self.user.jid clearValue:YES];
    
    }
    
    [self createPopUpforText];
    [self createPopUpforMedia];
    [self createPopUpforSticker];
    
    deleteMsgIdAry = [NSMutableArray new];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(timerDead:) name:@"privateTimer" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(privatePassword:) name:@"privatePassword" object:nil];
    
    isImpOn = NO;
    
    UIImage *impImageIcon1 = [UIImage imageNamed:@"impMsgTv1"];
    UIImage *impImageIcon2 = [UIImage imageNamed:@"impMsgTV2"];

    
    if (isImpOn)
    {
        [impBtn setImage:impImageIcon2 forState:UIControlStateNormal];
    }
    else
    {
        [impBtn setImage:impImageIcon1 forState:UIControlStateNormal];
        
    }

    
    if (isCamOpened || isvideoPlayed)
    {
        isCamOpened = NO;
        isvideoPlayed = NO;
    }
    else if (isGalleryOpen)
    {
        isGalleryOpen = NO;
    }
    
    else if ([UIApplication sharedApplication].statusBarHidden ==  YES) {
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
        self.view.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height-20);
    }
    
    [self.navigationController.navigationBar addSubview:profileImgNav];
    [self.navigationController.navigationBar addSubview:deleteDoneBtn];
    [self.navigationController.navigationBar addSubview:backButton];
    [self.navigationController.navigationBar addSubview:self.titleLabel];
    [self.navigationController.navigationBar addSubview:iconlabel];

   
    [self.titleLabel setTitle:user.displayName forState:UIControlStateNormal];
    
    isMenuShowing = NO;
    isFetching = NO;

    userProfileImage =  [avatarImg profileImage:user];
    myProfileImage = [avatarImg myProfileImage];
    
 


    [profileImgNav setImage:userProfileImage];
    
    if (canExecuteViewWillAppear) {
        canExecuteViewWillAppear = NO;
        [chatTableView reloadData];
    }
    

    [self createBeeBoard:beeArray];
    
    self.singleton.chatScreenUser = user.jidStr;
    [self createM13ContectMenu];
    
    [self createPrivateChatView];
    [self editInputVw_Create];
    
    int textSizeValue;
    NSInteger textsize = [[NSUserDefaults standardUserDefaults] integerForKey:@"textSize"];
    if (textsize == 0)
        textSizeValue = 15;
    else if (textsize == 1)
        textSizeValue = 17;
    else
        textSizeValue = 19;
	self.chatTextview.font = [UIFont fontWithName:FontString size:textSizeValue];
    
    UIImage *smileyImage = [UIImage imageNamed:@"smiley"];

    simleyBtn.frame =  CGRectMake(2, (inputView.frame.size.height/2) - (smileyImage.size.height/2),smileyImage.size.width,smileyImage.size.height);
     self.chatTextview.frame = CGRectMake(simleyBtn.frame.origin.x + simleyBtn.frame.size.width + 4, 1, self.view.frame.size.width + 15 - (simleyBtn.frame.size.width + 60 + 10), inputView.frame.size.height - 5);
    sendBtn.frame = CGRectMake(self.chatTextview.frame.origin.x + self.chatTextview.frame.size.width -2 ,inputView.frame.size.height - 35 , 57, 30);
    
    //[impBtn setBackgroundColor:[UIColor greenColor]];
   // UIImage *impImageIcon1 = [UIImage imageNamed:@"impMsgTv1"];

    
  //  impBtn.frame = CGRectMake(_chatTextview.frame.size.width - ((impImageIcon1.size.width / 2)-12), -5, impImageIcon1.size.width, impImageIcon1.size.height);
    switchButton.userInteractionEnabled = YES;

    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:TURN_BUBBLE_MENU] == YES)
    {
        
        [self createBubbleMenu];
        menuVw = [menuView sharedInstance:[NSArray arrayWithObjects:@"privatechat_w.png",@"gallery_w.png",@"photo_w.png",@"video_w.png",@"ruthere_w.png",nil] btnTitleAry:[NSArray arrayWithObjects:@"Clear chat",@"Send File",@"Photo",@"Video",@"Are you there?",nil] screeenSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)];
        [self.view addSubview:menuVw];
        menuVw.hidden = YES;
        menuVw.delegte = self;
      
        
        
        [chatTableView removeGestureRecognizer:swipeRightGesture];
        [chatTableView removeGestureRecognizer:swipeLeftGesture];
        [self.chatSlideMenu removeGestureRecognizer:slideswipegesture];

    }
    else
    {
        [self.navigationController.navigationBar addSubview:menuButton];
        [swipeRightGesture setDirection:UISwipeGestureRecognizerDirectionRight];
        [chatTableView addGestureRecognizer:swipeRightGesture];
        [swipeLeftGesture setDirection:UISwipeGestureRecognizerDirectionLeft];
        [chatTableView addGestureRecognizer:swipeLeftGesture];
        [self.chatSlideMenu addGestureRecognizer:slideswipegesture];
    }
    
    //show present user's presence
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:ONLINE_OFFLINE_VISIBILITY] == YES)
    {
    
    if ([user isOnline])
    {
        for (useResource in user.resources)
        {
            if ([useResource.jid.bareJID isEqualToJID:user.jid.bareJID])
            {
                //statusLabel.text = useResource.status;
                if([useResource.type isEqualToString:@"available"])
                {
                     iconlabel.backgroundColor = [UIColor greenColor];
                     presenceLabel.text = @"Online";
                    statusLabel.text = @"Status:";
                }
                else if([useResource.type isEqualToString:@"unavailable"])
                {
                    iconlabel.backgroundColor = [UIColor yellowColor];
                    presenceLabel.text = @"Away";
                    [lastActivity sendLastActivityQueryToJID:user.jid];
                }
            }
        }
    }
    else
    {
        iconlabel.backgroundColor = [UIColor redColor];
        statusLabel.text = @"Status";
        presenceLabel.text = @"Unavailable";
        [lastActivity sendLastActivityQueryToJID:user.jid];
        
    }
    
    presenceView.frame = CGRectMake(self.view.frame.size.width - 100 , 0 , 100, 40);
    presenceView.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.9];
    presenceView.layer.cornerRadius = 3.0;
    [inputView addSubview:presenceView];
    
    statusLabel.frame = CGRectMake(0 , 0, 100, 15);
    statusLabel.backgroundColor = [UIColor lightGrayColor];
    statusLabel.layer.cornerRadius = 3.0;
    statusLabel.clipsToBounds = YES;
    [presenceView addSubview:statusLabel];
    
    presenceLabel.frame = CGRectMake(12 , statusLabel.frame.size.height, 100, 25);
    [presenceView addSubview:presenceLabel];
    
    
    

    
    [presenceView setAlpha:0.0f];
    
    }
    
  


    
    int unreadMsgCount = [[[NSUserDefaults standardUserDefaults]valueForKey:[NSString stringWithFormat:@"%@%@",KEY_UNREAD,user.jidStr]]intValue];
   
   // NSLog(@"UnreadCount of %@ --> %d", user.jidStr,unreadMsgCount);
    
    if (unreadMsgCount == 0)
    {
        [self scrollBubbleViewToBottomAnimated:YES];
        
    }
    else
    {
        [self scrollToFirstUnreadMsgPosition:YES];
    }
    
    if (!isPrivateChat)
    {
        [[self xmppMessageArchivingCoreDataStorage]updateUnreadMessage:self.user.jid clearValue:YES];

    }

    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
   
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:ONLINE_OFFLINE_VISIBILITY] == YES)
    {
        [self popAnimatedforPresenceView:YES];
    }

    
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    
    //simleyBtn.frame =  CGRectMake(0, inputView.frame.size.height/2 - smileyImage.size.height/2,smileyImage.size.width,smileyImage.size.height);
    //sendBtn.frame = CGRectMake(self.chatTextview.frame.origin.x+self.chatTextview.frame.size.width + 4 ,inputView.frame.size.height - 35 , 57, 30);
    
}

- (UIImage *)tintImage:(UIImage *)image withColor:(UIColor *)color
{
    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, image.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    CGContextClipToMask(context, rect, image.CGImage);
    [color setFill];
    CGContextFillRect(context, rect);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark BEE Life Cycle
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)createBeeBoard:(NSArray*)beeAry
{
    beeCollVw = nil;
    beeCollVw = [[BeeCollectionView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 64)imgAry:beeAry];
    beeCollVw.BeeCollectionDelegate  =self;
    beeCollVw.userInteractionEnabled = YES;
    UIImage *tintImg = [self tintImage:[UIImage imageNamed:@"popBg"] withColor:[UIColor whiteColor]];
   
    beeCollVw.backgroundColor = [UIColor clearColor];
    beeCollVw.bgIMgVw.image = tintImg;
    [beeCollVw setAlpha:0.0f];
    [self.view addSubview:beeCollVw];
    
}
-(void)beeBtnClicked
{

    [UIView animateWithDuration:0.2
                          delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         if (beeCollVw.frame.origin.y > self.view.frame.size.height- 64) {
                             
                             [beeCollVw setAlpha:1.0f];
                             //                             beeCollVw.frame = CGRectMake(5, - beeCollVw.frame.size.height, self.view.frame.size.width - 10, beeCollVw.frame.size.height);
                             beeCollVw.frame = CGRectMake(0,  inputView.frame.origin.y - beeCollVw.frame.size.height , self.view.frame.size.width, beeCollVw.frame.size.height);
                         }
                         else
                         {
                             [beeCollVw setAlpha:0.0f];

                             beeCollVw.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, beeCollVw.frame.size.height);
                             
                         }
                         
                     }
                     completion:^(BOOL finished){
                         
                     }];
}

-(void)closeBee_Board
{
    
        [UIView animateWithDuration:0.2
                              delay:0
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                                                    
                             [beeCollVw setAlpha:0.0f];
                             beeCollVw.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, beeCollVw.frame.size.height);
                             
                         }
                         completion:^(BOOL finished){
                             
                         }];
    
}






-(void)tapGestureState_xpoint:(int)xpoint ypoint:(int)ypoint index:(long)index
{
    [self closeBee_Board];

    [self SendBeeMsg:[NSString stringWithFormat:@"%@",[beeFullArray objectAtIndex:index]]];
    
}


-(void)viewWillDisappear:(BOOL)animated
{
    

    if (passwordVw)
    {
        [passwordVw removeFromSuperview];
        passwordVw = nil;
    }
    
  //  [bubble restoreBtn];

    if (menuVw)
    {
        [menuVw removeFromSuperview];
        menuVw  = nil;
    }
    
    if (bubble)
    {
        [bubble removeFromSuperview];
        bubble  = nil;
    }
    
 
    [profileImgNav removeFromSuperview];
    [iconlabel removeFromSuperview];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"bubbleStatus" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"menuBtnClicked" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"privateTimer" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"privatePassword" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        // back button was pressed.  We know this is true because self is no longer
        // in the navigation stack.
        canExecuteViewWillAppear = YES;
        isPrivateChat = NO;
        
    }
    
    
    if (!isGalleryOpen && !isCamOpened && !isvideoPlayed)
    {
        fetchedResultsController = nil;
    }
    
    if (ifShowingStickers) {
        ifShowingStickers = !ifShowingStickers;
    }
    else
    {
       // [[self xmppMessageArchivingCoreDataStorage] updateUnreadMessage:self.user.jid clearValue:YES];
        if(!isPrivateChat)
        {
            [[self xmppMessageArchivingCoreDataStorage] updateUnreadMessage:self.user.jid clearValue:YES];
        }
    }
    
    myProfileImage = nil;
    userProfileImage = nil;
    self.chatTextview.text = @"";
    [self.titleLabel removeFromSuperview];
    [profileImgNav removeFromSuperview];
    [backButton removeFromSuperview];
    [menuButton removeFromSuperview];
    self.singleton.chatScreenUser = nil;
    
    [self resignkeyBoard];
    
    if (privateVw) {
        [UIView animateWithDuration:0.3
                              delay:0
                            options:UIViewAnimationCurveEaseInOut
                         animations:^{
                             privateVw.frame =  CGRectMake(0, -privateVw.frame.size.height, privateVw.frame.size.width, privateVw.frame.size.height);
                             
                         }
                         completion:^(BOOL finished){
                             [privateVw removeFromSuperview];
                             privateVw = nil;
                         }];
        
    }
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0),^
                   {
                       [messageMethods stateMessage:@"gone" jid:user.jidStr];
                   }
                   );
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Action For Back Button
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)backbuttonAction:(id)sender
{
    isPrivateChat = NO;
    isEditMode = NO;
  //  isPrivateHistoryVw = NO;
    canExecuteViewWillAppear = YES;
    
    [_chatTextview resignFirstResponder];

    if (    isPrivateHistoryVw ) {
        
        isPrivateHistoryVw = NO;
        menu.shouldShow = YES;

        [self createPopUpforText];
        [self createPopUpforMedia];
        fetchedResultsController.delegate=nil;
        fetchedResultsController = nil;
        [UIView transitionWithView: chatTableView
                          duration: 0.35f
                           options: UIViewAnimationOptionTransitionCrossDissolve
                        animations: ^(void)
         {
             [chatTableView reloadData];
         }
                        completion: ^(BOOL isFinished)
         {
         }];

        
    }
    else if (showImpMsgLy)
    {
        
        [UIView transitionWithView:popUp
                          duration:0.2
                           options:UIViewAnimationOptionTransitionNone
                        animations:^{
                            popUp.transform =  CGAffineTransformMakeScale(0.3, 0.3);
                            [menuButton setHidden:NO];

                            
                            
                        } completion:^(BOOL finished)
         
         {
             [popUp removeFromSuperview];
             
             showImpMsgLy = NO;
             menu.shouldShow = YES;

         }];

        
        
        
        

    }else if (isImageEditorOpend)
    {
        [UIView transitionWithView:imgEdit
                          duration:0.2
                           options:UIViewAnimationOptionTransitionNone
                        animations:^{
                            imgEdit.transform =  CGAffineTransformMakeScale(0.3, 0.3);
                            
                            
                        } completion:^(BOOL finished)
         
         {
             [imgEdit removeFromSuperview];
             
             isImageEditorOpend = NO;
             menu.shouldShow = YES;
         }];
        

        
        
    }
    
    else
    {

    for (UIViewController *controller in [self.navigationController viewControllers])
    {
        if ([controller isKindOfClass:[ViewController class]])
        {
            [self.navigationController popToViewController:controller animated:YES];
            break;
        }
    }
    
    
    if (self.presentingViewController)
    {
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        
    }
    }
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIBubbleTableViewDataSource implementation
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger)rowsForBubbleTable:(UIBubbleTableView *)tableView
{
    return [self.messageData count];
}
- (NSBubbleData *)bubbleTableView:(UIBubbleTableView *)tableView dataForRow:(NSInteger)row
{
    return [self.messageData objectAtIndex:row];
}
-(void)resignkeyBoard
{
    [self.popupMenu_txt dismissAnimated:YES];
    [self.popupMenu_media dismissAnimated:YES];
    [self.popupMenu_sticker dismissAnimated:YES];
    
    
    [self.chatTextview resignFirstResponder];
    [self closeBee_Board];
    if (isMenuShowing)
        [self menuButtonAction:nil];
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Keyboard events
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    if(showImpMsgLy)
    {
        [UIView transitionWithView: popUp
                          duration: 0.35f
                           options: UIViewAnimationOptionTransitionCrossDissolve
                        animations: ^(void)
         {
         }
                        completion: ^(BOOL isFinished)
         {
             [popUp removeFromSuperview];
             
             showImpMsgLy = NO;
         }];
    }
    
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:[aNotification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue] animations:^{
        CGRect frame = inputView.frame;
        CGRect frame_For_Chat = chatTableView.frame;

        
        int V_height = (abs((int)self.view.frame.size.height));
        int I_height = (abs((int)inputView.frame.size.height));
        int I_OriginY = (abs((int)inputView.frame.origin.y));
        int actual_Y = V_height - I_height;
        
        
   
        

        frame_For_Chat = chatTableView.frame;

 
        if(actual_Y >= I_OriginY + 3 || actual_Y <= I_OriginY + 3 )
        {
          
            frame.origin.y =  (self.view.frame.size.height) - (kbSize.height + inputView.frame.size.height);
            frame_For_Chat.size.height =  frame.origin.y;
        }
        
        inputView.frame = frame;
        chatTableView.frame = frame_For_Chat;
        [self scrollBubbleViewToBottomAnimated:NO];
        
    }];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
  //  NSDictionary* info = [aNotification userInfo];
    //CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:[aNotification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]
                     animations:^{
        CGRect frame = inputView.frame;
        
        frame.origin.y = (self.view.frame.size.height - inputView.frame.size.height);
        inputView.frame = frame;
        frame = chatTableView.frame;
        frame.size.height = (self.view.frame.size.height - inputView.frame.size.height);
        chatTableView.frame = frame;
        [self scrollBubbleViewToBottomAnimated:NO];
    }];
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Delegate Methods for Message TextView
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    float diff = (growingTextView.frame.size.height - height);
	CGRect r = inputView.frame;
    r.size.height -= diff;
    r.origin.y += diff;
	inputView.frame = r;
}
- (BOOL)growingTextViewShouldBeginEditing:(HPGrowingTextView *)growingTextView;
{
    if (isMenuShowing)
        [self menuButtonAction:nil];
    return YES;
}
-(void)growingTextViewDidBeginEditing:(HPGrowingTextView *)growingTextView
{
    
    checkComposing = YES;
    [messageMethods stateMessage:@"composing" jid:user.jidStr];
    
}
- (void)growingTextViewDidChange:(HPGrowingTextView *)growingTextViewn
{
    if (!checkComposing) {
        [messageMethods stateMessage:@"composing" jid:user.jidStr];
        checkComposing = YES;
    }
}
- (void)growingTextViewDidEndEditing:(HPGrowingTextView *)growingTextView;
{
    [messageMethods stateMessage:@"inactive" jid:user.jidStr];
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Action for sending Message
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)SendButtonAction:(id)sender
{
    //[self.chatTextview resignFirstResponder];
    NSString *OrgMessageStr = self.chatTextview.text;
    
    NSData *data = [OrgMessageStr dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    NSString *messageStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    
    if([messageStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0) {
        if (!isPrivateChat) {
            
            if (isImpOn)
            {
                [messageMethods impTextmessage:messageStr jid:user.jidStr];
                [self performSelector:@selector(impButtonAction:) withObject:self];

            }
            else
            {
            [messageMethods textmessage:messageStr jid:user.jidStr];
            }

            
        }
        else
        {
            NSString *messageId = [[self xmppStream] generateUUID];
            XMPPMessage *privateMessage = [XMPPMessage new];
            [privateMessage addAttributeWithName:@"id"  stringValue:messageId];
            [privateMessage addAttributeWithName:@"to" stringValue:user.jidStr];
            [privateMessage addAttributeWithName:@"from" stringValue:[self xmppStream].myJID.bare];
            [privateMessage addAttributeWithName:@"type" stringValue:@"chat"];
            NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
            [body setStringValue:messageStr];
            [privateMessage addChild:body];
            NSXMLElement *private = [NSXMLElement elementWithName:@"private"];
            [private addAttributeWithName:@"xmlns" stringValue:@"private"];
            [private addAttributeWithName:@"type" stringValue:@"message"];
            [privateMessage addChild:private];
            [[self xmppStream] sendElement:privateMessage];
            
            
        }
        self.chatTextview.text = @"";
        
    }
        checkComposing = NO;
        [messageMethods stateMessage:@"inactive" jid:user.jidStr];
}



-(UIView*)mediaCellView:(CGSize)imageSize status:(NSString*)status image:(UIImage*)image isSender:(BOOL)isSender msgId:(NSString*)msgId
{
    
   // [UIImage imageNamed:@"placeholder"]
    
    UIView* mediaView = [UIView new];
    mediaView.frame = CGRectMake(15, 0, imageSize.width + 10, imageSize.height + 10);
    mediaView.backgroundColor = [UIColor whiteColor];
    mediaView.layer.cornerRadius = 5.0f;
    mediaView.layer.masksToBounds = YES;


    
    UIImageView* cellmediaImageView = [UIImageView new];
    cellmediaImageView.tag = 100;
    cellmediaImageView.frame = CGRectMake(5, 5,imageSize.width, imageSize.height);
    cellmediaImageView.image = image;
    cellmediaImageView.layer.cornerRadius = 5.0f;
    [cellmediaImageView.layer setBorderWidth:5.0f];
    [cellmediaImageView.layer setBorderColor:(__bridge CGColorRef)([UIColor whiteColor])];
    cellmediaImageView.layer.masksToBounds = YES;
    cellmediaImageView.userInteractionEnabled = YES;
    [mediaView addSubview:cellmediaImageView];
    
    
    UIView* blackTransVw = [UIView new];
    blackTransVw.backgroundColor = RGBA(0, 0, 0, 0.6);
    blackTransVw.frame = CGRectMake((cellmediaImageView.frame.size.width - widthOfBlackTransVw)/2, (cellmediaImageView.frame.size.height - widthOfBlackTransVw)/2, widthOfBlackTransVw, widthOfBlackTransVw);
    blackTransVw.layer.cornerRadius = widthOfBlackTransVw/2;
    blackTransVw.layer.masksToBounds = YES;
    [cellmediaImageView addSubview:blackTransVw];
    
    if ([[self.singleton.mediaFileIndicatorDict allKeys] containsObject:msgId])
    {
        RMDownloadIndicator* uploadIndicator = [self.singleton.mediaFileIndicatorDict objectForKey:msgId];
        [blackTransVw addSubview:uploadIndicator];
    }
    else
    {
        UIImageView* iconImgVw = [UIImageView new];
        [blackTransVw addSubview:iconImgVw];
        
        if ([status isEqualToString:@"0"] || [status isEqualToString:@"3"])
        {
            UIButton* celldownloadBtn = [UIButton buttonWithType:UIButtonTypeSystem];
            celldownloadBtn.frame = CGRectMake(0 , 0, imageSize.width, imageSize.height);
            if ([status isEqualToString:@"0"])
            {
                if (isSender)
                {
                    UIImage * icon = [UIImage imageNamed:@"retry"];
                    iconImgVw.image = icon;
                    iconImgVw.frame = CGRectMake((blackTransVw.frame.size.width - icon.size.width)/2, (blackTransVw.frame.size.height - icon.size.height)/2, icon.size.width, icon.size.height);
                    
                    if (!isEditMode)
                    {                  [celldownloadBtn addTarget:self action:@selector(UploadBtn_CLICKED:event:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else
                    {
                        celldownloadBtn.userInteractionEnabled = NO;
                    }
                }
                else
                {
                    UIImage * icon = [UIImage imageNamed:@"download"];
                    iconImgVw.image = icon;
                    iconImgVw.frame = CGRectMake((blackTransVw.frame.size.width - icon.size.width)/2, (blackTransVw.frame.size.height - icon.size.height)/2, icon.size.width, icon.size.height);
                    if (!isEditMode)
                    {                [celldownloadBtn addTarget:self action:@selector(downloadBtn_CLICKED:event:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else
                    {
                        celldownloadBtn.userInteractionEnabled = NO;
                    }
                    
                }
            }
            else
            {
                if (isSender)
                {
                    UIImage * icon = [UIImage imageNamed:@"retry"];
                    iconImgVw.image = icon;
                    iconImgVw.frame = CGRectMake((blackTransVw.frame.size.width - icon.size.width)/2, (blackTransVw.frame.size.height - icon.size.height)/2, icon.size.width, icon.size.height);
                    
                    if (!isEditMode)
                    {
                        [celldownloadBtn addTarget:self action:@selector(UploadBtn_CLICKED:event:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else
                    {
                        celldownloadBtn.userInteractionEnabled = NO;
                    }
                    
                }
                else
                {
                    UIImage * icon = [UIImage imageNamed:@"retry"];
                    iconImgVw.image = icon;
                    iconImgVw.frame = CGRectMake((blackTransVw.frame.size.width - icon.size.width)/2, (blackTransVw.frame.size.height - icon.size.height)/2, icon.size.width, icon.size.height);
                    if (!isEditMode)
                    {
                        [celldownloadBtn addTarget:self action:@selector(downloadBtn_CLICKED:event:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else
                    {
                        celldownloadBtn.userInteractionEnabled = NO;
                    }
                    
                }
            }
            
            
            [celldownloadBtn setTintColor:[UIColor brownColor]];
            celldownloadBtn.tag = 101;
            [cellmediaImageView addSubview:celldownloadBtn];
        }
        
        if ([status isEqualToString:@"2"])
        {
            UIButton* cellPlayBtn = [UIButton buttonWithType:UIButtonTypeSystem];
            cellPlayBtn.frame = CGRectMake(0 , 0, image.size.width, image.size.height);
            
            mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
            NSMutableDictionary* dict = [mediaDb fetchMedia:msgId];
            
            if ([[dict objectForKey:@"video_img"] isEqualToString:@"video"])
            {
                
                UIImage * icon = [UIImage imageNamed:@"play"];
                iconImgVw.image = icon;
                iconImgVw.frame = CGRectMake((blackTransVw.frame.size.width - icon.size.width)/2, (blackTransVw.frame.size.height - icon.size.height)/2, icon.size.width, icon.size.height);
            }
            else
            {
                [blackTransVw removeFromSuperview];
            }
            if (!isEditMode) {
                [cellPlayBtn addTarget:self action:@selector(photoImageClicked:event:) forControlEvents:UIControlEventTouchUpInside];
                
            }
            else
            {
                cellPlayBtn.userInteractionEnabled = NO;
            }
            [cellPlayBtn setTintColor:[UIColor brownColor]];
            
            cellPlayBtn.tag = 102;
            [cellmediaImageView addSubview:cellPlayBtn];
        }
    }
    
    return mediaView;
}
-(UIView *)PhotoViewForBubbleData:(UIImage *)image status:(NSString*)status isSender:(BOOL)isSender msgId:(NSString*)msgId
{
    
    UIImage  *newImage =    [UIImage squareImageWithImage:image scaledToSize:CGSizeMake(150,150)];
    
    UIView* PhotoView = [self mediaCellView:newImage.size status:status image:newImage isSender:isSender msgId:msgId];
    return PhotoView;
}

-(void)photoImageClicked:(id)sender event:(id)event
{
    
  //  UIButton *button = (UIButton *)sender;
//    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:chatTableView];
//    NSIndexPath *indexPath = [chatTableView indexPathForRowAtPoint:buttonPosition];
//    NSLog(@"%ld",(long)indexPath.row);

    

    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPos = [touch locationInView:chatTableView];
    NSIndexPath *indexPath = [chatTableView  indexPathForRowAtPoint:touchPos];
  //  NSLog(@"%ld",(long)indexPath.row);
    
    
    if(indexPath != nil){
        
        UIBubbleTableViewCell *swipedCell  = (UIBubbleTableViewCell*)[chatTableView cellForRowAtIndexPath:indexPath];
//        NSLog(@"%@",swipedCell.data.view);
//        NSLog(@"%@",swipedCell.data.jidStr);
//        NSLog(@"%@",swipedCell.data.date);
//        NSLog(@"%@",swipedCell.data.msgID);
        CGRect myRect = [chatTableView rectForRowAtIndexPath:indexPath];
        NSMutableDictionary* dict = [[mediaDbEditor sharedInstance] fetchMedia:swipedCell.data.msgID];
        
        
        if ([[dict objectForKey:@"video_img"] isEqualToString:@"video"])
        {
            if ([dict objectForKey:@"url"])
            {
                isvideoPlayed = YES;
                
                MPMoviePlayerViewController* movieController = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL fileURLWithPath:[dict objectForKey:@"url"]]];
                [movieController.moviePlayer prepareToPlay];
                [self presentMoviePlayerViewControllerAnimated:movieController];
                [movieController.moviePlayer play];
            }
        }
        else
        {
            if ([dict objectForKey:@"url"])
            {
                CGPoint touchPosInSelf = [touch locationInView:self.view];
             //   NSLog(@"%@  %@ %@  %f", NSStringFromCGRect(myRect),NSStringFromCGPoint(chatTableView.contentOffset),NSStringFromCGPoint(touchPosInSelf) ,myRect.origin.y - chatTableView.contentOffset.y);
                UIImage* image = [UIImage imageWithContentsOfFile:[dict objectForKey:@"url"]];
                UIImageView* cellmediaImageView = [UIImageView new];
                cellmediaImageView.tag = 100;
                cellmediaImageView.frame = CGRectMake(self.view.frame.size.width/2 ,self.view.frame.size.height,image.size.width, image.size.height);
                cellmediaImageView.image = image;
                canExecuteViewWillAppear = YES;
                //[EXPhotoViewer showImageFrom:cellmediaImageView];
                
                if (!isFetching)
                {
                
                if (!isPrivateHistoryVw)
                {
                    [self getFilesData:user.jid completion:^(NSArray *filesArray, BOOL success) {
                        
                        if(success)
                        {
                            NSUInteger indexValue;
                            for(FileRecord *record in self.photos)
                            {
                                
                                NSString *stringToCompare = [NSString stringWithFormat:@"%@",record.URL];
                                int lengthOfString = [stringToCompare length];
                                NSString *str = [stringToCompare substringFromIndex:lengthOfString-15];
                                
                                NSString *stringFromLocal = [NSString stringWithFormat:@"%@", [NSURL fileURLWithPath:[dict objectForKey:@"url"]]];
                                int lengthOfLocalString = [stringFromLocal length];
                                NSString *str1 = [stringFromLocal substringFromIndex:lengthOfLocalString-15];
                                
                            //    NSLog(@"%@",[NSURL fileURLWithPath:[dict objectForKey:@"url"]]);
                                if ([str isEqualToString:str1])
                                {
                                    indexValue = [self.photos indexOfObject:record];
                                    pushIndex = indexValue;

                                }
                            }
                            dispatch_async(dispatch_get_main_queue(), ^
                                           {
                                               isFetching = NO;
                                               DetailGalleryViewController *detailsView = [DetailGalleryViewController new];
                                               detailsView.detailArray = [self.photos mutableCopy];
                                               detailsView.indexpath = [NSIndexPath indexPathForRow:indexValue inSection:0];
                                               [self.navigationController pushViewController:detailsView animated:YES];
                                               
                                           });
                        }
                    }];
                    
                }
                else
                {
                [self getPrivateFilesData:user.jid completion:^(NSArray *filesArray, BOOL success) {
                    
                    if(success)
                    {
                        NSUInteger indexValue;
                        
                        for(FileRecord *record in self.photos)
                        {
                            
                            NSString *stringToCompare = [NSString stringWithFormat:@"%@",record.URL];
                            int lengthOfString = (int)[stringToCompare length];
                            NSString *str = [stringToCompare substringFromIndex:lengthOfString-15];
                            
                            NSString *stringFromLocal = [NSString stringWithFormat:@"%@", [NSURL fileURLWithPath:[dict objectForKey:@"url"]]];
                            int lengthOfLocalString = (int)[stringFromLocal length];
                            NSString *str1 = [stringFromLocal substringFromIndex:lengthOfLocalString-15];
                            
                            NSLog(@"%@",[NSURL fileURLWithPath:[dict objectForKey:@"url"]]);
                           if ([str isEqualToString:str1])
                           {
                                indexValue = [self.photos indexOfObject:record];
                            }
                        }
                        dispatch_async(dispatch_get_main_queue(), ^
                                       {
                                           isFetching = NO;
                                           DetailGalleryViewController *detailsView = [DetailGalleryViewController new];
                                           detailsView.detailArray = [self.photos mutableCopy];
                                           detailsView.indexpath = [NSIndexPath indexPathForRow:indexValue inSection:0];
                                           
                                           [self.navigationController pushViewController:detailsView animated:YES];
                                           
                                       });
                    }
                }];
                }

            }
            }
        }
    }
    
}






-(void)getFilesData:(XMPPJID *)roomjid completion:(void (^)(NSArray *filesArray,BOOL success))completion
{
    isFetching = YES;
    [self.photos removeAllObjects];
    [[FetchsharedFile sharedMediaServer] fetchMediaFiles:roomjid.bare From:@"sharedFileView" block:^(NSArray *items, NSError *error) {
        if (!error && items.count)
        {

            
            for (NSDictionary *dic in items)
            {
                if(isPrivateChat)
                {
                    FileRecord *record = [[FileRecord alloc] init];
                    record.URL = [NSURL fileURLWithPath:[self getFullDocumentUrl:[dic objectForKey:@"url"]]];
                    record.type = [dic objectForKey:@"video_img"];
                    record.isPrivate = YES;
                    record.msgId = [dic objectForKey:@"msgId"];
                    [self.photos addObject:record];
                    record = nil;
                    
                }
                else
                {
                
                NSNumber  *boolean = (NSNumber *)[dic objectForKey:@"isPrivate"];
                if([boolean boolValue] == YES)
                {
                    // this is the YES case
                }
                else
                {
                    
                    FileRecord *record = [[FileRecord alloc] init];
                    record.URL = [NSURL fileURLWithPath:[self getFullDocumentUrl:[dic objectForKey:@"url"]]];
                    record.type = [dic objectForKey:@"video_img"];
                    record.isPrivate = NO;
                    record.msgId = [dic objectForKey:@"msgId"];
                    [self.photos addObject:record];
                    record = nil;
                    
                }
                }
            
        }
        }
        if (self.photos.count > 0)
            completion(self.photos,YES);
        else
            completion(nil,NO);
        
    }];
    
}

-(void)getPrivateFilesData:(XMPPJID *)roomjid completion:(void (^)(NSArray *filesArray,BOOL success))completion
{
    isFetching = YES;

    [self.photos removeAllObjects];
    [[FetchsharedFile sharedMediaServer] fetchMediaFiles:roomjid.bare From:@"sharedFileView" block:^(NSArray *items, NSError *error) {
        if (!error && items.count)
        {

            for (NSDictionary *dic in items)
            {
                
                NSNumber  *boolean = (NSNumber *)[dic objectForKey:@"isPrivate"];
                if([boolean boolValue] == YES)
                {
                //    NSLog(@"YES --->> isPrivate");
                    // this is the YES case
                    FileRecord *record = [[FileRecord alloc] init];
                    record.URL = [NSURL fileURLWithPath:[self getFullDocumentUrl:[dic objectForKey:@"url"]]];
                    record.type = [dic objectForKey:@"video_img"];
                    record.isPrivate = YES;
                    record.msgId = [dic objectForKey:@"msgId"];
                    [self.photos addObject:record];
                    record = nil;

                }
                else
                {
               //     NSLog(@"NO --->> isPrivate");
                    
                    
                }
                
            }
        }
        if (self.photos.count > 0)
            completion(self.photos,YES);
        else
            completion(nil,NO);
        
    }];
    
}




-(void)downloadBtn_CLICKED:(id)sender event:(id)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPos = [touch locationInView:chatTableView];
    NSIndexPath *indexPath = [chatTableView  indexPathForRowAtPoint:touchPos];
    if(indexPath != nil){
        UIBubbleTableViewCell *swipedCell  = (UIBubbleTableViewCell*)[chatTableView cellForRowAtIndexPath:indexPath];
//        NSLog(@"%@",swipedCell.data.view);
//        NSLog(@"%@",swipedCell.data.jidStr);
//        NSLog(@"%@",swipedCell.data.date);
//        NSLog(@"%@",swipedCell.data.msgID);
        mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
        NSMutableDictionary* dict = [mediaDb fetchMedia:swipedCell.data.msgID];
        
        NSArray *filterArray = [NSArray arrayWithObject:[dict objectForKey:@"url"]];
        
        NSArray *beginMatch = [filterArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self BEGINSWITH[cd] %@", @"http"]];

        
        if (beginMatch.count == 1)
        {
            [self fileDownload1:[dict objectForKey:@"url"] msgId:[dict objectForKey:@"msgId"]];
        }
        else
        {
            [self imageCache:[dict objectForKey:@"url"] indexPath:indexPath MsgID:[dict objectForKey:@"msgId"]];
        }
        
        }
    
    
}


-(void)fileDownload1:(NSString*)urlString msgId:(NSString*)msgId
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request] ;
    
    RMDownloadIndicator* downloadIndicator =   [self addDownloadIndicators];
    self.singleton = [Singleton sharedMySingleton];
    [self.singleton.mediaFileIndicatorDict setObject:downloadIndicator forKey:msgId];
    [chatTableView reloadData];
    
    NSString* path = [self getUniqueFilenameInFolder:@"mediaFile" forFileExtension:[[urlString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"."]] lastObject]];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *newFilePath = [[paths lastObject] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",path]];
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:newFilePath append:NO];
    
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        [self updateView:((float)totalBytesRead/ totalBytesExpectedToRead)*100 indicatorView:downloadIndicator];
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
    {
      //  NSLog(@"Successfully downloaded file to %@", newFilePath);
        

        NSMutableDictionary* dict = [NSMutableDictionary new];
        [dict setObject:msgId forKey:@"msgId"];
        [dict setObject:@"2" forKey:@"success_failure"];
        [dict setObject:@"" forKey:@"imgData"];
        [dict setObject:newFilePath forKey:@"url"];
//        [dict setObject:[NSNumber numberWithBool:NO] forKey:@"isPrivate"];
        [dict setObject:[NSNumber numberWithBool:NO] forKey:@"loc_cloud"];
        mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
        [mediaDb updateMedia:dict];
        mediaDb = nil;
        
        [self.singleton.mediaFileIndicatorDict removeObjectForKey:msgId];
        [chatTableView reloadData];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
      //  NSLog(@"Error: %@", error);
     //   [self isUploadingMsgId:msgId success:@"3"];
        [self isNotDownloaded: msgId success:@"3" servicePath:urlString];
        [self.singleton.mediaFileIndicatorDict removeObjectForKey:msgId];
        [chatTableView reloadData];
        
    }];
    
    [operation start];
}
-(void)fileDownload:(NSString*)urlString msgId:(NSString*)msgId
{
    NSURL* sourceURL = [NSURL URLWithString:urlString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:sourceURL];
    
    AFHTTPRequestOperation *operation =   [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    RMDownloadIndicator* downloadIndicator =   [self addDownloadIndicators];
    self.singleton = [Singleton sharedMySingleton];
    [self.singleton.mediaFileIndicatorDict setObject:downloadIndicator forKey:msgId];
    [chatTableView reloadData];
    
    NSString* path = [self getUniqueFilenameInFolder:@"mediaFile" forFileExtension:[[urlString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"."]] lastObject]];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *newFilePath = [[paths lastObject] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",path]];
    
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:newFilePath append:NO];
    
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        [self updateView:((float)totalBytesRead/ totalBytesExpectedToRead)*100 indicatorView:downloadIndicator];
        
    }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         [self.singleton.mediaFileIndicatorDict removeObjectForKey:msgId];
         [chatTableView reloadData];
         
         NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         responseStr = [responseStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    //     NSLog(@"Response Str : %@",responseStr);
         
         NSError *error = nil;
         id objectsFromJson = [NSJSONSerialization JSONObjectWithData:[responseStr dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&error];
         
         if (!error) {
             NSLog(@"Success");
             //> NSLog(@"Object from Json : %@",objectsFromJson);
             
             if ([[objectsFromJson allKeys] containsObject:@"ResponseMsg"]) {
                 if ([[objectsFromJson valueForKey:@"ResponseMsg"] caseInsensitiveCompare:@"Success"] == NSOrderedSame)
                 {
                     
                     
                     [self isUploadingMsgId:msgId success:@"2"];
                     
                     
                     
                 }
                 else {
                     [self isUploadingMsgId:msgId success:@"3"];
                 }
                 
                 
             }
         }
         else {
         //    NSLog(@"Error : %@",error);
             [self isUploadingMsgId:msgId success:@"3"];
             
             [[[UIAlertView alloc] initWithTitle:ALERTVIEW_TITLE message:@"\u26A0 Invalid response from server." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
         }
         [self.singleton.mediaFileIndicatorDict removeObjectForKey:msgId];
         [chatTableView reloadData];
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         
         [self isUploadingMsgId:msgId success:@"3"];
         [self.singleton.mediaFileIndicatorDict removeObjectForKey:msgId];
         [chatTableView reloadData];
         
         
      //   NSLog(@"error : %@",  operation.responseString);
         
         [[[UIAlertView alloc] initWithTitle:ALERTVIEW_TITLE message:@"\u26A0 Invalid response from server." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
     }];
    [operation start];
}

-(void)UploadBtn_CLICKED:(id)sender event:(id)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPos = [touch locationInView:chatTableView];
    NSIndexPath *indexPath = [chatTableView  indexPathForRowAtPoint:touchPos];
    if(indexPath != nil){
        UIBubbleTableViewCell *swipedCell  = (UIBubbleTableViewCell*)[chatTableView cellForRowAtIndexPath:indexPath];
//        NSLog(@"%@",swipedCell.data.view);
//        NSLog(@"%@",swipedCell.data.jidStr);
//        NSLog(@"%@",swipedCell.data.date);
//        NSLog(@"%@",swipedCell.data.msgID);
        
        mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
        NSMutableDictionary* dict = [mediaDb fetchMedia:swipedCell.data.msgID];
        
        [self postPhoto:[dict objectForKey:@"url"]:swipedCell.data.msgID fileType:[dict objectForKey:@"video_img"]];
        
    }
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - post media
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)postMedia :(NSData*)nsdata :(NSString*)mimeType :(NSString*)name :(NSString*)fileName {
    
    AFHTTPClient* httpClient = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:FILE_UPLOAD]];
    
    
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    
    NSMutableURLRequest *afRequest = [httpClient multipartFormRequestWithMethod:@"POST" path:@"TestImage1" parameters:nil constructingBodyWithBlock:^(id <AFMultipartFormData>formData)
                                      {
                                          
                                          [formData appendPartWithFileData:nsdata name:name fileName:fileName mimeType:mimeType];
                                          
                                      }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:afRequest];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten,long long totalBytesWritten,long long totalBytesExpectedToWrite){
        
    //    NSLog(@"Sent %lld bytes %lld      div %.2lld",totalBytesWritten, totalBytesExpectedToWrite, totalBytesWritten/totalBytesExpectedToWrite);
        
        
    }];
    
    [operation  setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:kNilOptions error:nil];
        
        NSLog(@"dict: %@",dict);
        
    }
                                      failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"error: %@",  operation.responseString);
         
         
         
         
     }];
    [operation start];
    
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - push To View_RUTer
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(void)pushToView_RUTer:(NSMutableDictionary*)ruter_Dict
{
    if(!isEditMode)
    {
        RUTerViewController* ruter_Obj = [[RUTerViewController alloc]init];
        ruter_Obj.title_Name = user.displayName;
        ruter_Obj.RUTer_Dict = ruter_Dict;
        canExecuteViewWillAppear = YES;
        [self.navigationController pushViewController:ruter_Obj animated:YES];
    }
}

- (NSFetchedResultsController *)fetchedResultsController {
    
    if (fetchedResultsController)
        return fetchedResultsController;
    
    
    NSManagedObjectContext *moc = [[self xmppMessageArchivingCoreDataStorage] mainThreadManagedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:moc];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    if (isPrivateHistoryVw) {
        NSString *predicateFrmt = @"bareJidStr == %@ AND isPrivate == YES";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, self.user.jidStr];
        [fetchRequest setPredicate:predicate];
    }
    else
    {
        NSString *predicateFrmt = @"bareJidStr == %@ AND privateStatus == NO";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, self.user.jidStr];
        [fetchRequest setPredicate:predicate];
    }
    
    [fetchRequest setSortDescriptors:[NSSortDescriptor sortValues:@"timestamp" ascending:YES]];
    [fetchRequest setFetchBatchSize:10];
    
    fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:moc sectionNameKeyPath:@"sectionDate" cacheName:nil];
    [fetchedResultsController setDelegate:self];
    NSError *error = nil;
    if (![fetchedResultsController performFetch:&error])
        NSLog(@"ERROR IN CHAT FETCHCONTROLL:::::%@",error);
    
	return fetchedResultsController;
}

- (XMPPMessageArchiving_Message_CoreDataObject*)MsgWithID:(NSString*)msgId
{
    NSManagedObjectContext *moc = [[self xmppMessageArchivingCoreDataStorage] mainThreadManagedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:moc];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    NSString *predicateFrmt = @"messageID == %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,msgId];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:[NSSortDescriptor sortValues:@"timestamp" ascending:YES]];
    NSArray *delArray=[moc executeFetchRequest:fetchRequest error:nil];
    
    
    for (XMPPMessageArchiving_Message_CoreDataObject *CoreDataObject in delArray)
    {
        return CoreDataObject;
    }
    return nil;
}

- (void)clearChatWithID:(NSString*)JidStr
{
    
    
    NSManagedObjectContext *moc = [[self xmppMessageArchivingCoreDataStorage] mainThreadManagedObjectContext];
    
    [moc performBlockAndWait:^{
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:moc];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    NSString *predicateFrmt = @"bareJidStr == %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,JidStr];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:[NSSortDescriptor sortValues:@"timestamp" ascending:YES]];
    NSArray *delArray=[moc executeFetchRequest:fetchRequest error:nil];
    
    
    for (XMPPMessageArchiving_Message_CoreDataObject *CoreDataObject in delArray)
    {
        if (isPrivateHistoryVw) {
            
            if (CoreDataObject.isPrivate)
            {
                NSXMLElement *file = [CoreDataObject.message elementForName:@"file"];
                
                if (file)
                {
                    mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
                    [mediaDb deleteMedia:CoreDataObject.messageID];
                }
                
                
                
                [moc deleteObject:CoreDataObject];
            }
            
            NSError *error = nil;
            if (![moc save:&error])
            {
              //  NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
            }

            
        }
        else
        {            
        if (!CoreDataObject.isPrivate)
        {
            NSXMLElement *file = [CoreDataObject.message elementForName:@"file"];

            if (file)
            {
                mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
                [mediaDb deleteMedia:CoreDataObject.messageID];
            }
    
        

            [moc deleteObject:CoreDataObject];
        }

        NSError *error = nil;
        if (![moc save:&error])
        {
          //  NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
        }
        }
        
    }
    
    [[self xmppMessageArchivingCoreDataStorage]updateUnreadMessage:self.user.jid clearValue:YES];
    
        
        [[self xmppMessageArchivingCoreDataStorage]updateMessageOnRecentChat:self.user.jidStr];

  
    }];

}

-(void)updateOnRecentContact:(NSString *)Jidstr withString:(NSString *)UpdateString;
{

    NSManagedObjectContext *moc = [[self xmppMessageArchivingCoreDataStorage] mainThreadManagedObjectContext];
    [moc performBlockAndWait:^{
    NSEntityDescription *contactEnityDes = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Contact_CoreDataObject" inManagedObjectContext:moc];
    NSFetchRequest *contactRequest = [[NSFetchRequest alloc]init];
    [contactRequest setEntity:contactEnityDes];
    NSString *predicateFrmt = @"bareJidStr == %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,Jidstr];
    [contactRequest setPredicate:predicate];
    NSArray *delArray=[moc executeFetchRequest:contactRequest error:nil];
    
    for (XMPPMessageArchiving_Contact_CoreDataObject *coreMessage in delArray)
    {
        [coreMessage setValue:UpdateString forKey:@"mostRecentMessageBody"];
        NSError *error = nil;
        if (![moc save:&error])
        {
         //   NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
        }


    }
    }];
     

}





#pragma mark - Table view data source methods

// The data source methods are handled primarily by the fetch results controller
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[[self fetchedResultsController] sections] count];
}
// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSArray *sections = [[self fetchedResultsController] sections];
    if (section < [sections count])
    {
        id <NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
        return sectionInfo.numberOfObjects;
    }
    
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return tableView.sectionHeaderHeight+27;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(10,0,300,tableView.sectionHeaderHeight+17)];
    UIView *lineView = [UIView new];
    lineView.frame = CGRectMake(0, 0, tableView.frame.size.width, 1);
    customView.backgroundColor = [UIColor clearColor];
    
    
    // create the label objects
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.backgroundColor = RGBA(119, 186, 240, 0.7);
    //headerLabel.backgroundColor = [UIColor redColor];
    headerLabel.font = [UIFont fontWithName:FontString size:12];
    headerLabel.frame = CGRectMake((tableView.frame.size.width/2)-50,0,100,20);
    headerLabel.textColor = [UIColor whiteColor];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.layer.cornerRadius = 10;
    headerLabel.clipsToBounds = YES;
    
    
    if ([chatTableView.dataSource tableView:tableView numberOfRowsInSection:section] == 0) {
        return nil;
    }
    NSArray *sections = [[self fetchedResultsController] sections];
    if (section < [sections count])
    {
        id <NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
        headerLabel.text =  sectionInfo.name;
    }
    //[customView addSubview:lineView];
    [customView addSubview:headerLabel];
    
    return customView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    id details = [self getBubbledata:indexPath];
    if ([details isKindOfClass:[NSBubbleData class]]) {
        NSBubbleData *data = (NSBubbleData *)details;
        CGFloat height = (CGFloat)MAX(data.insets.top + data.view.frame.size.height + data.insets.bottom + 15 ,data.insets.top + data.view.frame.size.height + data.insets.bottom - 10);
        
       // NSLog(@"Height --->> %f" , height);
        
        details = nil; data = nil;
        
        return height;
    }
    else if ([details isKindOfClass:[NSHeaderData class]])
        return (CGFloat)[UIBubbleHeaderTableViewCell height];
    else
        return (CGFloat)MAX([UIBubbleTypingTableViewCell height], 60);
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *Tablecell;
    
    
    static NSString *CellIdentifier;
    id details = [self getBubbledata:indexPath];
    if ([details isKindOfClass:[NSBubbleData class]])
    {
        NSBubbleData *data = (NSBubbleData *)details;
        CellIdentifier = @"thebubbletable";
        UIBubbleTableViewCell *cell = (UIBubbleTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = (UIBubbleTableViewCell *)[[UIBubbleTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        cell.data = data;
        
        if (isEditMode) {
            if (cell.showAvatar)
            {
            cell.showAvatar = NO;
            }
            XMPPMessageArchiving_Message_CoreDataObject *coremessage = [[self fetchedResultsController] objectAtIndexPath:indexPath];
            
            
            if ([deleteMsgIdAry containsObject:coremessage.messageID])
            {
                cell.deleteCell = NO;
                cell.deleteCell_selected = YES;
            }
            else
            {
                cell.deleteCell = YES;
                cell.deleteCell_selected = NO;
            }
        }
        else
        {
            cell.showAvatar = YES;
            cell.deleteCell = NO;
        }
        
        Tablecell =  cell;
    }
    else if([details isKindOfClass:[NSHeaderData class]])
    {
        CellIdentifier = @"tblBubbleHeaderCell";
        UIBubbleHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        NSHeaderData *data = (NSHeaderData *)details;
        if (cell == nil) cell = [[UIBubbleHeaderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.date = data.text;
        Tablecell = cell;
    }
    else if([(NSString *)details isEqualToString:@"composing"])
    {
        CellIdentifier = @"tblBubbleTypingCell";
        UIBubbleTypingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) cell = [[UIBubbleTypingTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.typeString = @"SomeOne";
        Tablecell =  cell;
    }
    
    Tablecell.exclusiveTouch = YES;
    return Tablecell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}



- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.popupMenu_txt dismissAnimated:YES];
    [self.popupMenu_media dismissAnimated:YES];
    [self.popupMenu_sticker dismissAnimated:YES];
    
    
}
//-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
//{
//        [self.navigationController setNavigationBarHidden:YES animated:YES];
//}
//-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
//{
//        [self.navigationController setNavigationBarHidden:YES animated:YES];
//}
//-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
//{
//    if (!decelerate) {
//        [self.navigationController setNavigationBarHidden:NO animated:YES];
//    }
//}
//-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    [self.navigationController setNavigationBarHidden:NO animated:YES];
//
//}


-(id)getBubbledata:(NSIndexPath *)indexPath {
    
    NSBubbleData *messageDetails;
    XMPPMessageArchiving_Message_CoreDataObject *coremessage = [[self fetchedResultsController ] objectAtIndexPath:indexPath];
    if ([coremessage.composing isEqualToNumber:[NSNumber numberWithInt:1]] && [coremessage.outgoing isEqualToNumber:[NSNumber numberWithInt:0]])
    {
       composingTimer = [NSTimer scheduledTimerWithTimeInterval:3.0
                                         target:self
                                       selector:@selector(timerFired:)
                                        userInfo:[NSDictionary dictionaryWithObjectsAndKeys:indexPath, @"currentIndexPath", nil]
                                        repeats:NO];
        
      
        return @"composing";

    }
    
    NSXMLElement *element = [[NSXMLElement alloc] initWithXMLString:coremessage.messageStr error:nil];
    
    NSXMLElement *private = [coremessage.message elementForName:@"private"];
    
    if ([[element attributeStringValueForName:@"to"] isEqualToString:self.user.jidStr]){
        
        
        NSXMLElement *ruder = [coremessage.message elementForName:@"ruthere"];
        NSXMLElement *file = [coremessage.message elementForName:@"file"];
        NSXMLElement *imp = [coremessage.message elementForName:@"imp"];
        

        if (ruder) {
            
            if (![[[ruder attributeForName:@"type"] stringValue] isEqualToString:@"request"])
            {
                NSHeaderData * data = [NSHeaderData dataWithText:[NSString stringWithFormat:@"%@ by you",coremessage.body] date:coremessage.timestamp jidStr:user.jidStr name:@"sdfds" roomJid:@"sdfsd"];
                
                UILongPressGestureRecognizer*  longPressText = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressText:)];
                longPressText.delegate = self;
                [messageDetails.view setUserInteractionEnabled:YES];
                [messageDetails.view addGestureRecognizer:longPressText];
                
                return data;
            }
            
            else
            {
                NSString* statusStr = [self stringForStatus:[DBEditer_obj GetStatus_RUTerId:[[ruder attributeForName: @"ruthere_id"]stringValue] Sender:@"YES"user_Id:[[self xmppStream] myJID].user]];
                messageDetails = [NSBubbleData dataWithView:[self ruderView:@"BubbleTypeMine" status:statusStr] date:coremessage.timestamp type:BubbleTypeMine insets:UIEdgeInsetsMake(5.0, 5.0, 7.0, 15.0) jidString:user.jidStr RuderID:[NSString stringWithFormat:@"%@",[[ruder attributeForName: @"ruthere_id"]stringValue]] deliveryStr:coremessage.delivery  chattype:@"singlechat" isSender:YES messageID:coremessage.messageID];
                messageDetails.avatar = myProfileImage;
                
                //[self sendVirtualMessageForRuthereReached:[[ruder attributeForName: @"ruthere_id"] stringValue] statusStr:statusStr];


                UILongPressGestureRecognizer*  longPressText = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressText:)];
                longPressText.delegate = self;
                [messageDetails.view setUserInteractionEnabled:YES];
                [messageDetails.view addGestureRecognizer:longPressText];
                


            }
        }
        else if(file)
        {
            
            
            messageDetails = [self constructMediaViewBubledata:coremessage indexPath:indexPath];

            
//            messageDetails.avatar = myProfileImage;
            
            if (!isEditMode) {
                UILongPressGestureRecognizer*  longPressText = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressText:)];
                longPressText.delegate = self;
                [messageDetails.view setUserInteractionEnabled:YES];
                [messageDetails.view addGestureRecognizer:longPressText];
                

            }
            
            
        }
        else if([beeFullArray containsObject:coremessage.body])
        {
            
            NSString *smileyName = coremessage.body;
            
            if ([[smileyName substringWithRange:NSMakeRange(0, 3)] isEqual:@"sti"])
            {
                
            messageDetails = [NSBubbleData dataWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png",[coremessage.body stringByReplacingOccurrencesOfString:@"0" withString:@"00"]]] date:coremessage.timestamp type:BubbleTypeBeeMine jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"singlechat" messageID:nil];
                
            }
            else
            {
                
                NSURL *url = [[NSBundle mainBundle] URLForResource:coremessage.body withExtension:@""];
                messageDetails = [NSBubbleData dataWithImage:[UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]] date:coremessage.timestamp type:BubbleTypeBeeMine jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"singlechat" messageID:nil];
                
            }
            
//            messageDetails.avatar = myProfileImage;
            
            if (!isEditMode) {
                UILongPressGestureRecognizer*  longPressText = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressText:)];
                longPressText.delegate = self;
                [messageDetails.view setUserInteractionEnabled:YES];
                [messageDetails.view addGestureRecognizer:longPressText];
                

            }
        } else if (imp)
            
        {
            NSData *newdata=[coremessage.body dataUsingEncoding:NSUTF8StringEncoding
                                           allowLossyConversion:YES];
            NSString *Bubblestring=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];
            
            
            
            if (private)
            {
              messageDetails = [NSBubbleData dataWithTextWithTag:Bubblestring date:coremessage.timestamp type:BubbleTypePrivateMine jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"singlechat" messageID:coremessage.messageID impMsg:YES];
            }
            else
            {
                messageDetails = [NSBubbleData dataWithTextWithTag:Bubblestring date:coremessage.timestamp type:BubbleTypeMine jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"singlechat" messageID:coremessage.messageID impMsg:YES];
                
                
            }
            if (!isEditMode) {
                UILongPressGestureRecognizer*  longPressText = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressText:)];
                longPressText.delegate = self;
                [messageDetails.view setUserInteractionEnabled:YES];
                [messageDetails.view addGestureRecognizer:longPressText];
                

            }
            
            
//            messageDetails.avatar = myProfileImage;
        }
        else
        {
            
            
            //Decoding Sting
            NSData *newdata=[coremessage.body dataUsingEncoding:NSUTF8StringEncoding
                                          allowLossyConversion:YES];
            NSString *Bubblestring=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];

            
            
            if (private) {
                messageDetails = [NSBubbleData dataWithText:Bubblestring date:coremessage.timestamp type:BubbleTypePrivateMine jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"singlechat" messageID:coremessage.messageID];
            }
            else
            {
                messageDetails = [NSBubbleData dataWithText:Bubblestring date:coremessage.timestamp type:BubbleTypeMine jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"singlechat" messageID:coremessage.messageID];
            }
            if (!isEditMode) {
                UILongPressGestureRecognizer*  longPressText = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressText:)];
                longPressText.delegate = self;
                [messageDetails.view setUserInteractionEnabled:YES];
                [messageDetails.view addGestureRecognizer:longPressText];
                

            }
            
            
//            messageDetails.avatar = myProfileImage;
        }
    }
    else
    {
        
        NSXMLElement *ruder = [coremessage.message elementForName:@"ruthere"];
        NSXMLElement *file = [coremessage.message elementForName:@"file"];
        NSXMLElement *imp = [coremessage.message elementForName:@"imp"];


        if (ruder) {
            
            if (![[[ruder attributeForName:@"type"] stringValue] isEqualToString:@"request"])
            {
                NSHeaderData * data = [NSHeaderData dataWithText:[NSString stringWithFormat:@"%@ by %@",coremessage.body,user.displayName] date:coremessage.timestamp jidStr:user.jidStr name:@"sdfds" roomJid:@"sdfsd"];
                return data;
            }
            
            else
            {
                NSString* statusStr = [self stringForStatus:[DBEditer_obj GetStatus_RUTerId:[[ruder attributeForName: @"ruthere_id"]stringValue] Sender:@"YES"user_Id:user.jid.user]];
                
              //  [self sendVirtualMessageForRuthereReached:[[ruder attributeForName: @"ruthere_id"] stringValue] statusStr:statusStr];
                
                messageDetails = [NSBubbleData dataWithView:[self ruderView:@"BubbleTypeSomeoneElse" status:statusStr] date:coremessage.timestamp type:BubbleTypeSomeoneElse insets:UIEdgeInsetsMake(5.0, 15.0, 7.0, 8.0) jidString:user.jidStr RuderID:[NSString stringWithFormat:@"%@",[[ruder attributeForName: @"ruthere_id"]stringValue]] deliveryStr:coremessage.delivery  chattype:@"singlechat" isSender:NO messageID:coremessage.messageID];
                
//                messageDetails.avatar = userProfileImage;

                UILongPressGestureRecognizer*  longPressText = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressText:)];
                longPressText.delegate = self;
                [messageDetails.view setUserInteractionEnabled:YES];
                [messageDetails.view addGestureRecognizer:longPressText];
                

            }
        }
        else if(file)
        {
            
            
            messageDetails = [self constructMediaViewBubledata:coremessage indexPath:indexPath];

            
//            messageDetails.avatar = userProfileImage;
            
            UILongPressGestureRecognizer*  longPressText = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressText:)];
            longPressText.delegate = self;
            [messageDetails.view setUserInteractionEnabled:YES];
            [messageDetails.view addGestureRecognizer:longPressText];
            

            
        }
        
        else if([beeFullArray containsObject:coremessage.body])
        {
            
            messageDetails = [NSBubbleData dataWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png",[coremessage.body stringByReplacingOccurrencesOfString:@"0" withString:@"00"]]] date:coremessage.timestamp type:BubbleTypeBeeSomeoneElse jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"singlechat" messageID:nil];
            messageDetails.avatar = userProfileImage;
            UILongPressGestureRecognizer*  longPressText = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressText:)];
            longPressText.delegate = self;
            [messageDetails.view setUserInteractionEnabled:YES];
            [messageDetails.view addGestureRecognizer:longPressText];
            

            
        }
        else if (imp)
        {
            NSData *newdata=[coremessage.body dataUsingEncoding:NSUTF8StringEncoding
                                           allowLossyConversion:YES];
            NSString *Bubblestring=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];
            
            
            if (private)
            {
                messageDetails = [NSBubbleData dataWithTextWithTag:Bubblestring date:coremessage.timestamp type:BubbleTypePrivateSomeoneElse jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"singlechat" messageID:coremessage.messageID impMsg:YES];
            }
            else
            {
             messageDetails = [NSBubbleData dataWithTextWithTag:Bubblestring date:coremessage.timestamp type:BubbleTypeSomeoneElse jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"singlechat" messageID:coremessage.messageID impMsg:YES];
            }
            
            messageDetails.avatar = userProfileImage;
            UILongPressGestureRecognizer*  longPressText = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressText:)];
            longPressText.delegate = self;
            [messageDetails.view setUserInteractionEnabled:YES];
            [messageDetails.view addGestureRecognizer:longPressText];
            


        }
        else
        {
            NSData *newdata=[coremessage.body dataUsingEncoding:NSUTF8StringEncoding
                                           allowLossyConversion:YES];
            NSString *Bubblestring=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];
            
            if (private) {
                messageDetails = [NSBubbleData dataWithText:Bubblestring date:coremessage.timestamp type:BubbleTypePrivateSomeoneElse jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"singlechat" messageID:coremessage.messageID];
            }
            else
            {
                messageDetails = [NSBubbleData dataWithText:Bubblestring date:coremessage.timestamp type:BubbleTypeSomeoneElse jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"singlechat" messageID:coremessage.messageID];
            }
//            messageDetails.avatar = userProfileImage;
            UILongPressGestureRecognizer*  longPressText = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressText:)];
            longPressText.delegate = self;
            [messageDetails.view setUserInteractionEnabled:YES];
            [messageDetails.view addGestureRecognizer:longPressText];


        }
    }
    return messageDetails;
    
}

#pragma mark - Getting user presence
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence
{
    if([[[presence from] bareJID] isEqualToJID:user.jid])
    {
        NSString *presenceType = [presence type]; // online/offline
        if ([presenceType isEqualToString:@"available"])
        {
            presenceLabel.frame = CGRectMake(12 , statusLabel.frame.size.height, 100, 25);

            iconlabel.backgroundColor = [UIColor greenColor];
            presenceLabel.text = @"Online";
            statusLabel.text = @"Status:";
            iconlabel.hidden = NO;
            
            [self popAnimatedforPresenceView:YES];


           // lastLoginLabel.text = @"";
        }
        else if ([presenceType isEqualToString:@"unavailable"])
        {
            presenceLabel.frame = CGRectMake(12 , statusLabel.frame.size.height, 100, 25);

            iconlabel.backgroundColor = [UIColor yellowColor];
            presenceLabel.text = @"Away";
            statusLabel.text = @"Last buzzed:";
            iconlabel.hidden = NO;
            [lastActivity sendLastActivityQueryToJID:user.jid];
            
            [self popAnimatedforPresenceView:YES];

           // if (presence.status != nil)
              //  statusLabel.text = [presence status];
            
       // }
        }
    }
}

#pragma mark - Getting Last Seen
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (void)xmppLastActivity:(XMPPLastActivity *)sender didReceiveResponse:(XMPPIQ *)response
{
    NSXMLElement *query = [response elementForName:@"query" xmlns:@"jabber:iq:last"];
    if ([[query attributeForName:@"seconds"] stringValue] != nil)
    {
        NSTimeInterval timeinterval = [[[query attributeForName:@"seconds"] stringValue] doubleValue];
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
        [offsetComponents setSecond:-timeinterval]; // note that I'm setting it to -1
        NSDate *lastDate = [gregorian dateByAddingComponents:offsetComponents toDate:[NSDate date] options:0];
        
        statusLabel.text = @"Last Buzzed:";
        iconlabel.backgroundColor = [UIColor yellowColor];
        presenceLabel.frame = CGRectMake(2 , statusLabel.frame.size.height, 100, 25);
        presenceLabel.text = [NSString stringWithFormat:@"%@",[self dateToString:lastDate]];
       
        [self popAnimatedforPresenceView:YES];
        
        gregorian = nil;
        offsetComponents = nil;
        lastDate = nil;
    }
}

#pragma mark - Converting Date to String
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(NSString *)dateToString:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd, yyyy HH:mma"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    return stringFromDate;
}

#pragma mark - Popup the presence view
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(void)popAnimatedforPresenceView:(BOOL)animated
{
    if (animated)
    {
        [UIView animateKeyframesWithDuration:1.5 delay:0.7 options: UIViewKeyframeAnimationOptionAutoreverse  animations:^{
            
            [presenceView setAlpha:0.0f];
                CGRect presenceFrame = presenceView.frame;
                presenceFrame.origin.y = - 40;
                presenceView.frame = presenceFrame;
                [presenceView setAlpha:1.0f];
            
        } completion:^(BOOL finished) {
            [presenceView setAlpha:0.0f];
            CGRect presenceFrame = presenceView.frame;
            presenceFrame.origin.y = 40;
            presenceView.frame = presenceFrame;


        }];

    }
}




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Did Receive Message
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message
{
    NSXMLElement * directGroupInvite = [message elementForName:@"x" xmlns:@"jabber:x:conference"];
    
    if (self.singleton.chatScreenUser) {
        
        
        if ([[[message from] bareJID] isEqualToJID:user.jid])
        {
            if([message isChatMessageWithBody] && !directGroupInvite)
            {
                NSXMLElement *ruder = [message elementForName:@"ruthere"];
                NSXMLElement *private = [message elementForName:@"private"];
                
                if (ruder) {
                    
                    if (![[[ruder attributeForName:@"type"] stringValue] isEqualToString:@"request"])
                    {
                        [chatTableView reloadData];
                    }
                    
                }
                if([beeFullArray containsObject:message.body])
                {
                    imagePoper* popImage = [[imagePoper alloc]initFrame_Image:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png",message.body]] onView:self.view];
                }
                if (private)
                {
                    if (![[[private attributeForName:@"type"] stringValue] isEqualToString:@"message"])
                    {
                        [self createPrivateChatView];
                    }
                }
            }
        }
    }
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [chatTableView beginUpdates];
    [chatTableView layoutIfNeeded];

}
- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    NSLog(@"reloading Index %ld",(long)indexPath.row);
    switch(type)
    {
        case NSFetchedResultsChangeInsert:
            [chatTableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
            
        case NSFetchedResultsChangeDelete:
            [chatTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [chatTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            [chatTableView reloadData];
            break;
            
        case NSFetchedResultsChangeMove:
            [chatTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            [chatTableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
    }
}
- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [chatTableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationNone];
            break;
            
        case NSFetchedResultsChangeDelete:
            [chatTableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationNone];
            break;
    }
}
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [chatTableView endUpdates];
    [self scrollBubbleViewToBottomAnimated:YES];
}



- (void)timerFired:(NSTimer *)timer
{
    NSDictionary *dict = [timer userInfo];
    NSIndexPath *composeIndex = [dict valueForKey:@"currentIndexPath"];
    
    NSArray *anArrayOfIndexPath = [NSArray arrayWithArray:[chatTableView indexPathsForVisibleRows]];
    
    NSIndexPath *indexPath= [anArrayOfIndexPath lastObject];
    
    XMPPMessageArchiving_Message_CoreDataObject *coremessage = [[self fetchedResultsController ] objectAtIndexPath:indexPath];
    if ([coremessage.composing isEqualToNumber:[NSNumber numberWithInt:1]] && [coremessage.outgoing isEqualToNumber:[NSNumber numberWithInt:0]])
    {
       UIBubbleTableViewCell *cell = (UIBubbleTableViewCell *)[chatTableView cellForRowAtIndexPath:indexPath];
       [cell setHidden:YES];
    }
    
    [composingTimer invalidate];
}


- (void) scrollBubbleViewToBottomAnimated:(BOOL)animated {
    
    if (!shouldScrollToBottom)
    {
        shouldScrollToBottom = YES;
    }

    NSInteger lastSectionIdx = [chatTableView numberOfSections] - 1;
    

 

    if (lastSectionIdx >= 0 && [chatTableView numberOfRowsInSection:lastSectionIdx] > 0)
        {
         [chatTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:([chatTableView numberOfRowsInSection:lastSectionIdx] - 1) inSection:lastSectionIdx] atScrollPosition:UITableViewScrollPositionBottom animated:animated];
        }
}

-(void)scrollToFirstUnreadMsgPosition: (BOOL)scroll
{
    
    int unreadMsgCount = [[[NSUserDefaults standardUserDefaults]valueForKey:[NSString stringWithFormat:@"%@%@",KEY_UNREAD,user.jidStr]]intValue];
    
    if (unreadMsgCount != 0)
    {
        NSInteger lastSectionIdx = [chatTableView numberOfSections] - 1;
        [chatTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:([chatTableView numberOfRowsInSection:lastSectionIdx] - (unreadMsgCount)) inSection:lastSectionIdx] atScrollPosition:UITableViewScrollPositionBottom animated:scroll];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:[NSString stringWithFormat:@"%@%@",KEY_UNREAD,user.jidStr]];
    }
    
}

-(void)tappedCell:(UITapGestureRecognizer *)sender
{
    CGPoint location = [sender locationInView:chatTableView];
    NSIndexPath *swipedIndexPath = [chatTableView indexPathForRowAtPoint:location];
    UITableViewCell *swipedCell  = (UITableViewCell*)[chatTableView cellForRowAtIndexPath:swipedIndexPath];
    if ([swipedCell isKindOfClass:[UIBubbleTableViewCell class]]) {
        UIBubbleTableViewCell *bubbleCell = (UIBubbleTableViewCell *)swipedCell;
        
        if(bubbleCell.deleteCell)
        {
            XMPPMessageArchiving_Message_CoreDataObject *coremessage = [[self fetchedResultsController ] objectAtIndexPath:swipedIndexPath];

            
            if ([deleteMsgIdAry containsObject:coremessage.messageID] )
            {
                [deleteMsgIdAry removeObject:coremessage.messageID];
                bubbleCell.deleteImage.image = [UIImage imageNamed:@"check_U"];
            }
            else
            {
                [deleteMsgIdAry addObject:coremessage.messageID];
                bubbleCell.deleteImage.image = [UIImage imageNamed:@"check_S"];
            }
//
        }
        else
        {
            if ([bubbleCell.data.view isKindOfClass:[UILabel class]]) {
                self.textLabel = (UILabel *)bubbleCell.data.view;
                CFIndex index = [self characterIndexAtPoint:[sender locationInView:self.textLabel]];
                NSError *error = NULL;
                NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
                NSArray *matchArray = [detector matchesInString:self.textLabel.text options:0 range:NSMakeRange(0, self.textLabel.text.length)];
                if(matchArray.count)
                {
                    for (NSTextCheckingResult *match in matchArray) {
                        if ([match resultType] == NSTextCheckingTypeLink) {
                            NSRange matchRange = [match range];
                            if ([self isIndex:index inRange:matchRange])
                            {
                                NSArray *filterArray = [NSArray arrayWithObject:[NSString stringWithFormat:@"%@",match.URL]];
                                
                                NSArray *beginMatch = [filterArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self CONTAINS[cd] %@", @"maps.google.com"]];
                                
                                if(beginMatch.count == 1)
                                {
                                    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps://"]]) {
                                        
                                        NSString *urlString = [NSString stringWithFormat:@"%@",match.URL];
                                        
                                        urlString = [urlString stringByReplacingOccurrencesOfString:@"https://maps.google.com/maps?"
                                                                             withString:@"comgooglemaps://maps.google.com/maps?"];
                                        
                                        NSLog(@"modified URL : %@",urlString);
                                        [[UIApplication sharedApplication] openURL:[NSURL
                                                                                    URLWithString:urlString]];
                                    }
                                    else
                                    {
                                        [[UIApplication sharedApplication] openURL:match.URL];
                                    }

                                }
                                else
                                {
                                    [[UIApplication sharedApplication] openURL:match.URL];
                                }

                                
                                
                              
                                break;
                            }
                            else if(bubbleCell.data.msgID && [bubbleCell.data.chatType isEqualToString:@"singlechat"] && bubbleCell.data.type == BubbleTypeSomeoneElse)
                            {
                                
                                
                            }
                        }
                    }
                }
            }
        }
    }
    [self resignkeyBoard];
    
}
- (BOOL)isIndex:(CFIndex)index inRange:(NSRange)range {
    return index > range.location && index < range.location+range.length;
}
- (CFIndex)characterIndexAtPoint:(CGPoint)point {
    
    NSMutableAttributedString* optimizedAttributedText = [self.textLabel.attributedText mutableCopy];
    [self.textLabel.attributedText enumerateAttributesInRange:NSMakeRange(0, [self.textLabel.attributedText length]) options:0 usingBlock:^(NSDictionary *attrs, NSRange range, BOOL *stop) {
        if (!attrs[(NSString*)kCTFontAttributeName]) {
            [optimizedAttributedText addAttribute:(NSString*)kCTFontAttributeName value:self.textLabel.font range:NSMakeRange(0, [self.textLabel.attributedText length])];
        }
        if (!attrs[(NSString*)kCTParagraphStyleAttributeName]) {
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineBreakMode:self.textLabel.lineBreakMode];
            [optimizedAttributedText addAttribute:(NSString*)kCTParagraphStyleAttributeName value:paragraphStyle range:range];
        }
    }];
    // modify kCTLineBreakByTruncatingTail lineBreakMode to kCTLineBreakByWordWrapping
    [optimizedAttributedText enumerateAttribute:(NSString*)kCTParagraphStyleAttributeName inRange:NSMakeRange(0, [optimizedAttributedText length]) options:0 usingBlock:^(id value, NSRange range, BOOL *stop) {
        NSMutableParagraphStyle* paragraphStyle = [value mutableCopy];
        if ([paragraphStyle lineBreakMode] == kCTLineBreakByTruncatingTail) {
            [paragraphStyle setLineBreakMode:kCTLineBreakByWordWrapping];
        }
        [optimizedAttributedText removeAttribute:(NSString*)kCTParagraphStyleAttributeName range:range];
        [optimizedAttributedText addAttribute:(NSString*)kCTParagraphStyleAttributeName value:paragraphStyle range:range];
    }];
    if (!CGRectContainsPoint(self.textLabel.bounds, point)) {
        return NSNotFound;
    }
    CGRect textRect = [self textRect];
    if (!CGRectContainsPoint(textRect, point)) {
        return NSNotFound;
    }
    // Offset tap coordinates by textRect origin to make them relative to the origin of frame
    point = CGPointMake(point.x - textRect.origin.x, point.y - textRect.origin.y);
    // Convert tap coordinates (start at top left) to CT coordinates (start at bottom left)
    point = CGPointMake(point.x, textRect.size.height - point.y);
    //////
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)optimizedAttributedText);
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, textRect);
    CTFrameRef frame = CTFramesetterCreateFrame(framesetter, CFRangeMake(0, [self.textLabel.attributedText length]), path, NULL);
    if (frame == NULL) {
        CFRelease(path);
        return NSNotFound;
    }
    CFArrayRef lines = CTFrameGetLines(frame);
    NSInteger numberOfLines = self.textLabel.numberOfLines > 0 ? MIN(self.textLabel.numberOfLines, CFArrayGetCount(lines)) : CFArrayGetCount(lines);
    if (numberOfLines == 0) {
        CFRelease(frame);
        CFRelease(path);
        return NSNotFound;
    }
    
    NSUInteger idx = NSNotFound;
    CGPoint lineOrigins[numberOfLines];
    CTFrameGetLineOrigins(frame, CFRangeMake(0, numberOfLines), lineOrigins);
    
    for (CFIndex lineIndex = 0; lineIndex < numberOfLines; lineIndex++) {
        
        CGPoint lineOrigin = lineOrigins[lineIndex];
        CTLineRef line = CFArrayGetValueAtIndex(lines, lineIndex);
        
        // Get bounding information of line
        CGFloat ascent, descent, leading, width;
        width = CTLineGetTypographicBounds(line, &ascent, &descent, &leading);
        CGFloat yMin = floor(lineOrigin.y - descent);
        CGFloat yMax = ceil(lineOrigin.y + ascent);
        
        // Check if we've already passed the line
        if (point.y > yMax) {
            break;
        }
        // Check if the point is within this line vertically
        if (point.y >= yMin) {
            // Check if the point is within this line horizontally
            if (point.x >= lineOrigin.x && point.x <= lineOrigin.x + width) {
                // Convert CT coordinates to line-relative coordinates
                CGPoint relativePoint = CGPointMake(point.x - lineOrigin.x, point.y - lineOrigin.y);
                idx = CTLineGetStringIndexForPosition(line, relativePoint);
                
                break;
            }
        }
    }
    CFRelease(frame);
    CFRelease(path);
    
    return idx;
}

- (CGRect)textRect {
    
    CGRect textRect = [self.textLabel textRectForBounds:self.textLabel.bounds limitedToNumberOfLines:self.textLabel.numberOfLines];
    textRect.origin.y = (self.textLabel.bounds.size.height - textRect.size.height)/2;
    
    if (self.textLabel.textAlignment == NSTextAlignmentCenter) {
        textRect.origin.x = (self.textLabel.bounds.size.width - textRect.size.width)/2;
    }
    if (self.textLabel.textAlignment == NSTextAlignmentRight) {
        textRect.origin.x = self.textLabel.bounds.size.width - textRect.size.width;
    }
    
    return textRect;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Are You There View
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(UIView*)ruderView:(NSString *)type status:(NSString*)status
{
    UIImage*mapImage = [UIImage imageNamed:@"map-view.png"];
    UIView *ruderView  = [UIView new];
    ruderView.frame = CGRectMake(0, 0, 200, mapImage.size.height+ 5);
    ruderView.userInteractionEnabled = YES;
    ruderView.backgroundColor = [UIColor clearColor];
    
    UIButton *ruderFulBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    ruderFulBtn.frame = ruderView.frame;
    [ruderFulBtn setBackgroundColor:[UIColor clearColor]];
	[ruderFulBtn addTarget:self action:@selector(ruderViewBtnAction:event:) forControlEvents:UIControlEventTouchUpInside];
	[ruderView addSubview:ruderFulBtn];
    
    UIImageView *mapImageView = [UIImageView new];
    mapImageView.backgroundColor = [UIColor clearColor];
    mapImageView.image = mapImage;
    [ruderView addSubview:mapImageView];
    
    UILabel *ruderLabel = [UILabel new];
    ruderLabel.backgroundColor = [UIColor clearColor];
    ruderLabel.textColor = [UIColor darkGrayColor];
    ruderLabel.textAlignment = NSTextAlignmentCenter;
    ruderLabel.font = [UIFont fontWithName:FontString size:10];
    ruderLabel.text = @"Hi,r u there yet?";
    ruderLabel.userInteractionEnabled = YES;
    [ruderView addSubview:ruderLabel];
    
    UIButton *ruderViewBtn = [UIButton buttonWithType:UIButtonTypeCustom];
	[ruderViewBtn setTitle:status forState:UIControlStateNormal];
    [ruderViewBtn setBackgroundColor:[UIColor lightGrayColor]];
    ruderViewBtn.titleLabel.font = [UIFont fontWithName:FontString size:10.0f];
    ruderViewBtn.userInteractionEnabled = NO;
    [ruderViewBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[ruderView addSubview:ruderViewBtn];
    
    if ([type isEqualToString:@"BubbleTypeSomeoneElse"]) {
        mapImageView.frame = CGRectMake(ruderView.frame.size.width - (mapImage.size.width+6.5), ruderView.frame.origin.y+3,mapImage.size.width - 5, mapImage.size.height - 5);
        ruderLabel.frame = CGRectMake(ruderView.frame.size.width- (mapImage.size.width+6.5) -110,ruderView.frame.origin.y,ruderView.frame.size.width-100,15);
        ruderViewBtn.frame = CGRectMake(ruderView.frame.size.width-(mapImage.size.width+6.5) -110,ruderLabel.frame.origin.y+ruderLabel.frame.size.height+2,ruderView.frame.size.width-100,15);
        
        [ruderViewBtn setBackgroundColor:[UIColor whiteColor]];

    }
    else
    {
        mapImageView.frame = CGRectMake(ruderView.frame.origin.x+1, ruderView.frame.origin.y+ 6.5 ,mapImage.size.width - 5, mapImage.size.height - 5);
        ruderLabel.frame = CGRectMake(mapImageView.frame.origin.x+mapImageView.frame.size.width+10,ruderView.frame.origin.y,ruderView.frame.size.width-100,15);
        ruderViewBtn.frame = CGRectMake(mapImageView.frame.origin.x+mapImageView.frame.size.width+10,ruderLabel.frame.origin.y+ruderLabel.frame.size.height+2,ruderView.frame.size.width-100,15);
    }
    [mapImageView sizeToFit];
    return ruderView;
}

-(void)updateChatView
{
    [chatTableView reloadData];
}


-(UIView*)ruderView1:(NSString *)type status:(NSString*)status
{
    UIImage* RUTer_Bg_img;
    if ([type isEqualToString:@"BubbleTypeSomeoneElse"])
    {
        RUTer_Bg_img = [UIImage imageNamed:@"RUTerBgReceiver"];
    }
    else
    {
        RUTer_Bg_img = [UIImage imageNamed:@"RUTerBgSender"];
        
    }
    
    UIImageView* RUTer_Bg = [UIImageView new];
    RUTer_Bg.frame = CGRectMake(0, 0, RUTer_Bg_img.size.width, RUTer_Bg_img.size.height);
    RUTer_Bg.image = RUTer_Bg_img;
    return RUTer_Bg;
    
}
-(void)ruderViewBtnAction:(id)sender event:(id)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPos = [touch locationInView:chatTableView];
    NSIndexPath *indexPath = [chatTableView  indexPathForRowAtPoint:touchPos];
    if(indexPath != nil){
        UIBubbleTableViewCell *swipedCell  = (UIBubbleTableViewCell*)[chatTableView cellForRowAtIndexPath:indexPath];
//        NSLog(@"%@",swipedCell.data.view);
//        NSLog(@"%@",swipedCell.data.jidStr);
//        NSLog(@"%@",swipedCell.data.date);
//        NSLog(@"%@",swipedCell.data.ruderID);
        
        
        NSMutableDictionary* RuterDict = [DBEditer_obj fetchData_RUTER_ID:swipedCell.data.ruderID];
        
        [self pushToView_RUTer:RuterDict];
        
    }
    
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    UIImage *backButtonImage = [UIImage imageNamed:@"back_btn.png"];
    imagePickerBackButton = [UIButton buttonWithType:UIButtonTypeCustom];
    imagePickerBackButton.frame = CGRectMake(0, 0, backButtonImage.size.width, backButtonImage.size.height);
    [imagePickerBackButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
    [imagePickerBackButton addTarget:self action:@selector(popCurrentViewController) forControlEvents:UIControlEventTouchUpInside];
    [viewController.navigationController.navigationBar addSubview:imagePickerBackButton];
    viewController.navigationItem.hidesBackButton = YES;
    
    if ([NSStringFromClass([viewController class]) isEqualToString:@"PUUIAlbumListViewController" ])
        imagePickerBackButton.hidden = YES;
    else
        imagePickerBackButton.hidden = NO;
    
    imagePickerController = viewController;
}
-(void)popCurrentViewController
{
    [imagePickerBackButton removeFromSuperview];
    [imagePickerController.navigationController popViewControllerAnimated:YES];
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Method to access Media
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)takePhoto
{
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
      
        [[self appDelegate].statusBarView setHidden:YES];
        isCamOpened = YES;
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.delegate = self;
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        if( [UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceRear ])
        {
            controller.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        }
        else
        {
            controller.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        }
        
        
        [self presentViewController:controller animated:YES completion:NULL];
    }
    
}
- (void)takevideo
{
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [[self appDelegate].statusBarView setHidden:YES];
        isCamOpened = YES;
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.delegate = self;
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        controller.mediaTypes =  [NSArray arrayWithObject:(NSString *)kUTTypeMovie];
        controller.videoQuality = UIImagePickerControllerQualityTypeMedium;
        if( [UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceRear ])
        {
            controller.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        }
        else
        {
            controller.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        }
        
        [self presentViewController:controller animated:YES completion:NULL];
    }
    
}
-(void)addtowindow
{
    notiVw* noti = [[notiVw alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    
    AppDelegate* myDelegate = (((AppDelegate*) [UIApplication sharedApplication].delegate));
    [myDelegate.window addSubview:noti];
    
}
- (void)openPhotoAlbum
{
    isGalleryOpen = YES;
    
    
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.delegate = self;
    controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    controller.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie,(NSString *)kUTTypeImage, nil];
    [self presentViewController:controller animated:YES completion:NULL];
}



- (void)showCameraForBG
{
    isGalleryOpen = YES;

    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
    [[self appDelegate].statusBarView setHidden:YES];
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.delegate = self;
    controller.sourceType = UIImagePickerControllerSourceTypeCamera;
    controller.cameraDevice = UIImagePickerControllerCameraDeviceRear;
    controller.view.tag = 100;
    [self presentViewController:controller animated:YES completion:NULL];
    }
}

- (void)openPhotoAlbumForBG
{
    isGalleryOpen = YES;

    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.delegate = self;
    controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    controller.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeImage, nil];
    controller.view.tag = 100;
    [self presentViewController:controller animated:YES completion:NULL];
    
}
#pragma mark -

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    if ([buttonTitle isEqualToString:NSLocalizedString(@"Photo Album", nil)]) {
        [self openPhotoAlbumForBG];
        
    } else if ([buttonTitle isEqualToString:NSLocalizedString(@"Camera", nil)]) {
        [self showCameraForBG];
    }
    else if ([buttonTitle isEqualToString:NSLocalizedString(@"Set Default", nil)])
    {
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:[NSString stringWithFormat:@"%@_BG",user.jidStr]];
        self.view.backgroundColor = CHAT_BACKGROUNDIMAGE;

    }
}






- (void)setBackground
{
    isGalleryOpen = YES;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Change your chat background"
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Camera", nil)];
    }
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Photo Album", nil)];

    [actionSheet addButtonWithTitle:NSLocalizedString(@"Set Default", nil)];
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    actionSheet.cancelButtonIndex = actionSheet.numberOfButtons - 1;
    
    
    [actionSheet showFromToolbar:self.navigationController.toolbar];
    
}

-(NSMutableDictionary*)generateRuterDict
{
    NSMutableDictionary* ruterDict =   [NSMutableDictionary new];
    [ruterDict setObject:@"iAmSender" forKey:@"YES"];
    [ruterDict setObject:@"isActivated" forKey:@"NO"];
    [ruterDict setObject:[self getSenderDict] forKey:@"Sender"];
    [ruterDict setObject:[self getReceiverAry:[NSMutableArray arrayWithObjects:[self removeDomainName_String:user.jidStr],nil]] forKey:@"Receiver"];
    [ruterDict setObject:@"1.5" forKey:@"range"];
    
    return ruterDict;
}

-(NSMutableDictionary*)getSenderDict
{
    NSMutableDictionary* senderDict =   [NSMutableDictionary new];
    [senderDict setObject:@"iS_Sender" forKey:@"YES"];
    [senderDict setObject:@"id" forKey:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME]];
    
    return senderDict;
    
}

-(NSMutableArray*)getReceiverAry:(NSMutableArray*)ids
{
    NSMutableArray* ReceiverAry =   [NSMutableArray new];
    
    for (NSString* str in ids)
    {
        NSMutableDictionary* ReceiverDict =   [NSMutableDictionary new];
        [ReceiverDict setObject:@"NO" forKey:@"iS_Sender"];
        [ReceiverDict setObject:str forKey:@"id"];
        [ReceiverAry addObject:ReceiverDict];
    }
    
    
    return ReceiverAry;
}
#pragma mark - remove DomainName from String

-(NSString*)removeDomainName_String:(NSString*)string
{
    return [string stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"@%@",DOMAINAME] withString:@""];
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Image Picker delegate Method
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////



- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    if (picker.view.tag == 100)
    {
        
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        [picker dismissViewControllerAnimated:YES completion:^{
            [self openEditor:image];
        }];
        
    }
    else
    {
        
//    NSString *mediaType = [info valueForKey:UIImagePickerControllerMediaType];
    
//    if([mediaType isEqualToString:(NSString*)kUTTypeImage])
//    {
//        UIImage *photoTaken = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
//        //Save Photo to library only if it wasnt already saved i.e. its just been taken
//        if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
//            UIImageWriteToSavedPhotosAlbum(photoTaken, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
//        }
//}
    
    

    
    
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    if( [picker sourceType] == UIImagePickerControllerSourceTypeCamera )
    {
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        [library writeImageToSavedPhotosAlbum:image.CGImage orientation:(ALAssetOrientation)image.imageOrientation completionBlock:^(NSURL *assetURL, NSError *error )
         {
           //  NSLog(@"IMAGE SAVED TO PHOTO ALBUM");
             [library assetForURL:assetURL resultBlock:^(ALAsset *asset )
              {
                 // NSLog(@"we have our ALAsset!");
              }
                     failureBlock:^(NSError *error )
              {
                //  NSLog(@"Error loading asset");
              }];
         }];
    }
    
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:^
    {
       
        NSString* pathStr;
        
        NSURL *mURL = [info objectForKey:UIImagePickerControllerMediaURL];
        
        
        [self createMediaFolder:@"mediaFile"];
        if (mURL == NULL)
        {
            /*
            isImageEditorOpend = YES;
            menu.shouldShow = NO;
            imgEdit = [[editImageView alloc]initWithFrame_onView:self.view image:[info objectForKey:UIImagePickerControllerOriginalImage]];
            [imgEdit projectImageForEdit];
            
            */
            
            NSString* path = [self getUniqueFilenameInFolder:@"mediaFile" forFileExtension:@"jpg"];
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *newFilePath = [[paths lastObject] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",path]];
            
            //>=-- Fix orientation and Save as data ------=<//
            UIImage* imag = [UIImage imageWithImage:[info objectForKey:UIImagePickerControllerOriginalImage] scaledToWidth:self.view.frame.size.width];
            NSData *imageData = UIImageJPEGRepresentation(imag, 0.7);
            
            
            
            if (imageData != nil) {
                [imageData writeToFile:newFilePath atomically:YES];
            }
            
            NSLog(@"imageData.length    %lu",(unsigned long)imageData.length);
            
            pathStr = [NSString stringWithString:newFilePath];
         //   NSLog(@"pathStr : %@",pathStr);
            
          //  NSString *imageStr = [NSString fontAwesomeIconStringForEnum:FACamera];
            NSString *appendingStr = [NSString stringWithFormat:@"📷 Image"];
            
            NSString* msgId = [self sendVirtualMessage:appendingStr:nil];
            
            if (msgId)
            {
                [self mediaDbEditor_msgId:msgId fileName:@"image" url:pathStr];
                [self postPhoto:pathStr:msgId fileType:@"image" ];
            }
            else
            {
             //   NSLog(@"Failed to generate msg id");
            }
             
        }
        else
        {
            NSString* path = [self getUniqueFilenameInFolder:@"mediaFile" forFileExtension:[[mURL.path componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"."]] lastObject]];
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *newFilePath = [[paths lastObject] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",path]];
            
            NSURL* urlStr = [NSURL fileURLWithPath:newFilePath];
            pathStr = newFilePath;
            [self convertVideoToLowQuailtyWithInputURL:mURL outputURL:urlStr handler:^(AVAssetExportSession *exportSession)
             {
                 if (exportSession.status == AVAssetExportSessionStatusCompleted)
                 {
                     NSLog(@"completed\n %@",newFilePath);
                    // NSString *imageStr = [NSString fontAwesomeIconStringForEnum:FACamera];
                     NSString *appendingStr = [NSString stringWithFormat:@"📹 Video"];
                     
                     NSString* msgId = [self sendVirtualMessage:appendingStr:nil];
                     
                     if (msgId)
                     {
                         [self mediaDbEditor_msgId:msgId fileName:@"video" url:pathStr];
                         
                         [self postPhoto:newFilePath:msgId fileType:@"video"];
                     }
                     else
                     {
                       //  NSLog(@"Failed to generate msg id");
                     }
                     
                 }
                 else
                 {
                     printf("error\n");
                     
                 }
             }];
        }
        
        
    }];
    }
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    UIAlertView *alert;
    //NSLog(@"Image:%@", image);
    if (error) {
        alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                           message:[error localizedDescription]
                                          delegate:nil
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil];
        [alert show];
    }
    
}




- (void)openEditor:(UIImage*)img
{
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.image = img;
    
  

    controller.cropAspectRatio =  9.0f / 16.0f;
    
    
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    
    isGalleryOpen = YES;

    [self presentViewController:navigationController animated:YES completion:NULL];
}


#pragma mark -

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    UIImage* imageScaled = [UIImage imageWithImage:croppedImage scaledToSize:self.view.frame.size];

    [self.view setBackgroundColor:[UIColor colorWithPatternImage:imageScaled]];
    
    [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(imageScaled) forKey:[NSString stringWithFormat:@"%@_BG",user.jidStr]];
    

    
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
  
    [controller dismissViewControllerAnimated:YES completion:NULL];
}



-(void)mediaDbEditor_msgId:(NSString*)msgId fileName:(NSString*)fileName url:(NSString*)url
{
    
    
    mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
    NSMutableDictionary* dict = [NSMutableDictionary new];
    [dict setObject:msgId forKey:@"msgId"];
    [dict setObject:user.jidStr forKey:@"userId"];
    [dict setObject:fileName forKey:@"fileName"];
    [dict setObject:url forKey:@"url"];
    [dict setObject:[NSNumber numberWithBool:NO] forKey:@"loc_cloud"];
    [dict setObject:[NSNumber numberWithBool:NO] forKey:@"sent_received"];
    [dict setObject:@"0" forKey:@"success_failure"];
    [dict setObject:[NSNumber numberWithBool:NO] forKey:@"sender_receiver"];
    [dict setObject:[NSNumber numberWithBool:NO] forKey:@"isPrivate"];
    
    [dict setObject:[NSNumber numberWithFloat:0.0] forKey:@"percentLoaded"];
    [dict setObject:fileName forKey:@"video_img"];
    
    
    
    [mediaDb checkAndInsert:dict];
    
    
    //loc_cloud        0 --> localUrl     1 -- > webUrl
    //sent_received    0 --> Sent         1 -- > Received
    //success_failure  0 --> pending      1 -- > uploading/downloading  2 --> success  3 --> failure
    //sender_receiver  0 --> sender       1 -- > receiver
    //video_img        0 --> video        1 -- > img
    
    
}

-(NSBubbleData*)constructMediaViewBubledata:(XMPPMessageArchiving_Message_CoreDataObject *)message indexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *mediaDict =  [[mediaDbEditor sharedInstance] fetchMedia:message.messageID];
    NSBubbleData *Bubbledata;
    
    if (![[mediaDict objectForKey:@"sent_received"] boolValue])
    {
        if ([self.imageCache objectForKey:message.messageID]) {
            
            Bubbledata = [NSBubbleData dataWithView:[self PhotoViewForBubbleData:[self.imageCache objectForKey:message.messageID] status:[mediaDict objectForKey:@"success_failure"] isSender:YES msgId:[mediaDict objectForKey:@"msgId"]] date:message.timestamp type:BubbleTypeBeeMine insets:UIEdgeInsetsMake(5.0, 0.0, 3.0, 5.0) jidString:user.jidStr RuderID:nil deliveryStr:message.delivery chattype:@"singlechat" isSender:YES messageID:[mediaDict objectForKey:@"msgId"]];
        }
        else
        {//placeholder
            Bubbledata = [NSBubbleData dataWithView:[self PhotoViewForBubbleData:[UIImage imageNamed:@"placeholder"] status:[mediaDict objectForKey:@"success_failure"] isSender:YES msgId:[mediaDict objectForKey:@"msgId"]] date:message.timestamp type:BubbleTypeBeeMine insets:UIEdgeInsetsMake(5.0, 0.0, 3.0, 5.0) jidString:user.jidStr RuderID:nil deliveryStr:message.delivery chattype:@"singlechat" isSender:YES messageID:[mediaDict objectForKey:@"msgId"]];
            
            if ([[mediaDict objectForKey:@"video_img"] isEqualToString:@"video"])
                [self constructImageFromVidUrl:[mediaDict objectForKey:@"url"] Indexpath:indexPath MsgID:message.messageID];
            else
                [self imageCache:[self getFullDocumentUrl:[mediaDict objectForKey:@"url"]] indexPath:indexPath MsgID:message.messageID];
        }
    }
    else
    {
        if ([[mediaDict objectForKey:@"video_img"] isEqualToString:@"video"])
        {
            if ([[mediaDict objectForKey:@"success_failure"] isEqualToString:@"0"] || [[mediaDict objectForKey:@"success_failure"] isEqualToString:@"3"] || [[mediaDict objectForKey:@"success_failure"] isEqualToString:@"1"])
            {
                UIImage* image = [self decodeBase64ToImage:[mediaDict objectForKey:@"imgData"] Indexpath:indexPath MsgID:message.messageID];
                
                Bubbledata= [NSBubbleData dataWithView:[self PhotoViewForBubbleData:image status:[mediaDict objectForKey:@"success_failure"] isSender:NO msgId:[mediaDict objectForKey:@"msgId"]] date:message.timestamp type:BubbleTypeBeeSomeoneElse insets:UIEdgeInsetsMake(0.0, 5.0, 3.0, 3.0) jidString:user.jidStr RuderID:nil deliveryStr:message.delivery chattype:@"singlechat" isSender:NO messageID:[mediaDict objectForKey:@"msgId"]];
            }
            else
            {
                if ([self.imageCache objectForKey:message.messageID])
                {
                    
                    Bubbledata= [NSBubbleData dataWithView:[self PhotoViewForBubbleData:[self.imageCache objectForKey:message.messageID] status:[mediaDict objectForKey:@"success_failure"] isSender:NO msgId:[mediaDict objectForKey:@"msgId"]] date:message.timestamp type:BubbleTypeBeeSomeoneElse insets:UIEdgeInsetsMake(0.0, 5.0, 3.0, 3.0) jidString:user.jidStr RuderID:nil deliveryStr:message.delivery chattype:@"singlechat" isSender:NO messageID:[mediaDict objectForKey:@"msgId"]];
                }
                else
                {
                    Bubbledata= [NSBubbleData dataWithView:[self PhotoViewForBubbleData:[UIImage imageNamed:@"placeholder"] status:[mediaDict objectForKey:@"success_failure"] isSender:NO msgId:[mediaDict objectForKey:@"msgId"]] date:message.timestamp type:BubbleTypeBeeSomeoneElse insets:UIEdgeInsetsMake(0.0, 5.0, 3.0, 3.0) jidString:user.jidStr RuderID:nil deliveryStr:message.delivery chattype:@"singlechat" isSender:NO messageID:[mediaDict objectForKey:@"msgId"]];
                    
                    [self constructImageFromVidUrl:[mediaDict objectForKey:@"url"] Indexpath:indexPath MsgID:message.messageID];
                }
            }
        }
        else
        {
            if ([[mediaDict objectForKey:@"success_failure"] isEqualToString:@"0"] || [[mediaDict objectForKey:@"success_failure"] isEqualToString:@"3"] || [[mediaDict objectForKey:@"success_failure"] isEqualToString:@"1"])
            {
                UIImage* image = [self decodeBase64ToImage:[mediaDict objectForKey:@"imgData"] Indexpath:indexPath MsgID:message.messageID];
                
                Bubbledata= [NSBubbleData dataWithView:[self PhotoViewForBubbleData:image status:[mediaDict objectForKey:@"success_failure"] isSender:NO msgId:[mediaDict objectForKey:@"msgId"]] date:message.timestamp type:BubbleTypeBeeSomeoneElse insets:UIEdgeInsetsMake(0.0, 5.0, 3.0, 3.0) jidString:user.jidStr RuderID:nil deliveryStr:message.delivery chattype:@"singlechat" isSender:NO messageID:[mediaDict objectForKey:@"msgId"]];
            }
            else
            {
                if ([self.imageCache objectForKey:message.messageID])
                {
                    Bubbledata= [NSBubbleData dataWithView:[self PhotoViewForBubbleData:[self.imageCache objectForKey:message.messageID] status:[mediaDict objectForKey:@"success_failure"] isSender:NO msgId:[mediaDict objectForKey:@"msgId"]] date:message.timestamp type:BubbleTypeBeeSomeoneElse insets:UIEdgeInsetsMake(0.0, 5.0, 3.0, 3.0) jidString:user.jidStr RuderID:nil deliveryStr:message.delivery chattype:@"singlechat" isSender:NO messageID:[mediaDict objectForKey:@"msgId"]];
                }
                else
                {
                    Bubbledata= [NSBubbleData dataWithView:[self PhotoViewForBubbleData:[UIImage imageNamed:@"placeholder"] status:[mediaDict objectForKey:@"success_failure"] isSender:NO msgId:[mediaDict objectForKey:@"msgId"]] date:message.timestamp type:BubbleTypeBeeSomeoneElse insets:UIEdgeInsetsMake(0.0, 5.0, 3.0, 3.0) jidString:user.jidStr RuderID:nil deliveryStr:message.delivery chattype:@"singlechat" isSender:NO messageID:[mediaDict objectForKey:@"msgId"]];
                    
                    [self imageCache:[self getFullDocumentUrl:[mediaDict objectForKey:@"url"]] indexPath:indexPath MsgID:message.messageID];
                }
            }
        }
    }
    mediaDict = nil;
    return Bubbledata;
    
}
//-->Get the Image And Store in NSCache (self.imageCache)
-(void)imageCache:(NSString*)imageUrlString indexPath:(NSIndexPath*)indexPath MsgID:(NSString *)msgID
{
    __weak chatViewController *chatClass = self;
    [self.imageDownloadingQueue addOperationWithBlock:^{
        NSURL *imageUrl   = [NSURL fileURLWithPath:imageUrlString];
        NSData *imageData = [NSData dataWithContentsOfURL:imageUrl];
        UIImage *image    = nil;
        if (imageData)
            image = [UIImage imageWithData:imageData];
        if (image)
        {
            // add the image to your cache
            [self.imageCache setObject:image forKey:msgID];
            // finally, update the user interface in the main queue
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [chatClass reloadChatTableView:indexPath];
            }];
        }
    }];
    
}
//-->Get the Image from video And Store in NSCache (self.imageCache)
-(UIImage *)constructImageFromVidUrl:(NSString *)url
{
    
    NSURL* sourceURL = [NSURL fileURLWithPath:[self getFullDocumentUrl:url]];
    AVAsset *asset = [AVAsset assetWithURL:sourceURL];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    imageGenerator.appliesPreferredTrackTransform=TRUE;
    CMTime time = CMTimeMake(1, 1);
    NSError *err = NULL;

    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:&err];
    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
    return thumbnail;
}

- (NSString *)getFullDocumentUrl:(NSString *)fileName
{
    
    NSRange range = [fileName rangeOfString:@"Documents"];
    
    NSString *newString = [fileName substringFromIndex:range.location+9];
   // NSLog(@"new string %@",newString);
    
    return [NSString stringWithFormat:@"%@/%@",[self applicationDocumentsDirectory],newString];
}

- (NSString *)applicationDocumentsDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}


-(void)constructImageFromVidUrl:(NSString*)url Indexpath:(NSIndexPath *)indexPath MsgID:(NSString *)msgID
{
    __weak chatViewController *chatClass = self;
    [self.imageDownloadingQueue addOperationWithBlock:^{
        
        if([chatClass constructImageFromVidUrl:url] != nil)
        {
        [self.imageCache setObject:[chatClass constructImageFromVidUrl:url] forKey:msgID];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [chatClass reloadChatTableView:indexPath];
        }];
        }
    }];
}

//--> Decode base 64 Image data to UIImage
- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData Indexpath:(NSIndexPath *)indexPath MsgID:(NSString *)msgID
{
    //    __weak chatViewController *chatClass = self;
    //    [self.imageDownloadingQueue addOperationWithBlock:^{
    //    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    //        [self.imageCache setObject:[UIImage imageWithData:data] forKey:msgID];
    //        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
    //            [chatClass reloadChatTableView:indexPath];
    //                    }];
    //    }];
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}
//-->Reload Chat view
-(void)reloadChatTableView:(NSIndexPath *)indexPath
{
    NSArray *visibleCellsArray = [chatTableView indexPathsForVisibleRows];
    if ([visibleCellsArray containsObject:indexPath]) {
        [chatTableView beginUpdates];
        [chatTableView reloadRowsAtIndexPaths:@[indexPath]withRowAnimation:UITableViewRowAnimationNone];
        [chatTableView endUpdates];
    }
}



-(NSString*)sendVirtualMessage:(NSString*)str :(NSString*)msgId
{
    NSString* messageId;
    if (msgId == nil)
    {
        messageId=[self.xmppStream generateUUID];
    }
    else
    {
        messageId = msgId;
    }
    
    
    
    NSXMLElement *media = [NSXMLElement elementWithName:@"file"];
    
    XMPPMessage *groupInviteMessage = [XMPPMessage new];
    [groupInviteMessage addAttributeWithName:@"id"  stringValue:messageId];
    [groupInviteMessage addAttributeWithName:@"to" stringValue:user.jidStr];
    [groupInviteMessage addAttributeWithName:@"from" stringValue:[self xmppStream].myJID.bare];
    [groupInviteMessage addAttributeWithName:@"type" stringValue:@"chat"];
    NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
    [body setStringValue:str];
    [groupInviteMessage addChild:body];
    [groupInviteMessage addChild:media];
    
    if (isPrivateChat) {
        
        NSXMLElement *private = [NSXMLElement elementWithName:@"private"];
        [private addAttributeWithName:@"xmlns" stringValue:@"private"];
        [private addAttributeWithName:@"type" stringValue:@"message"];
        [groupInviteMessage addChild:private];
        
    }

    if (msgId == nil)
    {
        
        [[self xmppMessageArchivingCoreDataStorage ] sendingVirtualMediaMessage:groupInviteMessage XmppStream:[self xmppStream]];
    }
    else
    {
        [self.xmppStream sendElement:groupInviteMessage];
        
    }
    
    return messageId;
}
-(void)deleteFileAtPath:(NSString*)pathStr
{
    NSError *error;
    if ([[NSFileManager defaultManager] isDeletableFileAtPath:pathStr]) {
        BOOL success = [[NSFileManager defaultManager] removeItemAtPath:pathStr error:&error];
        if (!success) {
         //   NSLog(@"Error removing file at path: %@", error.localizedDescription);
        }
        else
        {
         //   NSLog(@"Successfully removed file at path");
        }
    }
}
-(void)createMediaFolder:(NSString*)folderName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"mediaFile"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath]){
        
        NSError* error;
        if(  [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error])
        {
            
        }
        
        else
        {
            NSLog(@"[%@] ERROR: attempting to write create MyFolder directory", [self class]);
            NSAssert( FALSE, @"Failed to create directory maybe out of disk space?");
        }
    }
}
-(NSString *)getUniqueFilenameInFolder:(NSString *)folder forFileExtension:(NSString *)fileExtension {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *existingFiles = [fileManager contentsOfDirectoryAtPath:folder error:nil];
    NSString *uniqueFilename;
    
    do {
        CFUUIDRef newUniqueId = CFUUIDCreate(kCFAllocatorDefault);
        CFStringRef newUniqueIdString = CFUUIDCreateString(kCFAllocatorDefault, newUniqueId);
        
        uniqueFilename = [[folder stringByAppendingPathComponent:(__bridge NSString *)newUniqueIdString] stringByAppendingPathExtension:fileExtension];
        
        CFRelease(newUniqueId);
        CFRelease(newUniqueIdString);
    } while ([existingFiles containsObject:uniqueFilename]);
    
    return uniqueFilename;
}



- (NSString *)encodeToBase64String:(NSData *)imageData
{
    return [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

-(void)postPhoto :(NSString*)pathStr :(NSString*)msgId fileType:(NSString*)fileType
{
    
    NSMutableArray* ary = [NSMutableArray new];
    [ary addObject:user.jidStr];
    
    RMDownloadIndicator* uploadIndicator =   [self addDownloadIndicators];
    self.singleton = [Singleton sharedMySingleton];
    [self.singleton.mediaFileIndicatorDict setObject:uploadIndicator forKey:msgId];
    [chatTableView reloadData];
    
    NSMutableDictionary *paramMutable = [[NSMutableDictionary alloc] init];
    [paramMutable setObject:[self xmppStream].myJID.bare forKey:@"sender_id"];
    [paramMutable setObject:ary forKey:@"receiver_id"];
    
    NSMutableDictionary *rParam = [[NSMutableDictionary alloc] init];
    [rParam setObject:@"mediaFile" forKey:@"functionName"];
    [rParam setObject:paramMutable forKey:@"Parameter"];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:rParam options:NSJSONWritingPrettyPrinted error:&error];
    
    AFHTTPClient* httpClient = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:FILE_UPLOAD]];
    
    
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    
    NSMutableURLRequest *afRequest = [httpClient multipartFormRequestWithMethod:@"POST" path:@"" parameters:nil constructingBodyWithBlock:^(id <AFMultipartFormData>formData)
                                      {
                                          if (pathStr != NULL)
                                          {
                                              //> Media is attached
                                              NSURL *mUrl = [NSURL URLWithString:pathStr];
                                              
                                              //>=-  IF URL IS EMPTY MAKE IT AS COMPLETE FILE PATH -=<//
                                              if (mUrl == NULL) {
                                                  mUrl = [NSURL fileURLWithPath:pathStr];
                                              }
                                              if ([[NSString stringWithFormat:@"%@",pathStr] hasPrefix:@"/"]) {
                                                  mUrl = [NSURL fileURLWithPath:pathStr];
                                              }
                                              
                                              if (mUrl != NULL)
                                              {
                                                  NSArray *extentionAry = [[mUrl lastPathComponent] componentsSeparatedByString:@"."];
                                                  // NSLog(@"extentionAry : %@", extentionAry);
                                                  
                                                  NSString *mimeType;
                                                  if ([[extentionAry lastObject] caseInsensitiveCompare:@"MOV"]==NSOrderedSame)
                                                      mimeType = @"video/mov";
                                                  else if ([[extentionAry lastObject] caseInsensitiveCompare:@"MP4"]==NSOrderedSame)
                                                      mimeType = @"video/mp4";
                                                  else if ([[extentionAry lastObject] caseInsensitiveCompare:@"3GP"]==NSOrderedSame)
                                                      mimeType = @"video/3gp";
                                                  else if ([[extentionAry lastObject] caseInsensitiveCompare:@"PNG"]==NSOrderedSame)
                                                      mimeType = @"image/png";
                                                  else if ([[extentionAry lastObject] caseInsensitiveCompare:@"JPG"]==NSOrderedSame)
                                                  {
                                                      mimeType = @"image/jpg";
                                                  }
                                                  else if ([[extentionAry lastObject] caseInsensitiveCompare:@"JPEG"]==NSOrderedSame)
                                                  {
                                                      mimeType = @"image/jpeg";
                                                  }
                                                  
                                                  if (mimeType!=NULL) {
                                                      
                                                      NSLog(@"mUrl : %@", mUrl);
                                                      NSLog(@"mimeType : %@", mimeType);
                                                      // NSLog(@"mimeType : %@", [mUrl lastPathComponent]);
                                                      
                                                      [formData appendPartWithFileURL:mUrl name:@"Path" fileName:[mUrl lastPathComponent] mimeType:mimeType error:nil];
                                                      
                                                  }
                                                  
                                              }
                                              
                                          }
                                          
                                          
                                          [formData appendPartWithFormData:jsonData name:@"Request"];
                                          NSLog(@"formData  %@",formData);
                                          
                                      }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:afRequest];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten,long long totalBytesWritten,long long totalBytesExpectedToWrite){
        
        [self updateView:((float)totalBytesWritten/ totalBytesExpectedToWrite)*100 indicatorView:uploadIndicator];
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         
         
         NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         responseStr = [responseStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
       //  NSLog(@"Response Str : %@",responseStr);
         
         NSError *error = nil;
         id objectsFromJson = [NSJSONSerialization JSONObjectWithData:[responseStr dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&error];
         
         if (!error) {
             NSLog(@"Success");
             //> NSLog(@"Object from Json : %@",objectsFromJson);
             
             if ([[objectsFromJson allKeys] containsObject:@"ResponseMsg"]) {
                 if ([[objectsFromJson valueForKey:@"ResponseMsg"] caseInsensitiveCompare:@"Success"] == NSOrderedSame)
                 {
                     UIImage* previewImg;
                     if ([fileType isEqualToString:@"video"])
                     {
                         previewImg = [self constructImageFromVidUrl:pathStr];
                     }
                     else
                     {
                         previewImg = [UIImage imageWithContentsOfFile:pathStr];
                     }
                     
                     UIImage* imag = [UIImage imageWithImage:previewImg scaledToWidth:30];;
                     NSData *imageData_30 = UIImageJPEGRepresentation(imag, 0.1);
                     
                     [self isUploadingMsgId:msgId success:@"2"];
                     
                     [self sendMediaMsg:[objectsFromJson valueForKey:@"UrlString"] file_id:[objectsFromJson valueForKey:@"FileId"] file_type:fileType file_size:[objectsFromJson valueForKey:@"FileSize"] file_preview:[self encodeToBase64String:imageData_30] msgId:msgId];
                     
                 }
                 else {
                     [self isUploadingMsgId:msgId success:@"3"];
                 }
                 
                 
             }
         }
         else {
             NSLog(@"Error : %@",error);
             [self isUploadingMsgId:msgId success:@"3"];
             
             [[[UIAlertView alloc] initWithTitle:ALERTVIEW_TITLE message:@"\u26A0 Invalid response from server." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
         }
         [self.singleton.mediaFileIndicatorDict removeObjectForKey:msgId];
         [chatTableView reloadData];
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         
         [self isUploadingMsgId:msgId success:@"3"];
         [self.singleton.mediaFileIndicatorDict removeObjectForKey:msgId];
         [chatTableView reloadData];
         
         
    //     NSLog(@"error : %@",  operation.responseString);
         
         [[[UIAlertView alloc] initWithTitle:ALERTVIEW_TITLE message:@"\u26A0 Invalid response from server." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
     }];
    [operation start];
    
}



-(void)sendMediaMsg:(NSString*)file_url file_id:(NSString*)file_id file_type:(NSString*)file_type file_size:(NSString*)file_size file_preview:(NSString*)file_preview msgId:(NSString*)msgId
{
    NSString *messageStr = file_type;
    if([messageStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length > 0) {
        
        NSString *messageID=msgId;
        
        NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
        [body setStringValue:messageStr];
        
        
        
        
        NSXMLElement *file = [NSXMLElement elementWithName:@"file"];
        [file addAttributeWithName:@"xmlns" stringValue:@"shaili:file"];
        [file addAttributeWithName:@"file_url" stringValue:file_url];
        [file addAttributeWithName:@"file_id" stringValue:file_id];
        [file addAttributeWithName:@"file_type" stringValue:file_type];
        [file addAttributeWithName:@"file_size" stringValue:file_size];
        
        [file addAttributeWithName:@"from" stringValue:[[self xmppStream] myJID].bare];
        
        NSXMLElement *filepreview = [NSXMLElement elementWithName:@"file_preview"];
        [filepreview setStringValue:file_preview];
        [file addChild:filepreview];
        
        NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
        [message addAttributeWithName:@"id" stringValue:messageID];
        [message addAttributeWithName:@"type" stringValue:@"chat"];
        [message addAttributeWithName:@"to" stringValue:[NSString stringWithFormat:@"%@",user.jidStr]];
        [message addChild:body];
        [message addChild:file];
        if (isPrivateChat) {
            
            NSXMLElement *private = [NSXMLElement elementWithName:@"private"];
            [private addAttributeWithName:@"xmlns" stringValue:@"private"];
            [private addAttributeWithName:@"type" stringValue:@"message"];
            [message addChild:private];
            
            
        }
        
        [self.xmppStream sendElement:message];
        
        if (isPrivateChat)
        {
            mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
            NSMutableDictionary* dict = [NSMutableDictionary new];
            [dict setObject:messageID  forKey:@"msgId"];
            [dict setObject:[NSNumber numberWithBool:YES] forKey:@"isPrivate"];
            [mediaDb updateMedia:dict];
            
        }
        
    }
}

-(void)isUploadingMsgId:(NSString*)msgId success:(NSString*)success
{
    mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
    
    NSMutableDictionary* dict = [NSMutableDictionary new];
    [dict setObject:msgId forKey:@"msgId"];
    [dict setObject:success forKey:@"success_failure"];
    [mediaDb checkAndInsert:dict];
    
}

-(void)isNotDownloaded:(NSString*)msgId success:(NSString*)success servicePath:(NSString *)serviceFilePath
{
    mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
    
    NSMutableDictionary* dict = [NSMutableDictionary new];
    [dict setObject:msgId forKey:@"msgId"];
    [dict setObject:success forKey:@"success_failure"];
    [dict setObject:serviceFilePath forKey:@"url"];
    [mediaDb checkAndInsert:dict];
    
}
- (RMDownloadIndicator*)addDownloadIndicators
{
    
    
    RMDownloadIndicator *filledIndicator = [[RMDownloadIndicator alloc]initWithFrame:CGRectMake(2, 2, widthOfBlackTransVw - 4, widthOfBlackTransVw - 4) type:kRMClosedIndicator];
    [filledIndicator setBackgroundColor:[UIColor clearColor]];
    [filledIndicator setFillColor:RGBA(255, 255, 255, 1)];
    [filledIndicator setStrokeColor:RGBA(34, 160, 246, 1)];
    filledIndicator.radiusPercent = 0.91;
    filledIndicator.coverWidth = 3;
    [filledIndicator loadIndicator];
    
    UILabel* percentLbl = [UILabel new];
    percentLbl.frame = CGRectMake(0, (filledIndicator.frame.size.width - 30)/2, filledIndicator.frame.size.width, 30);
    percentLbl.backgroundColor = [UIColor clearColor];
    percentLbl.textColor = [UIColor whiteColor];
    percentLbl.font = [UIFont fontWithName:FontString size:12];
    percentLbl.textAlignment = NSTextAlignmentCenter;
    [filledIndicator addSubview:percentLbl];
    
    return filledIndicator;
    
}

- (void)updateView:(CGFloat)val indicatorView:(RMDownloadIndicator*)indicatorView
{
    for (UILabel* lbl in [indicatorView subviews])
    {
        lbl.text = [NSString stringWithFormat:@"%.00f",val];
    }
    [indicatorView updateWithTotalBytes:100 downloadedBytes:val];
    
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Action for sending Sticker
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(void)SendBeeMsg:(NSString*)beeStr
{
    NSString *messageStr = beeStr;
    
    
    if([messageStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0) {
        if (!isPrivateChat) {
            [messageMethods textmessage:messageStr jid:user.jidStr];
            
        }
        else
        {
            NSString *messageId = [[self xmppStream] generateUUID];
            XMPPMessage *privateMessage = [XMPPMessage new];
            [privateMessage addAttributeWithName:@"id"  stringValue:messageId];
            [privateMessage addAttributeWithName:@"to" stringValue:user.jidStr];
            [privateMessage addAttributeWithName:@"from" stringValue:[self xmppStream].myJID.bare];
            [privateMessage addAttributeWithName:@"type" stringValue:@"chat"];
            NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
            [body setStringValue:messageStr];
            [privateMessage addChild:body];
            NSXMLElement *private = [NSXMLElement elementWithName:@"private"];
            [private addAttributeWithName:@"xmlns" stringValue:@"private"];
            [private addAttributeWithName:@"type" stringValue:@"message"];
            [privateMessage addChild:private];
            [[self xmppStream] sendElement:privateMessage];
            
        }
        
    }
    
}


-(void)sendVirtualMessageForRuthereReached:(NSString *)RUTer_Id statusStr:(NSString *)rutherStatus
{
    DBEditer* dbEditer_Obj = [DBEditer new];
    
    NSMutableDictionary* RuterDict = [dbEditer_Obj fetchData_RUTER_ID:RUTer_Id];
    
    
    if ([rutherStatus isEqualToString:@"2"])
    {
        NSString *messageID=[self.xmppStream generateUUID];
        
        NSString *roomID = [NSString stringWithFormat:@"%@@shaili",[RuterDict valueForKey:@"from_Id"]];
        
        
        NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
        [body setStringValue:@"Are you there reached destination"];
        NSXMLElement *ruthere = [NSXMLElement elementWithName:@"ruthere"];
        [ruthere addAttributeWithName:@"xmlns" stringValue:@"ruthere"];
        [ruthere addAttributeWithName:@"ruthere_id" stringValue:[RuterDict valueForKey:@"RUTer_Id"]];
        [ruthere addAttributeWithName:@"type" stringValue:@"response"];
        [ruthere addAttributeWithName:@"from" stringValue:user.jidStr];
        [ruthere addAttributeWithName:@"accepted" stringValue:@"true"];
        
        XMPPMessage *message = [XMPPMessage new];
        [message addAttributeWithName:@"id" stringValue:messageID];
        [message addAttributeWithName:@"type" stringValue:@"groupchat"];
        [message addAttributeWithName:@"to" stringValue:roomID];
        [message addChild:body];
        [message addChild:ruthere];
        
        [[self xmppMessageArchivingCoreDataStorage ] sendingVirtualMediaMessage:message XmppStream:[self xmppStream]];

    }
    
    
    
    
}




-(NSString*)stringForStatus:(NSString*)status
{
    
    if ([status isEqualToString:@"0"]) {
        return @"Pending";
    }
    else if ([status isEqualToString:@"1"]) {
        return @"Active";
    }
    else if ([status isEqualToString:@"2"]) {
        
        
        return @"Reached";
    }
    else if ([status isEqualToString:@"3"]) {
        return @"Rejected";
    }
    else if ([status isEqualToString:@"4"])
    {
        return @"Cancelled";
    }
    else
    {
        return @"UNKNOWN";
    }
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Gesture Methods
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)swipeGestureAction:(UISwipeGestureRecognizer *)gesture
{
    if(!isEditMode)
    {
        if (gesture.direction == UISwipeGestureRecognizerDirectionRight)
            isMenuShowing = YES;
        else if(gesture.direction == UISwipeGestureRecognizerDirectionLeft)
            isMenuShowing = NO;
        chatTableView.scrollEnabled = NO;
        [self menuButtonAction:nil];
    }
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Chat Slide Menu
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)menuButtonAction:(id)sender
{
    if (isMenuShowing)
    {

        
        [UIView animateWithDuration:1.0 delay:0.0 usingSpringWithDamping:0.5 initialSpringVelocity:0.7 options:0 animations:^{
          self.chatSlideMenu.frame = CGRectMake(self.view.frame.size.width+100, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height, 100, self.view.frame.size.height+HEIGHT_REDUCE);
            
        } completion:^(BOOL finished) {}];
        
        isMenuShowing = NO;
    }
    else
    {

        
        [UIView animateWithDuration:1.0 delay:0.0 usingSpringWithDamping:0.5 initialSpringVelocity:0.7 options:0 animations:^{
            self.chatSlideMenu.frame = CGRectMake(self.view.frame.size.width-100, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height, 100, self.view.frame.size.height+HEIGHT_REDUCE);
            
        } completion:^(BOOL finished) {}];

        
        [self resignkeyBoard];
        isMenuShowing = YES;
    }
}

-(void)selectSlideMenuButton:(NSInteger)index
{
    switch (index) {
        case 1:
            [self pushToView_RUTer:[self generateRuterDict]];
            break;
        case 2:
        {
            [UIView transitionWithView:chatTableView
                              duration:0.5f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^(void) {
                                self.chatSlideMenu.frame = CGRectMake(self.view.frame.size.width+100, 0, 100, chatTableView.frame.size.height+HEIGHT_REDUCE);
                                [self changeViewToEditMode];
                            } completion:NULL];
            
            
            isMenuShowing = NO;
            
        }
            
            break;
        case 3:
        {
            
            if (!showImpMsgLy)
            {
                menu.shouldShow = NO;
                [menuButton setHidden:YES];
                
                popUp =[[ImpMsgPopUp alloc]initWithFrame:CGRectMake(0,0,chatTableView.frame.size.width, self.view.frame.size.height) view:self.view bareJIDStr:user.jidStr];
                [popUp setBackground:self.view];
                popUp.userDP = userProfileImage;
                popUp.myDP = myProfileImage;
                
                
                popUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
                
                [self.view addSubview:popUp];
                
                [UIView animateWithDuration:0.3/1.5 animations:^{
                    popUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.3/2 animations:^{
                        popUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
                    } completion:^(BOOL finished) {
                        [UIView animateWithDuration:0.3/2 animations:^{
                            popUp.transform = CGAffineTransformIdentity;
                            showImpMsgLy = YES;
                            
                        }];
                    }];
                }];
                
                
            }
        
        }
            break;
        case 4:
            [self takevideo];
            break;
        case 5:
            //[self openPhotoAlbum];
        {
            isGalleryOpen = YES;

            SecureGalleryViewController *secureGallery = [[SecureGalleryViewController alloc]init];
            [self.navigationController pushViewController:secureGallery animated:YES];
        }
            
            
            
            break;
        case 6:
        {
            [UIView transitionWithView:chatTableView
                              duration:0.3f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^(void) {
                                self.chatSlideMenu.frame = CGRectMake(self.view.frame.size.width+100, 0, 100, chatTableView.frame.size.height+HEIGHT_REDUCE);
                                ;

                            } completion: ^(BOOL finished){
                                [self setBackground];
                            }];

            isMenuShowing = NO;

        }
            break;
        default:
            break;
    }
}




-(void)selectBubbleMenuButton:(NSInteger)index
{
    switch (index) {
        case 1:
            [self pushToView_RUTer:[self generateRuterDict]];
            break;
        case 2:
        {
            [UIView transitionWithView:chatTableView
                              duration:0.5f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^(void) {
                                self.chatSlideMenu.frame = CGRectMake(self.view.frame.size.width+100, 0, 100, chatTableView.frame.size.height+HEIGHT_REDUCE);
                                [self changeViewToEditMode];
                            } completion:NULL];
            
            
            isMenuShowing = NO;
            
        }
            
            break;
        case 3:
        {
            
            if (!showImpMsgLy)
            {
                menu.shouldShow = NO;
                
                popUp =[[ImpMsgPopUp alloc]initWithFrame:CGRectMake(0,0,chatTableView.frame.size.width, self.view.frame.size.height) view:self.view bareJIDStr:user.jidStr];
                [popUp setBackground:self.view];
                popUp.userDP = userProfileImage;
                popUp.myDP = myProfileImage;
                
                
                popUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
                
                [self.view addSubview:popUp];
                
                [UIView animateWithDuration:0.3/1.5 animations:^{
                    popUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.3/2 animations:^{
                        popUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
                    } completion:^(BOOL finished) {
                        [UIView animateWithDuration:0.3/2 animations:^{
                            popUp.transform = CGAffineTransformIdentity;
                            showImpMsgLy = YES;
                            
                        }];
                    }];
                }];

                
            }
            
        }
            break;
        case 4:
            [self takevideo];
            break;
        case 5:
          //  [self openPhotoAlbum];
            
        {
            isGalleryOpen = YES;
            
            SecureGalleryViewController *secureGallery = [[SecureGalleryViewController alloc]init];
            [self.navigationController pushViewController:secureGallery animated:YES];
        }
            
            
            break;
        case 6:
        {
            [UIView transitionWithView:chatTableView
                              duration:0.3f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^(void) {
                                self.chatSlideMenu.frame = CGRectMake(self.view.frame.size.width+100, 0, 100, chatTableView.frame.size.height+HEIGHT_REDUCE);
                                ;
                                
                            } completion: ^(BOOL finished){
                                [self setBackground];
                            }];
            
            isMenuShowing = NO;
            
        }
            break;
        default:
            break;
    }
}

-(void)galleryButtonAction:(id)sender
{
    [self openPhotoAlbum];
}


-(void)cameraButtonAction:(id)sender
{
    [self takePhoto];
}


-(void)impButtonAction:(id)sender
{
    UIImage *impImageIcon1 = [UIImage imageNamed:@"impMsgTv1"];
    UIImage *impImageIcon2 = [UIImage imageNamed:@"impMsgTV2"];

    [impBtn setImage:impImageIcon2 forState:UIControlStateNormal];

    isImpOn = !isImpOn;
    
    if (isImpOn)
    {
        [impBtn setImage:impImageIcon2 forState:UIControlStateNormal];
    }
    else
    {
        [impBtn setImage:impImageIcon1 forState:UIControlStateNormal];

    }
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}



- (void)convertVideoToLowQuailtyWithInputURL:(NSURL*)inputURL
                                   outputURL:(NSURL*)outputURL
                                     handler:(void (^)(AVAssetExportSession*))handler
{
    [[NSFileManager defaultManager] removeItemAtURL:outputURL error:nil];
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:inputURL options:nil];
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetMediumQuality];
    exportSession.outputURL = outputURL;
    exportSession.outputFileType = AVFileTypeQuickTimeMovie;
    [exportSession exportAsynchronouslyWithCompletionHandler:^(void)
     {
         handler(exportSession);
     }];
}

-(void)pushToProfileView
{
    [_chatTextview resignFirstResponder];

    if (!isEditMode)
    {
        
        if (!showImpMsgLy)
        {
            canExecuteViewWillAppear = YES;

            ProfileViewController * profile = [ProfileViewController new];
            profile.userDetails = user;
            [self.navigationController pushViewController:profile animated:YES];

           
        }
    }
  
    
}

-(void)createM13ContectMenu
{
    //Create the items
    M13ContextMenuItemIOS7 *bookmarkItem = [[M13ContextMenuItemIOS7 alloc] initWithUnselectedIcon:[UIImage imageNamed:@"BookmarkIcon"] selectedIcon:[UIImage imageNamed:@"BookmarkIcon"]];
    M13ContextMenuItemIOS7 *uploadItem = [[M13ContextMenuItemIOS7 alloc] initWithUnselectedIcon:[UIImage imageNamed:@"UploadIcon"] selectedIcon:[UIImage imageNamed:@"UploadIcon"]];
    M13ContextMenuItemIOS7 *trashIcon = [[M13ContextMenuItemIOS7 alloc] initWithUnselectedIcon:[UIImage imageNamed:@"TrashIcon"] selectedIcon:[UIImage imageNamed:@"TrashIconSelected"]];
	//Create the menu
    menu = [[M13ContextMenu alloc] initWithMenuItems:@[bookmarkItem, uploadItem,trashIcon]];
    menu.delegate = self;
    
    //Create the gesture recognizer
    longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressText:)];
    longPress.minimumPressDuration = 0.5;
    longPress.delegate = self;
    [self.view setUserInteractionEnabled:YES];
    self.view.tag = 110;
    self.view.exclusiveTouch = YES;
    [self.view addGestureRecognizer:longPress];
    
}


- (BOOL)shouldShowContextMenu:(M13ContextMenu *)contextMenu atPoint:(CGPoint)point
{
    [self resignkeyBoard];
    return YES;
}

- (void)contextMenu:(M13ContextMenu *)contextMenu atPoint:(CGPoint)point didSelectItemAtIndex:(NSInteger)index
{
    
    if (index == 0) {
        isPrivateHistoryVw = !isPrivateHistoryVw;
        
        if (isPrivateHistoryVw)
        {
            menu.shouldShow = NO;
            fromSecurityQuest = NO;
            
            [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
            [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];

            
            NSString *pwdString = [[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat:@"%@",MASTER_PIN]];
            if (pwdString.length == 0) {
                passwordVw = [[PasswordPin alloc]initWithFrame_onView:self.view LabelString:@"Set your Master Unlock Key" password:@"" Hint:SET_PRIVATE_PWD JID:self.user.jidStr];
                [passwordVw createPasswordVw];
            }
            else
            {
                passwordVw = [[PasswordPin alloc]initWithFrame_onView:self.view LabelString:@"Enter the Master Unlock Key" password:@"" Hint:ENTER_PRIVATE_PWD JID:self.user.jidStr];
                [passwordVw createPasswordVw];
            }
        
        }
        else
        {
            fetchedResultsController.delegate=nil;
            fetchedResultsController = nil;
            [UIView transitionWithView: chatTableView
                              duration: 0.35f
                               options: UIViewAnimationOptionTransitionCrossDissolve
                            animations: ^(void)
             {
                 [chatTableView reloadData];
             }
                            completion: ^(BOOL isFinished)
             {
                 
             }];
        }
        
        
    } else if (index == 1)
    {
        if (!isPrivateChat)
        {
            [self sendPrivateReq];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERTVIEW_TITLE message:@"You're already in Private mode." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
    }
    else if (index == 2)
    {
        menu.shouldShow = NO;
       
        popUp =[[ImpMsgPopUp alloc]initWithFrame:CGRectMake(0,0,chatTableView.frame.size.width, self.view.frame.size.height) view:self.view bareJIDStr:user.jidStr];
        [popUp setBackground:self.view];
        popUp.userDP = userProfileImage;
        popUp.myDP = myProfileImage;
        
        
        popUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
        
        [self.view addSubview:popUp];
        
        [UIView animateWithDuration:0.3/1.5 animations:^{
            popUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/2 animations:^{
                popUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.3/2 animations:^{
                    popUp.transform = CGAffineTransformIdentity;
                    showImpMsgLy = YES;

                }];
            }];
        }];

        
            
        
/*

        [UIView transitionWithView: self.view
                          duration: 0.5f
                           options: UIViewAnimationOptionCurveEaseIn animations: ^(void)
         {
         }
                        completion: ^(BOOL isFinished)
         {
             [self.view addSubview:popUp];
             showImpMsgLy = YES;


         }];
*/

    }
    else
    {
        [[self xmppMessageArchivingCoreDataStorage] updateUnreadMessage:self.user.jid clearValue:YES];
        
    }
    
}

//private Password Notification
-(void)privatePassword:(NSNotification *)notification
{
    menu.shouldShow = YES;
    
    
    [passwordVw removeFromSuperview];
    passwordVw = nil;
    
    NSDictionary *dict = [notification userInfo];
    
    if ([[dict objectForKey:@"Mode"] isEqualToString:SET_PRIVATE_PWD])
    {
        if ([[dict objectForKey:@"isCorrect"] isEqualToString:@"YES"])
        {
            menu.shouldShow = NO;
            passwordVw = [[PasswordPin alloc]initWithFrame_onView:self.view LabelString:@"Confirm your Master Unlock Key " password:@"" Hint:CONFRM_PRIVATE_PWD JID:self.user.jidStr];
            [passwordVw createPasswordVw];
            
            
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:MASTER_PIN];
            
            isPrivateHistoryVw = !isPrivateHistoryVw;
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];

        }
        
    }
    else if ([[dict objectForKey:@"Mode"] isEqualToString:CONFRM_PRIVATE_PWD])
    {
        if ([[dict objectForKey:@"isCorrect"] isEqualToString:@"YES"])
        {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];

            
            fetchedResultsController.delegate=nil;
            fetchedResultsController = nil;
            [UIView transitionWithView: chatTableView
                              duration: 0.35f
                               options: UIViewAnimationOptionTransitionCrossDissolve
                            animations: ^(void)
             {
                 [chatTableView reloadData];
           

             }
                            completion: ^(BOOL isFinished)
             {
             }];
            
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:MASTER_PIN];
            
            isPrivateHistoryVw = !isPrivateHistoryVw;
        }
        
        
    }else if ([[dict objectForKey:@"Mode"] isEqualToString:ENTER_PRIVATE_PWD])
    {
        if ([[dict objectForKey:@"isCorrect"] isEqualToString:@"YES"])
        {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];

            
            fetchedResultsController.delegate=nil;
            fetchedResultsController = nil;
            [UIView transitionWithView: chatTableView
                              duration: 0.35f
                               options: UIViewAnimationOptionTransitionCrossDissolve
                            animations: ^(void)
             {
                 [chatTableView reloadData];
             }
                            completion: ^(BOOL isFinished)
             {
             }];
            
        }
        else
        {
            isPrivateHistoryVw = !isPrivateHistoryVw;
        }
        
        
    }
    
    [self createPopUpforText];
    [self createPopUpforMedia];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];


}

-(void)createPrivateChatView
{
    if (privateVw) {
        [UIView animateWithDuration:0.3
                              delay:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             privateVw.frame =  CGRectMake(privateVw.frame.origin.x, -privateVw.frame.size.height, privateVw.frame.size.width, privateVw.frame.size.height);
                             
                         }
                         completion:^(BOOL finished){
                             [privateVw removeFromSuperview];
                             privateVw = nil;
                             [self showPrivateVw];
                             
                         }];
        
    }
    else
    {
        [self showPrivateVw];
    }
    
}

-(void)showPrivateVw
{
    privateChatDbEditer* privateSection = [privateChatDbEditer new];
    NSMutableDictionary* privateSectionDict = [privateSection fetchprivateChat:user.jidStr];
    isPrivateChat = NO;
    if ([[privateSectionDict objectForKey:@"status"] isEqualToString:@"pending"])
    {
        privateVw = [self createPrivateChatView_withString:[NSString stringWithFormat:@"Your private request pending"] status:@"not_request"];
        [self.view addSubview:privateVw];
        [self pushPrivateVwDown:privateVw];
    }
    else if ([[privateSectionDict objectForKey:@"status"] isEqualToString:@"request"])
    {
        privateVw = [self createPrivateChatView_withString:[NSString stringWithFormat:@"Private chat request from %@",user.displayName ] status:@"request"];
        [self.view addSubview:privateVw];
        [self pushPrivateVwDown:privateVw];
        
    }
    else if ([[privateSectionDict objectForKey:@"status"] isEqualToString:@"accepted"])
    {
        NSLog(@"Private chat - > %@",privateSectionDict);
        NSNumber  *isSender = (NSNumber *) [privateSectionDict valueForKey:@"is_Sender"];
        
        if ([isSender boolValue])
        {
            privateVw = [self createPrivateChatView_withString:[NSString stringWithFormat:@"You have initiated private chat with %@!",user.displayName] status:@"not_request"];
            
        }
        else
     {
         privateVw = [self createPrivateChatView_withString:[NSString stringWithFormat:@"%@ initiated private chat with you!",user.displayName ] status:@"not_request"];

     }
    


               [self.view addSubview:privateVw];
        [self pushPrivateVwDown:privateVw];
        isPrivateChat = YES;
        impBtn.hidden = YES;
    }
    else
    {
        isPrivateChat = NO;
        
        impBtn.hidden = NO;

        [[self xmppMessageArchivingCoreDataStorage]updateUnreadMessage:self.user.jid clearValue:YES];
    }
    
}

-(UIView*)createPrivateChatView_withString:(NSString*)status_msg status:(NSString*)status
{
    UIView* private_Vw = [UIView new];
    UIButton* cancelBtn;
    UIButton* acceptBtn;
    UIButton* denyBtn;
    UIButton* notNowBtn;
    UIImageView* PrivateMan = [UIImageView new];
    UILabel *titleLbl = [UILabel new];

    
    UIImage *PrivateManImage = [UIImage imageNamed:@"privateMan0"];
    
    
    private_Vw.frame = CGRectMake(0, -38, self.view.frame.size.width ,38);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        UIVisualEffect *blurEffect;
        blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *visualEffectView;
        visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        visualEffectView.frame = private_Vw.bounds;
        [private_Vw addSubview:visualEffectView];
    }
    else
    {
        private_Vw.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.9];
    }

    
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"private-man" withExtension:@"gif"];
    PrivateMan.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
    [private_Vw addSubview:PrivateMan];
    
    
    
    if ([status isEqualToString:@"not_request"])
    {
        
        UIFont *font = [UIFont fontWithName:FontString size:12];

        CGRect textRect = [status_msg boundingRectWithSize:CGSizeMake(320, 9999)
                                                            options:NSStringDrawingUsesLineFragmentOrigin
                                                         attributes:@{NSFontAttributeName:font}
                                                            context:nil];
        
        PrivateMan.frame = CGRectMake( ((private_Vw.frame.size.width - PrivateManImage.size.width)/2), (private_Vw.frame.size.height - PrivateManImage.size.height)/2, PrivateManImage.size.width, PrivateManImage.size.height);
        
        titleLbl.frame = CGRectMake(((private_Vw.frame.size.width - (textRect.size.width+10))/2), PrivateMan.frame.size.height  + PrivateMan.frame.origin.y + 10 , textRect.size.width + 10, 20);
        titleLbl.backgroundColor = [UIColor clearColor];
        titleLbl.textColor = [UIColor whiteColor];
        titleLbl.font = [UIFont fontWithName:FontString size:12];
        titleLbl.backgroundColor = RGBA(119, 186, 240, 0.7);
        titleLbl.textAlignment = NSTextAlignmentCenter;
        titleLbl.layer.cornerRadius = 10;
        titleLbl.clipsToBounds = YES;
        titleLbl.text = status_msg;
        [private_Vw addSubview:titleLbl];

        
        cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        cancelBtn.frame = CGRectMake(private_Vw.frame.size.width - 40,(private_Vw.frame.size.height - 30)/2, 30, 30);
        cancelBtn.backgroundColor = [UIColor clearColor];
        [cancelBtn addTarget:self action:@selector(cancelBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cancelBtn setTitle:@"X" forState:UIControlStateNormal];
        [cancelBtn  setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        cancelBtn.titleLabel.font = [UIFont fontWithName:FontString size:15];//[UIFont systemFontOfSize:14];
        cancelBtn.layer.cornerRadius = 30/2;
        cancelBtn.layer.borderColor =  RGBA(237, 63, 29, 1).CGColor;
        cancelBtn.layer.borderWidth = 2.0f;
        [private_Vw addSubview:cancelBtn];
        
    }
    
    else if ([status isEqualToString:@"request"])
    {
        
        PrivateMan.frame = CGRectMake(((private_Vw.frame.size.width - 200)/2) -  PrivateManImage.size.width, (private_Vw.frame.size.height - PrivateManImage.size.height)/2, PrivateManImage.size.width, PrivateManImage.size.height);
        
        
        acceptBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        acceptBtn.frame = CGRectMake(PrivateMan.frame.origin.x + PrivateMan.frame.size.width + 14, 2, 78, 35);
        acceptBtn.backgroundColor = [UIColor clearColor];
        [acceptBtn addTarget:self action:@selector(acceptBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [acceptBtn setTitle:@"Accept" forState:UIControlStateNormal];
        [acceptBtn  setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        acceptBtn.titleLabel.font = [UIFont fontWithName:FontString size:15];//[UIFont systemFontOfSize:14];
        acceptBtn.layer.cornerRadius = 10;
        acceptBtn.layer.borderColor =  RGBA(118, 117, 59, 1).CGColor;
        acceptBtn.layer.borderWidth = 2.0f;
        [private_Vw addSubview:acceptBtn];
        
        
        
        denyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        denyBtn.frame = CGRectMake(acceptBtn.frame.origin.x + acceptBtn.frame.size.width + 20, 2, 78, 35);
        denyBtn.backgroundColor = [UIColor clearColor];
        [denyBtn addTarget:self action:@selector(denyBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [denyBtn setTitle:@"Deny" forState:UIControlStateNormal];
        [denyBtn  setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        denyBtn.titleLabel.font = [UIFont fontWithName:FontString size:15];//[UIFont systemFontOfSize:14];
        denyBtn.layer.cornerRadius = 10;
        denyBtn.layer.borderColor =  RGBA(237, 63, 29, 1).CGColor;
        denyBtn.layer.borderWidth = 2.0f;
        [private_Vw addSubview:denyBtn];
        
        
    }
    
    
    
    return private_Vw;
}



/*Previous Flow
 -(UIView*)createPrivateChatView_withString:(NSString*)status_msg status:(NSString*)status
 {
 UIView* private_Vw = [UIView new];
 UIButton* cancelBtn;
 UIButton* acceptBtn;
 UIButton* denyBtn;
 UIButton* notNowBtn;
 UIImageView* PrivateMan = [UIImageView new];
 
 UIImage *PrivateManImage = [UIImage imageNamed:@"privateMan0"];
 
 int hideBtnWidth = 22;
 
 private_Vw.frame = CGRectMake(-hideBtnWidth, -38, self.view.frame.size.width + hideBtnWidth,38);
 
 
 PrivateMan.image = PrivateManImage;
 [private_Vw addSubview:PrivateMan];
 
 
 
 if ([status isEqualToString:@"not_request"])
 {
 
 PrivateMan.frame = CGRectMake( ((private_Vw.frame.size.width - PrivateManImage.size.width)/2), (private_Vw.frame.size.height - PrivateManImage.size.height)/2, PrivateManImage.size.width, PrivateManImage.size.height);
 
 cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
 cancelBtn.frame = CGRectMake(private_Vw.frame.size.width - 40,(private_Vw.frame.size.height - 30)/2, 30, 30);
 cancelBtn.backgroundColor = [UIColor clearColor];
 [cancelBtn addTarget:self action:@selector(cancelBtnAction:) forControlEvents:UIControlEventTouchUpInside];
 [cancelBtn setTitle:@"X" forState:UIControlStateNormal];
 [cancelBtn  setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
 cancelBtn.titleLabel.font = [UIFont fontWithName:FontString size:15];//[UIFont systemFontOfSize:14];
 cancelBtn.layer.cornerRadius = 30/2;
 cancelBtn.layer.borderColor =  RGBA(237, 63, 29, 1).CGColor;
 cancelBtn.layer.borderWidth = 2.0f;
 [private_Vw addSubview:cancelBtn];
 }
 else if ([status isEqualToString:@"request"])
 {
 
 PrivateMan.frame = CGRectMake(hideBtnWidth+27, (private_Vw.frame.size.height - PrivateManImage.size.height)/2, PrivateManImage.size.width, PrivateManImage.size.height);
 
 
 acceptBtn = [UIButton buttonWithType:UIButtonTypeCustom];
 acceptBtn.frame = CGRectMake(PrivateMan.frame.origin.x + PrivateMan.frame.size.width + 7, (private_Vw.frame.size.height - 27)/2, 68, 27);
 acceptBtn.backgroundColor = [UIColor clearColor];
 [acceptBtn addTarget:self action:@selector(acceptBtnAction:) forControlEvents:UIControlEventTouchUpInside];
 [acceptBtn setTitle:@"Accept" forState:UIControlStateNormal];
 [acceptBtn  setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
 acceptBtn.titleLabel.font = [UIFont fontWithName:FontString size:15];//[UIFont systemFontOfSize:14];
 acceptBtn.layer.cornerRadius = 27/2;
 acceptBtn.layer.borderColor =  RGBA(118, 117, 59, 1).CGColor;
 acceptBtn.layer.borderWidth = 2.0f;
 [private_Vw addSubview:acceptBtn];
 
 
 
 denyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
 denyBtn.frame = CGRectMake(acceptBtn.frame.origin.x + acceptBtn.frame.size.width + 10, (private_Vw.frame.size.height - 27)/2, 68, 27);
 denyBtn.backgroundColor = [UIColor clearColor];
 [denyBtn addTarget:self action:@selector(denyBtnAction:) forControlEvents:UIControlEventTouchUpInside];
 [denyBtn setTitle:@"Deny" forState:UIControlStateNormal];
 [denyBtn  setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
 denyBtn.titleLabel.font = [UIFont fontWithName:FontString size:15];//[UIFont systemFontOfSize:14];
 denyBtn.layer.cornerRadius = 27/2;
 denyBtn.layer.borderColor =  RGBA(237, 63, 29, 1).CGColor;
 denyBtn.layer.borderWidth = 2.0f;
 [private_Vw addSubview:denyBtn];
 
 
 
 
 notNowBtn = [UIButton buttonWithType:UIButtonTypeCustom];
 notNowBtn.frame = CGRectMake(denyBtn.frame.origin.x + denyBtn.frame.size.width + 10, (private_Vw.frame.size.height - 27)/2, 68, 27);
 notNowBtn.backgroundColor = [UIColor clearColor];
 [notNowBtn addTarget:self action:@selector(notNowBtnAction:) forControlEvents:UIControlEventTouchUpInside];
 [notNowBtn setTitle:@"Not now" forState:UIControlStateNormal];
 [notNowBtn  setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
 notNowBtn.titleLabel.font = [UIFont fontWithName:FontString size:15];//[UIFont systemFontOfSize:14];
 notNowBtn.layer.cornerRadius = 27/2;
 notNowBtn.layer.borderColor =  RGBA(135, 151, 52, 1).CGColor;
 notNowBtn.layer.borderWidth = 2.0f;
 [private_Vw addSubview:notNowBtn];
 }
 
 
 
 
 
 
 if (acceptBtn) {
 UIImage *hideImage = [UIImage imageNamed:@"privateHide"];
 
 UIButton* hideBtn = [UIButton buttonWithType:UIButtonTypeCustom];
 hideBtn.frame = CGRectMake(0, 0, hideImage.size.width, hideImage.size.height);
 hideBtn.backgroundColor = [UIColor clearColor];
 [hideBtn setImage:hideImage forState:UIControlStateNormal];
 [hideBtn addTarget:self action:@selector(hideBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
 [private_Vw addSubview:hideBtn];
 hideImage = nil;
 
 
 
 
 }
 private_Vw.backgroundColor = [UIColor blackColor];
 
 return private_Vw;
 }
 */

-(CGSize)frameForText:(NSString*)text sizeWithFont:(UIFont*)font constrainedToSize:(CGSize)size lineBreakMode:(NSLineBreakMode)lineBreakMode  {
    
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.lineBreakMode = lineBreakMode;
    
    NSDictionary * attributes = @{NSFontAttributeName:font,
                                  NSParagraphStyleAttributeName:paragraphStyle
                                  };
    
    
    CGRect textRect = [text boundingRectWithSize:size
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:attributes
                                         context:nil];
    
    //Contains both width & height ... Needed: The height
    return textRect.size;
}
-(void)pushPrivateVwDown:(UIView*)privVw
{
    
    [UIView animateWithDuration:0.25 animations:^{
        privVw.frame =  CGRectMake(privVw.frame.origin.x , self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height, privVw.frame.size.width, privVw.frame.size.height);
    }];
}

-(void)pushPrivateVwSide:(UIView*)privVw
{
    NSLog(@"%@",privVw.subviews);
    [UIView animateWithDuration:0.25 animations:^{
        privVw.frame =  CGRectMake(self.view.frame.size.width - (privVw.frame.size.width - self.view.frame.size.width), self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height, privVw.frame.size.width, privVw.frame.size.height);
    }];
    
    
}

-(void)hideBtnClicked:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    UIView* privVw = btn.superview;
    [UIView animateWithDuration:0.25 animations:^{
        privVw.frame =  CGRectMake(self.view.frame.size.width - privVw.frame.size.width , self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height, privVw.frame.size.width, privVw.frame.size.height);
    }];
}
-(void)sendPrivateReq
{
    privateChatDbEditer* privateSection = [privateChatDbEditer new];
    NSMutableDictionary* dict = [NSMutableDictionary new];
    [dict setObject:user.jidStr forKey:@"iD"];
    [dict setObject:[NSNumber numberWithBool:YES] forKey:@"is_Sender"];
    
    
    NSMutableDictionary* privateSectionDict = [privateSection fetchprivateChat:user.jidStr];
    if ([[privateSectionDict objectForKey:@"status"] isEqualToString:@"request"])
    {
        [self timerInvalidate:user.jidStr];
        [dict setObject:@"accepted" forKey:@"status"];
        [dict setObject:[NSNumber numberWithDouble:0] forKey:@"timer"];
        [dict setObject:[NSNumber numberWithBool:YES] forKey:@"is_Sender"];

        
        
    }
    
    else
    {
        [dict setObject:@"pending" forKey:@"status"];
        [dict setObject:[NSNumber numberWithDouble:30] forKey:@"timer"];
        [dict setObject:[NSNumber numberWithBool:YES] forKey:@"is_Sender"];
        
    }
    
    
    BOOL check = [privateSection checkAndInsert:dict];
    if (check )
    {
        
        NSLog(@"success %@",[privateSection fetchprivateChat]);
    }
    
    
    
    [self createPrivateChatView];
    
    NSString *messageId = [[self xmppStream] generateUUID];
    XMPPMessage *privateMessage = [XMPPMessage new];
    [privateMessage addAttributeWithName:@"id"  stringValue:messageId];
    [privateMessage addAttributeWithName:@"to" stringValue:user.jidStr];
    [privateMessage addAttributeWithName:@"from" stringValue:[self xmppStream].myJID.bare];
    [privateMessage addAttributeWithName:@"type" stringValue:@"chat"];
    NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
    [body setStringValue:@""];
    [privateMessage addChild:body];
    NSXMLElement *private = [NSXMLElement elementWithName:@"private"];
    [private addAttributeWithName:@"xmlns" stringValue:@"private"];
    [private addAttributeWithName:@"type" stringValue:@"request"];
    [privateMessage addChild:private];
    [[self xmppStream] sendElement:privateMessage];
    
}
-(void)receivedPrivateReq
{
    
    
    [self createPrivateChatView];
}

-(void)acceptBtnAction:(id)sender
{
    
    privateChatDbEditer* privateSection = [privateChatDbEditer new];
    NSMutableDictionary* dict = [NSMutableDictionary new];
    [dict setObject:user.jidStr forKey:@"iD"];
    [dict setObject:@"accepted" forKey:@"status"];
    [dict setObject:[NSNumber numberWithDouble:0] forKey:@"timer"];
    [dict setObject:[NSNumber numberWithBool:NO] forKey:@"is_Sender"];

    
    BOOL check = [privateSection checkAndInsert:dict];
    if (check )
    {
        NSString *messageId = [[self xmppStream] generateUUID];
        XMPPMessage *privateMessage = [XMPPMessage new];
        [privateMessage addAttributeWithName:@"id"  stringValue:messageId];
        [privateMessage addAttributeWithName:@"to" stringValue:user.jidStr];
        [privateMessage addAttributeWithName:@"from" stringValue:[self xmppStream].myJID.bare];
        [privateMessage addAttributeWithName:@"type" stringValue:@"chat"];
        NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
        [body setStringValue:@""];
        [privateMessage addChild:body];
        NSXMLElement *private = [NSXMLElement elementWithName:@"private"];
        [private addAttributeWithName:@"xmlns" stringValue:@"private"];
        [private addAttributeWithName:@"type" stringValue:@"response"];
        [private addAttributeWithName:@"response" stringValue:@"1"];
        [privateMessage addChild:private];
        [[self xmppStream] sendElement:privateMessage];
        
    }
    [self timerInvalidate:user.jidStr];
    [self createPrivateChatView];
    
}

-(void)denyBtnAction:(id)sender
{
    privateChatDbEditer* privateSection = [privateChatDbEditer new];
    NSMutableDictionary* dict = [NSMutableDictionary new];
    [dict setObject:user.jidStr forKey:@"iD"];
    [dict setObject:@"denied" forKey:@"status"];
    [dict setObject:[NSNumber numberWithDouble:0] forKey:@"timer"];
    
    BOOL check = [privateSection checkAndInsert:dict];
    if (check )
    {
        NSString *messageId = [[self xmppStream] generateUUID];
        XMPPMessage *privateMessage = [XMPPMessage new];
        [privateMessage addAttributeWithName:@"id"  stringValue:messageId];
        [privateMessage addAttributeWithName:@"to" stringValue:user.jidStr];
        [privateMessage addAttributeWithName:@"from" stringValue:[self xmppStream].myJID.bare];
        [privateMessage addAttributeWithName:@"type" stringValue:@"chat"];
        NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
        [body setStringValue:@""];
        [privateMessage addChild:body];
        NSXMLElement *private = [NSXMLElement elementWithName:@"private"];
        [private addAttributeWithName:@"xmlns" stringValue:@"private"];
        [private addAttributeWithName:@"type" stringValue:@"response"];
        [private addAttributeWithName:@"response" stringValue:@"0"];
        [privateMessage addChild:private];
        [[self xmppStream] sendElement:privateMessage];
        
    }
    [self timerInvalidate:user.jidStr];
    
    [self createPrivateChatView];
}

-(void)cancelBtnAction:(id)sender
{
    
    privateChatDbEditer* privateSection = [privateChatDbEditer new];
    NSMutableDictionary* dict = [NSMutableDictionary new];
    [dict setObject:user.jidStr forKey:@"iD"];
    
    
    BOOL check = [privateSection deletePrivateChat:user.jidStr];
    [self timerInvalidate:user.jidStr];
    if (check )
    {
        NSString *messageId = [[self xmppStream] generateUUID];
        XMPPMessage *privateMessage = [XMPPMessage new];
        [privateMessage addAttributeWithName:@"id"  stringValue:messageId];
        [privateMessage addAttributeWithName:@"to" stringValue:user.jidStr];
        [privateMessage addAttributeWithName:@"from" stringValue:[self xmppStream].myJID.bare];
        [privateMessage addAttributeWithName:@"type" stringValue:@"chat"];
        NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
        [body setStringValue:@""];
        [privateMessage addChild:body];
        NSXMLElement *private = [NSXMLElement elementWithName:@"private"];
        [private addAttributeWithName:@"xmlns" stringValue:@"private"];
        [private addAttributeWithName:@"type" stringValue:@"close"];
        [privateMessage addChild:private];
        [[self xmppStream] sendElement:privateMessage];
        
    }
    [self timerInvalidate:user.jidStr];
    
    [self createPrivateChatView];
}

-(void)notNowBtnAction:(id)sender
{
    UIButton *notNowBtn = (UIButton *)sender;
    UIView* view = notNowBtn.superview;
    [self pushPrivateVwSide:view] ;
}

-(void)timerDead:(NSNotification *)notification
{
    NSDictionary *dict = [notification userInfo];
    
  //  NSLog(@"2");
    if ([[dict objectForKey:@"user_id"] isEqualToString:user.jidStr])
    {
        [self createPrivateChatView];
    }
}
-(void)timerInvalidate:(NSString*)ID
{
    self.singleton = [Singleton sharedMySingleton];
    if ([self.singleton.privateReqIndicatorDict objectForKey:ID]) {
        NSTimer* timer = [self.singleton.privateReqIndicatorDict objectForKey:ID];
        [timer invalidate];
        [self.singleton.privateReqIndicatorDict removeObjectForKey:ID];
    }
}


-(void)longPressText:(UILongPressGestureRecognizer *)gesture
{
    
    if (gesture.view.tag == 110)
    {
        if([menu respondsToSelector:@selector(showMenuUponActivationOfGetsure:)])
        {
        [menu showMenuUponActivationOfGetsure:gesture];
        }
    }
    else
        
    {


    if (gesture.state == UIGestureRecognizerStateBegan)
    {
        [self.popupMenu_media dismissAnimated:NO];
        [self.popupMenu_txt dismissAnimated:NO];
        [self.popupMenu_sticker dismissAnimated:NO];
        
        CGPoint location = [gesture locationInView:chatTableView];
        
        XMPPMessageArchiving_Message_CoreDataObject *coremessage = [[self fetchedResultsController ] objectAtIndexPath:[chatTableView indexPathForRowAtPoint:location]];
        
        
        
        
        CGPoint touchPos = [gesture locationInView:chatTableView];
        NSIndexPath *indexPath = [chatTableView  indexPathForRowAtPoint:touchPos];
        UITableViewCell *tappedCell = [chatTableView cellForRowAtIndexPath:indexPath];
        
        CGFloat fullScreenHeight = 568.0;
        CGFloat remaingPortion = fullScreenHeight - tappedCell.frame.origin.y;
        
        CGFloat cellStartingPoint = fullScreenHeight - remaingPortion;
        
 
        
        EditMessageId = coremessage.messageID;
        CGPoint location2;
        CGPoint location3;
        location2  = [gesture locationInView:self.view];
        location3  = [gesture locationInView:gesture.view];
        
        if (location2.y <= 150)
        {
            location2  = [gesture locationInView:self.view];
            location3  = [gesture locationInView:gesture.view];
            
            self.popupMenu_txt.arrowDirection = QBPopupMenuArrowDirectionUp;
            self.popupMenu_sticker.arrowDirection = QBPopupMenuArrowDirectionUp;
            self.popupMenu_media.arrowDirection = QBPopupMenuArrowDirectionUp;
        }
        else
        {
            location2  = [gesture locationInView:self.view];
            location3  = [gesture locationInView:gesture.view];
            
            self.popupMenu_txt.arrowDirection = QBPopupMenuArrowDirectionDown;
            self.popupMenu_media.arrowDirection = QBPopupMenuArrowDirectionDown;
            self.popupMenu_sticker.arrowDirection = QBPopupMenuArrowDirectionDown;
        }
        
        
        
        NSXMLElement *file = [coremessage.message elementForName:@"file"];
        if (file) {
            [self showMenu_media:CGPointMake(location2.x, location2.y - location3.y)];
            mediaDbEditor* mediaInfo = [mediaDbEditor sharedInstance];
         //   NSLog(@"%@",[mediaInfo fetchMedia:coremessage.messageID]) ;
            
        }
        else if([beeFullArray containsObject:coremessage.body])
        {
            [self showMenu_sticker:CGPointMake(location2.x, location2.y - location3.y)];
            
        }
        else
        {
            NSXMLElement *ruder = [coremessage.message elementForName:@"ruthere"];
            if (ruder)
            {
                [self showMenu_sticker:CGPointMake(location2.x, location2.y - location3.y)];
            }
            else
            {
                if (tappedCell.frame.size.height>400)
                {
                    
                    [self showMenu_txt:CGPointMake(location2.x, self.view.frame.size.height/2)];
                    
                }
                else
                {
                    [self showMenu_txt:CGPointMake(location2.x, location2.y - location3.y)];

                }
                
            }
        }
    }
    }
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if (![gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]] && ![otherGestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]])
    {
        return YES;
    }
    
    return NO;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;
}






-(void)createPopUpforText
{
    if (isPrivateHistoryVw)
    {
        // QBPopupMenuItem *item2 = [QBPopupMenuItem itemWithTitle:@"Secure" target:self action:@selector(SecureMethod)];
        QBPopupMenuItem *item3 = [QBPopupMenuItem itemWithTitle:@"Copy" target:self action:@selector(copyStringMethod)];
        QBPopupMenuItem *item4 = [QBPopupMenuItem itemWithTitle:@"Delete" target:self action:@selector(deleteMethod)];
        QBPopupMenuItem *item5 = [QBPopupMenuItem itemWithTitle:@"UnSecure" target:self action:@selector(makeTextUnSecure)];
        
        //QBPopupMenuItem *item6 = [QBPopupMenuItem itemWithImage:[UIImage imageNamed:@"test"] target:self action:@selector(action)];
        NSArray *items = @[ item3, item4,item5];
        
        QBPopupMenu *popupMenu = [[QBPopupMenu alloc] initWithItems:items];
        popupMenu.highlightedColor = [[UIColor blackColor] colorWithAlphaComponent:1];
        self.popupMenu_txt = popupMenu;
    }
    else
    {
        // QBPopupMenuItem *item2 = [QBPopupMenuItem itemWithTitle:@"Secure" target:self action:@selector(SecureMethod)];
        QBPopupMenuItem *item3 = [QBPopupMenuItem itemWithTitle:@"Copy" target:self action:@selector(copyStringMethod)];
        QBPopupMenuItem *item4 = [QBPopupMenuItem itemWithTitle:@"Delete" target:self action:@selector(deleteMethod)];
        QBPopupMenuItem *item5 = [QBPopupMenuItem itemWithTitle:@"Secure" target:self action:@selector(makeTextSecure)];
        
        //QBPopupMenuItem *item6 = [QBPopupMenuItem itemWithImage:[UIImage imageNamed:@"test"] target:self action:@selector(action)];
        NSArray *items = @[ item3, item4,item5];
        
        QBPopupMenu *popupMenu = [[QBPopupMenu alloc] initWithItems:items];
        popupMenu.highlightedColor = [[UIColor blackColor] colorWithAlphaComponent:1];
        self.popupMenu_txt = popupMenu;
        
    }
    
}
-(void)createPopUpforMedia
{
    
    if (isPrivateHistoryVw)
    {
        
        QBPopupMenuItem *item2 = [QBPopupMenuItem itemWithTitle:@"UnSecure" target:self action:@selector(makeImageUnSecure)];
        QBPopupMenuItem *item4 = [QBPopupMenuItem itemWithTitle:@"Delete" target:self action:@selector(deleteMethod)];
        NSArray *items = @[ item2, item4];
        
        QBPopupMenu *popupMenu = [[QBPopupMenu alloc] initWithItems:items];
        popupMenu.highlightedColor = [[UIColor blackColor] colorWithAlphaComponent:1];
        self.popupMenu_media = popupMenu;
    }
    else
    {
        QBPopupMenuItem *item2 = [QBPopupMenuItem itemWithTitle:@"Secure" target:self action:@selector(SecureMethod)];
        QBPopupMenuItem *item4 = [QBPopupMenuItem itemWithTitle:@"Delete" target:self action:@selector(deleteMethod)];
        NSArray *items = @[ item2, item4];
        
        QBPopupMenu *popupMenu = [[QBPopupMenu alloc] initWithItems:items];
        popupMenu.highlightedColor = [[UIColor blackColor] colorWithAlphaComponent:1];
        self.popupMenu_media = popupMenu;
    }
}


-(void)createPopUpforSticker
{
    QBPopupMenuItem *item4 = [QBPopupMenuItem itemWithTitle:@"Delete" target:self action:@selector(deleteMethod)];
    NSArray *items = @[ item4];
    
    QBPopupMenu *popupMenu = [[QBPopupMenu alloc] initWithItems:items];
    popupMenu.highlightedColor = [[UIColor blackColor] colorWithAlphaComponent:1];
    self.popupMenu_sticker = popupMenu;
}
-(void) showMenu_txt:(CGPoint )obj{
    [self.popupMenu_txt showInView:self.view targetRect:CGRectMake(obj.x, obj.y, 10, 10) animated:YES];
}

-(void) showMenu_media:(CGPoint )obj{
    [self.popupMenu_media showInView:self.view targetRect:CGRectMake(obj.x, obj.y, 10, 10) animated:YES];
}
-(void) showMenu_sticker:(CGPoint )obj{
    [self.popupMenu_sticker showInView:self.view targetRect:CGRectMake(obj.x, obj.y, 10, 10) animated:YES];
}

-(void)copyStringMethod
{
    XMPPMessageArchiving_Message_CoreDataObject *coremessage = [self MsgWithID:EditMessageId];
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    
    NSData *newdata=[coremessage.body dataUsingEncoding:NSUTF8StringEncoding
                                   allowLossyConversion:YES];
     pasteboard.string =[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];

    
}
-(void)SecureMethod
{
    
    NSMutableDictionary *mediaDict =  [[mediaDbEditor sharedInstance] fetchMedia:EditMessageId];
    
    NSLog(@"%@",mediaDict);
    
    if (mediaDict) {
        
        if ([[mediaDict objectForKey:@"success_failure"] isEqualToString:@"0"] || [[mediaDict objectForKey:@"success_failure"] isEqualToString:@"3"] )
        {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"OOPS!" message:@"Please download the media." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else
        {
            
            XMPPMessageArchiving_Message_CoreDataObject *coremessage = [self MsgWithID:EditMessageId];
            coremessage.isPrivate = YES;
            coremessage.privateStatus = YES;
            
            
            mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
            NSMutableDictionary* dict = [NSMutableDictionary new];
            [dict setObject:EditMessageId  forKey:@"msgId"];
            [dict setObject:[NSNumber numberWithBool:YES] forKey:@"isPrivate"];
            [mediaDb updateMedia:dict];
            
            NSManagedObjectContext *context= [self managedObjecMessageContact];
            NSError *error = nil;
            
            if (![context save:&error])
            {
                //  NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
            }

          

        
        }

    }
    
    
    
}



-(void)makeTextSecure
{
    
    XMPPMessageArchiving_Message_CoreDataObject *coremessage = [self MsgWithID:EditMessageId];
    coremessage.isPrivate = YES;
    coremessage.privateStatus = YES;
    
    NSManagedObjectContext *context= [self managedObjecMessageContact];
    
    
    NSError *error = nil;
    
    if (![context save:&error])
    {
      //  NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
    }
    
}

-(void)makeTextUnSecure
{
    
    XMPPMessageArchiving_Message_CoreDataObject *coremessage = [self MsgWithID:EditMessageId];
    coremessage.isPrivate = NO;
    coremessage.privateStatus = NO;
    
    NSManagedObjectContext *context= [self managedObjecMessageContact];
    
    
    NSError *error = nil;
    
    if (![context save:&error])
    {
    //    NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
    }
    
}


-(void)makeImageUnSecure
{
    XMPPMessageArchiving_Message_CoreDataObject *coremessage = [self MsgWithID:EditMessageId];
    coremessage.isPrivate = NO;
    coremessage.privateStatus = NO;
    
    mediaDbEditor* mediaDb = [mediaDbEditor new];
    NSMutableDictionary* dict = [NSMutableDictionary new];
    [dict setObject:EditMessageId  forKey:@"msgId"];
    [dict setObject:[NSNumber numberWithBool:NO] forKey:@"isPrivate"];
    [mediaDb updateMedia:dict];
    
    NSManagedObjectContext *context= [self managedObjecMessageContact];
    NSError *error = nil;
    
    if (![context save:&error])
    {
     //   NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
    }
}




-(void)deleteMethod
{
    shouldScrollToBottom = NO;
    XMPPMessageArchiving_Message_CoreDataObject *coremessage = [self MsgWithID:EditMessageId];
    
    NSManagedObjectContext *context= [self managedObjecMessageContact];
    
    NSXMLElement *file = [coremessage.message elementForName:@"file"];
    
    if (file)
    {
        mediaDbEditor* mediaDb = [mediaDbEditor new];
        [mediaDb deleteMedia:coremessage.messageID];
    }
    

    
    [context deleteObject:coremessage];
    
    NSError *error = nil;
    
    if (![context save:&error])
    {
      //  NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
    }
    
    
        [[self xmppMessageArchivingCoreDataStorage]updateMessageOnRecentChat:self.user.jidStr];
    
    
}

-(void)editInputVw_Create
{
    editInputView = [UIView new];
    editInputView.hidden = YES;
    editInputView.frame = CGRectMake(0, self.view.frame.size.height - 50, self.view.frame.size.width, 50);
    editInputView.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.5];
    [self.view addSubview:editInputView];
    
    UIButton* deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    deleteBtn.frame = CGRectMake(editInputView.frame.size.width - 100, (editInputView.frame.size.height - 30)/2, 80, 30);
    [deleteBtn setTitle:@"Delete" forState:UIControlStateNormal];
    [deleteBtn setTintColor:[UIColor whiteColor]];
    [deleteBtn addTarget:self action:@selector(deleteBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    deleteBtn.backgroundColor = RGBA(46, 141, 214, 1);
    deleteBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    deleteBtn.layer.cornerRadius = 0.5;
   [editInputView addSubview:deleteBtn];
    
    
    
    UIButton* clearAllBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    clearAllBtn.frame = CGRectMake(self.view.frame.origin.x + 30, (editInputView.frame.size.height - 30)/2, 80, 30);
    [clearAllBtn setTitle:@"Clear All" forState:UIControlStateNormal];
    [clearAllBtn setTintColor:[UIColor whiteColor]];
    [clearAllBtn addTarget:self action:@selector(clearAllBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    clearAllBtn.backgroundColor = RGBA(46, 141, 214, 1);
    clearAllBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    clearAllBtn.layer.cornerRadius = 0.5;
    [editInputView addSubview:clearAllBtn];
    
}
-(void)deleteBtnClicked
{
    for (NSString* msgId in deleteMsgIdAry) {
        shouldScrollToBottom = NO;
        XMPPMessageArchiving_Message_CoreDataObject *coremessage = [self MsgWithID:msgId];
        
        NSManagedObjectContext *context= [self managedObjecMessageContact];
        
        NSXMLElement *file = [coremessage.message elementForName:@"file"];
        
        if (file)
        {
            mediaDbEditor* mediaDb = [mediaDbEditor new];
            [mediaDb deleteMedia:coremessage.messageID];
        }
        

        
        [context deleteObject:coremessage];
        
        NSError *error = nil;
        
        if (![context save:&error])
        {
         //   NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
        }
        
    }
    
   
    [[self xmppMessageArchivingCoreDataStorage]updateMessageOnRecentChat:self.user.jidStr];

    [deleteMsgIdAry removeAllObjects];
    
}

-(void)clearAllBtnClicked
{
    
    UIAlertView *clearAlert = [[UIAlertView alloc]initWithTitle:@"This will clear all your message(s). Are you sure?" message:nil delegate:self cancelButtonTitle:@"No! I'm not" otherButtonTitles:@"Yes! I am", nil];
    [clearAlert dismissWithClickedButtonIndex:1 animated:YES];
    [clearAlert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        [self clearChatWithID:user.jidStr];

    }
}
-(void)changeViewToEditMode
{
    [UIView transitionWithView:chatTableView
                      duration:0.5f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        isEditMode = YES;
                        
                        [deleteMsgIdAry removeAllObjects];
                        
                        [chatTableView reloadData];
                        
                        //to be hidden
                        backButton.hidden = YES;
                        menuButton.hidden = YES;
                        inputView.hidden = YES;
                        deleteDoneBtn.hidden = NO;
                        menu.shouldShow = NO;
                        bubble.hidden = YES;
                        editInputView.hidden = NO;
                        
                    } completion:NULL];
    
    
    //    [UIView transitionWithView:self.view
    //                      duration:0.5f
    //                       options:UIViewAnimationOptionTransitionCrossDissolve
    //                    animations:^(void) {
    //
    //                        bubble.hidden = YES;
    //
    //
    //                    } completion:NULL];
    
}

-(void)changeViewToNormalMode
{
    [UIView transitionWithView:chatTableView
                      duration:0.5f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        isEditMode = NO;
                        [deleteMsgIdAry removeAllObjects];
                        [chatTableView reloadData];
                        
                        //to be hidden
                        menuButton.hidden = NO;
                        inputView.hidden = NO;
                        deleteDoneBtn.hidden  = YES;
                        menu.shouldShow = YES;
                        backButton.hidden = NO;
                        bubble.hidden = NO;
                        editInputView.hidden = YES;
                    } completion:NULL];
    
    
    //    [UIView transitionWithView:self.view
    //                      duration:0.5f
    //                       options:UIViewAnimationOptionTransitionCrossDissolve
    //                    animations:^(void) {
    //
    //                        bubble.hidden = NO;
    //
    //
    //                    } completion:NULL];
    
    
    
    
}


#pragma mark Bubble Menu

-(void)createBubbleMenu
{
    if (bubble) {
        [bubble removeFromSuperview];
        bubble  = nil;
    }
    
    UIImage* btnimg = [UIImage imageNamed:@"bubble"];

    bubble = [bubbleVw sharedInstance:CGRectMake(100, 100, btnimg.size.width, btnimg.size.height) size:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)  ];
    
//    UIImage *bubbleMaskImg = [self maskImage:userProfileImage withMask:btnimg];
//    
//    [bubble setImage:bubbleMaskImg forState:UIControlStateNormal];
//    [bubble setImage:bubbleMaskImg forState:UIControlStateSelected];
//    [bubble setImage:bubbleMaskImg forState:UIControlStateSelected | UIControlStateHighlighted];
     [bubble.layer setCornerRadius:btnimg.size.width /2];
//    
    
//    UIImageView *tintView = [[UIImageView alloc]init];
//    [tintView setContentMode:UIViewContentModeScaleAspectFit];
//    UIImage * image = [UIImage imageNamed:@"bubble"];
//    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//    tintView.image = image;
//    tintView.translatesAutoresizingMaskIntoConstraints = NO;
//    [bubble addSubview:tintView];
    
    [self.view addSubview:bubble];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bubbleStatus:) name:@"bubbleStatus" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(menuBtnClicked:) name:@"menuBtnClicked" object:nil];
    
}

- (UIImage*) maskImage:(UIImage *)image withMask:(UIImage *)maskImage {
    
    CGImageRef maskRef = maskImage.CGImage;
    
    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                        CGImageGetHeight(maskRef),
                                        CGImageGetBitsPerComponent(maskRef),
                                        CGImageGetBitsPerPixel(maskRef),
                                        CGImageGetBytesPerRow(maskRef),
                                        CGImageGetDataProvider(maskRef), NULL, false);
    
    CGImageRef masked = CGImageCreateWithMask([image CGImage], mask);
    return [UIImage imageWithCGImage:masked];
}




//private Password Notification
-(void)bubbleStatus:(NSNotification *)notification
{
    
    NSDictionary *dict = [notification userInfo];
    
    if ([[dict objectForKey:@"bubbleStatus"] isEqualToString:@"Clicked"])
    {
        [self resignkeyBoard];
        [UIView transitionWithView:menuVw
                          duration:0.3
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            menuVw.hidden = NO;
                            menu.shouldShow = NO;
                            [menuVw open];
                            [menuVw setImage:bubble.imageView.image];

                            
                            
                        } completion:^(BOOL finished){
                        }];
        
    }
}

//menuBtnClicked Notification
-(void)menuBtnClicked:(NSNotification *)notification
{
    menu.shouldShow = YES;

    NSDictionary *dict = [notification userInfo];
    [bubble restoreBtn];
    [UIView transitionWithView:menuVw
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        menuVw.hidden = YES;
                        [menuVw _close:nil];
                        
                        
                        
                    } completion:^(BOOL finished){
                    }];
    if ([[dict objectForKey:@"menuBtnClicked"] isEqualToString:@"0"])
    {
        [UIView transitionWithView:chatTableView
                          duration:0.5f
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^(void) {
                            self.chatSlideMenu.frame = CGRectMake(self.view.frame.size.width+100, 0, 100, chatTableView.frame.size.height+HEIGHT_REDUCE);
                            [self changeViewToEditMode];
                        } completion:NULL];
        
        isMenuShowing = NO;
        
    }
    else if ([[dict objectForKey:@"menuBtnClicked"] isEqualToString:@"1"])
    {
        [self openPhotoAlbum];
        
    }
    else if ([[dict objectForKey:@"menuBtnClicked"] isEqualToString:@"2"])
    {
        [self takePhoto];
        
    }
    else if ([[dict objectForKey:@"menuBtnClicked"] isEqualToString:@"3"])
    {
        [self takevideo];
        
    }
    else if ([[dict objectForKey:@"menuBtnClicked"] isEqualToString:@"4"])
    {
        [self pushToView_RUTer:[self generateRuterDict]];
        
    }
    
    
}



- (void)startImageDownloadingForRecord:(FileRecord *)record msgId:(NSString*)msgId indexPath:(NSIndexPath*)indexpath
{
  //  NSLog(@"%@",indexpath);
    
    if (![self.pendingOperations.downloadsInProgress.allKeys containsObject:msgId]) {
        FileDownloader *imageDownloader = [[FileDownloader alloc] initWithFileRecord:record atIndexPath:indexpath msgId:msgId  delegate:self];
        [self.pendingOperations.downloadsInProgress setObject:imageDownloader forKey:msgId];
        [self.pendingOperations.downloadQueue addOperation:imageDownloader];
    }
}

/*
 #pragma mark - ImageDownloader delegate
 - (void)DownloaderDidFinish:(FileDownloader *)downloader {
 NSIndexPath *indexPath = downloader.indexPathInTableView;
 FileRecord *theRecord = downloader.fileRecord;
 theRecord.msgId = downloader.msgId;
 if (theRecord) {
 [self.photosDict setObject:theRecord forKey:theRecord.msgId];
 
 [chatTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:YES];
 
 [self.pendingOperations.downloadsInProgress removeObjectForKey:indexPath];
 }
 
 }
 
 #pragma mark - UIScrollView delegate
 
 - (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
 // 1: As soon as the user starts scrolling, you will want to suspend all operations and take a look at what the user wants to see.
 [self suspendAllOperations];
 }
 - (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
 // 2: If the value of decelerate is NO, that means the user stopped dragging the table view. Therefore you want to resume suspended operations, cancel operations for offscreen cells, and start operations for onscreen cells.
 if (!decelerate) {
 [self loadImagesForOnscreenCells];
 [self resumeAllOperations];
 }
 }
 - (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
 // 3: This delegate method tells you that table view stopped scrolling, so you will do the same as in #2.
 [self loadImagesForOnscreenCells];
 [self resumeAllOperations];
 }
 
 #pragma mark - Cancelling, suspending, resuming queues / operations
 - (void)suspendAllOperations {
 [self.pendingOperations.downloadQueue setSuspended:YES];
 }
 - (void)resumeAllOperations {
 [self.pendingOperations.downloadQueue setSuspended:NO];
 }
 - (void)cancelAllOperations {
 [self.pendingOperations.downloadQueue cancelAllOperations];
 }
 - (void)loadImagesForOnscreenCells {
 
 // 1: Get a set of visible rows.
 NSMutableDictionary* tempDict = [ self getMediaVisibleMsgIds:[chatTableView indexPathsForVisibleRows]];
 NSSet *visibleRows = (NSSet*)[tempDict objectForKey:@"visibleMediaIds"];
 NSMutableDictionary* indexDict = (NSMutableDictionary*)[tempDict objectForKey:@"indexDict"];
 
 // 2: Get a set of all pending operations (download and filtration).
 NSMutableSet *pendingOperations = [NSMutableSet setWithArray:[self.pendingOperations.downloadsInProgress allKeys]];
 
 NSMutableSet *toBeCancelled = [pendingOperations mutableCopy];
 NSMutableSet *toBeStarted = [visibleRows mutableCopy];
 
 // 3: Rows (or indexPaths) that need an operation = visible rows ñ pendings.
 [toBeStarted minusSet:pendingOperations];
 
 // 4: Rows (or indexPaths) that their operations should be cancelled = pendings ñ visible rows.
 [toBeCancelled minusSet:visibleRows];
 
 // 5: Loop through those to be cancelled, cancel them, and remove their reference from PendingOperations.
 for (NSString *msgId in toBeCancelled) {
 
 FileDownloader *pendingDownload = [self.pendingOperations.downloadsInProgress objectForKey:msgId];
 [pendingDownload cancel];
 [self.pendingOperations.downloadsInProgress removeObjectForKey:msgId];
 }
 toBeCancelled = nil;
 
 // 6: Loop through those to be started, and call startOperationsForFileRecord:atIndexPath: for each.
 for (NSString *msgId in toBeStarted) {
 
 FileRecord *recordToProcess;
 
 recordToProcess = [self.photosDict objectForKey:msgId];
 
 
 [self startImageDownloadingForRecord:recordToProcess msgId:msgId indexPath:(NSIndexPath*)[indexDict objectForKey:msgId]];
 }
 toBeStarted = nil;
 
 }
 
 -(NSMutableDictionary*)getMediaVisibleMsgIds:(NSArray*)visibleIndex
 {
 NSMutableDictionary* tempDict = [NSMutableDictionary new];
 NSMutableDictionary* indexDict = [NSMutableDictionary new];
 
 NSMutableSet *visibleMediaIds;
 for (NSIndexPath *IndexPath in visibleIndex) {
 
 XMPPMessageArchiving_Message_CoreDataObject *coremessage = [[self fetchedResultsController ] objectAtIndexPath:IndexPath];
 NSXMLElement *file = [coremessage.message elementForName:@"file"];
 if(file)
 {
 [visibleMediaIds addObject:coremessage.messageID];
 [indexDict setObject:IndexPath forKey:coremessage.messageID];
 
 }
 }
 
 if ([visibleMediaIds count])
 {
 [tempDict setObject:visibleMediaIds forKey:@"visibleMediaIds"];
 [tempDict setObject:indexDict forKey:@"indexDict"];
 }
 
 
 
 return tempDict;
 }
 
 -(UIImage*)getImage:(NSString*)msgId type:(NSString*)type indexPath:(NSIndexPath *)indexPath
 {
 UIImage* image;
 FileRecord *aRecord;
 
 if ([self.photosDict objectForKey:msgId]) {
 aRecord = [self.photosDict objectForKey:msgId];
 
 }
 else
 {
 mediaDbEditor* mediaInfo = [mediaDbEditor new];
 
 NSMutableDictionary* mediaDict = [mediaInfo fetchMedia:msgId];
 if (mediaDict) {
 FileRecord *record = [[FileRecord alloc] init];
 record.URL = [NSURL fileURLWithPath:[mediaDict objectForKey:@"url"]];
 record.type = type;
 
 [self.photosDict setObject:record forKey:msgId];
 aRecord = record;
 record = nil;
 }
 
 
 
 
 
 }
 
 if (aRecord.hasFile) {
 image = [UIImage generatePhotoThumbnail:aRecord.image withSide:220];
 
 
 }
 else if (aRecord.isFailed)
 image = [UIImage imageNamed:@"defaultPerson.png"];
 else {
 image = [UIImage imageNamed:@"defaultPerson.png"];
 
 if (!chatTableView.dragging && !chatTableView.decelerating) {
 [self startImageDownloadingForRecord:aRecord msgId:msgId indexPath:indexPath];
 }
 }
 return image;
 
 }
 
 #pragma mark  -PendingOperations
 - (PendingOperations *)pendingOperations {
 if (!_pendingOperations) {
 _pendingOperations = [[PendingOperations alloc] init];
 }
 return _pendingOperations;
 }
 */
-(void)dealloc
{
    
  //  NSLog(@"DEALLOC IN SINGLE CHAT");
    avatarImg  = nil;
    updateUnreadMsg = nil;
    messageMethods = nil;
}
- (void)didReceiveMemoryWarning

{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    [self.imageCache removeAllObjects];
  //  NSLog(@"RECEIVED MEMORY WARNINGS IN chatViewController");
    
}
@end
