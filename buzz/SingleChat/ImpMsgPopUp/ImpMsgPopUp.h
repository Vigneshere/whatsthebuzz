//
//  ImpMsgPopUp.h
//  buzz
//
//  Created by Shriram Rajendran on 24/06/15.
//  Copyright (c) 2015 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImpMsgPopUp : UIView <UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *imgMsgAry;
    NSString *bareJidStr;
    UIView *baseView;
}
@property (nonatomic,strong)UIImage *userDP;
@property (nonatomic,strong)UIImage *myDP;
@property (nonatomic)BOOL ImpFromGroup;


- (id)initWithFrame:(CGRect)frame view:(UIView *)view bareJIDStr:(NSString *)bareJIDStr;
-(void)setBackground:(UIView *)CurrentView;
@end
