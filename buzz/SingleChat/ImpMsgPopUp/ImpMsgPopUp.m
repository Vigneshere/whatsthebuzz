//
//  ImpMsgPopUp.m
//  buzz
//
//  Created by Shriram Rajendran on 24/06/15.
//  Copyright (c) 2015 Shailisolutions. All rights reserved.
//

#import "ImpMsgPopUp.h"
#import "NSBubbleData.h"
#import "UIBubbleTableViewCell.h"
#import "UIBubbleTypingTableViewCell.h"
#import "UIImageEffects.h"
#import "UIColor+HexColors.h"
@implementation ImpMsgPopUp

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (AppDelegate *)appDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (XMPPStream *)xmppStream
{
    return [[self appDelegate] xmppStream];
}
-(XMPPMessageArchivingCoreDataStorage *)xmppMessageArchivingCoreDataStorage
{
    
    return [[self appDelegate] xmppMessageArchivingStorage];
}

- (NSManagedObjectContext *)managedObjecMessageContact
{
    return [[[self appDelegate] xmppMessageArchivingStorage] mainThreadManagedObjectContext];
}

- (NSManagedObjectContext *)managedObjectContext_roster
{
    return [[[self appDelegate] xmppRosterStorage] mainThreadManagedObjectContext];
}


- (id)initWithFrame:(CGRect)frame view:(UIView *)view bareJIDStr:(NSString *)bareJIDStr
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = RGBA(0, 0, 0, 0.0);
        baseView = view;
        
        imgMsgAry = [self getImpMsgAlone:bareJIDStr];
        bareJidStr =bareJIDStr;
        NSLog(@"image Count-> %lu",(unsigned long)imgMsgAry.count);
        //[self setBackground:baseView];
        [self createTableVw];
    }
    return self;
}

-(void)createTableVw
{
    UITableView *imgTblView =[[UITableView alloc]initWithFrame:CGRectMake(0,70, self.frame.size.width, self.frame.size.height - 70) style:UITableViewStylePlain];
    imgTblView.delegate = self;
    imgTblView.dataSource = self;
    imgTblView.backgroundColor = [UIColor clearColor];
    imgTblView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:imgTblView];

    
}

-(NSMutableArray *)getImpMsgAlone:(NSString *)JidStr
{
    NSManagedObjectContext *moc = [[self xmppMessageArchivingCoreDataStorage] mainThreadManagedObjectContext];
    
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:moc];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:entity];
        NSString *predicateFrmt = @"bareJidStr == %@";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,JidStr];
        [fetchRequest setPredicate:predicate];
        [fetchRequest setSortDescriptors:[NSSortDescriptor sortValues:@"timestamp" ascending:YES]];
        NSArray *delArray=[moc executeFetchRequest:fetchRequest error:nil];
        
      NSMutableArray *impMsgAry = [NSMutableArray new];
        for (XMPPMessageArchiving_Message_CoreDataObject *CoreDataObject in delArray)
        {
            NSXMLElement *imp = [CoreDataObject.message elementForName:@"imp"];
            
            if (imp) {
                [impMsgAry addObject:CoreDataObject];
            }
        }
    
    return impMsgAry;
    

}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
   
    return imgMsgAry.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return tableView.sectionHeaderHeight+27;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(10,0,self.frame.size.width,tableView.sectionHeaderHeight+17)];
    customView.backgroundColor = [UIColor clearColor];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.backgroundColor = [UIColor blackColor];
    headerLabel.font = [UIFont fontWithName:FontString size:12];
    headerLabel.frame = CGRectMake((tableView.frame.size.width/2)- 75,0,150,20);
    headerLabel.textColor = [UIColor whiteColor];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.layer.cornerRadius = 10;
    headerLabel.clipsToBounds = YES;
    headerLabel.text = @"Important Messages";
    [customView addSubview:headerLabel];
    
    return customView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    id details = [self getBubbledata:indexPath];
    if ([details isKindOfClass:[NSBubbleData class]]) {
        NSBubbleData *data = (NSBubbleData *)details;
        CGFloat height = (CGFloat)MAX(data.insets.top + data.view.frame.size.height + data.insets.bottom+20,data.insets.top + data.view.frame.size.height + data.insets.bottom+30);
        
        
        details = nil; data = nil;
        
        return height;
    }
       else
        return (CGFloat)MAX([UIBubbleTypingTableViewCell height], 60);
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *Tablecell;
    
    
    static NSString *CellIdentifier;
    id details = [self getBubbledata:indexPath];
    if ([details isKindOfClass:[NSBubbleData class]])
    {
        NSBubbleData *data = (NSBubbleData *)details;
        CellIdentifier = @"thebubbletable";
        UIBubbleTableViewCell *cell = (UIBubbleTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = (UIBubbleTableViewCell *)[[UIBubbleTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        cell.data = data;
        
  
            cell.showAvatar = YES;
            cell.deleteCell = NO;
        
        Tablecell =  cell;
    }
        return Tablecell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(id)getBubbledata:(NSIndexPath *)indexPath {
    
    NSBubbleData *messageDetails;
    XMPPMessageArchiving_Message_CoreDataObject *coremessage = [imgMsgAry objectAtIndex:indexPath.row];
    
    
    
    
    NSXMLElement *element = [[NSXMLElement alloc] initWithXMLString:coremessage.messageStr error:nil];
    
    NSXMLElement *private = [coremessage.message elementForName:@"private"];
    
    
    if (_ImpFromGroup) {
        
        NSString *userStr = [self getDisplayName:[[coremessage.message from] resource]];
        if (userStr == nil) {
            userStr = [userStr stringByReplacingOccurrencesOfString:DOMAINAME withString:@""];
        }

        
        NSXMLElement *imp = [coremessage.message elementForName:@"imp"];

        
        if ([[coremessage.message from].resource isEqualToString:[[self xmppStream] myJID].user])
        {
                
                
                NSXMLElement *imp = [coremessage.message elementForName:@"imp"];
                
                if (imp)
                    
                {
                    NSData *newdata=[coremessage.body dataUsingEncoding:NSUTF8StringEncoding
                                                   allowLossyConversion:YES];
                    NSString *Bubblestring=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];
                    
                    
                    
                    if (private)
                    {
                        messageDetails = [NSBubbleData dataWithTextWithTag:Bubblestring date:coremessage.timestamp type:BubbleTypePrivateMine jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"group" messageID:coremessage.messageID impMsg:YES];
                    }
                    else
                    {
                        messageDetails = [NSBubbleData dataWithTextWithTag:Bubblestring date:coremessage.timestamp type:BubbleTypeMine jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"group" messageID:coremessage.messageID impMsg:YES];
                        
                        
                    }
                    
                    
                }
                
                
            }
            else
            {
                
                
                NSXMLElement *imp = [coremessage.message elementForName:@"imp"];
                
                
                if (imp)
                {
                    NSData *newdata=[coremessage.body dataUsingEncoding:NSUTF8StringEncoding
                                                   allowLossyConversion:YES];
                    NSString *Bubblestring=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];
                    
                    
                    if (private)
                    {
                        messageDetails = [NSBubbleData dataWithTextWithTag:[NSString stringWithFormat:@"%@\n%@",userStr,Bubblestring] date:coremessage.timestamp type:BubbleTypePrivateSomeoneElse jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"group" messageID:coremessage.messageID impMsg:YES];
                    }
                    else
                    {
                        messageDetails = [NSBubbleData dataWithTextWithTag:[NSString stringWithFormat:@"%@\n%@",userStr,Bubblestring] date:coremessage.timestamp type:BubbleTypeSomeoneElse jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"group" messageID:coremessage.messageID impMsg:YES];
                    }
                    
                    messageDetails.avatar = _userDP;
                    
                }
            }
            return messageDetails;

            
        }
    
    else
    {
    
    if ([[element attributeStringValueForName:@"to"] isEqualToString:bareJidStr]){
        
        
       
        NSXMLElement *imp = [coremessage.message elementForName:@"imp"];
     
        if (imp)
            
        {
            NSData *newdata=[coremessage.body dataUsingEncoding:NSUTF8StringEncoding
                                           allowLossyConversion:YES];
            NSString *Bubblestring=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];
            
            
            
            if (private)
            {
                messageDetails = [NSBubbleData dataWithTextWithTag:Bubblestring date:coremessage.timestamp type:BubbleTypePrivateMine jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"singlechat" messageID:coremessage.messageID impMsg:YES];
            }
            else
            {
                messageDetails = [NSBubbleData dataWithTextWithTag:Bubblestring date:coremessage.timestamp type:BubbleTypeMine jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"singlechat" messageID:coremessage.messageID impMsg:YES];
                
                
            }
           
            
            messageDetails.avatar = _myDP;
        }
        
            
               }
    else
    {
        
     
        NSXMLElement *imp = [coremessage.message elementForName:@"imp"];
        
        
           if (imp)
        {
            NSData *newdata=[coremessage.body dataUsingEncoding:NSUTF8StringEncoding
                                           allowLossyConversion:YES];
            NSString *Bubblestring=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];
            
            
            if (private)
            {
                messageDetails = [NSBubbleData dataWithTextWithTag:Bubblestring date:coremessage.timestamp type:BubbleTypePrivateSomeoneElse jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"singlechat" messageID:coremessage.messageID impMsg:YES];
            }
            else
            {
                messageDetails = [NSBubbleData dataWithTextWithTag:Bubblestring date:coremessage.timestamp type:BubbleTypeSomeoneElse jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"singlechat" messageID:coremessage.messageID impMsg:YES];
            }
            
            messageDetails.avatar = _userDP;
            
        }
    }
    return messageDetails;
    }
    
}

-(void)setBackground:(UIView *)CurrentView;
{
    CGRect rect = [baseView bounds];
    UIGraphicsBeginImageContextWithOptions(rect.size,YES,0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [baseView.layer renderInContext:context];
    UIImage *capturedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    capturedImage = [UIImageEffects imageByApplyingLightEffectToImage:capturedImage];
    self.backgroundColor = [UIColor colorWithPatternImage:capturedImage];
    
//    [baseView addSubview:self];

}


-(NSString *)getDisplayName:(NSString *)resourceStr
{
    if ([resourceStr rangeOfString:DOMAINAME].location==NSNotFound )
        resourceStr = [resourceStr stringByAppendingString:[NSString stringWithFormat:@"@%@",DOMAINAME]];
    
    if ([resourceStr isEqualToString:[self xmppStream].myJID.bare ])
        return @"You";
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:[self managedObjectContext_roster]];
    [request setEntity:entity];
    NSString *predicateFrmt = @"jidStr = %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, resourceStr];
    [request setPredicate:predicate];
    NSArray *empArray=[[self managedObjectContext_roster] executeFetchRequest:request error:nil];
    if ([empArray count] == 1)
    {
        for (XMPPUserCoreDataStorageObject *userdetails in  empArray)
        {
            return userdetails.displayName;
        }
    }
    return [resourceStr stringByReplacingOccurrencesOfString:DOMAINAME withString:@""];;
}






@end
