//
//  chatViewController.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 22/02/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XMPPRosterCoreDataStorage.h"
#import "HPGrowingTextView.h"
#import "Singleton.h"
#import "NSBubbleData.h"
#import "UIImage+animatedGIF.h"
#import "NSHeaderData.h"
#import "UIBubbleHeaderTableViewCell.h"
#import <CoreText/CoreText.h>
#import "UIBubbleTypingTableViewCell.h"
#import "UIBubbleTableViewCell.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "PECropViewController.h"
#import "PendingOperations.h"
#import "FileDownloader.h"
#import "FileRecord.h"
#import "SecureGalleryViewController.h"


@interface chatViewController : UIViewController<HPGrowingTextViewDelegate,NSFetchedResultsControllerDelegate,FileDownloaderDelegate,PECropViewControllerDelegate,UIActionSheetDelegate,UIAlertViewDelegate,UIGestureRecognizerDelegate>
{
        NSFetchedResultsController *fetchedResultsController;
        NSUInteger pushIndex;
    NSIndexPath *pushIndexPath;
}
@property(nonatomic,retain)XMPPUserCoreDataStorageObject *user;
@property (strong, nonatomic) Singleton* singleton;
@property (nonatomic, strong) NSMutableArray *photos;
@property(nonatomic,strong)NSMutableDictionary *photosDict;
@property (nonatomic, strong) PendingOperations *pendingOperations;


-(NSBubbleData*)constructMediaViewBubledata:(NSMutableDictionary*)mediaDict :(NSBubbleData*)Bubbledata;

@end
