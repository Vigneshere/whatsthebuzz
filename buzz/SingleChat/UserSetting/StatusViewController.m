//
//  StatusViewController.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 18/03/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "StatusViewController.h"
#define intialStatusAry @[@"Using WhatsTheBuzz!😊",@"Feeling excited!",@"Bored",@"Single & Happy!",@"Monday Morning 😟",@"Hooray! Weekend",@"Party Hard!🍻",@"Watching Movie!",@"Broken up!"]
#define checkIndex @"checkIndex"
#define statusAry @"statusArray"
#define status @"status"
@interface StatusViewController ()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UILabel *titleLabel;
    UITextField *statusTextView;
    UITableView *statusTableView;
    NSMutableArray *statusArray;
    NSUserDefaults* statusDefaults;
    UIButton *doneBtn,*deleteAllBtn;
    UILabel *currentStatusLabel;
    UILabel *newStatusLabel;
    UIButton *editButton;
    UIView *statusView;

}
@property (nonatomic, retain) NSIndexPath* checkedIndexPath;

@end

@implementation StatusViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Appdelegate Values
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (AppDelegate *)appDelegate
{
	return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (XMPPStream *)xmppStream
{
	return [[self appDelegate] xmppStream];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    titleLabel = [UILabel new];
    statusTextView = [UITextField new];
    statusTableView = [UITableView new];
    currentStatusLabel = [UILabel new];
    newStatusLabel = [UILabel new];
     statusDefaults = [NSUserDefaults standardUserDefaults];
    
    if(IS_IPHONE_4)
    {
        self.view.backgroundColor = BACKGROUNDIMAGE_4;
        
    }
    else
    {
        self.view.backgroundColor = BACKGROUNDIMAGE;
        
    }

    [[NSUserDefaults standardUserDefaults] setObject:intialStatusAry forKey:statusAry];

    if ([statusDefaults mutableArrayValueForKey:statusAry].count == 0 && ![statusDefaults boolForKey:@"NoStatusDetails"]) {
        [[NSUserDefaults standardUserDefaults] setObject:intialStatusAry forKey:statusAry];
        [statusDefaults setBool:YES forKey:@"NoStatusDetails"];
    }
    [statusDefaults synchronize];

    
    editButton = [UIButton buttonWithType:UIButtonTypeCustom];
    editButton.frame = CGRectMake(self.view.frame.size.width-65,10,60, 25);
    [editButton setTitle:@"Edit" forState:UIControlStateNormal];
    editButton.titleLabel.font  = [UIFont fontWithName:FontString size:15];
    [editButton setTitleColor:RGBA(53, 152, 219, 1) forState:UIControlStateNormal];
    [editButton addTarget:self action:@selector(editBarButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    editButton.layer.cornerRadius = 12;
    editButton.layer.borderColor =  RGBA(53, 152, 219, 1).CGColor;
    editButton.layer.borderWidth = 1.2f;
 
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController.navigationBar addSubview:editButton];
    
    titleLabel.frame = CGRectMake(0,5,self.navigationController.navigationBar.frame.size.width, 34);
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = [UIColor darkGrayColor];
    titleLabel.text = @"Status";
    titleLabel.font = [UIFont fontWithName:FontString size:20];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.navigationController.navigationBar addSubview:titleLabel];
    
    currentStatusLabel.font = [UIFont fontWithName:FontString size:18];
    currentStatusLabel.frame = CGRectMake(10, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height+10, self.navigationController.navigationBar.frame.size.width, currentStatusLabel.font.lineHeight);
    currentStatusLabel.backgroundColor = [UIColor clearColor];
    currentStatusLabel.textColor = RGBA(53, 152, 219, 1);
    currentStatusLabel.text = @"Your Current Status:";
    currentStatusLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:currentStatusLabel];
    
    statusView = [UIView new];
    statusView.frame = CGRectMake(10, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height+40, self.view.frame.size.width - 20, 40);
    statusView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.3];
    statusView.layer.cornerRadius = 10;
    [self.view addSubview:statusView];

    statusTextView.frame = CGRectMake(statusView.frame.origin.x+10, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height+40, self.view.frame.size.width - 20, 40);
    statusTextView.backgroundColor = [UIColor clearColor];
    statusTextView.textColor = [UIColor darkGrayColor];
    statusTextView.font = [UIFont fontWithName:FontString size:15];
    statusTextView.delegate = self;
    statusTextView.keyboardType = UIKeyboardTypeDefault;
    statusTextView.clearButtonMode = YES;
    [self.view addSubview:statusTextView];
   
    doneBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    doneBtn.frame = CGRectMake(self.view.frame.size.width + 45, statusView.frame.origin.y+8, 40, 25);
    [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
    [doneBtn setTitleColor:RGBA(53, 152, 219, 1) forState:UIControlStateNormal];
    doneBtn.backgroundColor = [UIColor clearColor];
    [doneBtn addTarget:self action:@selector(doneBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    doneBtn.titleLabel.font = [UIFont fontWithName:FontString size:13];
    doneBtn.layer.cornerRadius = 12;
    doneBtn.layer.borderColor =  RGBA(53, 152, 219, 1).CGColor;
    doneBtn.layer.borderWidth = 1.2f;
    [self.view addSubview:doneBtn];
    
    newStatusLabel.font = [UIFont fontWithName:FontString size:18];
    newStatusLabel.frame = CGRectMake(10, statusView.frame.origin.y+statusView.frame.size.height+15, 200, newStatusLabel.font.lineHeight);
    newStatusLabel.backgroundColor = [UIColor clearColor];
    newStatusLabel.textColor = RGBA(53, 152, 219, 1);
    newStatusLabel.text = @"Select your new status";
    newStatusLabel.adjustsFontSizeToFitWidth = YES;
    newStatusLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:newStatusLabel];
    
    deleteAllBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    deleteAllBtn.frame = CGRectMake(self.view.frame.size.width - 85, statusView.frame.origin.y+statusView.frame.size.height+15, 80, 25);
    [deleteAllBtn setTitle:@"DeleteAll" forState:UIControlStateNormal];
    [deleteAllBtn setTitleColor:RGBA(53, 152, 219, 1) forState:UIControlStateNormal];
    deleteAllBtn.backgroundColor = [UIColor clearColor];
    [deleteAllBtn addTarget:self action:@selector(DeleteAllBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    deleteAllBtn.titleLabel.font  = [UIFont fontWithName:FontString size:15];
    deleteAllBtn.layer.cornerRadius = 12;
    deleteAllBtn.layer.borderColor =  RGBA(53, 152, 219, 1).CGColor;
    deleteAllBtn.layer.borderWidth = 1.2f;
    [self.view addSubview:deleteAllBtn];
    deleteAllBtn.hidden = YES;
    deleteAllBtn.alpha = 0.0;
    
    statusTextView.text = [[NSUserDefaults standardUserDefaults] objectForKey:status];
    
    

    
    
    statusTableView.frame = CGRectMake(10, statusTextView.frame.origin.y+statusTextView.frame.size.height+45, self.view.frame.size.width-20, self.view.frame.size.height-(newStatusLabel.frame.origin.y+newStatusLabel.frame.size.height+40));
    statusTableView.delegate = self;
    statusTableView.dataSource = self;
    statusTableView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.3];
    statusTableView.opaque = YES;
    statusTableView.layer.cornerRadius  = 10;
    [self.view addSubview:statusTableView];
    
    [self ChangeTheHeightOfStatusTableView];

}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];

    [titleLabel removeFromSuperview];
    [statusDefaults synchronize];
    [editButton removeFromSuperview];
    
       
}
-(void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    statusTextView.frame = CGRectMake(statusView.frame.origin.x+10, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height+40, statusView.frame.size.width-10, 40);
    

}



-(void)ChangeTheHeightOfStatusTableView
{
    int i = 8;
//    if (IS_IPHONE_5) {
//        i = 9;
//        statusTableView.frame = CGRectMake(10,statusTextView.frame.origin.y+statusTextView.frame.size.height+45, self.view.frame.size.width-20, self.view.frame.size.height-(statusTextView.frame.origin.y+statusTextView.frame.size.height+50));
//    }
//    else
//        statusTableView.frame = CGRectMake(10, statusTextView.frame.origin.y+statusTextView.frame.size.height+45, self.view.frame.size.width-20, self.view.frame.size.height-(statusTextView.frame.origin.y+statusTextView.frame.size.height+50));
    
  
        NSUInteger tableViewheight = [[statusDefaults mutableArrayValueForKey:statusAry]count];
        if(tableViewheight != 0)
        {
            statusTableView.frame = CGRectMake(10, statusTextView.frame.origin.y+statusTextView.frame.size.height+45, self.view.frame.size.width-20, tableViewheight*40);
        }
        else
        {
            statusTableView.frame = CGRectMake(10, statusTextView.frame.origin.y+statusTextView.frame.size.height+45, self.view.frame.size.width-20, 50);
        }
    
    CGRect tableRect = statusTableView.frame;
    if (tableRect.size.height >= self.view.frame.size.height-(newStatusLabel.frame.origin.y+newStatusLabel.frame.size.height+40) )
    {
        statusTableView.frame = CGRectMake(10, statusTextView.frame.origin.y+statusTextView.frame.size.height+45, self.view.frame.size.width-20, self.view.frame.size.height-(newStatusLabel.frame.origin.y+newStatusLabel.frame.size.height+40));
    }
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark TableView DataSource and Delegate Methods
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    return [statusDefaults mutableArrayValueForKey:statusAry].count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MyIdentifier"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
    }

    if ([statusDefaults objectForKey:checkIndex] && [[statusDefaults objectForKey:checkIndex] intValue] == indexPath.row)
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
       else
        cell.accessoryType = UITableViewCellAccessoryNone;
    
    cell.textLabel.text = [[statusDefaults mutableArrayValueForKey:statusAry] objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont fontWithName:FontString size:15];
    cell.textLabel.textColor = [UIColor darkGrayColor];
    return  cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [statusTextView resignFirstResponder];
    statusTextView.text = [[statusDefaults mutableArrayValueForKey:statusAry] objectAtIndex:indexPath.row];
    [self updateStatus:statusTextView.text];
    if([statusDefaults objectForKey:checkIndex])
    {
        UITableViewCell* uncheckCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[[statusDefaults objectForKey:checkIndex] intValue] inSection:0]];
        uncheckCell.accessoryType = UITableViewCellAccessoryNone;
    }
    if([[statusDefaults objectForKey:checkIndex] intValue] == indexPath.row)
    {
        UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    [statusDefaults setObject:[NSNumber numberWithInteger:indexPath.row] forKey:checkIndex];
    [statusDefaults setObject:statusTextView.text forKey:status];

}
-(void)doneBtnAction:(id)sender
{
    if ([statusTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length >0) {
        
    [statusTextView resignFirstResponder];
    [self updateStatus:statusTextView.text];
    [statusDefaults setObject:statusTextView.text forKey:status];
    if (![[statusDefaults mutableArrayValueForKey:statusAry] containsObject:statusTextView.text])
    {
         [[statusDefaults mutableArrayValueForKey:statusAry] insertObject:statusTextView.text atIndex:0];
         [statusDefaults setObject:@0 forKey:checkIndex];
    }
    else
            [statusDefaults setObject:[NSNumber numberWithInteger:[[statusDefaults mutableArrayValueForKey:statusAry] indexOfObject: statusTextView.text]] forKey:checkIndex];
    }
         [statusTableView reloadData];
    [self ChangeTheHeightOfStatusTableView];
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [[statusDefaults mutableArrayValueForKey:statusAry] removeObjectAtIndex:indexPath.row];
        if (indexPath.row == [[statusDefaults objectForKey:checkIndex] intValue])
            [statusDefaults removeObjectForKey:checkIndex];
    }
        [statusTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [self ChangeTheHeightOfStatusTableView];
    
//    if ([statusDefaults mutableArrayValueForKey:statusAry].count == 0)
//        [self changeDeleteAllButton:YES];

}
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    [[statusDefaults mutableArrayValueForKey:statusAry] exchangeObjectAtIndex:fromIndexPath.row withObjectAtIndex:toIndexPath.row];
    [statusDefaults setObject:[NSNumber numberWithInteger:toIndexPath.row] forKey:checkIndex];
}

-(void)changeDeleteAllButton:(BOOL)value
{
    [UIView animateWithDuration:1.0  animations:^{
        deleteAllBtn.hidden = value;
        if (value)
            deleteAllBtn.alpha = 0.0;
        else
            deleteAllBtn.alpha = 1.0;
        
    }];
}
- (void)editBarButtonPressed:(UIButton *)sender{
    
    if (statusTableView.editing == NO) {
        [sender setTitle:@"Done" forState:UIControlStateNormal];
        [statusTableView setEditing:YES animated:YES];
        [self changeDeleteAllButton:NO];

    } else {
        [sender setTitle:@"Edit" forState:UIControlStateNormal];
        [statusTableView setEditing:NO animated:YES];
        [self changeDeleteAllButton:YES];
    }
    
//    if ([statusDefaults mutableArrayValueForKey:statusAry].count == 0)
//        [self changeDeleteAllButton:YES];
    
    
}
-(void)DeleteAllBtnAction:(id)sender
{
    [statusDefaults removeObjectForKey:statusAry];
    [statusDefaults removeObjectForKey:checkIndex];
    [statusDefaults setBool:YES forKey:@"NoStatusDetails"];
    [self ChangeTheHeightOfStatusTableView];
    [statusTableView reloadData];

}
-(void)updateStatus:(NSString *)str
{
    XMPPPresence *presence = [XMPPPresence presence];
    NSXMLElement *show = [NSXMLElement elementWithName:@"show" stringValue:@"Available"];
    NSXMLElement *statusVal = [NSXMLElement elementWithName:@"status" stringValue:statusTextView.text];
    [presence addChild:show];
    [presence addChild:statusVal];
    [[self xmppStream] sendElement:presence];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    [self animateTextView:YES];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    return textField.text.length + (string.length - range.length) <= 30;
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextView:NO];

}
-(void)animateTextView:(BOOL)value
{
    if (value == YES) {
        [UIView animateWithDuration:.3
                         animations:^ {
                             CGRect newBounds = statusView.frame;
                             newBounds.size.width -= 40;
                             statusView.frame = newBounds;
                             [statusView layoutSubviews];
                             CGRect btnBounds  = doneBtn.frame;
                             btnBounds.origin.x = statusView.frame.origin.x+statusView.frame.size.width+5;
                             doneBtn.frame =btnBounds;
                             
                         }];
    }
    else{
        [UIView animateWithDuration:.3
                         animations:^ {
                             CGRect newBounds = statusView.frame;
                             newBounds.size.width += 40;
                             statusView.frame = newBounds;
                             [statusView layoutSubviews];

                             CGRect btnBounds  = doneBtn.frame;
                             btnBounds.origin.x = statusView.frame.origin.x+statusView.frame.size.width+10;
                             doneBtn.frame =btnBounds;
                             
                         }];
    }

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
