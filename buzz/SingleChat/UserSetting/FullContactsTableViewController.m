//
//  FullContactsTableViewController.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 27/06/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "FullContactsTableViewController.h"
#import "contactData.h"
#import "ProfileViewController.h"
#import <MessageUI/MessageUI.h>

@interface FullContactsTableViewController ()<UISearchBarDelegate,UISearchDisplayDelegate,MFMessageComposeViewControllerDelegate>
{
    UISearchDisplayController *searchDisplayController;
    NSMutableArray *sortedKeys;
    NSMutableArray *searchArray;
    ProfileViewController *contactInvite;
    UIButton *cancelButton;
    UIButton *doneButton;
}
@property(nonatomic,strong)NSMutableArray *contactArray;
@property(nonatomic,strong)UISearchBar *searchBar;
@end

@implementation FullContactsTableViewController


#pragma mark - View Life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = RGBA(249, 249, 249, 1);
    
    
    //==> Navigation Buttons
    cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    //==>Search Display Bar
    _searchBar = [[UISearchBar alloc] init];
    _searchBar.delegate = self;
    searchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:self.searchBar contentsController:self];
    searchDisplayController.delegate = self;
    searchDisplayController.searchResultsDataSource = self;
    searchDisplayController.searchResultsDelegate = self;
    searchDisplayController.navigationItem.title = @"search";
    self.tableView.tableHeaderView = searchDisplayController.searchBar;
    
    //==>Makes the scroll Soomth When Clicking on search Bar
    [self setAutomaticallyAdjustsScrollViewInsets:YES];
    [self setExtendedLayoutIncludesOpaqueBars:YES];
    
    //==>AddArray and remove array used when contactData phonenumbers have more than one data.<==//-->Start
    NSMutableArray *addArray = [NSMutableArray new];
    NSMutableArray *removeArray = [NSMutableArray new];
    
    
    if ([self appDelegate].contactsAry.count)
    {
        self.contactArray = [[self appDelegate].contactsAry mutableCopy];
        for (contactData *data in self.contactArray)
        {
            
            if (data.phoneNumbers.count > 1)
            {
                [removeArray addObject:data];
                for (id number in data.phoneNumbers)
                {
                    contactData *newperson = [contactData new];
                    newperson.phoneNumbers = [@[number] mutableCopy];
                    newperson.firstNames = data.firstNames;
                    newperson.lastNames = data.lastNames;
                    [addArray addObject:newperson];
                }
            }
        }
        
        [self.contactArray addObjectsFromArray:addArray];
        [self.contactArray removeObjectsInArray:removeArray];
        
        if ([self.controlString isEqualToString:@"invite"])
        {
            
            //==>Remove and add IM contacts
            NSMutableArray *removeforImContactsArray = [NSMutableArray new];
            NSMutableArray *addImcontactsArray = [NSMutableArray new];
            
            for (contactData *data in self.contactArray)
            {
                if (data.phoneNumbers.count)
                {
                NSString *dataPhoneNumber = [[data.phoneNumbers[0] componentsSeparatedByCharactersInSet:
                                              [[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"]
                                               invertedSet]]
                                             componentsJoinedByString:@""];
                
                //==>Checking Phone contacts with Im contacts
                [self getDisplayName:[XMPPJID jidWithString:[NSString stringWithFormat:@"%@@%@",dataPhoneNumber,DOMAINAME]] compeletion:^(BOOL value)
                 {
                     if (value == YES)//==>if value == YES IM Contact Or Invite Contact
                     {
                         [removeforImContactsArray addObject:data];
                          data.type = @"imContact";
                         [addImcontactsArray addObject:data];
                     }
                     
                 }];
                    
                dataPhoneNumber = nil;
                }
            }
            [self.contactArray removeObjectsInArray:removeforImContactsArray];
            
            //==>SOS For future Expansion.
            if ([self.controlString isEqualToString:@"sos"])
                [self.contactArray addObjectsFromArray:addImcontactsArray];
            
            removeforImContactsArray = nil;
            addImcontactsArray = nil;
        }
    }
    
    addArray = nil;
    removeArray = nil;
    //==>AddArray and remove array used when contactData phonenumbers have more than one data.<==//-->End

    
    //==>Array with selected items.
    self.contactSelectArray = [NSMutableArray new];
    
    //==>Sort the Contact list according to alpabetical order
    self.contactArray = [[self.contactArray sortedArrayUsingDescriptors:[NSSortDescriptor sortValues:@"firstNames" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]] mutableCopy];
    

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [self.contactSelectArray removeAllObjects];
    self.navigationController.navigationBar.barTintColor = RGBA(53, 152, 219, 1);
    self.navigationController.navigationBar.translucent  = NO;
    
    UIImage *backButtonImage = [UIImage imageNamed:@"back"];
    cancelButton.frame = CGRectMake(0, 10, backButtonImage.size.width, backButtonImage.size.height);
    [cancelButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.navigationBar addSubview:cancelButton];
    
    doneButton.frame = CGRectMake(self.view.frame.size.width-80, 10, 80, backButtonImage.size.height);
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [doneButton addTarget:self action:@selector(doneButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.navigationBar addSubview:doneButton];
    
    if([self.controlString isEqualToString:@"invite"])
        [doneButton setTitle:@"Send" forState:UIControlStateNormal];
    else
        [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    
    [self.tableView reloadData];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [cancelButton removeFromSuperview];
    [doneButton removeFromSuperview];
}
#pragma mark - cancel Button clicked
-(void)cancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - Send Button clicked

-(void)doneButtonClicked
{
    NSMutableArray *numberArray = [NSMutableArray new];
    for (contactData *data in self.contactSelectArray)
    {
        if (data.phoneNumbers.count)
        {
            [numberArray addObject:data.phoneNumbers[0]];

        }
    }

    
    if ([MFMessageComposeViewController canSendText])
    {
        MFMessageComposeViewController *messageComposer =
        [[MFMessageComposeViewController alloc] init];
        NSString *message = @"Hi There! I'm greatly excited in inviting you to use WhatstheBuzz App. Try it now!";
        [messageComposer setBody:message];
        messageComposer.messageComposeDelegate = self;
        messageComposer.recipients = numberArray;
        [self presentViewController:messageComposer animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"OOPS!" message:@"Sending messages not allowed!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }

  /*
    [self dismissViewControllerAnimated:YES completion:^
     {
         if ([self.controlString isEqualToString:@"invite"]) {
            
             //==>Get the Numbers to send SMS
             NSMutableArray *numberArray = [NSMutableArray new];
             for (contactData *data in self.contactSelectArray)
             {
                 [numberArray addObject:data.phoneNumbers[0]];
                 
             }
             
             //==>MFMessageComposeViewController to send Invite SMS
             if ([MFMessageComposeViewController canSendText]) {
                 MFMessageComposeViewController *messageComposer =
                 [[MFMessageComposeViewController alloc] init];
                 NSString *message = @"Your Message here";
                 [messageComposer setBody:message];
                 messageComposer.messageComposeDelegate = self;
                 messageComposer.recipients = numberArray;
                 [self presentViewController:messageComposer animated:YES completion:nil];
             }
         }
         //==>SOS For future Expansion.
         else if([self.controlString isEqualToString:@"sos"])
         {
             
         }
     }];*/
}

#pragma mark -MFMessageComposeViewController Delegate Method
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result)
	{
		case MessageComposeResultCancelled:
			NSLog(@"Result: SMS sending canceled");
			break;
		case MessageComposeResultSent:
        {
            UIAlertView*  alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                             message:@"Message sent"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
            
            [alert show];
        }
			break;
		case MessageComposeResultFailed:
        {
            UIAlertView*  alert = [[UIAlertView alloc] initWithTitle:@"OOPS"
                                                             message:@"Message sending failed"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
            
            [alert show];
        }
			break;
		default:
        {
          NSLog( @"Result: SMS not sent");
            UIAlertView*  alert = [[UIAlertView alloc] initWithTitle:@"OOPS"
                                                             message:@"Message not sent"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
            
            [alert show];
        }
			break;
	}
    
	[self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Appdelegate Method
-(AppDelegate *)appDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
#pragma mark - Table view data source and delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
        return [searchArray count];
    else
        return self.contactArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier  = @"contactListIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UIView *checkView = [UIView new];
        checkView.frame = CGRectMake(cell.frame.size.width-50, 15, 20, 20);
        checkView.backgroundColor = RGBA(249, 249, 249, 1);
        checkView.tag = 1;
        checkView.layer.cornerRadius = 10;
        checkView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        checkView.layer.borderWidth = 0.5;
        [cell.contentView addSubview:checkView];
    
    }
    
    contactData *data;
    if (tableView == self.searchDisplayController.searchResultsTableView)
        data = searchArray[indexPath.row];
    else
        data = self.contactArray[indexPath.row];
    
    cell.textLabel.text  =[NSString stringWithFormat:@"%@  %@",data.firstNames,data.lastNames];
    cell.textLabel.font  = [UIFont fontWithName:FontString size:16];
    
    if (data.phoneNumbers.count)
    {
    cell.detailTextLabel.text = [[data.phoneNumbers[0] componentsSeparatedByCharactersInSet:
                                  [[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"]
                                   invertedSet]]
                                 componentsJoinedByString:@""];
    }
    cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    
    UIView *checkView = (UIView *)[cell.contentView viewWithTag:1];
    
    if ([self.contactSelectArray containsObject:data])
        checkView.backgroundColor = RGBA(53, 152, 219, 1);
    else
        checkView.backgroundColor = RGBA(249, 249, 249, 1);
    data = nil;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    contactData *data;
    
    if(tableView ==  self.searchDisplayController.searchResultsTableView)
        data = searchArray[indexPath.row];
    else
        data = self.contactArray[indexPath.row];
    
    //==>Adding and removing the selected item in the contact list
    if ([self.contactSelectArray containsObject:data])
        [self.contactSelectArray removeObject:data];
    else
        [self.contactSelectArray addObject:data];
    
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation: UITableViewRowAnimationFade];
    data = nil;
}
#pragma mark - Search Display Delegate Methods
- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [searchArray removeAllObjects];
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"(%K CONTAINS[c] %@) OR (%K CONTAINS[c] %@)",
                               @"firstNames", searchString, @"lastNames", searchString];
    searchArray = [NSMutableArray arrayWithArray:[self.contactArray filteredArrayUsingPredicate:predicate2]];
    return YES;
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.tableView reloadData];
}
#pragma mark - Roster managedObjectContext
- (NSManagedObjectContext *)managedObjectContext_roster
{
	return [[(AppDelegate *)[[UIApplication sharedApplication]delegate] xmppRosterStorage] mainThreadManagedObjectContext];
}
#pragma mark - Method to check IM contact
- (void)getDisplayName:(XMPPJID *)Str compeletion:(void (^)(BOOL))completion
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject" inManagedObjectContext:[self managedObjectContext_roster]];
    [request setEntity:entity];
    NSString *predicateFrmt = @"jidStr = %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, Str.bareJID];
    [request setPredicate:predicate];
    NSArray *empArray=[[self managedObjectContext_roster] executeFetchRequest:request error:nil];
    if ([empArray count] == 1)
    completion(YES);
    else
        completion(NO);
}

@end
