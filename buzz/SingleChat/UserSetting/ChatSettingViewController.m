//
//  ChatSettingViewController.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 12/06/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "ChatSettingViewController.h"
#import "TextSizeSettingViewController.h"
#import "MediaSettingViewController.h"
#import "mediaDbEditor.h"
#define ChatSettingArray @[@"Text size",@"Turn on Bubble Menu",@"Quick Visibility",@"Clear history"]

@interface ChatSettingViewController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation ChatSettingViewController
{
    UITableView *chatSettingtableView;
    UILabel *titleLabel;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.view.backgroundColor = BACKGROUNDIMAGE;
    
    chatSettingtableView = [UITableView new];
    chatSettingtableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    chatSettingtableView.delegate = self;
    chatSettingtableView.dataSource = self;
    chatSettingtableView.backgroundColor = [UIColor clearColor];
    chatSettingtableView.separatorColor = [UIColor clearColor];
    [self.view addSubview:chatSettingtableView];
    
    titleLabel = [UILabel new];

    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    titleLabel.frame = CGRectMake(0, 10, self.view.frame.size.width, 30);
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = @"Chat Settings";
    titleLabel.font = [UIFont fontWithName:FontString size:20];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.navigationController.navigationBar addSubview:titleLabel];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [titleLabel removeFromSuperview];
}
-(void)backbuttonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Tableview DataSource AND Delgate Methods
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    return ChatSettingArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"chatSettingtableViewIdentifier"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"chatSettingtableViewIdentifier"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(cell.frame.origin.x+10, 49, self.view.frame.size.width-20, 1)];
        separatorLineView.backgroundColor = RGBA(204, 204, 204, 0.4);
        [cell.contentView addSubview:separatorLineView];
        
        UISwitch *switchButton = [[UISwitch alloc] initWithFrame:CGRectMake(self.view.frame.size.width-70, 10, 25, 25)];
        switchButton.tag = 100;
        [switchButton addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
        [cell.contentView addSubview:switchButton];
        
        
    }
    cell.textLabel.text = [ChatSettingArray objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont fontWithName:FontString size:16];
    cell.textLabel.textColor = RGBA(130, 130, 130, 1);
    
    if (indexPath.row == 3 || indexPath.row == 0)
    {
        [[cell.contentView viewWithTag:100] removeFromSuperview];
        [[cell.contentView viewWithTag:200] removeFromSuperview];

    }
     if (indexPath.row == 1)
     {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:TURN_BUBBLE_MENU] == YES)
            [(UISwitch *)[cell.contentView viewWithTag:100] setOn:YES];
        else
            [(UISwitch *)[cell.contentView viewWithTag:100] setOn:NO];
    }
    
    if (indexPath.row == 2)
    {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:ONLINE_OFFLINE_VISIBILITY] == YES)
            [(UISwitch *)[cell.contentView viewWithTag:100] setOn:YES];
        else
            [(UISwitch *)[cell.contentView viewWithTag:100] setOn:NO];
    }
    else if (indexPath.row == 3)
    {
        cell.textLabel.textColor  = [UIColor redColor];

    }
    
    return  cell;
}
-(void)switchChanged:(id)sender
{
    UISwitch *switchButton  = (UISwitch *)sender;
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:chatSettingtableView];
    NSIndexPath *indexPath = [chatSettingtableView indexPathForRowAtPoint:buttonPosition];
  //  NSLog(@"%ld",(long)indexPath.row);
    
    if (indexPath.row == 1)
    {
        if (switchButton.isOn)
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:TURN_BUBBLE_MENU];
        else
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:TURN_BUBBLE_MENU];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
    }
    else
    {
            if (switchButton.isOn)
            {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:ONLINE_OFFLINE_VISIBILITY];

            }
          else
        {
          
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:ONLINE_OFFLINE_VISIBILITY];

        }
        
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    switch (indexPath.row) {
//        case 0:
//        {
//            MediaSettingViewController *mediaDownloadSetting  = [MediaSettingViewController new];
//            [self.navigationController pushViewController:mediaDownloadSetting animated:YES];
//        }
//            
//            break;
        case 0:
        {
            TextSizeSettingViewController *textSizeSetting  = [TextSizeSettingViewController new];
            [self.navigationController pushViewController:textSizeSetting animated:YES];
        }
            break;
        
        case 3:
            
            [self clearAllHistory];
            break;
            
        default:
            break;
    }
    
}
-(void)clearAllHistory
{
   
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Clear history"
                                                    message:@"Are you sure want to clear history!"
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    [alert show];

}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // the user clicked OK
    if (buttonIndex == 1) {
        XMPPMessageArchivingCoreDataStorage *storage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
        NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
        NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:moc];
        NSFetchRequest *request = [[NSFetchRequest alloc]init];
       [request setEntity:entityDescription];
        NSError *error;
        NSArray *messages = [moc executeFetchRequest:request error:&error];
        
        for (XMPPMessageArchiving_Message_CoreDataObject *coremessage in messages) {
            
            NSXMLElement *file = [coremessage.message elementForName:@"file"];
            
            if (file)
            {
                mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
                [mediaDb deleteMedia:coremessage.messageID];
            }
            
            [moc deleteObject:coremessage];
        }
        
        NSEntityDescription *contactEnityDes = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Contact_CoreDataObject"
                                                             inManagedObjectContext:moc];
        NSFetchRequest *contactRequest = [[NSFetchRequest alloc]init];
        [contactRequest setEntity:contactEnityDes];
        NSArray *recentContactMessage = [moc executeFetchRequest:contactRequest error:&error];
        
        for (XMPPMessageArchiving_Contact_CoreDataObject *recentCoremessage in recentContactMessage) {
            
            [moc deleteObject:recentCoremessage];
        }

        [moc save:nil];
    
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
