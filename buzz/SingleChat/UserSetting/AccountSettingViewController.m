//
//  AccountSettingViewController.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 12/06/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "AccountSettingViewController.h"
#import "NetworkUsageViewController.h"
#import "NotificationSettingViewController.h"

#import "DeleteAccountViewController.h"
#define AccountSettingArray @[@"Notification",@"Delete Account",@"Message usage"]
@interface AccountSettingViewController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation AccountSettingViewController
{
    UITableView *accountSettingtableView;
    UILabel *titleLabel;

}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
//    NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale.
//    NSString *countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
    
    if(IS_IPHONE_4)
    {
        self.view.backgroundColor = BACKGROUNDIMAGE_4;

    }
    else
    {
        self.view.backgroundColor = BACKGROUNDIMAGE;

    }
    
 
    
    accountSettingtableView = [UITableView new];
    accountSettingtableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    accountSettingtableView.delegate = self;
    accountSettingtableView.dataSource = self;
    accountSettingtableView.backgroundColor = [UIColor clearColor];
    accountSettingtableView.separatorColor = [UIColor clearColor];
    [self.view addSubview:accountSettingtableView];
    titleLabel = [UILabel new];

          
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    
    titleLabel.frame = CGRectMake(0, 10, self.view.frame.size.width, 30);
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = @"Account Settings";
    titleLabel.font = [UIFont fontWithName:FontString size:20];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.navigationController.navigationBar addSubview:titleLabel];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [titleLabel removeFromSuperview];
}
-(void)backbuttonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Tableview DataSource AND Delgate Methods
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    return AccountSettingArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"accountSettingtableViewIdentifier"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"accountSettingtableViewIdentifier"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(cell.frame.origin.x+10, 49, self.view.frame.size.width-20, 1)];
        separatorLineView.backgroundColor = RGBA(204, 204, 204, 0.4);
        [cell.contentView addSubview:separatorLineView];
    }
    cell.textLabel.text = [AccountSettingArray objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont fontWithName:FontString size:16];
    cell.textLabel.textColor = RGBA(130, 130, 130, 1);
    return  cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
            
        case 0:
        {
            NotificationSettingViewController *notificationSetting  = [NotificationSettingViewController new];
            [self.navigationController pushViewController:notificationSetting animated:YES];
        }
            break;
        case 1:
        {
          
            DeleteAccountViewController *deleteAcc = [DeleteAccountViewController new];
            [self.navigationController pushViewController:deleteAcc animated:YES];
        }
            break;

        case 2:
        {
            NetworkUsageViewController *networkUsage  = [NetworkUsageViewController new];
            [self.navigationController pushViewController:networkUsage animated:YES];
        }
            break;
            
        default:
            break;
    }
    
}


-(AppDelegate *)appDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (XMPPStream *)xmppStream
{
        return [[self appDelegate] xmppStream];
}
         
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
