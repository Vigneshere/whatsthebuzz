//
//  DeleteAccountViewController.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 23/06/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "DeleteAccountViewController.h"
#import "AFHTTPClient.h"
#import "AFJSONRequestOperation.h"
#import "RXMLElement.h"
#import "numberRegViewController.h"
#import "mediaDbEditor.h"
#import <FacebookSDK/FacebookSDK.h>


#define deleteMessagesArrar @[@"Delete your account from What the buzz",@"Erase all your message history",@"Erase all your group from What the buzz"]
@interface DeleteAccountViewController ()

@end

@implementation DeleteAccountViewController
{
    UILabel *titleLabel;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if(IS_IPHONE_4)
    {
        self.view.backgroundColor = BACKGROUNDIMAGE_4;
        
    }
    else
    {
        self.view.backgroundColor = BACKGROUNDIMAGE;
        
    }

    UILabel *deleteLabel = [UILabel new];
    deleteLabel.frame = CGRectMake(10, self.view.frame.origin.y+10, self.view.frame.size.width-10, 60);
    deleteLabel.text = @"To delete your account,confirm your country code and enter your phone number";
    deleteLabel.backgroundColor = [UIColor clearColor];
    deleteLabel.numberOfLines = 2;
    deleteLabel.font = [UIFont fontWithName:FontString size:14];
    deleteLabel.textColor = [UIColor grayColor];
    //[self.view addSubview:deleteLabel];
    
    UILabel *numberLabel = [UILabel new];
    numberLabel.frame = CGRectMake(self.view.frame.origin.x+50, deleteLabel.frame.origin.y+deleteLabel.frame.size.height+5, self.view.frame.size.width-100, 30);
    numberLabel.text = [[self xmppStream] myJID].user;
    numberLabel.textAlignment = NSTextAlignmentCenter;
    numberLabel.backgroundColor = [UIColor clearColor];
    numberLabel.font = [UIFont fontWithName:FontString size:15];
    numberLabel.layer.borderColor = [UIColor grayColor].CGColor;
    numberLabel.layer.borderWidth = 0.5;
    numberLabel.textColor = [UIColor grayColor];
    [self.view addSubview:numberLabel];
    

    
    UIButton *deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [deleteButton setTitle:@"Delete Account" forState:UIControlStateNormal];
    [deleteButton setBackgroundColor:RGBA(226, 61, 61, 1.0)];
    deleteButton.frame = CGRectMake(self.view.frame.origin.x+75, numberLabel.frame.origin.y+numberLabel.frame.size.height+20, self.view.frame.size.width-150, 30);
    deleteButton.titleLabel.font = [UIFont fontWithName:FontString size:15];
    [deleteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [deleteButton addTarget:self action:@selector(deleteButtonAction) forControlEvents:UIControlEventTouchUpInside];
    deleteButton.layer.cornerRadius = 5;
    [deleteButton setImageEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
    [deleteButton setImage:[UIImage imageNamed:@"delete_acc.png"] forState:UIControlStateNormal];

    [self.view addSubview:deleteButton];
    
//    UIView *numberfldView = [UIView new];
//    numberfldView.frame = CGRectMake(self.view.frame.origin.x+50, deleteLabel.frame.origin.y+deleteLabel.frame.size.height+5, self.view.frame.size.width-100, 30);
//    numberfldView.backgroundColor = [UIColor whiteColor];
//    numberfldView.layer.borderColor = RGBA(203, 203, 203, 1).CGColor;
//    numberfldView.layer.borderWidth = 1.0f;
//    [self.view addSubview:numberfldView];
//    
//    UILabel *countryCode_Lbl = [UILabel new];
//    countryCode_Lbl.frame = CGRectMake(0, 0, 50, numberfldView.frame.size.height);
//    countryCode_Lbl.backgroundColor = [UIColor whiteColor];
//    countryCode_Lbl.layer.borderColor = RGBA(203, 203, 203, 1).CGColor;
//    countryCode_Lbl.layer.borderWidth = 1.0f;
//    countryCode_Lbl.text = @"+91";
//    countryCode_Lbl.font =  [UIFont fontWithName:FontString size:23];//[UIFont fontWithName:FontString size:18];
//    countryCode_Lbl.textColor = [UIColor blackColor];
//    countryCode_Lbl.textAlignment = NSTextAlignmentCenter;
//    [numberfldView addSubview:countryCode_Lbl];
//    
//    UITextField *numberFld = [UITextField new];
//    numberFld .frame = CGRectMake(countryCode_Lbl.frame.size.width, 0, 200 , numberfldView.frame.size.height);
//    //numberFld.delegate = self;
//    numberFld.keyboardType = UIKeyboardTypeNumberPad;
//    numberFld.font =  [UIFont fontWithName:FontString size:23]; //[UIFont fontWithName:FontString size:18];
//    numberFld.textColor = [UIColor blackColor];
//    numberFld.textAlignment = NSTextAlignmentLeft;
//    numberFld.backgroundColor = [UIColor clearColor];
//    numberFld.text = @"95971696692";
//    numberFld.userInteractionEnabled = NO;
//    [numberfldView addSubview:numberFld];


    UIImage *deleteAccImage = [UIImage imageNamed:@"delete_tag.png"] ;
    for (int i = 0; i < 3; i ++) {
        
        UIImageView *deleteImageView = [UIImageView new];
        deleteImageView.frame = CGRectMake(25, deleteButton.frame.origin.y+deleteButton.frame.size.height+(i*20)+20,deleteAccImage.size.width , deleteAccImage.size.height+10);
        deleteImageView.image = deleteAccImage;
        [self.view addSubview:deleteImageView];
        
        UILabel *textlabel = [UILabel new];
        textlabel.frame = CGRectMake(deleteImageView.frame.origin.x+20, deleteImageView.frame.origin.y-3, self.view.frame.size.width-10, deleteAccImage.size.height+15);
        textlabel.text = deleteMessagesArrar[i];
        textlabel.backgroundColor = [UIColor clearColor];
        textlabel.font = [UIFont fontWithName:FontString size:12];
        textlabel.textColor = RGBA(226, 61, 61, 1.0);
        [self.view addSubview:textlabel];
    }
    
    titleLabel = [UILabel new];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    titleLabel.frame = CGRectMake(0, 10, self.view.frame.size.width, 30);
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = @"Chat Settings";
    titleLabel.font = [UIFont fontWithName:FontString size:20];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.navigationController.navigationBar addSubview:titleLabel];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [titleLabel removeFromSuperview];
}
-(void)deleteButtonAction
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Account"
                                                    message:@"Are you sure want to delete this account!"
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // the user clicked OK
    if (buttonIndex == 1) {
        [self deleteThisAccount];
        
    }
}
-(void)deleteThisAccount
{
    [self LoadingWithText:@"Deleting Account"];

    NSString *urlStr = [NSString stringWithFormat:@"http://%@:9090/plugins/userService/userservice?type=delete&secret=shaili&username=%@",HOSTNAME,[[self xmppStream] myJID].user];
    NSString *properlyEscapedURL = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    properlyEscapedURL = [properlyEscapedURL stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    
    NSURL *url = [NSURL URLWithString:properlyEscapedURL];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]
                                         initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *str = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        
        
        RXMLElement *rxml = [RXMLElement elementFromXMLString:str encoding:NSUTF8StringEncoding];
        
      //  NSLog(@"xml  ::: %@",rxml);
        NSString *string = [NSString stringWithFormat:@"%@",rxml];
        if ([string isEqualToString:@"ok"]) {
            
       
            [self clearAllMessageHistory];
            [self clearContacts];
            [self cleargroupDetails];
            [[self xmppStream] disconnect];

            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:USERNAME];
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"MASTER_QUESTION"];
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"MASTER_ANSWER"];
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:SET_PRIVATE_PWD];
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:CONFRM_PRIVATE_PWD];
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:MASTER_PIN];
            
            [self unLoadView];

            [self changeRootVc];

            
        }
        else if ([string isEqualToString:@"UserNotFoundException"])
        {
            [self clearAllMessageHistory];
            [self clearContacts];
            [self cleargroupDetails];
            [[self xmppStream] disconnect];
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:USERNAME];
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"MASTER_QUESTION"];
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"MASTER_ANSWER"];
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:SET_PRIVATE_PWD];
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:CONFRM_PRIVATE_PWD];
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:MASTER_PIN];

            [self unLoadView];
            
            [self changeRootVc];
        }
        
        //** Empty the Values.
        
        [[NSUserDefaults standardUserDefaults]setObject:nil forKey:USERNAME_XMPP];
        [[NSUserDefaults standardUserDefaults]setObject:nil forKey:USERNAME];

        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self unLoadView];

    
    }];
    
    [operation start];
    
}

-(void)changeRootVc
{
    numberRegViewController *numberReg = [numberRegViewController new];
    UINavigationController*  contoller = [[UINavigationController alloc]initWithRootViewController:numberReg];

    
    NSString *ver = [[UIDevice currentDevice] systemVersion];
    int ver_int = [ver intValue];
    
    if (ver_int < 7) {
        [contoller.navigationBar setTintColor:[UIColor clearColor]];
        
    }
    
    else {
        [contoller.navigationBar setBarTintColor:[UIColor clearColor]];
        
    }
    
    
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        
    [[self appDelegate] setWindow:[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]]];
    [[[self appDelegate] window] setRootViewController:contoller];
    [[[self appDelegate] window] setBackgroundColor:[UIColor whiteColor]];
    [[[self appDelegate] window] makeKeyAndVisible];
    
    UIView *statusBarView = [UIView new];
    statusBarView.frame = CGRectMake(0, 0,[[self appDelegate] window].frame.size.width, 20);
    statusBarView.backgroundColor = [UIColor colorWithRed:53/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];  //RGBA(53, 152, 219, 1)
    [[[self appDelegate] window] addSubview:statusBarView];
    
    // Uncomment to assign a custom backgroung image
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"nav-bar"] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    
    UIImage *backButtonImg = [UIImage imageNamed:@"back_btn.png"];
    
    UIImage *backButtonHomeImage = [[UIImage imageNamed:@"back_btn.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, backButtonImg.size.width - 1, 0, 0)];
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:backButtonHomeImage  forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, - backButtonHomeImage.size.height*2) forBarMetrics:UIBarMetricsDefault];

    
    
}
-(void)clearAllMessageHistory
{
    NSManagedObjectContext *moc = [[self appDelegate].xmppMessageArchivingStorage mainThreadManagedObjectContext];
    [moc performBlockAndWait:^{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entityDescription];
    NSError *error;
    NSArray *messages = [moc executeFetchRequest:request error:&error];
    
    for (XMPPMessageArchiving_Message_CoreDataObject *coremessage in messages)
    {
        NSXMLElement *file = [coremessage.message elementForName:@"file"];
        
        if (file)
        {
            mediaDbEditor* mediaDb = [mediaDbEditor new];
            [mediaDb deleteMedia:coremessage.messageID];
        }
        
        [moc deleteObject:coremessage];
    }
    
    NSEntityDescription *contactEnityDes = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Contact_CoreDataObject"  inManagedObjectContext:moc];
    NSFetchRequest *contactRequest = [[NSFetchRequest alloc]init];
    [contactRequest setEntity:contactEnityDes];
    NSArray *recentContactMessage = [moc executeFetchRequest:contactRequest error:&error];
    
    for (XMPPMessageArchiving_Contact_CoreDataObject *recentCoremessage in recentContactMessage) {
        [moc deleteObject:recentCoremessage];
    }
    }];
    NSError *error;
    if ([moc save:&error]) {
        
      //  NSLog(@"DB SAVE ERROR in clearAllHistory%@",error);
    }
}
-(void)cleargroupDetails
{
    XMPPRoomCoreDataStorage *storage = [XMPPRoomCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
    [moc performBlockAndWait:^{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:storage.occupantEntityName
                                                         inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entityDescription];
    NSError *error;

 
    
    NSEntityDescription *occupantentityDescription = [NSEntityDescription entityForName:storage.messageDetailsEntityName  inManagedObjectContext:moc];
    NSFetchRequest *occupantrequest = [[NSFetchRequest alloc]init];
    [occupantrequest setEntity:occupantentityDescription];
    
    NSArray *occupantgroupDetails = [moc executeFetchRequest:occupantrequest error:&error];
    for (XMPPRoomOccupantCoreDataStorageObject *occupantdetails in occupantgroupDetails) {
        
        
//        XMPPMessage *groupJoinMessage = [[XMPPMessage alloc]init];
//        [groupJoinMessage addAttributeWithName:@"id" stringValue:[NSString stringWithFormat:@"%@",[[self xmppStream] generateUUID]]];
//        [groupJoinMessage addAttributeWithName:@"to" stringValue:[occupantdetails.roomJID full]];
//        
//        [groupJoinMessage addAttributeWithName:@"type" stringValue:@"groupchat"];
//        NSXMLElement *groupJoinLeave = [NSXMLElement elementWithName:@"group"];
//        [groupJoinLeave addAttributeWithName:@"xmlns" stringValue:[NSString stringWithFormat:@"%@:group",DOMAINAME]];
//        [groupJoinLeave addAttributeWithName:@"type" stringValue:@"0"];
//        [groupJoinMessage addChild:groupJoinLeave];
//        NSXMLElement *bodyMessage = [NSXMLElement elementWithName:@"body" stringValue:@"left"];
//        [groupJoinMessage addChild:bodyMessage];
//        [[self xmppStream] sendElement:groupJoinMessage];
//        
//        //this is to inform other members who has left the group
//        XMPPPresence *presence = [XMPPPresence presence];
//        [presence addAttributeWithName:@"to" stringValue:[occupantdetails.roomJID full]];
//        [presence addAttributeWithName:@"type" stringValue:@"unavailable"];
//        [[self xmppStream] sendElement:presence];
        
        
        [moc deleteObject:occupantdetails];
    }
        
        
        NSArray *groupDetails = [moc executeFetchRequest:request error:&error];
        for (XMPPRoomMessageDetails *details in groupDetails) {
            
            
            
            [moc deleteObject:details];
            
            
        }
        
        
    }];
    
    


    NSError *error;
    if ([moc save:&error]) {
     
     //   NSLog(@"DB SAVE ERROR in CleargroupDetails%@",error);
    }
}
-(void)clearContacts
{
    [[[self appDelegate] xmppRosterStorage] clearAllUsersAndResourcesForXMPPStream:[self xmppStream]];
    
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Appdelegate Values
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (AppDelegate *)appDelegate
{
	return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (XMPPStream *)xmppStream
{
	return [[self appDelegate] xmppStream];
}

-(void)LoadingWithText:(NSString*)String
{
    [DejalBezelActivityView activityViewForView:self.view withLabel:String width:120];
}

-(void)unLoadView
{
    [DejalBezelActivityView removeViewAnimated:YES];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
