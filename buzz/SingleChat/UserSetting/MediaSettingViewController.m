//
//  MediaSettingViewController.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 16/06/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "MediaSettingViewController.h"
#define MediaSettingArray @[@"When using mobile data",@"When connected on wifi"]
#define MediaDetailsSettingArray @[@"Choose media on/off while you connected in mobile data",@"Choose media on/off while you connected on wifi",]


@interface MediaSettingViewController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation MediaSettingViewController

{
    UITableView *mediaSettingtableView;
    UILabel *titleLabel;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(IS_IPHONE_4)
    {
        self.view.backgroundColor = BACKGROUNDIMAGE_4;
        
    }
    else
    {
        self.view.backgroundColor = BACKGROUNDIMAGE;
        
    }
	// Do any additional setup after loading the view.
    mediaSettingtableView = [UITableView new];
    mediaSettingtableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    mediaSettingtableView.delegate = self;
    mediaSettingtableView.dataSource = self;
    mediaSettingtableView.backgroundColor = [UIColor clearColor];
    mediaSettingtableView.separatorColor = [UIColor clearColor];
    [self.view addSubview:mediaSettingtableView];
    
    titleLabel = [UILabel new];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    titleLabel.frame = CGRectMake(0, 10, self.view.frame.size.width, 30);
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = @"Media download option";
    titleLabel.font = [UIFont fontWithName:FontString size:20];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.navigationController.navigationBar addSubview:titleLabel];

}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
   
    [titleLabel removeFromSuperview];
}
-(void)backbuttonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Tableview DataSource AND Delgate Methods
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    return MediaSettingArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"accountSettingtableViewIdentifier"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"accountSettingtableViewIdentifier"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(cell.frame.origin.x+10, 54, cell.frame.size.width-20, 1)];
        separatorLineView.backgroundColor = RGBA(204, 204, 204, 0.4);
        [cell.contentView addSubview:separatorLineView];
        UISwitch *switchButton = [[UISwitch alloc] initWithFrame:CGRectMake(cell.frame.size.width-70, 10, 15, 15)];
        switchButton.tag = 100;
        [switchButton addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
        [cell.contentView addSubview:switchButton];
    }
    cell.textLabel.text = [MediaSettingArray objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont fontWithName:FontString size:16];
    cell.textLabel.textColor = RGBA(130, 130, 130, 1);
    cell.detailTextLabel.text = [MediaDetailsSettingArray objectAtIndex:indexPath.row];
    cell.detailTextLabel.font = [UIFont fontWithName:FontString size:8];
    cell.detailTextLabel.textColor = RGBA(130, 130, 130, 1);
    UISwitch *notificationSwitch = (UISwitch *)[cell.contentView viewWithTag:100];

    if ( indexPath.row == 0)
    {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"mediaDownloadMobileDataOption"] == YES)
            [notificationSwitch setOn:YES];
        else
            [notificationSwitch setOn:NO];
    }
    else if(indexPath.row == 1)
    {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"mediaDownloadWIFIOption"] == YES)
            [notificationSwitch setOn:YES];
        else
            [notificationSwitch setOn:NO];
    }


    return  cell;
}

-(void)switchChanged:(id)sender
{
    UISwitch *switchButton = (UISwitch *)sender;
    CGPoint center= switchButton.center;
    CGPoint rootViewPoint = [switchButton.superview convertPoint:center toView:mediaSettingtableView];
    NSIndexPath *indexPath = [mediaSettingtableView indexPathForRowAtPoint:rootViewPoint];
    if (indexPath.row == 0) {
        if (switchButton.isOn)
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"mediaDownloadMobileDataOption"];
            else
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"mediaDownloadMobileDataOption"];

    }
  else if (indexPath.row == 1)
  {
      if (switchButton.isOn)
          [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"mediaDownloadWIFIOption"];
      else
          [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"mediaDownloadWIFIOption"];
  }
    [[NSUserDefaults standardUserDefaults]synchronize];

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}
    - (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
