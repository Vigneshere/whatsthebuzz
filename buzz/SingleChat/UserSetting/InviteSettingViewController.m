//
//  InviteSettingViewController.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 12/06/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "InviteSettingViewController.h"
#import <MessageUI/MessageUI.h>
#import "FullContactsTableViewController.h"
#define InviteSettingArray @[@"Email",@"SMS"]
#define InviteSettingImageArray @[@"Inviteemail.png",@"Invitesms.ong"]

@interface InviteSettingViewController ()<UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>

@end

@implementation InviteSettingViewController
{
    UITableView *inviteSettingtableView;
    UIButton *backButton;
    FullContactsTableViewController *contactList;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Invite";
    if(IS_IPHONE_4)
    {
        self.view.backgroundColor = BACKGROUNDIMAGE_4;
        
    }
    else
    {
        self.view.backgroundColor = BACKGROUNDIMAGE;
        
    }
    self.navigationItem.hidesBackButton = NO;
    inviteSettingtableView = [UITableView new];
    inviteSettingtableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    inviteSettingtableView.delegate = self;
    inviteSettingtableView.dataSource = self;
    inviteSettingtableView.backgroundColor = [UIColor clearColor];
    inviteSettingtableView.separatorColor = [UIColor clearColor];
    [self.view addSubview:inviteSettingtableView];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Tableview DataSource AND Delgate Methods
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    return InviteSettingArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"inviteSettingtableViewIdentifier"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"inviteSettingtableViewIdentifier"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(cell.frame.origin.x+10, 49, self.view.frame.size.width-20, 1)];
        separatorLineView.backgroundColor = RGBA(204, 204, 204, 0.4);
        [cell.contentView addSubview:separatorLineView];
    }
    cell.textLabel.text = InviteSettingArray [indexPath.row];
    cell.textLabel.font = [UIFont fontWithName:FontString size:16];
    cell.textLabel.textColor = RGBA(130, 130, 130, 1);
    cell.imageView.image = [UIImage imageNamed:InviteSettingImageArray[indexPath.row]];
    return  cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //
    //    [[[UIAlertView alloc] initWithTitle:ALERTVIEW_TITLE message:@"Under Construction" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
    
    if (indexPath.row == 0) {
        if ([MFMailComposeViewController canSendMail]) {
            MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
            controller.mailComposeDelegate = self;
            [controller setSubject:@"Invitation for WhatstheBuzz!"];
            [controller setMessageBody:@"Hi There! I'm greatly excited in inviting you to use WhatstheBuzz App. Try it now!" isHTML:NO];
            // [controller setToRecipients:@[@"deepakmathan@gmail.com"]];
            [self.navigationController presentViewController:controller animated:YES completion:nil];
        }
        
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"OOPS!" message:@"Please configure your email on settings" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else
    {
        if (!contactList) {
            contactList = [FullContactsTableViewController new];
        }
        contactList.controlString = @"invite";
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:contactList];
        [self presentViewController:navigationController animated:YES completion:nil];
        
        
        //        if ([MFMessageComposeViewController canSendText]) {
        //            MFMessageComposeViewController *messageComposer =
        //            [[MFMessageComposeViewController alloc] init];
        //            NSString *message = @"Your Message here";
        //            [messageComposer setBody:message];
        //            messageComposer.messageComposeDelegate = self;
        //            [self presentViewController:messageComposer animated:YES completion:nil];
        //        }
    }
    
}
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
          //  NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
        //    NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
         //   NSLog(@"Mail sent");
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Hooray!" message:@"Your invitation is Sent!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
            break;
        case MFMailComposeResultFailed:
         //   NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result {
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
