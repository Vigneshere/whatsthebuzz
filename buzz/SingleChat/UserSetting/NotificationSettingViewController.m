//
//  NotiificationSettingViewController.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 13/06/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "NotificationSettingViewController.h"
@interface NotificationSettingViewController ()<UITableViewDelegate,UITableViewDataSource>

@end


@implementation NotificationSettingViewController
{
    UITableView *notificationSettingtableView;
    UILabel *titleLabel;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if(IS_IPHONE_4)
    {
        self.view.backgroundColor = BACKGROUNDIMAGE_4;
        
    }
    else
    {
        self.view.backgroundColor = BACKGROUNDIMAGE;
        
    }
    
    notificationSettingtableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    notificationSettingtableView.delegate = self;
    notificationSettingtableView.dataSource = self;
    notificationSettingtableView.backgroundColor = [UIColor clearColor];
    notificationSettingtableView.separatorColor = [UIColor clearColor];
    [self.view addSubview:notificationSettingtableView];
    titleLabel = [UILabel new];

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    titleLabel.frame = CGRectMake(0, 10, self.view.frame.size.width, 30);
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = @"Notification";
    titleLabel.font = [UIFont fontWithName:FontString size:20];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.navigationController.navigationBar addSubview:titleLabel];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [titleLabel removeFromSuperview];
}
-(void)backbuttonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Tableview DataSource AND Delgate Methods
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}
-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
        return 1;
    else if (section == 1)
        return 1;
    return 0;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [UIView new];
    view.frame = CGRectMake(0, 0, tableView.frame.size.width, 30);
    
    UILabel *label = [UILabel new];
    label.frame = CGRectMake(10, 5, tableView.frame.size.width, view.frame.size.height);
    label.backgroundColor = [UIColor clearColor];
    
    if (section == 0)
        label.text = @"Chat notification";
    else
        label.text = @"Group Chat notification";
    label.font = [UIFont fontWithName:FontString size:16];
    label.textColor = [UIColor blackColor];
    
    [view addSubview:label];
    UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(view.frame.origin.x+10, 29, view.frame.size.width-20, 1)];
    separatorLineView.backgroundColor = RGBA(204, 204, 204, 0.4);
    [view addSubview:separatorLineView];
    
    return view;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"accountSettingtableViewIdentifier"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"accountSettingtableViewIdentifier"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(cell.frame.origin.x+10, 49, self.view.frame.size.width-20, 1)];
        separatorLineView.backgroundColor = RGBA(204, 204, 204, 0.4);
        [cell.contentView addSubview:separatorLineView];
        
        UISwitch *switchButton = [[UISwitch alloc] initWithFrame:CGRectMake(self.view.frame.size.width-70, 10, 15, 15)];
        switchButton.tag = 100;
        [switchButton addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
        [cell.contentView addSubview:switchButton];
    }
    
    if (indexPath.row % 2 == 0)
    {
        cell.textLabel.text = @"Conversation tone";
        cell.detailTextLabel.text = @"Play sound for incoming";
        UISwitch *notificationSwitch = (UISwitch *)[cell.contentView viewWithTag:100];
        if (indexPath.section == 0 && indexPath.row == 0)
        {
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"SingleChatNotification"] == YES)
                [notificationSwitch setOn:YES];
            else
                [notificationSwitch setOn:NO];
        }
        else if(indexPath.section == 1 && indexPath.row == 0)
        {
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"GroupChatNotification"] == YES)
                [notificationSwitch setOn:YES];
            else
                [notificationSwitch setOn:NO];
        }
    }
//    else
//    {
//        cell.textLabel.text = @"Notification tone";
//        cell.detailTextLabel.text = @"Select sound to play for incoming and outgoing message";
//        [[cell.contentView viewWithTag:100] removeFromSuperview];
//    }
    cell.textLabel.font = [UIFont fontWithName:FontString size:16];
    cell.textLabel.textColor = RGBA(130, 130, 130, 1);
    cell.detailTextLabel.font = [UIFont fontWithName:FontString size:10];
    cell.detailTextLabel.textColor = RGBA(130, 130, 130, 1);
    
    return  cell;
}
-(void)switchChanged:(id)sender
{
    UISwitch *switchButton = (UISwitch *)sender;
    CGPoint center= switchButton.center;
    CGPoint rootViewPoint = [switchButton.superview convertPoint:center toView:notificationSettingtableView];
    NSIndexPath *indexPath = [notificationSettingtableView indexPathForRowAtPoint:rootViewPoint];
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        if (switchButton.isOn)
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SingleChatNotification"];
        else
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"SingleChatNotification"];
    }
    else if(indexPath.section == 1 && indexPath.row == 0)
    {
        if (switchButton.isOn)
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"GroupChatNotification"];
        else
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"GroupChatNotification"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
