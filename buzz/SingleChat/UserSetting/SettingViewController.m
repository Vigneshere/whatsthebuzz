//
//  settingViewController.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 18/03/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "SettingViewController.h"
#import "profileFormViewController.h"
#import "ChatSettingViewController.h"
#import "InviteSettingViewController.h"
#import "AccountSettingViewController.h"
#define settingArray @[@"Profile",@"Account",@"Chat",@"Network status"]
#define SettingimgAry @[@"profile.png",@"account.png",@"chat.png",@"invite.png",@"network.png"]


@interface SettingViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *settingtableView;
    UILabel *titleLabel;
}
@end

@implementation SettingViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if(IS_IPHONE_4)
    {
        self.view.backgroundColor = BACKGROUNDIMAGE_4;
        
    }
    else
    {
        self.view.backgroundColor = BACKGROUNDIMAGE;
        
    }
    
    settingtableView = [UITableView new];
    settingtableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    settingtableView.delegate = self;
    settingtableView.dataSource = self;
    settingtableView.backgroundColor = [UIColor clearColor];
    settingtableView.separatorColor = [UIColor clearColor];
    [self.view addSubview:settingtableView];
    
    titleLabel = [UILabel new];
   
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    titleLabel.frame = CGRectMake(0, 10, self.view.frame.size.width, 30);
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = @"Settings";
    titleLabel.font = [UIFont fontWithName:FontString size:20];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.navigationController.navigationBar addSubview:titleLabel];

    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [titleLabel removeFromSuperview];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Tableview DataSource AND Delgate Methods
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    return settingArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MyIdentifier"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(cell.frame.origin.x+10, 49, self.view.frame.size.width-20, 1)];
        separatorLineView.backgroundColor = RGBA(204, 204, 204, 0.4);
        [cell.contentView addSubview:separatorLineView];
    }
    cell.textLabel.text = [settingArray objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont fontWithName:FontString size:16];
    cell.textLabel.textColor = RGBA(130, 130, 130, 1);
    cell.imageView.image = [UIImage imageNamed:[SettingimgAry objectAtIndex:indexPath.row]];
    if (indexPath.row == 4) {
        if ([[self xmppStream] isConnected])
        {

        cell.detailTextLabel.text = @"Connected";
     
        cell.detailTextLabel.textColor = RGBA(150, 164, 74, 1.0);
            
        }
        else
        {
            cell.detailTextLabel.text = @"Not connected";
            cell.detailTextLabel.textColor = RGBA(241, 64, 60,0.8);
        }
        
    }
 return  cell;
}
- (AppDelegate *)appDelegate
{
	return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (XMPPStream *)xmppStream
{
	return [[self appDelegate] xmppStream];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (indexPath.row) {
        case 0:
        {
            profileFormViewController *profile = [profileFormViewController new];
            profile.viewControllerString = @"userSetting";
            [self.navigationController pushViewController:profile animated:YES];
        }
            
            break;
        case 1:
        {
            AccountSettingViewController *accountSetting = [AccountSettingViewController new];
            [self.navigationController pushViewController:accountSetting animated:YES];
        }
            
            break;
        case 2:
        {
            ChatSettingViewController *chatSetting = [ChatSettingViewController new ];
            [self.navigationController pushViewController:chatSetting animated:YES];
        }
            
            break;
            
            /*
        case 3:
        {
            InviteSettingViewController *inviteSetting = [InviteSettingViewController new ];
            [self.navigationController pushViewController:inviteSetting animated:YES];
        }
             */
            break;
               default:
            break;
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
