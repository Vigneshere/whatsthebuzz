//
//  editImageView.h
//  buzz
//
//  Created by Vignesh on 25/06/15.
//  Copyright (c) 2015 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface editImageView : UIView
{
    
}
-(id)initWithFrame_onView:(UIView *)onview image:(UIImage *)editingImage;
-(void)projectImageForEdit;

@property (strong, nonatomic) UIView *onView;
@property (strong, nonatomic) UIImage *imageForEdit;



@end
