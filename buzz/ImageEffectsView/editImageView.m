//
//  editImageView.m
//  buzz
//
//  Created by Vignesh on 25/06/15.
//  Copyright (c) 2015 Shailisolutions. All rights reserved.
//

#import "editImageView.h"
#import "UIImage+Utility.h"
#import <Accelerate/Accelerate.h>
#import "DejalActivityView.h"
#import "UIColor+HexColors.h"




@implementation editImageView
{
    UIImageView *imagDisplayView;
    UIView *effectsOptionsView ;
    UIView *effctsView,*filterView;
    NSNumber *hueValue,*highlightValue,*blurValur,*invertValue,*sepiaValue;
    UISlider *slider,*highlightSlider,*blurSlider;
    UIActivityIndicatorView *activityIndicator;
    UIButton *essentialBtns;
    NSOperationQueue   *imageProcessQueue;
 
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initWithFrame_onView:(UIView *)onview image:(UIImage *)editingImage
{
    self = [super initWithFrame:CGRectMake(0, 0, onview.frame.size.width, onview.frame.size.height)];
    if (self) {
     
        self.onView = onview;
        self.imageForEdit = editingImage;
        imageProcessQueue = [NSOperationQueue new];
    }
    return self;
}

-(void)projectImageForEdit
{
    imagDisplayView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 64, self.frame.size.width , self.frame.size.height - 140)];
    imagDisplayView.backgroundColor = [UIColor clearColor];
    imagDisplayView.layer.cornerRadius = 3;
    imagDisplayView.userInteractionEnabled = YES;
    imagDisplayView.image = self.imageForEdit;
    [self addSubview:imagDisplayView];
    
    effectsOptionsView = [[UIView alloc]initWithFrame:CGRectMake(0, imagDisplayView.frame.size.height + 64, self.frame.size.width, self.frame.size.height - (imagDisplayView.frame.size.height - 24))];
    effectsOptionsView.layer.cornerRadius = 3;
    effectsOptionsView.backgroundColor = [UIColor colorWithHexString:@"D7D7D7"];
    [self addSubview:effectsOptionsView];
    
    
    CGFloat rowWidth = (CGRectGetWidth(self.frame)/3);
    NSArray    *FirstOptions = [[NSArray alloc]initWithObjects:@"Add Effects",@"Add Filter",@"Send Image" ,nil];

    for (int i = 0; i < 3; i++) {
        UIButton *_button = [UIButton buttonWithType:UIButtonTypeCustom];
        _button.backgroundColor = [UIColor clearColor];
        
        CGFloat y = 0.0f;
    
        if (i == 0)
        {
            y = 0.2f;
        }
        
        
        UIImage *imgForBtn = [UIImage imageWithImage:self.imageForEdit scaledToSize:CGSizeMake(60, 45)];
        [_button setFrame:CGRectMake(i*rowWidth + 30, 15, 70, 45)];
        [_button addTarget:self action:@selector(toolBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [_button setClipsToBounds:YES];
         _button.layer.cornerRadius = 6;
        [_button setBackgroundColor:[UIColor whiteColor]];
        _button.titleLabel.font = [UIFont boldSystemFontOfSize:10];
        [_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_button setTitle:FirstOptions[i] forState:UIControlStateNormal];
        [_button setBackgroundImage:imgForBtn forState:UIControlStateNormal];
        UIImage *highlightedBtnColor = [self imageWithColor:[UIColor lightGrayColor]];
        [_button setBackgroundImage:highlightedBtnColor forState:UIControlStateHighlighted];
        
        _button.tag = i+1;
        [effectsOptionsView addSubview:_button];
    }
    
    [self.onView addSubview:self];
    
    
    hueValue = [NSNumber numberWithFloat:5.0f];
    highlightValue = [NSNumber numberWithFloat:5.0];
    blurValur = [NSNumber numberWithFloat:5.0];
    invertValue = [NSNumber numberWithFloat:5.0];
    sepiaValue = [NSNumber numberWithFloat:5.0];
    
    slider = [[UISlider alloc]init ];
    slider.frame = CGRectMake(self.frame.size.width/2 - 100, imagDisplayView.frame.size.height - 120, 200, 30);
    slider.userInteractionEnabled = YES;
    slider.minimumValue = 0.0;
    slider.maximumValue = 20.0;
    slider.tag = 100;
   slider.continuous = NO;
    [slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
    slider.value = [hueValue floatValue];
    slider.hidden = YES;
    [imagDisplayView addSubview:slider];
    
    highlightSlider = [[UISlider alloc]initWithFrame:CGRectMake(self.frame.size.width/2 - 100, imagDisplayView.frame.size.height - 120, 200, 30)];
    highlightSlider.userInteractionEnabled = YES;
    highlightSlider.minimumValue = 0.0;
    highlightSlider.maximumValue = 20.0;
    highlightSlider.tag = 200;
    highlightSlider.continuous = NO;
    [highlightSlider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
    highlightSlider.value = [hueValue floatValue];
    highlightSlider.hidden = YES;
    [imagDisplayView addSubview:highlightSlider];
    
    blurSlider = [[UISlider alloc]init];
    blurSlider.frame = CGRectMake(self.frame.size.width/2 - 100, imagDisplayView.frame.size.height - 120, 200, 30);
    blurSlider.userInteractionEnabled = YES;
    blurSlider.minimumValue = 0.0;
    blurSlider.maximumValue = 20.0;
    blurSlider.tag = 300;
    blurSlider.continuous = NO;
    [blurSlider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
    blurSlider.value = [blurValur floatValue];
    blurSlider.hidden = YES;
    [imagDisplayView addSubview:blurSlider];

    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = CGPointMake(imagDisplayView.frame.size.width/2, imagDisplayView.frame.size.height/2);
     activityIndicator.hidesWhenStopped = YES;
    [imagDisplayView addSubview:activityIndicator];
    

    
}

-(void)toolBtnAction:(UIButton *)sender
{
    if (sender.tag == 1)
    {
        
        [activityIndicator startAnimating];
        
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            essentialBtns = [UIButton buttonWithType:UIButtonTypeCustom];
            UIImage *tickImg = [UIImage imageNamed:@"wrong"];
            essentialBtns.frame = CGRectMake(self.frame.origin.x + (tickImg.size.width), imagDisplayView.frame.size.height - (tickImg.size.height + 20), tickImg.size.width, tickImg.size.height);
            [essentialBtns setImage:tickImg forState:UIControlStateNormal];
            [essentialBtns addTarget:self action:@selector(goBackEvent:) forControlEvents:UIControlEventTouchUpInside];
            essentialBtns.tag = sender.tag;
            essentialBtns.backgroundColor = [UIColor clearColor];
            [imagDisplayView addSubview:essentialBtns];
            
            UIButton *essentialSendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            UIImage *crossImg = [UIImage imageNamed:@"greenTick"];
            essentialSendBtn.frame = CGRectMake(self.frame.size.width - (2*crossImg.size.width), imagDisplayView.frame.size.height - (crossImg.size.height +20), crossImg.size.width,crossImg.size.height);
            essentialSendBtn.backgroundColor = [UIColor clearColor];
            [essentialSendBtn addTarget:self action:@selector(sendImg) forControlEvents:UIControlEventTouchUpInside];
            [essentialSendBtn setImage:crossImg forState:UIControlStateNormal];
            essentialSendBtn.tag = sender.tag;
            [imagDisplayView addSubview:essentialSendBtn];
            
            
            effctsView = [[UIView alloc]initWithFrame:CGRectMake(0, imagDisplayView.frame.size.height + 128, self.frame.size.width, self.frame.size.height - (imagDisplayView.frame.size.height - 30))];
            effctsView.backgroundColor = [UIColor colorWithHexString:@"D7D7D7"];
            [self addSubview:effctsView];
            effectsOptionsView.hidden = YES;
            
            [UIView animateWithDuration:0.2
                                  delay:0.0
                                options: UIViewAnimationOptionCurveEaseIn
                             animations:^{
                                 
                                effctsView.frame = CGRectMake(0, imagDisplayView.frame.size.height + 64, self.frame.size.width, self.frame.size.height - (imagDisplayView.frame.size.height - 30));
                            
                             }
                             completion:^(BOOL finished){

                             
                             }];
            
            
     
            
            NSArray    *menuImageArray = [[NSArray alloc]initWithObjects:@"HUE",@"HIGHLIGHT",@"BLUR",@"INVERT",@"SEPIA", nil];
            CGFloat rowWidth = (CGRectGetWidth(self.frame)/5);
            
            for (int i = 0; i < [menuImageArray count]; i++) {
                UIButton *_button = [UIButton buttonWithType:UIButtonTypeCustom];
                _button.backgroundColor = [UIColor clearColor];
                
                CGFloat y = 0.0f;
                
                if (i == 0)
                {
                    y = 0.2f;
                }
                
                UIImage *btnImg = [UIImage imageWithImage:self.imageForEdit scaledToSize:CGSizeMake(60, 45)];
                if (i == 0)
                {
                    btnImg = [self makeImageHue:5];
                    [_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    
                }
                else if (i == 1)
                {
                    btnImg = self.imageForEdit;
                    [_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    
                    
                }
                else if (i == 2)
                {
                    btnImg = [self makeImageBlur:[blurValur floatValue]];
                    [_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    
                }
                
                
                else if (i == 3)
                {
                    btnImg = [self makeImageInvertColor];
                    [_button setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
                    
                    
                }
                else
                {
                    btnImg = [self makeImageSepia];
                    [_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    
                }
                
                btnImg = [UIImage imageWithImage:btnImg scaledToSize:CGSizeMake(60, 45)];
                
                if (i == 0)
                {
                    [_button setFrame:CGRectMake(i*rowWidth + 10, 10, 60, 45)];
                    
                }
                else
                {
                    [_button setFrame:CGRectMake(i*rowWidth, 10, 60, 45)];
                    
                }
                
                [_button addTarget:self action:@selector(addEffects:) forControlEvents:UIControlEventTouchUpInside];
                [_button setClipsToBounds:false];
                _button.layer.cornerRadius = 15;
                [_button setBackgroundColor:[UIColor orangeColor]];
                _button.titleLabel.font = [UIFont boldSystemFontOfSize:10];
                [_button.titleLabel adjustsFontSizeToFitWidth];
                [_button setTitle:menuImageArray[i] forState:UIControlStateNormal];
                [_button setBackgroundImage:btnImg forState:UIControlStateNormal];
                _button.tag = i+10;
                [effctsView addSubview:_button];
            }
            
            [activityIndicator stopAnimating];

        });
        
        
        
    }
    else if (sender.tag == 2)
    {
        
        [activityIndicator startAnimating];
        
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            effectsOptionsView.hidden = YES;
            effctsView.hidden = YES;
            filterView = [[UIView alloc]initWithFrame:CGRectMake(0, imagDisplayView.frame.size.height + 128, self.frame.size.width, self.frame.size.height - (imagDisplayView.frame.size.height - 30))];
            filterView.backgroundColor = [UIColor colorWithHexString:@"D7D7D7"];
            [self addSubview:filterView];
            
            [UIView animateWithDuration:0.2
                                  delay:0.0
                                options: UIViewAnimationOptionCurveEaseIn
                             animations:^{
                                 
                      filterView.frame  =  CGRectMake(0, imagDisplayView.frame.size.height + 64, self.frame.size.width, self.frame.size.height - (imagDisplayView.frame.size.height - 30));
                                 
                             }
                             completion:^(BOOL finished){
                                 
                                 
                             }];
            
            essentialBtns = [UIButton buttonWithType:UIButtonTypeCustom];
            UIImage *tickImg = [UIImage imageNamed:@"wrong"];
            essentialBtns.frame = CGRectMake(self.frame.origin.x + (tickImg.size.width), imagDisplayView.frame.size.height - (tickImg.size.height + 20), tickImg.size.width, tickImg.size.height);
            [essentialBtns setImage:tickImg forState:UIControlStateNormal];
            [essentialBtns addTarget:self action:@selector(goBackEvent:) forControlEvents:UIControlEventTouchUpInside];
            essentialBtns.tag = sender.tag;
            essentialBtns.backgroundColor = [UIColor clearColor];
            [imagDisplayView addSubview:essentialBtns];
            
            UIButton *essentialSendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            UIImage *crossImg = [UIImage imageNamed:@"greenTick"];
            essentialSendBtn.frame = CGRectMake(self.frame.size.width - (2*crossImg.size.width), imagDisplayView.frame.size.height - (crossImg.size.height +20), crossImg.size.width,crossImg.size.height);
            essentialSendBtn.backgroundColor = [UIColor clearColor];
            [essentialSendBtn addTarget:self action:@selector(sendImg) forControlEvents:UIControlEventTouchUpInside];
            [essentialSendBtn setImage:crossImg forState:UIControlStateNormal];
            essentialSendBtn.tag = sender.tag;
            [imagDisplayView addSubview:essentialSendBtn];
            
            NSArray    *menuImageArray = [[NSArray alloc]initWithObjects:@"Linear",@"Vignette",@"Instant",@"Transfer",@"Fade", nil];
            CGFloat rowWidth = (CGRectGetWidth(self.frame)/5);
            
            for (int i = 0; i < [menuImageArray count]; i++) {
                UIButton *_button = [UIButton buttonWithType:UIButtonTypeCustom];
                _button.backgroundColor = [UIColor clearColor];
                
                CGFloat y = 0.0f;
                if (i == 0)
                {
                    y = 0.2f;
                }
                
                UIImage *btnImg = [UIImage imageWithImage:self.imageForEdit scaledToSize:CGSizeMake(60, 45)];
                if (i == 0)
                {
                    btnImg = [self filterImage:@"CISRGBToneCurveToLinear"];
                    
                }
                else if (i == 1)
                {
                    btnImg = [self filterImage:@"CIVignetteEffect"];
                    
                }
                else if (i == 2)
                {
                    btnImg = [self filterImage:@"CIPhotoEffectInstant"];
                    
                }
                
                else if (i == 3)
                {
                    btnImg = [self filterImage:@"CIPhotoEffectTransfer"];
                    
                    
                }
                else
                {
                    btnImg = [self filterImage:@"CIPhotoEffectFade"];
                    [_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    
                }
                
                if (i == 0)
                {
                    [_button setFrame:CGRectMake(i*rowWidth + 15, 5, 60, 45)];
                    
                }
                else
                {
                    [_button setFrame:CGRectMake(i*rowWidth, 5, 60, 45)];
                    
                }
                
                
                btnImg = [UIImage imageWithImage:btnImg scaledToSize:CGSizeMake(60, 45)];
                
                [_button setFrame:CGRectMake(i*rowWidth, 5, 60, 45)];
                [_button addTarget:self action:@selector(addEffects:) forControlEvents:UIControlEventTouchUpInside];
                [_button setClipsToBounds:false];
                _button.layer.cornerRadius = 6;
                [_button setBackgroundColor:[UIColor clearColor]];
                [_button setBackgroundImage:btnImg forState:UIControlStateNormal];
                _button.titleLabel.font = [UIFont boldSystemFontOfSize:10];
                [_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [_button setTitle:menuImageArray[i] forState:UIControlStateNormal];
                _button.tag = i+100;
                [filterView addSubview:_button];
            }
            
            [activityIndicator stopAnimating];
        });
    }
    

}

-(void)sendImg
{
    
}

-(void)goBackEvent:(id)sender
{
    
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                      
                         
                      filterView.frame  =   CGRectMake(0, self.frame.size.height, self.frame.size.width, self.frame.size.height - (imagDisplayView.frame.size.height - 30));
                         effctsView.frame =  CGRectMake(0, self.frame.size.height, self.frame.size.width, self.frame.size.height - (imagDisplayView.frame.size.height - 30));

                     }
                     completion:^(BOOL finished){
                         
                         effectsOptionsView.hidden = NO;
                         imagDisplayView.image = self.imageForEdit;

                     }];

    
    [slider setHidden: YES];
    [highlightSlider setHidden:YES];
    [blurSlider setHidden:YES];
}

-(void)addEffects:(UIButton *)sender
{
    
    UIButton *btn = (UIButton *)sender;
    if (btn.tag == 10)
    {
        

        
       imagDisplayView.image =   [self makeImageHue:[hueValue floatValue]];
        [highlightSlider setHidden: YES];
        [slider setHidden:NO];
        [blurSlider setHidden:YES];
    }
    else if (btn.tag == 11)
    {
        imagDisplayView.image = _imageForEdit;
        
        [self getHighlightedImage:[highlightValue floatValue] completion:^(UIImage* fliteredImage)
         {
             imagDisplayView.image = fliteredImage;
             
         }];
        
        [slider setHidden: YES];
        [highlightSlider setHidden:NO];
        [blurSlider setHidden:YES];

    }
    else if (btn.tag == 12)
    {
        
        blurSlider.frame = CGRectMake(self.frame.size.width/2 - 100, imagDisplayView.frame.size.height - 120, 200, 30);
        imagDisplayView.image =  [self makeImageBlur:[blurValur floatValue]];
        [slider setHidden: YES];
        [highlightSlider setHidden:YES];
        [blurSlider setHidden:NO];
    }
    else if (btn.tag == 13)
    {
        [self makeImageInvertColor];
        [slider setHidden: YES];
        [highlightSlider setHidden:YES];
        [blurSlider setHidden:YES];
    }
    else if (btn.tag == 14)
    {
        [self makeImageSepia];
        [slider setHidden: YES];
        [highlightSlider setHidden:YES];
        [blurSlider setHidden:YES];
    }
    
    else if (btn.tag == 100)
    {
        [self filterImage:@"CISRGBToneCurveToLinear"];
        [slider setHidden: YES];
        [highlightSlider setHidden:YES];
        [blurSlider setHidden:YES];
    }
    else if (btn.tag == 101)
    {
        [self filterImage:@"CIVignetteEffect"];
        [slider setHidden: YES];
        [highlightSlider setHidden:YES];
        [blurSlider setHidden:YES];
    }
    else if (btn.tag == 102)
    {
        [self filterImage:@"CIPhotoEffectInstant"];
        [slider setHidden: YES];
        [highlightSlider setHidden:YES];
        [blurSlider setHidden:YES];
    }
    else if (btn.tag == 103)
    {
        [self filterImage:@"CIPhotoEffectTransfer"];
        [slider setHidden: YES];
        [highlightSlider setHidden:YES];
        [blurSlider setHidden:YES];

    }
    else if (btn.tag == 104)
    {
        [self filterImage:@"CIPhotoEffectFade"];
        [slider setHidden: YES];
        [highlightSlider setHidden:YES];
        [blurSlider setHidden:YES];
    }
}

-(UIImage *)filterImage:(NSString *)filterName
{
    
    CIImage *ciImage = [[CIImage alloc] initWithImage:self.imageForEdit];
    CIFilter *filter = [CIFilter filterWithName:filterName keysAndValues:kCIInputImageKey, ciImage, nil];
    [filter setDefaults];
    
    if([filterName isEqualToString:@"CIVignetteEffect"]){
        CGFloat R = MIN(self.imageForEdit.size.width, self.imageForEdit.size.height)*self.imageForEdit.scale/2;
        CIVector *vct = [[CIVector alloc] initWithX:self.imageForEdit.size.width*self.imageForEdit.scale/2 Y:self.imageForEdit.size.height*self.imageForEdit.scale/2];
        [filter setValue:vct forKey:@"inputCenter"];
        [filter setValue:[NSNumber numberWithFloat:0.9] forKey:@"inputIntensity"];
        [filter setValue:[NSNumber numberWithFloat:R] forKey:@"inputRadius"];
    }
    
    CIContext *context = [CIContext contextWithOptions:@{kCIContextUseSoftwareRenderer : @(NO)}];
    CIImage *outputImage = [filter outputImage];
    CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
    UIImage *result = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    imagDisplayView.image = result;
    return result;
}

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

-(void)sliderAction:(id)sender
{
    UISlider *sliderAction = (UISlider *)sender;
    
    if (sliderAction.tag == 100)
    {
        
        hueValue = [NSNumber numberWithFloat:sliderAction.value];
        imagDisplayView.image = [self makeImageHue:[hueValue floatValue]];

    }
    else if (sliderAction.tag == 200)
    {
        highlightValue = [NSNumber numberWithFloat:sliderAction.value];
       [self getHighlightedImage:[highlightValue floatValue] completion:^(UIImage* fliteredImage)
        {
            imagDisplayView.image = fliteredImage;
            
        }];
        
           }
    else if (sliderAction.tag == 300)
    {
        blurValur = [NSNumber numberWithFloat:sliderAction.value];
        imagDisplayView.image = [self makeImageBlur:[blurValur floatValue]];
    }
}

-(UIImage *)makeImageHue:(float)number
{
    imagDisplayView.image = _imageForEdit;

    CIImage *ciImage = [[CIImage alloc] initWithImage:self.imageForEdit];
    CIFilter *filter = [CIFilter filterWithName:@"CIHueAdjust" keysAndValues:kCIInputImageKey, ciImage, nil];
    number = MIN(1.0, MAX(0.0, number));
    [filter setDefaults];
    [filter setValue:[NSNumber numberWithFloat:number] forKey:@"inputAngle"];
    CIContext *context = [CIContext contextWithOptions:@{kCIContextUseSoftwareRenderer : @(NO)}];
    CIImage *outputImage = [filter outputImage];
    CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
    
    UIImage *result = [UIImage imageWithCGImage:cgImage];
   // imagDisplayView.image = result;
    
    return result;
    
}

- (void)getHighlightedImage:(float)number completion:(void(^)(UIImage *))FliterdImage
{
    
   __block  UIActivityIndicatorView *activityIndicatorForHighLight = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicatorForHighLight.center = CGPointMake(imagDisplayView.frame.size.width/2, imagDisplayView.frame.size.height/2);
    activityIndicatorForHighLight.hidesWhenStopped = YES;
    [imagDisplayView addSubview:activityIndicatorForHighLight];
    
    [activityIndicatorForHighLight startAnimating];


        NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^
                                   {
                                       [[NSOperationQueue mainQueue] addOperationWithBlock:^
                                        {
                                            CIImage *ciImage = [CIImage imageWithCGImage:self.imageForEdit.CGImage];
                                            //    CIImage *ciImage = [[CIImage alloc] initWithImage:self.imageForEdit];
                                            CIFilter *filter = [CIFilter filterWithName:@"CIHighlightShadowAdjust" keysAndValues:kCIInputImageKey, ciImage, nil];
                                            //   number = MIN(1.0, MAX(0.0, number));
                                            [filter setDefaults];
                                            [filter setValue:[NSNumber numberWithFloat:number] forKey:@"inputHighlightAmount"];
                                            [filter setValue:[NSNumber numberWithFloat:number] forKey:@"inputShadowAmount"];
                                            CGFloat R = MAX(self.imageForEdit.size.width, self.imageForEdit.size.height) * 0.02 * number;
                                            [filter setValue:[NSNumber numberWithFloat:R] forKey:@"inputRadius"];
                                            
                                            CIContext *context = [CIContext contextWithOptions:@{kCIContextUseSoftwareRenderer : @(NO)}];
                                            CIImage *outputImage = [filter outputImage];
                                            CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
                                            
                                            UIImage *result = [UIImage imageWithCGImage:cgImage];
                                            
                                            CGImageRelease(cgImage);
                                            
                                            
                                                FliterdImage(result);
                                            [activityIndicatorForHighLight stopAnimating];
                                                
                                         }];
                                    
                                        }];
    
    [operation setQueuePriority:NSOperationQueuePriorityVeryHigh];
    [imageProcessQueue addOperation:operation];
    
    
}




-(UIImage *)makeImageHighlight:(float)number
{
    
    UIActivityIndicatorView *activityIndicatorForHighLight = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicatorForHighLight.center = CGPointMake(imagDisplayView.frame.size.width/2, imagDisplayView.frame.size.height/2);
    activityIndicatorForHighLight.hidesWhenStopped = YES;
    [imagDisplayView addSubview:activityIndicatorForHighLight];

    [activityIndicatorForHighLight startAnimating];

    CIImage *ciImage = [CIImage imageWithCGImage:self.imageForEdit.CGImage];
//    CIImage *ciImage = [[CIImage alloc] initWithImage:self.imageForEdit];
    CIFilter *filter = [CIFilter filterWithName:@"CIHighlightShadowAdjust" keysAndValues:kCIInputImageKey, ciImage, nil];
 //   number = MIN(1.0, MAX(0.0, number));
    [filter setDefaults];
    [filter setValue:[NSNumber numberWithFloat:number] forKey:@"inputHighlightAmount"];
    [filter setValue:[NSNumber numberWithFloat:number] forKey:@"inputShadowAmount"];
    CGFloat R = MAX(self.imageForEdit.size.width, self.imageForEdit.size.height) * 0.02 * number;
    [filter setValue:[NSNumber numberWithFloat:R] forKey:@"inputRadius"];
    
    CIContext *context = [CIContext contextWithOptions:@{kCIContextUseSoftwareRenderer : @(NO)}];
    CIImage *outputImage = [filter outputImage];
    CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
    
    UIImage *result = [UIImage imageWithCGImage:cgImage];
    
    CGImageRelease(cgImage);
    
    [activityIndicatorForHighLight stopAnimating];
    [activityIndicatorForHighLight removeFromSuperview];

  //  imagDisplayView.image = result;
    
    return result;
    
}


-(UIImage *)makeImageBlur:(CGFloat)blurLevel
{

    imagDisplayView.image = _imageForEdit;

    //  blurLevel = MIN(1.0, MAX(0.0, blurLevel));
    int boxSize = (int)(blurLevel * 0.1 * MIN(self.frame.size.width, self.frame.size.height));
    boxSize = boxSize - (boxSize % 2) + 1;
    
    NSData *imageData = UIImageJPEGRepresentation(self.imageForEdit, 1);
    UIImage *tmpImage = [UIImage imageWithData:imageData];
    
    CGImageRef img = tmpImage.CGImage;
    vImage_Buffer inBuffer, outBuffer;
    vImage_Error error;
    void *pixelBuffer;
    
    //create vImage_Buffer with data from CGImageRef
    CGDataProviderRef inProvider = CGImageGetDataProvider(img);
    CFDataRef inBitmapData = CGDataProviderCopyData(inProvider);
    
    inBuffer.width = CGImageGetWidth(img);
    inBuffer.height = CGImageGetHeight(img);
    inBuffer.rowBytes = CGImageGetBytesPerRow(img);
    
    inBuffer.data = (void*)CFDataGetBytePtr(inBitmapData);
    
    //create vImage_Buffer for output
    pixelBuffer = malloc(CGImageGetBytesPerRow(img) * CGImageGetHeight(img));
    
    outBuffer.data = pixelBuffer;
    outBuffer.width = CGImageGetWidth(img);
    outBuffer.height = CGImageGetHeight(img);
    outBuffer.rowBytes = CGImageGetBytesPerRow(img);
    
    NSInteger windowR = boxSize/2;
    CGFloat sig2 = windowR / 3.0;
    if(windowR>0){ sig2 = -1/(2*sig2*sig2); }
    
    int16_t *kernel = (int16_t*)malloc(boxSize*sizeof(int16_t));
    int32_t  sum = 0;
    for(NSInteger i=0; i<boxSize; ++i){
        kernel[i] = 255*exp(sig2*(i-windowR)*(i-windowR));
        sum += kernel[i];
    }
    
    // convolution
    error = vImageConvolve_ARGB8888(&inBuffer, &outBuffer, NULL, 0, 0, kernel, boxSize, 1, sum, NULL, kvImageEdgeExtend)?:
    vImageConvolve_ARGB8888(&outBuffer, &inBuffer, NULL, 0, 0, kernel, 1, boxSize, sum, NULL, kvImageEdgeExtend);
    outBuffer = inBuffer;
    
    free(kernel);
    
    if (error) {
        NSLog(@"error from convolution %ld", error);
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef ctx = CGBitmapContextCreate(outBuffer.data,
                                             outBuffer.width,
                                             outBuffer.height,
                                             8,
                                             outBuffer.rowBytes,
                                             colorSpace,
                                             kCGBitmapAlphaInfoMask & kCGImageAlphaNoneSkipLast);
    CGImageRef imageRef = CGBitmapContextCreateImage(ctx);
    UIImage *returnImage = [UIImage imageWithCGImage:imageRef];
    
    //clean up
    CGContextRelease(ctx);
    CGColorSpaceRelease(colorSpace);
    free(pixelBuffer);
    CFRelease(inBitmapData);
    CGImageRelease(imageRef);
    
    //imagDisplayView.image = returnImage;
    
    return returnImage;
    
    
}

-(UIImage *)makeImageInvertColor
{
    imagDisplayView.image = _imageForEdit;

    CIImage *ciImage = [[CIImage alloc] initWithImage:self.imageForEdit];
    CIFilter *filter = [CIFilter filterWithName:@"CIColorInvert" keysAndValues:kCIInputImageKey, ciImage, nil];
    [filter setDefaults];
    
    CIContext *context = [CIContext contextWithOptions:@{kCIContextUseSoftwareRenderer : @(NO)}];
    CIImage *outputImage = [filter outputImage];
    CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
    
    UIImage *result = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    imagDisplayView.image = result;
    
    return result;
}
-(UIImage *)makeImageSepia
{
    imagDisplayView.image = _imageForEdit;

    CIImage *ciImage = [[CIImage alloc] initWithImage:self.imageForEdit];
    CIFilter *filter = [CIFilter filterWithName:@"CISepiaTone" keysAndValues:kCIInputImageKey, ciImage, nil];
    [filter setDefaults];
    
    CIContext *context = [CIContext contextWithOptions:@{kCIContextUseSoftwareRenderer : @(NO)}];
    CIImage *outputImage = [filter outputImage];
    CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
    
    UIImage *result = [UIImage imageWithCGImage:cgImage];
    
    CGImageRelease(cgImage);
    imagDisplayView.image = result;
    
    return result;
    
}


@end
