//
//  chatViewController.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 22/02/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIBubbleTableView.h"
#import "XMPPRosterCoreDataStorage.h"
#import "HPGrowingTextView.h"
#import <MediaPlayer/MediaPlayer.h>
#import "PECropViewController.h"


@interface GroupChatViewController : UIViewController<UIBubbleTableViewDataSource,HPGrowingTextViewDelegate,UIActionSheetDelegate,PECropViewControllerDelegate>

@property(nonatomic,retain)XMPPJID *user;
@property (strong, nonatomic) Singleton* singleton;

-(instancetype)initWithRoomJid:(XMPPJID *)roomJID;
@end
