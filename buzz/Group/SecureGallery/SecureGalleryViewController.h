//
//  SecureGalleryViewController.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 30/07/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PendingOperations.h"
#import "FileDownloader.h"
#import "GroupInformationViewController.h"
#import "FetchsharedFile.h"
#import "FileRecord.h"
#import "SharedCollectionViewCell.h"
#import "PasswordPin.h"

@interface SecureGalleryViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,FileDownloaderDelegate>
@property(nonatomic,strong)NSMutableArray *photos;
@property(nonatomic,strong)NSMutableArray *videos;
@property (nonatomic, strong) PendingOperations *pendingOperations;
@property(nonatomic,strong)UICollectionView * collectionView;
@end
