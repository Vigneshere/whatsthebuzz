//
//  DetailGalleryViewController.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 30/07/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVFoundation.h>

@interface DetailGalleryViewController : UIViewController <UIActionSheetDelegate>
@property(nonatomic,strong)NSMutableArray *detailArray;
@property(nonatomic,strong)UICollectionView * collectionView;
@property(nonatomic,strong)NSIndexPath *indexpath;
@end
