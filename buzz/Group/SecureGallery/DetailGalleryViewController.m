//
//  DetailGalleryViewController.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 30/07/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "DetailGalleryViewController.h"
#import "DetailedImageCell.h"
#import "FileRecord.h"
#import <MediaPlayer/MediaPlayer.h>
#import "mediaDbEditor.h"


#define stickerZoomIn 0.4

#define stickerZoomOut 1.2
@interface DetailGalleryViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UIScrollViewDelegate,DetailedImageCellDelegate,UIScrollViewDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate>
@end

@implementation DetailGalleryViewController
{
    BOOL isFullScreen;
    UIToolbar *bottomToolBar;
    UIBarButtonItem *_nextButton;
	UIBarButtonItem *_prevButton;
	NSMutableArray *_barItems;
	CGFloat _prevNextButtonSize;
	NSInteger _currentIndex;
    UILabel *dataLabel;
    UIButton *menuButton;
}
-(void)loadView
{
    self.view = [[UIView alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
    self.view.clipsToBounds = YES;
    self.navigationController.navigationItem.hidesBackButton = YES;
    self.view.backgroundColor = RGBA(249, 249, 249, 1);
}


- (AppDelegate *)appDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (XMPPStream *)xmppStream
{
    return [[self appDelegate] xmppStream];
}
-(XMPPMessageArchivingCoreDataStorage *)xmppMessageArchivingCoreDataStorage
{
    
    return [[self appDelegate] xmppMessageArchivingStorage];
}

- (NSManagedObjectContext *)managedObjecMessageContact
{
    return [[[self appDelegate] xmppMessageArchivingStorage] mainThreadManagedObjectContext];
}


- (void)layoutButtons
{
	NSUInteger buttonWidth = roundf(bottomToolBar.frame.size.width / [_barItems count] - _prevNextButtonSize * .5);
	
	NSUInteger i, count = [_barItems count];
	for (i = 0; i < count; i++) {
		UIBarButtonItem *btn = [_barItems objectAtIndex:i];
		btn.width = buttonWidth;
	}
	[bottomToolBar setNeedsLayout];
}
-(void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(automaticallyAdjustsScrollViewInsets)]){
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    self.extendedLayoutIncludesOpaqueBars = YES;
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    _collectionView=[[UICollectionView alloc] initWithFrame:[[UIScreen mainScreen] bounds] collectionViewLayout:layout];
    [_collectionView setBackgroundColor:[UIColor clearColor] ];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [layout setMinimumInteritemSpacing:0.0f];
    [layout setMinimumLineSpacing:0.0f];
    [self.collectionView setPagingEnabled:YES];
    [_collectionView registerClass:[DetailedImageCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    
    bottomToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 44, self.view.frame.size.width, 44)];
    bottomToolBar.barStyle = UIBarStyleBlackTranslucent;
    bottomToolBar.backgroundColor = [UIColor blackColor];
    self.hidesBottomBarWhenPushed		= YES;
    
    _barItems = [NSMutableArray new];
    
    UIImage *leftIcon = [UIImage imageNamed:@"photo-gallery-left.png"];
    UIImage *rightIcon = [UIImage imageNamed:@"photo-gallery-right.png"];
    _nextButton = [[UIBarButtonItem alloc] initWithImage:rightIcon style:UIBarButtonItemStyleBordered target:self action:@selector(next)];
    _prevButton = [[UIBarButtonItem alloc] initWithImage:leftIcon style:UIBarButtonItemStyleBordered target:self action:@selector(previous)];
    
    _prevNextButtonSize = leftIcon.size.width;
    
    // add prev next to front of the array
    [_barItems insertObject:_nextButton atIndex:0];
    [_barItems insertObject:_prevButton atIndex:0];
    
    
    // set buttons on the toolbar.
    [bottomToolBar setItems:_barItems animated:NO];
    
    dataLabel = [UILabel new];
    dataLabel.frame = CGRectMake((self.view.frame.size.width/2)-50, 0,120,40);
    dataLabel.backgroundColor = [UIColor clearColor];
    dataLabel.textColor = [UIColor whiteColor];
    dataLabel.textAlignment = NSTextAlignmentCenter;
    dataLabel.font = [UIFont fontWithName:FontString size:15];
    [bottomToolBar addSubview:dataLabel];
    
    UIImage *menuImageIcon = [UIImage imageNamed:@"menu-icon"];
    menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    menuButton.backgroundColor = [UIColor clearColor];
    menuButton.frame = CGRectMake(self.view.frame.size.width - 60, 0, 60, 40);
    [menuButton setImage:menuImageIcon forState:UIControlStateNormal];
    menuButton.imageEdgeInsets = UIEdgeInsetsMake( 10.0, 5.0, 0.0, 0.0);
    [menuButton addTarget:self action:@selector(openActionSheet) forControlEvents:UIControlEventTouchDown];
    
    

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
     _currentIndex = self.indexpath.row;
     isFullScreen = YES;
    
    [_collectionView scrollToItemAtIndexPath:self.indexpath atScrollPosition:UICollectionViewScrollPositionNone animated:NO];

    [self updateButtons];
    [self layoutButtons];

    [self.view addSubview:_collectionView];
    [self.view addSubview:bottomToolBar];


    [self.navigationController.navigationBar addSubview:menuButton];

    // Disable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
    
    [self enterfullorExitScreen:NO];

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
   

    
    
}



-(void)viewWillDisappear:(BOOL)animated{

  
     if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        self.extendedLayoutIncludesOpaqueBars = YES;
    }
    
  [super viewWillDisappear:YES];
    
    // Enable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
    
    [menuButton removeFromSuperview];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}

#pragma mark - CollectionView DataSource And Delegate Methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.detailArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    DetailedImageCell *cell= (DetailedImageCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    cell.delegate = self;
    cell.backgroundColor=[UIColor clearColor];
   
   FileRecord *aRecord = [self.detailArray objectAtIndex:indexPath.row];
    if (aRecord.hasFile) {
        cell.imageView.image = aRecord.image;
    }
    else
    {
        cell.imageView.image = [self getImageFortheRecord:aRecord Indexpath:indexPath];
    }
    if ([aRecord.type isEqualToString:@"image"])
        [cell.imageView.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    else
        [cell.imageView addSubview:cell.blackTransVw];

    [cell.imageView sizeToFit];
    cell.imageView.center = cell.scrollView.center;
    
    [self.collectionView addGestureRecognizer:cell.scrollView.pinchGestureRecognizer];
    [self.collectionView addGestureRecognizer:cell.scrollView.panGestureRecognizer];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(DetailedImageCell *)cell
    forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.collectionView removeGestureRecognizer:cell.scrollView.pinchGestureRecognizer];
    [self.collectionView removeGestureRecognizer:cell.scrollView.panGestureRecognizer];
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return _collectionView.frame.size;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
   
    FileRecord *aRecord = [self.detailArray objectAtIndex:indexPath.row];
    self.indexpath = indexPath;


    if ([aRecord.type isEqualToString:@"video"]) {
        MPMoviePlayerViewController* movieController = [[MPMoviePlayerViewController alloc] initWithContentURL:aRecord.URL];
        [self presentMoviePlayerViewControllerAnimated:movieController];
        [movieController.moviePlayer prepareToPlay];
        [movieController.moviePlayer play];
        
       
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(doneButtonClick)
                                                     name:MPMoviePlayerDidExitFullscreenNotification
                                                   object:movieController.moviePlayer];

    }
    
}

-(void)enterfullorExitScreen:(BOOL)boolValue
{
    [self disableApp];

    if (boolValue == YES)
    {
        [_collectionView setBackgroundColor:[UIColor blackColor]];
        
        
        //[[UIApplication sharedApplication] setStatusBarHidden:boolValue withAnimation:UIStatusBarAnimationSlide];
        [self.navigationController setNavigationBarHidden:boolValue animated:!boolValue];
        
        [UIView beginAnimations:@"galleryOut" context:nil];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(enableApp)];
        bottomToolBar.alpha = 0.0;
        [UIView commitAnimations];
        isFullScreen =  NO;

    }
    else
    {
        [_collectionView setBackgroundColor:[UIColor clearColor]];
        //[[UIApplication sharedApplication] setStatusBarHidden:boolValue withAnimation:UIStatusBarAnimationSlide];
        [self.navigationController setNavigationBarHidden:boolValue animated:boolValue];
        
        [UIView beginAnimations:@"galleryIn" context:nil];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(enableApp)];
        bottomToolBar.alpha = 1.0;
        [UIView commitAnimations];
        isFullScreen = YES;
    }

}

-(void)doneButtonClick
{
    [self enterfullorExitScreen:NO];
 
}

- (void)enableApp
{
	[[UIApplication sharedApplication] endIgnoringInteractionEvents];
}
- (void)disableApp
{
	[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
}

-(UIImage *)getImageFortheRecord:(FileRecord *)aRecord Indexpath:(NSIndexPath *)indexPath
{
    UIImage *img = nil;
    
    NSString *mediaURL =[self getFullDocumentUrl:[aRecord.URL  absoluteString]];
    if ([aRecord.type isEqualToString:@"image"])
        
    
        
            img =  [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:mediaURL]]];
    else
         img = [self constructImageFromVidUrl:[NSURL URLWithString:mediaURL]];
    
    aRecord.image = img;
    [self.detailArray replaceObjectAtIndex:indexPath.row withObject:aRecord];

    return img;

}

- (NSString *)getFullDocumentUrl:(NSString *)fileName
{
    
    NSRange range = [fileName rangeOfString:@"Documents"];
    
    NSString *newString = [fileName substringFromIndex:range.location+10];
  //  NSLog(@"new string %@",newString);
    
    return [NSString stringWithFormat:@"file://%@/%@",[self applicationDocumentsDirectory],newString];
}

- (NSString *)applicationDocumentsDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

#pragma mark - Getting Single Image from Video File
-(UIImage*)constructImageFromVidUrl:(NSURL*)url
{
    AVAsset *asset = [AVAsset assetWithURL:url];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    CMTime time = CMTimeMake(1, 1);
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
    return thumbnail;
}

- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}
-(BOOL)needtoshowNavigation
{
     if (!isFullScreen)
         return NO;
    
         return YES;
}
-(void)didSelectImageView:(BOOL)selected Gesture:(UIGestureRecognizer *)gesture
{
    [self enterfullorExitScreen:selected];
    
    CGPoint p = [gesture locationInView:self.collectionView];
    
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:p];
    
    [self collectionView:self.collectionView didSelectItemAtIndexPath:indexPath];
}
-(void)imageZoomedIn
{
    _collectionView.scrollEnabled = NO;
}
-(void)imageZoomedOut
{
    _collectionView.scrollEnabled = YES;
}
-(void)scrolledAtEdges:(NSInteger)value
{
    
    if (value == 0) {
        [self previous];
    }
    else if (value == 1)
    {
        [self next];
    }
}

- (void)next
{
    
    [self zoomToOriginalSize];
    NSUInteger numberOfPhotos = self.detailArray.count;
	NSUInteger nextIndex = _currentIndex+1;
	
	// don't continue if we're out of images.
	if( nextIndex <= numberOfPhotos )
        [self scrollToIndexPosition:nextIndex];
    
}
-(void)previous
{
    [self zoomToOriginalSize];

	NSUInteger previousIndex = _currentIndex-1;
	[self scrollToIndexPosition:previousIndex];
}
-(void)scrollToIndexPosition:(NSInteger )index
{
    NSUInteger numPhotos = self.detailArray.count;
    if( index >= numPhotos ) index = 0;	
	if( numPhotos == 0 ) {
        _currentIndex = -1;
	}
	else {
        _currentIndex = index;
    [_collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
    }
      [self updateButtons];
    
   
    
}

- (void)updateButtons
{
	_prevButton.enabled = ( _currentIndex <= 0 ) ? NO : YES;
	_nextButton.enabled = ( _currentIndex >= self.detailArray.count-1 ) ? NO : YES;
    
    dataLabel.text = [NSString stringWithFormat:@"%ld/%ld",_currentIndex+1,(unsigned long)self.detailArray.count];
    
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    for (UICollectionViewCell *cell in [self.collectionView visibleCells]) {
        NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
        _currentIndex = indexPath.row;
        [self updateButtons];
    }
}

-(void)zoomToOriginalSize
{
    DetailedImageCell *cell = (DetailedImageCell *)[_collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:_currentIndex inSection:0]];
    
    if (cell.scrollView.zoomScale > cell.scrollView.minimumZoomScale) {
        CGRect zoomRect = [cell zoomRectForScale:cell.scrollView.minimumZoomScale withCenter:CGPointMake(0, 0)];
        [cell.scrollView zoomToRect:zoomRect animated:YES];
    }
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    if ([buttonTitle isEqualToString:NSLocalizedString(@"Save to Secure Gallery", nil)]) {
        [self makeImageSecure];
        
    } else if ([buttonTitle isEqualToString:NSLocalizedString(@"Save to Gallery", nil)])
    {
        [self saveToGallery];
    }
    else if ([buttonTitle isEqualToString:NSLocalizedString(@"Unpin from Secure Gallery", nil)])
    {
    [self makeImageUnSecure];
    }
  
}



- (void)openActionSheet
{
    FileRecord *aRecord = [self.detailArray objectAtIndex:_currentIndex];
    
 

    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Save to Gallery", nil)];
    if (aRecord.isPrivate) {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Unpin from Secure Gallery", nil)];
    }
    else
    {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Save to Secure Gallery", nil)];
    }
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    actionSheet.cancelButtonIndex = actionSheet.numberOfButtons - 1;
    
    
    [actionSheet showFromToolbar:bottomToolBar];
    
}


-(void)saveToGallery
{
    
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    FileRecord *aRecord = [self.detailArray objectAtIndex:_currentIndex];
    
    if([aRecord.type isEqualToString:@"image"])
    {
    if (aRecord.hasFile) {
         UIImage *image = aRecord.image;
        [library writeImageToSavedPhotosAlbum:[image CGImage] orientation:(ALAssetOrientation)[image imageOrientation] completionBlock:^(NSURL *assetURL, NSError *error){
            if (error)
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"OOPS" message:@"Something went wrong" delegate:nil cancelButtonTitle:@"Try Again!" otherButtonTitles: nil];
                [alert show];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Done" message:@"Image saved in Gallery." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
            }
        }];

    }
    }
    else
    {
        
        if ([aRecord.type isEqualToString:@"video"])
        {
            ALAssetsLibrary* library = [[ALAssetsLibrary alloc] init];
            [library writeVideoAtPathToSavedPhotosAlbum:aRecord.URL
                                        completionBlock:^(NSURL *assetURL, NSError *error){
            if (error)
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"OOPS" message:@"Something went wrong" delegate:nil cancelButtonTitle:@"Try Again!" otherButtonTitles: nil];
                [alert show];
            
            } else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success!" message:@"Video saved to gallery." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
                
            }
            }];
        }
        
    }
    
    
}


-(void)makeImageSecure
{
    
    FileRecord *aRecord = [self.detailArray objectAtIndex:_currentIndex];

    XMPPMessageArchiving_Message_CoreDataObject *coremessage = [self MsgWithID:aRecord.msgId];
    coremessage.isPrivate = YES;
    coremessage.privateStatus = YES;
    
    
    mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
    NSMutableDictionary* dict = [NSMutableDictionary new];
    [dict setObject:aRecord.msgId  forKey:@"msgId"];
    [dict setObject:[NSNumber numberWithBool:YES] forKey:@"isPrivate"];
    [mediaDb updateMedia:dict];
    
    NSManagedObjectContext *context= [self managedObjecMessageContact];
    NSError *error = nil;
    
    if (![context save:&error])
    {
        //  NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
    }
    
    //** Refresh
    NSInteger rowToDelete = self.indexpath.row;
    [self.detailArray removeObjectAtIndex:rowToDelete];
    [self.collectionView reloadData];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Hurray! Your Image is now safe. Access it through Secure Gallery" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alert show];
    
}

-(void)makeImageUnSecure
{
    
    FileRecord *aRecord = [self.detailArray objectAtIndex:_currentIndex];
    
    XMPPMessageArchiving_Message_CoreDataObject *coremessage = [self MsgWithID:aRecord.msgId];
    coremessage.isPrivate = NO;
    coremessage.privateStatus = NO;
    
    NSManagedObjectContext *context= [self managedObjecMessageContact];
    
    mediaDbEditor* mediaDb = [mediaDbEditor new];
    NSMutableDictionary* dict = [NSMutableDictionary new];
    [dict setObject:aRecord.msgId  forKey:@"msgId"];
    [dict setObject:[NSNumber numberWithBool:NO] forKey:@"isPrivate"];
    [mediaDb updateMedia:dict];
    
    NSError *error = nil;
    
    if (![context save:&error])
    {
        //    NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
    }
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Done!" message:@"The image is now visible to all." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alert show];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

  //  NSLog(@"MEMORY Warning in Detail Gallery View Controller");

}

- (XMPPMessageArchiving_Message_CoreDataObject*)MsgWithID:(NSString*)msgId
{
    NSManagedObjectContext *moc = [[self xmppMessageArchivingCoreDataStorage] mainThreadManagedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:moc];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    NSString *predicateFrmt = @"messageID == %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,msgId];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:[NSSortDescriptor sortValues:@"timestamp" ascending:YES]];
    NSArray *delArray=[moc executeFetchRequest:fetchRequest error:nil];
    
    
    for (XMPPMessageArchiving_Message_CoreDataObject *CoreDataObject in delArray)
    {
        return CoreDataObject;
    }
    return nil;
}



@end
