//
//  SharedCollectionViewCell.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 28/07/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "DetailedImageCell.h"
#define widthOfBlackTransVw 30
#define ZOOM_STEP 2.0

@implementation DetailedImageCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.scrollView = [UIScrollView new];
        self.scrollView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
        self.scrollView.delegate = self;
        self.scrollView.userInteractionEnabled  = YES;
        self.scrollView.bouncesZoom = YES;
        [self addSubview:self.scrollView];
        
        self.imageView = [UIImageView new];
        self.imageView.userInteractionEnabled = YES;
        self.imageView.clipsToBounds = YES;
        [self.scrollView addSubview:self.imageView];
        
        self.blackTransVw = [UIView new];
        self.blackTransVw.backgroundColor = RGBA(0, 0, 0, 0.6);
        self.blackTransVw.frame = CGRectMake((self.imageView.frame.size.width - widthOfBlackTransVw)/2, (self.imageView.frame.size.height - widthOfBlackTransVw)/2, widthOfBlackTransVw, widthOfBlackTransVw);
        self.blackTransVw.layer.cornerRadius = widthOfBlackTransVw/2;
        self.blackTransVw.layer.masksToBounds = YES;
        
        UIImage *playIcon = [UIImage imageNamed:@"play"];
        UIImageView* playImageIconView = [UIImageView new];
        playImageIconView.frame = CGRectMake((self.blackTransVw.frame.size.width - playIcon.size.width)/2, (self.blackTransVw.frame.size.height - playIcon.size.height)/2, playIcon.size.width, playIcon.size.height);
        playImageIconView.image = playIcon;
        playImageIconView.userInteractionEnabled = YES;
        [self.blackTransVw addSubview:playImageIconView];
        
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
        UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
        UITapGestureRecognizer *twoFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTwoFingerTap:)];
        
        [singleTap setNumberOfTapsRequired:1];
        [doubleTap setNumberOfTapsRequired:2];
        [twoFingerTap setNumberOfTouchesRequired:2];
        
        //Adding gesture recognizer
        [self addGestureRecognizer:singleTap];
        [self.imageView addGestureRecognizer:doubleTap];
        [self.imageView addGestureRecognizer:twoFingerTap];
        [singleTap requireGestureRecognizerToFail:doubleTap];

        
        float minimumScale = 1.0;
        self.scrollView.maximumZoomScale = 6.0;
        self.scrollView.minimumZoomScale = minimumScale;
        self.scrollView.zoomScale = minimumScale;
        [self.imageView setContentMode:UIViewContentModeScaleAspectFit];
        [self.scrollView setContentSize:self.imageView.bounds.size];
        self.imageView.autoresizingMask = (UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth);
        }
    return self;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}
- (void)scrollViewDidZoom:(UIScrollView *)aScrollView
{

    CGFloat offsetX = (self.scrollView.bounds.size.width > self.scrollView.contentSize.width)?
    (self.scrollView.bounds.size.width - self.scrollView.contentSize.width) * 0.5 : 0.0;
    CGFloat offsetY = (self.scrollView.bounds.size.height > self.scrollView.contentSize.height)?
    (self.scrollView.bounds.size.height - self.scrollView.contentSize.height) * 0.5 : 0.0;
    self.imageView.center = CGPointMake(self.scrollView.contentSize.width * 0.5 + offsetX,
                                   self.scrollView.contentSize.height * 0.5 + offsetY);
    
    
   
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    float rightEdge = self.scrollView.contentOffset.x + self.scrollView.frame.size.width;
    if ((rightEdge > self.scrollView.contentSize.width+100) && self.scrollView.contentSize.width > self.frame.size.width)
    {
        if([self.delegate respondsToSelector:@selector(scrolledAtEdges:)])
            [self.delegate scrolledAtEdges:1];
        
      //  NSLog(@"right");
    }
    else if(self.scrollView.contentOffset.x < -99)
    {
        if([self.delegate respondsToSelector:@selector(scrolledAtEdges:)])
            [self.delegate scrolledAtEdges:0];
        
     //   NSLog(@"left");

    }
    
}
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    if (scale <= 1){

        if([self.delegate respondsToSelector:@selector(imageZoomedOut)])
            [self.delegate imageZoomedOut];
    }
    else
    {
        if([self.delegate respondsToSelector:@selector(imageZoomedIn)])
            [self.delegate imageZoomedIn];
    }
    
    
}
#pragma mark TapDetectingImageViewDelegate methods

-(void)handleSingleTap:(UIGestureRecognizer *)gestureRecognizer
{
   
    
    if (![self.delegate needtoshowNavigation])
    {
    if([self.delegate respondsToSelector:@selector(didSelectImageView:Gesture:)])
        [self.delegate didSelectImageView:NO Gesture:gestureRecognizer];
    }
    else
    {
        if([self.delegate respondsToSelector:@selector(didSelectImageView:Gesture:)])
            [self.delegate didSelectImageView:YES Gesture:gestureRecognizer];
    }
}
- (void)handleDoubleTap:(UIGestureRecognizer *)gestureRecognizer {
    // zoom in
    float newScale = [self.scrollView zoomScale] * ZOOM_STEP;
    
    if (newScale > self.scrollView.maximumZoomScale){

        newScale = self.scrollView.minimumZoomScale;
        CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
        
        [self.scrollView zoomToRect:zoomRect animated:YES];
    }
    else{

        newScale = self.scrollView.maximumZoomScale;
        CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
        
        [self.scrollView zoomToRect:zoomRect animated:YES];
        }
}
- (void)handleTwoFingerTap:(UIGestureRecognizer *)gestureRecognizer {
    // two-finger tap zooms out
    float newScale = [self.scrollView zoomScale] / ZOOM_STEP;
    CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
    [self.scrollView zoomToRect:zoomRect animated:YES];
    
}
#pragma mark Utility methods

- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center {
    
    CGRect zoomRect;
    
    // the zoom rect is in the content view's coordinates.
    //    At a zoom scale of 1.0, it would be the size of the self.scrollView's bounds.
    //    As the zoom scale decreases, so more content is visible, the size of the rect grows.
    zoomRect.size.height = [self.scrollView frame].size.height / scale;
    zoomRect.size.width  = [self.scrollView frame].size.width  / scale;
    
    // choose an origin so as to get the right center.
    zoomRect.origin.x    = center.x - (zoomRect.size.width  / 2.0);
    zoomRect.origin.y    = center.y - (zoomRect.size.height / 2.0);
    
    return zoomRect;
}
@end
