//
//  SharedCollectionViewCell.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 28/07/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol DetailedImageCellDelegate <NSObject>
@required
-(BOOL)needtoshowNavigation;
-(void)didSelectImageView:(BOOL)selected Gesture:(UIGestureRecognizer *)gesture;
-(void)imageZoomedIn;
-(void)imageZoomedOut;
-(void)scrolledAtEdges:(NSInteger)value;
@end

@interface DetailedImageCell : UICollectionViewCell<UIScrollViewDelegate>
@property(nonatomic,strong)UIImageView* imageView;
@property(nonatomic,strong) UIView* blackTransVw;
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,assign)id <DetailedImageCellDelegate>delegate;
- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center;
@end
