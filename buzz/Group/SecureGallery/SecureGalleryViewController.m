//
//  SecureGalleryViewController.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 30/07/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "SecureGalleryViewController.h"
#import "DetailGalleryViewController.h"
#import "mediaDbEditor.h"
#import "AppDelegate.h"

@interface SecureGalleryViewController ()
@property (nonatomic, strong) NSMutableArray *cellHeights;


@end

@implementation SecureGalleryViewController
{
    UISegmentedControl *segmentedControl;
    PasswordPin* passwordVw;
    BOOL canSelect;
    NSMutableArray *selectedAry;
    UIButton *menuButton;
    UIButton *unSecureBtn;
    UILabel *titleLabel;

    
}


- (AppDelegate *)appDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}


- (NSManagedObjectContext *)managedObjecMessageContact
{
    return [[[self appDelegate] xmppMessageArchivingStorage] mainThreadManagedObjectContext];
}

-(XMPPMessageArchivingCoreDataStorage *)xmppMessageArchivingCoreDataStorage
{
    
    return [[self appDelegate] xmppMessageArchivingStorage];
}


-(void)loadView
{
    self.view = [[UIView alloc]initWithFrame:[[UIScreen mainScreen] applicationFrame]];
    
    if(IS_IPHONE_4)
    {
        self.view.backgroundColor = BACKGROUNDIMAGE_4;
        
    }
    else
    {
        self.view.backgroundColor = BACKGROUNDIMAGE;
        
    }
    // Initialization code
    self.photos = [NSMutableArray new];
    self.videos = [NSMutableArray new];
    selectedAry = [NSMutableArray new];
    NSArray *itemArray = [NSArray arrayWithObjects: @"Image", @"Video", nil];
    segmentedControl = [[UISegmentedControl alloc] initWithItems:itemArray];
    segmentedControl.frame = CGRectMake(5, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height + 10, self.view.frame.size.width-10, 30);
    [segmentedControl addTarget:self action:@selector(MySegmentControlAction:) forControlEvents: UIControlEventValueChanged];
    segmentedControl.selectedSegmentIndex = 0;
    [self.view addSubview:segmentedControl];
    
    UIImage *menuImageIcon = [UIImage imageNamed:@"menu-icon"];
    menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    menuButton.backgroundColor = [UIColor clearColor];
    menuButton.frame = CGRectMake(self.view.frame.size.width-60, 0, 60, 40);
    [menuButton setImage:menuImageIcon forState:UIControlStateNormal];
    menuButton.imageEdgeInsets = UIEdgeInsetsMake( 10.0, 5.0, 0.0, 0.0);
    [menuButton addTarget:self action:@selector(menuButtonAction) forControlEvents:UIControlEventTouchDown];

   // UIImage *menuImageIcon = [UIImage imageNamed:@"menu-icon"];
    unSecureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    unSecureBtn.backgroundColor = [UIColor clearColor];
    unSecureBtn.frame = CGRectMake(menuButton.frame.origin.x - 70, 0, 60, 40);
    [unSecureBtn setTitle:@"unsecure" forState:UIControlStateNormal];
    unSecureBtn.imageEdgeInsets = UIEdgeInsetsMake( 10.0, 5.0, 0.0, 0.0);
    [unSecureBtn addTarget:self action:@selector(unSecureButtonAction) forControlEvents:UIControlEventTouchDown];

    titleLabel = [UILabel new];
    titleLabel.frame = CGRectMake(60, 10, 320 - 120, 30);
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor blackColor] ;
    titleLabel.font = [UIFont fontWithName:FontString size:16];
    titleLabel.text = @"Secure Gallery";
    
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    
   
    
    
    _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, segmentedControl.frame.origin.y+segmentedControl.frame.size.height+10, self.view.frame.size.width, self.view.frame.size.height-(segmentedControl.frame.origin.y+segmentedControl.frame.size.height-10)) collectionViewLayout:layout];
    [_collectionView setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.0]];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [_collectionView setScrollEnabled:YES];
    [_collectionView registerClass:[SharedCollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    
  

    
    
    [self.view addSubview:_collectionView];
    
    [self getData:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(privatePassword:) name:@"privatePassword" object:nil];
    
    [self CheckPassword];

    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [self.navigationController.navigationBar addSubview:menuButton];
    [self.navigationController.navigationBar addSubview:titleLabel];
    [self.navigationController.navigationBar setHidden:NO];

    menuButton.hidden = YES;
    [_collectionView reloadData];

    segmentedControl.selectedSegmentIndex = 0;
    
    canSelect = NO;
    
}
-(void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    [self cancelAllOperations];
 
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    [menuButton removeFromSuperview];
    [unSecureBtn removeFromSuperview];
    [titleLabel removeFromSuperview];

}
- (void)MySegmentControlAction:(UISegmentedControl *)segment
{
    if (segment.selectedSegmentIndex == 0 && self.photos.count > 0)
    {
        menuButton.hidden = NO;
    }
    
    else if (segment.selectedSegmentIndex == 1 && self.videos.count > 0)
    {
        menuButton.hidden = NO;
    }
    else
    {
        menuButton.hidden = YES;

    }
    
  
    UIImage *menuImageIcon = [UIImage imageNamed:@"menu-icon"];
    [menuButton setImage:menuImageIcon forState:UIControlStateNormal];

    
    [selectedAry removeAllObjects];
    canSelect = NO;

    [_collectionView reloadData];
    [self cancelAllOperations];
}

-(void)menuButtonAction
{
    if(!canSelect)
    {
        canSelect = !canSelect;
        [selectedAry removeAllObjects];
        [_collectionView reloadData];
        
        
        
    //    [self.navigationController.navigationBar addSubview:unSecureBtn];


    }
    else
    {
       
        if (selectedAry.count == 0) {

            
            [selectedAry removeAllObjects];
            canSelect = !canSelect;
            [_collectionView reloadData];
            
            
            
        }
        else
        {
        for (NSString *msgId in selectedAry) {
            
            mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
            [mediaDb deleteMedia:msgId];
            [self getData:nil];
        }
        
        [selectedAry removeAllObjects];
        canSelect = !canSelect;
        [_collectionView reloadData];
            
            UIImage *menuImageIcon = [UIImage imageNamed:@"appleTrash"];
            [menuButton setImage:menuImageIcon forState:UIControlStateNormal];


        }


    }
    
    
}

-(void)unSecureButtonAction
{
    
    if (selectedAry.count == 0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Select atleast one item" message:nil delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
    for (NSString *msgId in selectedAry)
    {
        [self unSecureMethod:msgId];
    }
    
    [selectedAry removeAllObjects];
    [self getData:nil];
    [_collectionView reloadData];
    [unSecureBtn removeFromSuperview];
    }
}

-(void)unSecureMethod:(NSString *)EditMessageId
{
    XMPPMessageArchiving_Message_CoreDataObject *coremessage = [self MsgWithID:EditMessageId];
    coremessage.isPrivate = NO;
    coremessage.privateStatus = NO;
    
    
    mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
    NSMutableDictionary* dict = [NSMutableDictionary new];
    [dict setObject:EditMessageId  forKey:@"msgId"];
    [dict setObject:[NSNumber numberWithBool:NO] forKey:@"isPrivate"];
    [mediaDb updateMedia:dict];
    
    NSManagedObjectContext *context= [self managedObjecMessageContact];
    NSError *error = nil;
    
    if (![context save:&error])
    {
      //  NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
    }
    
}


- (XMPPMessageArchiving_Message_CoreDataObject*)MsgWithID:(NSString*)msgId
{
    NSManagedObjectContext *moc = [[self xmppMessageArchivingCoreDataStorage] mainThreadManagedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:moc];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    NSString *predicateFrmt = @"messageID == %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,msgId];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:[NSSortDescriptor sortValues:@"timestamp" ascending:YES]];
    NSArray *delArray=[moc executeFetchRequest:fetchRequest error:nil];
    
    
    for (XMPPMessageArchiving_Message_CoreDataObject *CoreDataObject in delArray)
    {
        return CoreDataObject;
    }
    return nil;
}





-(void)CheckPassword
{
    fromSecurityQuest = NO;

    NSString *pwdString = [[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat:@"%@",MASTER_PIN]];
    if (pwdString.length == 0) {
        passwordVw = [[PasswordPin alloc]initWithFrame_onView:self.view LabelString:@"Set your Master Unlock Key" password:@"" Hint:SET_PRIVATE_PWD JID:@""];
        [passwordVw createPasswordVw];
    }
    else
    {
        passwordVw = [[PasswordPin alloc]initWithFrame_onView:self.view LabelString:@"Enter the Master Unlock Key" password:@"" Hint:ENTER_PRIVATE_PWD JID:@""];
        [passwordVw createPasswordVw];
    }
    
    
}




-(void)privatePassword:(NSNotification *)notification
{
    
    [passwordVw removeFromSuperview];
    passwordVw = nil;
    
    NSDictionary *dict = [notification userInfo];
    
    if ([[dict objectForKey:@"Mode"] isEqualToString:SET_PRIVATE_PWD])
    {
        if ([[dict objectForKey:@"isCorrect"] isEqualToString:@"YES"])
        {
            passwordVw = [[PasswordPin alloc]initWithFrame_onView:self.view LabelString:@"Confirm your Master Unlock Key " password:@"" Hint:CONFRM_PRIVATE_PWD JID:@""];
            [passwordVw createPasswordVw];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:MASTER_PIN];
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    }
    else if ([[dict objectForKey:@"Mode"] isEqualToString:CONFRM_PRIVATE_PWD])
    {
        if ([[dict objectForKey:@"isCorrect"] isEqualToString:@"YES"])
        {
            
            
            [UIView transitionWithView: _collectionView
                              duration: 0.35f
                               options: UIViewAnimationOptionTransitionCrossDissolve
                            animations: ^(void)
             {
                 [_collectionView reloadData];
             }
                            completion: ^(BOOL isFinished)
             {
             }];
            
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:MASTER_PIN];
            [self.navigationController popViewControllerAnimated:YES];
            
            
        }
        
        
    }else if ([[dict objectForKey:@"Mode"] isEqualToString:ENTER_PRIVATE_PWD])
    {
        if ([[dict objectForKey:@"isCorrect"] isEqualToString:@"YES"])
        {
            
            
            [UIView transitionWithView: _collectionView
                              duration: 0.35f
                               options: UIViewAnimationOptionTransitionCrossDissolve
                            animations: ^(void)
             {
                 [_collectionView reloadData];
             }
                            completion: ^(BOOL isFinished)
             {
             }];
            
        }
        else
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
        
        
    }
}



#pragma mark - CollectionView DataSource And Delegate Methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    menuButton.hidden = YES;
    if (segmentedControl.selectedSegmentIndex == 0 && self.photos.count)
    {
        menuButton.hidden = NO;
        return self.photos.count;
    }
    else if (segmentedControl.selectedSegmentIndex == 1 && self.videos.count)
    {
        menuButton.hidden = NO;

        return self.videos.count;
    }
    
    return 0;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{

    SharedCollectionViewCell *cell= (SharedCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    cell.backgroundColor=[UIColor clearColor];
    if (canSelect)
    {
        cell.checkBoxImgVw.image = [UIImage imageNamed:@"check_U"];
    }
    else
    {
        cell.checkBoxImgVw.image = nil;
    }
    
    FileRecord *aRecord;
    
    [[cell.imageview subviews] makeObjectsPerformSelector: @selector(removeFromSuperview)];
    if (segmentedControl.selectedSegmentIndex == 0 && self.photos.count)
        aRecord = [self.photos objectAtIndex:indexPath.row];
    else if (segmentedControl.selectedSegmentIndex == 1 && self.videos.count)
        aRecord = [self.videos objectAtIndex:indexPath.row];
    
    
    if ([selectedAry containsObject:aRecord.msgId])
    {
        cell.checkBoxImgVw.image = [UIImage imageNamed:@"check_S"];
        
    }

    
    
    if (aRecord.hasFile)
    {
        cell.imageview.image = [UIImage generatePhotoThumbnail:aRecord.image withSide:100];
        if ([aRecord.type isEqualToString:@"image"])
            [cell.imageview.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        else
            [cell.imageview addSubview:cell.blackTransVw];
    }
    
    else if (aRecord.isFailed)
        cell.imageview.image = [UIImage imageNamed:@"placeholder"];
    else {
        cell.imageview.image = [UIImage imageNamed:@"placeholder"];
        
        if (!collectionView.dragging && !collectionView.decelerating) {
            [self startImageDownloadingForRecord:aRecord atIndexPath:indexPath];
        }
    }
    
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100, 100);
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5, 5,5, 5);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 1.0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 5.0;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (canSelect)
    {
        
        SharedCollectionViewCell *selectedCell  = (SharedCollectionViewCell * )[collectionView cellForItemAtIndexPath:indexPath];

        FileRecord *record;
        if (segmentedControl.selectedSegmentIndex == 0 && self.photos.count)
        {
            record = [self.photos objectAtIndex:indexPath.row];
            
        }
        else
            
        {
            record = [self.videos objectAtIndex:indexPath.row];

        }

        if ([selectedAry containsObject:record.msgId] )
        {
            [selectedAry removeObject:record.msgId];
            selectedCell.checkBoxImgVw.image = [UIImage imageNamed:@"check_U"];
        }
        else
        {
            [selectedAry addObject:record.msgId];
            selectedCell.checkBoxImgVw.image = [UIImage imageNamed:@"check_S"];
        }

    }
    else
    {
    
    
    if (segmentedControl.selectedSegmentIndex == 0 && self.photos.count)
    {
        FileRecord *record = [self.photos objectAtIndex:indexPath.row];
        
        [self showDetailView:self.photos Indexpath:indexPath image:record.image];
    }
    else if (segmentedControl.selectedSegmentIndex == 1 && self.videos.count)
    {
        FileRecord *record = [self.videos objectAtIndex:indexPath.row];

        [self showDetailView:self.videos Indexpath:indexPath image:record.image];
        
    }
    }
    
    if (selectedAry.count > 0)
    {
        UIImage *menuImageIcon = [UIImage imageNamed:@"appleTrash"];
        [menuButton setImage:menuImageIcon forState:UIControlStateNormal];
    }
    else
    {
         UIImage *menuImageIcon = [UIImage imageNamed:@"menu-icon"];
        [menuButton setImage:menuImageIcon forState:UIControlStateNormal];
    }

    
}

-(void)showDetailView:(NSMutableArray *)ary Indexpath:(NSIndexPath *)index image:(UIImage *)img
{
    DetailGalleryViewController *detailsView = [DetailGalleryViewController new];
    detailsView.detailArray = [ary mutableCopy];
    detailsView.indexpath = index;
    [self.navigationController pushViewController:detailsView animated:NO];
}
#pragma mark - Get Data from Database
-(void)getData:(XMPPJID *)roomjid
{
    [self.photos removeAllObjects];
    [self.videos removeAllObjects];
    [[FetchsharedFile sharedMediaServer] fetchMediaFiles:nil From:@"secureGallery" block:^(NSArray *items, NSError *error) {
        if (!error && items.count)
        {
            for (NSDictionary *dic in items)
            {
                FileRecord *record = [[FileRecord alloc] init];
                record.URL = [NSURL fileURLWithPath:[self getFullDocumentUrl:[dic objectForKey:@"url"]]];
                record.type = [dic objectForKey:@"video_img"];
                record.msgId = [dic objectForKey:@"msgId"];
                record.isPrivate = YES;
                if ([record.type isEqualToString:@"image"])
                    [self.photos addObject:record];
                else
                    [self.videos addObject:record];
                record = nil;
            }
            [_collectionView reloadData];
        }
        
    }];
}

- (NSString *)getFullDocumentUrl:(NSString *)fileName
{
    
    NSRange range = [fileName rangeOfString:@"Documents"];
    
    NSString *newString = [fileName substringFromIndex:range.location+9];
    //    NSLog(@"new string %@",newString);
    
    return [NSString stringWithFormat:@"%@/%@",[self applicationDocumentsDirectory],newString];
}

- (NSString *)applicationDocumentsDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}



- (void)startImageDownloadingForRecord:(FileRecord *)record atIndexPath:(NSIndexPath *)indexPath {
    if (![self.pendingOperations.downloadsInProgress.allKeys containsObject:indexPath]) {
        FileDownloader *imageDownloader = [[FileDownloader alloc] initWithFileRecord:record atIndexPath:indexPath delegate:self];
        [self.pendingOperations.downloadsInProgress setObject:imageDownloader forKey:indexPath];
        [self.pendingOperations.downloadQueue addOperation:imageDownloader];
    }
}
#pragma mark - ImageDownloader delegate
- (void)DownloaderDidFinish:(FileDownloader *)downloader {
    NSIndexPath *indexPath = downloader.indexPathInTableView;
    FileRecord *theRecord = downloader.fileRecord;
    if ([theRecord.type isEqualToString:@"image"])
        [self.photos replaceObjectAtIndex:indexPath.row withObject:theRecord];
    else
        [self.videos replaceObjectAtIndex:indexPath.row withObject:theRecord];
    
    [_collectionView reloadItemsAtIndexPaths:@[indexPath]];
    [self.pendingOperations.downloadsInProgress removeObjectForKey:indexPath];
}

#pragma mark - UIScrollView delegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    // 1: As soon as the user starts scrolling, you will want to suspend all operations and take a look at what the user wants to see.
    [self suspendAllOperations];
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    // 2: If the value of decelerate is NO, that means the user stopped dragging the table view. Therefore you want to resume suspended operations, cancel operations for offscreen cells, and start operations for onscreen cells.
    if (!decelerate) {
        [self loadImagesForOnscreenCells];
        [self resumeAllOperations];
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    // 3: This delegate method tells you that table view stopped scrolling, so you will do the same as in #2.
    [self loadImagesForOnscreenCells];
    [self resumeAllOperations];
}

#pragma mark - Cancelling, suspending, resuming queues / operations
- (void)suspendAllOperations {
    [self.pendingOperations.downloadQueue setSuspended:YES];
}
- (void)resumeAllOperations {
    [self.pendingOperations.downloadQueue setSuspended:NO];
}
- (void)cancelAllOperations {
    [self.pendingOperations.downloadQueue cancelAllOperations];
}
- (void)loadImagesForOnscreenCells {
    
    // 1: Get a set of visible rows.
    NSSet *visibleRows = [NSSet setWithArray:[_collectionView indexPathsForVisibleItems]];
    
    // 2: Get a set of all pending operations (download and filtration).
    NSMutableSet *pendingOperations = [NSMutableSet setWithArray:[self.pendingOperations.downloadsInProgress allKeys]];
    
    NSMutableSet *toBeCancelled = [pendingOperations mutableCopy];
    NSMutableSet *toBeStarted = [visibleRows mutableCopy];
    
    // 3: Rows (or indexPaths) that need an operation = visible rows ñ pendings.
    [toBeStarted minusSet:pendingOperations];
    
    // 4: Rows (or indexPaths) that their operations should be cancelled = pendings ñ visible rows.
    [toBeCancelled minusSet:visibleRows];
    
    // 5: Loop through those to be cancelled, cancel them, and remove their reference from PendingOperations.
    for (NSIndexPath *anIndexPath in toBeCancelled) {
        
        FileDownloader *pendingDownload = [self.pendingOperations.downloadsInProgress objectForKey:anIndexPath];
        [pendingDownload cancel];
        [self.pendingOperations.downloadsInProgress removeObjectForKey:anIndexPath];
    }
    toBeCancelled = nil;
    
    // 6: Loop through those to be started, and call startOperationsForFileRecord:atIndexPath: for each.
    for (NSIndexPath *anIndexPath in toBeStarted) {
        
        FileRecord *recordToProcess;
        if (segmentedControl.selectedSegmentIndex == 0)
            recordToProcess = [self.photos objectAtIndex:anIndexPath.row];
        else
            recordToProcess = [self.videos objectAtIndex:anIndexPath.row];
        if (!recordToProcess.image) {
            [self startImageDownloadingForRecord:recordToProcess atIndexPath:anIndexPath];
            
        }
        
    }
    toBeStarted = nil;
    
}
#pragma mark  -PendingOperations
- (PendingOperations *)pendingOperations {
    if (!_pendingOperations) {
        _pendingOperations = [[PendingOperations alloc] init];
    }
    return _pendingOperations;
}

- (void)didReceiveMemoryWarning
{
    [self cancelAllOperations];
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
  //  NSLog(@"MEMORY Warning at secureGalleryViewController");
    
}
@end
