//
//  GroupIconService.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 22/07/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "GroupIconService.h"
#import "AFHTTPClient.h"
#import "AFJSONRequestOperation.h"
#import "Header.h"
//#define BASE_URL [NSURL URLWithString:@"http://192.168.1.38/sosruthere/client4.php"]


@implementation GroupIconService

#pragma mark - Class Shared Instance
+ (GroupIconService*)sharedInstance
{
    static GroupIconService *_sharedInstance = nil;
    static dispatch_once_t dispatchOnce;
    dispatch_once(&dispatchOnce, ^{
        _sharedInstance = [[GroupIconService alloc] init];
    });
    return _sharedInstance;
}

#pragma mark - DownLoad and Save Group Icon
-(void)downloadIconAndSave:(XMPPJID *)roomJid
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSString *str = [NSString stringWithFormat:@"%@%@.png",ImageURLString,roomJid.bareJID.user];
        NSString *properlyEscapedURL = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
     //   properlyEscapedURL = [properlyEscapedURL stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:properlyEscapedURL]];
        if (data) {
            XMPPvCardCoreDataStorage *storage = [XMPPvCardCoreDataStorage sharedInstance];
            [storage setGroupIcon:data forJID:roomJid.bareJID xmppStream:[self xmppStream]];
        }
    });
}

#pragma mark - Appdelegate Values
-(AppDelegate *)appDelegate
{
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}
-(XMPPStream *)xmppStream{
    
    return [[self appDelegate] xmppStream];
}


#pragma mark - Upload Group Icon to server
-(void)uploadGroupImagetoServer:(XMPPJID *)roomjid Photo:(NSString *)filepath compeletion:(void (^) (BOOL))completed
{
    NSMutableDictionary *paramMutable = [[NSMutableDictionary alloc] init];
    [paramMutable setObject:roomjid.bareJID.user forKey:@"group_id"];
    
    NSMutableDictionary *rParam = [[NSMutableDictionary alloc] init];
    [rParam setObject:@"groupProfilePic" forKey:@"functionName"];
    [rParam setObject:paramMutable forKey:@"Parameter"];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:rParam options:NSJSONWritingPrettyPrinted error:&error];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:GROUP_ICON_SERVICE]];
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"" parameters:nil constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        
        NSURL *mUrl = [NSURL URLWithString:filepath];
        
        if (mUrl == NULL)
            mUrl = [NSURL fileURLWithPath:filepath];
        if ([[NSString stringWithFormat:@"%@",filepath] hasPrefix:@"/"])
            mUrl = [NSURL fileURLWithPath:filepath];
        
        if (mUrl != NULL)
        {
            NSArray *extentionAry = [[mUrl lastPathComponent] componentsSeparatedByString:@"."];
            NSString *mimeType;
            if ([[extentionAry lastObject] caseInsensitiveCompare:@"PNG"]==NSOrderedSame)
                mimeType = @"image/png";
            if (mimeType!=NULL)
            {
                [formData appendPartWithFileURL:mUrl name:@"Path" fileName:[mUrl lastPathComponent] mimeType:mimeType error:nil];
                [formData appendPartWithFormData:jsonData name:@"Request"];
            }
        }
        
    }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
     //   NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
    }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        responseStr = [responseStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        NSLog(@"Response Str : %@",responseStr);
        
        NSError *jsonError = nil;
        NSDictionary *objectsFromJson = [NSJSONSerialization JSONObjectWithData:[responseStr dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&jsonError];
        if (!jsonError) {
            if ([objectsFromJson[@"ReturnCode"] isEqualToString:@"1000"])
                completed(YES);
        }
        else
            completed(NO);

    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         completed(NO);

                                     }];
    
    [httpClient enqueueHTTPRequestOperation:operation];
}

#pragma mark - Save Image in file to upload to serve
-(void)saveImageToUploadInserver:(XMPPJID *)roomjid Photo:(UIImage *)img compeletion:(void (^)(BOOL, NSString *))completed
{
    if (img && roomjid) {
        UIImage* imag = [UIImage imageWithImage:img scaledToWidth:80];
        NSData *imageData = UIImagePNGRepresentation(imag);
        NSString *dataPath = [self createOrGetFilepath];
        NSString *filePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",roomjid.bareJID.user]]; //Add the file name
        if ([imageData writeToFile:filePath atomically:YES])
            completed(YES,filePath);
    }
}

#pragma mark - Remove the Group icon from local file Path
-(void)removeIconFromFilePath:(NSString *)filepath
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:filepath])
        [[NSFileManager defaultManager] removeItemAtPath:filepath error:nil];
}
#pragma mark - Create new file or get the file paths
-(NSString *)createOrGetFilepath
{
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/GroupIcon"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    return dataPath;
}

@end
