//
//  chatViewController.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 22/02/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "GroupChatViewController.h"
#import "AppDelegate.h"
#import "AvatarImage.h"
#import "UpdateDataBase.h"
#import "RUTerViewController.h"
#import "LocationManagerSingleton.h"
#import "DBEditer.h"
#import "ChatSildeMenuView.h"
#import "LocationManagerSingleton.h"
#import "NSHeaderData.h"
#import "UIBubbleHeaderTableViewCell.h"
#import <CoreText/CoreText.h>
#import "UIBubbleTypingTableViewCell.h"
#import "UIBubbleTableViewCell.h"
#import "XMPPRoomCoreDataStorage.h"
#import "mediaDbEditor.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "RMDownloadIndicator.h"
#import "AFHTTPClient.h"
#import "AFJSONRequestOperation.h"
#import "QBPopupMenu.h"
#import "PasswordPin.h"
#import "M13ContextMenu.h"
#import "M13ContextMenuItemIOS7.h"
#import "BeeCollectionView.h"
#import "GroupInformationViewController.h"
#import "imagePoper.h"
#import "bubbleVw.h"
#import "menuView.h"
#import "FetchsharedFile.h"
#import "DetailGalleryViewController.h"
#import "FileRecord.h"
#import "SecureGalleryViewController.h"
#import "ImpMsgPopUp.h"


#define domainName @"@shaili"
#define widthOfBlackTransVw 60

static NSString *const xmlns_chatstates = @"http://jabber.org/protocol/chatstates";

@interface GroupChatViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,Delegate_RUTer,chatSlideViewDelegate,Delegate_RUTer,UITableViewDelegate,UITableViewDataSource,NSFetchedResultsControllerDelegate,M13ContextMenuDelegate,BeeCollectionViewdelegate,bubbleViewDelegate>
{
    UIView *inputView;
    UIButton *sendBtn,*impBtn;
    AvatarImage *avatarImg;
    UpdateDataBase *updateUnreadMsg;
    __strong id <XMPPRoomStorage> xmppRoomStorage;
    XMPPJID *chatRoomJID;
    UIButton *menuButton,*backButton,*simleyBtn;
    BOOL isMenuShowing;
    UISwipeGestureRecognizer *swipeLeftGesture;
    UISwipeGestureRecognizer *swipeRightGesture;
    UISwipeGestureRecognizer *slideswipegesture;
    DBEditer* dbEditer_Obj;
    UITableView *chatTableView;
    NSFetchedResultsController *fetchedResultsController;
    ImpMsgPopUp *popUp;


    BOOL canExecuteViewWillAppear;
    BOOL isCamOpened;
    BOOL isGalleryOpen;
    BOOL isvideoPlayed;
    BOOL isPrivateChat;
    BOOL isFetching;

    NSString* EditMessageId;
    BOOL shouldScrollToBottom;

    BOOL isPrivateHistoryVw;
    BOOL isEditMode;
    BOOL showImpMsgLy;

    PasswordPin* passwordVw;

    M13ContextMenu *menu;
    UILongPressGestureRecognizer *longPress_M13ContextMenu;
    UIView* privateVw;
    UIView* editInputView;
    NSMutableArray* deleteMsgIdAry;
    UIButton* deleteDoneBtn,*cameraBtn,*galleryBtn;
    
    BOOL ifShowingStickers,isImpOn;
    BeeCollectionView* beeCollVw;
    NSArray* beeArray ;
    NSArray* beeFullArray;
    UIView* Bee_Board;
    UIView* BeeUp_Board;
    bubbleVw * bubble;
    menuView* menuVw;
    
    UIImageView *profileImgNav;

    
    UIViewController *imagePickerController;
    UIButton *imagePickerBackButton;



}
@property (nonatomic, strong)  XMPPRoom   * xmppRoom;
@property (nonatomic, strong)  UILabel   * textLabel;
@property(nonatomic, strong) NSMutableArray *photos;

@property(nonatomic,retain)NSMutableArray *messageData;
@property(nonatomic,retain)HPGrowingTextView *chatTextview;
@property(nonatomic,strong)UIButton *titleLabel;
@property(nonatomic,strong)ChatSildeMenuView *chatSlideMenu;

@property (nonatomic, strong) QBPopupMenu *popupMenu_txt;
@property (nonatomic, strong) QBPopupMenu *popupMenu_Tag;
@property (nonatomic, strong) QBPopupMenu *popupMenu_media;
@property (nonatomic, strong) QBPopupMenu *popupMenu_sticker;
@property(nonatomic,strong)GroupInformationViewController *groupInformation;
@property (nonatomic, strong) NSCache *imageCache;


@end

@implementation GroupChatViewController
@synthesize user,xmppRoom;

//-(instancetype)initWithRoomJid:(XMPPJID *)roomJID
//{
//    self = [super init];
//    if (self) {
//        xmppRoomStorage = [XMPPRoomCoreDataStorage sharedInstance];
//        xmppRoom = [[XMPPRoom alloc] initWithRoomStorage:xmppRoomStorage
//                                                     jid:roomJID
//                                           dispatchQueue:dispatch_get_main_queue()];
//        [xmppRoom activate:[self xmppStream]];
//        [xmppRoom addDelegate:self delegateQueue:dispatch_get_main_queue()];
//        if (![xmppRoom isJoined]) {
//            NSXMLElement *history = [NSXMLElement elementWithName:@"history"];
//            [history addAttributeWithName:@"maxstanzas" intValue:0];
//            [xmppRoom joinRoomUsingNickname:[[self xmppStream] myJID].user history:history];
//        }
//        
//       
//
//        self.user = roomJID;
//    }
//    return self;
//}
-(instancetype)init
{
    self = [super init];
    if (self)
    {
        
        
    }
    return self;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Appdelegate Values
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (AppDelegate *)appDelegate
{
	return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (XMPPStream *)xmppStream
{
	return [[self appDelegate] xmppStream];
}
-(XMPPMessageArchivingCoreDataStorage *)xmppMessageArchivingCoreDataStorage
{
    return [[self appDelegate] xmppMessageArchivingStorage];
}
- (NSManagedObjectContext *)managedObjecMessageContact
{
	return [[self xmppMessageArchivingCoreDataStorage] mainThreadManagedObjectContext];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark View Life Cycle
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:@"http://profilo-qa.augustasoftsol.com/api/filter/get/4"] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//        NSLog(@"%@", json);
//    }];
//    
//    [dataTask resume];
//    
    
    self.view.userInteractionEnabled = YES;
   // self.view.backgroundColor = CHAT_BACKGROUNDIMAGE;
    
    
    if(IS_IPHONE_4)
    {
        self.view.backgroundColor = BACKGROUNDIMAGE_4;
        
    }
    else
    {
        self.view.backgroundColor = BACKGROUNDIMAGE;
        
    }
    
    
    
    
    self.navigationItem.hidesBackButton = YES;
    
    chatTableView = [UITableView new];
    self.messageData = [NSMutableArray new];
    inputView = [UIView new];
    self.chatTextview = [HPGrowingTextView new];
    avatarImg = [AvatarImage new];
    updateUnreadMsg = [UpdateDataBase new];
    LocationManagerSingleton* locMang = [LocationManagerSingleton sharedSingleton];
    locMang.delegate_ruter = self;
     dbEditer_Obj = [DBEditer new];
    self.titleLabel = [UIButton buttonWithType:UIButtonTypeCustom];
    self.chatSlideMenu  = [ChatSildeMenuView new];
    inputView = [UIView new];
    self.chatTextview = [HPGrowingTextView new];
    self.photos = [NSMutableArray new];
    self.imageCache = [[NSCache alloc] init];
    profileImgNav = [UIImageView new];
    
    
    
    
    UIImage *impImageIcon1 = [UIImage imageNamed:@"impMsgTv1"];
    UIImage *impImageIcon2 = [UIImage imageNamed:@"impMsgTV2"];
    
    
    
    
    impBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    impBtn.backgroundColor = [UIColor clearColor];
    [impBtn setFrame:CGRectMake(0.0f, 0.0f, impImageIcon1.size.width, impImageIcon1.size.height)];
    [impBtn setImage:impImageIcon1 forState:UIControlStateNormal];
    [impBtn setImage:impImageIcon2 forState:UIControlStateHighlighted];
    //    impBtn.imageEdgeInsets = UIEdgeInsetsMake( 5.0, 5.0, 5.0, 5.0);
    [impBtn addTarget:self action:@selector(impButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    

    
    
    galleryBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    galleryBtn.backgroundColor = [UIColor clearColor];
    [galleryBtn setFrame:CGRectMake(0.0f, 0.0f, 40, 40)];
    [galleryBtn setImage:[UIImage imageNamed:@"galleryO"] forState:UIControlStateNormal];
    [galleryBtn addTarget:self action:@selector(galleryButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    cameraBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cameraBtn.backgroundColor = [UIColor clearColor];
    [cameraBtn setFrame:CGRectMake(0.0f, 0.0f, 40, 40)];
    [cameraBtn setImage:[UIImage imageNamed:@"cameraTool"] forState:UIControlStateNormal];
//    cameraBtn.imageEdgeInsets = UIEdgeInsetsMake( 5.0, 5.0, 5.0, 5.0);
    [cameraBtn addTarget:self action:@selector(cameraButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    

    
    
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlackOpaque;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithCustomView:impBtn],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithCustomView:cameraBtn],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithCustomView:galleryBtn],
                           
                           nil];
    [numberToolbar sizeToFit];
    _chatTextview.accessoryView = numberToolbar;
    

    
    


    // Keyboard events
    
    swipeLeftGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGestureAction:)];
    slideswipegesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGestureAction:)];
    swipeRightGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGestureAction:)];
    
    UIImage *backButtonImage = [UIImage imageNamed:@"back_btn"];
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 5, backButtonImage.size.width, backButtonImage.size.height);
    [backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    self.titleLabel.frame = CGRectMake(backButton.frame.origin.x + backButton.frame.size.width + 44, 10, self.view.frame.size.width - (backButton.frame.origin.x + backButton.frame.size.width + 100) , 30);
    self.titleLabel.backgroundColor = [UIColor clearColor];
    [self.titleLabel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.titleLabel addTarget:self action:@selector(pushtoGroupInformation) forControlEvents:UIControlEventTouchUpInside];
    self.titleLabel.titleLabel.font = [UIFont fontWithName:FontString size:18];
    
    
    UIImage *menuImageIcon = [UIImage imageNamed:@"menu-icon"];
    
    profileImgNav = [UIImageView new];
    profileImgNav.frame = CGRectMake(backButton.frame.origin.x + backButton.frame.size.width , self.navigationController.navigationBar.frame.size.height - 22, 44 , 44);
    profileImgNav.backgroundColor = [UIColor redColor];
    [profileImgNav.layer setCornerRadius:22];
    [profileImgNav setClipsToBounds:YES];
    profileImgNav.layer.borderWidth = 2.0;
    profileImgNav.layer.borderColor = [UIColor whiteColor].CGColor;

    
    
    
    
    deleteDoneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    deleteDoneBtn.frame = CGRectMake(self.view.frame.size.width-90, 10, 80, 30);
    [deleteDoneBtn setTitle:@"Done" forState:UIControlStateNormal];
    [deleteDoneBtn setTintColor:[UIColor whiteColor]];
    [deleteDoneBtn addTarget:self action:@selector(changeViewToNormalMode) forControlEvents:UIControlEventTouchUpInside];
    deleteDoneBtn.backgroundColor = RGBA(46, 141, 214, 1);
    deleteDoneBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    deleteDoneBtn.layer.cornerRadius = 15/2;
    deleteDoneBtn.hidden = YES;
    


    menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    menuButton.backgroundColor = [UIColor clearColor];
    menuButton.frame = CGRectMake(self.view.frame.size.width-60, 0, 60, 40);
    [menuButton setImage:menuImageIcon forState:UIControlStateNormal];
    menuButton.imageEdgeInsets = UIEdgeInsetsMake( 10.0, 5.0, 0.0, 0.0);
    [menuButton addTarget:self action:@selector(menuButtonAction:) forControlEvents:UIControlEventTouchDown];
    
    
    chatTableView = [UITableView new];
    chatTableView.delegate = self;
    chatTableView.dataSource = self;
    chatTableView.backgroundColor = [UIColor clearColor];
    chatTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    chatTableView.showsVerticalScrollIndicator = NO;
    chatTableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:chatTableView];
   
    chatTableView.contentInset = UIEdgeInsetsMake(- 62 , 0, 62, 0);

    
    self.chatSlideMenu = [[ChatSildeMenuView alloc]initWithFrame:CGRectMake(self.view.frame.size.width+100,self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height, 100, self.view.frame.size.height-40-(self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height))];
    self.chatSlideMenu.delegte = self;
    self.chatSlideMenu.menuForGroup = YES;
    [_chatSlideMenu addMenuView];
    
    inputView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:inputView];
    [self.view addSubview:self.chatSlideMenu];

    UIImage *smileyImage = [UIImage imageNamed:@"smiley"];
    simleyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    simleyBtn.backgroundColor = [UIColor clearColor];
    [simleyBtn setImage:smileyImage forState:UIControlStateNormal];
    // simleyBtn.imageEdgeInsets = UIEdgeInsetsMake( 7.0, 5.0, 0.0, 0.0);
    [simleyBtn addTarget:self action:@selector(beeBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    
    self.chatTextview.minNumberOfLines = 1;
    self.chatTextview.maxNumberOfLines = 3;
    self.chatTextview.returnKeyType = UIKeyboardTypeDefault; //just as an example
    self.chatTextview.delegate = self;
    self.chatTextview.backgroundColor = [UIColor whiteColor];
    self.chatTextview.placeholder = @"Buzz here..";
    self.chatTextview.animateHeightChange = YES;
    self.chatTextview.layer.cornerRadius = 0.5f ;
    self.chatTextview.clipsToBounds = YES;
    
    sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sendBtn.backgroundColor = [UIColor clearColor];
    [sendBtn addTarget:self action:@selector(SendButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [sendBtn setImage:[UIImage imageNamed:@"send_button"] forState:UIControlStateNormal];
  //  [sendBtn  setTitleColor:RGBA(53, 152, 219, 1) forState:UIControlStateNormal];
   // sendBtn.titleLabel.font = [UIFont fontWithName:FontString size:15];//[UIFont systemFontOfSize:14];
   // sendBtn.layer.cornerRadius = 30/2;
   // sendBtn.layer.borderColor =  RGBA(53, 152, 219, 1).CGColor;
   // sendBtn.layer.borderWidth = 1.2f;

  
    canExecuteViewWillAppear = YES;
    isCamOpened = NO;
    isGalleryOpen = NO;
    isvideoPlayed = NO;

    
    isPrivateHistoryVw = NO;
    isEditMode = NO;
    
    
    beeArray = [NSArray arrayWithObjects:@"sticker_001",@"sticker_002",@"sticker_003",@"sticker_004",@"sticker_005",nil];
    beeFullArray = [NSArray arrayWithObjects:@"sticker_01",@"sticker_02",@"sticker_03",@"sticker_04",@"sticker_05",nil];

//    beeArray = [NSArray arrayWithObjects:@"pow!",@"heart",@"awesome",@"grrrr!",@"yay!",nil];
//    beeFullArray = [NSArray arrayWithObjects:@"Pow!",@"Heart",@"Awesome",@"Grrrr!",@"Yay!",nil];
    
    Bee_Board = [UIView new];
    BeeUp_Board= [UIView new];

    UITapGestureRecognizer *swipeGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedCell:)];
    [chatTableView addGestureRecognizer:swipeGesture];
    
    chatTableView.frame = CGRectMake(self.view.frame.origin.x,0, self.view.frame.size.width, self.view.frame.size.height-40);
    inputView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.size.height - 40, self.view.frame.size.width, 40);
    

    simleyBtn.frame =  CGRectMake(0, (inputView.frame.size.height - smileyImage.size.height),smileyImage.size.width,smileyImage.size.height);
    self.chatTextview.frame = CGRectMake(36, 1, 217, 27);
    self.chatTextview.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
    self.chatTextview.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    sendBtn.frame = CGRectMake(self.chatTextview.frame.origin.x+self.chatTextview.frame.size.width + 4 ,inputView.frame.size.height - 35 , 57, 30);
    
    
    
    simleyBtn.frame =  CGRectMake(2, (inputView.frame.size.height/2) - (smileyImage.size.height/2),smileyImage.size.width,smileyImage.size.height);
    self.chatTextview.frame = CGRectMake(simleyBtn.frame.origin.x + simleyBtn.frame.size.width + 4, 1, self.view.frame.size.width + 15 - (simleyBtn.frame.size.width + 60 + 10), inputView.frame.size.height - 5);
    sendBtn.frame = CGRectMake(self.chatTextview.frame.origin.x + self.chatTextview.frame.size.width -2 ,inputView.frame.size.height - 35 , 57, 30);
    UIImage *rawEntryBackground = [UIImage imageNamed:@"chatborder"];
    UIImage *entryBackground = [rawEntryBackground stretchableImageWithLeftCapWidth:80 topCapHeight:98-20];
    UIImageView *entryImageView = [[UIImageView alloc] initWithImage:entryBackground];
    entryImageView.frame = CGRectMake(0, -98+inputView.frame.size.height, inputView.frame.size.width, 98);
    entryImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    
    [inputView addSubview:entryImageView];
    [inputView addSubview:self.chatTextview];
    [inputView addSubview:sendBtn];
	[inputView addSubview:simleyBtn];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [[self appDelegate].statusBarView setHidden:NO];

    NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@_BG",user.bare]];
    if (imageData)
    {
        UIImage* imageScaled = [UIImage imageWithImage:[UIImage imageWithData:imageData] scaledToSize:self.view.frame.size];
        
        self.view.backgroundColor = [UIColor colorWithPatternImage:imageScaled];
    }
    else
    {
      //  self.view.backgroundColor = CHAT_BACKGROUNDIMAGE;
        
        if(IS_IPHONE_4)
        {
            self.view.backgroundColor = BACKGROUNDIMAGE_4;
            
        }
        else
        {
            self.view.backgroundColor = BACKGROUNDIMAGE;
            
        }
        
        
    }
    
    self.singleton = [Singleton sharedMySingleton];

    self.singleton.chatScreenUser = self.user.bare;
    
    [self createPopUpForTag:[self getMemberlistForTag]];
    [self createPopUpforText];
    [self createPopUpforMedia];
    [self createPopUpforSticker];

    deleteMsgIdAry = [NSMutableArray new];
    
    
    
    chatTableView.frame = CGRectMake(self.view.frame.origin.x,self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height, self.view.frame.size.width, self.view.frame.size.height- (40 + self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height));
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(privatePassword:) name:@"privatePassword" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];

    
    if (isCamOpened || isvideoPlayed)
    {
        
        isCamOpened = NO;
        isvideoPlayed = NO;
        
    }
    
    else if (isGalleryOpen)
    {
        
        isGalleryOpen = NO;
        
    }
    
    else if ([UIApplication sharedApplication].statusBarHidden ==  YES) {
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
        self.view.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height-20);
    }

    [self.navigationController.navigationBar addSubview:deleteDoneBtn];
    [self.navigationController.navigationBar addSubview:profileImgNav];
    [self.navigationController.navigationBar addSubview:backButton];
    [self.navigationController.navigationBar addSubview:self.titleLabel];
    
    [profileImgNav setImage: [avatarImg groupProfileimage:self.user]];

    
    isMenuShowing = NO;
    isFetching = NO;

    [self.titleLabel setTitle:[self getRoomSubjectName:self.user] forState:UIControlStateNormal];
    [[self xmppStream] addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    [chatTableView reloadData];
    
    [self createBeeBoard:beeArray];

    
    //** long press menu
    
   // [self createM13ContectMenu];
    
    [self editInputVw_Create];

    int textSizeValue;
    NSInteger textsize = [[NSUserDefaults standardUserDefaults] integerForKey:@"textSize"];
    if (textsize == 0)
        textSizeValue = 15;
    else if (textsize == 1)
        textSizeValue = 17;
    else
        textSizeValue = 19;
	self.chatTextview.font = [UIFont fontWithName:FontString size:textSizeValue];
    self.chatTextview.frame = CGRectMake(self.chatTextview.frame.origin.x,(inputView.frame.size.height - self.chatTextview.frame.size.height-(3 - textsize)), self.chatTextview.frame.size.width, self.chatTextview.frame.size.height);
    if ([[NSUserDefaults standardUserDefaults] boolForKey:TURN_BUBBLE_MENU] == YES)
    {
        [self createBubbleMenu];
        
        menuVw = [menuView sharedInstance:[NSArray arrayWithObjects:@"privatechat_w.png",@"gallery_w.png",@"photo_w.png",@"video_w.png",@"ruthere_w.png",nil] btnTitleAry:[NSArray arrayWithObjects:@"Clear chat",@"Send File",@"Photo",@"Video",@"Are you there?",nil] screeenSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)];
        [self.view addSubview:menuVw];
        menuVw.hidden = YES;
        menuVw.delegte = self;
        [chatTableView removeGestureRecognizer:swipeRightGesture];
        [chatTableView removeGestureRecognizer:swipeLeftGesture];
        [self.chatSlideMenu removeGestureRecognizer:slideswipegesture];
        
    }
    else
    {
        [self.navigationController.navigationBar addSubview:menuButton];
        [swipeRightGesture setDirection:UISwipeGestureRecognizerDirectionRight];
        [chatTableView addGestureRecognizer:swipeRightGesture];
        [swipeLeftGesture setDirection:UISwipeGestureRecognizerDirectionLeft];
        [chatTableView addGestureRecognizer:swipeLeftGesture];
        [self.chatSlideMenu addGestureRecognizer:slideswipegesture];
    }
    
    
    int unreadMsgCount = [[[NSUserDefaults standardUserDefaults]valueForKey:[NSString stringWithFormat:@"%@%@",KEY_UNREAD,user.bare]]intValue];
    
    if (unreadMsgCount == 0)
    {
        shouldScrollToBottom = YES;
        [self scrollBubbleViewToBottomAnimated:YES];
    }
    else
    {
        [self scrollToFirstUnreadMsgPosition:YES];
    }
    
    [[self xmppMessageArchivingCoreDataStorage]updateUnreadMessage:user clearValue:YES];
    
    isImpOn = NO;
   showImpMsgLy = NO;



}

-(void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

}

-(void)viewWillDisappear:(BOOL)animated
{
    self.singleton.chatScreenUser = nil;

    if (menuVw) {
        [menuVw removeFromSuperview];
        menuVw  = nil;
    }
    
  //  [bubble restoreBtn];
    if (bubble) {
        [bubble removeFromSuperview];
        bubble  = nil;
    }
    
    [profileImgNav removeFromSuperview];

    if (!isGalleryOpen && !isCamOpened && !isvideoPlayed)
    {
        fetchedResultsController = nil;
    }
    
    if (ifShowingStickers) {
        ifShowingStickers = !ifShowingStickers;
    }
    else
    {
        XMPPMessageArchivingCoreDataStorage *storage  = [XMPPMessageArchivingCoreDataStorage sharedInstance];
        [storage updateUnreadMessage:self.user clearValue:YES];
    }
    
    [[self xmppStream] removeDelegate:self];
    [backButton removeFromSuperview];
    [menuButton removeFromSuperview];
    
    [self resignkeyBoard];
    [self.titleLabel removeFromSuperview];
    [self.chatTextview resignFirstResponder];
    self.chatTextview.text = @"";
    [[self xmppStream] removeDelegate:self delegateQueue:dispatch_get_main_queue()];
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Action For Back Button
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)backbuttonAction:(id)sender
{
    if (isPrivateChat || privateVw) {
        [self removePrivateChatView];
    }
    else if (showImpMsgLy)
    {
        
        [UIView transitionWithView:popUp
                          duration:0.2
                           options:UIViewAnimationOptionTransitionNone
                        animations:^{
                            popUp.transform =  CGAffineTransformMakeScale(0.3, 0.3);
                            [menuButton setHidden:NO];
                            
                        } completion:^(BOOL finished)
         
         {
             [popUp removeFromSuperview];
             
             showImpMsgLy = NO;
             menu.shouldShow = YES;
             
         }];

    }
    else
    {
    isEditMode = NO;
    isPrivateHistoryVw = NO;
    canExecuteViewWillAppear = YES;
    [self.navigationController popViewControllerAnimated:YES];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIBubbleTableViewDataSource implementation
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger)rowsForBubbleTable:(UIBubbleTableView *)tableView
{
    return [self.messageData count];
}

- (NSBubbleData *)bubbleTableView:(UIBubbleTableView *)tableView dataForRow:(NSInteger)row
{
    return [self.messageData objectAtIndex:row];
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Keyboard events
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:[aNotification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue] animations:^{
        CGRect frame = inputView.frame;
        CGRect frame_For_Chat = chatTableView.frame;
        
        
        int V_height = (abs((int)self.view.frame.size.height));
        int I_height = (abs((int)inputView.frame.size.height));
        int I_OriginY = (abs((int)inputView.frame.origin.y));
        int actual_Y = V_height - I_height;
        
        
        //NSLog(@"Height KEY : %d",V_height);
        //NSLog(@"Height VIEW : %d",I_height);
        //NSLog(@"Origin Y : %d",I_OriginY);
        //NSLog(@"Actual Y : %d",actual_Y);
        
        
        frame_For_Chat = chatTableView.frame;
        
        if(actual_Y >= I_OriginY + 3 || actual_Y <= I_OriginY + 3 )
        {
            
            frame.origin.y =  (self.view.frame.size.height) - (kbSize.height + inputView.frame.size.height);
            frame_For_Chat.size.height =  frame.origin.y;
        }
        
        inputView.frame = frame;
        
        
        chatTableView.frame = frame_For_Chat;
        [self scrollBubbleViewToBottomAnimated:NO];
        
    }];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    //  NSDictionary* info = [aNotification userInfo];
    //CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:[aNotification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]
                     animations:^{
                         CGRect frame = inputView.frame;
                         
                         frame.origin.y = (self.view.frame.size.height - inputView.frame.size.height);
                         inputView.frame = frame;
                         frame = chatTableView.frame;
                         frame.size.height = (self.view.frame.size.height - inputView.frame.size.height);
                         chatTableView.frame = frame;
                         [self scrollBubbleViewToBottomAnimated:NO];
                     }];
}



- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
{
    [self.popupMenu_txt dismissAnimated:YES];
    [self.popupMenu_media dismissAnimated:YES];
    [self.popupMenu_sticker dismissAnimated:YES];

}
-(void)resignkeyBoard
{
    [self.popupMenu_txt dismissAnimated:YES];
    [self.popupMenu_media dismissAnimated:YES];
    [self.popupMenu_sticker dismissAnimated:YES];
    [self.popupMenu_Tag dismissAnimated:YES];

    [self.chatTextview resignFirstResponder];
    if (isMenuShowing)
        [self menuButtonAction:nil];
    
    [self closeBee_Board];
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Delegate Methods for Message TextView
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    float diff = (growingTextView.frame.size.height - height);
	CGRect r = inputView.frame;
    r.size.height -= diff;
    r.origin.y += diff;
	inputView.frame = r;
}
- (BOOL)growingTextViewShouldBeginEditing:(HPGrowingTextView *)growingTextView;
{
    if (isMenuShowing)
        [self menuButtonAction:nil];
    return YES;
}

-(BOOL)growingTextView:(HPGrowingTextView *)growingTextView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
   // [self colorHashtag];
    
    return YES;
}

-(void)colorHashtag
{
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc]initWithString:_chatTextview.text];
    
    NSError *error = nil;
    
    //I Use regex to detect the pattern I want to change color
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"@(\\w+)" options:0 error:&error];
    
    
    
    NSArray *matches = [regex matchesInString:_chatTextview.text options:0 range:NSMakeRange(0, _chatTextview.text.length)];
    
    for (NSTextCheckingResult *match in matches) {
        NSRange wordRange = [match rangeAtIndex:0];
        [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:wordRange];
    }
    
    _chatTextview.text = [string string];
    
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Action for sending Message
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)SendButtonAction:(id)sender
{
    NSString *OrgMessageStr = self.chatTextview.text;
    
    NSData *data = [OrgMessageStr dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    NSString *messageStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    if([messageStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length > 0) {
        
        NSString *messageID=[self.xmppStream generateUUID];
        
        NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
        [body setStringValue:messageStr];
        NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
        [message addAttributeWithName:@"id" stringValue:messageID];
        [message addAttributeWithName:@"type" stringValue:@"groupchat"];
        [message addAttributeWithName:@"to" stringValue:[NSString stringWithFormat:@"%@",user]];
        [message addChild:body];
		
        if (isPrivateChat) {
            NSXMLElement *private = [NSXMLElement elementWithName:@"private"];
            [private addAttributeWithName:@"xmlns" stringValue:@"private"];
            [private addAttributeWithName:@"type" stringValue:@"message"];
            [message addChild:private];
        }
        
        if (isImpOn)
        {
            NSXMLElement *imp = [NSXMLElement elementWithName:@"imp"];
            [imp addAttributeWithName:@"xmlns" stringValue:@"imp"];
            [imp addAttributeWithName:@"type" stringValue:@"message"];
            [message addChild:imp];
            
            [self performSelector:@selector(impButtonAction:) withObject:self];
            
        }
                

        [self.xmppStream sendElement:message];
        
        self.chatTextview.text = @"";
        
    }
}


-(void)impButtonAction:(id)sender
{
    UIImage *impImageIcon1 = [UIImage imageNamed:@"impMsgTv1"];
    UIImage *impImageIcon2 = [UIImage imageNamed:@"impMsgTV2"];
    
    [impBtn setImage:impImageIcon2 forState:UIControlStateNormal];
    
    isImpOn = !isImpOn;
    
    if (isImpOn)
    {
        [impBtn setImage:impImageIcon2 forState:UIControlStateNormal];
    }
    else
    {
        [impBtn setImage:impImageIcon1 forState:UIControlStateNormal];
        
    }
    
}




- (NSFetchedResultsController *)fetchedResultsController {
    
    if (fetchedResultsController)
    {
        return fetchedResultsController;
    }
    XMPPMessageArchivingCoreDataStorage *storage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:moc];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    if (isPrivateHistoryVw) {
        NSString *predicateFrmt = @"bareJidStr == %@ AND isPrivate == YES";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, self.user.bare];
        [fetchRequest setPredicate:predicate];
    }
    else if(isPrivateChat)
    {
        NSString *predicateFrmt = @"bareJidStr == %@ AND privateStatus == NO";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, self.user.bare];
        [fetchRequest setPredicate:predicate];
    }
    else
    {
        NSString *predicateFrmt = @"bareJidStr == %@ AND privateStatus == NO";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, self.user.bare];
        [fetchRequest setPredicate:predicate];
    }
    [fetchRequest setSortDescriptors:[NSSortDescriptor sortValues:@"timestamp" ascending:YES]];
    fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:moc sectionNameKeyPath:@"sectionDate" cacheName:nil];
    [fetchedResultsController setDelegate:self];
    NSError *error = nil;
    if (![fetchedResultsController performFetch:&error])
        NSLog(@"ERROR IN CHAT FETCHCONTROLL:::::%@",error);
    
	return fetchedResultsController;
}
- (XMPPMessageArchiving_Message_CoreDataObject*)MsgWithID:(NSString*)msgId
{
    NSManagedObjectContext *moc = [[XMPPMessageArchivingCoreDataStorage sharedInstance] mainThreadManagedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:moc];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    NSString *predicateFrmt = @"messageID == %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,msgId];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:[NSSortDescriptor sortValues:@"timestamp" ascending:YES]];
    NSArray *delArray=[moc executeFetchRequest:fetchRequest error:nil];
    
    for (XMPPMessageArchiving_Message_CoreDataObject *CoreDataObject in delArray)
    {
            return CoreDataObject;
    }
    return nil;
}

- (void)clearChatWithID:(NSString*)JidStr
{
    NSManagedObjectContext *moc = [[XMPPMessageArchivingCoreDataStorage sharedInstance] mainThreadManagedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:moc];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    NSString *predicateFrmt = @"bareJidStr == %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,JidStr];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:[NSSortDescriptor sortValues:@"timestamp" ascending:YES]];
    NSArray *delArray=[moc executeFetchRequest:fetchRequest error:nil];
    
    
    for (XMPPMessageArchiving_Message_CoreDataObject *CoreDataObject in delArray)
    {
        
        [moc deleteObject:CoreDataObject];
        
        NSXMLElement *file = [CoreDataObject.message elementForName:@"file"];
        
        if (file)
        {
            mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
            [mediaDb deleteMedia:CoreDataObject.messageID];
        }

        
        NSError *error = nil;
        
        if (![moc save:&error])
        {
          //  NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
        }
        
    }
    
    
    [[self xmppMessageArchivingCoreDataStorage]updateUnreadMessage:user clearValue:YES];
    
    

    
    double delayInSeconds = 0.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [[self xmppMessageArchivingCoreDataStorage]updateMessageOnRecentChat:self.user.bare];


    });
}



-(void)updateOnRecentContact:(NSString *)Jidstr withString:(NSString *)UpdateString;
{
    
    NSManagedObjectContext *moc = [[self xmppMessageArchivingCoreDataStorage] mainThreadManagedObjectContext];
    NSEntityDescription *contactEnityDes = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Contact_CoreDataObject" inManagedObjectContext:moc];
    NSFetchRequest *contactRequest = [[NSFetchRequest alloc]init];
    [contactRequest setEntity:contactEnityDes];
    NSString *predicateFrmt = @"bareJidStr == %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,Jidstr];
    [contactRequest setPredicate:predicate];
    NSArray *delArray=[moc executeFetchRequest:contactRequest error:nil];
    
    for (XMPPMessageArchiving_Contact_CoreDataObject *coreMessage in delArray)
    {
        [coreMessage setValue:UpdateString forKey:@"mostRecentMessageBody"];
        NSError *error = nil;
        if (![moc save:&error])
        {
          //  NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
        }
        
        
    }
    
}


#pragma mark - Table view data source methods
// The data source methods are handled primarily by the fetch results controller
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[[self fetchedResultsController] sections] count];
}
// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSArray *sections = [[self fetchedResultsController] sections];
    if (section < [sections count])
    {
        id <NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
        return sectionInfo.numberOfObjects;
    }
    
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return tableView.sectionHeaderHeight+27;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(10,0,self.view.frame.size.width - 20,tableView.sectionHeaderHeight)];
    
    UIView *lineView = [UIView new];
    lineView.frame = CGRectMake(0, 25, tableView.frame.size.width, 1);
    lineView.backgroundColor = RGBA(119, 186, 240, 1.0);
    
    // create the label objects
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.backgroundColor = RGBA(119, 186, 240, 0.7);
    headerLabel.font = [UIFont fontWithName:FontString size:12];
    headerLabel.frame = CGRectMake((tableView.frame.size.width/2)- 50,0,100,20);
    headerLabel.textColor = [UIColor whiteColor];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.layer.cornerRadius = 10;
    headerLabel.clipsToBounds = YES;
    
    
    if ([chatTableView.dataSource tableView:tableView numberOfRowsInSection:section] == 0) {
        return nil;
    }
    NSArray *sections = [[self fetchedResultsController] sections];
    if (section < [sections count])
    {
        id <NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
        headerLabel.text =  sectionInfo.name;
    }
    [customView addSubview:headerLabel];
    
    return customView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    id details = [self getBubbledata:indexPath];
    if ([details isKindOfClass:[NSBubbleData class]]) {
        NSBubbleData *data = (NSBubbleData *)details;
        CGFloat height = (CGFloat)MAX(data.insets.top + data.view.frame.size.height + data.insets.bottom+ 15,data.insets.top + data.view.frame.size.height + data.insets.bottom);
        details = nil; data = nil;
        return height;
    }
    else if ([details isKindOfClass:[NSHeaderData class]])
        return (CGFloat)[UIBubbleHeaderTableViewCell height];
        return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *Tablecell;
    static NSString *CellIdentifier;
    id details = [self getBubbledata:indexPath];
    if ([details isKindOfClass:[NSBubbleData class]])
    {
        NSBubbleData *data = (NSBubbleData *)details;
        CellIdentifier = @"thebubbletable";
        UIBubbleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) cell = [[UIBubbleTableViewCell alloc] init];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.data = data;
        
        if (isEditMode) {
            cell.showAvatar = NO;
            XMPPMessageArchiving_Message_CoreDataObject *coremessage = [[self fetchedResultsController ] objectAtIndexPath:indexPath];
            
            
            if ([deleteMsgIdAry containsObject:coremessage.messageID] )
            {
                cell.deleteCell = YES;
                cell.deleteCell_selected = YES;
            }
            else
            {
                cell.deleteCell = YES;
                cell.deleteCell_selected = NO;
            }
        }
        else
        {
            cell.showAvatar = YES;
            cell.deleteCell = NO;
            
        }

        Tablecell =  cell;
    }
    else if([details isKindOfClass:[NSHeaderData class]])
    {
        CellIdentifier = @"tblBubbleHeaderCell";
        UIBubbleHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        NSHeaderData *data = (NSHeaderData *)details;
        if (cell == nil) cell = [[UIBubbleHeaderTableViewCell alloc] init];
        cell.backgroundColor = [UIColor clearColor];
        cell.date = data.text;
        Tablecell = cell;
    }
       return Tablecell;
}


-(id)getBubbledata:(NSIndexPath *)indexPath {
    
    NSBubbleData *messageDetails;
    XMPPMessageArchiving_Message_CoreDataObject *coremessage = [[self fetchedResultsController ] objectAtIndexPath:indexPath];
    NSString *userStr = [self getDisplayName:[[coremessage.message from] resource]];
    if (userStr == nil) {
        userStr = [userStr stringByReplacingOccurrencesOfString:DOMAINAME withString:@""];
    }
    
    NSRange stringRange = {0, MIN([userStr length], 20)};
    
    // adjust the range to include dependent chars
    stringRange = [userStr rangeOfComposedCharacterSequencesForRange:stringRange];
    
    // Now you can create the short string
    userStr = [userStr substringWithRange:stringRange];
    
    
    if ([coremessage.message isGroupChatMessageWithSubject])
    {
        NSHeaderData * data = [NSHeaderData dataWithText:[NSString stringWithFormat:@"%@ changed the subject to \"%@\"", userStr,coremessage.body] date:coremessage.timestamp jidStr:coremessage.bareJidStr name:@"sdfds" roomJid:@"sdfsd"];
        return data;
    }
    else if ([coremessage.message elementForName:@"group"])
    {
        
        if ([coremessage.body isEqualToString:@"invited"])
        {
            NSXMLElement *groupJoinLeave = [coremessage.message elementForName:@"group"];
            NSString *invitedUser = [self getDisplayName: [[groupJoinLeave attributeForName:@"who"]stringValue]];
            
            if ([invitedUser isEqualToString:@"You"])
            {
                invitedUser = @"You are";
            }
            
            NSHeaderData * data = [NSHeaderData dataWithText:[NSString stringWithFormat:@"%@ invited by  %@", invitedUser ,userStr] date:coremessage.timestamp jidStr:coremessage.bareJidStr name:@"" roomJid:@""];
            
            return data;
        }
        else
        {
        NSHeaderData * data = [NSHeaderData dataWithText:coremessage.body date:coremessage.timestamp jidStr:coremessage.bareJidStr name:@"" roomJid:@""];
            return data;
        }
       
        
        
    }
    NSXMLElement *private = [coremessage.message elementForName:@"private"];
    NSXMLElement *imp = [coremessage.message elementForName:@"imp"];


    if ([[coremessage.message from].resource isEqualToString:[[self xmppStream] myJID].user]){
        NSXMLElement *ruder = [coremessage.message elementForName:@"ruthere"];
        NSXMLElement *file = [coremessage.message elementForName:@"file"];


        if (ruder) {
            
            
            if (![[[ruder attributeForName:@"type"] stringValue] isEqualToString:@"request"])
            {
                NSHeaderData * data = [NSHeaderData dataWithText:[NSString stringWithFormat:@"%@ by %@",coremessage.body,[self getDisplayName:[coremessage.message from].resource]] date:coremessage.timestamp jidStr:xmppRoom.roomJID.bare name:@"sdfds" roomJid:@"sdfsd"];
                return data;
            }
            
            else
            {
                NSString* statusStr = [self stringForStatus:[dbEditer_Obj GetStatus_RUTerId:[[ruder attributeForName: @"ruthere_id"]stringValue] Sender:@"YES"user_Id:[[self xmppStream] myJID].user]];
                
              //  [self sendVirtualMessageForRuthereReached:[[ruder attributeForName: @"ruthere_id"] stringValue] statusStr:statusStr];
                
                messageDetails = [NSBubbleData dataWithView:[self ruderView:@"BubbleTypeMine" status:statusStr] date:coremessage.timestamp type:BubbleTypeMine insets:UIEdgeInsetsMake(5.0, 5.0, 7.0, 15.0) jidString:user.bare RuderID:[NSString stringWithFormat:@"%@",[[ruder attributeForName: @"ruthere_id"]stringValue]] deliveryStr:nil chattype:@"group" isSender:YES messageID:coremessage.messageID];
            }
        }
        else if(file)
        {
            messageDetails = [self constructMediaViewBubledata:coremessage indexPath:indexPath];
            if(!isEditMode)
            {
            UILongPressGestureRecognizer*  longPressText = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressText:)];
            [messageDetails.view setUserInteractionEnabled:YES];
            [messageDetails.view addGestureRecognizer:longPressText];
            }
            
        }
        else if([beeFullArray containsObject:coremessage.body])
        {
            messageDetails = [NSBubbleData dataWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png",[coremessage.body stringByReplacingOccurrencesOfString:@"0" withString:@"00"]]] date:coremessage.timestamp type:BubbleTypeBeeMine jidString:coremessage.bareJidStr deliveryStr:nil chattype:@"group" messageID:nil];
            
            if(!isEditMode)
            {
            UILongPressGestureRecognizer*  longPressText = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressText:)];
            [messageDetails.view setUserInteractionEnabled:YES];
            [messageDetails.view addGestureRecognizer:longPressText];
            }
        }
        else if (imp)
            
        {
            NSData *newdata=[coremessage.body dataUsingEncoding:NSUTF8StringEncoding
                                           allowLossyConversion:YES];
            NSString *Bubblestring=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];
            
            
            
            if (private)
            {
                messageDetails = [NSBubbleData dataWithTextWithTag:Bubblestring date:coremessage.timestamp type:BubbleTypePrivateMine jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"singlechat" messageID:coremessage.messageID impMsg:YES];
            }
            else
            {
                messageDetails = [NSBubbleData dataWithTextWithTag:Bubblestring date:coremessage.timestamp type:BubbleTypeMine jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"singlechat" messageID:coremessage.messageID impMsg:YES];
                
                
            }
            if (!isEditMode) {
                UILongPressGestureRecognizer*  longPressText = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressText:)];
                longPressText.delegate = self;
                [messageDetails.view setUserInteractionEnabled:YES];
                [messageDetails.view addGestureRecognizer:longPressText];
                
                
            }
            
            
        }

    else
        {
            NSData *newdata=[coremessage.body dataUsingEncoding:NSUTF8StringEncoding
                                           allowLossyConversion:YES];
            NSString *Bubblestring=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];


            if (private) {
        messageDetails = [NSBubbleData dataWithText:Bubblestring date:coremessage.timestamp type:BubbleTypePrivateMine jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"group" messageID:coremessage.messageID];
            }
            else
            {
                 messageDetails = [NSBubbleData dataWithText:Bubblestring date:coremessage.timestamp type:BubbleTypeMine jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"group" messageID:coremessage.messageID];
            }
            if(!isEditMode)
            {
            UILongPressGestureRecognizer*  longPressText = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressText:)];
            [messageDetails.view setUserInteractionEnabled:YES];
            [messageDetails.view addGestureRecognizer:longPressText];
            }
        }
        [messageDetails setAvatar:[avatarImg myProfileImage]];

    }
    else
    {
        NSXMLElement *ruder = [coremessage.message elementForName:@"ruthere"];
        NSXMLElement *file = [coremessage.message elementForName:@"file"];

        if (ruder) {
            
            if (![[[ruder attributeForName:@"type"] stringValue] isEqualToString:@"request"])
            {
                NSHeaderData * data = [NSHeaderData dataWithText:[NSString stringWithFormat:@"%@ by %@",coremessage.body,[self getDisplayName:[coremessage.message from].resource]] date:coremessage.timestamp jidStr:xmppRoom.roomJID.bare name:@"sdfds" roomJid:@"sdfsd"];
                return data;
            }
            
            else
            {
                NSString* statusStr = [self stringForStatus:[dbEditer_Obj GetStatus_RUTerId:[[ruder attributeForName: @"ruthere_id"]stringValue] Sender:@"YES"user_Id:[coremessage.message from].resource]];
                
                //[self sendVirtualMessageForRuthereReached:[[ruder attributeForName: @"ruthere_id"] stringValue] statusStr:statusStr];

                
                messageDetails = [NSBubbleData dataWithView:[self ruderView:@"BubbleTypeSomeoneElse" status:statusStr] date:coremessage.timestamp type:BubbleTypeSomeoneElse insets:UIEdgeInsetsMake(5.0, 15.0, 7.0, 8.0) jidString:user.bare RuderID:[NSString stringWithFormat:@"%@",[[ruder attributeForName: @"ruthere_id"]stringValue]] deliveryStr:nil  chattype:@"group" isSender:NO messageID:coremessage.messageID];
            }
        }
        
        else if(file)
        {
            messageDetails = [self constructMediaViewBubledata:coremessage indexPath:indexPath];
            
            if(!isEditMode)
            {
            UILongPressGestureRecognizer*  longPressText = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressText:)];
            [messageDetails.view setUserInteractionEnabled:YES];
            [messageDetails.view addGestureRecognizer:longPressText];
            }
            
        }

        else if([beeFullArray containsObject:coremessage.body])
        {
            
            messageDetails = [NSBubbleData dataWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png",[coremessage.body stringByReplacingOccurrencesOfString:@"0" withString:@"00"]]] date:coremessage.timestamp type:BubbleTypeBeeSomeoneElse jidString:coremessage.bareJidStr deliveryStr:nil chattype:@"singlechat" messageID:nil userName:userStr];
            
            if(!isEditMode)
            {
            UILongPressGestureRecognizer*  longPressText = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressText:)];
            [messageDetails.view setUserInteractionEnabled:YES];
            [messageDetails.view addGestureRecognizer:longPressText];
            }
            
        }
        else if (imp)
        {
            NSData *newdata=[coremessage.body dataUsingEncoding:NSUTF8StringEncoding
                                           allowLossyConversion:YES];
            NSString *Bubblestring=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];
            
            
            if (private)
            {
                messageDetails = [NSBubbleData dataWithTextWithTag:[NSString stringWithFormat:@"%@\n%@",userStr,Bubblestring]  date:coremessage.timestamp type:BubbleTypePrivateSomeoneElse jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"group" messageID:coremessage.messageID impMsg:YES];
            }
            else
            {
                messageDetails = [NSBubbleData dataWithTextWithTag:[NSString stringWithFormat:@"%@\n%@",userStr,Bubblestring]  date:coremessage.timestamp type:BubbleTypeSomeoneElse jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"group" messageID:coremessage.messageID impMsg:YES];
            }
            
            UILongPressGestureRecognizer*  longPressText = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressText:)];
            longPressText.delegate = self;
            [messageDetails.view setUserInteractionEnabled:YES];
            [messageDetails.view addGestureRecognizer:longPressText];
            
        }
        
        
        else

        {
            NSData *newdata=[coremessage.body dataUsingEncoding:NSUTF8StringEncoding
                                           allowLossyConversion:YES];
            NSString *Bubblestring=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];

            if (private) {

            messageDetails = [NSBubbleData dataWithText:[NSString stringWithFormat:@"%@\n%@",userStr,Bubblestring]  date:coremessage.timestamp type:BubbleTypePrivateSomeoneElse jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"group" messageID:coremessage.messageID];
            }
            else
            {
                 messageDetails = [NSBubbleData dataWithText:[NSString stringWithFormat:@"%@\n%@",userStr,Bubblestring]  date:coremessage.timestamp type:BubbleTypeSomeoneElse jidString:coremessage.bareJidStr deliveryStr:coremessage.delivery chattype:@"group" messageID:coremessage.messageID];
            }
            
            if(!isEditMode)
            {
            UILongPressGestureRecognizer*  longPressText = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressText:)];
            [messageDetails.view setUserInteractionEnabled:YES];
            [messageDetails.view addGestureRecognizer:longPressText];
            }
        }
        NSString *resourceStr = [NSString stringWithFormat:@"%@",coremessage.message.from.resource];
        
        if ([resourceStr rangeOfString:DOMAINAME].location == NSNotFound)
            resourceStr = [resourceStr stringByAppendingString:DOMAINAME];
        [messageDetails setAvatar:[avatarImg groupProfileimage:[XMPPJID jidWithString:resourceStr]]];

    }
    return messageDetails;
    
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Did Receive Message
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message
{
    self.singleton = [Singleton sharedMySingleton];

    if (self.singleton.chatScreenUser) {

    if ([[[message from] bareJID] isEqualToJID:user])
    {
        if([message isGroupChatMessage] && [[message from] resource]!= nil)
        {
            if (![message isGroupChatMessageWithSubject]) {
                
                if ([[message from].resource isEqualToString:[[self xmppStream] myJID].user]) {
                    NSXMLElement *ruder = [message elementForName:@"ruthere"];
                    
                    if (ruder)
                    {
                        if (![[[ruder attributeForName:@"type"] stringValue] isEqualToString:@"request"])
                        {
                            [chatTableView reloadData];
                        }
                        
                    }
   
                }
                else
                {
                    NSXMLElement *ruder = [message elementForName:@"ruthere"];
                    if (ruder) {
                        if (![[[ruder attributeForName:@"type"] stringValue] isEqualToString:@"request"])
                        {
                            [chatTableView reloadData];
                            
                        }
                        
                    }
                    
                }
                
                
               
            }
            
        }
       
    }
        else
        {
            if([message isGroupChatMessage] && [[message from] resource]!= nil)
        {
            if (![message isGroupChatMessageWithSubject]) {

        if([beeFullArray containsObject:message.body])
        {
            imagePoper* popImage = [[imagePoper alloc]initFrame_Image:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png",message.body]] onView:self.view];
            
        }
            }
        }
        }
    }
}
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    
    [chatTableView beginUpdates];
}
- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    switch(type)
    {
        case NSFetchedResultsChangeInsert:
            [chatTableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
            
        case NSFetchedResultsChangeDelete:
            [chatTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [chatTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
            
        case NSFetchedResultsChangeMove:
            [chatTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            [chatTableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
    }
}
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [chatTableView endUpdates];
    [self scrollBubbleViewToBottomAnimated:YES];
}
- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [chatTableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [chatTableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}
- (void) scrollBubbleViewToBottomAnimated:(BOOL)animated {
    
    {
        if (!shouldScrollToBottom) {
            shouldScrollToBottom = YES;
        }
        else
        {
            NSInteger lastSectionIdx = [chatTableView numberOfSections] - 1;
            if (lastSectionIdx >= 0 && [chatTableView numberOfRowsInSection:lastSectionIdx] > 0)
            {
                [chatTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:([chatTableView numberOfRowsInSection:lastSectionIdx] - 1) inSection:lastSectionIdx] atScrollPosition:UITableViewScrollPositionBottom animated:animated];
            }
        }
    }
   
}

-(void)scrollToFirstUnreadMsgPosition: (BOOL)scroll
{
    
    int unreadMsgCount = [[[NSUserDefaults standardUserDefaults]valueForKey:[NSString stringWithFormat:@"%@%@",KEY_UNREAD,user.bare]]intValue];
    
    if (unreadMsgCount != 0)
    {
        NSInteger lastSectionIdx = [chatTableView numberOfSections] - 1;
        [chatTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:([chatTableView numberOfRowsInSection:lastSectionIdx] - (unreadMsgCount )) inSection:lastSectionIdx] atScrollPosition:UITableViewScrollPositionBottom animated:scroll];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:[NSString stringWithFormat:@"%@%@",KEY_UNREAD,user.bare]];
    }
    
}

-(void)tappedCell:(UITapGestureRecognizer *)sender
{
    CGPoint location = [sender locationInView:chatTableView];
    NSIndexPath *swipedIndexPath = [chatTableView indexPathForRowAtPoint:location];
    UITableViewCell *swipedCell  = (UITableViewCell*)[chatTableView cellForRowAtIndexPath:swipedIndexPath];
    if ([swipedCell isKindOfClass:[UIBubbleTableViewCell class]]) {
        UIBubbleTableViewCell *bubbleCell = (UIBubbleTableViewCell *)swipedCell;
        
        if(bubbleCell.deleteCell)
        {
            XMPPMessageArchiving_Message_CoreDataObject *coremessage = [[self fetchedResultsController ] objectAtIndexPath:swipedIndexPath];
            
            if ([deleteMsgIdAry containsObject:coremessage.messageID] )
            {
                [deleteMsgIdAry removeObject:coremessage.messageID];
                bubbleCell.deleteImage.image = [UIImage imageNamed:@"check_U"];
            }
            else
            {
                [deleteMsgIdAry addObject:coremessage.messageID];
                bubbleCell.deleteImage.image = [UIImage imageNamed:@"check_S"];
            }
        }
        else
        {

        if ([bubbleCell.data.view isKindOfClass:[UILabel class]]) {
            self.textLabel = (UILabel *)bubbleCell.data.view;
            CFIndex index = [self characterIndexAtPoint:[sender locationInView:self.textLabel]];
            NSError *error = NULL;
            NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
            NSArray *matchArray = [detector matchesInString:self.textLabel.text options:0 range:NSMakeRange(0, self.textLabel.text.length)];
            if(matchArray.count)
            {
                for (NSTextCheckingResult *match in matchArray) {
                    if ([match resultType] == NSTextCheckingTypeLink) {
                        NSRange matchRange = [match range];
                        if ([self isIndex:index inRange:matchRange]) {
                            [[UIApplication sharedApplication] openURL:match.URL];
                            break;
                        }
                        else if(bubbleCell.data.msgID && [bubbleCell.data.chatType isEqualToString:@"singlechat"] && bubbleCell.data.type == BubbleTypeSomeoneElse)
                        {
                            
                            
                        }
                    }
                }
            }
        }
        }
    }
    [self resignkeyBoard];
    
}
- (BOOL)isIndex:(CFIndex)index inRange:(NSRange)range {
    return index > range.location && index < range.location+range.length;
}
- (CFIndex)characterIndexAtPoint:(CGPoint)point {
    
    NSMutableAttributedString* optimizedAttributedText = [self.textLabel.attributedText mutableCopy];
    [self.textLabel.attributedText enumerateAttributesInRange:NSMakeRange(0, [self.textLabel.attributedText length]) options:0 usingBlock:^(NSDictionary *attrs, NSRange range, BOOL *stop) {
        if (!attrs[(NSString*)kCTFontAttributeName]) {
            [optimizedAttributedText addAttribute:(NSString*)kCTFontAttributeName value:self.textLabel.font range:NSMakeRange(0, [self.textLabel.attributedText length])];
        }
        if (!attrs[(NSString*)kCTParagraphStyleAttributeName]) {
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineBreakMode:self.textLabel.lineBreakMode];
            [optimizedAttributedText addAttribute:(NSString*)kCTParagraphStyleAttributeName value:paragraphStyle range:range];
        }
    }];
    // modify kCTLineBreakByTruncatingTail lineBreakMode to kCTLineBreakByWordWrapping
    [optimizedAttributedText enumerateAttribute:(NSString*)kCTParagraphStyleAttributeName inRange:NSMakeRange(0, [optimizedAttributedText length]) options:0 usingBlock:^(id value, NSRange range, BOOL *stop) {
        NSMutableParagraphStyle* paragraphStyle = [value mutableCopy];
        if ([paragraphStyle lineBreakMode] == kCTLineBreakByTruncatingTail) {
            [paragraphStyle setLineBreakMode:kCTLineBreakByWordWrapping];
        }
        [optimizedAttributedText removeAttribute:(NSString*)kCTParagraphStyleAttributeName range:range];
        [optimizedAttributedText addAttribute:(NSString*)kCTParagraphStyleAttributeName value:paragraphStyle range:range];
    }];
    if (!CGRectContainsPoint(self.textLabel.bounds, point)) {
        return NSNotFound;
    }
    CGRect textRect = [self textRect];
    if (!CGRectContainsPoint(textRect, point)) {
        return NSNotFound;
    }
    // Offset tap coordinates by textRect origin to make them relative to the origin of frame
    point = CGPointMake(point.x - textRect.origin.x, point.y - textRect.origin.y);
    // Convert tap coordinates (start at top left) to CT coordinates (start at bottom left)
    point = CGPointMake(point.x, textRect.size.height - point.y);
    //////
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)optimizedAttributedText);
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, textRect);
    CTFrameRef frame = CTFramesetterCreateFrame(framesetter, CFRangeMake(0, [self.textLabel.attributedText length]), path, NULL);
    if (frame == NULL) {
        CFRelease(path);
        return NSNotFound;
    }
    CFArrayRef lines = CTFrameGetLines(frame);
    NSInteger numberOfLines = self.textLabel.numberOfLines > 0 ? MIN(self.textLabel.numberOfLines, CFArrayGetCount(lines)) : CFArrayGetCount(lines);
    if (numberOfLines == 0) {
        CFRelease(frame);
        CFRelease(path);
        return NSNotFound;
    }
    
    NSUInteger idx = NSNotFound;
    CGPoint lineOrigins[numberOfLines];
    CTFrameGetLineOrigins(frame, CFRangeMake(0, numberOfLines), lineOrigins);
    
    for (CFIndex lineIndex = 0; lineIndex < numberOfLines; lineIndex++) {
        
        CGPoint lineOrigin = lineOrigins[lineIndex];
        CTLineRef line = CFArrayGetValueAtIndex(lines, lineIndex);
        
        // Get bounding information of line
        CGFloat ascent, descent, leading, width;
        width = CTLineGetTypographicBounds(line, &ascent, &descent, &leading);
        CGFloat yMin = floor(lineOrigin.y - descent);
        CGFloat yMax = ceil(lineOrigin.y + ascent);
        
        // Check if we've already passed the line
        if (point.y > yMax) {
            break;
        }
        // Check if the point is within this line vertically
        if (point.y >= yMin) {
            // Check if the point is within this line horizontally
            if (point.x >= lineOrigin.x && point.x <= lineOrigin.x + width) {
                // Convert CT coordinates to line-relative coordinates
                CGPoint relativePoint = CGPointMake(point.x - lineOrigin.x, point.y - lineOrigin.y);
                idx = CTLineGetStringIndexForPosition(line, relativePoint);
                
                break;
            }
        }
    }
    CFRelease(frame);
    CFRelease(path);
    
    return idx;
}

- (CGRect)textRect {
    
    CGRect textRect = [self.textLabel textRectForBounds:self.textLabel.bounds limitedToNumberOfLines:self.textLabel.numberOfLines];
    textRect.origin.y = (self.textLabel.bounds.size.height - textRect.size.height)/2;
    
    if (self.textLabel.textAlignment == NSTextAlignmentCenter) {
        textRect.origin.x = (self.textLabel.bounds.size.width - textRect.size.width)/2;
    }
    if (self.textLabel.textAlignment == NSTextAlignmentRight) {
        textRect.origin.x = self.textLabel.bounds.size.width - textRect.size.width;
    }
    
    return textRect;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Get Display Name for JID
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(NSString *)getDisplayName:(NSString *)resourceStr
{
    if ([resourceStr rangeOfString:DOMAINAME].location==NSNotFound )
        resourceStr = [resourceStr stringByAppendingString:[NSString stringWithFormat:@"@%@",DOMAINAME]];
    
    if ([resourceStr isEqualToString:[self xmppStream].myJID.bare ])
        return @"You";
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:[self managedObjectContext_roster]];
    [request setEntity:entity];
    NSString *predicateFrmt = @"jidStr = %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, resourceStr];
    [request setPredicate:predicate];
    NSArray *empArray=[[self managedObjectContext_roster] executeFetchRequest:request error:nil];
    if ([empArray count] == 1)
    {
        for (XMPPUserCoreDataStorageObject *userdetails in  empArray)
        {
            return userdetails.displayName;
        }
    }
    return [resourceStr stringByReplacingOccurrencesOfString:DOMAINAME withString:@""];;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Are You There View
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(UIView*)ruderView:(NSString *)type status:(NSString*)status
{
    UIImage*mapImage = [UIImage imageNamed:@"map-view.png"];
    UIView *ruderView  = [UIView new];
    ruderView.frame = CGRectMake(0, 0, 200, mapImage.size.height);
    ruderView.userInteractionEnabled = YES;
    ruderView.backgroundColor = [UIColor clearColor];
    
    UIImageView *mapImageView = [UIImageView new];
    mapImageView.backgroundColor = [UIColor clearColor];
    mapImageView.image = mapImage;
    [ruderView addSubview:mapImageView];
    
    UILabel *ruderLabel = [UILabel new];
    ruderLabel.backgroundColor = [UIColor clearColor];
    ruderLabel.textColor = [UIColor darkGrayColor];
    ruderLabel.textAlignment = NSTextAlignmentCenter;
    ruderLabel.font = [UIFont fontWithName:FontString size:10];
    ruderLabel.text = @"Hi,r u there yet?";
    ruderLabel.userInteractionEnabled = YES;
    [ruderView addSubview:ruderLabel];
    
    UIButton *ruderViewBtn = [UIButton buttonWithType:UIButtonTypeCustom];
	[ruderViewBtn setTitle:status forState:UIControlStateNormal];
    [ruderViewBtn setBackgroundColor:[UIColor lightGrayColor]];
    ruderViewBtn.titleLabel.font = [UIFont fontWithName:FontString size:10.0f];
    [ruderViewBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[ruderViewBtn addTarget:self action:@selector(ruderViewBtnAction:event:) forControlEvents:UIControlEventTouchUpInside];
	[ruderView addSubview:ruderViewBtn];
    
    if ([type isEqualToString:@"BubbleTypeSomeoneElse"]) {
        mapImageView.frame = CGRectMake(ruderView.frame.size.width-mapImage.size.width+1.5, ruderView.frame.origin.y+1,mapImage.size.width, mapImage.size.height);
        ruderLabel.frame = CGRectMake(ruderView.frame.size.width-mapImage.size.width-110,ruderView.frame.origin.y,ruderView.frame.size.width-100,15);
        ruderViewBtn.frame = CGRectMake(ruderView.frame.size.width-mapImage.size.width-110,ruderLabel.frame.origin.y+ruderLabel.frame.size.height+2,ruderView.frame.size.width-100,15);
    }
    else
    {
        mapImageView.frame = CGRectMake(ruderView.frame.origin.x+1, ruderView.frame.origin.y+2,mapImage.size.width, mapImage.size.height);
        ruderLabel.frame = CGRectMake(mapImageView.frame.origin.x+mapImageView.frame.size.width+10,ruderView.frame.origin.y,ruderView.frame.size.width-100,15);
        ruderViewBtn.frame = CGRectMake(mapImageView.frame.origin.x+mapImageView.frame.size.width+10,ruderLabel.frame.origin.y+ruderLabel.frame.size.height+2,ruderView.frame.size.width-100,15);
    }
    [mapImageView sizeToFit];
    return ruderView;
}
-(void)ruderViewBtnAction:(id)sender event:(id)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPos = [touch locationInView:chatTableView];
    NSIndexPath *indexPath = [chatTableView  indexPathForRowAtPoint:touchPos];
    if(indexPath != nil){
        UIBubbleTableViewCell *swipedCell  = (UIBubbleTableViewCell*)[chatTableView cellForRowAtIndexPath:indexPath];
//        NSLog(@"%@",swipedCell.data.view);
//        NSLog(@"%@",swipedCell.data.jidStr);
//        NSLog(@"%@",swipedCell.data.date);
//        NSLog(@"%@",swipedCell.data.ruderID);
        NSMutableDictionary* RuterDict = [dbEditer_Obj fetchData_RUTER_ID:swipedCell.data.ruderID];
        [self pushToView_RUTer:RuterDict];
    }
    
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark XMPPRoom Delegate Method
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)xmppRoomDidJoin:(XMPPRoom *)sender{
}

- (void)xmppRoom:(XMPPRoom *)sender occupantDidJoin:(XMPPJID *)occupantJID withPresence:(XMPPPresence *)presence;
{
    if (![[occupantJID resource] isEqualToString:[self xmppStream].myJID.user]) {
        NSString *userStr = [self getDisplayName:[occupantJID resource]];
        if (userStr == nil) {
            userStr = [userStr stringByReplacingOccurrencesOfString:domainName withString:@""];
        }
        NSHeaderData * data = [NSHeaderData dataWithText:[NSString stringWithFormat:@"%@ has joined", userStr] date:[NSDate date] jidStr:@"asd" name:@"sdfds" roomJid:@"sdfsd"];
        
        [self.messageData addObject:data];
        
        [chatTableView reloadData];
        [self scrollBubbleViewToBottomAnimated:YES];
        
     }
}
- (void)xmppRoom:(XMPPRoom *)sender occupantDidLeave:(XMPPJID *)occupantJID withPresence:(XMPPPresence *)presence
{
    if (![[occupantJID resource] isEqualToString:[self xmppStream].myJID.user]) {
        
        NSString *userStr = [self getDisplayName:[occupantJID resource]];
        if (userStr == nil) {
            userStr = [userStr stringByReplacingOccurrencesOfString:domainName withString:@""];
        }
        NSHeaderData * data = [NSHeaderData dataWithText:[NSString stringWithFormat:@"%@ has left", userStr] date:[NSDate date] jidStr:xmppRoom.roomJID.bare name:@"sdfds" roomJid:@"sdfsd"];
        [self.messageData addObject:data];
        
        [chatTableView reloadData];
        [self scrollBubbleViewToBottomAnimated:YES];
    }
}
- (void)xmppRoom:(XMPPRoom *)sender didReceiveMessage:(XMPPMessage *)message fromOccupant:(XMPPJID *)occupantJID
{
    
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Load Occupant Details
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(void)loadOccupant
{
    XMPPRoomCoreDataStorage *storage = [XMPPRoomCoreDataStorage sharedInstance];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"roomJIDStr == %@", user];
    NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:storage.occupantEntityName
                                                         inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    request.predicate = predicate;
    [request setEntity:entityDescription];
    [request setSortDescriptors:[NSSortDescriptor sortValues:@"createdAt" ascending:YES]];
    NSError *error;
    NSArray *messages_arc = [moc executeFetchRequest:request error:&error];
    for (XMPPRoomOccupantCoreDataStorageObject *coremessage in messages_arc)
    {
        if (![coremessage.realJIDStr isEqualToString:[[self xmppStream]myJID].bare] ) {
            NSHeaderData * data = [NSHeaderData dataWithText:coremessage.presencetext date:coremessage.createdAt jidStr:coremessage.roomJIDStr name:@"sdfds" roomJid:coremessage.roomJIDStr];
            [self.messageData addObject:data];
            [chatTableView reloadData];
            [self scrollBubbleViewToBottomAnimated:YES];
        }
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Core Data
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (NSManagedObjectContext *)managedObjectContext_roster
{
	return [[[self appDelegate] xmppRosterStorage] mainThreadManagedObjectContext];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Method to get room Name
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(NSString *)getRoomSubjectName:(XMPPJID *)jid
{
    XMPPRoomCoreDataStorage *storage = [XMPPRoomCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:storage.messageDetailsEntityName
                                              inManagedObjectContext:moc];
    [request setEntity:entity];
    NSString *predicateFrmt = @"roomJIDStr = %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, [jid bareJID]];
    [request setPredicate:predicate];
    NSArray *empArray=[moc executeFetchRequest:request error:nil];
    if ([empArray count] == 1){
        for (XMPPRoomMessageDetails *details in  empArray)
        {
            if (details.subject != nil)
                return details.subject;
        }
    }
    return @"Creating Group";
}
- (void)xmppRoom:(XMPPRoom *)sender didFetchMembersList:(NSArray *)items
{

    NSMutableArray* ReceiverAry = [NSMutableArray new];
    
    for (NSXMLElement* element in items)
    {
        
       
if (![[self removeDomainName_String:[[element attributeForName:@"jid"] stringValue]] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME]])
{
            [ReceiverAry addObject:[self removeDomainName_String:[[element attributeForName:@"jid"] stringValue]]];
        }
        
        
    }
    
    if ([ReceiverAry count])
    {
        [self pushToView_RUTer:[self generateRuterDict:ReceiverAry]];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ALERTVIEW_TITLE message:@"There are no Members online" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }

}

- (void)xmppRoom:(XMPPRoom *)sender didNotFetchMembersList:(XMPPIQ *)iqError
{

    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ALERTVIEW_TITLE message:@" Failed Fetch Members List" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];

}

-(void)pushToView_RUTer:(NSMutableDictionary*)ruter_Dict
{
    
    RUTerViewController* ruter_Obj = [[RUTerViewController alloc]init];
    ruter_Obj.title_Name = [self getRoomSubjectName:user];
    ruter_Obj.Room_Id = [user bare];;
    ruter_Obj.RUTer_Dict = ruter_Dict;
    [self.navigationController pushViewController:ruter_Obj animated:YES];
    
}

-(void)pushToProfileView
{
//    if (!isEditMode) {
//        ProfileViewController * profile = [ProfileViewController new];
//        profile.userDetails = user;
//        [self.navigationController pushViewController:profile animated:YES];
//    }
//    
}
-(NSString*)stringForStatus:(NSString*)status
{
    
    if ([status isEqualToString:@"0"]) {
        return @"Pending";
    }
    else if ([status isEqualToString:@"1"]) {
        return @"Active";
    }
    else if ([status isEqualToString:@"2"]) {
        return @"Reached";
    }
    else if ([status isEqualToString:@"3"]) {
        return @"Rejected";
    }
    else if ([status isEqualToString:@"4"])
    {
        return @"Cancelled";
    }
    else
    {
        return @"UNKNOWN";
    }
    
}


-(void)sendVirtualMessageForRuthereReached:(NSString *)RUTer_Id statusStr:(NSString *)rutherStatus
{
    
    NSMutableDictionary* RuterDict = [dbEditer_Obj fetchData_RUTER_ID:RUTer_Id];
    
    
    if ([rutherStatus isEqualToString:@"2"])
    {
        NSString *messageID=[self.xmppStream generateUUID];
        
        NSString *roomID = [NSString stringWithFormat:@"%@@shaili",[RuterDict valueForKey:@"from_Id"]];
        
        
        NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
        [body setStringValue:@"Are you there reached destination"];
        NSXMLElement *ruthere = [NSXMLElement elementWithName:@"ruthere"];
        [ruthere addAttributeWithName:@"xmlns" stringValue:@"ruthere"];
        [ruthere addAttributeWithName:@"ruthere_id" stringValue:[RuterDict valueForKey:@"RUTer_Id"]];
        [ruthere addAttributeWithName:@"type" stringValue:@"response"];
        [ruthere addAttributeWithName:@"from" stringValue:roomID];
        [ruthere addAttributeWithName:@"accepted" stringValue:@"true"];
        
        XMPPMessage *message = [XMPPMessage new];
        [message addAttributeWithName:@"id" stringValue:messageID];
        [message addAttributeWithName:@"type" stringValue:@"groupchat"];
        [message addAttributeWithName:@"to" stringValue:user.bare];
        [message addChild:body];
        [message addChild:ruthere];
        
        [[self xmppMessageArchivingCoreDataStorage ] sendingVirtualMediaMessage:message XmppStream:[self xmppStream]];
        
    }
    
    
    
    
}





#pragma mark - remove DomainName from String

-(NSString*)removeDomainName_String:(NSString*)string
{
    return [string stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"@%@",DOMAINAME] withString:@""];
}
-(NSMutableDictionary*)generateRuterDict :(NSMutableArray*)ReceiverAry
{
   
    NSMutableDictionary* ruterDict =   [NSMutableDictionary new];
    [ruterDict setObject:@"iAmSender" forKey:@"YES"];
    [ruterDict setObject:@"isActivated" forKey:@"NO"];
    [ruterDict setObject:[self getSenderDict] forKey:@"Sender"];
    [ruterDict setObject:[self getReceiverAry:ReceiverAry] forKey:@"Receiver"];
     [ruterDict setObject:@"1.5" forKey:@"range"];
    return ruterDict;
}

-(NSMutableDictionary*)getSenderDict
{
    NSMutableDictionary* senderDict =   [NSMutableDictionary new];
    [senderDict setObject:@"iS_Sender" forKey:@"YES"];
    [senderDict setObject:@"id" forKey:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME]];
    
    return senderDict;
    
}

-(NSMutableArray*)getReceiverAry:(NSMutableArray*)ids
{
    NSMutableArray* ReceiverAry =   [NSMutableArray new];
    
    for (NSString* str in ids)
    {
        NSMutableDictionary* ReceiverDict =   [NSMutableDictionary new];
        [ReceiverDict setObject:@"NO" forKey:@"iS_Sender"];
        [ReceiverDict setObject:str forKey:@"id"];
        [ReceiverAry addObject:ReceiverDict];
    }
    
    
    return ReceiverAry;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Gesture Methods
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)swipeGestureAction:(UISwipeGestureRecognizer *)gesture
{
    if (!isEditMode) {
        if (gesture.direction == UISwipeGestureRecognizerDirectionRight)
            isMenuShowing = YES;
        else if(gesture.direction == UISwipeGestureRecognizerDirectionLeft)
            isMenuShowing = NO;
        chatTableView.scrollEnabled = NO;
        [self menuButtonAction:nil];
    }
    
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Chat Slide Menu
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)menuButtonAction:(id)sender
{
    

        if (isMenuShowing)
        {
            
            
            [UIView animateWithDuration:1.0 delay:0.0 usingSpringWithDamping:0.5 initialSpringVelocity:0.7 options:0 animations:^{
                self.chatSlideMenu.frame = CGRectMake(self.view.frame.size.width+100, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height, 100,self.chatSlideMenu.frame.size.height);
                
            } completion:^(BOOL finished) {}];
            
            isMenuShowing = NO;
        }
        else
        {
            
            
            [UIView animateWithDuration:1.0 delay:0.0 usingSpringWithDamping:0.5 initialSpringVelocity:0.7 options:0 animations:^{
               self.chatSlideMenu.frame = CGRectMake(self.view.frame.size.width-100,self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height, 100, self.chatSlideMenu.frame.size.height);
                
            } completion:^(BOOL finished) {}];
            
            
            [self resignkeyBoard];
            isMenuShowing = YES;
        }
}
    
    


-(void)getMemberlist
{
    XMPPRoomCoreDataStorage *storage = [XMPPRoomCoreDataStorage sharedInstance];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"roomJIDStr == %@", user];
    NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:storage.occupantEntityName
                                                         inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    request.predicate = predicate;
    [request setEntity:entityDescription];
    request.resultType = NSDictionaryResultType;
    request.propertiesToFetch = [NSArray arrayWithObject:[[entityDescription propertiesByName] objectForKey:@"nickname"]];
    request.returnsDistinctResults = YES;
    
    // Now it should yield an NSArray of distinct values in dictionaries.
    NSArray *dictionaries = [moc executeFetchRequest:request error:nil];
    NSMutableArray* ReceiverAry = [NSMutableArray new];
    
    for (NSMutableDictionary* element in dictionaries)
    {
        
        
        if (![[element objectForKey:@"nickname"] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME]])
        {
            [ReceiverAry addObject:[element objectForKey:@"nickname"]];
        }
        
        
    }
    
    if ([ReceiverAry count])
    {
        [self pushToView_RUTer:[self generateRuterDict:ReceiverAry]];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ALERTVIEW_TITLE message:@"There are no Members online" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Chat Slide Menu Delegate Method
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)selectSlideMenuButton:(NSInteger)index
{
    switch (index) {
        case 1:
            [self getMemberlist];
            break;
        case 2:
        {
            [UIView transitionWithView:chatTableView
                              duration:0.5f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^(void) {
                                self.chatSlideMenu.frame = CGRectMake(self.view.frame.size.width+100, 0, 100, self.chatSlideMenu.frame.size.height);
                                [self changeViewToEditMode];
                            } completion:NULL];
            
            isMenuShowing = NO;

                    }
            break;

        case 3:
        {
            [UIView transitionWithView:chatTableView
                              duration:0.5f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^(void) {
                                self.chatSlideMenu.frame = CGRectMake(self.view.frame.size.width+100, 0, 100, self.chatSlideMenu.frame.size.height);
                            } completion:NULL];
            
            isMenuShowing = NO;
            
        
            if(!showImpMsgLy)
            {
                
                [menuButton setHidden:YES];

            menu.shouldShow = NO;
            
            popUp =[[ImpMsgPopUp alloc]initWithFrame:CGRectMake(0,0,chatTableView.frame.size.width, self.view.frame.size.height) view:self.view bareJIDStr:user.bare];
            
            popUp.ImpFromGroup = YES;
            [popUp setBackground:self.view];
            
            
            popUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
            
            [self.view addSubview:popUp];
            
            [UIView animateWithDuration:0.3/1.5 animations:^{
                popUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.3/2 animations:^{
                    popUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.3/2 animations:^{
                        popUp.transform = CGAffineTransformIdentity;
                        showImpMsgLy = YES;
                        
                    }];
                }];
            }];
            }
    }
            break;
        case 4:
        [self takevideo];
            break;
        case 5:
        {
            isGalleryOpen = YES;
            
            SecureGalleryViewController *secureGallery = [[SecureGalleryViewController alloc]init];
            [self.navigationController pushViewController:secureGallery animated:YES];
        }
            break;
        case 6:
        {
            [UIView transitionWithView:chatTableView
                              duration:0.3f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^(void) {
                                self.chatSlideMenu.frame = CGRectMake(self.view.frame.size.width+100, 0, 100, chatTableView.frame.size.height);
                                ;
                                
                            } completion: ^(BOOL finished){
                                [self setBackground];
                            }];
            
            isMenuShowing = NO;
            
        }
           default:
            break;
    }
}

-(void)selectBubbleMenuButton:(NSInteger)index
{
    [bubble restoreBtn];
    switch (index) {
        case 1:
            [self getMemberlist];
            break;
        case 2:
        {
            [UIView transitionWithView:chatTableView
                              duration:0.5f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^(void) {
                                self.chatSlideMenu.frame = CGRectMake(self.view.frame.size.width+100, 0, 100, self.chatSlideMenu.frame.size.height);
                                [self changeViewToEditMode];
                            } completion:NULL];
            
            isMenuShowing = NO;
            
        }
            break;
            
        case 3:
        {
         
            if(!showImpMsgLy)
            {
                menu.shouldShow = NO;
                
                popUp =[[ImpMsgPopUp alloc]initWithFrame:CGRectMake(0,0,chatTableView.frame.size.width, self.view.frame.size.height) view:self.view bareJIDStr:user.bare];
                
                popUp.ImpFromGroup = YES;
                [popUp setBackground:self.view];
                
                
                popUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
                
                [self.view addSubview:popUp];
                
                [UIView animateWithDuration:0.3/1.5 animations:^{
                    popUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.3/2 animations:^{
                        popUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
                    } completion:^(BOOL finished) {
                        [UIView animateWithDuration:0.3/2 animations:^{
                            popUp.transform = CGAffineTransformIdentity;
                            showImpMsgLy = YES;
                            
                        }];
                    }];
                }];
            }
        
        }
            break;
        case 4:
            [self takevideo];
            break;
        case 5:
        {
            isGalleryOpen = YES;
            
            SecureGalleryViewController *secureGallery = [[SecureGalleryViewController alloc]init];
            [self.navigationController pushViewController:secureGallery animated:YES];

        }
            break;
        case 6:
        {
            [UIView transitionWithView:chatTableView
                              duration:0.3f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^(void) {
                                self.chatSlideMenu.frame = CGRectMake(self.view.frame.size.width+100, 0, 100, chatTableView.frame.size.height);
                                ;
                                
                            } completion: ^(BOOL finished){
                                [self setBackground];
                            }];
            
            isMenuShowing = NO;
            
        }
            
        default:
            break;
    }

    
}

- (void)setBackground
{
    isGalleryOpen = YES;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Camera", nil)];
    }
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Photo Album", nil)];
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Set Default", nil)];
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    actionSheet.cancelButtonIndex = actionSheet.numberOfButtons - 1;
    
    
    [actionSheet showFromToolbar:self.navigationController.toolbar];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    if ([buttonTitle isEqualToString:NSLocalizedString(@"Photo Album", nil)]) {
        [self openPhotoAlbumForBG];
        
    } else if ([buttonTitle isEqualToString:NSLocalizedString(@"Camera", nil)]) {
        [self showCameraForBG];
    }
    else if ([buttonTitle isEqualToString:NSLocalizedString(@"Set Default", nil)])
    {
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:[NSString stringWithFormat:@"%@_BG",user.bare]];
        self.view.backgroundColor = CHAT_BACKGROUNDIMAGE;
        
    }
    
}




- (void)showCameraForBG
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    [[self appDelegate].statusBarView setHidden:YES];
    controller.delegate = self;
    controller.sourceType = UIImagePickerControllerSourceTypeCamera;
    controller.cameraDevice = UIImagePickerControllerCameraDeviceRear;
    controller.view.tag = 100;
    [self presentViewController:controller animated:YES completion:NULL];
    }
}

- (void)openPhotoAlbumForBG
{
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.delegate = self;
    controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    controller.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeImage, nil];
    controller.view.tag = 100;
    [self presentViewController:controller animated:YES completion:NULL];
    
}





//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Method to access Media
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)takePhoto
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        isCamOpened = YES;
        [[self appDelegate].statusBarView setHidden:YES];
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.delegate = self;
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        if( [UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceRear ])
        {
            controller.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        }
        else
        {
            controller.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        }
        
        
        [self presentViewController:controller animated:YES completion:NULL];
    }
    
}
- (void)takevideo
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        isCamOpened = YES;
        [[self appDelegate].statusBarView setHidden:YES];
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.delegate = self;
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        controller.mediaTypes =  [NSArray arrayWithObject:(NSString *)kUTTypeMovie];
        controller.videoQuality = UIImagePickerControllerQualityTypeMedium;
        if( [UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceRear ])
        {
            controller.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        }
        else
        {
            controller.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        }
        
        
        [self presentViewController:controller animated:YES completion:NULL];
    }
    
}
- (void)openPhotoAlbum
{
    isGalleryOpen = YES;
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.delegate = self;
    controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    controller.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie,(NSString *)kUTTypeImage, nil];
    [self presentViewController:controller animated:YES completion:NULL];
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Image Picker delegate Method
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////



- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    if (picker.view.tag == 100)
    {
        
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        [picker dismissViewControllerAnimated:YES completion:^{
            [self openEditor:image];
        }];
        
    }
    else
    {

    [picker dismissViewControllerAnimated:YES completion:^{
        
        NSString* pathStr;
        
        NSURL *mURL = [info objectForKey:UIImagePickerControllerMediaURL];
        
        
        [self createMediaFolder:@"mediaFile"];
        if (mURL == NULL)
        {
            NSString* path = [self getUniqueFilenameInFolder:@"mediaFile" forFileExtension:@"jpg"];
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *newFilePath = [[paths lastObject] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",path]];
            
            //>=-- Fix orientation and Save as data ------=<//
            UIImage* imag = [UIImage imageWithImage:[info objectForKey:UIImagePickerControllerOriginalImage] scaledToWidth:self.view.frame.size.width];
            NSData *imageData = UIImageJPEGRepresentation(imag, 0.7);
            
            
            
            if (imageData != nil) {
                [imageData writeToFile:newFilePath atomically:YES];
            }
            
       //     NSLog(@"imageData.length    %lu",(unsigned long)imageData.length);
            
            pathStr = [NSString stringWithString:newFilePath];
         //   NSLog(@"pathStr : %@",pathStr);
            
            NSString* msgId = [self sendVirtualMessage:@"📷 Image ":nil];
            
            if (msgId)
            {
                [self mediaDbEditor_msgId:msgId fileName:@"image" url:pathStr];
                
                [self postPhoto:pathStr:msgId fileType:@"image" ];
            }
            else
            {
             //   NSLog(@"Failed to generate msg id");
            }
        }
        else
        {
            NSString* path = [self getUniqueFilenameInFolder:@"mediaFile" forFileExtension:[[mURL.path componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"."]] lastObject]];
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *newFilePath = [[paths lastObject] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",path]];
            
            NSURL* urlStr = [NSURL fileURLWithPath:newFilePath];
            pathStr = newFilePath;
            [self convertVideoToLowQuailtyWithInputURL:mURL outputURL:urlStr handler:^(AVAssetExportSession *exportSession)
             {
                 if (exportSession.status == AVAssetExportSessionStatusCompleted)
                 {
                   //  NSLog(@"completed\n %@",newFilePath);
                     NSString* msgId = [self sendVirtualMessage:@"📹 Video ":nil];
                     
                     if (msgId)
                     {
                         [self mediaDbEditor_msgId:msgId fileName:@"video" url:pathStr];
                         
                         [self postPhoto:newFilePath:msgId fileType:@"video"];
                     }
                     else
                     {
                       //  NSLog(@"Failed to generate msg id");
                     }
                     
                 }
                 else
                 {
                     printf("error\n");
                     
                 }
             }];
        }
        
        
    }];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


- (void)openEditor:(UIImage*)img
{
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.image = img;
    
    
    
    controller.cropAspectRatio =  9.0f / 16.0f;
    
    
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    
    [self presentViewController:navigationController animated:YES completion:NULL];
}


#pragma mark -

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    UIImage* imageScaled = [UIImage imageWithImage:croppedImage scaledToSize:self.view.frame.size];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:imageScaled]];
    
    [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(imageScaled) forKey:[NSString stringWithFormat:@"%@_BG",user.bare]];
    
    
    
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
}





- (void)convertVideoToLowQuailtyWithInputURL:(NSURL*)inputURL
                                   outputURL:(NSURL*)outputURL
                                     handler:(void (^)(AVAssetExportSession*))handler
{
    [[NSFileManager defaultManager] removeItemAtURL:outputURL error:nil];
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:inputURL options:nil];
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetMediumQuality];
    exportSession.outputURL = outputURL;
    exportSession.outputFileType = AVFileTypeQuickTimeMovie;
    [exportSession exportAsynchronouslyWithCompletionHandler:^(void)
     {
         handler(exportSession);
     }];
}


-(void)mediaDbEditor_msgId:(NSString*)msgId fileName:(NSString*)fileName url:(NSString*)url
{
   mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
    NSMutableDictionary* dict = [NSMutableDictionary new];
    [dict setObject:msgId forKey:@"msgId"];
    [dict setObject:[NSString stringWithFormat:@"%@",user] forKey:@"userId"];
    [dict setObject:fileName forKey:@"fileName"];
    [dict setObject:url forKey:@"url"];
    [dict setObject:[NSNumber numberWithBool:NO] forKey:@"loc_cloud"];
    [dict setObject:[NSNumber numberWithBool:NO] forKey:@"sent_received"];
    [dict setObject:@"0" forKey:@"success_failure"];
    [dict setObject:[NSNumber numberWithBool:NO] forKey:@"sender_receiver"];
    [dict setObject:[NSNumber numberWithFloat:0.0] forKey:@"percentLoaded"];
    [dict setObject:fileName forKey:@"video_img"];
    [dict setObject:[NSNumber numberWithBool:NO] forKey:@"isPrivate"];

    
    
    
    [mediaDb checkAndInsert:dict];
    
    
    //loc_cloud        0 --> localUrl     1 -- > webUrl
    //sent_received    0 --> Sent         1 -- > Received
    //success_failure  0 --> pending      1 -- > uploading/downloading  2 --> success  3 --> failure
    //sender_receiver  0 --> sender       1 -- > receiver
    //video_img        0 --> video        1 -- > img
    
    
}

-(NSBubbleData*)constructMediaViewBubledata:(XMPPMessageArchiving_Message_CoreDataObject *)coremessage indexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *mediaDict =  [[mediaDbEditor sharedInstance] fetchMedia:coremessage.messageID];
    NSBubbleData *Bubbledata;
    if (![[mediaDict objectForKey:@"sent_received"] boolValue])
    {
        if ([[mediaDict objectForKey:@"video_img"] isEqualToString:@"video"])
        {
            Bubbledata = [NSBubbleData dataWithView:[self PhotoViewForBubbleData:[self constructImageFromVidUrl:[mediaDict objectForKey:@"url"]] status:[mediaDict objectForKey:@"success_failure"] isSender:YES msgId:[mediaDict objectForKey:@"msgId"] :coremessage] date:coremessage.timestamp type:BubbleTypeBeeMine insets:UIEdgeInsetsMake(5.0, 0.0, 3.0, 5.0)  jidString:[self xmppStream].myJID.bare RuderID:nil deliveryStr:coremessage.delivery chattype:@"singlechat" isSender:YES messageID:[mediaDict objectForKey:@"msgId"]];
        }
        else
        {
            Bubbledata = [NSBubbleData dataWithView:[self PhotoViewForBubbleData:[UIImage imageWithContentsOfFile:[self getFullDocumentUrl:[mediaDict objectForKey:@"url"]]] status:[mediaDict objectForKey:@"success_failure"] isSender:YES msgId:[mediaDict objectForKey:@"msgId"] :coremessage] date:coremessage.timestamp type:BubbleTypeBeeMine insets:UIEdgeInsetsMake(5.0, 0.0, 3.0, 5.0)  jidString:[self xmppStream].myJID.bare RuderID:nil deliveryStr:coremessage.delivery chattype:@"singlechat" isSender:YES messageID:[mediaDict objectForKey:@"msgId"]];
        }
    }
    else
    {
        if ([[mediaDict objectForKey:@"video_img"] isEqualToString:@"video"])
        {
            if ([[mediaDict objectForKey:@"success_failure"] isEqualToString:@"0"] || [[mediaDict objectForKey:@"success_failure"] isEqualToString:@"3"] || [[mediaDict objectForKey:@"success_failure"] isEqualToString:@"1"])
            {
                UIImage* image = [self decodeBase64ToImage:[mediaDict objectForKey:@"imgData"]];
                Bubbledata= [NSBubbleData dataWithView:[self PhotoViewForBubbleData:image status:[mediaDict objectForKey:@"success_failure"] isSender:NO msgId:[mediaDict objectForKey:@"msgId"] :coremessage] date:coremessage.timestamp type:BubbleTypeBeeSomeoneElse insets:UIEdgeInsetsMake(0.0, 5.0, 3.0, 3.0) jidString:[self xmppStream].myJID.bare RuderID:nil deliveryStr:coremessage.delivery chattype:@"singlechat" isSender:NO messageID:[mediaDict objectForKey:@"msgId"]];
            }
            else
            {
                Bubbledata= [NSBubbleData dataWithView:[self PhotoViewForBubbleData:[self constructImageFromVidUrl:[mediaDict objectForKey:@"url"]] status:[mediaDict objectForKey:@"success_failure"] isSender:NO msgId:[mediaDict objectForKey:@"msgId"]:coremessage] date:coremessage.timestamp type:BubbleTypeBeeSomeoneElse insets:UIEdgeInsetsMake(0.0, 5.0, 3.0, 3.0) jidString:[self xmppStream].myJID.bare RuderID:nil deliveryStr:coremessage.delivery chattype:@"singlechat" isSender:NO messageID:[mediaDict objectForKey:@"msgId"]];
            }
        }
        else
        {
            if ([[mediaDict objectForKey:@"success_failure"] isEqualToString:@"0"] || [[mediaDict objectForKey:@"success_failure"] isEqualToString:@"3"] || [[mediaDict objectForKey:@"success_failure"] isEqualToString:@"1"])
            {
                UIImage* image = [self decodeBase64ToImage:[mediaDict objectForKey:@"imgData"]];
                Bubbledata= [NSBubbleData dataWithView:[self PhotoViewForBubbleData:image status:[mediaDict objectForKey:@"success_failure"] isSender:NO msgId:[mediaDict objectForKey:@"msgId"]:coremessage] date:coremessage.timestamp type:BubbleTypeBeeSomeoneElse insets:UIEdgeInsetsMake(0.0, 5.0, 3.0, 3.0) jidString:[self xmppStream].myJID.bare RuderID:nil deliveryStr:coremessage.delivery chattype:@"singlechat" isSender:NO messageID:[mediaDict objectForKey:@"msgId"]];
            }
            else
            {
                Bubbledata= [NSBubbleData dataWithView:[self PhotoViewForBubbleData:[UIImage imageWithContentsOfFile: [self getFullDocumentUrl:[mediaDict objectForKey:@"url"]]] status:[mediaDict objectForKey:@"success_failure"] isSender:NO msgId:[mediaDict objectForKey:@"msgId"] :coremessage] date:coremessage.timestamp type:BubbleTypeBeeSomeoneElse insets:UIEdgeInsetsMake(0.0, 5.0, 3.0, 3.0) jidString:[self xmppStream].myJID.bare RuderID:nil deliveryStr:coremessage.delivery chattype:@"singlechat" isSender:NO messageID:[mediaDict objectForKey:@"msgId"]];
            }
        }
        
    }
    
    return Bubbledata;
    
}

-(UIImage*)constructImageFromVidUrl:(NSString*)url
{
    
    NSURL* sourceURL = [NSURL fileURLWithPath:[self getFullDocumentUrl:url]];
    AVAsset *asset = [AVAsset assetWithURL:sourceURL];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    imageGenerator.appliesPreferredTrackTransform=TRUE;
    CMTime time = CMTimeMake(1, 1);
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
//    NSLog(@"thumbnnail  %f",thumbnail.size.height);
    return thumbnail;
}
- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}
-(NSString*)sendVirtualMessage:(NSString*)str :(NSString*)msgId
{
    NSString* messageId;
    if (msgId == nil)
    {
        messageId=[self.xmppStream generateUUID];
    }
    else
    {
        messageId = msgId;
    }
    
    
    NSXMLElement *media = [NSXMLElement elementWithName:@"file"];
    
    XMPPMessage *groupInviteMessage = [XMPPMessage new];
    [groupInviteMessage addAttributeWithName:@"id"  stringValue:messageId];
    [groupInviteMessage addAttributeWithName:@"to" stringValue:[NSString stringWithFormat:@"%@/%@",user,[self xmppStream].myJID.user]];
    [groupInviteMessage addAttributeWithName:@"from" stringValue:[NSString stringWithFormat:@"%@/%@",user,[self xmppStream].myJID.user]];
    [groupInviteMessage addAttributeWithName:@"type" stringValue:@"groupchat"];
    NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
    [body setStringValue:str];
    [groupInviteMessage addChild:body];
    [groupInviteMessage addChild:media];
    
    if (msgId == nil)
    {
        
        XMPPMessageArchivingCoreDataStorage *storage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
        [storage sendingVirtualMediaMessage:groupInviteMessage XmppStream:[self xmppStream]];
    }
    else
    {
        [self.xmppStream sendElement:groupInviteMessage];
        
    }
    
    return messageId;
}
-(void)deleteFileAtPath:(NSString*)pathStr
{
    NSError *error;
    if ([[NSFileManager defaultManager] isDeletableFileAtPath:pathStr]) {
        BOOL success = [[NSFileManager defaultManager] removeItemAtPath:pathStr error:&error];
        if (!success) {
         //   NSLog(@"Error removing file at path: %@", error.localizedDescription);
        }
        else
        {
           // NSLog(@"Successfully removed file at path");
        }
    }
}
-(void)createMediaFolder:(NSString*)folderName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"mediaFile"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath]){
        
        NSError* error;
        if(  [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error])
        {
            
        }
        
        else
        {
          //  NSLog(@"[%@] ERROR: attempting to write create MyFolder directory", [self class]);
            NSAssert( FALSE, @"Failed to create directory maybe out of disk space?");
        }
    }
}
-(NSString *)getUniqueFilenameInFolder:(NSString *)folder forFileExtension:(NSString *)fileExtension {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *existingFiles = [fileManager contentsOfDirectoryAtPath:folder error:nil];
    NSString *uniqueFilename;
    
    do {
        CFUUIDRef newUniqueId = CFUUIDCreate(kCFAllocatorDefault);
        CFStringRef newUniqueIdString = CFUUIDCreateString(kCFAllocatorDefault, newUniqueId);
        
        uniqueFilename = [[folder stringByAppendingPathComponent:(__bridge NSString *)newUniqueIdString] stringByAppendingPathExtension:fileExtension];
        
        CFRelease(newUniqueId);
        CFRelease(newUniqueIdString);
    } while ([existingFiles containsObject:uniqueFilename]);
    
    return uniqueFilename;
}


- (NSString *)encodeToBase64String:(NSData *)imageData {
    return [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}
-(void)postPhoto :(NSString*)pathStr :(NSString*)msgId fileType:(NSString*)fileType
{
    
    NSMutableArray* ary = [NSMutableArray new];
    [ary addObject:[self xmppStream].myJID.bare];
    
    RMDownloadIndicator* uploadIndicator =   [self addDownloadIndicators];
    self.singleton = [Singleton sharedMySingleton];
    [self.singleton.mediaFileIndicatorDict setObject:uploadIndicator forKey:msgId];
    [chatTableView reloadData];
    
    NSMutableDictionary *paramMutable = [[NSMutableDictionary alloc] init];
    [paramMutable setObject:[self xmppStream].myJID.bare forKey:@"sender_id"];
    [paramMutable setObject:ary forKey:@"receiver_id"];
    
    NSMutableDictionary *rParam = [[NSMutableDictionary alloc] init];
    [rParam setObject:@"mediaFile" forKey:@"functionName"];
    [rParam setObject:paramMutable forKey:@"Parameter"];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:rParam options:NSJSONWritingPrettyPrinted error:&error];
    
    AFHTTPClient* httpClient = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:FILE_UPLOAD]];
    
    
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    
    NSMutableURLRequest *afRequest = [httpClient multipartFormRequestWithMethod:@"POST" path:@"" parameters:nil constructingBodyWithBlock:^(id <AFMultipartFormData>formData)
                                      {
                                          if (pathStr != NULL)
                                          {
                                              //> Media is attached
                                              NSURL *mUrl = [NSURL URLWithString:pathStr];
                                              
                                              //>=-  IF URL IS EMPTY MAKE IT AS COMPLETE FILE PATH -=<//
                                              if (mUrl == NULL) {
                                                  mUrl = [NSURL fileURLWithPath:pathStr];
                                              }
                                              if ([[NSString stringWithFormat:@"%@",pathStr] hasPrefix:@"/"]) {
                                                  mUrl = [NSURL fileURLWithPath:pathStr];
                                              }
                                              
                                              if (mUrl != NULL)
                                              {
                                                  NSArray *extentionAry = [[mUrl lastPathComponent] componentsSeparatedByString:@"."];
                                                  // NSLog(@"extentionAry : %@", extentionAry);
                                                  
                                                  NSString *mimeType;
                                                  if ([[extentionAry lastObject] caseInsensitiveCompare:@"MOV"]==NSOrderedSame)
                                                  mimeType = @"video/mov";
                                                  else if ([[extentionAry lastObject] caseInsensitiveCompare:@"MP4"]==NSOrderedSame)
                                                  mimeType = @"video/mp4";
                                                  else if ([[extentionAry lastObject] caseInsensitiveCompare:@"3GP"]==NSOrderedSame)
                                                  mimeType = @"video/3gp";
                                                  else if ([[extentionAry lastObject] caseInsensitiveCompare:@"PNG"]==NSOrderedSame)
                                                  mimeType = @"image/png";
                                                  else if ([[extentionAry lastObject] caseInsensitiveCompare:@"JPG"]==NSOrderedSame)
                                                  {
                                                      mimeType = @"image/jpg";
                                                  }
                                                  else if ([[extentionAry lastObject] caseInsensitiveCompare:@"JPEG"]==NSOrderedSame)
                                                  {
                                                      mimeType = @"image/jpeg";
                                                  }
                                                  
                                                  if (mimeType!=NULL) {
                                                      
                                                   //   NSLog(@"mUrl : %@", mUrl);
                                                   //   NSLog(@"mimeType : %@", mimeType);
                                                      // NSLog(@"mimeType : %@", [mUrl lastPathComponent]);
                                                      
                                                      [formData appendPartWithFileURL:mUrl name:@"Path" fileName:[mUrl lastPathComponent] mimeType:mimeType error:nil];
                                                      
                                                  }
                                                  
                                              }
                                              
                                          }
                                          
                                          
                                          [formData appendPartWithFormData:jsonData name:@"Request"];
                                          NSLog(@"formData  %@",formData);
                                          
                                      }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:afRequest];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten,long long totalBytesWritten,long long totalBytesExpectedToWrite){
        
        [self updateView:((float)totalBytesWritten/ totalBytesExpectedToWrite)*100 indicatorView:uploadIndicator];
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         
         
         NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         responseStr = [responseStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    //     NSLog(@"Response Str : %@",responseStr);
         
         NSError *error = nil;
         id objectsFromJson = [NSJSONSerialization JSONObjectWithData:[responseStr dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&error];
         
         if (!error) {
          //   NSLog(@"Success");
             //> NSLog(@"Object from Json : %@",objectsFromJson);
             
             if ([[objectsFromJson allKeys] containsObject:@"ResponseMsg"]) {
                 if ([[objectsFromJson valueForKey:@"ResponseMsg"] caseInsensitiveCompare:@"Success"] == NSOrderedSame)
                 {
                     UIImage* previewImg;
                     if ([fileType isEqualToString:@"video"])
                     {
                         previewImg = [self constructImageFromVidUrl:pathStr];
                     }
                     else
                     {
                         previewImg = [UIImage imageWithContentsOfFile:pathStr];
                     }
                     
                     UIImage* imag = [UIImage imageWithImage:previewImg scaledToWidth:30];;
                     NSData *imageData_30 = UIImageJPEGRepresentation(imag, 0.1);
                     
                     [self isUploadingMsgId:msgId success:@"2"];
                     
                     [self sendMediaMsg:[objectsFromJson valueForKey:@"UrlString"] file_id:[objectsFromJson valueForKey:@"FileId"] file_type:fileType file_size:[objectsFromJson valueForKey:@"FileSize"] file_preview:[self encodeToBase64String:imageData_30] msgId:msgId];
                     
                 }
                 else {
                     [self isUploadingMsgId:msgId success:@"3"];
                 }
                 
                 
             }
         }
         else {
          //   NSLog(@"Error : %@",error);
             [self isUploadingMsgId:msgId success:@"3"];
             
             [[[UIAlertView alloc] initWithTitle:ALERTVIEW_TITLE message:@"\u26A0 Invalid response from server." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
         }
         [self.singleton.mediaFileIndicatorDict removeObjectForKey:msgId];
         [chatTableView reloadData];
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         
         [self isUploadingMsgId:msgId success:@"3"];
         [self.singleton.mediaFileIndicatorDict removeObjectForKey:msgId];
         [chatTableView reloadData];
         
         
      //   NSLog(@"error : %@",  operation.responseString);
         
         [[[UIAlertView alloc] initWithTitle:ALERTVIEW_TITLE message:@"\u26A0 Invalid response from server." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
     }];
    [operation start];
    
}



-(void)sendMediaMsg:(NSString*)file_url file_id:(NSString*)file_id file_type:(NSString*)file_type file_size:(NSString*)file_size file_preview:(NSString*)file_preview msgId:(NSString*)msgId
{
    NSString *messageStr = file_type;
    if([messageStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length > 0) {

        NSString *messageID=msgId;
        
        NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
        [body setStringValue:messageStr];
        NSXMLElement *file = [NSXMLElement elementWithName:@"file"];
        [file addAttributeWithName:@"xmlns" stringValue:@"shaili:file"];
        [file addAttributeWithName:@"file_url" stringValue:file_url];
        [file addAttributeWithName:@"file_id" stringValue:file_id];
        [file addAttributeWithName:@"file_type" stringValue:file_type];
        [file addAttributeWithName:@"file_size" stringValue:file_size];
        
        [file addAttributeWithName:@"from" stringValue:[[self xmppStream] myJID].bare];
        
        NSXMLElement *filepreview = [NSXMLElement elementWithName:@"file_preview"];
        [filepreview setStringValue:file_preview];
        [file addChild:filepreview];
        
       
        NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
        [message addAttributeWithName:@"id" stringValue:messageID];
        [message addAttributeWithName:@"type" stringValue:@"groupchat"];
        [message addAttributeWithName:@"to" stringValue:[NSString stringWithFormat:@"%@",user]];
        [message addChild:body];
        [message addChild:file];
        
        if (isPrivateChat) {
            
            NSXMLElement *private = [NSXMLElement elementWithName:@"private"];
            [private addAttributeWithName:@"xmlns" stringValue:@"private"];
            [private addAttributeWithName:@"type" stringValue:@"message"];
            [message addChild:private];
            
            
        }
        [self.xmppStream sendElement:message];
        
        if (isPrivateChat)
        {
            mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
            NSMutableDictionary* dict = [NSMutableDictionary new];
            [dict setObject:[[message attributeForName:@"id"] stringValue]  forKey:@"msgId"];
            [dict setObject:[NSNumber numberWithBool:YES] forKey:@"isPrivate"];
            [mediaDb updateMedia:dict];
            
        }
        
    }
}

-(void)isUploadingMsgId:(NSString*)msgId success:(NSString*)success
{
    mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
    
    NSMutableDictionary* dict = [NSMutableDictionary new];
    [dict setObject:msgId forKey:@"msgId"];
    [dict setObject:success forKey:@"success_failure"];
    [mediaDb checkAndInsert:dict];
    
}
- (RMDownloadIndicator*)addDownloadIndicators
{
    
    
    RMDownloadIndicator *filledIndicator = [[RMDownloadIndicator alloc]initWithFrame:CGRectMake(2, 2, widthOfBlackTransVw - 4, widthOfBlackTransVw - 4) type:kRMClosedIndicator];
    [filledIndicator setBackgroundColor:[UIColor clearColor]];
    [filledIndicator setFillColor:RGBA(255, 255, 255, 1)];
    [filledIndicator setStrokeColor:RGBA(34, 160, 246, 1)];
    filledIndicator.radiusPercent = 0.91;
    filledIndicator.coverWidth = 3;
    [filledIndicator loadIndicator];
    
    UILabel* percentLbl = [UILabel new];
    percentLbl.frame = CGRectMake(0, (filledIndicator.frame.size.width - 30)/2, filledIndicator.frame.size.width, 30);
    percentLbl.backgroundColor = [UIColor clearColor];
    percentLbl.textColor = [UIColor whiteColor];
    percentLbl.font = [UIFont fontWithName:FontString size:12];
    percentLbl.textAlignment = NSTextAlignmentCenter;
    [filledIndicator addSubview:percentLbl];
    
    return filledIndicator;
    
}

- (void)updateView:(CGFloat)val indicatorView:(RMDownloadIndicator*)indicatorView
{
    for (UILabel* lbl in [indicatorView subviews])
    {
        lbl.text = [NSString stringWithFormat:@"%.00f",val];
    }
    [indicatorView updateWithTotalBytes:100 downloadedBytes:val];
    
}

-(UIView*)mediaCellView:(CGSize)imageSize status:(NSString*)status image:(UIImage*)image isSender:(BOOL)isSender msgId:(NSString*)msgId :(XMPPMessageArchiving_Message_CoreDataObject*)coremessage
{
    
    
    UIView* mediaView = [UIView new];
    mediaView.frame = CGRectMake(15, 0, imageSize.width + 10, imageSize.height + 10);
    mediaView.backgroundColor = [UIColor whiteColor];
    mediaView.layer.cornerRadius = 5.0f;
    mediaView.layer.masksToBounds = YES;
    
    
    
    UIImageView* cellmediaImageView = [UIImageView new];
    cellmediaImageView.tag = 100;
    cellmediaImageView.frame = CGRectMake(5, 5,imageSize.width, imageSize.height);
    cellmediaImageView.image = image;
    cellmediaImageView.layer.cornerRadius = 5.0f;
    cellmediaImageView.layer.masksToBounds = YES;

    cellmediaImageView.userInteractionEnabled = YES;
    [mediaView addSubview:cellmediaImageView];
    
    
    UIView* blackTransVw = [UIView new];
    blackTransVw.backgroundColor = RGBA(0, 0, 0, 0.6);
     if (!isSender) {
    blackTransVw.frame = CGRectMake((cellmediaImageView.frame.size.width - widthOfBlackTransVw)/2, ((cellmediaImageView.frame.size.height + 20 - widthOfBlackTransVw)/2), widthOfBlackTransVw, widthOfBlackTransVw);
     }
    else
    {
        blackTransVw.frame = CGRectMake((cellmediaImageView.frame.size.width - widthOfBlackTransVw)/2, (cellmediaImageView.frame.size.height - widthOfBlackTransVw)/2, widthOfBlackTransVw, widthOfBlackTransVw);

    }
    blackTransVw.layer.cornerRadius = widthOfBlackTransVw/2;
    blackTransVw.layer.masksToBounds = YES;
    [cellmediaImageView addSubview:blackTransVw];
    
    self.singleton = [Singleton sharedMySingleton];
    if ([[self.singleton.mediaFileIndicatorDict allKeys] containsObject:msgId])
    {
        RMDownloadIndicator* uploadIndicator = [self.singleton.mediaFileIndicatorDict objectForKey:msgId];
        [blackTransVw addSubview:uploadIndicator];
    }
    else
    {
        UIImageView* iconImgVw = [UIImageView new];
        [blackTransVw addSubview:iconImgVw];
        
        if ([status isEqualToString:@"0"] || [status isEqualToString:@"3"] )
        {
            UIButton* celldownloadBtn = [UIButton buttonWithType:UIButtonTypeSystem];
            celldownloadBtn.frame = CGRectMake(0 , 0, imageSize.width, imageSize.height);
            if ([status isEqualToString:@"0"])
            {
                if (isSender)
                {
                    UIImage * icon = [UIImage imageNamed:@"retry"];
                    iconImgVw.image = icon;
                    iconImgVw.frame = CGRectMake((blackTransVw.frame.size.width - icon.size.width)/2, (blackTransVw.frame.size.height - icon.size.height)/2, icon.size.width, icon.size.height);
                    
                    if (!isEditMode) {
                        [celldownloadBtn addTarget:self action:@selector(UploadBtn_CLICKED:event:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else
                    {
                        celldownloadBtn.userInteractionEnabled = NO;
                    }
                    
                }
                else
                {
                    UIImage * icon = [UIImage imageNamed:@"download"];
                    iconImgVw.image = icon;
                    iconImgVw.frame = CGRectMake((blackTransVw.frame.size.width - icon.size.width)/2, (blackTransVw.frame.size.height - icon.size.height)/2, icon.size.width, icon.size.height);
                    
                    if (!isEditMode) {
                        [celldownloadBtn addTarget:self action:@selector(downloadBtn_CLICKED:event:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else
                    {
                        celldownloadBtn.userInteractionEnabled = NO;
                    }
                    
                }
            }
            else
            {
                if (isSender)
                {
                    UIImage * icon = [UIImage imageNamed:@"retry"];
                    iconImgVw.image = icon;
                    iconImgVw.frame = CGRectMake((blackTransVw.frame.size.width - icon.size.width)/2, (blackTransVw.frame.size.height - icon.size.height)/2, icon.size.width, icon.size.height);
                    
                    if (!isEditMode) {
                        [celldownloadBtn addTarget:self action:@selector(UploadBtn_CLICKED:event:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else
                    {
                        celldownloadBtn.userInteractionEnabled = NO;
                    }
                    
                }
                else
                {
                    UIImage * icon = [UIImage imageNamed:@"retry"];
                    iconImgVw.image = icon;
                    iconImgVw.frame = CGRectMake((blackTransVw.frame.size.width - icon.size.width)/2, (blackTransVw.frame.size.height - icon.size.height)/2, icon.size.width, icon.size.height);
                    if (!isEditMode) {
                        [celldownloadBtn addTarget:self action:@selector(downloadBtn_CLICKED:event:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else
                    {
                        celldownloadBtn.userInteractionEnabled = NO;
                }
                }
            }
            
            
            [celldownloadBtn setTintColor:[UIColor brownColor]];
            celldownloadBtn.tag = 101;
            [cellmediaImageView addSubview:celldownloadBtn];
        }
        
        if ([status isEqualToString:@"2"])
        {
            UIButton* cellPlayBtn = [UIButton buttonWithType:UIButtonTypeSystem];
            cellPlayBtn.frame = CGRectMake(0 , 0, image.size.width, image.size.height);
            
            mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
            NSMutableDictionary* dict = [mediaDb fetchMedia:msgId];
            
            if ([[dict objectForKey:@"video_img"] isEqualToString:@"video"])
            {
                
                UIImage * icon = [UIImage imageNamed:@"play"];
                iconImgVw.image = icon;
                iconImgVw.frame = CGRectMake((blackTransVw.frame.size.width - icon.size.width)/2, (blackTransVw.frame.size.height - icon.size.height)/2, icon.size.width, icon.size.height);
            }
            else
            {
                [blackTransVw removeFromSuperview];
            }
            
            if (!isEditMode) {
                [cellPlayBtn addTarget:self action:@selector(photoImageClicked:event:) forControlEvents:UIControlEventTouchUpInside];
            }
            else
            {
                cellPlayBtn.userInteractionEnabled = NO;
            }

            [cellPlayBtn setTintColor:[UIColor brownColor]];
            
            cellPlayBtn.tag = 102;
            [cellmediaImageView addSubview:cellPlayBtn];
        }
    }
    
    if (!isSender) {
        UILabel* nameLbl = [UILabel new];
        nameLbl.frame = CGRectMake(cellmediaImageView.frame.origin.x, cellmediaImageView.frame.origin.y, imageSize.width , 20);
        nameLbl.text = [NSString stringWithFormat:@"  %@",[self getDisplayName:[coremessage.message from].resource]];
        nameLbl.textColor = [UIColor whiteColor];
        nameLbl.backgroundColor = RGBA(1.0, 1.0, 1.0, 0.6);
        nameLbl.font = [UIFont fontWithName:FontString size:13];
        [mediaView addSubview:nameLbl];
    }
    return mediaView;
}
-(UIView *)PhotoViewForBubbleData:(UIImage *)image status:(NSString*)status isSender:(BOOL)isSender msgId:(NSString*)msgId :(XMPPMessageArchiving_Message_CoreDataObject*)coremessage
{
    CGSize size = image.size;
    if (size.width > 220 && size.height > 220)
    {
        size.width = 220;
        size.height = 220;
    }
    else if(size.width > 220)
    {
        size.height /= (size.width / 220);
        size.width = 220;
    }
    else if(size.height > 220)
    size.height  = 220;
    
    UIImage  *newImage =    [UIImage squareImageWithImage:image scaledToSize:CGSizeMake(150, 150)];
    
    UIView* PhotoView = [self mediaCellView:newImage.size status:status image:newImage isSender:isSender msgId:msgId :coremessage ];
    return PhotoView;
}


-(void)photoImageClicked:(id)sender event:(id)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPos = [touch locationInView:chatTableView];
    NSIndexPath *indexPath = [chatTableView  indexPathForRowAtPoint:touchPos];
    
    if(indexPath != nil)
    {
        UIBubbleTableViewCell *swipedCell  = (UIBubbleTableViewCell*)[chatTableView cellForRowAtIndexPath:indexPath];
        mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
        NSMutableDictionary* dict = [mediaDb fetchMedia:swipedCell.data.msgID];
        NSString *mediaURL = [self getFullDocumentUrl:[dict objectForKey:@"url"]];

        if ([[dict objectForKey:@"video_img"] isEqualToString:@"video"])
        {
            if ([dict objectForKey:@"url"])
            {
                isvideoPlayed = YES;
                MPMoviePlayerViewController* movieController = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL fileURLWithPath:mediaURL]];
                [self presentMoviePlayerViewControllerAnimated:movieController];
                [movieController.moviePlayer play];
            }
        }
        else
        {
            if ([dict objectForKey:@"url"])
            {
                UIImage* image = [UIImage imageWithContentsOfFile:mediaURL];
                UIImageView* cellmediaImageView = [UIImageView new];
                cellmediaImageView.tag = 100;
                cellmediaImageView.frame = CGRectMake(self.view.frame.size.width/2 ,self.view.frame.size.height,image.size.width, image.size.height);
                cellmediaImageView.image = image;
                
                if (!isFetching) {
                    
                
                [self getFilesData:user completion:^(NSArray *filesArray, BOOL success) {
                    
                    if(success)
                    {
                        NSUInteger indexValue;
                        for(FileRecord *record in self.photos)
                        {
                            
                            NSString *stringToCompare = [NSString stringWithFormat:@"%@",record.URL];
                            int lengthOfString = [stringToCompare length];
                            NSString *str = [stringToCompare substringFromIndex:lengthOfString-15];
                            
                            NSString *stringFromLocal = [NSString stringWithFormat:@"%@", [NSURL fileURLWithPath:[dict objectForKey:@"url"]]];
                            int lengthOfLocalString = [stringFromLocal length];
                            NSString *str1 = [stringFromLocal substringFromIndex:lengthOfLocalString-15];
                            
                            if ([str isEqualToString:str1])
                            {
                                indexValue = [self.photos indexOfObject:record];
                            }
                        }                        dispatch_async(dispatch_get_main_queue(), ^
                                       {
                                           isFetching = NO;

                                           DetailGalleryViewController *detailsView = [DetailGalleryViewController new];
                                           detailsView.detailArray = [self.photos mutableCopy];
                                           detailsView.indexpath = [NSIndexPath indexPathForRow:indexValue inSection:0];
                                           [self.navigationController pushViewController:detailsView animated:NO];
                                       });
                    }
                }];
                }
                
            }
        }
        
        
    }
}

-(void)getFilesData:(XMPPJID *)roomjid completion:(void (^)(NSArray *filesArray,BOOL success))completion
{
    isFetching = YES;
    [self.photos removeAllObjects];
    [[FetchsharedFile sharedMediaServer] fetchMediaFiles:roomjid.bare From:@"sharedFileView" block:^(NSArray *items, NSError *error) {
        if (!error && items.count)
        {
           
                for (NSDictionary *dic in items)
                {
                    //  NSNumber*boolean = [dic objectForKey:@"isPrivate"];
                    
                    NSNumber  *boolean = (NSNumber *)[dic objectForKey:@"isPrivate"];
                    if([boolean boolValue] == YES)
                    {
                      //  NSLog(@"YES --->> isPrivate");
                        // this is the YES case
                    }
                    else
                    {
                       // NSLog(@"NO --->> isPrivate");
                        
                        FileRecord *record = [[FileRecord alloc] init];
                        record.URL = [NSURL fileURLWithPath:[self getFullDocumentUrl:[dic objectForKey:@"url"]]];
                        record.type = [dic objectForKey:@"video_img"];
                        record.msgId = [dic objectForKey:@"msgId"];
                        record.isPrivate = NO;
                        [self.photos addObject:record];
                        record = nil;
                        
                    }
                    
                }

            
            
        }
        if (self.photos.count > 0)
            completion(self.photos,YES);
        else
            completion(nil,NO);
        
    }];
}

-(void)downloadBtn_CLICKED:(id)sender event:(id)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPos = [touch locationInView:chatTableView];
    NSIndexPath *indexPath = [chatTableView  indexPathForRowAtPoint:touchPos];
    if(indexPath != nil){
        UIBubbleTableViewCell *swipedCell  = (UIBubbleTableViewCell*)[chatTableView cellForRowAtIndexPath:indexPath];
//        NSLog(@"%@",swipedCell.data.view);
//        NSLog(@"%@",swipedCell.data.jidStr);
//        NSLog(@"%@",swipedCell.data.date);
//        NSLog(@"%@",swipedCell.data.msgID);
        mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
        NSMutableDictionary* dict = [mediaDb fetchMedia:swipedCell.data.msgID];
        NSArray *filterArray = [NSArray arrayWithObject:[dict objectForKey:@"url"]];
        
        NSArray *beginMatch = [filterArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self BEGINSWITH[cd] %@", @"http"]];
        
        
        if (beginMatch.count == 1)
        {
            [self fileDownload1:[dict objectForKey:@"url"] msgId:[dict objectForKey:@"msgId"]];
        }
        else
        {
            [self imageCache:[dict objectForKey:@"url"] indexPath:indexPath MsgID:[dict objectForKey:@"msgId"]];
        }

        
        
        
//        if ([dict objectForKey:@"url"])
//        {
//            [self fileDownload1:[dict objectForKey:@"url"] msgId:[dict objectForKey:@"msgId"]];
//        }
//        
        
    }
    
    
}

-(void)imageCache:(NSString*)imageUrlString indexPath:(NSIndexPath*)indexPath MsgID:(NSString *)msgID
{
        NSURL *imageUrl   = [NSURL fileURLWithPath:imageUrlString];
        NSData *imageData = [NSData dataWithContentsOfURL:imageUrl];
        UIImage *image    = nil;
        if (imageData)
            image = [UIImage imageWithData:imageData];
        if (image)
        {
            // add the image to your cache
            [self.imageCache setObject:image forKey:msgID];
            // finally, update the user interface in the main queue
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [chatTableView reloadData];
            }];
        }
    
}



- (NSString *)getFullDocumentUrl:(NSString *)fileName
{
    
    NSRange range = [fileName rangeOfString:@"Documents"];
    
    NSString *newString = [fileName substringFromIndex:range.location+9];
  //  NSLog(@"new string %@",newString);

    return [NSString stringWithFormat:@"%@/%@",[self applicationDocumentsDirectory],newString];
}

- (NSString *)applicationDocumentsDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}


-(void)fileDownload1:(NSString*)urlString msgId:(NSString*)msgId
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request] ;
    
    RMDownloadIndicator* downloadIndicator =   [self addDownloadIndicators];
    self.singleton = [Singleton sharedMySingleton];
    [self.singleton.mediaFileIndicatorDict setObject:downloadIndicator forKey:msgId];
    [chatTableView reloadData];
    
    NSString* path = [self getUniqueFilenameInFolder:@"mediaFile" forFileExtension:[[urlString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"."]] lastObject]];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *newFilePath = [[paths lastObject] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",path]];
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:newFilePath append:NO];
    
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        [self updateView:((float)totalBytesRead/ totalBytesExpectedToRead)*100 indicatorView:downloadIndicator];
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
       // NSLog(@"Successfully downloaded file to %@", newFilePath);
        
        
        NSMutableDictionary* dict = [NSMutableDictionary new];
        [dict setObject:msgId forKey:@"msgId"];
        [dict setObject:@"2" forKey:@"success_failure"];
        [dict setObject:@"" forKey:@"imgData"];
        [dict setObject:newFilePath forKey:@"url"];
        [dict setObject:[NSNumber numberWithBool:NO] forKey:@"loc_cloud"];
        mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
        [mediaDb updateMedia:dict];
        mediaDb = nil;
        
        [self.singleton.mediaFileIndicatorDict removeObjectForKey:msgId];
        [chatTableView reloadData];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
       // NSLog(@"Error: %@", error);
       // [self isUploadingMsgId:msgId success:@"3"];
        [self isNotDownloaded: msgId success:@"3" servicePath:urlString];

        [self.singleton.mediaFileIndicatorDict removeObjectForKey:msgId];
        [chatTableView reloadData];
        
    }];
    
    [operation start];
}
-(void)fileDownload:(NSString*)urlString msgId:(NSString*)msgId
{
    NSURL* sourceURL = [NSURL URLWithString:urlString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:sourceURL];
    
    AFHTTPRequestOperation *operation =   [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    RMDownloadIndicator* downloadIndicator =   [self addDownloadIndicators];
    self.singleton = [Singleton sharedMySingleton];
    [self.singleton.mediaFileIndicatorDict setObject:downloadIndicator forKey:msgId];
    [chatTableView reloadData];
    
    NSString* path = [self getUniqueFilenameInFolder:@"mediaFile" forFileExtension:[[urlString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"."]] lastObject]];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *newFilePath = [[paths lastObject] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",path]];
    
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:newFilePath append:NO];
    
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        [self updateView:((float)totalBytesRead/ totalBytesExpectedToRead)*100 indicatorView:downloadIndicator];
        
    }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         [self.singleton.mediaFileIndicatorDict removeObjectForKey:msgId];
         [chatTableView reloadData];
         
         NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         responseStr = [responseStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
       //  NSLog(@"Response Str : %@",responseStr);
         
         NSError *error = nil;
         id objectsFromJson = [NSJSONSerialization JSONObjectWithData:[responseStr dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&error];
         
         if (!error) {
            // NSLog(@"Success");
             //> NSLog(@"Object from Json : %@",objectsFromJson);
             
             if ([[objectsFromJson allKeys] containsObject:@"ResponseMsg"]) {
                 if ([[objectsFromJson valueForKey:@"ResponseMsg"] caseInsensitiveCompare:@"Success"] == NSOrderedSame)
                 {
                     
                     
                     [self isUploadingMsgId:msgId success:@"2"];
                     
                     
                     
                 }
                 else {
                     [self isUploadingMsgId:msgId success:@"3"];
                 }
                 
                 
             }
         }
         else {
        //     NSLog(@"Error : %@",error);
             [self isUploadingMsgId:msgId success:@"3"];
             
             [[[UIAlertView alloc] initWithTitle:ALERTVIEW_TITLE message:@"\u26A0 Invalid response from server." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
         }
         [self.singleton.mediaFileIndicatorDict removeObjectForKey:msgId];
         [chatTableView reloadData];
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         
         [self isUploadingMsgId:msgId success:@"3"];
         [self.singleton.mediaFileIndicatorDict removeObjectForKey:msgId];
         [chatTableView reloadData];
         
         
      //   NSLog(@"error : %@",  operation.responseString);
         
         [[[UIAlertView alloc] initWithTitle:ALERTVIEW_TITLE message:@"\u26A0 Invalid response from server." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
     }];
    [operation start];
}

-(void)isNotDownloaded:(NSString*)msgId success:(NSString*)success servicePath:(NSString *)serviceFilePath
{
    mediaDbEditor* mediaDb = [mediaDbEditor new];
    
    NSMutableDictionary* dict = [NSMutableDictionary new];
    [dict setObject:msgId forKey:@"msgId"];
    [dict setObject:success forKey:@"success_failure"];
    [dict setObject:serviceFilePath forKey:@"url"];
    [mediaDb checkAndInsert:dict];
    
}

-(void)UploadBtn_CLICKED:(id)sender event:(id)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPos = [touch locationInView:chatTableView];
    NSIndexPath *indexPath = [chatTableView  indexPathForRowAtPoint:touchPos];
    if(indexPath != nil){
        UIBubbleTableViewCell *swipedCell  = (UIBubbleTableViewCell*)[chatTableView cellForRowAtIndexPath:indexPath];
//        NSLog(@"%@",swipedCell.data.view);
//        NSLog(@"%@",swipedCell.data.jidStr);
//        NSLog(@"%@",swipedCell.data.date);
//        NSLog(@"%@",swipedCell.data.msgID);
        
        mediaDbEditor* mediaDb = [mediaDbEditor new];
        NSMutableDictionary* dict = [mediaDb fetchMedia:swipedCell.data.msgID];
        
        [self postPhoto:[dict objectForKey:@"url"]:swipedCell.data.msgID fileType:[dict objectForKey:@"video_img"]];
        
    }
    
    
    
    
}



#pragma mark QB Popup Menu

-(void)longPressText:(UILongPressGestureRecognizer *)gesture
{
    if (gesture.view.tag == 110)
    {
        if([menu respondsToSelector:@selector(showMenuUponActivationOfGetsure:)])
        {
            [menu showMenuUponActivationOfGetsure:gesture];
        }
    }
    else
        

    {
    
    if (gesture.state == UIGestureRecognizerStateBegan)
    {
        [self.popupMenu_media dismissAnimated:NO];
        [self.popupMenu_txt dismissAnimated:NO];
        [self.popupMenu_sticker dismissAnimated:NO];
        
        CGPoint location = [gesture locationInView:chatTableView];
        
        XMPPMessageArchiving_Message_CoreDataObject *coremessage = [[self fetchedResultsController ] objectAtIndexPath:[chatTableView indexPathForRowAtPoint:location]];
        
        EditMessageId = coremessage.messageID;
        
        CGPoint location2 = [gesture locationInView:self.view];
        CGPoint location3 = [gesture locationInView:gesture.view];
        
        NSXMLElement *file = [coremessage.message elementForName:@"file"];
        if (file) {
            [self showMenu_media:CGPointMake(location2.x, location2.y - location3.y)];
           
            
        }
       
        else if([beeFullArray containsObject:coremessage.body])
        {
            [self showMenu_sticker:CGPointMake(location2.x, location2.y - location3.y)];

        }
        else
        {
            [self showMenu_txt:CGPointMake(location2.x, location2.y - location3.y)];
        }
    }
    }
    
}

-(NSArray *)getMemberlistForTag
{
    XMPPRoomCoreDataStorage *storage = [XMPPRoomCoreDataStorage sharedInstance];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"roomJIDStr == %@", user];
    NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:storage.occupantEntityName
                                                         inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    request.predicate = predicate;
    [request setEntity:entityDescription];
    request.resultType = NSDictionaryResultType;
    request.propertiesToFetch = [NSArray arrayWithObjects:[[entityDescription propertiesByName] objectForKey:@"nickname"],[[entityDescription propertiesByName] objectForKey:@"affiliation"],nil];
    request.returnsDistinctResults = YES;
    
    NSMutableArray *memberAry =[NSMutableArray new];
    // Now it should yield an NSArray of distinct values in dictionaries.
    NSArray *dictionaries = [moc executeFetchRequest:request error:nil];
    for (NSMutableDictionary* element in dictionaries)
    {
        
        if (![[element objectForKey:@"nickname"] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:USERNAME]])
        {
            [memberAry addObject:[element objectForKey:@"nickname"]];
        }
    }
    
    return memberAry;
    
}




-(void)createPopUpForTag:(NSArray *)MemeberList
{
    
    NSMutableArray *items = [NSMutableArray new];

    for (int i = 0; i< MemeberList.count; i++)
    {
        QBPopupMenuItem *item = [QBPopupMenuItem itemWithTitle:[MemeberList objectAtIndex:i] target:self action:@selector(selectedMember:)];
        [items addObject:item];

    }

        QBPopupMenu *popupMenu = [[QBPopupMenu alloc] initWithItems:items];
        popupMenu.highlightedColor = [[UIColor blackColor] colorWithAlphaComponent:1];
        popupMenu.arrowDirection = QBPopupMenuArrowDirectionDown;
        self.popupMenu_Tag = popupMenu;
    
}

-(void)selectedMember:(QBPopupMenuItem *)sender
{

    NSLog(@"Selecte Member is%@",sender.title);
}

-(void)createPopUpforText
{
    if (isPrivateHistoryVw)
    {
        // QBPopupMenuItem *item2 = [QBPopupMenuItem itemWithTitle:@"Secure" target:self action:@selector(SecureMethod)];
        QBPopupMenuItem *item3 = [QBPopupMenuItem itemWithTitle:@"Copy" target:self action:@selector(copyStringMethod)];
        QBPopupMenuItem *item4 = [QBPopupMenuItem itemWithTitle:@"Delete" target:self action:@selector(deleteMethod)];
     //   QBPopupMenuItem *item5 = [QBPopupMenuItem itemWithTitle:@"UnSecure" target:self action:@selector(makeTextUnSecure)];
        
        //QBPopupMenuItem *item6 = [QBPopupMenuItem itemWithImage:[UIImage imageNamed:@"test"] target:self action:@selector(action)];
        NSArray *items = @[ item3, item4];
        
        QBPopupMenu *popupMenu = [[QBPopupMenu alloc] initWithItems:items];
        popupMenu.highlightedColor = [[UIColor blackColor] colorWithAlphaComponent:1];
        self.popupMenu_txt = popupMenu;
    }
    else
    {
        // QBPopupMenuItem *item2 = [QBPopupMenuItem itemWithTitle:@"Secure" target:self action:@selector(SecureMethod)];
        QBPopupMenuItem *item3 = [QBPopupMenuItem itemWithTitle:@"Copy" target:self action:@selector(copyStringMethod)];
        QBPopupMenuItem *item4 = [QBPopupMenuItem itemWithTitle:@"Delete" target:self action:@selector(deleteMethod)];
    //    QBPopupMenuItem *item5 = [QBPopupMenuItem itemWithTitle:@"Secure" target:self action:@selector(makeTextSecure)];
        
        //QBPopupMenuItem *item6 = [QBPopupMenuItem itemWithImage:[UIImage imageNamed:@"test"] target:self action:@selector(action)];
        NSArray *items = @[ item3, item4];
        
        QBPopupMenu *popupMenu = [[QBPopupMenu alloc] initWithItems:items];
        popupMenu.highlightedColor = [[UIColor blackColor] colorWithAlphaComponent:1];
        self.popupMenu_txt = popupMenu;
        
    }
    
}
-(void)createPopUpforMedia
{
    
    if (isPrivateHistoryVw)
    {
        
        QBPopupMenuItem *item2 = [QBPopupMenuItem itemWithTitle:@"UnSecure" target:self action:@selector(makeImageUnSecure)];
        QBPopupMenuItem *item4 = [QBPopupMenuItem itemWithTitle:@"Delete" target:self action:@selector(deleteMethod)];
        NSArray *items = @[ item2, item4];
        
        QBPopupMenu *popupMenu = [[QBPopupMenu alloc] initWithItems:items];
        popupMenu.highlightedColor = [[UIColor blackColor] colorWithAlphaComponent:1];
        self.popupMenu_media = popupMenu;
    }
    else
    {
        QBPopupMenuItem *item2 = [QBPopupMenuItem itemWithTitle:@"Secure" target:self action:@selector(SecureMethod)];
        QBPopupMenuItem *item4 = [QBPopupMenuItem itemWithTitle:@"Delete" target:self action:@selector(deleteMethod)];
        NSArray *items = @[ item2, item4];
        
        QBPopupMenu *popupMenu = [[QBPopupMenu alloc] initWithItems:items];
        popupMenu.highlightedColor = [[UIColor blackColor] colorWithAlphaComponent:1];
        self.popupMenu_media = popupMenu;
    }
    
    
    
}


-(void)createPopUpforSticker
{
    self.popupMenu_sticker= nil;
    QBPopupMenuItem *item4 = [QBPopupMenuItem itemWithTitle:@"Delete" target:self action:@selector(deleteMethod)];
    NSArray *items = @[item4];
    
    QBPopupMenu *popupMenu = [[QBPopupMenu alloc] initWithItems:items];
    popupMenu.highlightedColor = [[UIColor blackColor] colorWithAlphaComponent:1];
    self.popupMenu_sticker = popupMenu;
    
}

-(void) showMenu_txt:(CGPoint )obj{
    [self.popupMenu_txt showInView:self.view targetRect:CGRectMake(obj.x, obj.y, 10, 10) animated:YES];
}

-(void) showMenu_media:(CGPoint )obj{
    [self.popupMenu_media showInView:self.view targetRect:CGRectMake(obj.x, obj.y, 10, 10) animated:YES];
}
-(void) showMenu_sticker:(CGPoint )obj{
    [self.popupMenu_sticker showInView:self.view targetRect:CGRectMake(obj.x, obj.y, 10, 10) animated:YES];
}

-(void)copyStringMethod
{
    XMPPMessageArchiving_Message_CoreDataObject *coremessage = [self MsgWithID:EditMessageId];
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    NSData *newdata=[coremessage.body dataUsingEncoding:NSUTF8StringEncoding
                                   allowLossyConversion:YES];
    pasteboard.string =[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];
    
}


-(void)SecureMethod
{
    NSMutableDictionary *mediaDict =  [[mediaDbEditor sharedInstance] fetchMedia:EditMessageId];
    
    NSLog(@"%@",mediaDict);
    
    if (mediaDict) {
        
        if ([[mediaDict objectForKey:@"success_failure"] isEqualToString:@"0"] || [[mediaDict objectForKey:@"success_failure"] isEqualToString:@"3"] )
        {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"OOPS!" message:@"Please download the media." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else
        {
            
            XMPPMessageArchiving_Message_CoreDataObject *coremessage = [self MsgWithID:EditMessageId];
            coremessage.isPrivate = YES;
            coremessage.privateStatus = YES;
            
            
            mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
            NSMutableDictionary* dict = [NSMutableDictionary new];
            [dict setObject:EditMessageId  forKey:@"msgId"];
            [dict setObject:[NSNumber numberWithBool:YES] forKey:@"isPrivate"];
            [mediaDb updateMedia:dict];
            
            NSManagedObjectContext *context= [self managedObjecMessageContact];
            NSError *error = nil;
            
            if (![context save:&error])
            {
                //  NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
            }
            
            
            
            
        }
        
    }
}

-(void)makeTextSecure
{
    
    XMPPMessageArchiving_Message_CoreDataObject *coremessage = [self MsgWithID:EditMessageId];
    coremessage.isPrivate = YES;
    coremessage.privateStatus = YES;
    
    NSManagedObjectContext *context= [self managedObjecMessageContact];
    
      

    
    
    NSError *error = nil;
    
    if (![context save:&error])
    {
      //  NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
    }
    
}



-(void)makeTextUnSecure
{
    
    XMPPMessageArchiving_Message_CoreDataObject *coremessage = [self MsgWithID:EditMessageId];
    coremessage.isPrivate = NO;
    coremessage.privateStatus = NO;
    
    NSManagedObjectContext *context= [self managedObjecMessageContact];
    
    
    NSError *error = nil;
    
    if (![context save:&error])
    {
     //   NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
    }
    
}


-(void)makeImageUnSecure
{
    XMPPMessageArchiving_Message_CoreDataObject *coremessage = [self MsgWithID:EditMessageId];
    coremessage.isPrivate = NO;
    coremessage.privateStatus = NO;
    
    mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
    NSMutableDictionary* dict = [NSMutableDictionary new];
    [dict setObject:EditMessageId  forKey:@"msgId"];
    [dict setObject:[NSNumber numberWithBool:NO] forKey:@"isPrivate"];
    [mediaDb updateMedia:dict];
    
    NSManagedObjectContext *context= [self managedObjecMessageContact];
    NSError *error = nil;
    
    if (![context save:&error])
    {
    //    NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
    }
}



-(void)deleteMethod
{
    shouldScrollToBottom = NO;
    XMPPMessageArchiving_Message_CoreDataObject *coremessage = [self MsgWithID:EditMessageId];
    
    NSManagedObjectContext *context= [self managedObjecMessageContact];
    
    NSXMLElement *file = [coremessage.message elementForName:@"file"];
    
    if (file)
    {
        mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
        [mediaDb deleteMedia:coremessage.messageID];
    }
    

    
    [context deleteObject:coremessage];
    
    NSError *error = nil;
    
    if (![context save:&error])
    {
      //  NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
    }
    
    [[self xmppMessageArchivingCoreDataStorage]updateMessageOnRecentChat:self.user.bare];

    
    
}


#pragma mark M13 ContectMenu


-(void)createM13ContectMenu
{
    //Create the items
    M13ContextMenuItemIOS7 *bookmarkItem = [[M13ContextMenuItemIOS7 alloc] initWithUnselectedIcon:[UIImage imageNamed:@"BookmarkIcon"] selectedIcon:[UIImage imageNamed:@"BookmarkIconSelected"]];
    M13ContextMenuItemIOS7 *uploadItem = [[M13ContextMenuItemIOS7 alloc] initWithUnselectedIcon:[UIImage imageNamed:@"UploadIcon"] selectedIcon:[UIImage imageNamed:@"UploadIconSelected"]];
    M13ContextMenuItemIOS7 *trashIcon = [[M13ContextMenuItemIOS7 alloc] initWithUnselectedIcon:[UIImage imageNamed:@"TrashIcon"] selectedIcon:[UIImage imageNamed:@"TrashIconSelected"]];
	//Create the menu
    menu = [[M13ContextMenu alloc] initWithMenuItems:@[trashIcon,bookmarkItem,uploadItem]];
    menu.delegate = self;
    
    //Create the gesture recognizer
    longPress_M13ContextMenu = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressText:)];
    longPress_M13ContextMenu.minimumPressDuration = 0.5;
    [self.view setUserInteractionEnabled:YES];
    self.view.tag = 110;
    self.view.exclusiveTouch = YES;
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:longPress_M13ContextMenu];
}



- (BOOL)shouldShowContextMenu:(M13ContextMenu *)contextMenu atPoint:(CGPoint)point
{
    [self resignkeyBoard];
    return YES;
}

- (void)contextMenu:(M13ContextMenu *)contextMenu atPoint:(CGPoint)point didSelectItemAtIndex:(NSInteger)index
{
    if (index == 0) {
//        isPrivateHistoryVw = !isPrivateHistoryVw;
//        
//        if (isPrivateHistoryVw)
//        {
//            menu.shouldShow = NO;
//
//            passwordVw = [[PasswordPin alloc]initWithFrame_onView:self.view LabelString:@"Type your pin" password:@"1234"];
//            [passwordVw createPasswordVw];
//        }
//        else
//        {
//            
//            fetchedResultsController.delegate=nil;
//            fetchedResultsController = nil;
//            [UIView transitionWithView: chatTableView
//                              duration: 0.35f
//                               options: UIViewAnimationOptionTransitionCrossDissolve
//                            animations: ^(void)
//             {
//                 [chatTableView reloadData];
//             }
//                            completion: ^(BOOL isFinished)
//             {
//             }];
//        }
//
        
        menu.shouldShow = NO;
        
        popUp =[[ImpMsgPopUp alloc]initWithFrame:CGRectMake(0,0,chatTableView.frame.size.width, self.view.frame.size.height) view:self.view bareJIDStr:user.bare];
        [popUp setBackground:self.view];
        
        
        popUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
        
        [self.view addSubview:popUp];
        
        [UIView animateWithDuration:0.3/1.5 animations:^{
            popUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/2 animations:^{
                popUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.3/2 animations:^{
                    popUp.transform = CGAffineTransformIdentity;
                    showImpMsgLy = YES;
                }];
            }];
        }];
        

        
    }
    else if (index == 1)
    {
        [self showPrivateVw];
        
        
    }
    else {
        [[self xmppMessageArchivingCoreDataStorage] updateUnreadMessage:self.user clearValue:YES];
        
    }
    
}
//private Password Notification
-(void)privatePassword:(NSNotification *)notification
{
    menu.shouldShow = YES;
    
    [self createPopUpforText];
    [self createPopUpforMedia];
    [passwordVw removeFromSuperview];
    passwordVw = nil;
    
    NSDictionary *dict = [notification userInfo];
    
    if ([[dict objectForKey:@"Mode"] isEqualToString:SET_PRIVATE_PWD])
    {
        if ([[dict objectForKey:@"isCorrect"] isEqualToString:@"YES"])
        {
            menu.shouldShow = NO;
            passwordVw = [[PasswordPin alloc]initWithFrame_onView:self.view LabelString:@"Confirm your Master Unlock Key " password:@"" Hint:CONFRM_PRIVATE_PWD JID:self.xmppRoom.roomJID.bare];
            [passwordVw createPasswordVw];
            
            
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:MASTER_PIN];
            
            isPrivateHistoryVw = !isPrivateHistoryVw;
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
            
        }
        
    }
    else if ([[dict objectForKey:@"Mode"] isEqualToString:CONFRM_PRIVATE_PWD])
    {
        if ([[dict objectForKey:@"isCorrect"] isEqualToString:@"YES"])
        {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
            
            
            fetchedResultsController.delegate=nil;
            fetchedResultsController = nil;
            [UIView transitionWithView: chatTableView
                              duration: 0.35f
                               options: UIViewAnimationOptionTransitionCrossDissolve
                            animations: ^(void)
             {
                 [chatTableView reloadData];
                 
                 
             }
                            completion: ^(BOOL isFinished)
             {
             }];
            
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:MASTER_PIN];
            
            isPrivateHistoryVw = !isPrivateHistoryVw;
        }
        
        
    }else if ([[dict objectForKey:@"Mode"] isEqualToString:ENTER_PRIVATE_PWD])
    {
        if ([[dict objectForKey:@"isCorrect"] isEqualToString:@"YES"])
        {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
            
            
            fetchedResultsController.delegate=nil;
            fetchedResultsController = nil;
            [UIView transitionWithView: chatTableView
                              duration: 0.35f
                               options: UIViewAnimationOptionTransitionCrossDissolve
                            animations: ^(void)
             {
                 [chatTableView reloadData];
             }
                            completion: ^(BOOL isFinished)
             {
             }];
            
        }
        else
        {
            isPrivateHistoryVw = !isPrivateHistoryVw;
        }
        
        
    }
}

-(void)removePrivateChatView
{
   
    isPrivateChat = NO;
        [UIView animateWithDuration:0.3
                              delay:0
                            options:UIViewAnimationCurveEaseInOut
                         animations:^{
                             privateVw.frame =  CGRectMake(privateVw.frame.origin.x, -privateVw.frame.size.height, privateVw.frame.size.width, privateVw.frame.size.height);
                             
                         }
                         completion:^(BOOL finished){
                             [privateVw removeFromSuperview];
                             privateVw = nil;
                             
                         }];
        
   
    
}

-(void)showPrivateVw
{
   
    if (!isPrivateChat)
    {
        privateVw = [self createPrivateChatView_withString:[NSString stringWithFormat:@"On private"] status:@"not_request"];
        [self.view addSubview:privateVw];
        [self pushPrivateVwDown:privateVw];
        isPrivateChat = YES;
    }
    else
    {
        [self removePrivateChatView];
    }
    
}
-(UIView*)createPrivateChatView_withString:(NSString*)status_msg status:(NSString*)status
{
    UIView* private_Vw = [UIView new];
    
    
    
    UIView* privateStatusView = [UIView new];
    UIButton* cancelBtn;
    UIButton* acceptBtn;
    UIButton* denyBtn;
    UIButton* notNowBtn;
    
    UILabel* statusLbl = [UILabel new];
    
    
    
    
    UIFont *myFont = [UIFont fontWithName:FontString size:18];
    CGSize timeSize;
    if ([status isEqualToString:@"not_request"])
    {
        timeSize = [self frameForText:status_msg sizeWithFont:myFont constrainedToSize:CGSizeMake(self.view.frame.size.width - (2*34), 30) lineBreakMode:statusLbl.lineBreakMode];
    }
    if ([status isEqualToString:@"request"])
    {
        timeSize = [self frameForText:status_msg sizeWithFont:myFont constrainedToSize:CGSizeMake(self.view.frame.size.width , 30) lineBreakMode:statusLbl.lineBreakMode];
    }
    
    statusLbl.frame = CGRectMake((self.view.frame.size.width - timeSize.width)/2, 0, timeSize.width, 30);
    statusLbl.text = status_msg;
    statusLbl.font = myFont;
    statusLbl.textAlignment = NSTextAlignmentCenter;
    statusLbl.textColor = [UIColor whiteColor];
    statusLbl.adjustsFontSizeToFitWidth = YES;
    [privateStatusView addSubview:statusLbl];
    
    if ([status isEqualToString:@"not_request"])
    {
        
        cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        cancelBtn.backgroundColor = RGBA(106, 106, 106, 1);
        [cancelBtn setTitle:@"X" forState:UIControlStateNormal];
        [cancelBtn setTintColor:[UIColor whiteColor]];
        cancelBtn.titleLabel.font = [UIFont fontWithName:FontString size:17];
        [cancelBtn addTarget:self action:@selector(cancelBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [privateStatusView addSubview:cancelBtn];
    }
    else if ([status isEqualToString:@"request"])
    {
        acceptBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        acceptBtn.frame = CGRectMake(1*20, statusLbl.frame.origin.y + statusLbl.frame.size.height, (self.view.frame.size.width - 80)/3, 25);
        [acceptBtn setTitle:@"Accept" forState:UIControlStateNormal];
        acceptBtn.titleLabel.font = [UIFont fontWithName:FontString size:15.0f];
        [acceptBtn setTintColor:[UIColor whiteColor]];
        acceptBtn.backgroundColor = RGBA(113,159,15, 1);
        acceptBtn.layer.cornerRadius = 25/2;
        //[acceptBtn addTarget:self action:@selector(acceptBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [privateStatusView addSubview:acceptBtn];
        
        denyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        denyBtn.frame = CGRectMake((2*20) + (1* ((self.view.frame.size.width - 80)/3)), statusLbl.frame.origin.y + statusLbl.frame.size.height, (self.view.frame.size.width - 80)/3, 25);
        [denyBtn setTitle:@"Deny" forState:UIControlStateNormal];
        denyBtn.titleLabel.font = [UIFont fontWithName:FontString size:15.0f];
        [denyBtn setTintColor:[UIColor whiteColor]];
        denyBtn.backgroundColor = RGBA(239,59,2, 1);
        denyBtn.layer.cornerRadius = 25/2;
       // [denyBtn addTarget:self action:@selector(denyBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [privateStatusView addSubview:denyBtn];
        
        notNowBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        notNowBtn.frame = CGRectMake((3*20) + (2* ((self.view.frame.size.width - 80)/3)), statusLbl.frame.origin.y + statusLbl.frame.size.height, (self.view.frame.size.width - 80)/3, 25);
        [notNowBtn setTitle:@"Not now" forState:UIControlStateNormal];
        notNowBtn.titleLabel.font = [UIFont fontWithName:FontString size:15.0f];
        [notNowBtn setTintColor:[UIColor whiteColor]];
        notNowBtn.backgroundColor = RGBA(234,106,3, 1);
        notNowBtn.layer.cornerRadius = 25/2;
       // [notNowBtn addTarget:self action:@selector(notNowBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [privateStatusView addSubview:notNowBtn];
        
    }
    
    int privateStatusViewHeight;
    privateStatusViewHeight = 30;
    
    
    
    if (acceptBtn)
    {
        privateStatusViewHeight += acceptBtn.frame.size.height + 5;
    }
    
    privateStatusView.frame = CGRectMake(0,  0 , self.view.frame.size.width, privateStatusViewHeight);
    privateStatusView.backgroundColor = RGBA(0, 0, 0, 0.7);
    
    if (cancelBtn) {
        cancelBtn.frame = CGRectMake( self.view.frame.size.width -(statusLbl.frame.size.height + 5), (privateStatusView.frame.size.height - (statusLbl.frame.size.height -5))/2 , statusLbl.frame.size.height - 5  , statusLbl.frame.size.height -5);
        cancelBtn.layer.cornerRadius = cancelBtn.frame.size.width/2;
        
    }
    
    if (acceptBtn) {
        UIImage *hideImage = [UIImage imageNamed:@"privateHide"];
        
        UIButton* hideBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        hideBtn.frame = CGRectMake(0, 0, hideImage.size.width, hideImage.size.height);
        hideBtn.backgroundColor = [UIColor clearColor];
        [hideBtn setImage:hideImage forState:UIControlStateNormal];
       // [hideBtn addTarget:self action:@selector(hideBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [private_Vw addSubview:hideBtn];
        hideImage = nil;
        
        private_Vw.frame = CGRectMake(-hideBtn.frame.size.width, -privateStatusView.frame.size.height, self.view.frame.size.width + hideBtn.frame.size.width,privateStatusViewHeight);
        privateStatusView.frame = CGRectMake(hideBtn.frame.size.width, 0, privateStatusView.frame.size.width, privateStatusView.frame.size.height);
        
        
    }
    else
    {
        private_Vw.frame = CGRectMake(0, -privateStatusView.frame.size.height, self.view.frame.size.width ,privateStatusViewHeight);
        privateStatusView.frame = CGRectMake(0, 0, privateStatusView.frame.size.width, privateStatusView.frame.size.height);
    }
    [private_Vw addSubview:privateStatusView];
    private_Vw.backgroundColor = [UIColor clearColor];
    
    return private_Vw;
}

-(CGSize)frameForText:(NSString*)text sizeWithFont:(UIFont*)font constrainedToSize:(CGSize)size lineBreakMode:(NSLineBreakMode)lineBreakMode  {
    
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.lineBreakMode = lineBreakMode;
    
    NSDictionary * attributes = @{NSFontAttributeName:font,
                                  NSParagraphStyleAttributeName:paragraphStyle
                                  };
    
    
    CGRect textRect = [text boundingRectWithSize:size
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:attributes
                                         context:nil];
    
    return textRect.size;
}

-(void)pushPrivateVwDown:(UIView*)privVw
{
    
    [UIView animateWithDuration:0.25 animations:^{
        privVw.frame =  CGRectMake(self.view.frame.size.width - privVw.frame.size.width , 0, privVw.frame.size.width, privVw.frame.size.height);
    }];
}


-(void)cancelBtnAction:(id)sender
{
    
    [self removePrivateChatView];
}

#pragma mark edit Input View


-(void)editInputVw_Create
{
    editInputView = [UIView new];
    editInputView.hidden = YES;
    editInputView.frame = CGRectMake(0, self.view.frame.size.height - 50, self.view.frame.size.width, 50);
    editInputView.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.5];
    [self.view addSubview:editInputView];
    
    UIButton* deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    deleteBtn.frame = CGRectMake(editInputView.frame.size.width - 100, (editInputView.frame.size.height - 30)/2, 80, 30);
    [deleteBtn setTitle:@"Delete" forState:UIControlStateNormal];
    [deleteBtn setTintColor:[UIColor whiteColor]];
    [deleteBtn addTarget:self action:@selector(deleteBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    deleteBtn.backgroundColor = RGBA(46, 141, 214, 1);
    deleteBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    deleteBtn.layer.cornerRadius = 0.5;
    [editInputView addSubview:deleteBtn];
    
    
    
    UIButton* clearAllBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    clearAllBtn.frame = CGRectMake(self.view.frame.origin.x + 30, (editInputView.frame.size.height - 30)/2, 80, 30);
    [clearAllBtn setTitle:@"Clear All" forState:UIControlStateNormal];
    [clearAllBtn setTintColor:[UIColor whiteColor]];
    [clearAllBtn addTarget:self action:@selector(clearAllBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    clearAllBtn.backgroundColor = RGBA(46, 141, 214, 1);
    clearAllBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    clearAllBtn.layer.cornerRadius = 0.5;
    [editInputView addSubview:clearAllBtn];
    
}


-(void)deleteBtnClicked
{
    for (NSString* msgId in deleteMsgIdAry) {
        shouldScrollToBottom = NO;
        XMPPMessageArchiving_Message_CoreDataObject *coremessage = [self MsgWithID:msgId];
        
        NSManagedObjectContext *context= [self managedObjecMessageContact];
        
        NSXMLElement *file = [coremessage.message elementForName:@"file"];
        
        if (file)
        {
            mediaDbEditor* mediaDb = [mediaDbEditor sharedInstance];
            [mediaDb deleteMedia:coremessage.messageID];
        }

        
        [context deleteObject:coremessage];
        
        NSError *error = nil;
        
        if (![context save:&error])
        {
          //  NSLog(@"Sorry, couldn't delete values %@", [error localizedDescription]);
        }
        
    }
    
    [[self xmppMessageArchivingCoreDataStorage]updateMessageOnRecentChat:self.user.bare];
    
    [deleteMsgIdAry removeAllObjects];
}

-(void)clearAllBtnClicked
{
    [[self xmppMessageArchivingCoreDataStorage]clearChatWithID:[NSString stringWithFormat:@"%@",user.bare]];
    XMPPRoomCoreDataStorage *storage = [XMPPRoomCoreDataStorage sharedInstance];
    [storage resumeOldMessageDeletionForRoom:user];
    [storage groupunreadedMsg:user clearValue:YES];
}
-(void)changeViewToEditMode
{
    
    [UIView transitionWithView:chatTableView
                      duration:0.5f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        isEditMode = YES;
                        
                        [deleteMsgIdAry removeAllObjects];
                        
                        [chatTableView reloadData];
                        
                        //to be hidden
                        backButton.hidden = YES;
                        menuButton.hidden = YES;
                        inputView.hidden = YES;
                        deleteDoneBtn.hidden = NO;
                        
                        menu.shouldShow = NO;
                        bubble.hidden = YES;

                        
                        editInputView.hidden = NO;
                    } completion:NULL];

    
    
//    [UIView transitionWithView:self.view
//                      duration:0.5f
//                       options:UIViewAnimationOptionTransitionCrossDissolve
//                    animations:^(void) {
//                        
//                        bubble.hidden = YES;
//                        
//                        
//                    } completion:NULL];
    
    
    
}

-(void)changeViewToNormalMode
{
    [UIView transitionWithView:chatTableView
                      duration:0.5f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        isEditMode = NO;
                        [deleteMsgIdAry removeAllObjects];
                        [chatTableView reloadData];
                        
                        //to be hidden
                        bubble.hidden = NO;

                        backButton.hidden = NO;
                        menuButton.hidden = NO;
                        inputView.hidden = NO;
                        deleteDoneBtn.hidden  = YES;
                        menu.shouldShow = YES;
                        editInputView.hidden = YES;
                    } completion:NULL];
    
//    [UIView transitionWithView:self.view
//                      duration:0.5f
//                       options:UIViewAnimationOptionTransitionCrossDissolve
//                    animations:^(void) {
//                       
//                        bubble.hidden = NO;
//                        
//                       
//                    } completion:NULL];
    
    
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark BEE Life Cycle
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)createBeeBoard:(NSArray*)beeAry
{
    beeCollVw = nil;
    beeCollVw = [[BeeCollectionView alloc] initWithFrame:CGRectMake(5, self.view.frame.size.height, self.view.frame.size.width - 10, 64)imgAry:beeAry];
    beeCollVw.BeeCollectionDelegate  =self;
    beeCollVw.userInteractionEnabled = YES;
    UIImage *tintImg = [self tintImage:[UIImage imageNamed:@"popBg"] withColor:[UIColor whiteColor]];
    
    beeCollVw.backgroundColor = [UIColor clearColor];
    beeCollVw.bgIMgVw.image = tintImg;
    [beeCollVw setAlpha:0.0f];
    [self.view addSubview:beeCollVw];
    
    beeCollVw.frame = CGRectMake(0, self.view.frame.size.height, simleyBtn.frame.size.width, beeCollVw.frame.size.height);

    
}
-(void)beeBtnClicked
{
    
    
    
    [UIView animateWithDuration:0.2
                          delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         if (beeCollVw.frame.origin.y > self.view.frame.size.height- 64) {
                             
                              beeCollVw.frame = CGRectMake(0,  inputView.frame.origin.y - beeCollVw.frame.size.height , self.view.frame.size.width, beeCollVw.frame.size.height);
                             [beeCollVw setAlpha:1.0f];

                         }
                         else
                         {
                             
                             beeCollVw.frame = CGRectMake(0, self.view.frame.size.height, simleyBtn.frame.size.width, beeCollVw.frame.size.height);
                             [beeCollVw setAlpha:0.0f];

                         }
                         
                     }
                     completion:^(BOOL finished){
                                             }];
}

-(void)closeBee_Board
{
    
    [UIView animateWithDuration:0.2
                          delay:0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         [beeCollVw setAlpha:0.0f];
                         beeCollVw.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, beeCollVw.frame.size.height);
                         
                     }
                     completion:^(BOOL finished){
                         
                     }];
    
}


-(void)tapGestureState_xpoint:(int)xpoint ypoint:(int)ypoint index:(long)index
{
    [self closeBee_Board];
    [self SendBeeMsg:[NSString stringWithFormat:@"%@",[beeFullArray objectAtIndex:index]]];
    
}




- (UIImage *)tintImage:(UIImage *)image withColor:(UIColor *)color
{
    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, image.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    CGContextClipToMask(context, rect, image.CGImage);
    [color setFill];
    CGContextFillRect(context, rect);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Action for sending Sticker
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(void)SendBeeMsg:(NSString*)beeStr
{
      NSString *messageStr = beeStr;
    
    
    if([messageStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length > 0) {
        
        NSString *messageID=[self.xmppStream generateUUID];
        
        NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
        [body setStringValue:messageStr];
        NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
        [message addAttributeWithName:@"id" stringValue:messageID];
        [message addAttributeWithName:@"type" stringValue:@"groupchat"];
        [message addAttributeWithName:@"to" stringValue:[NSString stringWithFormat:@"%@",user]];
        [message addChild:body];
		
        if (isPrivateChat) {
            NSXMLElement *private = [NSXMLElement elementWithName:@"private"];
            [private addAttributeWithName:@"xmlns" stringValue:@"private"];
            [private addAttributeWithName:@"type" stringValue:@"message"];
            [message addChild:private];
        }
        [self.xmppStream sendElement:message];
        self.chatTextview.text = @"";
        
    }
    
}


-(void)galleryButtonAction:(id)sender
{
    [self openPhotoAlbum];
}


-(void)cameraButtonAction:(id)sender
{
    [self takePhoto];
}




#pragma mark Bubble Menu

-(void)createBubbleMenu
{
    if (bubble) {
        [bubble removeFromSuperview];
        bubble  = nil;
    }
    bubble = [bubbleVw sharedInstance:CGRectMake(100, 100, 30, 30) size:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)  ];
    UIImage* btnimg = [UIImage imageNamed:@"bubble"];
    UIImage* groupIcon = [avatarImg groupProfileimage:self.user];

    UIImage *bubbleMaskImg = [self maskImage:groupIcon withMask:btnimg];

//    [bubble setImage:bubbleMaskImg forState:UIControlStateNormal];
//    [bubble setImage:bubbleMaskImg forState:UIControlStateSelected];
//    [bubble setImage:bubbleMaskImg forState:UIControlStateSelected | UIControlStateHighlighted];
    [bubble.layer setCornerRadius:btnimg.size.width /2];

    [self.view addSubview:bubble];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bubbleStatus:) name:@"bubbleStatus" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(menuBtnClicked:) name:@"menuBtnClicked" object:nil];
    
}

- (UIImage*) maskImage:(UIImage *)image withMask:(UIImage *)maskImage {
    
    CGImageRef maskRef = maskImage.CGImage;
    
    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                        CGImageGetHeight(maskRef),
                                        CGImageGetBitsPerComponent(maskRef),
                                        CGImageGetBitsPerPixel(maskRef),
                                        CGImageGetBytesPerRow(maskRef),
                                        CGImageGetDataProvider(maskRef), NULL, false);
    
    CGImageRef masked = CGImageCreateWithMask([image CGImage], mask);
    return [UIImage imageWithCGImage:masked];
}



//private Password Notification
-(void)bubbleStatus:(NSNotification *)notification
{
    
    NSDictionary *dict = [notification userInfo];
    
    if ([[dict objectForKey:@"bubbleStatus"] isEqualToString:@"Clicked"])
    {
        [self resignkeyBoard];
        [UIView transitionWithView:menuVw
                          duration:0.3
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            menuVw.hidden = NO;
                            [menuVw open];
                            [menuVw setImage:bubble.imageView.image];

                        } completion:^(BOOL finished){
                        }];
        
    }
}

//menuBtnClicked Notification
-(void)menuBtnClicked:(NSNotification *)notification
{
    
    NSDictionary *dict = [notification userInfo];
    [bubble restoreBtn];

    [UIView transitionWithView:menuVw
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        menuVw.hidden = YES;
                        [menuVw _close:nil];
                        
                        
                        
                    } completion:^(BOOL finished){
                    }];
    if ([[dict objectForKey:@"menuBtnClicked"] isEqualToString:@"0"])
    {
        [UIView transitionWithView:chatTableView
                          duration:0.5f
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^(void) {
                            self.chatSlideMenu.frame = CGRectMake(self.view.frame.size.width+100, 0, 100, self.chatSlideMenu.frame.size.height);
                            [self changeViewToEditMode];
                        } completion:NULL];
        
        isMenuShowing = NO;
        
    }
    else if ([[dict objectForKey:@"menuBtnClicked"] isEqualToString:@"1"])
    {
        [self openPhotoAlbum];
        
    }
    else if ([[dict objectForKey:@"menuBtnClicked"] isEqualToString:@"2"])
    {
        [self takePhoto];
        
    }
    else if ([[dict objectForKey:@"menuBtnClicked"] isEqualToString:@"3"])
    {
        [self takevideo];
        
    }
    else if ([[dict objectForKey:@"menuBtnClicked"] isEqualToString:@"4"])
    {
        [self getMemberlist];
        
    }
    
    
}

#pragma mark - Action for clicking title to show group information
-(void)pushtoGroupInformation
{
    
    if (!isEditMode)
    {
    if(!self.groupInformation)
       self.groupInformation = [GroupInformationViewController new];
   self.groupInformation.roomJID =  user;
   self.groupInformation.titleString = self.titleLabel.titleLabel.text;
   [self.navigationController pushViewController:self.groupInformation animated:YES];
        
    }
    
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    UIImage *backButtonImage = [UIImage imageNamed:@"back_btn.png"];
    imagePickerBackButton = [UIButton buttonWithType:UIButtonTypeCustom];
    imagePickerBackButton.frame = CGRectMake(0, 0, backButtonImage.size.width, backButtonImage.size.height);
    [imagePickerBackButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
    [imagePickerBackButton addTarget:self action:@selector(popCurrentViewController) forControlEvents:UIControlEventTouchUpInside];
    [viewController.navigationController.navigationBar addSubview:imagePickerBackButton];
    viewController.navigationItem.hidesBackButton = YES;
    
    if ([NSStringFromClass([viewController class]) isEqualToString:@"PUUIAlbumListViewController" ])
        imagePickerBackButton.hidden = YES;
    else
        imagePickerBackButton.hidden = NO;
    
    imagePickerController = viewController;
}
-(void)popCurrentViewController
{
    [imagePickerBackButton removeFromSuperview];
    [imagePickerController.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [self.imageCache removeAllObjects];
    
  //  NSLog(@"RECEIVED MEMORY WARNINGS IN chatViewController");
    
}
@end
