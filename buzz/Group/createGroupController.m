//
//  createGroupController.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 05/03/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "createGroupController.h"
#import "PECropViewController.h"
#import "contactTableView.h"
#import "ContactCell.h"
#import "AvatarImage.h"
#import "DDLog.h"
#import "NSData+XMPP.h"
#import "GroupIconService.h"


// Log levels: off, error, warn, info, verbose
static const int ddLogLevel = LOG_LEVEL_VERBOSE;

@interface createGroupController ()<UITextFieldDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,PECropViewControllerDelegate,UINavigationControllerDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UIBarButtonItem *createBtn;
    UIButton *addBtn;
    UIView *contactView;
    AvatarImage *avatarImg;
    __strong id <XMPPRoomStorage> xmppRoomStorage;
    XMPPJID *chatRoomJID;
    
    BOOL isProfilePicChanged;
}
@property (nonatomic,strong)  XMPPRoom   * xmppRoom;;
@property(nonatomic,strong)UIImageView *groupImageView;
@property(nonatomic,strong)UITextField *groupNameTextFld;
@property(nonatomic,strong)contactTableView *contactTable;
@property(nonatomic,strong)UITableView *groupMemberTableView;

@end

@implementation createGroupController
@synthesize xmppRoom;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Appdelegate Values
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (AppDelegate *)appDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (XMPPStream *)xmppStream
{
    return [[self appDelegate] xmppStream];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(IS_IPHONE_4)
    {
        self.view.backgroundColor = BACKGROUNDIMAGE_4;
        
    }
    else
    {
        self.view.backgroundColor = BACKGROUNDIMAGE;
        
    }
    self.title = @"New Group";
    
    createBtn = [[UIBarButtonItem alloc] initWithTitle:@"Create" style:UIBarButtonItemStylePlain target:self action:@selector(createBtnAction)];
    self.navigationItem.rightBarButtonItem = createBtn;
    createBtn.enabled = NO;
    
    addBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.groupImageView = [UIImageView new];
    self.groupNameTextFld = [UITextField new];
    self.groupImageView.image = [UIImage imageNamed:@"profpic"];
    contactView = [UIView new];
    self.contactTable = [[contactTableView alloc]init];
    self.groupMemberTableView =  [UITableView new];
    avatarImg = [AvatarImage new];
    [[self xmppStream]  addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    isProfilePicChanged = NO;
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [[self appDelegate].statusBarView setHidden:NO];
    
    self.groupImageView.frame = CGRectMake((self.view.frame.size.width/2)-40, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height+30, 80, 80);
    self.groupImageView.layer.cornerRadius = 40;
    self.groupImageView.clipsToBounds = YES;
    self.groupImageView.userInteractionEnabled = YES;
    [self.view addSubview:self.groupImageView];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addImage)];
    singleTap.numberOfTapsRequired = 1;
    [self.groupImageView addGestureRecognizer:singleTap];
    
    _groupNameTextFld.frame = CGRectMake(30, self.groupImageView.frame.origin.y+self.groupImageView.frame.size.height+10, self.view.frame.size.width - 60, 40);
    _groupNameTextFld.borderStyle = UITextBorderStyleRoundedRect;
    _groupNameTextFld.font = [UIFont fontWithName:FontString size:15];
    _groupNameTextFld.placeholder = @"Enter the Group Subject";
    _groupNameTextFld.autocorrectionType = UITextAutocorrectionTypeNo;
    _groupNameTextFld.keyboardType = UIKeyboardTypeDefault;
    _groupNameTextFld.returnKeyType = UIReturnKeyDone;
    _groupNameTextFld.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _groupNameTextFld.delegate = self;
    [self.view addSubview:_groupNameTextFld];
    
    addBtn.frame = CGRectMake(_groupNameTextFld.frame.origin.x, _groupNameTextFld.frame.origin.y+_groupNameTextFld.frame.size.height, 200, 27);
    //   addBtn.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
    [addBtn setTitle:@"Add Group Participants" forState:UIControlStateNormal];
    addBtn.titleLabel.font = [UIFont fontWithName:FontString size:15.0f];
    [addBtn setTitleColor:RGBA(53, 152, 219, 1) forState:UIControlStateNormal];
    [addBtn addTarget:self action:@selector(showContactTableView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:addBtn];
    
    self.groupMemberTableView.frame = CGRectMake(0, addBtn.frame.origin.y+addBtn.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-(addBtn.frame.origin.y+addBtn.frame.size.height));
    self.groupMemberTableView.delegate = self;
    self.groupMemberTableView.dataSource = self;
    self.groupMemberTableView.backgroundColor = [UIColor clearColor];
    self.groupMemberTableView.separatorColor = [UIColor clearColor];
    self.groupMemberTableView.rowHeight = 50;
    self.groupMemberTableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:self.groupMemberTableView];
    
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Cancel Button Action
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)cancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Tapping Action in groupImageView
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)addImage
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:NSLocalizedString(@"Photo Album", nil), nil];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Camera", nil)];
    }
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    actionSheet.cancelButtonIndex = actionSheet.numberOfButtons - 1;
    [actionSheet showFromToolbar:self.navigationController.toolbar];
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Method to access Media
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)showCamera
{
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    
    [[self appDelegate].statusBarView setHidden:YES];
    
    controller.delegate = self;
    controller.sourceType = UIImagePickerControllerSourceTypeCamera;
    controller.cameraDevice = UIImagePickerControllerCameraDeviceFront;
    [self presentViewController:controller animated:YES completion:NULL];
}
- (void)openPhotoAlbum
{
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.delegate = self;
    controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:controller animated:YES completion:NULL];
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Action Sheet Delegate Method
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////-
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    if ([buttonTitle isEqualToString:NSLocalizedString(@"Photo Album", nil)]) {
        [self openPhotoAlbum];
    } else if ([buttonTitle isEqualToString:NSLocalizedString(@"Camera", nil)]) {
        [self showCamera];
    }
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Image Picker delegate Method
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        [self openEditor:image];
    }];
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Action for Editing Image
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)openEditor:(UIImage*)img
{
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.image = img;
    controller.cropAspectRatio = 1.0f;
    
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    
    [self presentViewController:navigationController animated:YES completion:NULL];
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark PECropViewControllerDelegate Methods
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    self.groupImageView.image =croppedImage;
    isProfilePicChanged = YES;
    
    
    
}
- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

-(void)showContactTableView:(id)sender
{
    [contactView removeFromSuperview];
    [self.groupNameTextFld resignFirstResponder];
    contactView.frame = CGRectMake(0, self.view.frame.size.height+10, self.view.frame.size.width, self.view.frame.size.height);
    contactView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg"]];
    [UIView animateWithDuration:0.25 animations:^{
        contactView.frame =  CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height, self.view.frame.size.width, self.view.frame.size.height - self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height);
        [self.view addSubview:contactView];
    }];
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(contactView.frame.size.width-70,5,70,45);
    doneBtn.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
    [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
    doneBtn.titleLabel.font = [UIFont fontWithName:FontString size:20.0f];
    [doneBtn setTitleColor:RGBA(53, 152, 219, 1) forState:UIControlStateNormal];
    [doneBtn addTarget:self action:@selector(doneBtnCliked:) forControlEvents:UIControlEventTouchUpInside];
    [contactView addSubview:doneBtn];
    
    self.contactTable.frame = CGRectMake(0, 50, self.view.frame.size.width, self.view.frame.size.height - 150);
    self.contactTable.viewControllerStr = @"createGroupController";
    [contactView addSubview:self.contactTable];
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - TableView delegate and DataSource Methods
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    if (self.contactTable.groupMemberArray.count != 0)
        return self.contactTable.groupMemberArray.count;
    else
        return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - TableView DataSource cellForRowAtIndexPath
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self respondsToSelector:@selector(setSectionIndexColor:)])
    {
        tableView.sectionIndexBackgroundColor = [UIColor clearColor];
        tableView.sectionIndexTrackingBackgroundColor = [UIColor whiteColor];
    }
    static NSString *CellIdentifier = @"Cell";
    ContactCell *cell = (ContactCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[ContactCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    XMPPUserCoreDataStorageObject *user = [self.contactTable.groupMemberArray objectAtIndex:indexPath.row];
    cell.contactNameLbl.text = user.displayName;
    cell.contactImageView.image = [avatarImg profileImage:user];
    [cell.checkView removeFromSuperview];
    cell.unreadMessageLabel.hidden = YES;
    return cell;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - TableView Editing Method
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [self.contactTable.groupMemberArray removeObjectAtIndex:indexPath.row];
        [self.groupMemberTableView reloadData];
        [self.contactTable reloadData];
        if (self.contactTable.groupMemberArray.count == 0 )
            createBtn.enabled = NO;
    }
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - TextField delegates
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString * searchStr = [self.groupNameTextFld.text stringByReplacingCharactersInRange:range withString:string];
    if (self.contactTable.groupMemberArray.count > 0 && [searchStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length > 0)
        createBtn.enabled = YES;
    else
        createBtn.enabled = NO;
    
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Dont Button Action in Contact TableView
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)doneBtnCliked:(id)sender
{
    [[self view] endEditing:YES];
    [UIView animateWithDuration:0.25 animations:^{
        contactView.frame =  CGRectMake(0, self.view.frame.size.height+10, self.view.frame.size.width, self.view.frame.size.height);
    }];
    [self.groupMemberTableView reloadData];
    if (self.contactTable.groupMemberArray.count > 0 && [self.groupNameTextFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length > 0)
        createBtn.enabled = YES;
    else
        createBtn.enabled = NO;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Next Button Action
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)createBtnAction
{
    
    NSString *groupName = nil;
    
    NSDate *date = [[NSDate alloc]init];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMddhhmmss"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    groupName = [NSString stringWithFormat:@"%@_%@",[self xmppStream].myJID.user,stringFromDate];
    
    XMPPJID *jid = [XMPPJID jidWithUser:groupName domain:[NSString stringWithFormat:@"conference.%@",DOMAINAME] resource:@"user"];
    XMPPRoomCoreDataStorage *Storage = [XMPPRoomCoreDataStorage sharedInstance];
    xmppRoom = [[XMPPRoom alloc] initWithRoomStorage:Storage jid:jid dispatchQueue:dispatch_get_main_queue()];
    [xmppRoom activate:[self xmppStream]];
    [xmppRoom addDelegate:self delegateQueue:dispatch_get_main_queue()];
    [xmppRoom joinRoomUsingNickname:[self xmppStream].myJID.user history:nil password:PASSWORD_XMPP];
    [self ConfigureNewRoom];
    
}

-(void)ConfigureNewRoom
{
    [xmppRoom fetchConfigurationForm];
    
}
- (void)xmppRoomDidCreate:(XMPPRoom *)sender{
    // DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    [sender configureRoomUsingOptions:nil];
    
}
- (void)xmppRoomDidJoin:(XMPPRoom *)sender{
    // DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    [self sendCongfirurationForm:sender];
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^
                   {
                       
                       for (XMPPUserCoreDataStorageObject *str in self.contactTable.groupMemberArray) {
                           [sender inviteUser:[XMPPJID jidWithString:str.jidStr] withMessage:@"Pls Join the Group"];
                           XMPPMessage *groupInviteMessage = [XMPPMessage new];
                           [groupInviteMessage addAttributeWithName:@"id"  stringValue:[[self xmppStream] generateUUID]];
                           [groupInviteMessage addAttributeWithName:@"to" stringValue:str.jidStr];
                           [groupInviteMessage addAttributeWithName:@"from" stringValue:[self xmppStream].myJID.bare];
                           [groupInviteMessage addAttributeWithName:@"type" stringValue:@"chat"];
                           NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
                           [body setStringValue:@"Pls join the group"];
                           NSXMLElement *xValue = [NSXMLElement elementWithName:@"x"];
                           [xValue addAttributeWithName:@"xmlns" stringValue:@"jabber:x:conference"];
                           [xValue addAttributeWithName:@"jid" stringValue:sender.roomJID.bare];
                           [groupInviteMessage addChild:body];
                           [groupInviteMessage addChild:xValue];
                           [[self xmppStream] sendElement:groupInviteMessage];
                       }
                       [sender fetchBanList];
                       [sender fetchMembersList];
                       [sender fetchModeratorsList];
                       [xmppRoom removeDelegate:self];
                       [[self xmppStream] removeDelegate:self];
                       xmppRoom = nil;
                       
                       
                       XMPPMessage *mess = [[XMPPMessage alloc]init];
                       [mess addAttributeWithName:@"id" stringValue:[NSString stringWithFormat:@"%@",[[self xmppStream] generateUUID]]];
                       [mess addAttributeWithName:@"from" stringValue:sender.roomJID.bare];
                       [mess addAttributeWithName:@"to" stringValue:sender.roomJID.bare];
                       [mess addAttributeWithName:@"type" stringValue:@"groupchat"];
                       NSXMLElement *subjectElement = [NSXMLElement elementWithName:@"subject" stringValue:self.groupNameTextFld.text];
                       [mess addChild:subjectElement];
                       [[self xmppStream] sendElement:mess];
                       
                       
                       XMPPMessage *groupJoinMessage = [[XMPPMessage alloc]init];
                       [groupJoinMessage addAttributeWithName:@"id" stringValue:[NSString stringWithFormat:@"%@",[[self xmppStream] generateUUID]]];
                       [groupJoinMessage addAttributeWithName:@"to" stringValue:sender.roomJID.bare];
                       
                       [groupJoinMessage addAttributeWithName:@"type" stringValue:@"groupchat"];
                       NSXMLElement *groupJoinLeave = [NSXMLElement elementWithName:@"group"];
                       [groupJoinLeave addAttributeWithName:@"xmlns" stringValue:[NSString stringWithFormat:@"%@:group",DOMAINAME]];
                       [groupJoinLeave addAttributeWithName:@"type" stringValue:@"1"];
                       [groupJoinMessage addChild:groupJoinLeave];
                       NSXMLElement *bodyMessage = [NSXMLElement elementWithName:@"body" stringValue:@"joined"];
                       [groupJoinMessage addChild:bodyMessage];
                       [[self xmppStream] sendElement:groupJoinMessage];
                       
                       if (isProfilePicChanged)
                       {
                           
                           [[GroupIconService sharedInstance] saveImageToUploadInserver:sender.roomJID Photo:self.groupImageView.image compeletion:^(BOOL finished,NSString *filePath)
                            {
                                if (finished) {
                                    [[GroupIconService sharedInstance] uploadGroupImagetoServer:sender.roomJID Photo:filePath compeletion:^(BOOL uploadFinished)
                                     {
                                         if (uploadFinished) {
                                             XMPPMessage *groupIconMessage = [[XMPPMessage alloc]init];
                                             [groupIconMessage addAttributeWithName:@"id" stringValue:[NSString stringWithFormat:@"%@",[[self xmppStream] generateUUID]]];
                                             [groupIconMessage addAttributeWithName:@"to" stringValue:sender.roomJID.bare];
                                             
                                             [groupIconMessage addAttributeWithName:@"type" stringValue:@"groupchat"];
                                             NSXMLElement *groupImage = [NSXMLElement elementWithName:@"group"];
                                             [groupImage addAttributeWithName:@"xmlns" stringValue:[NSString stringWithFormat:@"%@:group",DOMAINAME]];
                                             [groupImage addAttributeWithName:@"type" stringValue:@"3"];
                                             [groupIconMessage addChild:groupImage];
                                             NSXMLElement *groupiconbodyMessage = [NSXMLElement elementWithName:@"body" stringValue:@"Group icon is changed"];
                                             [groupIconMessage addChild:groupiconbodyMessage];
                                             [[self xmppStream] sendElement:groupIconMessage];
                                             
                                             [[GroupIconService sharedInstance] downloadIconAndSave:sender.roomJID];
                                             [[GroupIconService sharedInstance] removeIconFromFilePath:filePath];
                                         }
                                         
                                         
                                     }];
                                }
                                
                            }];
                       }
                       
                   });
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)sendCongfirurationForm:(XMPPRoom*)sender
{
    NSXMLElement *query = [[NSXMLElement alloc] initWithXMLString:@"<query xmlns='http://jabber.org/protocol/muc#owner'/>"
                                                            error:nil];
    NSXMLElement *x = [NSXMLElement elementWithName:@"x"
                                              xmlns:@"jabber:x:data"];
    [x addAttributeWithName:@"type" stringValue:@"submit"];
    NSXMLElement *field1 = [NSXMLElement elementWithName:@"field"];
    [field1 addAttributeWithName:@"var" stringValue:@"FORM_TYPE"];
    NSXMLElement *value1 = [NSXMLElement elementWithName:@"value"
                                             stringValue:@"http://jabber.org/protocol/muc#roomconfig"];
    [field1 addChild:value1];
    
    NSXMLElement *field2 = [NSXMLElement elementWithName:@"field"];
    [field2 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_roomname"];
    NSXMLElement *value2 = [NSXMLElement elementWithName:@"value" stringValue:self.groupNameTextFld.text];
    [field2 addChild:value2];
    
    NSXMLElement *field3 = [NSXMLElement elementWithName:@"field"];
    [field3 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_roomdesc"];
    NSXMLElement *value3 = [NSXMLElement elementWithName:@"value" stringValue:@"new"];
    [field3 addChild:value3];
    
    NSXMLElement *field35 = [NSXMLElement elementWithName:@"field"];
    [field35 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_enablelogging"];
    NSXMLElement *value35 = [NSXMLElement elementWithName:@"value" stringValue:@"1"];
    [field35 addChild:value35];
    
    
    NSXMLElement *field4 = [NSXMLElement elementWithName:@"field"];
    [field4 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_changesubject"];
    NSXMLElement *value4 = [NSXMLElement elementWithName:@"value" stringValue:@"1"];
    [field4 addChild:value4];
    
    NSXMLElement *field5 = [NSXMLElement elementWithName:@"field"];
    [field5 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_allowinvites"];
    NSXMLElement *value5 = [NSXMLElement elementWithName:@"value" stringValue:@"1"];
    [field5 addChild:value5];
    
    NSXMLElement *field6 = [NSXMLElement elementWithName:@"field"];
    [field6 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_allowpm"];
    NSXMLElement *value6 = [NSXMLElement elementWithName:@"value" stringValue:@"anyone"];
    [field6 addChild:value6];
    
    NSXMLElement *field7 = [NSXMLElement elementWithName:@"field"];
    [field7 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_maxusers"];
    NSXMLElement *value7 = [NSXMLElement elementWithName:@"value" stringValue:@"50"];
    [field7 addChild:value7];
    
    
    NSXMLElement *field8 = [NSXMLElement elementWithName:@"field"];
    [field8 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_publicroom"];
    NSXMLElement *value8 = [NSXMLElement elementWithName:@"value" stringValue:@"1"];
    [field8 addChild:value8];
    
    NSXMLElement *field9 = [NSXMLElement elementWithName:@"field"];
    [field9 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_persistentroom"];
    NSXMLElement *value9 = [NSXMLElement elementWithName:@"value" stringValue:@"1"];
    [field9 addChild:value9];
    
    
    NSXMLElement *field10 = [NSXMLElement elementWithName:@"field"];
    [field10 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_moderatedroom"];
    NSXMLElement *value10 = [NSXMLElement elementWithName:@"value" stringValue:@"1"];
    [field10 addChild:value10];
    
    
    NSXMLElement *field11 = [NSXMLElement elementWithName:@"field"];
    [field11 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_membersonly"];
    NSXMLElement *value11 = [NSXMLElement elementWithName:@"value" stringValue:@"1"];
    [field11 addChild:value11];
    
    NSXMLElement *field12 = [NSXMLElement elementWithName:@"field"];
    [field12 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_passwordprotectedroom"];
    NSXMLElement *value12 = [NSXMLElement elementWithName:@"value" stringValue:@"0"];
    [field12 addChild:value12];
    
    
    NSXMLElement *field13 = [NSXMLElement elementWithName:@"field"];
    [field13 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_roomsecret"];
    NSXMLElement *value13 = [NSXMLElement elementWithName:@"value" stringValue:self.groupNameTextFld.text];
    [field13 addChild:value13];
    
    
    NSXMLElement *field14 = [NSXMLElement elementWithName:@"field"];
    [field14 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_whois"];
    NSXMLElement *value14 = [NSXMLElement elementWithName:@"value" stringValue:@"anyone"];
    [field14 addChild:value14];
    
    NSXMLElement *field15 = [NSXMLElement elementWithName:@"field"];
    [field15 addAttributeWithName:@"var" stringValue:@"muc#maxhistoryfetch"];
    NSXMLElement *value15 = [NSXMLElement elementWithName:@"value" stringValue:@"10"];
    [field15 addChild:value15];
    
    NSXMLElement *field16 = [NSXMLElement elementWithName:@"field"];
    [field16 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_roomadmins"];
    NSXMLElement *value16 = [NSXMLElement elementWithName:@"value" stringValue:[self xmppStream].myJID.bare];
    [field16 addChild:value16];
    
    NSXMLElement *field17 = [NSXMLElement elementWithName:@"field"];
    [field17 addAttributeWithName:@"var" stringValue:@"x-muc#roomconfig_registration"];
    NSXMLElement *value17 = [NSXMLElement elementWithName:@"value" stringValue:@"0"];
    [field17 addChild:value17];
    
    
    NSXMLElement *field18 = [NSXMLElement elementWithName:@"field"];
    [field18 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_presencebroadcast"];
    NSXMLElement *value181 = [NSXMLElement elementWithName:@"value" stringValue:@"moderator"];
    NSXMLElement *value182 = [NSXMLElement elementWithName:@"value" stringValue:@"participant"];
    NSXMLElement *value183 = [NSXMLElement elementWithName:@"value" stringValue:@"visitor"];
    
    [field18 addChild:value181];
    [field18 addChild:value182];
    [field18 addChild:value183];
    
    
    NSXMLElement *field19 = [NSXMLElement elementWithName:@"field"];
    [field19 addAttributeWithName:@"var" stringValue:@"x-muc#roomconfig_reservednick"];
    NSXMLElement *value19 = [NSXMLElement elementWithName:@"value" stringValue:@"1"];
    [field19 addChild:value19];
    
    [x addChild:field1];
    [x addChild:field2];
    [x addChild:field3];
    [x addChild:field35];
    [x addChild:field4];
    [x addChild:field5];
    [x addChild:field6];
    [x addChild:field7];
    [x addChild:field8];
    [x addChild:field9];
    [x addChild:field10];
    [x addChild:field11];
    [x addChild:field12];
    [x addChild:field13];
    [x addChild:field14];
    [x addChild:field15];
    [x addChild:field16];
    [x addChild:field17];
    [x addChild:field18];
    [query addChild:x];
    
    NSXMLElement *roomOptions = [NSXMLElement elementWithName:@"iq"];
    [roomOptions addAttributeWithName:@"from" stringValue:[NSString stringWithFormat:@"%@",[XMPPJID jidWithString:[[self xmppStream] myJID].user resource:[[self xmppStream] myJID].resource]]];
    [roomOptions addAttributeWithName:@"type" stringValue:@"set"];
    [roomOptions addAttributeWithName:@"id" stringValue:[[self xmppStream] generateUUID]];
    [roomOptions addAttributeWithName:@"to" stringValue:sender.roomJID.bare];
    [roomOptions addChild:query];
    [[self xmppStream] sendElement:roomOptions];
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark XMPPRoom delegates
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)xmppRoomDidLeave:(XMPPRoom *)sender
{
    //   DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    //  NSLog(@"ROOM DID LEAVE");
}

- (void)xmppRoom:(XMPPRoom *)sender occupantDidJoin:(XMPPJID *)occupantJID withPresence:(XMPPPresence *)presence;
{
    //   DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    NSLog(@"%@",[NSString stringWithFormat:@"occupant did join: %@", [occupantJID resource]]);
}
- (void)xmppRoom:(XMPPRoom *)sender occupantDidLeave:(XMPPJID *)occupantJID withPresence:(XMPPPresence *)presence
{
    //  DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    //  NSLog(@"%@",[NSString stringWithFormat:@"occupant did Leave: %@", [occupantJID resource]]);
}

- (void)xmppRoom:(XMPPRoom *)sender didReceiveMessage:(XMPPMessage *)message fromOccupant:(XMPPJID *)occupantJID
{
    
}
- (void)xmppRoom:(XMPPRoom *)sender didFetchConfigurationForm:(NSXMLElement *)configForm{
    
    //  DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    //  NSLog(@"CONFIGAT:::::%@",sender.roomJID.user);
}

- (void)xmppRoom:(XMPPRoom *)sender didFetchBanList:(NSArray *)items
{
    //   DDLogInfo(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)xmppRoom:(XMPPRoom *)sender didNotFetchBanList:(XMPPIQ *)iqError
{
    //   DDLogInfo(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)xmppRoom:(XMPPRoom *)sender didFetchMembersList:(NSArray *)items
{
    //  DDLogInfo(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)xmppRoom:(XMPPRoom *)sender didNotFetchMembersList:(XMPPIQ *)iqError
{
    //   DDLogInfo(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)xmppRoom:(XMPPRoom *)sender didFetchModeratorsList:(NSArray *)items
{
    //  DDLogInfo(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)xmppRoom:(XMPPRoom *)sender didNotFetchModeratorsList:(XMPPIQ *)iqError
{
    //   DDLogInfo(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)handleDidLeaveRoom:(XMPPRoom *)room
{
    // DDLogInfo(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end