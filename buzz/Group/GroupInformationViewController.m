//
//  GroupInformationViewController.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 11/07/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "GroupInformationViewController.h"
#import "contactTableView.h"
#import "AvatarImage.h"
#import "PECropViewController.h"
#import "GroupIconService.h"
#import "sharedfileView.h"
#import "DetailGalleryViewController.h"
#import "UIImageEffects.h"
#import "MessageMethods.h"

static const int ddLogLevel = LOG_LEVEL_VERBOSE;

#define Xposition 10
#define Width 300
@interface GroupInformationViewController ()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,PECropViewControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,SharedFiledelegate>
@property(nonatomic,strong)UIImageView *groupImageView;
@property(nonatomic,retain)UITextField *groupNameTextFld;
@property(nonatomic,retain)contactTableView *contactTable;
@property (nonatomic,strong)  XMPPRoom   * xmppRoom;;


@end



@implementation GroupInformationViewController
{
    NSMutableArray* memberArray;
    UITableView *groupMemberTableView;
    UIView *contactView;
    NSMutableArray *userdetailsArray;
    AvatarImage *avatarImage;
    DotLineView *lineView;
    UILabel *sharedfileLabel;
    BOOL carouselCheck;
    sharedfileView *shareFileView;
    UILabel *groupMemberlabel;
    NSOperationQueue *groupIconQueue;
    BOOL isFirstTime;
    UIButton *backButton;
    UILabel *titleLabel;
    UIView *groupNameView;
    UIImageView  *blurView;
    
    BOOL isInAddParticpant;

    
}

@synthesize xmppRoom;
- (void)loadView
{
    self.view = [[UIView alloc]initWithFrame:[[UIScreen mainScreen] applicationFrame]];
    
    if(IS_IPHONE_4)
    {
        self.view.backgroundColor = BACKGROUNDIMAGE_4;
        
    }
    else
    {
        self.view.backgroundColor = BACKGROUNDIMAGE;
        
    }
    
    
    contactView = [UIView new];
    lineView = [DotLineView new];
    self.navigationItem.hidesBackButton = YES;
    
    UIImage *backButtonImage = [UIImage imageNamed:@"back_btn"];
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 5, backButtonImage.size.width, backButtonImage.size.height);
    [backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonAction) forControlEvents:UIControlEventTouchUpInside];
    
    titleLabel = [UILabel new];
    titleLabel.frame = CGRectMake(0, 10, self.view.frame.size.width, 30);
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = @"Group info";
    titleLabel.font = [UIFont fontWithName:FontString size:20];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    
    
    
    
    
    
    [self groupDetailsView];
    [self sharedFilesView];
    [self groupMemberView];
    memberArray = [NSMutableArray new];
    userdetailsArray = [NSMutableArray new];
    avatarImage = [AvatarImage new];
    groupIconQueue = [NSOperationQueue new];
    
}

-(void)groupDetailsView
{
    
    blurView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width,(self.view.frame.size.height * 35)/100)];
    blurView.backgroundColor = [UIColor clearColor];
    blurView.userInteractionEnabled = YES;
    [self.view addSubview:blurView];
    
    
    
    self.groupImageView = [UIImageView new];
    self.groupImageView.frame = CGRectMake((blurView.frame.size.width/2)-50, (blurView.frame.size.height/2)-50, 100, 100);
    self.groupImageView.layer.cornerRadius = 50;
    self.groupImageView.layer.borderWidth = 2.0;
    self.groupImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.groupImageView.clipsToBounds = YES;
    self.groupImageView.userInteractionEnabled = YES;
    [blurView addSubview:self.groupImageView];
    self.groupImageView.image = [UIImage imageNamed:@"profpic"];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addImage)];
    singleTap.numberOfTapsRequired = 1;
    [self.groupImageView addGestureRecognizer:singleTap];
    
    
    groupNameView = [UIView new];
    groupNameView.frame = CGRectMake(10, blurView.frame.size.height - 50, self.view.frame.size.width - 20, 50);
    groupNameView.backgroundColor = [UIColor clearColor];
    [blurView addSubview:groupNameView];
    
    
    self.groupNameTextFld = [UITextField new];
    self.groupNameTextFld.frame = CGRectMake(0,10,groupNameView.frame.size.width , 40);
    self.groupNameTextFld.borderStyle = UITextBorderStyleNone;
    self.groupNameTextFld.backgroundColor = [UIColor clearColor];
    self.groupNameTextFld.textColor = [UIColor whiteColor];
    self.groupNameTextFld.font = [UIFont fontWithName:FontString size:13];
    self.groupNameTextFld.autocorrectionType = UITextAutocorrectionTypeNo;
    self.groupNameTextFld.keyboardType = UIKeyboardTypeDefault;
    self.groupNameTextFld.returnKeyType = UIReturnKeyDone;
    self.groupNameTextFld.textAlignment = NSTextAlignmentCenter;
    self.groupNameTextFld.delegate = self;
    [groupNameView addSubview:self.groupNameTextFld];
    
}
-(void)sharedFilesView
{
    UIView *SharedFileslineView = [UIView new];
    SharedFileslineView.frame = CGRectMake(0, blurView.frame.origin.y+blurView.frame.size.height+22, self.view.frame.size.width, 1);
    SharedFileslineView.backgroundColor = RGBA(119, 186, 240, 0.2);
    [self.view addSubview:SharedFileslineView];
    
    sharedfileLabel = [UILabel new];
    sharedfileLabel.frame = CGRectMake((self.view.frame.size.width/2)-50, blurView.frame.origin.y+blurView.frame.size.height+10,100,25);
    sharedfileLabel.backgroundColor = RGBA(119, 186, 240,0.5);
    sharedfileLabel.textColor = [UIColor whiteColor];
    sharedfileLabel.textAlignment = NSTextAlignmentCenter;
    sharedfileLabel.layer.cornerRadius = 10;
    sharedfileLabel.clipsToBounds = YES;
    sharedfileLabel.font = [UIFont fontWithName:FontString size:14];
    sharedfileLabel.text = @"Shared Files";
    [self.view addSubview:sharedfileLabel];
    
    shareFileView = [[sharedfileView alloc]initWithFrame:CGRectMake(Xposition, sharedfileLabel.frame.origin.y+sharedfileLabel.frame.size.height+10, Width, 60)];
    shareFileView.backgroundColor = [UIColor clearColor];
    shareFileView.delegate = self;
    [self.view addSubview:shareFileView];
    
}
-(void)groupMemberView
{
    
    UIView *groupMemberlineView = [UIView new];
    groupMemberlineView.frame = CGRectMake(0, shareFileView.frame.origin.y+shareFileView.frame.size.height+22, self.view.frame.size.width, 1);
    groupMemberlineView.backgroundColor = RGBA(119, 186, 240,0.2);
    [self.view addSubview:groupMemberlineView];
    
    
    groupMemberlabel = [UILabel new];
    groupMemberlabel.frame = CGRectMake((self.view.frame.size.width/2)- 75, shareFileView.frame.origin.y+shareFileView.frame.size.height+10,150,25);
    groupMemberlabel.backgroundColor = RGBA(119, 186, 240, 0.5);
    groupMemberlabel.textColor = [UIColor whiteColor];
    groupMemberlabel.textAlignment = NSTextAlignmentCenter;
    groupMemberlabel.font = [UIFont fontWithName:FontString size:12];
    groupMemberlabel.layer.cornerRadius = 10;
    groupMemberlabel.clipsToBounds = YES;
    groupMemberlabel.text = @"Online Group Members";
    [self.view addSubview:groupMemberlabel];
    
    groupMemberTableView = [UITableView new];
    groupMemberTableView.frame = CGRectMake(0, groupMemberlabel.frame.origin.y+groupMemberlabel.frame.size.height+10,self.view.frame.size.width, (self.view.frame.size.height)-(groupMemberlabel.frame.origin.y+groupMemberlabel.frame.size.height-10));
    groupMemberTableView.delegate = self;
    groupMemberTableView.dataSource = self;
    groupMemberTableView.backgroundColor = [UIColor clearColor];
    groupMemberTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:groupMemberTableView];
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [[self appDelegate].statusBarView setHidden:NO];
    
    [self.navigationController.navigationBar addSubview:titleLabel];
    
    [self.navigationController.navigationBar addSubview:backButton];
    
    if (!isFirstTime)
    {
        self.groupNameTextFld.text = self.titleString;
        self.groupImageView.image = [avatarImage groupProfileimage:self.roomJID];
        blurView.image = [UIImageEffects imageByApplyingLightEffectToProfileImage:[avatarImage groupProfileimage:self.roomJID]];
        
        [[GroupIconService sharedInstance]downloadIconAndSave:self.roomJID];
        [self getMemberlist];
        isFirstTime = YES;
        isInAddParticpant = NO;
    }
    
    [shareFileView getData:self.roomJID];

    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [backButton removeFromSuperview];
    [titleLabel removeFromSuperview];
    [shareFileView cancelAllOperations];
}
-(void)backbuttonAction
{
    if (isInAddParticpant) {
        
        [[self view] endEditing:YES];
        [UIView animateWithDuration:0.25 animations:^{
            contactView.frame =  CGRectMake(0, self.view.frame.size.height+10, self.view.frame.size.width, self.view.frame.size.height);
        } completion:^(BOOL finished)
         {
             [contactView removeFromSuperview];
             isInAddParticpant = NO;
         }];
        
    }
    else
    {
    
    [self.navigationController popViewControllerAnimated:YES];
    [backButton removeFromSuperview];
    isFirstTime = NO;
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)didSelectShareFile:(NSIndexPath *)indexpath Array:(NSMutableArray *)ary
{
    DetailGalleryViewController *detailsView = [DetailGalleryViewController new];
    detailsView.detailArray = [ary mutableCopy];
    detailsView.indexpath = indexpath;
    [self.navigationController pushViewController:detailsView animated:NO];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return memberArray.count+1;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 55;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"groupInformationcell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    //** Declare all the Variables
    
    if (cell == nil) {
        
        cell  = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        // Initialization code
        
        UIImageView *cellgroupImageView = [UIImageView new];
        cellgroupImageView.frame = CGRectMake(cell.frame.origin.x+10, cell.frame.origin.y+5, 40, 40);
        cellgroupImageView.layer.cornerRadius = 20;
        cellgroupImageView.clipsToBounds = YES;
        cellgroupImageView.tag = 1;
        [cell.contentView addSubview:cellgroupImageView];
        
        UILabel *groupNameLbl = [UILabel new];
        groupNameLbl.frame = CGRectMake(cellgroupImageView.frame.origin.x+cellgroupImageView.frame.size.width+20, cell.frame.origin.y+10, 220, 30);
        groupNameLbl.backgroundColor = [UIColor clearColor];
        groupNameLbl.textColor =[UIColor blackColor];
        groupNameLbl.textAlignment = NSTextAlignmentCenter;
        groupNameLbl.font = [UIFont fontWithName:FontString size:15];
        groupNameLbl.tag = 2;
        [cell.contentView addSubview:groupNameLbl];
        [lineView drawLine:CGRectMake(10, 55-0.2, self.view.frame.size.width-20, 0.2) :cell :[UIColor grayColor]];
        
        UILabel *groupAdmin = [UILabel new];
        groupAdmin.tag = 3;
        groupAdmin.frame = CGRectMake(self.view.frame.size.width - 100, cell.frame.origin.y + 15, 80, 20);
        groupAdmin.backgroundColor = RGBA(119, 186, 240, 0.5);
        groupAdmin.textColor =[UIColor whiteColor];
        groupAdmin.textAlignment = NSTextAlignmentCenter;
        groupAdmin.font = [UIFont fontWithName:FontString size:10];
        groupAdmin.layer.cornerRadius = 10;
        groupAdmin.clipsToBounds = YES;
        [cell.contentView addSubview:groupAdmin];
    }
    
    UIImageView *gImageView = (UIImageView *)[cell viewWithTag:1];
    UILabel *groupAdmin = (UILabel *)[cell viewWithTag:3];
    
    if(indexPath.row == memberArray.count)
    {
        UILabel *groupNameLabel = (UILabel *)[cell viewWithTag:2];
        groupNameLabel.text = @"Delete and Exit group";
        groupNameLabel.font = [UIFont fontWithName:FontString size:18];
        groupNameLabel.textAlignment = NSTextAlignmentCenter;
        groupNameLabel.textColor = [UIColor redColor];
        gImageView.hidden = YES;
        groupAdmin.hidden = YES;
    }
    
    else if([memberArray[indexPath.row][@"Can_add_Memebers"] isEqualToString:@"0"] && indexPath.row == memberArray.count - 1)
    {
        UILabel *groupNameLabel = (UILabel *)[cell viewWithTag:2];
        groupNameLabel.text = @"Add Participants";
        groupNameLabel.font = [UIFont fontWithName:FontString size:18];
        groupNameLabel.textAlignment = NSTextAlignmentCenter;
        groupNameLabel.textColor = [UIColor greenColor];
        gImageView.hidden = YES;
        groupAdmin.hidden = YES;
    }
    else
    {
        gImageView.hidden = NO;
        gImageView.image = [avatarImage groupProfileimage:[XMPPJID jidWithString:memberArray[indexPath.row][@"jid"]]];
        UILabel *groupNameLabel = (UILabel *)[cell viewWithTag:2];
        groupNameLabel.textAlignment = NSTextAlignmentLeft;
        
        groupNameLabel.textColor = [UIColor blackColor];
        groupNameLabel.text = memberArray[indexPath.row][@"nickname"];
        groupAdmin.hidden = YES;
        
        if ([memberArray[indexPath.row][@"am_An_Owner"] isEqualToString:@"0"])
        {
            groupAdmin.hidden = NO;
            groupAdmin.text = @"Group Admin";
        }
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.row == memberArray.count)
    {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"This action will delete your group permanently. Continue? " message:nil delegate:self cancelButtonTitle:@"No!" otherButtonTitles:@"Yes!", nil];
        alert.tag = 112;
        [alert show];
        
    }
    else if([memberArray[indexPath.row][@"Can_add_Memebers"] isEqualToString:@"0"])
    {
        [self addParticipants];
        
    }
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 112 && buttonIndex == 1 )
    {
        
        [self deleteAndExitGroup];

    }

}

-(void)getMemberlist
{
    [memberArray removeAllObjects];
    XMPPRoomCoreDataStorage *storage = [XMPPRoomCoreDataStorage sharedInstance];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"roomJIDStr == %@", self.roomJID.bare];
    NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:storage.occupantEntityName
                                                         inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    request.predicate = predicate;
    [request setEntity:entityDescription];
    request.resultType = NSDictionaryResultType;
    request.propertiesToFetch = [NSArray arrayWithObjects:[[entityDescription propertiesByName] objectForKey:@"nickname"],[[entityDescription propertiesByName] objectForKey:@"affiliation"],nil];
    request.returnsDistinctResults = YES;
    
    // Now it should yield an NSArray of distinct values in dictionaries.
    NSArray *dictionaries = [moc executeFetchRequest:request error:nil];
    for (NSMutableDictionary* element in dictionaries)
    {
        NSMutableDictionary *detailsDict = [NSMutableDictionary new];
        [detailsDict setObject:[self getDisplayName:[element objectForKey:@"nickname"]] forKey:@"nickname"];
        [detailsDict setObject:[self getmemberJid:[element objectForKey:@"nickname"]] forKey:@"jid"];
        [detailsDict setObject:[element objectForKey:@"affiliation"] forKey:@"affiliation"];
        if ([[element valueForKey:@"affiliation"] isEqualToString:@"owner"])
        {
            [detailsDict setObject:@"0" forKey:@"am_An_Owner"];
            //  NSLog(@"am an OWNER");
        }
        else
        {
            [detailsDict setObject:@"1" forKey:@"am_An_Owner"];
            //    NSLog(@"Am not an OWNER");
            
        }
        
        
        [memberArray addObject:detailsDict];
    }
    
    for (NSMutableDictionary* element in dictionaries)
    {
        if ([[element valueForKey:@"affiliation"] isEqualToString:@"owner"])
        {
            if ([[self getmemberJid:[element objectForKey:@"nickname"]] isEqualToString:[self xmppStream].myJID.bare])
            {
                
                NSMutableDictionary *detailsDict = [NSMutableDictionary new];
                [detailsDict setObject:@"0" forKey:@"Can_add_Memebers"];
                
                
                [memberArray addObject:detailsDict];
                //NSLog(@"Current member can Add Participants");
                
            }
        }
    }
    
    [groupMemberTableView reloadData];
}
-(NSString *)getmemberJid:(NSString *)resourceStr
{
    if ([resourceStr rangeOfString:DOMAINAME].location==NSNotFound )
        resourceStr = [resourceStr stringByAppendingString:[NSString stringWithFormat:@"@%@",DOMAINAME]];
    
    return resourceStr;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Get Display Name for JID
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(NSString *)getDisplayName:(NSString *)resourceStr
{
    if ([resourceStr rangeOfString:DOMAINAME].location==NSNotFound )
        resourceStr = [resourceStr stringByAppendingString:[NSString stringWithFormat:@"@%@",DOMAINAME]];
    
    if ([resourceStr isEqualToString:[self xmppStream].myJID.bare])
        return @"You";
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:[self managedObjectContext_roster]];
    [request setEntity:entity];
    NSString *predicateFrmt = @"jidStr = %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, resourceStr];
    [request setPredicate:predicate];
    NSArray *empArray=[[self managedObjectContext_roster] executeFetchRequest:request error:nil];
    if ([empArray count] == 1)
    {
        for (XMPPUserCoreDataStorageObject *userdetails in empArray)
        {
            [userdetailsArray addObject:userdetails];
            return userdetails.displayName;
        }
    }
    return [resourceStr stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"@%@",DOMAINAME] withString:@""];;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Appdelegate values
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (NSManagedObjectContext *)managedObjectContext_roster
{
    return [[[self appDelegate] xmppRosterStorage] mainThreadManagedObjectContext];
}
-(AppDelegate *)appDelegate
{
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}
-(XMPPStream *)xmppStream{
    
    return [[self appDelegate] xmppStream];
}

-(void)clearAllMessageHistory
{
    NSManagedObjectContext *moc = [[self appDelegate].xmppMessageArchivingStorage mainThreadManagedObjectContext];
    [moc performBlockAndWait:^{
        NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:moc];
        NSFetchRequest *request = [[NSFetchRequest alloc]init];
        NSString *predicateFrmt = @"bareJidStr = %@";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, self.roomJID.bare];
        [request setPredicate:predicate];
        [request setEntity:entityDescription];
        NSError *error;
        NSArray *messages = [moc executeFetchRequest:request error:&error];
        
        for (XMPPMessageArchiving_Message_CoreDataObject *coremessage in messages)
        {
            [moc deleteObject:coremessage];
        }
        
        NSEntityDescription *contactEnityDes = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Contact_CoreDataObject"  inManagedObjectContext:moc];
        NSFetchRequest *contactRequest = [[NSFetchRequest alloc]init];
        [contactRequest setEntity:contactEnityDes];
        NSString *predicateFrmt2 = @"bareJidStr = %@";
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:predicateFrmt2, self.roomJID.bare];
        [contactRequest setPredicate:predicate2];
        NSArray *recentContactMessage = [moc executeFetchRequest:contactRequest error:&error];
        
        for (XMPPMessageArchiving_Contact_CoreDataObject *recentCoremessage in recentContactMessage) {
            [moc deleteObject:recentCoremessage];
        }
    }];
    NSError *error;
    if (![moc save:&error]) {
        
        //NSLog(@"DB SAVE ERROR in clearAllHistory%@",error);
    }
}
-(void)cleargroupDetails
{
    XMPPRoomCoreDataStorage *storage = [XMPPRoomCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
    [moc performBlockAndWait:^{
        NSEntityDescription *entityDescription = [NSEntityDescription entityForName:storage.occupantEntityName
                                                             inManagedObjectContext:moc];
        NSFetchRequest *request = [[NSFetchRequest alloc]init];
        [request setEntity:entityDescription];
        NSString *predicateFrmt = @"roomJIDStr = %@";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, self.roomJID.bare];
        [request setPredicate:predicate];
        NSError *error;

        NSArray *groupDetails = [moc executeFetchRequest:request error:&error];
        for (XMPPRoomMessageDetails *details in groupDetails)
        {
            [moc deleteObject:details];
        }
        
        NSEntityDescription *occupantentityDescription = [NSEntityDescription entityForName:storage.messageDetailsEntityName  inManagedObjectContext:moc];
        NSFetchRequest *occupantrequest = [[NSFetchRequest alloc]init];
        [occupantrequest setEntity:occupantentityDescription];
        NSString *predicateFrmt2 = @"roomJIDStr = %@";
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:predicateFrmt2, self.roomJID.bare];
        [occupantrequest setPredicate:predicate2];
        NSArray *occupantgroupDetails = [moc executeFetchRequest:occupantrequest error:&error];
        for (XMPPRoomOccupantCoreDataStorageObject *occupantdetails in occupantgroupDetails) {
            [moc deleteObject:occupantdetails];
        }
    }];
    
    NSError *error;
    if (![moc save:&error]) {
        NSLog(@"DB SAVE ERROR in CleargroupDetails%@",error);
    }
}
#pragma mark - Textfield delegate Methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    XMPPMessage *mess = [[XMPPMessage alloc]init];
    [mess addAttributeWithName:@"id" stringValue:[NSString stringWithFormat:@"%@",[[self xmppStream] generateUUID]]];
    [mess addAttributeWithName:@"from" stringValue:self.roomJID.bare];
    [mess addAttributeWithName:@"to" stringValue:self.roomJID.bare];
    [mess addAttributeWithName:@"type" stringValue:@"groupchat"];
    NSXMLElement *subjectElement = [NSXMLElement elementWithName:@"subject" stringValue:self.groupNameTextFld.text];
    [mess addChild:subjectElement];
    [[self xmppStream] sendElement:mess];
    return YES;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Tapping Action in groupImageView
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)addImage
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:NSLocalizedString(@"Photo Album", nil), nil];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Camera", nil)];
    }
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    actionSheet.cancelButtonIndex = actionSheet.numberOfButtons - 1;
    [actionSheet showFromToolbar:self.navigationController.toolbar];
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Method to access Media
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)showCamera
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        [[self appDelegate].statusBarView setHidden:YES];
        controller.delegate = self;
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        controller.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        [self presentViewController:controller animated:YES completion:NULL];
    }
}
- (void)openPhotoAlbum
{
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.delegate = self;
    controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:controller animated:YES completion:NULL];
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Action Sheet Delegate Method
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////-
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    if ([buttonTitle isEqualToString:NSLocalizedString(@"Photo Album", nil)]) {
        [self openPhotoAlbum];
    } else if ([buttonTitle isEqualToString:NSLocalizedString(@"Camera", nil)]) {
        [self showCamera];
    }
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Image Picker delegate Method
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        [self openEditor:image];
    }];
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Action for Editing Image
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)openEditor:(UIImage*)img
{
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.image = img;
    controller.cropAspectRatio = 1.0f;
    
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    
    [self presentViewController:navigationController animated:YES completion:NULL];
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark PECropViewControllerDelegate Methods
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage
{
    self.groupImageView.image = croppedImage;
    
    blurView.image = [UIImageEffects imageByApplyingLightEffectToProfileImage:croppedImage];
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
    NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^
                                   {
                                       [[GroupIconService sharedInstance] saveImageToUploadInserver:self.roomJID Photo:croppedImage compeletion:^(BOOL finished,NSString *filePath)
                                        {
                                            if (finished) {
                                                [[GroupIconService sharedInstance] uploadGroupImagetoServer:self.roomJID Photo:filePath compeletion:^(BOOL uploadFinished)
                                                 {
                                                     if (uploadFinished)
                                                     {
                                                         XMPPMessage *groupIconMessage = [[XMPPMessage alloc]init];
                                                         [groupIconMessage addAttributeWithName:@"id" stringValue:[NSString stringWithFormat:@"%@",[[self xmppStream] generateUUID]]];
                                                         [groupIconMessage addAttributeWithName:@"to" stringValue:self.roomJID.bare];
                                                         
                                                         [groupIconMessage addAttributeWithName:@"type" stringValue:@"groupchat"];
                                                         NSXMLElement *groupImage = [NSXMLElement elementWithName:@"group"];
                                                         [groupImage addAttributeWithName:@"xmlns" stringValue:[NSString stringWithFormat:@"%@:group",DOMAINAME]];
                                                         [groupImage addAttributeWithName:@"type" stringValue:@"3"];
                                                         [groupIconMessage addChild:groupImage];
                                                         NSXMLElement *groupiconbodyMessage = [NSXMLElement elementWithName:@"body" stringValue:@"Group icon is changed"];
                                                         [groupIconMessage addChild:groupiconbodyMessage];
                                                         [[self xmppStream] sendElement:groupIconMessage];
                                                         [[GroupIconService sharedInstance] removeIconFromFilePath:filePath];
                                                         [[GroupIconService sharedInstance]downloadIconAndSave:self.roomJID];
                                                         
                                                     }
                                                     else
                                                     {
                                                         [[NSOperationQueue mainQueue] addOperationWithBlock:^
                                                          {
                                                              [[[UIAlertView alloc] initWithTitle:ALERTVIEW_TITLE message:@"\u26A0 Error from Server" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
                                                          }];
                                                     }
                                                 }];
                                            }
                                            
                                        }];
                                       
                                   }];
    
    [operation setQueuePriority:NSOperationQueuePriorityVeryHigh];
    [groupIconQueue addOperation:operation];
}
- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

-(void)addParticipants
{
    NSLog(@"addParticipants Clicked");
    
    [self.contactTable.groupMemberArray removeAllObjects];
    [self.groupNameTextFld resignFirstResponder];
    if (!contactView)
    {
        contactView = [UIView new];
    }
    contactView.frame = CGRectMake(0, self.view.frame.size.height+10, self.view.frame.size.width, self.view.frame.size.height);
    contactView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg"]];
    [UIView animateWithDuration:0.25 animations:^{
        contactView.frame =  CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
        UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        doneBtn.frame = CGRectMake(contactView.frame.size.width-70,5,70,45);
        doneBtn.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
        [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
        doneBtn.titleLabel.font = [UIFont fontWithName:FontString size:20.0f];
        [doneBtn setTitleColor:RGBA(53, 152, 219, 1) forState:UIControlStateNormal];
        [doneBtn addTarget:self action:@selector(doneBtnCliked:) forControlEvents:UIControlEventTouchUpInside];
        [contactView addSubview:doneBtn];

        [_contactTable removeFromSuperview];
        if (_contactTable) {
            _contactTable = nil;
        }
        self.contactTable = [[contactTableView alloc]init];
        self.contactTable.frame = CGRectMake(0, 50, self.view.frame.size.width, self.view.frame.size.height-150);
        self.contactTable.viewControllerStr = @"createGroupController";
        self.contactTable.groupMemberList =  [self getMemberJIDList];
        [self.contactTable reloadData];
        
        [contactView addSubview:self.contactTable];

        
        
        [self.view addSubview:contactView];
    }];
    
    isInAddParticpant = YES;
    }


-(NSMutableArray *)getMemberJIDList
{
  
        NSMutableArray *JIDListAry = [NSMutableArray new];
        XMPPRoomCoreDataStorage *storage = [XMPPRoomCoreDataStorage sharedInstance];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"roomJIDStr == %@", self.roomJID.bare];
        NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
        NSEntityDescription *entityDescription = [NSEntityDescription entityForName:storage.occupantEntityName
                                                             inManagedObjectContext:moc];
        NSFetchRequest *request = [[NSFetchRequest alloc]init];
        request.predicate = predicate;
        [request setEntity:entityDescription];
        request.resultType = NSDictionaryResultType;
        request.propertiesToFetch = [NSArray arrayWithObjects:[[entityDescription propertiesByName] objectForKey:@"nickname"],[[entityDescription propertiesByName] objectForKey:@"affiliation"],nil];
        request.returnsDistinctResults = YES;
        
        // Now it should yield an NSArray of distinct values in dictionaries.
        NSArray *dictionaries = [moc executeFetchRequest:request error:nil];
        for (NSMutableDictionary* element in dictionaries)
        {
            [JIDListAry addObject :[self getmemberJid:[element objectForKey:@"nickname"]]];
        }
    
    return JIDListAry;
}
-(void)doneBtnCliked:(NSMutableArray*)selectedAry
{
    //   NSLog(@"selected array ====== %@",self.contactTable.groupMemberArray);
    
    [[self view] endEditing:YES];
    [UIView animateWithDuration:0.25 animations:^{
        contactView.frame =  CGRectMake(0, self.view.frame.size.height+10, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished)
    {
        [contactView removeFromSuperview];
        isInAddParticpant = NO;
    }];
    
    
    
    XMPPJID *jid = self.roomJID;
    XMPPRoomCoreDataStorage *Storage = [XMPPRoomCoreDataStorage sharedInstance];
    xmppRoom = [[XMPPRoom alloc] initWithRoomStorage:Storage jid:jid dispatchQueue:dispatch_get_main_queue()];
    [xmppRoom activate:[self xmppStream]];
    [xmppRoom addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    
    for (XMPPUserCoreDataStorageObject *user in self.contactTable.groupMemberArray )
    {
        
        [xmppRoom inviteUser:[XMPPJID jidWithString:user.jidStr] withMessage:@"Pls Join the Group"];
        XMPPMessage *groupInviteMessage = [XMPPMessage new];
        [groupInviteMessage addAttributeWithName:@"id"  stringValue:[[self xmppStream] generateUUID]];
        [groupInviteMessage addAttributeWithName:@"to" stringValue:user.jidStr];
        [groupInviteMessage addAttributeWithName:@"from" stringValue:[self xmppStream].myJID.bare];
        [groupInviteMessage addAttributeWithName:@"type" stringValue:@"chat"];
        NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
        [body setStringValue:@"Pls join the group"];
        NSXMLElement *xValue = [NSXMLElement elementWithName:@"x"];
        [xValue addAttributeWithName:@"xmlns" stringValue:@"jabber:x:conference"];
        [xValue addAttributeWithName:@"jid" stringValue:xmppRoom.roomJID.bare];
        [groupInviteMessage addChild:body];
        [groupInviteMessage addChild:xValue];
        [[self xmppStream] sendElement:groupInviteMessage];
        
     
        
        XMPPMessage *groupJoinMessage = [[XMPPMessage alloc]init];
        [groupJoinMessage addAttributeWithName:@"id" stringValue:[NSString stringWithFormat:@"%@",[[self xmppStream] generateUUID]]];
        [groupJoinMessage addAttributeWithName:@"to" stringValue:self.roomJID.bare];
        [groupJoinMessage addAttributeWithName:@"type" stringValue:@"groupchat"];
        NSXMLElement *groupJoinLeave = [NSXMLElement elementWithName:@"group"];
        [groupJoinLeave addAttributeWithName:@"xmlns" stringValue:[NSString stringWithFormat:@"%@:group",DOMAINAME]];
        [groupJoinLeave addAttributeWithName:@"type" stringValue:@"4"];
        [groupJoinLeave addAttributeWithName:@"who" stringValue:user.jidStr];
        [groupJoinMessage addChild:groupJoinLeave];
        NSXMLElement *bodyMessage = [NSXMLElement elementWithName:@"body" stringValue:@"invited"];
        [groupJoinMessage addChild:bodyMessage];
        [[self xmppStream] sendElement:groupJoinMessage];
        
        }
    
    [xmppRoom fetchBanList];
    [xmppRoom fetchMembersList];
    [xmppRoom fetchModeratorsList];
    
    
    
    
    
}



- (void)xmppRoomDidCreate:(XMPPRoom *)sender{
    //  DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    [sender configureRoomUsingOptions:nil];
    
}
- (void)xmppRoomDidJoin:(XMPPRoom *)sender{
    // DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    // [self sendCongfirurationForm:sender];
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^
                   {
                       
                       
                       [sender fetchBanList];
                       [sender fetchMembersList];
                       [sender fetchModeratorsList];
                       [xmppRoom removeDelegate:self];
                       [[self xmppStream] removeDelegate:self];
                       xmppRoom = nil;
                       
                       
                       XMPPMessage *groupJoinMessage = [[XMPPMessage alloc]init];
                       [groupJoinMessage addAttributeWithName:@"id" stringValue:[NSString stringWithFormat:@"%@",[[self xmppStream] generateUUID]]];
                       [groupJoinMessage addAttributeWithName:@"to" stringValue:sender.roomJID.bare];
                       
                       [groupJoinMessage addAttributeWithName:@"type" stringValue:@"groupchat"];
                       NSXMLElement *groupJoinLeave = [NSXMLElement elementWithName:@"group"];
                       [groupJoinLeave addAttributeWithName:@"xmlns" stringValue:[NSString stringWithFormat:@"%@:group",DOMAINAME]];
                       [groupJoinLeave addAttributeWithName:@"type" stringValue:@"1"];
                       [groupJoinMessage addChild:groupJoinLeave];
                       NSXMLElement *bodyMessage = [NSXMLElement elementWithName:@"body" stringValue:@"joined"];
                       [groupJoinMessage addChild:bodyMessage];
                       [[self xmppStream] sendElement:groupJoinMessage];
                       
                       
                   });
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)xmppRoom:(XMPPRoom *)sender occupantDidJoin:(XMPPJID *)occupantJID withPresence:(XMPPPresence *)presence;
{
    //   DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    NSLog(@"%@",[NSString stringWithFormat:@"occupant did join: %@", [occupantJID resource]]);
}

-(void)sendCongfirurationForm:(XMPPRoom*)sender
{
    NSXMLElement *query = [[NSXMLElement alloc] initWithXMLString:@"<query xmlns='http://jabber.org/protocol/muc#owner'/>"
                                                            error:nil];
    NSXMLElement *x = [NSXMLElement elementWithName:@"x"
                                              xmlns:@"jabber:x:data"];
    [x addAttributeWithName:@"type" stringValue:@"submit"];
    NSXMLElement *field1 = [NSXMLElement elementWithName:@"field"];
    [field1 addAttributeWithName:@"var" stringValue:@"FORM_TYPE"];
    NSXMLElement *value1 = [NSXMLElement elementWithName:@"value"
                                             stringValue:@"http://jabber.org/protocol/muc#roomconfig"];
    [field1 addChild:value1];
    
    NSXMLElement *field2 = [NSXMLElement elementWithName:@"field"];
    [field2 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_roomname"];
    NSXMLElement *value2 = [NSXMLElement elementWithName:@"value" stringValue:self.groupNameTextFld.text];
    [field2 addChild:value2];
    
    NSXMLElement *field3 = [NSXMLElement elementWithName:@"field"];
    [field3 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_roomdesc"];
    NSXMLElement *value3 = [NSXMLElement elementWithName:@"value" stringValue:@"new"];
    [field3 addChild:value3];
    
    NSXMLElement *field35 = [NSXMLElement elementWithName:@"field"];
    [field35 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_enablelogging"];
    NSXMLElement *value35 = [NSXMLElement elementWithName:@"value" stringValue:@"1"];
    [field35 addChild:value35];
    
    
    NSXMLElement *field4 = [NSXMLElement elementWithName:@"field"];
    [field4 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_changesubject"];
    NSXMLElement *value4 = [NSXMLElement elementWithName:@"value" stringValue:@"1"];
    [field4 addChild:value4];
    
    NSXMLElement *field5 = [NSXMLElement elementWithName:@"field"];
    [field5 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_allowinvites"];
    NSXMLElement *value5 = [NSXMLElement elementWithName:@"value" stringValue:@"1"];
    [field5 addChild:value5];
    
    NSXMLElement *field6 = [NSXMLElement elementWithName:@"field"];
    [field6 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_allowpm"];
    NSXMLElement *value6 = [NSXMLElement elementWithName:@"value" stringValue:@"anyone"];
    [field6 addChild:value6];
    
    NSXMLElement *field7 = [NSXMLElement elementWithName:@"field"];
    [field7 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_maxusers"];
    NSXMLElement *value7 = [NSXMLElement elementWithName:@"value" stringValue:@"50"];
    [field7 addChild:value7];
    
    
    NSXMLElement *field8 = [NSXMLElement elementWithName:@"field"];
    [field8 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_publicroom"];
    NSXMLElement *value8 = [NSXMLElement elementWithName:@"value" stringValue:@"1"];
    [field8 addChild:value8];
    
    NSXMLElement *field9 = [NSXMLElement elementWithName:@"field"];
    [field9 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_persistentroom"];
    NSXMLElement *value9 = [NSXMLElement elementWithName:@"value" stringValue:@"1"];
    [field9 addChild:value9];
    
    
    NSXMLElement *field10 = [NSXMLElement elementWithName:@"field"];
    [field10 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_moderatedroom"];
    NSXMLElement *value10 = [NSXMLElement elementWithName:@"value" stringValue:@"1"];
    [field10 addChild:value10];
    
    
    NSXMLElement *field11 = [NSXMLElement elementWithName:@"field"];
    [field11 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_membersonly"];
    NSXMLElement *value11 = [NSXMLElement elementWithName:@"value" stringValue:@"1"];
    [field11 addChild:value11];
    
    NSXMLElement *field12 = [NSXMLElement elementWithName:@"field"];
    [field12 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_passwordprotectedroom"];
    NSXMLElement *value12 = [NSXMLElement elementWithName:@"value" stringValue:@"0"];
    [field12 addChild:value12];
    
    
    NSXMLElement *field13 = [NSXMLElement elementWithName:@"field"];
    [field13 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_roomsecret"];
    NSXMLElement *value13 = [NSXMLElement elementWithName:@"value" stringValue:self.groupNameTextFld.text];
    [field13 addChild:value13];
    
    
    NSXMLElement *field14 = [NSXMLElement elementWithName:@"field"];
    [field14 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_whois"];
    NSXMLElement *value14 = [NSXMLElement elementWithName:@"value" stringValue:@"anyone"];
    [field14 addChild:value14];
    
    NSXMLElement *field15 = [NSXMLElement elementWithName:@"field"];
    [field15 addAttributeWithName:@"var" stringValue:@"muc#maxhistoryfetch"];
    NSXMLElement *value15 = [NSXMLElement elementWithName:@"value" stringValue:@"10"];
    [field15 addChild:value15];
    
    NSXMLElement *field16 = [NSXMLElement elementWithName:@"field"];
    [field16 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_roomadmins"];
    NSXMLElement *value16 = [NSXMLElement elementWithName:@"value" stringValue:[self xmppStream].myJID.bare];
    [field16 addChild:value16];
    
    NSXMLElement *field17 = [NSXMLElement elementWithName:@"field"];
    [field17 addAttributeWithName:@"var" stringValue:@"x-muc#roomconfig_registration"];
    NSXMLElement *value17 = [NSXMLElement elementWithName:@"value" stringValue:@"0"];
    [field17 addChild:value17];
    
    
    NSXMLElement *field18 = [NSXMLElement elementWithName:@"field"];
    [field18 addAttributeWithName:@"var" stringValue:@"muc#roomconfig_presencebroadcast"];
    NSXMLElement *value181 = [NSXMLElement elementWithName:@"value" stringValue:@"moderator"];
    NSXMLElement *value182 = [NSXMLElement elementWithName:@"value" stringValue:@"participant"];
    NSXMLElement *value183 = [NSXMLElement elementWithName:@"value" stringValue:@"visitor"];
    
    [field18 addChild:value181];
    [field18 addChild:value182];
    [field18 addChild:value183];
    
    
    NSXMLElement *field19 = [NSXMLElement elementWithName:@"field"];
    [field19 addAttributeWithName:@"var" stringValue:@"x-muc#roomconfig_reservednick"];
    NSXMLElement *value19 = [NSXMLElement elementWithName:@"value" stringValue:@"1"];
    [field19 addChild:value19];
    
    [x addChild:field1];
    [x addChild:field2];
    [x addChild:field3];
    [x addChild:field35];
    [x addChild:field4];
    [x addChild:field5];
    [x addChild:field6];
    [x addChild:field7];
    [x addChild:field8];
    [x addChild:field9];
    [x addChild:field10];
    [x addChild:field11];
    [x addChild:field12];
    [x addChild:field13];
    [x addChild:field14];
    [x addChild:field15];
    [x addChild:field16];
    [x addChild:field17];
    [x addChild:field18];
    [query addChild:x];
    
    NSXMLElement *roomOptions = [NSXMLElement elementWithName:@"iq"];
    [roomOptions addAttributeWithName:@"from" stringValue:[NSString stringWithFormat:@"%@",[XMPPJID jidWithString:[[self xmppStream] myJID].user resource:[[self xmppStream] myJID].resource]]];
    [roomOptions addAttributeWithName:@"type" stringValue:@"set"];
    [roomOptions addAttributeWithName:@"id" stringValue:[[self xmppStream] generateUUID]];
    [roomOptions addAttributeWithName:@"to" stringValue:sender.roomJID.bare];
    [roomOptions addChild:query];
    [[self xmppStream] sendElement:roomOptions];
}




#pragma mark - Delete And Exit Action
-(void)deleteAndExitGroup
{
    
    //this is to delete the group
    
    XMPPMessage *groupJoinMessage = [[XMPPMessage alloc]init];
    [groupJoinMessage addAttributeWithName:@"id" stringValue:[NSString stringWithFormat:@"%@",[[self xmppStream] generateUUID]]];
    [groupJoinMessage addAttributeWithName:@"to" stringValue:[self.roomJID full]];
    
    [groupJoinMessage addAttributeWithName:@"type" stringValue:@"groupchat"];
    NSXMLElement *groupJoinLeave = [NSXMLElement elementWithName:@"group"];
    [groupJoinLeave addAttributeWithName:@"xmlns" stringValue:[NSString stringWithFormat:@"%@:group",DOMAINAME]];
    [groupJoinLeave addAttributeWithName:@"type" stringValue:@"0"];
    [groupJoinMessage addChild:groupJoinLeave];
    NSXMLElement *bodyMessage = [NSXMLElement elementWithName:@"body" stringValue:@"left"];
    [groupJoinMessage addChild:bodyMessage];
    [[self xmppStream] sendElement:groupJoinMessage];
    
    
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //this is to inform other members who has left the group
        XMPPPresence *presence = [XMPPPresence presence];
        [presence addAttributeWithName:@"to" stringValue:[self.roomJID full]];
        [presence addAttributeWithName:@"type" stringValue:@"unavailable"];
        [[self xmppStream] sendElement:presence];
        [self clearAllMessageHistory];
        [self cleargroupDetails];
        [self.navigationController popToRootViewControllerAnimated:YES];
    });
    
}
@end
