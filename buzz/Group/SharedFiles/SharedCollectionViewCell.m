//
//  SharedCollectionViewCell.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 28/07/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "SharedCollectionViewCell.h"
#define widthOfBlackTransVw 30


@implementation SharedCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.imageview = [UIImageView new];
        self.imageview.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
        self.imageview.image = [UIImage imageNamed:[NSString stringWithFormat:@"defaultPerson.png"]];
        self.imageview.userInteractionEnabled = YES;
        [self.contentView addSubview:self.imageview];
        
        self.blackTransVw = [UIView new];
        self.blackTransVw.backgroundColor = RGBA(0, 0, 0, 0.6);
        self.blackTransVw.frame = CGRectMake((self.imageview.frame.size.width - widthOfBlackTransVw)/2, (self.imageview.frame.size.height - widthOfBlackTransVw)/2, widthOfBlackTransVw, widthOfBlackTransVw);
        self.blackTransVw.layer.cornerRadius = widthOfBlackTransVw/2;
        self.blackTransVw.layer.masksToBounds = YES;
        
        UIImage *playIcon = [UIImage imageNamed:@"play"];
        UIImageView* playImageIconView = [UIImageView new];
        playImageIconView.frame = CGRectMake((self.blackTransVw.frame.size.width - playIcon.size.width)/2, (self.blackTransVw.frame.size.height - playIcon.size.height)/2, playIcon.size.width, playIcon.size.height);
        playImageIconView.image = playIcon;
        [self.blackTransVw addSubview:playImageIconView];
    }
    return self;
}


@end
