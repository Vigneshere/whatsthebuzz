//
//  SharedCollectionViewCell.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 28/07/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SharedCollectionViewCell : UICollectionViewCell
@property(nonatomic,strong)UIImageView* imageview;
@property(nonatomic,strong) UIView* blackTransVw;

@end
