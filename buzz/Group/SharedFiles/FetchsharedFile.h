//
//  FetchsharedFile.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 28/07/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^FetchBlock)(NSArray *items, NSError *error);

@interface FetchsharedFile : UIView
@property (strong) NSOperationQueue *operationQueue;
+ (id)sharedMediaServer;
- (void)fetchMediaFiles:(NSString *)searchString block:(FetchBlock)block;
@end
