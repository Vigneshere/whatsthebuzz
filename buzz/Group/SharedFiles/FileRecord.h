//
//  PhotoRecord.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 28/07/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h> // because we need UIImage

@interface FileRecord : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSURL *URL;
@property (nonatomic,strong) NSString *type;
@property (nonatomic, readonly) BOOL hasFile;
@property (nonatomic, getter = isFailed) BOOL failed; 

@end