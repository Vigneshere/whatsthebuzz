//
//  FileRecord.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 28/07/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "FileRecord.h"

@implementation FileRecord

@synthesize name = _name;
@synthesize image = _image;
@synthesize URL = _URL;
@synthesize hasFile = _hasFile;
@synthesize failed = _failed;

- (BOOL)hasFile {
    return _image != nil;
}
- (BOOL)isFailed {
    return _failed;
}
@end