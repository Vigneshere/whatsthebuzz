//
//  FetchsharedFile.m
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 28/07/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import "FetchsharedFile.h"
#import "mediaDbEditor.h"

@implementation FetchsharedFile
@synthesize operationQueue = _operationQueue;


#pragma mark - Shared Class Instance
+ (id)sharedMediaServer;
{
    static dispatch_once_t onceToken;
    static id sharedMediaServer = nil;
    
    dispatch_once( &onceToken, ^{
        sharedMediaServer = [[[self class] alloc] init];
    });
    
    return sharedMediaServer;
}

#pragma mark - NSObject

- (id)init;
{
    if ( ( self = [super init] ) )
    {
        _operationQueue = [[NSOperationQueue alloc] init];
        _operationQueue.maxConcurrentOperationCount = 2;
    }
    
    return self;
}

#pragma mark - Fetch Files from Database
-(void)fetchMediaFiles:(NSString *)userID block:(FetchBlock)block
{
    NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^
                                  {
    NSManagedObjectContext *context = [[mediaDbEditor sharedInstance] managedObjectContext];
    NSFetchRequest *fetchrequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *fetchentity = [NSEntityDescription entityForName:@"FileInfo"
                                                   inManagedObjectContext:context];
    [fetchrequest setEntity:fetchentity];
    fetchrequest.resultType = NSDictionaryResultType;
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(userId = \"%@\") AND loc_cloud == 0",userID]];
    [fetchrequest setPredicate:pred];
    fetchrequest.propertiesToFetch = [NSArray arrayWithObjects:[[fetchentity propertiesByName] objectForKey:@"url"],[[fetchentity propertiesByName] objectForKey:@"video_img"],nil];
    NSError *error = nil;
    NSArray *fetchArray=[context executeFetchRequest:fetchrequest error:&error];
                                     
                                      //==>Main Queue
                                      [[NSOperationQueue mainQueue] addOperationWithBlock:^
                                      {
                                      if ( error )
                                          block( nil, error );
                                      else
                                          block( fetchArray, nil );
                                      }];
                                      
                                  }];
    
    [_operationQueue addOperation:operation];
}
@end
