//
//  GroupInformationViewController.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 11/07/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DDLog.h"

@interface GroupInformationViewController : UIViewController<UIAlertViewDelegate>
@property(nonatomic,strong)XMPPJID *roomJID;
@property(nonatomic,strong)NSString *titleString;

@end
