//
//  GroupIconService.h
//  buzz
//
//  Created by shaili_solutions_MacPro4 on 22/07/14.
//  Copyright (c) 2014 Shailisolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GroupIconService : NSObject
+ (GroupIconService*)sharedInstance;
-(void)downloadIconAndSave:(XMPPJID *)roomJid;
-(void)saveImageToUploadInserver:(XMPPJID *)roomjid Photo:(UIImage *)img compeletion:(void (^) (BOOL,NSString *))completed;
-(void)uploadGroupImagetoServer:(XMPPJID *)roomjid Photo:(NSString *)filepath compeletion:(void (^) (BOOL))completed;
-(void)removeIconFromFilePath:(NSString *)filepath;

@end
